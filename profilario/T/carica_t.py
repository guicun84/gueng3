from scipy import *
import pandas as pa

dati=[[20,3,4],
	[25,3.5,4.5],
	[30,3,4,5],
	[35,3.5,4,4.5,5.5],
	[40,4,5,6],
	[50,6,7],
	[60,7,8],
	[70,8,9],
	[80,9,10],
	[100,9,11]]

prT=pa.DataFrame()
for i in dati:
	b=i[0]
	for s in i[1:]:
		prT=prT.append({'b':b,'h':b,'s':s},ignore_index=1)

prT['A']=(prT.b+prT.h-prT.s)*prT.s #mm^2
prT['G']=prT.A/1e6*7850 #Kg/m
prT['ys']=prT.apply(lambda p:p.h-(p.b*p.s*(p.h-p.s/2) + (p.h-p.s)**2/2*p.s)/p.A,1)
prT['Jy']=prT.apply(lambda p:1/12*p.b*p.s**3 + p.b*p.s*(p.h-p.s/2)**2 + 1/12*p.s*(p.h-p.s)**3 + p.s*(p.h-p.s)**3/4 -p.A*(p.h-p.ys)**2,1)
prT['Jz']=prT.apply(lambda p:1/12*(p.b*p.s**3 + (p.h*p.s)*p.s**3),1)
prT['Wy']=prT.Jy/(prT.h-prT.ys)
prT['Wz']=prT.Jz/prT.b*2
prT['iz']=sqrt(prT.Jz/prT.A)
prT['iy']=sqrt(prT.Jy/prT.A)
prT['t']=prT['s']
prT['zg']=prT.ys
prT.drop(columns=['s','ys'])
prT['nome']=prT.apply(lambda x:f'TV{x.h:.0f}x{x.b:.0f}x{x.t:.1f}',1)
prT=prT['nome,b,h,t,G,A,Jy,Jz,Wy,Wz,iy,iz,zg'.split(',')]


prT.to_csv('T.csv')
print(prT)
