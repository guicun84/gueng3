from gueng.utils import Rettangolo as Re
def Wt(pr):
	'''calcolo del modulo di resistenza a torsione di un profilo'''
	return (Re(pr.b,pr.tf).Wt*2*pr.tf + Re(pr.h-2*pr.tf,pr.tw).Wt*pr.tw)/pr.tf
