#-*-coding:latin-*-
 
from .profili import prof
from gueng.acciaio.CNR import omega_1,omega
from gueng.utils import Verifiche

def predimensiona(l,qslu=None,qsle=None,query='',sort='G',r=1000,linst=None,prs=None,title='',cu=1,Mslu=None,d_=None,Minst=None,Tslu=None,Tsle=None,fyk=275.,a=1,multInst=1.4,rot=0,Nslu=0):
	'''l=luce di calcolo mm
	qslu=carico sulla trave a slu KN/m (N/mm)
	qsle=carico sulla trave a sle KN/m /N/mm)
	query= stringa da aggiungere alla query di base sulle verifiche es.'and h<=140'..
	sort= parametro su cui ordinare
	r= rapportro massimo tra luce e freccia
	linst= luce per il calcolo di omega 1, se None viene presa l
	prs= dataframe dei profili tra cui cercale (defalut tutti quelli di facile reperimento)
	title= identificativo della trave
	cu= coefficiente d'uso massimo
	Mslu= momento massimo imposto
	d_=deformazione *J imposta
	Minst momento instabilita' imposto
	a=inclinazione asse :1=orizontale 0=verticale
	multInst=1.4 coefficiente per instabilita' con cairchi superiori: [1.4 se carico destabilizzante, 1 se carico stabilizzante(inferiore)]
	rot=rotazione sezione 0 su asse forte in gradi
	Nslu=carico assiale'''
	fyd=fyk/1.15
	if linst is None: linst=l #luce per instabilita'
	if prs is None: prs=prof.travi_.copy() #profili da utilizzare
	dam=l/r #deformazione ammissibile
	#calcolo delle sollecitazioni se non imposte
	if Mslu is None: Mslu=qslu*l**2/8 
	if Tslu is None: Tslu=qslu*l/2
	if Tsle is None: Tsle=qsle*l/2

	#deformazione per peso proprio
	prs.loc[:,'dp']=5/384*prs.G*a/100*(l)**4/210e3/prs.Jy
	#deformazione per carichi esterni
	if d_ is None:
		prs.loc[:,'dq']=5/384*qsle*(l)**4/210e3/prs.Jy
	else:
		prs.loc[:,'dq']=d_/prs.Jy
	#sommo deformazioni
	prs.loc[:,'d']=prs.dp+prs.dq
	#momento per peso proprio
	prs.loc[:,'Mp']=prs.G*a/100*(l)**2/8
	#calcolo instabilita'
	if 'om1' not in list(prs.keys()): #posso cos� calcolarla a parte per casi + complessi
		if 'tf' in list(prs.keys()):
			prs.loc[:,'om1']=prs.apply(lambda x:omega_1(fyk,x.h,linst,x.b,x.tf),1)
		elif 's' in list(prs.keys()):
			prs.loc[:,'om1']=prs.apply(lambda x:omega_1(fyk,x.h,linst,x.b,x.s),1)
		elif 't' in list(prs.keys()):
			prs.loc[:,'om1']=prs.apply(lambda x:omega_1(fyk,x.h,linst,x.b,x.t),1)
		prs.loc[:,'om1']=prs.om1*multInst
	#calcolo momento resistente
	prs.loc[:,'Mrd']=prs.Wy*fyd
	#momento resistente per instabilita'
	prs.loc[:,'Mins']=prs.Mrd/prs.om1
	prs.loc[:,'peso']=prs.G*l/1e3
	#sforzo normale
	prs.loc[:,'Nrd']=prs.A*fyd
	#sforzo normale resistente per instabilita':
	if 'iv' in list(prs.keys()):
		prs.loc[:,'lam']=linst/prs.iv
	else:
		prs.loc[:,'lam']=linst/prs.iz
	prs.loc[:,'om']=prs.apply(lambda x:omega(x.lam,fyk,verbose=0),1)
	prs.loc[:,'Nins']=prs.Nrd/prs.om
	#calcolo taglio resistente
	if 'tf' in list(prs.keys()) and 'tw' in list(prs.keys()):
		prs.loc[:,'Vrd']=(prs.h-2*prs.tf)*prs.tw*fyd/(3)**.5/1.5
	elif 't' in list(prs.keys()):
		prs.loc[:,'Vrd']=(prs.h-2*prs.t)*prs.t*fyd/(3)**.5/1.5
	elif 's' in list(prs.keys()):
		prs.loc[:,'Vrd']=(prs.h-2*prs.s)*prs.s*fyd/(3)**.5/1.5
	#coefficienti d'uso
	prs.loc[:,'cu1']=(prs.d)/(dam) #deformazione
	prs.loc[:,'cu2']=(Mslu+prs.Mp*1.3)/prs.Mrd #momento flettente
	if Minst is None: 
		Minst=1.3*2/3*(Mslu) #momento instabilita'
	prs.loc[:,'cu3']=(Minst+(1.3*2/3*prs.Mp*1.3)) / prs.Mins
	prs.loc[:,'cu4']=(Tslu+1.3*prs.peso/200)/prs.Vrd #taglio
	prs.loc[:,'cu5']=Nslu/prs.Nrd #sforzo normale
	prs.loc[:,'cu6']=Nslu/prs.Nins #sforzo normale instabilita'
	prs.loc[:,'cu7']=prs.cu5+prs.cu2 #pressoflessioen
	prs.loc[:,'cu8']=prs.cu6+prs.cu3 #pressoflessione instabilita'
	prs.loc[:,'cu']=prs.apply(lambda x:x[['cu1','cu2','cu3','cu4','cu5','cu6','cu7','cu8']].max(),1)
	#ricerca del profilo migliore
#	prs_=prs.query('cu1<=@cu and cu2<=@cu and cu3<=@cu and cu4<=@cu and cu4<=@cu and cu6<=@cu and cu7<=@cu and cu8<=@cu' + query).sort(sort)[:10]
	prs_=prs.query('cu<=@cu ' + query).sort_values(sort)[:10]
	#se trovo un profilo migliore estraggo un po di dati ulteriori per output
	if prs_.shape[0]>=1:
		status=True
		pr=prs_.iloc[0]
		st_out=''
	else:
		status=False
		cumin=prs.cu.min()
		pr=prs.query('cu==@cumin').iloc[0]
		st_out='NESSUN PROFILO VERIFICATO'

	peso=pr.G*l/1e3
	ver=Verifiche(title + ':'+pr['name'],1)
	ver(pr.dq+pr.dp,1,dam,'deformazione assoluta',rel=l)
	ver(Mslu+1.3*pr.Mp,1,pr.Mrd,'verifica a flessione')
	ver(Minst+(1.3*2/3*pr.Mp*1.3),1,pr.Mins,'verifica a instabilita\' flessotorsionale')
	ver(Tslu+peso/2*1.3,1,pr.Vrd,'verifica a taglio')
	ver(Nslu,1,pr.Nrd,'verifica a compressione')
	ver(Nslu,1,pr.Nrd/pr.om, 'verifica a instabilita\' euleriana')
	ver(pr.cu7,1,1, 'verifica a pressoflessione')
	ver(pr.cu8,1,1, 'verifica a pressoflessione + instabilita\'')
	Mslu+=pr.Mp*1.3
	Tslu+=peso/2*1.3
	Tsle+=peso/2
	st_out='''{_title}\n{}'''.format(st_out,_title=title.upper())
	st_out +='''
Luci di calcolo
l={l} mm luce di calcolo
l_i={linst}mm luce per instabilita'''.format(**locals())
	if qslu is not None:
		st_out+='''

Carichi distribuiti:
q_slu={qslu:.4g} KN/m
q_sle={qsle:.4g} KN/m

 '''.format(**locals())

	st_out+='''

considero profilo {pr[name]}

Verifica a deformazione:
d_sle={pr.d:.4g} mm deformazione a SLE
d_amm={dam:.4g} mm deformazione ammissibile (l/{r})
{ver.st[0]}
{ver.streli[0]}

Verifica a flessione:
M_slu={Mslu:.4g} Nmm
M_rd={pr.Mrd:.4g} Nmm
{ver.st[1]}

Verifica instabilita' flessotorsionale
li={linst:.4g} mm distanza ritegni torsionali
M_eq={Minst_:.4g} Nmm momento equivalente
omega_1={pr.om1:.4g} 
Mird={pr.Mins:.4g} Nmm momento resistente a instabilita'
{ver.st[2]}

Verifica a compressione
N_slu={Nslu:.4g} N
N_rd={pr.Nrd:.4g} N
{ver.st[4]}

Verifica a instabilita\' euleriana
li={linst:.4g} mm
lam={pr.lam:.4g} 
om={pr.om:.4g}
Nird={pr.Nins:.4g} N
{ver.st[5]}

Verifica pressoflessione
{ver.st[6]}

Verifica pressoflessione + instabilita\'
{ver.st[7]}

Verifica a taglio:
Tslu={Tslu:.4g} N
Vrd={pr.Vrd} N
{ver.st[3]}

peso elemento={peso} Kg
'''.format(Minst_=Minst+(1.3*2/3*pr.Mp*1.3) , _title=title.upper(),**locals())
	return locals()




