﻿import sqlite3,sys,os,re
from collections import namedtuple
class Profilario(object):
	_version_='1.5'
	def __init__(self):
		path=os.path.dirname(__file__)
		fil_name=os.path.join(path,'profilario.sqlite')
##		path=[i for i in sys.path if re.search('Python\d+$',i)][0]
##		fil_name=os.path.join(path,'Lib','gueng','profilario.sqlite')
		self.db=sqlite3.connect(fil_name)

	def ex(self,*args):
		return self.db.execute(*args)
	
	def ex_(self,*args):
		return self.ex(*args).fetchall()

	def get(self,val):
		'''mantenuto per compatibilità prima versione ma da togliere a breve'''
		return self.__getitem__(val)
	
	def __getitem__(self,val):
		val=val.upper()
		if 'HE_A' in val:
			dat=self.db.execute('select * from HEA where name=?',(val,))
		elif 'HE_B' in val:
			dat=self.db.execute('select * from HEB where name=?',(val,))
		elif 'HE_M' in val:
			dat=self.db.execute('select * from HEM where name=?',(val,))
		elif 'L_' in val:
			val=val.replace('X','x')
			dat=self.db.execute('select * from LU where name=?',(val,))
		elif 'LD' in val:
			val=val.replace('X','x')
			val=val.replace('D','')
			dat=self.db.execute('select * from LD where name=?',(val,))
		elif 'IPE' in val:
			dat=self.db.execute('select * from IPE where name=?',(val,))
		elif 'IPN' in val:
			dat=self.db.execute('select * from IPN where name=?',(val,))
		elif 'UPN' in val:
			dat=self.db.execute('select * from UPN where name=?',(val,))
		else:
			return None
		keys=[i[0] for i in dat.description]
		dat=dat.fetchall()
		#comincio con delle brutte pezze da togliere
		#negli ipn c'è una voce sballata \xc3\x98
		if 'IPN' in val:
			print(keys)
			ind=keys.index('\xc3\x98')
			print(ind)
			print(dat)
			del keys[ind]
			del dat[ind]
		if dat:
			return namedtuple('Profilo',keys)(*dat[0])
		else:
			return None

	def __getattr__(self,attr):
		attr=attr.upper()
		test=False
		test_ipn=False
		#intercetto le HE:
		c=re.search('HE([A-Z]+)(\d+)',attr)
		if c:
			ty=c.groups()[0]
			if ty[0] not in ['A','B','M']:
				raise TypeError('Profilo HE sconosciuto')
			n=c.groups()[1]
			tab='HE'+ty[0]
			name='HE_%s_%s'%(n,ty)
			dat=self.db.execute('select * from %s where name=?'%(tab),(name,))
			test=True
		if not test:
			#intercetto le IPE
			c=re.search('IPE([A-Z]*)(\d+)',attr)
			if c :
				ty=c.groups()[0]
				if ty not in ['','A','O']:
					raise TypeError('Profilo IPE sconosciuto')
				n=c.groups()[1]
				if ty:
					name='IPE_%s_%s'%(ty,n)
				else:
					name='IPE_%s'%n
				dat=self.db.execute('select * from IPE where name=?',(name,))
				test=True

		if not test:
			#intercetto le UPN
			c=re.search('UPN(\d+)(x?\d*)',attr)
			if c :
				name='UPN_%s%s'%c.groups()
				dat=self.db.execute('select * from UPN where name=?',(name,))
				test=True

		if not test:
			#intercetto le IPN
			c=re.search('IPN(\d+)',attr)
			if c :
				test_ipn=True
				name='IPN_%s'%c.groups()
				dat=self.db.execute('select * from IPN where name=?',(name,))
				test=True

		if not test:
			#intercetto L
			c=re.search('L(\d+)X(\d+)(X?\d*)',attr)
			if c :
				if c.groups()[2]:
					tab='LD'
					name='L_%sx%s%s'%c.groups()
				else:
					tab='LU'
					name='L_%sx%sx%s'%(c.groups()[0],c.groups()[0],c.groups()[1])
				dat=self.db.execute('select * from %s where name=?'%(tab),(name,))
				test=True
		if not test:
			raise TypeError('Profilo %s non classificato'%(attr))
		else:
			keys=[i[0] for i in dat.description]
			dat=dat.fetchall()
			#comincio con delle brutte pezze da togliere
			#negli ipn c'è una voce sballata \xc3\x98
			if test_ipn:
				ind=keys.index('\xc3\x98')
				del keys[ind]
				dat=list(dat[0])
				del dat[ind]
				dat=[tuple(dat)]

			if dat:
				return namedtuple('Profilo',keys)(*dat[0])
			else:
				raise TypeError('Profilo %s sconosciuto'%attr)


prof=Profilario()
		
			
		
	
		
