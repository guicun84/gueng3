
from gueng.utils import floatPath as fp
import re,pandas as pa
from scipy import *

def parseQuadri(finame):
	p1=re.compile('({0})\s*$'.format(fp)) #b 
	p2=re.compile(('\s+'.join(['({0})']*8)).format(fp))
	fi=open(finame)
	sez=[]
	cur=None
	rettangolari=empty((0,9),float)
	mult=10**r_[0,0,0,2,4,1,3,4,3]
	for r in fi:
		dat=p1.match(r)
		if dat:
			dat=array(dat.groups(),int)
			sez.append(dat)
			continue
		dat=p2.search(r)
		if dat:
			dat=array(dat.groups(),float)
			if cur is None:
				cur=[sez.pop(0),dat[0]]
			elif cur[1]<dat[0]:cur[1]=dat[0]
			else: cur=[sez.pop(0),dat[0]]
			rettangolari=vstack((rettangolari,r_[cur[0],dat]*mult))
	if len(sez)!=0 and False:
		raise IOError('sez={} , non tutti i casi vagliati correttamente'.format(sez))
	print(sez)
	rettangolari=pa.DataFrame(rettangolari)
	rettangolari.columns=['b s G A Jy iy Wy Jt Wt'.split()]
	print(rettangolari.shape)
	print(rettangolari.columns)
	return rettangolari

quadrati=parseQuadri('quadri__EN10210.txt')
print(quadrati.b.unique())
quadratiFreddo=parseQuadri('quadri_freddo_EN10210.txt')
print(quadratiFreddo.b.unique())


def parseRettangoli(finame):
	p1=re.compile('({0}) x ({0})'.format(fp)) #b e h del profilo
	p2=re.compile(('\s+'.join(['({0})']*11)).format(fp))
	fi=open(finame)
	sez=[]
	cur=None
	rettangolari=empty((0,13),float)
	mult=10**r_[0,0,0,0,2,4,4,1,1,3,3,4,3]
	for r in fi:
		dat=p1.search(r)
		if dat:
			dat=array(dat.groups(),int)
			sez.append(dat)
			continue
		dat=p2.search(r)
		if dat:
			dat=array(dat.groups(),float)
			if cur is None:
				cur=[sez.pop(0),dat[0]]
			elif cur[1]<dat[0]:cur[1]=dat[0]
			else: cur=[sez.pop(0),dat[0]]
			rettangolari=vstack((rettangolari,r_[cur[0],dat]*mult))
	if len(sez)!=0:
		raise IOError('sez={} , non tutti i casi vagliati correttamente'.format(sez))
	print(sez)
	rettangolari=pa.DataFrame(rettangolari)
	rettangolari.columns=['h b s G A Jy Jz iy iz Wy Wz Jt Wt'.split()]
	print(rettangolari.shape)
	print(rettangolari.columns)
	return rettangolari
rettangoli=parseRettangoli('rettangoli_EN10210.txt')
rettangoliFreddo=parseRettangoli('rettangoli_freddo_EN10210.txt')

import sqlite3
db=sqlite3.connect('scatolari_EN10210.sqlite')
quadrati.to_sql('quadri',db,if_exists='replace')
quadratiFreddo.to_sql('quadriFreddo',db,if_exists='replace')
rettangoli.to_sql('rettangolari',db,if_exists='replace')
rettangoliFreddo.to_sql('rettangolariFreddo',db,if_exists='replace')
db.commit()

