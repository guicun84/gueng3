
import pandas as pa

dats=pa.read_csv('quadri.txt',sep='\s+, ?')
print(dats)

def dati(s,b,h=None):
	fl=True
	if h is None:
		h=b
		fl=False
	A=(b-s+h-s)*s*2
	G=A/1e6*7850
	Jy=2*(1/12*s*h**3 + 1/12*(b-2*s)*s**3 + (b-2*s)*s*((h-s)/2)**2)
	Jt=4*((b-s)*(h-s))**2*s/(2*(b+h-s-s))
	Wy=Jy/h*2
	Wt=(b-s)*(h-s)*s*2
	iy=sqrt(Jy/A)
	if fl:
		Jz=2*(1/12*s*b**3 + 1/12*(h-2*s)*s**3 + (h-2*s)*s*((b-s)/2)**2)
		Wz=Jz/b*2
		iz=sqrt(Jz/A)
	return locals()

dat=pa.DataFrame()
for i in dats.iterrows():
	j=i[1]
	for i in list(j.keys())[1:]:
		if not isnan(j[i]):
			dat=dat.append(dati(float(i),j.b),ignore_index=1)
dat=dat['b h s G A Wy Jy iy Jt Wt'.split()]
print(dat)
import sqlite3
db=sqlite3.connect('vicini.sqlite')
dat.to_sql('quadri',db,if_exists='replace')

############################################################
dats=pa.read_csv('rect.txt',sep='\s+, ?')
print(dats)

dat=pa.DataFrame()
for i in dats.iterrows():
	j=i[1]
	for i in list(j.keys())[2:]:
		if not isnan(j[i]):
			dat=dat.append(dati(float(i),j.b,j.h),ignore_index=1)
dat=dat['b h s G A Wy Jy iy Wz Jz iz Jt Wt'.split()]
print(dat)
dat.to_sql('rect',db,if_exists='replace')
