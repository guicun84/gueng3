
import pandas as pa
from io import StringIO 
from scipy import sqrt

dati=open('rettangolari.txt').read()
dati=dati.replace(' ','')
dati=dati.replace(',','.')

dati=dati.split('\n')
spessori=[float(i) for i in dati[0].split('|')[1:]]
dat=[]
for r in dati[2:-1]:
	r=r.split('|')
	h,b=[float(i) for i in r[0].lower().split('x')]
	for n,i in enumerate(spessori):
		if r[n+1]:
			name='{:.0f}x{:.0f}x{:.1f}'.format(h,b,i)
			dat.append({'name':name,'h':h,'b':b,'s':i,'G':float(r[n+1])})
dat=pa.DataFrame(dat)
dat=dat['name h b s G'.split()]
dat['A']=(dat.b*dat.h)-((dat.b-2*dat.s)*(dat.h-2*dat.s))
dat['Jy']=1/12*(dat.h**3*dat.b - (dat.h-2*dat.s)**3*(dat.b -2*dat.s))
dat['Wy']=dat.Jy/dat.h*2
dat['iy']=sqrt(dat.Jy/dat.A)
dat['Jz']=1/12*(dat.b**3*dat.h - (dat.b-2*dat.s)**3*(dat.h -2*dat.s))
dat['Wz']=dat.Jz/b*2
dat['iz']=sqrt(dat.Jz/dat.A)
dat.to_csv('rettangolari.csv')


dat['rho']=dat.G/dat.A*1e6

from pylab import *
clf()
dat.rho.plot()
xticks(dat.index,dat.name.values,rotation=90)
axhline(dat.rho.mean(),color='r')
p=dat.rho.quantile([.05,.95])
axhspan(p[.05],p[.95],color='g',alpha=.5)


