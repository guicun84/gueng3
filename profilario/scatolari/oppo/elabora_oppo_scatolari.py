import lxml.html as html
import re
import pandas as pa
doc=html.parse('oppo_rettangolari.html')
tab=doc.xpath('//table')[0]
rows=tab.getchildren()[2:]
bh=''
dat=pa.DataFrame()
for r in rows:
	dts=r.getchildren()
	num=1
	if len(dts)==10:
		bh=dts[0].text_content()
		b,h=re.search('(\d+)\s+x\s+(\d+)',bh).groups()
		b=float(b)
		h=float(h)
		num=0
	s=float(dts[1-num].text_content().replace('.','').replace(',','.'))
	G=float(dts[2-num].text_content().replace('.','').replace(',','.'))
	A=float(dts[3-num].text_content().replace('.','').replace(',','.'))
	Jy=float(dts[4-num].text_content().replace('.','').replace(',','.'))
	Jz=float(dts[5-num].text_content().replace('.','').replace(',','.'))
	Wy=float(dts[6-num].text_content().replace('.','').replace(',','.'))
	Wz=float(dts[7-num].text_content().replace('.','').replace(',','.'))
	iy=float(dts[8-num].text_content().replace('.','').replace(',','.'))
	iz=float(dts[9-num].text_content().replace('.','').replace(',','.'))
	dat=dat.append([[b,h,s,G,A,Jy,Jz,Wy,Wz,iy,iz]],ignore_index=1)

dat.columns='b h s G A Jy Jz Wy Wz iy iz'.split()
dat.A*=1e2
dat.Jy*=1e4
dat.Jz*=1e4
dat.Wy*=1e3
dat.Wz*=1e3
dat.iy*=10
dat.iz*=10
print(dat)

import sqlite3
db=sqlite3.connect('rettangolari_oppo.sqlite')
dat.to_sql('rettangolari',db)
db.commit()
