fi=open('quadri_EN10210.txt')
for i in range(6): fi.readline()
datas=[[float(j) for j in i.split('\t')] for i in fi.readlines()]
last=None
for n,i in enumerate(datas):
	if len(i)==9:
		last=i[0]
	else:
		d=[last]
		d.extend(i)
		datas[n]=d
assert all([len(i)==9 for i in datas])

keys='b	s	G	A	J	i	W	Jt	C'.split('\t')
from pandas import DataFrame
datas=DataFrame(datas)
datas.columns=keys
datas.A*=1e2
datas.J*=1e4
datas.Jt*=1e4
datas.i*=10
datas.W*=1e3
datas.C*=1e3
print(datas)

datas.to_csv('quadri_EN10210.csv')
