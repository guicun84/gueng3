
import pandas as pa
from scipy import *

#importo (calcolo) tutti i piatti dal prontuario vicini

def calcola(b,s):
	A=b*s
	Jy=1/12*s*b**3
	Jz=1/12*b*s**3
	iy,iz=sqrt(r_[Jy,Jz]/A)
	Wy,Wz=2*r_[Jy,Jz]/(b,s)
	G=A*7850/1e6
	name='{:.0f}x{:.0f}'.format(b,s)
	return locals()

ordine='name b s G A Wy Wz Jy Jz iy iz'.split()
piatti=pa.DataFrame()

def elabora(bs,ss):
	try: len(ss)
	except: ss=[ss]
	try: len(bs)
	except: bs=[bs]
	out=[]
	for b in bs:
		for s in ss:
			out.append(calcola(b,s))
	return out

b=r_[10,12,14,15,16,18,20,22,25,28,30,25,40,45,50,60,70,80,90,100,110,120,130,140,150,160,200.]
s=r_[3,4,5,6,8.]
piatti=piatti.append(elabora(b,s),ignore_index=True)

piatti=piatti.append(elabora(b[b>10],10),ignore_index=True)
piatti=piatti.append(elabora(b[b>12],12),ignore_index=True)
piatti=piatti.append(elabora(b[b>16],15),ignore_index=True)
piatti=piatti.append(elabora(b[b>20],20),ignore_index=True)
piatti=piatti.append(elabora(b[b>25],25),ignore_index=True)
piatti=piatti.append(elabora(b[b>30],30),ignore_index=True)
piatti=piatti.append(elabora(b[b>40],40),ignore_index=True)
piatti=piatti.append(elabora(b[b>60],50),ignore_index=True)
piatti=piatti.append(elabora(b[b>70],60),ignore_index=True)



#larghipiatti
b=r_[arange(160,290,10),300,arange(320,380,10),arange(400,460,10)]
s=r_[10,11,12,15,20,25,30,35,40,50,60,70,80,100]
piatti=piatti.append(elabora(b[b<=350],s),ignore_index=True)
piatti=piatti.append(elabora(b[(b>350) & (b<410)],s[(s>=12)]),ignore_index=True)
piatti=piatti.append(elabora(b[(b>=410)],s[(s>=20)&(s<=50)]),ignore_index=True)
piatti=piatti.append(elabora(160,45))

piatti=piatti.drop_duplicates()
try:
	piatti=piatti.sort_values(['b','s'])
except:
	piatti=piatti.sort(['b','s'])
piatti.index=arange(len(piatti))
piatti=piatti[ordine]




