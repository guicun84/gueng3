import pandas as pa
import os
from numpy import isnan,sqrt
basedir=os.path.dirname(__file__)

dat=pa.read_csv(os.path.join(basedir,'piatti_larghi.txt'),skiprows=6,sep='; *',index_col=0)
dat.columns=[int(i) for i in dat.columns]
dat.astype(float)


def elabora(dat):
	pl=[]
	k=list(dat.keys())[1:]

	for i in dat.iterrows():
		d=i[1]
		for j in k:
			if not isnan(d[j]):
				pl.append({'b':i[0],'s':j, 'G':d[j]})
	pl=pa.DataFrame(pl)['b s G'.split()]
	pl['A']=pl.b*pl.s
	pl['Jy']=1./12*pl.s*pl.b**3
	pl['Jz']=1./12*pl.s**3*pl.b

	pl['Wz']=pl.Jz/pl.s*2
	pl['Wy']=pl.Jy/pl.b*2
	pl['iz']=sqrt(pl.Jz/pl.A)
	pl['iy']=sqrt(pl.Jy/pl.A)
	pl['name']=pl.apply(lambda x:'{:.0f}x{:.0f}'.format(x.b,x.s),1)
	return pl['name b s G A Jy Jz Wy Wz iy iz'.split()]

pl=elabora(dat)

dat=pa.read_csv(os.path.join(basedir,'piatti.txt'),skiprows=7,header=0,index_col=0,sep=';\s+')
dat.columns=[float(i) for i in dat.columns]
dat.astype(float)

ps=elabora(dat)

piatti=ps.copy()
piatti=piatti.append(pl,ignore_index=True)

piatti.to_csv(os.path.join(basedir,'tutti_piatti.csv'))

