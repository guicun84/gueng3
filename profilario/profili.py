﻿import sqlite3,sys,os,re
from collections import namedtuple,OrderedDict
from pandas import read_sql,DataFrame,Series
import re
from .bulloni import Bullone
class Profilario(object):
	_version_='1.5'
	def __init__(self):
		path=os.path.dirname(__file__)
		fil_name=os.path.join(path,'profilario.sqlite')
##		path=[i for i in sys.path if re.search('Python\d+$',i)][0]
##		fil_name=os.path.join(path,'Lib','gueng','profilario.sqlite')
		self.db=sqlite3.connect(fil_name)
		self.lu=read_sql('select * from LU',self.db)
		self.ld=read_sql('select * from LD',self.db)
		self.hea=read_sql('select * from HEA',self.db)
		self.heb=read_sql('select * from HEB',self.db)
		self.hem=read_sql('select * from HEM',self.db)
		self.ipe=read_sql('select * from IPE',self.db)
		self.upn=read_sql('select * from UPN',self.db)
		self.ipn=read_sql('select * from IPN',self.db)
		mask=self.hea.apply(lambda x:bool(re.match('^HE_\d+_A$',x['name'])),1)
		self.hea_=self.hea[mask]
		mask=self.heb.apply(lambda x:bool(re.match('^HE_\d+_B$',x['name'])),1)
		self.heb_=self.heb[mask]
		mask=self.hem.apply(lambda x:bool(re.match('^HE_\d+_M$',x['name'])),1)
		self.hem_=self.hem[mask]
		mask=self.ipe.apply(lambda x:bool(re.match('^IPE_\d+$',x['name'])),1)
		self.ipe_=self.ipe[mask]
		self.lu_=self.lu.query('uni==1')
		self.ld_=self.ld.query('uni==1')
		self.l=DataFrame()
		for i in self.lu,self.ld:
			self.l=self.l.append(i,ignore_index=1)
		self.l_=DataFrame()
		for i in self.lu_,self.ld_:
			self.l_=self.l_.append(i,ignore_index=1)
		self.travi=DataFrame()
		for i in self.hea,self.heb,self.hem,self.ipe:
			self.travi=self.travi.append(i,ignore_index=True)
		self.travi_=DataFrame()
		for i in self.hea_,self.heb_,self.hem_,self.ipe_:
			self.travi_=self.travi_.append(i,ignore_index=True)

	def ex(self,*args):
		return self.db.execute(*args)
	
	def ex_(self,*args):
		return self.ex(*args).fetchall()

	def get(self,val):
		'''mantenuto per compatibilità prima versione ma da togliere a breve'''
		return self.__getitem__(val)
	
	def __getitem__(self,val):
		val=val.upper()
		if re.match('HE_\d+_A',val):
			dat=self.db.execute('select * from HEA where name=?',(val,))
		elif re.match('HE_\d+_B',val):
			dat=self.db.execute('select * from HEB where name=?',(val,))
		elif re.match('HE_\d+_M',val):
			dat=self.db.execute('select * from HEM where name=?',(val,))
		elif 'L' in val:
			#controllo se sono uguali o disuguali:
			s=re.search('L_?(\d+)[xX](\d+)[xX]?(\d*)',val)
			val=re.sub('L(\d)','L_\1',val)
			val=val.replace('X','x')
			if s.group(1)==s.group(2): 
				dat=self.db.execute('select * from LU where name=?',(val,))
			else:
				dat=self.db.execute('select * from LD where name=?',(val,))
		elif 'IPE' in val:
			dat=self.db.execute('select * from IPE where name=?',(val,))
		elif 'IPN' in val:
			return self.__getattr__(val.replace('_',''))
		elif 'UPN' in val:
			dat=self.db.execute('select * from UPN where name=?',(val,))
		else:
			return None
		keys=[i[0] for i in dat.description]
		dat=dat.fetchall()
		#comincio con delle brutte pezze da togliere
		#negli ipn c'è una voce sballata \xc3\x98
		if 'IPN' in val:
			print(keys)
			ind=keys.index('\xc3\x98')
			print(ind)
			print(dat)
			del keys[ind]
			del dat[ind]
		if dat:
			return namedtuple('Profilo',keys)(*dat[0])
		else:
			return None

	def __getattr__(self,attr):
		attr=attr.upper()
		test=False
		test_ipn=False
		#intercetto le HE:
		c=re.search('HE([A-Z]+)(\d+)',attr)
		if c:
			ty=c.groups()[0]
			if ty[0] not in ['A','B','M']:
				raise TypeError('Profilo HE sconosciuto')
			n=c.groups()[1]
			tab='HE'+ty[0]
			name='HE_%s_%s'%(n,ty)
			dat=self.db.execute('select * from %s where name=?'%(tab),(name,))
			test=True
		if not test:
			#intercetto le IPE
			c=re.search('IPE([A-Z]*)(\d+)',attr)
			if c :
				ty=c.groups()[0]
				if ty not in ['','A','O']:
					raise TypeError('Profilo IPE sconosciuto')
				n=c.groups()[1]
				if ty:
					name='IPE_%s_%s'%(ty,n)
				else:
					name='IPE_%s'%n
				dat=self.db.execute('select * from IPE where name=?',(name,))
				test=True

		if not test:
			#intercetto le UPN
			c=re.search('UPN(\d+)(x?\d*)',attr)
			if c :
				name='UPN_%s%s'%c.groups()
				dat=self.db.execute('select * from UPN where name=?',(name,))
				test=True

		if not test:
			#intercetto le IPN
			c=re.search('IPN(\d+)',attr)
			if c :
				test_ipn=True
				name='IPN_%s'%c.groups()
				dat=self.db.execute('select * from IPN where name=?',(name,))
				test=True

		if not test:
			#intercetto L
			c=re.search('L(\d+)X(\d+)X?(\d*)',attr)
			if c :
				if c.groups()[2]:
					tab='LD'
					name='L_%sx%sx%s'%c.groups()
				else:
					tab='LU'
					name='L_%sx%sx%s'%(c.groups()[0],c.groups()[0],c.groups()[1])
				dat=self.db.execute('select * from %s where name=?'%(tab),(name,))
				test=True
		if not test:
			raise TypeError('Profilo %s non classificato'%(attr))
		else:
			keys=[i[0] for i in dat.description]
			dat=dat.fetchall()
			#comincio con delle brutte pezze da togliere
			#negli ipn c'è una voce sballata \xc3\x98
			if test_ipn:
#				ind=keys.index('\xc3\x98')
#				del keys[ind]
				dat=list(dat[0])
#				del dat[ind]
				dat=[tuple(dat)]

			if dat:
				#return namedtuple('Profilo',keys)(*dat[0])
				return Series(OrderedDict(list(zip(keys,dat[0]))))

			else:
				raise TypeError('Profilo %s sconosciuto'%attr)

	def emin(self,pr,bul):
		'''calcola eccentricità minima per i profili a L in modo da garantire che la rondella del bullone non interferisca con il raggio internod ella piega del profilo.
		pr= profilo a L
		bul=diametro bullone'''
		if type(pr)==str:
			pr=self.__getattr__(pr)
		if type(bul)!=Bullone:	
			bul=Bullone()(bul)
		return pr.t+pr.r1+bul.dro/2+1 #+1 per avere un mm di margine


prof=Profilario()
		
			
		
	
		
