from scipy import r_,where
from collections import namedtuple
'''Bulloni M	S diametro interrno bu	e diametro esterno bullone	k spessore testa	m spessore bullone	d diametro rondella	spessore rondella	Ares'''


class Bullone(object):
	dati=r_[[[10,17,19.63,6.4,8,20,2,58],
	    [12,22,23.91,8,10,24,3,84],
		[14,24,26.17,9,11,28,4,115],
		[16,27,29.56,10,13,30,4,157],
		[18,30,32.95,12,15,34,4,192],
		[20,32,35.03,13,16,36,4,245],
		[22,36,39.55,14,18,40,4,303],
		[24,41,45.2,15,19,44,4,353],
		[27,46,50.85,17,22,50,5,459]]]
	def __init__(self):
		'''m: sigla bullone
di: diametro minimo testa
de: diametro massimo testa
ste: spessore testa
sbu: spessore bullone
dro: diametro rondella
sro: spessore rondella
Ares: Area resistente'''
	def __call__(self,m):
		ind=where(Bullone.dati[:,0]==m)[0]
		m,di,de,ste,sbu,dro,sro,Ares=Bullone.dati[ind,:].flatten()
		out=namedtuple('Bullone','m,di,de,ste,sbu,dro,sro,Ares'.split(','))(m,di,de,ste,sbu,dro,sro,Ares)
		return out	
	def cerca(self,A):
		'''ricerca un bullone data l'area minima da ottenere
		A=Area in mm'''
		ind=where(Bullone.dati[:,-1]>=A)[0][0]
		m,di,de,ste,sbu,dro,sro,Ares=Bullone.dati[ind,:].flatten()
		out=namedtuple('Bullone','m,di,de,ste,sbu,dro,sro,Ares'.split(','))(m,di,de,ste,sbu,dro,sro,Ares)
		return out	
	def __getattr__(self,val):
		val=int(val[1:])
		return self.__call__(val)


bul=Bullone()

