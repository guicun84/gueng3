import os
from pandas import read_csv

class Bullone(object):
	fi=os.path.join(os.path.dirname(__file__),'buls.csv')
	dati=read_csv(fi,sep='\s*,\s*',index_col=0,engine='python')
	def __init__(self):
		'''m: sigla bullone
di: diametro minimo testa
de: diametro massimo testa
ste: spessore testa
sbu: spessore bullone
dro: diametro rondella
sro: spessore rondella
Ares: Area resistente'''
	def __call__(self,m):
		return Bullone.dati.query('m==@m').iloc[0]
	def cerca(self,A):
		'''ricerca un bullone data l'area minima da ottenere
		A=Area in mm'''
		return Bullone.dati.query('Ares>=@A').iloc[0]
	def __getattr__(self,val):
		val=int(val[1:])
		return self.__call__(val)


bul=Bullone()

