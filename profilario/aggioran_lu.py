from gueng.profilario import profilario
p=profilario()

st='''L_45x45x3	23	22	M12
L_45x45x4	23	22	M12
L_45x45x4.5	23	22	M12
L_50x50x4	25	25	M14
L_50x50x5	28	24	M12
L_50x50x6	28	25	M12
l_50x50x8	28	27	M12
L_60x60x10	35	32	M14
L_60x60x5	32	29	M16
L_60x60x6	32	30	M16
L_60x60x8	32	32	M16
L_65x65x7	32	32	M16
L_70x70x10	38	37	M18
L_70x70x6	35	35	M20
L_70x70x7	38	34	M18
L_70x70x8	38	35	M18
L_75x75x6	40	36	M20
L_75x75x8	40	37	M20
L_80x80x10	42	42	M22
L_80x80x6	42	38	M22
L_80x80x8	42	40	M22
L_90x90x10	48	45	M24
L_90x90x12	48	47	M24
L_90x90x6	44	44	M22
L_100x100x15	58	51	M27'''
st=st.split('\n')
for r in st:
	name,fmax,fmin,phi=r.split('\t')
	fmax=float(fmax)
	fmin=float(fmin)
	p.ex('update lu set ezmin=:fmin,ezmax=:fmax,eymin=:fmin,eymax=:fmax,phiy=:phi where name=:name',locals())
p.ex('update lu set phiz=phiy')
