#-*-coding:latin-*-

from scipy import sqrt,r_,pi
import re
from gueng.utils import toDict
from pandas import Series

def farfalla(pr,i):
	#controllo che siano profili a L (se no come si mettono a farfalla)
	if not re.match('L_',pr['name']):
		raise TypeError('impossibile disporre a farfalla il profilo {}'.format(pr.name))
	A=pr.A*2
	G=pr.G*2
	Jy=2*(pr.Jy + pr.A*(pr.ys+i/2)**2) #lungo assi piattebande
	Jm=2*(pr.Jv + pr.A*((pr.ys+i/2)*sqrt(2))**2) #minimo su asse non intersecante 2 L 
	JM=2*(pr.Ju) #massimo su asse intersecante 2 L
	if Jm>JM: Jm,JM=JM,Jm
	im=sqrt(Jm/A)
	iM=sqrt(JM/A)
	iy=sqrt(Jy/A)
	name='2x{}/{} a farfalla'.format(pr['name'],i)
	b=pr.b
	t=pr.t
	keys='name A G Jy Jm JM iy im iM b t'
	out=Series(toDict(keys))[keys.split()]

def laccoppiati(pr,i):
	'''pr=profilo
	i=spessore imbottitura intermedia
	'''
	if not re.match('L_',pr['name']):
		raise TypeError('il profilo {} non � un profilo a L'.format(pr['name']))
	b=2*pr.b+i
	h=pr.b
	t=pr.t
	A=pr.A*2
	G=pr.G*2
	Jy=2*pr.Jy
	Jz=2*(pr.Jz + pr.A*(i/2+pr.ys)**2)
	Wy=2*pr.Wy
	Wz=Jz/pr.h*2
	iy=pr.iy
	iz=(Jz/A)**.5
	name='2x{}/{} affiancati'.format(pr['name'],i)
	keys='name A G b h t Wy Wz Jy Jz iy iz'
	return Series(toDict(keys))[keys.split()]

def iaccoppiati(pr,i):
	'''considera i porofili accoppiati lungo asse y[orizontale]
	i=distanza tra baricentri dei profili'''
	try:name=pr['name']
	except: name=pr.name
	A=pr.A*2
	G=pr.G*2
	Jz=2*(pr.Jz + pr.A*(i/2)**2)
	Jy=2*pr.Jy
	iy,iz=sqrt(r_[Jy,Jz]/A)
	Wy=2*pr.Wy
	Wz=Jz/abs((pr.b+i))/2
	name='2x{}/{}'.format(name,i)
	b=pr.b*2
	h=pr.h
	IT=2*pr.IT
	Iwx=2*pr.Iwx
	out='A G Jy Jz Wy Wz iy iz b h t name IT Iwx'
	for i in 'tf tw'.split():
		if i in list(pr.keys()):
			locals()[i]=pr[i]
			out+=' {}'.format(i)
	return Series(toDict(out))
	
	

def accoppiati(pr,i):
	'''considera i porofili accoppiati lungo asse y[orizontale]
	i=distanza tra baricentri dei profili'''
	print('da controllare troppo generica ')
	try:name=pr['name']
	except: name=pr.name
	if re.match('L_',name):
		ys=pr.ys
		t=pr.t
	elif re.match('C\d+x\d+x\d+',name):
		ys=pr.Xs
		t=pr.tw
	elif re.match('UPN',name):
		ys=pr.Ys
		t=pr.tw
	else: 
		if 'Ys' in list(pr.keys()): #cos� riesce a valutare correttamente anche i profili ipe
			ys=pr.Ys
			t=pr.tw
		else: 
			ys=0
			t=pr.tw
	A=pr.A*2
	G=pr.G*2
	Jz=2*(pr.Jz + pr.A*(ys+i/2)**2)
	Jy=2*pr.Jy
	iy,iz=sqrt(r_[Jy,Jz]/A)
	Wy=2*pr.Wy
	Wz=Jz/abs((pr.b-pr.ys+i))/2
	name='2x{}/{}'.format(name,i)
	b=pr.b*2
	h=pr.h
	IT=2*pr.IT
	Iwx=2*pr.Iwx
	out='A G Jy Jz Wy Wz iy iz b h t name IT Iwx'
	for i in 'tf tw'.split():
		if i in list(pr.keys()):
			locals()[i]=pr[i]
			out+=' {}'.format(i)
	return Series(toDict(out))

def upnaccoppiati(pr,d,schiena=True,rot=False):
	'''pr=profili da accoppiare
	d=distanza tra esterno dell'anima dei profili
	shiena=True se schiena-schiena, false se creano scatolare
	rot=False per mantenere orientamento nello spazio, ture per ruotare di 90�
	'''
	if schiena:
		name='2x{}/{}s'.format(pr['name'],d)
		dw=d+2*pr.b
		d+=2*pr.Ys
	else: 
		name='2x{}/{}f'.format(pr['name'],d)
		dw=d
		d-=2*pr.Ys
	A=pr.A*2
	Jz=2*(pr.Jz + pr.A*(d/2)**2)
	Wz=Jz/dw*2
	Jy=2*pr.Jy
	Wy=2*pr.Wy
	G=pr.G*2
	IT=2*pr.IT
	Iwx=2*pr.Iwx
	b=dw
	h=pr.h
	if rot: b,h=h,b #ruoto base e altezza
	iy=pr.iy
	iz=sqrt(Jz/A)
	t=pr.tw
	tf=pr.tf
	tw=pr.tw
	if rot: tf,tw=tw,tf #ruoto spessori
	Ys=pr.h/2
	#valori plastici
	Wply=2*pr.Wply
	Wplz=2*(pr.A*d)
	#aree di taglio:
	Avy=2*(pr.h-2*pr.tf)*pr.tw/1.5
	Avz=4*(pr.b-pr.tw)*pr.tf/1.5
	out=Series(locals())
	for i in 'pr d dw schiena rot'.split():
		del out[i]
	if not rot:	
		return out
	else:
		o2=Series()
		for i in list(out.keys()):
			if 'z' in i:
				ii=i.replace('z','y')
				o2[ii]=out[i]
			elif 'y' in i:
				ii=i.replace('y','z')
				o2[ii]=out[i]
			else:
				o2[i]=out[i]
		return o2

def Lcalastrellati(pr,b,h=None,interno=False,Jfull=False):
	'''
	valuta caratteristiche di 4 profili a L calastrellati (o traliciati)
	pr=profilo L
	b=base lungo x h=altezza lungo y,se none =base
	interno=False|True : se true b,h riferite interno
	Jfull=False|True se True considera anche momento statico del profilo se no solo trasporto
	'''
	if h==None:h=b
	A=pr.A*4
	bb=b/2-pr.ys
	hh=h/2-pr.ys
	if interno:
		bb+=pr.t
		hh+=pr.t
		b+=2*pr.t
		h+=2*pr.t
	Jy=4*pr.A*hh**2
	Jz=4*pr.A*bb**2
	if Jfull:
		Jy+=4*pr.Jy
		Jz+=4*pr.Jz
	Wy=Jy/hh*2
	Wz=Jz/bb*2
	iy=(Jy/A)**.5
	iz=(Jz/A)**.5
	Jt=1/3*pr.t**3*pr.b*8
	name=f'4{pr["name"]}cal{b}x{h}'
	out=Series(dict(name=name,G=4*pr.G,b=b,h=h,A=A,Wy=Wy,Wz=Wz,Jy=Jy,Jz=Jz,iy=iy,iz=iz,Avy=A/2,Avz=A/2,Jt=Jt,))
	return out



