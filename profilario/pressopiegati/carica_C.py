
from scipy import *
import pandas as pa
import re

#carico la tabella di dimensioni,spessori e pesi
fi='C.txt'
dati=pa.read_csv('C.txt',skiprows=6,sep=' +\t',engine='python')

def calcola_profilo(b,h,s,G):
	name='C{:d}x{:d}x{:.2f}'.format(b,h,s)
	A=2*b*s + (h-2*s)*s
	G=A*7850/1e6 #quelli sulla tabella sono sparati, li correggo tutti per sicurezza
	Sy=2*b*s*b/2 + (h-2*s)*s*s/2
	Xs=Sy/A
	Jy=2*(1/12 *b*s**3 + b*s *((h-s)/2)**2) + 1/12*(s*(h-2*s)**3)
	Jz=2*(1/12*s*b**3 + b*s*(b/2-Xs)**2) + 1/12*(h-2*s)*s**3 + (h-2*s)*s*(Xs-s/2)**2
	iz=sqrt(Jz/A)
	iy=sqrt(Jy/A)
	Wz=Jz/max(Xs,b-Xs)
	Wy=Jy/h*2
	tf=s
	tw=s
	del s
	return locals()


path=re.compile('(?P<b>\d+)X(?P<h>\d+)X(?P<b2>\d+)')
dts=[]
for n,i in dati.iterrows():
		dt=path.match(i.profilo)
		if not dt:
			print(i.profilo)
			continue
		dt=dt.groupdict()
		for j in dt:dt[j]=int(dt[j])
		for m,h in enumerate(i):
			if m==0 or isnan(h):continue
			else:dts.append(calcola_profilo(dt['b'],dt['h'],float(list(i.keys())[m]),h))

dts=pa.DataFrame(dts)['name A G b h tf tw Xs Jy Wy iy Jz Wz iz'.split()]
print(dts)
dts.to_csv('C_aperti.csv')
