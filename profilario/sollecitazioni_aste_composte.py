#-*-coding:latin-*-

from scipy import sqrt,r_,pi
import re
from gueng.utils import toDict
from pandas import Series

def farfalla(pr,i):
	#controllo che siano profili a L (se no come si mettono a farfalla)
	if re.match('L_',pr.name):
		A=pr.A*2
		G=pr.G*2
		Jy=2*(pr.Jy + pr.A*(pr.ys+i/2)**2) #lungo assi piattebande
		Jm=2*(pr.Jv + pr.A*((pr.ys+i/2)*sqrt(2))**2) #minimo su asse non intersecante 2 L 
		JM=2*(pr.Ju) #massimo su asse intersecante 2 L
		if Jm>JM: Jm,JM=JM,Jm
		im=sqrt(Jm/A)
		iM=sqrt(JM/A)
		iy=sqrt(Jy/A)
		name='2x{}/{} a farfalla'.format(pr.name,i)
		b=pr.b
		t=pr.t
		return toDict('A G Jy Jm JM iy im iM b t name')
	else:
		raise TypeError('impossibile disporre a farfalla il profilo {}'.format(pr.name))


def accoppiati(pr,i):
	'''considera i porofili accoppiati lungo asse y[orizontale]
	i=distanza tra baricentri dei profili'''
	print('da controllare troppo generica ')
	try:name=pr['name']
	except: name=pr.name
	if re.match('L_',name):
		ys=pr.ys
		t=pr.t
	elif re.match('C\d+x\d+x\d+',name):
		ys=pr.Xs
		t=pr.tw
	elif re.match('UPN',name):
		ys=pr.Ys
		t=pr.tw
	else: 
		if 'Ys' in list(pr.keys()): #coś riesce a valutare correttamente anche i profili ipe
			ys=pr.Ys
			t=pr.tw
		else: 
			ys=0
			t=pr.tw
	A=pr.A*2
	G=pr.G*2
	Jz=2*(pr.Jz + pr.A*(ys+i/2)**2)
	Jy=2*pr.Jy
	iy,iz=sqrt(r_[Jy,Jz]/A)
	Wy=2*pr.Wy
	Wz=Jz/abs((pr.b-pr.ys+i))/2
	name='2x{}/{}'.format(name,i)
	b=pr.b*2
	h=pr.h
	IT=2*pr.IT
	Iwx=2*pr.Iwx
	out='A G Jy Jz Wy Wz iy iz b h t name IT Iwx'
	for i in 'tf tw'.split():
		if i in list(pr.keys()):
			locals()[i]=pr[i]
			out+=' {}'.format(i)
	return Series(toDict(out))

def upnAccoppiati(pr,d,schiena=True,rot=False):
	'''pr=profili da accoppiare
	d=distanza tra esterno dell'anima dei profili
	shiena=True se schiena-schiena, false altrimenti
	rot=False per mantenere orientamento nello spazio, ture per ruotare di 90°
	'''
	if schiena:
		name='2x{}/{}s'.format(pr['name'],d)
		dw=d+2*pr.b
		d+=2*pr.Ys
	else: 
		name='2x{}/{}f'.format(pr['name'],d)
		dw=d
		d-=2*pr.Ys
	A=pr.A*2
	Jz=2*(pr.Jz + pr.A*(d/2)**2)
	Wz=Jz/dw*2
	Jy=2*pr.Jy
	Wy=2*pr.Wy
	G=pr.G*2
	IT=2*pr.IT
	Iwx=2*pr.Iwx
	b=dw
	h=pr.h
	if rot: b,h=h,b #ruoto base e altezza
	iy=pr.iy
	iz=sqrt(Jz/A)
def Naccoppiati(N,l,pr,d=0,inte=-3,E=210e3):
	''' calcolo sforzo normale massimo per L accoppiati
	N=sforzo normale astta
	l=lunghezza asta
	pr=profilo a L
	d=spessore piastra accoppiamento, se negativo considera piastra dello stesso spessore del profilo
	inte=interasse, negativo indica numero imbottiture
	E=modulo elastico 
	tutto N e mm'''
	if inte<0:#controllo interasse
		inte=l/(-inte-1) #interasse calastrelli
	Vsd=N*.025 #forza di taglio interna da EC3 
	Msd=Vsd/pi*inte #momento interno
	h0=d+2*pr.ys #distanza assi profili
	Jeff=.5*pr.A*(h0)**2 #J efficace sezione
	Ncr=pi**2*Jeff*E/l**2 #N critico del profilo
	Nfsd=N/2+Msd*h0*pr.A/Jeff #sforzo sulla singola asta
	stout='''inte ={inte:.4g} interasse calastrelli
Vsd ={Vsd:.4g} forza di taglio interna da EC3 
Msd ={Msd:.4g} momento interno
h0 ={h0:.4g} distanza assi profili
Jeff ={Jeff:.4g} J efficace sezione
Ncr ={Ncr:.4g} N critico del profilo
Nfsd ={Nfsd:.4g} sforzo sulla singola asta'''.format(**locals())
	return Nfsd,locals()


def Lclasse3(pr,fyk=275):
	'''controlla se il profilo è inclasse 3'''
	ep=sqrt(235/fyk)
	v1=pr.h/pr.t
	t1=v1<=15*ep
	v2=(pr.b+pr.h)/2/pr.t
	t2=v2<=11.5*ep
	return t1 & t2

from gueng.acciaio.CNR import omega

from gueng.profilario import prof

def predimAstaAccoppiata_old(N,l,d,inte=-3,linst=None,fyk=275,cumax=1,ls=None,gammas=1.15):
	'''predimensiona asta ad L accoppiati
	N azione assiale massima
	l=lunghezza asta 
	d=spessore piastra accoppiamento
	inte=interasse imbottiture
	linst=lunghezza per instabilità complessiva briglia None se =l
	fyk=resistenza acciaio'
	cumax=massimo coefficiente d'uso
	ls=profili su cui cercare'''
	if linst is None:
		linst=l
	if inte<0:
		inte=l/(-1-inte)
	if ls is None: ls=prof.l_.copy()
	ls=ls[ls.apply(lambda x:Lclasse3(x,fyk),1)]#prendo solo profili classe 3
	ls['dts']=ls.apply(lambda x: Naccoppiati(N,l,x,d,inte),1) #dati sforzo accoppiato
	ls['Nfsd']=ls.apply(lambda x: x.dts[0],1) #sforzo asssiale asta singola
#	ls['Jtot']=ls.apply(lambda x: x.dts[1]['Jtot'],1) #J toticace asta composta
#	ls['itot']=sqrt(ls.Jtot/ls.A/2) #raggio minimo inerzia asta composta
	ls['itot']=ls.iy
	ls['lamtot']=linst/ls.itot #snellezza asta composta
	ls['omtot']=ls.apply(lambda x: omega(x.lamtot,fyk,'c',verbose=0),1) #omega asta composta
	ls['Nrdtot']=2*ls.A/ls.omtot*275/gammas #resistenza ad instabilità asta composta

	ls['lam1']=inte/ls.iv #snellezza asta singola
	ls['om1']=ls.apply(lambda x: omega(x.lam1,fyk,'c',verbose=0),1) #omega asta singola
	ls['Nrd1']=ls.A/ls.om1*275/gammas #resistenza ad instabilità asta singola

	ls['cutot']=2*ls.Nfsd/ls.Nrdtot
	ls['cu1']=ls.Nfsd/ls.Nrd1
	prs=ls.query('cutot<=@cumax and cu1<=@cumax and lamtot<=210 and lam1<=210').sort('G')
	if len(prs):
		pr=prs.iloc[0]
	else:
		pr=None
	return pr,locals()



def mu(lam): #coefficiete di efficacia per aste composte
	if lam<=75: return 1
	elif lam<=150: return 2-lam/75
	else: return 0

def predimAstaAccoppiata(N,l,d,bc,inte=-3,linst=None,M=0,fyk=275,cumax=1,ls=None,E=210e3,gammas=1.15):
	'''predimensiona asta ad L accoppiati
	N azione assiale massima
	l=lunghezza asta 
	d=spessore piastra accoppiamento
	bc=dimensioni calastrello lungo l'asse elemento (sezione bc*d)
	inte=interasse imbottiture
	linst=lunghezza per instabilità complessiva briglia None se =l
	M=momento flettente
	fyk=resistenza acciaio'
	cumax=massimo coefficiente d'uso
	ls=profili su cui cercare'''
	if linst is None:
		linst=l
	if inte<0:
		inte=l/(-1-inte)
	if ls is None: ls=prof.l_.copy()
	ls['h0']=d+2*ls.ys
	Jc=1/12*d*bc**3
	e0=linst/500
	ls=ls[ls.apply(lambda x:Lclasse3(x,fyk),1)]#prendo solo profili classe 3
	ls['lammin']=linst/ls.iy #usa iy controllato
	ls['Ncr']=pi**2*E*ls.A*2/ls.lammin**2
	ls['lam_']=sqrt(ls.A*2*fyk/ls.Ncr)
	ls['phi']=.5*(1+.49*(ls.lam_-.2)+ls.lam_**2)
	ls['chi']=1/(ls.phi + sqrt(ls.phi**2-ls.lam_**2))
	ls['Nbrd']=ls.chi*2*ls.A*fyk/gammas
	ls['lam']=linst*sqrt(2*ls.A/(.5*ls.h0**2*ls.A+2*Jc))
	ls['mu']=ls.apply(lambda x:mu(x.lam),1)
	ls['Jeff']=.5*ls.h0**2*ls.A + 2*ls.mu*Jc
	ls['Sv']=2*pi**2*E*Jc/inte**2
	ls['Ncr_']=pi**2*E*ls.Jeff/linst**2
	ls['Med']=(N*e0+M)/(1-N/ls.Ncr_-N/ls.Sv)
	ls['DN']=ls.Med*ls.h0*ls.A/(2*ls.Jeff)
	ls['Nced']=.5*N+ls.DN
	ls['Ntot']=2*ls.Nced
	ls['Ved']=pi*ls.Med/linst
	ls['Mced']=ls.Ved*inte/4
	ls['Vced']=ls.Ved/2
	ls['cutot']=ls.Ntot/ls.Nbrd
	Vcrd=d*bc/sqrt(3)*fyk/gammas
	ls['cuVcal']=2*ls.Vced/Vcrd
	ls['Medcal']=ls.Ved*inte/2
	Mrdcal=1/6*d*bc**2*fyk/gammas
	ls['cuMcal']=ls.Medcal/Mrdcal



	prs=ls.query('cutot<=@cumax and cuVcal<=@cumax and cuMcal<=@cumax').sort('G')
	if len(prs):
		pr=prs.iloc[0]
	else:
		pr=None
	return pr,locals()
