#!/usr/bin/env python
#-*-coding:utf-8-*-

from  pylab import *
from scipy import *
from copy import deepcopy as copy
from scipy.linalg import norm,solve
from scipy.optimize import leastsq
ion()


class linea:
	Mrot=c_[[0.,-1],[1.,0]].T
	def __init__(self,i,j):
		self.i=i.astype(float)
		self.j=j.astype(float)
		self.orienta()

	def orienta(self):
		self.e1=self.j-self.i
		self.l=norm(self.e1)
		self.e1/=norm(self.l)
		self.e2=dot(self.Mrot,self.e1)

	def inverti(self):
		p=self.i
		self.i=self.j
		self.j=p
		self.orienta()
	
	def asse(self,x):
		return self.i+self.e1*x


	def interseca(self,l):
		'''l1.i+l1.e1*x - (l2.i+l2.e2*y)={0,0}
		[[e1][e2]] * [x,y] +(l1i-l2.i)=[0,0]'''
		A=c_[self.e1,l.e1]
		b=self.i-l.i
		try:
			v=solve(A,-b)
		except LinAlgError:
			return None
		p=self.i+self.e1*v[0]
		return p
	@property
	def Sx(self):
		#calcola momento statico dell'area definita dal segmento rispetto ad asse x
		v=self.j-self.i
		s1=v[0]*self.i[1]*self.i[1]*.5
		s2=v[0]*v[1]*.5* (self.i[1]+1./3*v[1])
		return -s1-s2

	@property
	def Ix(self):
		v=self.j-self.i
		s1=1./12*v[0]*self.i[1]**3 + v[0]*self.i[1]*(self.i[1]*.5)**2
		s2=1./36*v[0]*v[1]**3+ .5*v[0]*v[1]*(self.i[1]+1./3*v[1])**2
		return -s1-s2

	@property
	def A(self):
		v=self.j-self.i
		a1=v[0]*self.i[1]
		a2=.5*v[0]*v[1]
		return -a1-a2				
	
	def plot(self,form=None):
		x=[self.i[0],self.j[0]]
		y=[self.i[1],self.j[1]]
		if form:
			plot(x,y,form)
		else:
			plot(x,y)
		axis('equal')
	def plot_verso(self,form='r'):
		p1=self.asse(self.l/2)
		p2=p1+self.l/10*(-self.e1+self.e2)
		x=[i[0] for i in [p1,p2]]
		y=[i[1] for i in [p1,p2]]
		plot(x,y,form)
		axis('equal')
			

class area:
	_version_='1.8'
	def __init__(self,lati,centra=False,weight=1):
		#centra serve per ricentrare le coordinate sul baricentro
		if all([isinstance(i,linea) for i in lati]):
			self.lati=lati
		else: #genero la superficie attraverso i punti
			self.lati=[]
			pi=None
			for p in lati:
				if pi is None:
					pi=p
					continue
				else:
					self.lati.append(linea(pi.copy(),p.copy()))
					pi=p
			self.lati.append(linea(lati[-1].copy(),lati[0].copy()))
		self.centra_flg=centra
		if self.centra_flg:
			self.centra()
		self.weight=weight

	def centra(self):
		c=self.sposta_punti(self.G)
		vars(self).update(vars(c))

		

	def sposta_punti(self,G):
		sez=copy(self)
		for l in sez.lati:
			l.i-=G
			l.j-=G
			l.orienta()
		return sez	

	def ruota(self,ang):
		mc=copy(self)
		m=array([[cos(ang),sin(ang)],[-sin(ang),cos(ang)]]).T
		for l in mc.lati:
			l.i=dot(m,l.i)
			l.j=dot(m,l.j)
			l.orienta()
		return mc
	
	def b(self,y=0):
		if not self.limiti[2]<y<self.limiti[3]:
			return 0
		'''restituisce lo spessre totale delle varie sezioni ad una data quota'''
		lim=self.limiti
		l=linea(r_[lim[0]-1,y],r_[lim[1]+1,y])
		intersec=[]
		for i in self.lati:
			p=l.interseca(i)
			if p is not None:
				v=dot((p-i.i),i.e1)
#				print v,i.l
				if 0<=v<=i.l:
#					plot(p[0],p[1],'xr')
					intersec.append(p[0])					
		#trovati i punti di intersezione:
		intersec=[i for i in set(intersec)]
		intersec.sort()
#		print intersec
		i=None
		s=0
		for p in intersec:
			if i is None:
				i=p
			else:
				s+=(p-i)
				i=None
		return s
	
	bv=vectorize(b)

	def Ibs(self,y='min'):
		'''calcola il valore di (I s)/Sx* per la valutazione delle tensioni di taglio'''
		if y =='min':
			ymin=leastsq(self.Ibs,0.)[0][0]
			return self.Ibs(ymin)
		else:
			return -self.Ix*self.b(y)/self.Sx_(y)
	
	@property
	def Sx(self):
		s=0
		for l in self.lati:
			s+=l.Sx
		return s

	def Sx_(self,y,parte=1):
		l=self.limiti
		if l[2]<y<l[3]:
			l=linea(r_[0.,y],r_[1.,y])
			ts=area(self.taglia(l,parte))
			return ts.Sx
		else:
			return 0.
	@property   
	def Ix(self): #rispetto all'asse x
		I=0.
		for i in self.lati:
			I+=i.Ix
		return I

	@property
	def Jx(self): #baricentrico
		return self.Ix-self.A*self.G[1]**2

	@property
	def Sy(self):
		sez=area.ruota(self,pi/2)
		return sez.Sx
	
	@property
	def Iy(self):
		sez=area.ruota(self,pi/2)
		return sez.Ix

	@property
	def Jy(self):
		sez=area.ruota(self,pi/2)
		return sez.Jx

	@property
	def Wx(self):
		#elastico
		l=self.limiti
		y=r_[l[2],l[3]]
		return min(abs(self.Jx/y))
	
	@property
	def Wy(self):
		sez=area.ruota(self,pi/2)
		return sez.Wx

	def Wxp(self,y=0.):
		lim=self.limiti
		if lim[2]<y<lim[3]:
			l=linea(r_[0.,y],r_[1.,y])
			s1=area(self.taglia(l,-1))
			s2=area(self.taglia(l,1))
			return s1.Sx-s2.Sx
		else:
			return 0.
	
	def Wyp(self,x=0.):
		sez=area.ruota(self,pi/2)
		return sez.Wxp(x)

	def Apl(self,y=0):
		lim=self.limiti
		if y<=lim[2]: return self.A
		elif y>=lim[3]: return -self.A
		l=linea(r_[0.,y],r_[1.,y])
		s1=area(self.taglia(l,-1))
		s2=area(self.taglia(l,1))
		return s1.A-s2.A

	def y0p(self,Apl=0):
		def minimizzami(yp,Apl):
			return self.Apl(yp)-Apl
		yop=leastsq(minimizzami,0,args=(Apl,))[0][0]
		return yop
		
		
	@property
	def A(self):
		a=0
		for l in self.lati:
			a+=l.A
		return a

	@property
	def G(self):
		return r_[self.Sy,self.Sx]/self.A

	@property
	def limiti(self):
		x=[]
		y=[]
		for l in self.lati:
		    x.extend([l.i[0],l.j[0]])
		    y.extend([l.i[1],l.j[1]])
		xm=min(x)
		xM=max(x)
		ym=min(y)
		yM=max(y)
		return r_[xm,xM,ym,yM]

	@property
	def ptp(self):
		l=self.limiti
		return l[1]-l[0],l[3]-l[2]

	def taglia(self,asse,parte=1):
	#parte= verso positivo o negativo dell'asse dato su cui considerare la sezione tagliata
		lati_out=[]
		last_int=None
#		count=0
		for lato in self.lati:
#			count+=1
#		       print '\n',count
			p=asse.interseca(lato)
			if p is not None: #se ho punto di intersezione
##				plot(p[0],p[1],'xr')
				#controllo se taglio il lato
				v=p-lato.i
				l=dot(v,lato.e1)
#				print 'v,l,p',v,l,p
#				print 'lato.l',lato.l
				if 0<=l<lato.l: #taglio il lato:
#				       print 'taglio lato'
					c=dot(v,asse.e2)
#				       print 'test',c
					if c*parte>0:
#						print 'inizio-taglio'
						p1=lato.i.copy()
						p2=p
					else:
#						print 'taglio-fine'
						p1=p
						p2=lato.j.copy()
					if last_int is not None and  all(p1==p):
						lati_out.append(linea(last_int,p1))
						last_int=None
					else:
						last_int=p
					lati_out.append(linea(p1,p2))
					continue
			#controllo se il lato è dalla parte corretta				    
			v=lato.i-asse.i
			c=dot(v,asse.e2)*parte
			if c<0:
#			       print 'lato compreso nel contorno'
				p1=lato.i.copy()
				p2=lato.j.copy()
				last_int=None
			else:
#				print 'lato escluso dal contorno'
				continue
			lati_out.append(linea(p1,p2))
		#controllo chiusura lati:
		ini=lati_out[0].i
		fin=lati_out[-1].j
		if not all(ini==fin):
			l=linea(fin,ini)
			lati_out.append(l)
		return lati_out
   
	def plot(self,form='-b'):
		for i in self.lati:
			i.plot(form)
	def plot_versi(self,form='-r'):
		for i in self.lati:
			i.plot_verso(form)


class Cerchio(area):
	def __init__(self,de,di=None,G=r_[0,0]):
		vars(self).update(locals())
	def centra(self):
		pass
	def sposta_putni(self,G):
		sez=copy(self)
		sez.G-=G
		return sez
	def ruota(self,ang):
		sez=copy(self)
		m=array([[cos(ang),sin(ang)],[-sin(ang),cos(ang)]]).T
		sez.G=dot(m,sez.G)
		return sez
	def b(self,y=0):
		l=self.limiti
		if l[2]<y<l[3]:
			b=cos(arcsin((y-self.G[1])/(self.de/2)))*self.de
			if self.di:
				s=(self.de-self.di)/2
				if l[2]+s <y <l[3]-s:
					b2=cos(arcsin((y-self.G[1])/(self.di/2)))*self.di
					return b-b2
				else:
					return b
			else:
				return b
		else: 
			return 0


	def Ibs(self,y='min'):
		pass
	@property
	def Sx(self):
		pass
	def Sx_(self,y,parte=1):
		pass
	@property
	def Ix(self): #rispetto asse x
		return self.Jx + self.A*self.G[1]**2
	
	@property
	def Jx(self): #baricentrico
		if self.di:
			return pi*(self.de**4-self.di**4)/64
		else:
			return pi*self.de**4/64
	
	@property
	def Iy(self):
		return self.Jx + self.A*self.G[0]**2
	@property
	def Jy(self):
		return self.Jx
	@property
	def Wx(self):
		return self.Jx/(self.de/2)
	@property
	def Wy(self):
		return self.Wx
	def Wxp(self,y=0):
		pass
	def Wyp(self,y=0):
		pass
	def Apl(self,y=0):
		pass
	def y0p(self,Apl=0):
		pass
	@property
	def A(self):
		if self.di:
			return (self.de**2-self.di**2)*pi/4
		else:
			return self.de**2*pi/4
	@property
	def limiti(self):
		return r_[self.G[0]-self.de/2,self.G[0]+self.de/2,self.G[1]-self.de/2,self.G[1]+self.de/2]
	@property
	def ptp(self):
		return r_[self.de,self.de]
	def taglia(self,asse,parte=1):
		pass
	def plot(self,form='-b'):
		cr=Circle(self.G,self.de/2,edgecolor='b',fill=False)
		gca().add_patch(cr)
		if self.di:
			cr2=Circle(self.G,self.di/2,edgecolor='b',fill=False)
			gca().add_patch(cr2)
		draw()
		axis('equal')

	def plot_versi(self,form='-r'):
		pass

class regione:
	def __init__(self,a=[],centra=False):
		self.aree=a
		self.centra_flg=centra
		if self.centra_flg:
			self.centra()

	@property
	def A(self):
		return sum([i.A*i.weight for i in self.aree])
	@property
	def Sx(self):
		return sum([i.Sx*i.weight for i in self.aree])
	@property
	def Sy(self):
		s=self.ruota(pi/2)
		return s.Sx
	@property
	def G(self):
		yg=self.Sx/self.A
		xg=self.Sy/self.A
		return r_[xg,yg]
	@property
	def Ix(self):
		return sum([i.Ix*i.weight for i in self.aree])
	
	@property
	def Jx(self):
#		return sum([i.Jx + i.A*(i.G[1]-self.G[1])**2 for i in self.aree])
		return self.Ix-self.A*self.G[1]**2

	@property
	def Iy(self):
		s=self.ruota(pi/2)
		return s.Ix

	@property
	def Jy(self):
		s=self.ruota(pi/2)
		return s.Jx

	@property
	def Wx(self):
		return self.Jx/max(abs(self.limiti[2:]-self.G[1]))
	@property
	def Wy(self):
		s=self.ruota(pi/2)
		return s.Wx
	@property
	def limiti(self):
		lim=None
		for i in self.aree:
			tl=i.limiti
			if lim is None:
				lim=tl
			else:
				if tl[0]<lim[0]:
					lim[0]=tl[0]
				if tl[1]>lim[1]:
					lim[1]=tl[1]
				if tl[2]<lim[2]:
					lim[2]=tl[2]
				if tl[3]>lim[3]:
					lim[3]=tl[3]
		return lim
	
	@property
	def ptp(self):
		l=self.limiti
		return r_[l[1]-l[0],l[3]-l[2]]

	def sposta_punti(self,d):
		aree=[]
		for i in self.aree:
			aree.append(i.sposta_punti(d))
		return regione(aree)       
	def ruota(self,ang):
		aree=[]
		for i in self.aree:
			aree.append(i.ruota(ang))
		return regione(aree)
	def centra(self):
		self.aree=self.sposta_punti(self.G).aree

	def plot(self,form='-b'):
		for i in self.aree:
			i.plot(form)

	def Ibs(self,y='min'):
		if y=='min':
			print('da implementare')
			return 0

		else:
			b=r_[[i.b(y) for i in self.aree]]

	
	
			
if __name__=='__main__':
	h=78
	b=46
	tf=4.2
	ta=3.3
	a1=area([r_[0,0.],r_[b,0],r_[b,tf],r_[0,tf]])
	a2=area([r_[b/2-ta/2,tf],r_[b/2+ta/2,tf],r_[b/2+ta/2,h-tf],r_[b/2-ta/2,h-tf]])
	a3=area([r_[0.,h-tf],r_[b,h-tf],r_[b,h], r_[0,h]])
	re=regione([a1,a2,a3])
		
