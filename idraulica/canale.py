#-*-coding:latin-*-

from pylab import *
from scipy import *
from gueng.geometrie import area,linea

'''file per il calcolo della portata delle gronde per il mercato ortofrutticolo di genova'''

class Canale:
	def __init__(self,sezione,pendenza,k):
		'''implenenta la formula con coefficiente Glauker-strikler
		sezione=class Sezione
		pendenza= pendenza motrice (valore assoluto)
		k=coefficiente Glauker-Strikler'''
		vars(self).update(locals())

	def Q(self,h):
		'''calcolo portata: 
		h=altezza'''
#		return sqrt(self.pendenza)*(self.k*self.sezione.R(h)**(1/6))*self.sezione.A(h)*self.sezione.R(h)**.5
		return sqrt(self.pendenza)*(self.k*self.sezione.R(h)**(2/3))*self.sezione.A(h)

class Sezione:
	def __init__(self,a):
		'''a: class gueng.area: � la geometria massima della sezine del canale con x=0 nel punto piu basso'''
		self.a=a

	def sez(self,h):	
		'''ottiene area piena della sezione
		h=quota acqua'''
		l=linea(r_[0,h],r_[1,h])
		return area(self.a.taglia(l))
		
	def A(self,h=None,a=None):
		'''calcola area sezione piena
		h=altezza 
		a=eventuale area da usare?'''
		if a is None: a=self.sez(h)
		return a.A
		

	def P(self,h=None,a=None):
		'''calcola perimetro bagnato 
		h=altezza acqua
		a=area da usare'''
		if a is None: a=self.sez(h)
		p=[norm(i.j-i.i) for i in a.lati[:-1]]
		return sum(p)
	
	def R(self,h):
		'''calcolo raggio idrauilco
		h=altezza acqua'''
		a=self.sez(h)
		return self.A(a=a)/self.P(a=a)

