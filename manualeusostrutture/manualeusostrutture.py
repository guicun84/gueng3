# coding: utf-8
import sqlite3,pandas as pa,re
from lxml import html
from lxml.html import Element as E,builder as EE
from lxml.builder import E as EE
import os , re,pypandoc

def c2p(c):
	'converte da cm a pixels'
	d=92
	return f'{int(c/2.54*d)}'


def importanza(grav): #funzione registrata su db per ordinare l'importanza
	imp=['grave','lieve']
	grav=grav.lower()
	if grav in imp:return imp.index(grav)
	return len(imp)+1

def frq(frq): #funzione registrata su db per ottenere un numero dalla frequenza
	if re.match('^\s*\d+',frq):
		return float(re.search('\d+',frq).group())
	return 1000

class Manuale:
	db=sqlite3.connect(os.path.join(os.path.dirname(__file__),'manutenzione.sqlite'))
	db.create_function('importanza',1,importanza)
	db.create_function('frq',1,frq)

	@classmethod
	def tipologie(cl):
		return pa.read_sql('select tipologia,id from elementi order by tipologia',cl.db)

	def __init__(self,elementi):
		'elementi per cui redigere il manuale (vedi tipologie)'
		els=[]
		for e in elementi:
			if type(e)==int:
				els.append(self.db.execute('select tipologia from elementi where id=?',(e,)).fetchone()[0])
			else:
				ee=re.sub(' +','%',e)
				ee=f'%{ee}%'
				ee=self.db.execute('select tipologia from elementi where tipologia like ?',(ee,)).fetchall()
				if len(ee)==0:
					print(f'"{e}" non trovato')
				elif len(ee)==1:
					els.append(ee[0][0])
				else:
					ee=[f'-\n {i[0]}' for i in ee]
					print(f'Problema per {e}: potrebbe essere {"".join(ee)}')

		self.elementi=els

	def manualeuso(self):
		'regide la parte "manuale di uso"'
		elementi=self.elementi
		b=E('div')
		b.append(EE.H1("Manuale d'uso"))
		b.append(EE.H2('Descrizione opera'))
		b.append(EE.H2('Elementi manutenibili'))
		q=f'select tipologia,descrizione,uso from elementi where tipologia =?'
		for e in elementi:
			dat=self.db.execute(q,(e,)).fetchone()
			b.append(EE.H3(e))
			b.append(EE.H4('Descrizione'))
			if dat:
				b.append(EE.P(dat[1]))
			b.append(EE.H4(u"Modalità d'uso corretto"))
			if dat:
				b.append(EE.P(dat[2]))
			b.append(EE.H4("Collocazione e rappresentazione dell'elemento")),
			b.append( EE.P('Consultare le tavole del progetto'))
		return b

	def manualemanutenzione(self):
		'regide la parte "manuale di manutenzione"'
		elem=self.elementi
		b=E('div')
		b.append(EE.H1("Manuale di manutenzione"))
		for e in elem:
			b.append(EE.H2(e))
			
			b.append(EE.H3('Risorse per interventi'))
			tb=EE.TABLE()
			b.append(tb)
			tb.append(EE.THEAD(
				EE.TR( EE.TH("Intervento",width=c2p(7)),EE.TH('Risorse',width=c2p(11)))
			))
			bb=EE.TBODY()
			tb.append(bb)
			query='''select 
				i.intervento,
				group_concat(r.risorsa,', ')
				from 
				elementi e
				inner join elementointerventi ei  on ei.elemento=e.id
				inner join interventi i on i.id=ei.intervento
				inner join interventorisorse ir on ir.intervento=i.id
				inner join risorse r on r.id=ir.risorsa
				where e.tipologia=? group by i.id order by i.intervento '''
			for i in self.db.execute(query,(e,)):
				tb.append(EE.TR(EE.TD(i[0]),EE.TD(i[1])))
			
			b.append(EE.H3('Risorse per controlli'))
			tb=EE.TABLE()
			b.append(tb)
			tb.append(EE.THEAD(
				EE.TR( EE.TH("Controllo"),EE.TH('Risorse'))
			))
			bb=EE.TBODY()
			tb.append(bb)
			query='''select 
				c.controllo,
				group_concat(r.risorsa,', ')
				from 
				elementi e
				inner join elementocontrolli ec  on ec.elemento=e.id
				inner join controlli c on c.id=ec.controllo
				inner join controllorisorse cr on cr.controllo=c.id
				inner join risorse r on r.id=cr.risorsa
				where e.tipologia=? group by c.id order by c.controllo '''
			for i in self.db.execute(query,(e,)):
				tb.append(EE.TR(EE.TD(i[0]),EE.TD(i[1])))
			
			
			
			b.append(EE.H3("Livelli minimi delle prestazioni"))
			for i in self.db.execute('''
				select 
				p.prestazione,p.descrizione,p.minimo
				from 
				elementi e
				inner join elementoprestazioni ep  on ep.elemento=e.id
				inner join prestazioni p on p.id=ep.prestazione
				where e.tipologia=?''',(e,)):
				b.append(EE.DIV(EE.H4(i[0]),
						  EE.H5('Descrizione'),
						  EE.P(i[1]),
						  EE.H5('Livello minimo'),
						  EE.P(i[2]))
				)
				
			b.append(EE.H3("Anomalie"))
			tb=EE.TABLE()
			b.append(tb)
			tb.append(EE.THEAD(
				EE.TR( EE.TH("Anomalia"),EE.TH('Descrizione'),EE.TH("Pericolosità"))
			))
			bb=EE.TBODY()
			tb.append(bb)
			for i in self.db.execute('''
				select 
				a.anomalia,
				a.descrizione,
				a.valutazione
				from 
				elementi e
				inner join elementoanomalie ea  on ea.elemento=e.id
				inner join (
					select *,importanza(valutazione) as imp from anomalie
				) a  on a.id=ea.anomalia
				where e.tipologia=?
				order by imp,a.anomalia''',(e,)):
				bb.append(EE.TR(
					EE.TD(i[0]),
					EE.TD(i[1]),
					EE.TD(i[2]),
				))
				
			b.append(EE.H3("Interventi"))
			b.append(EE.H4("Eseguibili dall'utente"))
			dat=self.db.execute('''
				select 
				i.intervento
				from 
				elementi e
				inner join elementointerventi ei  on ei.elemento=e.id
				inner join interventi i on i.id=ei.intervento
				where e.tipologia=? and esecutore="Utente" order by i.intervento''',(e,))
			n=0
			l=EE.UL()
			for i in dat:
				l.append(EE.LI(i[0]))
				n+=1
			if n==0:
				l=EE.P('Nessuno')
			b.append(l)
			b.append(EE.H4("Eseguibili da personale specializzato"))
			dat=self.db.execute('''
				select 
				i.intervento
				from 
				elementi e
				inner join elementointerventi ei  on ei.elemento=e.id
				inner join interventi i on i.id=ei.intervento
				where e.tipologia=? and esecutore="Specializzato" order by i.intervento''',(e,))
			n=0
			l=EE.UL()
			for i in dat:
				l.append(EE.LI(i[0]))
				n+=1
			if n==0:
				l=EE.P('Nessuno')
			b.append(l)
		return b 

	def programmamanutenzione(self):
		'regide la parte "programma di manutenzione"'
		b=EE.DIV(
			EE.H1("Programma di manutenzione"))
		b.append(self.sottoprogrammacontrolli())
		b.append(self.sottoprogrammainterventi())
		b.append(self.sottoprogrammaprestazioni())
		return b

		
		
	def sottoprogrammacontrolli(self):
		'regide la parte "sottoprogramma dei controlli"'
		elem=self.elementi
		b=EE.DIV()
		b.append(EE.H2("Sottoprogramma dei Controlli"))
		for e in elem:
			b.append(EE.H3(e))
			tb=EE.TABLE(style="text-align:left;")
			b.append(tb)
			tb.append(EE.THEAD(
				EE.TR( EE.TH("Controllo"),EE.TH('Tipologia'),EE.TH("Frequenza"),EE.TH("Descrizione"))
			))
			tbb=EE.TBODY()
			tb.append(tbb)
			query='''
				select 
				c.controllo,
				c.tipologia,
				c.frequenza,
				c.descrizione,
				frq(c.frequenza) as fr
				from 
				elementi e
				inner join elementocontrolli ec  on ec.elemento=e.id
				inner join controlli c on c.id=ec.controllo
				where e.tipologia=? order by fr,c.controllo'''
			for i in self.db.execute(query,(e,)):
				tbb.append(EE.TR(
					EE.TD(i[0]),
					EE.TD(i[1]),
					EE.TD(i[2]),
					EE.TD('%s'%i[3]),
				))
		return b

	def sottoprogrammainterventi(self):
		'regide la parte "sottoprogramma degli interventi"'
		elem=self.elementi
		b=EE.DIV()
		b.append(EE.H2("Sottoprogramma degli interventi"))
		for e in elem:
			b.append(EE.H3(e))
			tb=EE.TABLE(style="text-align:left;")
			b.append(tb)
			tb.append(EE.THEAD(
				EE.TR( EE.TH("intervento"),EE.TH("Frequenza"),EE.TH("Descrizione"))
			))
			tbb=EE.TBODY()
			tb.append(tbb)
			query='''
				select 
				i.intervento,
				i.frequenza,
				i.descrizione,
				frq(i.frequenza) as fr
				from 
				elementi e
				inner join elementointerventi ei  on ei.elemento=e.id
				inner join interventi i on i.id=ei.intervento
				where e.tipologia=? order by fr,i.intervento'''
			for i in self.db.execute(query,(e,)):
				tbb.append(EE.TR(
					EE.TD(i[0]),
					EE.TD(i[1]),
					EE.TD(i[2]),
				))
		return b

	def sottoprogrammaprestazioni(self):
		'regide la parte "sottoprogramma delle prestazioni"'
		elem=self.elementi
		b=EE.DIV()
		b.append(EE.H2("Sottoprogramma delle prestazioni"))
		#recupero le prestazioni
		dat=self.db.execute(f'''
		select prestazione,descrizione,id from prestazioni where id in (
			select distinct p.id from prestazioni p
			inner join elementoprestazioni ep on ep.prestazione=p.id
			inner join elementi e on e.id=ep.elemento
			where e.tipologia in ({("?,"*len(elem))[:-1]}) order by p.id)
	   ''',elem)
		for p in dat:
			b.append(EE.H3(p[0]))
			b.append(EE.H4('Descrizione'))
			b.append(EE.P(p[1]))
			args=[p[-1]]
			args.extend(elem)
			dat2=self.db.execute(f'''select controllo,frequenza,frq(frequenza) as fr from controlli where id in (
			select distinct controllo from (
				select ec.controllo as controllo from 
				elementi  e
				inner join elementocontrolli ec on ec.elemento=e.id
				inner join (
					select pc.controllo as controllo from 
					prestazioni p
					inner join prestazionecontrolli pc on pc.prestazione=p.id
					where p.id=?
				) p on p.controllo=ec.controllo
				where e.tipologia in ({("?,"*len(elem))[:-1]})))
				order by fr
			''',args)
			tb=EE.TABLE(
				EE.THEAD(
					EE.TR(
						EE.TH('Controllo'),EE.TH('Frequenza'))
			))
			tbb=EE.TBODY()
			tb.append(tbb)
			n=0
			for i in dat2:
				tbb.append(EE.TR(EE.TD(i[0]),EE.TD(i[1])))
				n+=1
			if n>0:
				b.append(tb)
			else:
				b.append(EE.P('Nessun controllo specifico necessario'))
		return b

	def manualelxml(self):
		doc=EE.HTML(EE.HEAD(),EE.BODY())
		docb=doc[-1]
		docb.append(self.manualeuso())
		docb.append(self.manualemanutenzione())
		docb.append(self.programmamanutenzione())
		return doc

	def manualetoodt(self,finame):
		pypandoc.convert_text(html.tostring(self.manualelxml()),format='html',to='odt',outputfile=finame)

	def manualehtml(self):
		return html.tostring(self.manualelxml(),pretty_print=True,encoding='unicode')

	def manualetohtml(self,finame):
		with open(finame ,'w') as fi:
			fi.write(self.manualehtml())

