from scipy import *
from copy import deepcopy as copy

class successione_asse:
    def __init__(self):
        self.successione=[]
    def add(self,seg):
        self.successione.append(seg)

    def __str__(self):
        st=[]
        for i in self.successione:
            st.append('%s'%i)
        return ''.join(st)
    def __get__(self,n):
        return self.successione[n]
    def __getitem__(self,n):
        return self.successione.__getitem__(n)

    def __iter__(self):
        return self.successione.__iter__()            
        
 
class segmento_asse:
    def __init__(self,l,t,m,t_red=None):
        '''l=lunghezza
t=segno della tensione
m=moltiplicatore [1 o 0] se zona efficace o no'''
        vars(self).update(locals())
        

    def __str__(self):
        if self.m!=0:
            if self.t>0:
                s='+'
            else:
                s='-'
        else:
            s='*'
        return '|'+s*3+ '%s'%self.l +s*3+'|'
                  
class componente:
    def __init__(self,bp,t,fyk,sig1,sig2=None,sig_com_ed=None,psi=None,rew=False,gamma_2=1.25):
        if sig2 is None:
            sig2=sig1
        if sig_com_ed is None:
            sig_com_ed=max(r_[sig1,sig2])
        if psi is None:
                if sig1==0:
                    sig1=1e-4*abs(sig2)
                psi=sig2/sig1
        vars(self).update(locals())
        self.fyd=fyk/gamma_2
        self.t_red=self.t
        self.asse=successione_asse()

    def calcola(self):
        self.eps=sqrt(235e6/self.fyk)
        self.lam_p=self.bp/self.t/28.4/self.eps/sqrt(self.k_sig);
        if self.sig_com_ed<self.fyd:
            self.lam_p_red=self.lam_p*sqrt(self.sig_com_ed/self.fyd)
        else:
            self.lam_p_red=self.lam_p
            
        if self.lam_p_red<=.673:
            self.ro= 1.
        else:
            ro=(1.-.02/self.lam_p_red)/self.lam_p_red
            self.ro=min([1.,ro])
        self.bef=self.ro*self.bc
        

    @property
    def k_sig(self):
        '''dipende dal tipo di componente se a 2 o 1 appoggio'''
        pass

    def __str__(self):
        st=[]
        for i,j in list(vars(self).items()):
            if i=='self':continue
            st.append('%s: %s'%(i,j))
        return '\n'.join(st)

    @property
    def bc(self):
        if self.psi>=0:
            return self.bp
        else:
            sig=r_[self.sig1,self.sig2]
            sig.sort()
            return self.bp*sig[1]/abs(sig).sum()
    

#*****************************************************************************    

class lato_1_app(componente):
    def __init__(self,bp,t,fyk,sig1,sig2=None,sig_com_ed=None,psi=None,rew=False,gamma_2=1.25):
        componente.__init__(self,bp,t,fyk,sig1,sig2,sig_com_ed,psi,rew,gamma_2)
        self.calcola()
        self.definisci_successione()
        
    @property   
    def k_sig(self):
        if self.psi==1:
            return .43
        elif 1>self.psi>0:
            return .578/(self.psi+.34)
        elif self.psi==0:
            return 1.70
        elif 0>self.psi>-1:
            return 1.7+5*self.psi*1.71*self.psi**2
        else:
            return 23.8

    def definisci_successione(self):
        if self.psi>0:
            self.asse.add(segmento_asse(self.bef,1,1,self.t))
            if self.bef!=self.bp:
                self.asse.add(segmento_asse(self.bp-self.bef,1,0,self.t))
        else:
            if self.sig1>self.sig2:
                self.asse.add(segmento_asse(self.beff,1,1,self.t))
                if self.bc!=self.beff:
                    self.asse.add(segmento_asse(self.bc-self.bef,1,0,self.t))
                self.asse.add(segmento_asse(self.bp-self.bc,-1,1,self.t))
            else:
                self.asse.add(segmento_asse(self.bp-self.bc,-1,1,self.t))
                self.asse.add(segmento_asse(self.bef,1,1,self.t))
                if self.bef!=self.bc:
                    self.asse.add(segmento_asse(self.bc-self.bef,1,0,self.t))
                

#*****************************************************************************

class lato_2_app(componente):
    def __init__(self,bp,t,fyk,sig1,sig2=None,sig_com_ed=None,psi=None,rew=False,gamma_2=1.25):
        componente.__init__(self,bp,t,fyk,sig1,sig2,sig_com_ed,psi,rew,gamma_2)
        self.calcola()
        self.definisci_successione()
        
    @property
    def k_sig(self):
        if self.psi==1:
            return 4.
        elif 1>self.psi>0:
            return 8.2/(self.psi+1.05)
        elif self.psi==0:
            return 7.81
        elif 0>self.psi>-1:
            return 7.81-6.29*self.psi+9.78*self.psi**2
        elif self.psi==-1:
            return 23.8
        elif -1>self.psi>-3:
            return 5.98*(1-self.psi)**2
    @property
    def be1(self):
        if self.psi==1:
            return self.bp*self.ro*.5
        elif 0>self.psi>-1:
            return 2*self.bef/(5.-self.pis)
        else:
            return .4*self.bef

    @property
    def be2(self):
        return self.bef-self.be1

    def definisci_successione(self):
        if self.psi==1:
            self.asse.add(segmento_asse(self.be1,1,1,self.t))
            if self.be1+self.be2!=self.bp:
                self.asse.add(segmento_asse(self.bp-self.be1-self.be2,1,0,self.t))
            self.asse.add(segmento_asse(self.be2,1,1,self.t))
        elif self.psi>0:
            self.asse.add(segmento_asse(self.be1,1,1,self.t))
            if self.be1+self.be2!=self.bp:
                self.asse.add(segmento_asse(self.bp-self.be1-self.be2,1,0,self.t))              
            self.asse.add(segmento_asse(self.be2,1,1,self.t))
        else:
            self.asse.add(segmento_asse(self.be1,1,1,self.t))
            if self.be1+self.be2!=self.bc:
                self.asse.add(segmento_asse(self.bc-self.be1-self.be2,1,0,self.t))
            self.asse.add(segmento_asse(self.be2,1,1,self.t))
            self.asse.add(segmento_asse(self.bp-self.bc,-1,1,self.t))
        if self.sig1<self.sig2:
            self.asse=self.asse[-1::-1]
            

#-----------------------------------------------------------------------------
    
class irrigidimento:
    def __init__(self,h,b,c,d=None,sb=1,sc=1,sd=1,E=210e9):
        vars(self).update(locals())
        #ricalcolo gli elementi considerando sig_com_ed=fyd
        self.calcola_c()
        self.calcola_b()
        self.calcola_d()
        self.calcola_irrigidimento()
    
 

    def calcola_c(self):
        if isinstance(self.c,lato_1_app):
            c_=lato_1_app(self.c.bp, self.c.t, self.c.fyk, self.c.sig1, self.c.sig2, self.c.fyd)
        elif isinstance(self.c,lato_2_app):
            c_=lato_2_app(self.c.bp, self.c.t, self.c.fyk, self.c.sig1, self.c.sig2, self.c.fyd)

        if self.sc==1:
            asse=c_.asse
        else:
            asse=c_.asse[-1::-1]
        t=c_.t
        self.Ac=0
        self.Sxc=0
        l=0
        for i in asse:
            A=i.m*(i.l*t)
            self.Sxc+=A*(l+i.l/2)
            self.Ac+=A
            l+=i.l
        self.yGc=self.Sxc/self.Ac
        self.Ic=0
        l=0
        for i in asse:
            self.Ic+= 1./12*t*i.l**2 + i.m*(i.l*t)*(l+i.l/2-self.yGc)**2
            l+=i.l

    def calcola_b(self):
        if isinstance(self.b,lato_1_app):
            b_=lato_1_app(self.b.bp, self.b.t, self.b.fyk, self.b.sig1, self.b.sig2, self.b.fyd)
        elif isinstance(self.b,lato_2_app):
            b_=lato_2_app(self.b.bp, self.b.t, self.b.fyk, self.b.sig1, self.b.sig2, self.b.fyd)
            
        if self.sb==1:
            asse=b_.asse
        else:
            asse=b_.asse[-1::-1]
        t=b_.t
        self.Ab=0
        self.Sxb=0
        self.yGb=self.c.bp+t/2
        l=0
        #momenti statici e di inerzia li calcolo rispetto a bordo inferiore irrigidimento
        for i in asse:
            if i.m==0: break
            A=i.l*t
            self.Ab+=A
            self.Sxb+=A*(self.yGb)#rispetto bordo inferiore irrigidimento
            l+=i.l
        self.Ib=1./12*l*t**3 + self.Ab*(self.yGb)**2
        

    def calcola_d(self):
        #ricalcolo con sig_com_ed=fyd
        if isinstance(self.d,lato_1_app):
            d_=lato_1_app(self.d.bp, self.d.t, self.d.fyk, self.d.sig1, self.d.sig2, self.d.fyd)
        elif isinstance(self.d,lato_2_app):
            d_=lato_2_app(self.d.bp, self.d.t, self.d.fyk, self.d.sig1, self.d.sig2, self.d.fyd)
        elif self.d is None:
            d_=None
            
        if d_ is None:
            self.Id = self.Sxd = self.Ad =self.yGd= 0
        else:
            if self.sd==1:
                asse=d_.asse
            else:
                asse=d_.asse[-1::-1]
            t=d_.t
            self.Ad=0
            self.Sxd=0
            self.yGd=-t/2
            l=0
            #momenti statici e di inerzia li calcolo rispetto a bordo inferiore irrigidimento
            for i in asse:
                A=i.m*(i.l*t)
                self.Ad+=A
                self.Sxb+=A*(self.yGd)#rispetto bordo inferiore irrigidimento
                l+=i.m*i.l
            self.Id=1./12*l*t**3 + self.Ad*(self.yGd)**2

    def calcola_irrigidimento(self):
        self.As=self.Ab+self.Ac+self.Ad
        self.Sx=self.Sxb+self.Sxc+self.Sxd #momento statico totale sezione
        self.yG=self.Sx/self.As #baricentro totale sezione
        self.Is=self.Ib + self.Ab*(self.yGb-self.yG)**2 + \
                 self.Ic + self.Ac*(self.yGc-self.yG)**2+\
                 self.Id + self.Ad*(self.yGd-self.yG)**2

        Is_lim=r_[.31,4.86]*(1.5*self.h/self.b.bp)*(self.b.fyk/self.E)**2*(self.b.bp/self.b.t)**2 *self.As**2
        test=self.Is>Is_lim
        if test[1]:
            self.chi=1
        elif test[0]:
            self.chi=0.5
        else:
            self.chi=0
            print('''eseguire l'analisi con il metodo rigoroso itereativo''')
        self.As_red=self.chi*self.As*(self.b.fyd/self.b.sig_com_ed)
        if self.As_red> self.As:
            self.As_red=self.As

        #aggiorno gli spessori ridotti dei vari tratti    
        for i in [self.c,self.d]:
             if i is not None:
                 t_red=i.t*(self.As_red/self.As)
                 for j in i.asse:
                     if j.t_red is not None:
                         if t_red < j.t_red:
                             j.t_red=t_red
                     else:
                         j.t_red=t_red
        
        if self.sb==1:
            asse=self.b.asse
        else:
            asse=self.b.asse[-1::-1]
        t_red=self.b.t*(self.As_red/self.As)
        for i in asse:
            if not i.m: break
            if i.t_red is None:
                i.t_red=t_red
            else:
                if i.t_red> t_red:
                    i.t_red=t_red
            break
                    
        if self.sb==-1:
            asse=asse[-1::-1]
        self.b.asse.successione=copy(asse)
            
            
        
    def __str__(self):
        st=[]
        for i,j in list(vars(self).items()):
            if i=='self':continue
            st.append('%s: %s'%(i,j))
        return '\n'.join(st)

    
    
