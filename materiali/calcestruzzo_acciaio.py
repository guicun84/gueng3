#-*-coding:utf-8-*-

'nota: il modulo CA ha i suoi materiali NON importa questi...'

from pylab import *
from scipy import *
from scipy.optimize import root,fminbound,leastsq
import gueng.geometrie
from pdb import set_trace
from copy import deepcopy
import pandas as pa
import os

class Calcestruzzo(object):
	gamma_c=1.6 
	ec=.002
	ecu=.0035
	classi=pa.read_csv(os.path.join(os.path.dirname(__file__),'classi_cls.csv'),header=0)
	def __init__(self,Rck=None,fck=None,FC=1.,Ecm=None):
		self.__Rck=Rck
		self.__fck=fck
		self.__FC=FC
		self.__Ecm=Ecm
		if fck is None:
			self.__fck=.83*Rck
		else:
			self.__Rck=fck/.83
		self.calcola()
	
	def calcola(self):
		self.fck=self.__fck/self.FC
		self.Rck=self.__Rck/self.FC
		self.fcm=self.__fck+8 
		if self.Rck<=60:
			self.fctm=0.3*self.fcm**(2./3)
		else:
			self.fctm=2.12*log(1+self.fcm/10)
		self.fctk=.7*self.fctm
		self.fctk_=1.3*self.fctm
		if self.__Ecm is None:
			self.Ecm=22000*(self.fcm/10)**.3
		else:
			self.Ecm=self.__Ecm
			es=(-2+sqrt(2**2-4*-1*-.4))/(2*-1)
			ec=0.4/es/self.Ecm*self.fcm
			if ec<=self.ecu: self.ec=ec
			else:self.ec=self.ecu
		self.G=self.Ecm/(2*(.2+1))	
		self.calcolaFcd()

	def calcolaFcd(self,mode='SLU',mult=1):
		if type(mode)==str:
			mode=mode.upper()
		if mode=='SLU' or mode=='D':
			self.fcd=self.fck/self.gamma_c*.85*mult
		elif mode=='SLE':
			self.fcd=self.fcm*mult
		elif mode=='K':
			self.fcd=self.fck*mult
		elif mode=='M':
			self.fcd=self.fcm*mult

	def _Ecm(self):
		def zerome(x):
			return self.sigma(x)-.4*self.fcd
		x=root(zerome,self.ec/2)
		if x.status:
			return .4*self.fcd/x.x[0],x
		else:
			return x
	
	@property
	def FC(self):
		return self.__FC
	
	@FC.setter
	def FC(self,val):
		self.__FC=val
		self.calcola()

	def __sigma(self,ec):
		'''calcola la tensione nel calestruzzo in un punto'''
		if not 0< ec <=self.ecu:
			return 0.
		if 0<ec< self.ec:
			return (-1/self.ec**2*ec**2 + 2/self.ec*ec)*self.fcd
		elif self.ec<=ec<=self.ecu:
			return self.fcd
	__sigma_=vectorize(__sigma)	#versione vettorizzata della __sigma, in questo modo richiede come input self ed ec obbligatori	
	def sigma(self,ec): #versione vettorizzata di sigma che si comporta correttamente come __sigma in fase di chiamata
		'''calcola la tensione del calcestruzzo nota la deformazione'''
		return self.__sigma_(self,ec)

	def __str__(self):
		if self.__FC!=1:
			st='<#FC#> = %.4g\n'%self.__FC
		else:st=''
		st+= '''<#Rck#> = %(Rck).4g MPa
<#f_ck#> = %(fck).4g MPa
<#f_cm#> = %(fcm).4g MPa
<#f_ctm#> = %(fctm).4g MPa
<#f_ctk#> = %(fctk).4g MPa
<#E_cm#> = %(Ecm).4g MPa
<#G#> = %(G).4g MPa'''%vars(self)
		return st

	def plot_diagramma(self,mode='k',mult=1,**kargs):
		opt={'plot_E':bool(self.__Ecm),
			 'plot_ep':True,
			 'plot_info':True}
		opt.update(kargs)
		if not opt['plot_info']:
			for i in list(opt.keys()):
				opt[i]=False
		old_fcd=self.fcd
		self.calcolaFcd(mode,mult)
		ep=linspace(0,self.ecu,100)
		s=self.sigma(ep)
		plot(ep,s)
		if mode=='d':
			text(self.ec,self.fcd,r'$\alpha f_{c%s}$=%.4g MPa'%(mode,self.fcd),horizontalalignment='right')
		else:
			text(self.ec,self.fcd,r'$f_{c%s}$=%.4g MPa'%(mode,self.fcd),horizontalalignment='right')
		#modulo elastico
		if opt['plot_E']:
			E,x=self._Ecm()
			s=self.sigma(x.x)
			plot([0,x.x,x.x],[s,s,0],'--',color='grey')
			plot(x.x,s,'xr')
			plot([0,x.x],[0,s],'r')
			text(.5*x.x,.5*s,r'$E_{cm}$=%(E).4g MPa'%locals())
			text(x.x,s,r'$0.4 f_{c%s}$'%mode)
		#epsilon
		if opt['plot_ep']:
			h=r_[ylim()].ptp()
			plot([self.ec,self.ec],[0,self.fcd],'--',color='grey')
			text(self.ec,.01*h,r'$\epsilon_c$=%.4g'%self.ec,horizontalalignment='right')
			text(self.ecu,.05*h,r'$\epsilon_{cu}$=%.4g'%self.ecu,horizontalalignment='right')
		xlabel(r'$\epsilon$')
		ylabel(r'$\sigma_c [MPa]$')
		grid()
		self.fcd=old_fcd


class Acciaio:
	def __init__(self,fyk=450,ftk=530,*args,**kargs):
		self.fyk=fyk
		self.ftk=ftk
		self.euk_=0.079
		self.euk=0.01
		self.Es=210e3
		self.FC=1
		self.gamma_s=1.15
		vars(self).update(**kargs)
		self.calcola()

	def calcola(self):
		self.fyk/=self.FC
		self.ftk/=self.FC
		k=self.ftk/self.fyk
		self.eud=self.euk
		self.eyk=self.fyk/self.Es
		self.fyd=self.fyk/self.gamma_s
		self.ftd=self.fyk*k
		self.eyd=self.fyd/self.Es

	def sigma(self,es):
		return (abs(es)<=self.eud) * ((abs(es)<self.eyd) * self.Es*es + (abs(es)>=self.eyd)* sign(es)*self.fyd)

	def __str__(self):
		if self.FC==1:
			st=''
		else:
			st='<#FC#>= %(FC).4g'%vars(seflf)
		st+='''<#f_yk#> = %(fyk).4g MPa
<#f_tk#> = %(ftk).4g MPa
<#f_td#> = %(ftd).4g MPa
<#f_yd#> = %(fyd).4g MPa
<#e_uk#> = %(euk).4g
<#e_ud#> = %(eud).4g
<#e_yk#> = %(eyk).4g
<#e_yd#> = %(eyd).4g
<#E_s#> = %(Es).4g MPa'''%vars(self)
		return st

class AcciaioIncrudente(Acciaio): #acciaio con legame incrudente
	def sigma(self,es):
		return (abs(es)<=self.eud).astype(float) * ((abs(es)<self.eyd).astype(float) * self.Es*es + (abs(es)>=self.eyd).astype(float)* sign(es)*(self.fyd + ((self.ftd-self.fyd)/(self.eud -self.eyd)*(abs(es)-self.eyd) )))
def ancoraggio(cls,barra,fyd=391.):
	if hasattr(barra,'di'):
		phi=barra.phi
		fyd=barra.mat.fyd
	else:
		phi=barra
	eta={True:1,False:(132-phi)/100}[phi<=32]
	fbk=2.25*eta*cls.fctk
	fbd=fbk/1.5
	return 	phi**2*pi/4*fyd / (fbd*phi*pi)

