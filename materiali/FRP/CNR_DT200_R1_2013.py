from scipy import *
from gueng.materiali.legami import ElastoFragile
class FRP(ElastoFragile):
	kG=dict(preformato=0.023,impregnato=0.037)
	kq=dict(distribuito=1.25 , altro=1)
	gammab=dict(rara=1,frequente=1.2)
	def __init__(self,tf,bf,Ef,bc,fcd,fctd,tipo='impregnato',carico='distribuito',condizione='rara',FC=1,posizione='intermedia',gammafd=1.5,gammaRd=1):
		'''
		tf= spessore equivalente fibre
		bf=base fibre
		Ef=modulo elastico fibre
		bc=base cls
		fcd ,fctd  resistenza a compressione e trazione del cls
		tipo= tipo di rinforzo impregnato o preformato
		carico= tipo di carico distribuito oppure altro
		condizione='tipo di condizione di carico rara oppure frequente 
		FC=fattore di confidenza
		posizione= intermedia o estremità, posizione rispetto al nastro della sezione di verifica
		gammafd coefficiente parziale fibre
		gammaRd sovraresistenza= 1 per flessione, |1.2 taglio |1.1 confinamento
		'''

		##tensione massima per distacco di estremità 4.1.3
		kb=max((1,((2-bf/bc)/(1+bf/bc))**.5)) #4.3
		kG=FRP.kG[tipo] #4.3
		GAMMAFd=kb*kG/FC*(fcd*fctd)**.5 #energia specifica di frattura 4.2
		# tensione massima per distacco di estremità 4.4
		ffdd=1/gammafd*(2*Ef*GAMMAFd/tf)**.5 
		epsffdd=ffdd/Ef
		##tensione massima per distacco intermedio #4.1.4
		kq=FRP.kq[carico]
		kG2=.1 #sempre
		ffdd2=kq/gammafd*(Ef/tf*2*kb*kG2/FC*(fcd*fctd)**.5)**.5 
		epsffdd2=ffdd2/Ef

		#verifica tensioni di interfaccia a SLE #4.1.5
		gammab=FRP.gammab[condizione]
		fbdsle=0.21*kb*fctd/gammab/FC
		fbdslu=2*GAMMAFd/0.25

		#lunghezza minima di ancoraggio
		led=max((1/(gammaRd*fbd)*(pi**2*Etf*tf*GAMMAFd/2)**.5,200))
		
		vars(self).update(locals())

		if posizione=='intermedia':
			ElastoFragile.__init__(self,ffdd2,Ef,1,1)
		elif posizione=='estremità':
			ElastoFragile.__init__(self,ffdd,Ef,1,1)

	def __str__(self):
		st=f'''Ef={self.Ef:.1f} MPa
TENSIONE PER DISTACCO DI ESTREMITA'
kb={self.kb:.3f}
kG={self.kG:.3f}
GAMMA_Fd={self.GAMMAFd:.4g}
ffdd={self.ffdd:.2f} MPa --> epsffdd={self.epsffdd:.4g}
led={self.led:.1f} mm

TENSIONE PER DISTACCO INTERMEDIO
kq={self.kq:.3f}
kG2={self.kG2}
ffdd2={self.ffdd2:.2f} MPa --> epsffdd2={self.epsffdd2:.4g}

TENSINE DI INTERFACCIA
gammab={self.gammab:.3f}
fbdsle={self.fbdsle:.3f} MPa
fbdslu={self.fbdslu:.3f} MPa
'''
		return st

class AzioniAmbientali(float):
	'''calcolo coefficiente eta per condizione ambientale'''
	eta=dict(
	interna=dict(
		vetro=.75,
		aramidica=.85,
		carbonio=.95,
		),
	esterna=dict(
		vetro=.65,
		aramidica=.75,
		carbonio=.85,
		),
	aggressiva=dict(
		vetro=.5,
		aramidica=.7,
		carbonio=.85,
		),
	)

	def __init__(self,fibra,condizione):
		'''fibra=vetro | aramidica | carbonio
		condizione= interna | esterna | aggressiva
		'''
		vars(self).update(locals())
		self.eta=AzioniAmbientali.eta[condizione][fibra]
	
	def __new__(cls,fibra,condizione):
		n=float.__new__(cls,0.)
		n.__init__(fibra,condizione)
		out=float.__new__(cls,n.eta)
		vars(out).update(vars(n))
		del n
		return out

	def __str__(self):
		return f'condizione: {self.condizione} , fibra: {self.fibra}, eta={self.eta}'


		
class EffettiDurata(float):
	'''calcolo coefficiente eta per durata di carico'''
	eta=dict(
	lunga=dict(
		vetro=.3,
		aramidica=.5,
		carbonio=.8,
		),
	ciclico=dict(
		vetro=..5,
		aramidica=.5,
		carbonio=.5,
		),
	)

	def __init__(self,fibra,condizione):
		'''fibra=vetro | aramidica | carbonio
		durata= lunga | ciclica
		'''
		vars(self).update(locals())
		self.eta=EffettiDurata.eta[durata][fibra]
	
	def __new__(cls,fibra,condizione):
		n=float.__new__(cls,0.)
		n.__init__(fibra,condizione)
		out=float.__new__(cls,n.eta)
		vars(out).update(vars(n))
		del n
		return out

	def __str__(self):
		return f'durata: {self.durata} , fibra: {self.fibra}, eta={self.eta}'

	
