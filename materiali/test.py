
from gueng.materiali import Calcestruzzo
import unittest as ut,sys

Rck=50

class mytest(ut.TestCase):
	def setUp(self):
		self.cl=Calcestruzzo(Rck=Rck)

	def test_fck(self):
		'''controllo calcolo di fck'''
		ref=.83*Rck
		self.assertEqual(self.cl.fck,ref)

	def test_fcm(self):
		'''controllo calcolo di fcm'''
		ref=self.cl.fck+8
		self.assertEqual(self.cl.fcm,ref)

	def test_fctm(self):
		'''controllo calcolo di fctm'''
		if self.cl.Rck<=60:
			ref=0.3*self.cl.fcm**(2./3)
		else:
			ref= 2.12*log(1+self.cl.fcm/10)
		self.assertEqual(self.cl.fctm,ref)

	def test_fctk(self):
		'''controllo calcolo di fctk'''
		ref=self.cl.fctm*.7
		self.assertEqual(self.cl.fctk,ref)

	def test_fcd(self):
		'''controllo calcolo di fcd'''
		ref=self.cl.fck*.85/1.6
		self.assertAlmostEqual(ref/self.cl.fcd,1,5)

	def test_E(self):
		'''controllo calcolo di E'''
		ref=22e3*(self.cl.fcm/10)**.3
		self.assertEqual(ref,self.cl.Ecm)

	def test_G(self):
		'''controllo modulo di taglio'''
		ref=self.cl.Ecm/(2*(1+.2))
		self.assertEqual(ref,self.cl.G)


ut.main(testRunner=ut.TextTestRunner(stream=sys.stderr,verbosity=2))
