﻿
import os,sqlite3
from scipy import *

class Calcestruzzo:
	def __init__(self,rck):
		'rck in MPa'
		self.rck=rck
		self.fck=.83*self.rck
		self.fcm=self.fck+8.
		if self.rck<=60:
			self.fctm=0.3*self.fck**(2./3)
		else:
			self.fctm=2.12*log(1+fcm/10)
		self.fctk=self.fctm*0.7
		self.Ecm=22000*(self.fcm/10)**.3

class Acciaio_cls:
	def __init__(self,fyk):
		self.fyk=fyk
		self.fyd=self.fyk/1.15

class Acciaio:
	def __init__(self,fyk=None,ftk=None):
		#di default S235
		if fyk is None:
			fyk,ftk=[235,360]
		self.fyk=fyk
		self.ftk=ftk
		self.fyd=self.fyk/1.15

class Acciaio_bul:
	fy={4.6:240.,
		5.6:300.,
		6.6:360.,
		8.8:640.,
		10.9:900.}
	
	ft={4.6:400.,
		5.6:500.,
		6.6:600.,
		8.8:800.,
		10.9:1000.}

	outodf=False
	
	def __init__(self,cl):
		self.cl=cl
		if cl in list(Acciaio_bul.fy.keys()):
			self.fyk=Acciaio_bul.fy[cl]
			self.ftk=Acciaio_bul.ft[cl]
			self.fuk=self.ftk
		else:
			print(' Acciaio sconosciuto')
			self.ftk=float(int(cl))*100
			self.fyk=self.ft*(cl%1)
			self.fuk=self.ftk
			print(vars(self))

	def __str__(self):
		st='''<#f_uk#>={0.fuk:.4g} MPa ; <#f_yk#>={0.fyk:.4g} MPa'''.format(self)
		if not self.outodf:
			st=st.replace('<#','')
			st=st.replace('#>','')
		return st

class Legno:
	#per fare una bella cosa dovrei riportare tutti i coefficienti nel database legni.sqlite
	#per ora il legni.sqlite è fatto sulla base della cnr-dt-206-2006
	gamma_m={'massiccio':1.3,
			 'lamellare':1.25,
			 'parcicelle':1.3,
			 'fibre':1.3,
			 'compensato':1.2,
			 'unioni':1.3,
	'eccezionali':1}
	
	#da dm
	gamma_m={'massiccio':1.5,
			 'lamellare':1.45,
			 'parcicelle':1.5,
			 'fibre':1.5,
			 'compensato':1.4,
			 'unioni':1.5,
	'eccezionali':1}

	#NOTA: la voce compensato comprende anche scaglie e multistrato
	#definisce la tipologia per ottenere il gamma_m
	tipi={'mas':'massiccio',
		  'lam':'lamellare',
		  'lvl':'compensato',
		  'comp1':'compensato',
		  'comp2':'compensato',
		  'comp3':'compensato',
		  'osb2':'compensato',
		  'osb3':'compensato',
		  'truc4':'particelle',
		  'truc5':'particelle',
		  'truc6':'particelle',
		  'truc7':'particelle',
		  'hbla':'fibre',
		  'hbhla':'fibre',
		  'mbhla':'fibre',
		  'mbhhls':'fibre',
		  'mdfla':'fibre',
		  'mdfhls':'fibre'}
	#NOTA: la voce compensato comprende anche scaglie e multistrato
	path=os.path.dirname(__file__)
	db=sqlite3.connect(os.path.join(path,'legni.sqlite'))
	essenze=db.execute('select tipo,classe from legni').fetchall()
	outodt=True #impostare a true per out formattato con formule
		  
	
	def __init__(self,tipo,classe,durata,essenza,unioni=False,FC=1.):
		path=os.path.dirname(__file__)
		self.db=sqlite3.connect(os.path.join(path,'legni.sqlite'))
		self.FC=FC
		if tipo not in list(Legno.tipi.keys()):
			raise IOError
		vars(self).update(locals())
		if self.unioni:
			self.gamma_m=Legno.gamma_m['unioni']
		else:
			self.gamma_m=Legno.gamma_m[Legno.tipi[self.tipo]]
		self.calc_kmod()
		self.calc_kdef()
		self.carica_prop()

	


	def carica_prop(self):
		es=self.essenza.split()
		if len(es)==1:
			dat=self.db.execute('select * from legni where classe=?',(self.essenza,))
		else:
			dat=self.db.execute('select * from legni where tipo=? and classe=?',tuple(es))
		keys=[i[0] for i in dat.description]
		vals=dat.fetchall()[0]
		dat=dict(list(zip(keys,vals)))
		del dat['classe'],dat['id'],dat['tipo']
		vars(self).update(dat)
		prog={}
		for i in keys:
			if i[-1]=='k':
				j=i[:-1]+'d'
				v=dat[i]/self.gamma_m*self.kmod/self.FC
				prog[j]=v
		vars(self).update(prog)
	  

	def calcola(tipo,classe,durata,unioni=False):
		vars(self).update(locals())
		if self.unioni:
			self.gamma_m=Legno.gamma_m['unioni']
		else:
			self.gamma_m=Legno.gamma_m[Legno.tipi[self.tipo]]
		self.calc_kmod()
		self.calc_kdef()
		self.carica_prop()
		
	def calc_kmod(self):
		k_mod={	 'mas':{1:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.},
			   2:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.},
			   3:{'permanente':.5,
			  'lunga':.55,
			  'media':.65,
			  'breve':.7,
			  'istantanea':.9}},

					'lam':{1:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.},
			  2:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.},
			   3:{'permanente':.5,
			  'lunga':.55,
			  'media':.65,
			  'breve':.7,
			  'istantanea':.9}},
			  
					  'comp3':{1:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.},
			   2:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.},
			   3:{'permanente':.5,
			  'lunga':.55,
			  'media':.65,
			  'breve':.7,
			  'istantanea':.9}},

					'comp2':{1:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.},
			   2:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.}},

					   'comp1':{1:{'permanente':.6,
			  'lunga':.7,
			  'media':.8,
			  'breve':.9,
			  'istantanea':1.}},

					  'osb2':{1:{'permanente':.3,
			  'lunga':.45,
			  'media':.65,
			  'breve':.85,
			  'istantanea':1.}},

					  'osb3':{1:{'permanente':.4,
			  'lunga':.5,
			  'media':.7,
			  'breve':.9,
			  'istantanea':1.},
			   2:{'permanente':.3,
			  'lunga':.4,
			  'media':.55,
			  'breve':.7,
			  'istantanea':.9}},

					  'truc5':{1:{'permanente':.3,
			  'lunga':.45,
			  'media':.65,
			  'breve':.85,
			  'istantanea':1.},
			   2:{'permanente':.2,
			  'lunga':.3,
			  'media':.45,
			  'breve':.6,
			  'istantanea':.8}},

					  'truc7':{1:{'permanente':.4,
			  'lunga':.5,
			  'media':.7,
			  'breve':.9,
			  'istantanea':1.},
			   2:{'permanente':.3,
			  'lunga':.4,
			  'media':.55,
			  'breve':.7,
			  'istantanea':.9}},

					  'hbla':{1:{'permanente':.3,
			  'lunga':.45,
			  'media':.65,
			  'breve':.85,
			  'istantanea':1.}},

					  'hbhla':{1:{'permanente':.3,
			  'lunga':.45,
			  'media':.65,
			  'breve':.85,
			  'istantanea':1.},
			   2:{'permanente':.2,
			  'lunga':.3,
			  'media':.45,
			  'breve':.60,
			  'istantanea':.80}},

					   'mbhla':{1:{'permanente':.2,
			  'lunga':.4,
			  'media':.6,
			  'breve':.8,
			  'istantanea':1.}},

					  'mbhhls':{1:{'permanente':.2,
			  'lunga':.4,
			  'media':.6,
			  'breve':.8,
			  'istantanea':1.},
			   2:{'permanente':.0,
			  'lunga':.0,
			  'media':.0,
			  'breve':.45,
			  'istantanea':.80}},

					  'mdfla':{1:{'permanente':.2,
			  'lunga':.4,
			  'media':.6,
			  'breve':.8,
			  'istantanea':1.}},

					  'mdfhls':{1:{'permanente':.2,
			  'lunga':.4,
			  'media':.6,
			  'breve':.8,
			  'istantanea':1.},
			   2:{'permanente':.0,
			  'lunga':.0,
			  'media':.0,
			  'breve':.45,
			  'istantanea':.80}}}
		
		k_mod['truc4']=k_mod['truc5']
		k_mod['truc6']=k_mod['truc7']
		k_mod['lvl']=k_mod['lam']
		self.kmod=k_mod[self.tipo][self.classe][self.durata]

	def calc_kdef(self):
		kdef={'mas':{1:.6,
					 2:.8,
					 3:2.},
			  'lam':{1:.6,
					 2:.8,
					 3:2.},
			  'comp1':{1:.8,
					 2:0,
					 3:0},
			  'comp2':{1:.8,
					 2:1.,
					 3:0},
			  'comp3':{1:.8,
					 2:1.,
					 3:2.5},
			  'osb2':{1:2.25,
					 2:0,
					 3:0.},
			  'osb3':{1:1.5,
					 2:2.5,
					 3:0},
			  'truc4':{1:2.25 , 2:0 , 3:0},
			  'truc5':{1:2.25 , 2:3., 3:0},
			  'truc6':{1:1.5 , 2:.0 , 3:0.},
			  'truc7':{1:1.25 , 2:2.25 , 3:0},
			  'hbla':{1:2.25 , 2:0 , 3:0},
			  'hbhla':{1:1.25 , 2:3. , 3:0},
			  'mbhla':{1:3. , 2:0 , 3:0},
			  'mbhhls':{1:3. , 2:4. , 3:0},
			  'mdfla' :{1:2.25 , 2:0 , 3:0},
			  'mdfhls':{1:2.25, 2:3.,3:0}}
		
		kdef['lvl']=kdef['lam']

		self.kdef=kdef[self.tipo][self.classe]

	def __str__(self):
		st='''Tipo: {0.tipo}; Essenza: {0.essenza}; durata carico: {0.durata}
K~def~= {0.kdef:.4g} ; K~mod~={0.kmod:.4g} &gamma;~m~={0.gamma_m:.4g} ; FC={0.FC:.4g}
f~mk~={0.fmk:.4g} MPa --> f~md~={0.fmd:.4g} MPa
f~c0k~={0.fc0k:.4g} MPa --> f~c0d~={0.fc0d:.4g} MPa
f~c90k~={0.fc90k:.4g} MPa --> f~c90d~={0.fc90d:.4g} MPa
f~t0k~={0.ft0k:.4g} MPa --> f~t0d~={0.ft0d:.4g} MPa
f~t90k~={0.ft90k:.4g}MPa --> f~t90d~={0.ft90d:.4g} MPa
f~vk~={0.fvk:.4g} MPA --> f~vd~={0.fvd:.4g} MPa
E~005~={0.E005:.4g} MPa ; E~0m~={0.E0m:.4g} MPa ; E~90m~= {0.E90m:.4g} MPa
G~m~={0.Gm:.4g} MPa
&rho;~k~={0.rok:.2f} kg/m^3^'''.format(self)
#		if not hasattr(self,'outodt'):
#			self.outodt=False
#		if not self.outodt:
#			st=st.replace('$$','')
		return st

	def fcad(self,alpha_,conv=pi/180):
		alpha=alpha_*conv
		return self.fc0d/((self.fc0d/self.fc90d)*sin(alpha)**2+cos(alpha)**2)

