from numpy import *

class PiastraPL:
	def __init__(self,ac,t,b,l,index,mult=None,d0=1,use_ftd=False):
		'''ac=acciaio
		t=spessore
		b=bracci considerati
		l=lunghezza di ogni cerniera plastica
		index=array che specifica per ogni lunghezza quali rotazioni sommare
		mult=moltiplicatori per considerare parzializzazione cerniera:
			None=ones(len(l)), array, dict{indice:valore}
		d0=spostamento imposto
		use_ftd=False,True per usare resistenza ultima plastica
		'''
		#controllo dati di input
		#converto in array
		b=atleast_1d(b).astype(float)
		l=atleast_1d(l).astype(float)
		print(b,l)
		#controllo moltiplicatori
		if mult is None:
			mult=ones(len(l))
		elif type(mult)==dict:
			m=ones(len(l))
			for i,j in mult.items():
				m[i]=j
			mult=m
		vars(self).update(locals())

		if use_ftd:
			fd=self.ac.ftd
		else:
			fd=self.ac.fyd
		self.stima_spostamento()
		if self.d0< self.dmin:
			raise IOError(f'lo spostamento iniziale d0={d0} risulta inferiore allo spostamento per cui tutte le cerniere plastiche sono attivate pari a {self.dmin}')
		self.M0=self.t**2/4*fd #momento unitario
		'''considero unitariao lo spostamento dei bulloni, calcolo quindi la rotazione delle cerniere in base al braccio'''
		self.rotb=arctan(d0/b) #rotazioni di base
		self.rot=zeros(len(self.l),dtype=float)
		for n,i in enumerate(index):
			self.rot[n]=sum(self.rotb[i])*self.mult[n]
		self.Wii=self.l*self.rot*self.M0
		self.Wi=sum(self.Wii)

	def stima_spostamento(self):
		'''stimo lo spostamento necessario per portare a plasticizzazione tutte le cerniere plastiche, per farlo procedo
		calcolando il momento plastico senza coefficienti
		considerando trave incastrata con tale momento
		valutando in modo elastico quale sarebbe lo spostamento
		'''
		M=self.t**2/4*self.ac.fyk
		J=self.t**3/12
		xi=M/self.ac.Es/J
		br=self.b.max()
		xip=r_[-xi/br**2,0,xi]
		vp=polyint(xip,2)
		self.dmin=polyval(vp,br)
		return self.dmin,vp


		



