#-*- coding:utf-8-*-

from pylab import *
from scipy import *
from scipy.linalg import norm
ion()

#from pdb import set_trace
class Geometria:
	def __init__(self,pos,Ox=None,Oy=None,Oz=None,solo_trazione=False):
		'''pos= nx2 array delle posizioni dei bulloni
		Ox=polo per calcolo Wx [punto minimo di default]
		Oy=polo per calcolo Wy [minimo di default]
		Oz= polo per calcolo Wz [baricentro di default']
		solo_trazione =[False:True] True per considerare solo i bulloni in trazione'''
		if type(pos)==list:
			pos=r_[pos]
		test1=all([all(i==pos[1]) for i in pos[1:]])#controllo se le posizioni sono tutte uguali... sinifica che la bullonatura Ã¨ qadrata )
		if test1 and len(pos)==4:
			x,y=abs(pos[0])
			pos=r_[[[x,y],[-x,y],[-x,-y],[x,-y]]]
			del x,y
		del test1
		temp=pos.copy()
		for n,i in enumerate((Ox,Oy,Oz)):
			if i is not None:
				if not hasattr(i,'__len__'):
					i={1:r_[0,i],2:[i,0],3:[i,i]}[n]
				temp=vstack((temp,i))
		self.limiti=r_[temp[:,0].min(),temp[:,0].max(),temp[:,1].min(),temp[:,1].max()]
		del temp
		vars(self).update(locals())
		if Ox is None: Ox=r_[self.limiti[0],self.limiti[2]]
		elif Ox=='G': Ox=self.G
		if Oy is None: Oy=r_[self.limiti[0],self.limiti[2]]
		elif Oy=='G': Oy=self.G
		if Oz is None: Oz=self.G
		vars(self).update(locals())

	@property
	def G(self):
		if not hasattr(self,'_Geometria__G'):
			self.__G= self.pos.sum(0)/len(self.pos)
		return self.__G

	@property
	def nbul(self):
		return len(self.pos)

	def Wx_(self,O=None):
		#set_trace()
		if not hasattr(self,'_Geometria__Wx'):
			if O is None:
				O=self.Ox
			if hasattr(O,'__len__'):
				if len(O)==1:
					O=r_[0,O[0]]
#			else:
#				O=r_[0,O]
			b=self.pos-O
			if self.solo_trazione:
				b=b[b[:,1]>=0,1]
			else:
				b=b[:,1]
			if len(b)>0:
				out= sum(b**2)/max(abs(b))
			else:
				out=0
			if isnan(out): out=0
			self.__Wx=out
		return self.__Wx

	def Wy_(self,O=None):
		#set_trace()
		if not hasattr(self,'_Geometria__Wy'):
			if O is None:
				O=self.Oy
			if hasattr(O,'__len__'):
				if len(O)==1:
					O=r_[O[0],0]
#			else:
#				O=r_[O,0]
			b=self.pos-O
			if self.solo_trazione:
				b=b[b[:,0]>=0,0]
			else:
				b=b[:,0]
			if len(b)>0:
				out= sum(b**2)/max(abs(b))
			else:
				out=0
			if isnan(out): out=0
			self.__Wy= out
		return self.__Wy

	
	def Wz_(self,G=None):
#		set_trace()
		if not hasattr(self,'_Geometria__Wz'):
			if G is None:
				G=self.Oz
			b=self.pos-G
			br=r_[[dot(i,i) for i in b]]
			index=where(br==br.max())[0][-1]
			self.__Wz= br.sum()/sqrt(br[index])
			self.e=cross(r_[0,0,1],r_[b[index],0])[:2]
			self.e/=norm(self.e)
			self.__Wzdat={'index':index,'Wz':self.__Wz}
			self.__Wz=abs(self.__Wz)
		return self.__Wz

	@property
	def px(self):
		#percentuale di trazione rispetto al bullone più teso
		if not hasattr(self,'_px'):
			self._px=self.pos[:,1]-self.O[1]
			self._px[self._px<0]=0
			self._px/=self._px.max()
		return self._px


	@property
	def py(self):
		#percentuale di trazione rispetto al bullone più teso
		if not hasattr(self,'_py'):
			self._py=self.pos[:,0]-self.O[0]
			self._py[self._py<0]=0
			self._py/=self._py.max()
		return self._py

	def reset(self):
		del self.__Wx,self.__Wy,self.__Wz,self.__G

	@property
	def Wx(self):
		return self.Wx_()

	@property
	def Wy(self):
		return self.Wy_()

	@property
	def Wz(self):
		return self.Wz_()

	def plot(self):
		self.Wx
		self.Wy
		self.Wz
		plot(self.pos[:,0],self.pos[:,1],'or',ms=12,mew=3,label='Bulloni')
		plot(self.G[0],self.G[1],'xg',ms=12,mew=3,label='Baricentro')
		plot(self.Ox[0],self.Ox[1],'xb',ms=12,mew=3,label='Polo Wx')
		plot(self.Oy[0],self.Oy[1],'xc',ms=12,mew=3,label='Polo Wy')
		plot(self.Oz[0],self.Oz[1],'xy',ms=12,mew=3,label='Polo Wz')
		dx,dy=self.Wz*self.e
		dx,dy=r_[dx,dy]/norm(r_[dx,dy]) * sum([r_[xlim()].ptp(),r_[ylim()].ptp()])*.2
		x,y=self.pos[self.__Wzdat['index']]
		arrow(x,y,dx,dy,head_width=norm(r_[dx,dy])*.1)
		legend(loc=0,numpoints=1)
		axis('equal')
		grid(1)
		xlim(r_[xlim()]+r_[-1,1]*r_[xlim()].ptp()*.3)
		ylim(r_[ylim()]+r_[-1,1]*r_[ylim()].ptp()*.3)
		xlim(self.limiti[:2]+r_[-1,1]*self.limiti[:2].ptp()*.1)
		ylim(self.limiti[2:]+r_[-1,1]*self.limiti[2:].ptp()*.1)
		xlabel('x')
		ylabel('y')

	def __str__(self):
		wx,wy,wz,g,ox,oy,oz=self.Wx,self.Wy,self.Wz,self.G,self.Ox,self.Oy,self.Oz
		return  '''W~x~= %(wx)f\t O~x~=%(ox)s
W~y~= %(wy)f\t O~y~=%(oy)s
W~z~= %(wz)s\t O~z~=%(oz)s
G=%(g)s'''%locals()



if __name__=='__main__':
#	g=Geometria([r_[1,1]/sqrt(2)]*4)
#	pos=r_[[[-1,0],[1,0],[0,1],[0,-1]]]
	pos=r_[[[-1,0],[1,0]]]
	a=rad2deg(0)
	M=r_[[[cos(a),-sin(a)],[sin(a),cos(a)]]]
	pos=r_[[dot(M,i) for i in pos]]
	pos=[r_[1,.5]]*4
	pos=r_[[[-1,1],[-1,0],[-1,-1]]]
	pos=r_[pos,pos*r_[-1,1]]
	g=Geometria(pos)
	print(g)
	g.plot()


