
from gueng.utils import Verifiche,Tabella
from scipy import *
from scipy.linalg import norm


class Giunto:
	def __init__(self,geom,sol,bul,npt,l,Mr=eye(3),b=r_[0,0,0]):
		'''geom= classe geometria del giunto
		sol=sollecitazioni da verificare
		bul=bullone impiegato
		npt=piani di taglio
		l=lamiera
		Mr=matrice di orientamento del giunto nello spazio 
		b=bracci per momenti parassiti da centro bullonatura a punto applicazione forze'''
		vars(self).update(locals())

		self.ver=[Verifiche('Tagio bullone'),
			 Verifiche('Trazione bullone'),
			 Verifiche('Taglio-Trazione bullone'),
			 Verifiche('Rifollamento lamiera'),
			 Verifiche('Punzonamento lamiera')]

		self.tab=Tabella('cmb ide V[N] M[Nmm] Mpar[Mnn] Tsd1[N] Tsd2[N] Tsd3[N] Tsd[N] cu0 Nsd1[N] Nsd2[N] Nsd3[N]  Nsd4[N]  Nsd5[N] Nsd[n] cu1 cu2 Tsd_[N] cu3 cu4')

		Fvrd=bul.F_vrk/1.25
		Ftrd=bul.F_trk/1.25
		Fbrd=l.Fb_rk(bul)[0]/1.24
		Bprd=l.Bp_rk(bul)/1.24
		for s in self.sol:
			cmb=int(s[-2])
			e=int(s[-1])
			ide='elem %(e)d cmb%(cmb)d'%locals()
			V=dot(Mr,s[:3])
			M=dot(Mr,s[3:6])
			Mpar=cross(b,V) #momento parassita complessivo
			#verifiche a taglio bullone
			Tsd1=V[:2]/geom.nbul/npt
			Tsd2=M[2]/geom.Wz*geom.e/npt
			Tsd3=Mpar[2]/geom.Wz*geom.e/npt #dovuto al momento parassita
			Tsd=norm(Tsd1+Tsd2+Tsd3)
			cu0=Tsd/Fvrd
			self.ver[0](cu0,1,1,ide)
			#Verifica a trazione bullone:
			Nsd1=V[2]/geom.nbul
			if geom.Wx!=0:
				Nsd2=abs(M[0]/geom.Wx)
				Nsd4=abs(Mpar[0]/geom.Wx)
			else:
				Nsd2=0
				Nsd4=0
			if geom.Wy!=0:
				Nsd3=abs(M[1]/geom.Wy)
				Nsd5=abs(Mpar[1]/geom.Wy)
			else:
				Nsd3=0
				Nsd5=0
			Nsd=Nsd1+Nsd2+Nsd3+Nsd4+Nsd4
			if Nsd<0: Nsd=0
			cu1=Nsd/Ftrd
			self.ver[1](cu1,1,1,ide) #verifica a trazione
			cu2=cu0+cu1/1.4
			self.ver[2](cu2,1,1,ide)# verifica a taglio trazione
			#Verifica Lamiera:
			Tsd_=Tsd*npt
			cu3=Tsd_/Fbrd
			self.ver[3](cu3,1,1,ide)
			cu4=Nsd/Bprd
			self.ver[4](cu4,1,1,ide)
			self.tab()

	def __str__(self):
		st=[]
		for v in self.ver:
			v.full_out=2
			st.append('%s'%v)
			v.full_out=True
			st.append('')

		st.append('#'*60 + '\nTutto ok: ' + {True:'Si' , False:'No'}[all([v.all() for v in self.ver])])

		return '\n'.join(st)

	def all(self):
		return all([v.all() for v in self.ver])

	def __bool__(self):
		return int(self.all())

