import tkinter.ttk
from tkinter.ttk import Tkinter as tk
from collections import OrderedDict as Odi

class AcciaioGui(tkinter.ttk.LabelFrame):
	def __init__(self,parent):
		tkinter.ttk.LabelFrame.__init__(self,parent,text='Acciaio')
		self.values=Odi()
		for n,i in enumerate('fyk ftk'.split()):
			tkinter.ttk.Label(self,text=i).grid(row=n,column=0)
			self.values[i]=tkinter.ttk.Entry(self)
			self.values[i].grid(row=n,column=1)

	def __str__(self):
		return 'Acciaio({},{})'.format(self.values['fyk'].get(),self.values['ftk'].get())

class GeometriaGui(tkinter.ttk.LabelFrame):
	def __init__(self,parent):
		tkinter.ttk.LabelFrame.__init__(self,parent,text='Coordinate bulloni')
		bframe=tkinter.ttk.Frame(self)
		bframe.pack()
		self.frameX=tkinter.ttk.Frame(bframe)
		self.frameY=tkinter.ttk.Frame(bframe)
		for i in (self.frameX,self.frameY): i.pack(side=tk.LEFT)
		self.posx=[]
		self.posy=[]
		tkinter.ttk.Label(self.frameX,text='X').pack()
		tkinter.ttk.Label(self.frameY,text='Y').pack()
		self.addpos()

		fframe=tkinter.ttk.Frame(self)
		fframe.pack()
		bt=tkinter.ttk.Button(fframe,text='add',command=self.addpos)
		bt.index=1
		bt.pack(side=tk.LEFT)
		bt=tkinter.ttk.Button(fframe,text='del',command=self.delpos)
		bt.index=2
		bt.pack(side=tk.LEFT)

	def __str__(self):
		st='[' + ','.join(['[{},{}]'.format(self.posx[i].get(),self.posy[i].get()) for i in range(len(self.posx))]) +']'
		return 'Geometria({})'.format(st)

	def addpos(self):
		self.posx.append(tkinter.ttk.Entry(self.frameX))
		self.posx[-1].pack()
		self.posy.append(tkinter.ttk.Entry(self.frameY))
		self.posy[-1].pack()

	def delpos(self,n=None):
		if n is None: n=-1
		self.posx[n].pack_forget()
		self.posy[n].pack_forget()
		del self.posx[n]
		del self.posy[n]

class LamieraGui(tkinter.ttk.LabelFrame):
	def __init__(self,parent):
		tkinter.ttk.LabelFrame.__init__(self,parent,text='Lamiera')
		voci='spessore','e1','e2','p1','p2','lx','ly','nlam'
		self.values=Odi()
		for n,i in enumerate(voci):
			tkinter.ttk.Label(self,text=i).grid(row=n,column=0)
			self.values[i]=tkinter.ttk.Entry(self)
			self.values[i].grid(row=n,column=1)

	def __str__(self):
		d={}
		for i,j in list(self.values.items()):
			d[i]=j.get()
		return 'Lamiera(ac,{spessore},{e1},{e2},{p1},{p2},{lx},{ly})'.format(**d)
		

class BulloneGui(tkinter.ttk.LabelFrame):
	def __init__(self,parent):
		tkinter.ttk.LabelFrame.__init__(self,parent,text='Bulloni')
		self.entrys=Odi()
		tkinter.ttk.Label(self,text='Bullone').grid(row=0,column=0)
		self.entrys['Bullone']=tkinter.ttk.Combobox(self)
		self.entrys['Bullone']['values']=(10,12,14,16,18,20,22,24,27,30,32)
		self.entrys['Bullone'].grid(row=0,column=1)

		for n,i in enumerate('Classe npt'.split()):
			tkinter.ttk.Label(self,text=i).grid(row=n+1,column=0)
			self.entrys[i]=tkinter.ttk.Entry(self)
			self.entrys[i].grid(row=n+1,column=1)
		

	def __str__(self):
		return 'Bullone({},{})'.format(self.entrys['Bullone'].get(),self.entrys['Classe'].get())

class SollecitazioniGui(tkinter.ttk.LabelFrame):
	def __init__(self,parent):
		tkinter.ttk.LabelFrame.__init__(self,parent,text='Sollecitazioni')
		r=tkinter.ttk.Frame(self)
		r.pack(fill='x',expand=1)
		tkinter.ttk.Label(r,text='database').pack(side=tk.LEFT)
		self.db=tkinter.ttk.Entry(r)
		self.db.pack(side=tk.LEFT,fill='x',expand=1)
		r=tkinter.ttk.Frame(self)
		r.pack()
		tkinter.ttk.Label(r,text='code').pack()
		self.text=tk.Text(r,background='#000000',foreground='#f0f0f0',insertbackground='#f0f0f0')
		self.text.pack()
	
class GiuntoGui(tkinter.ttk.LabelFrame):
	def __init__(self,parent):
		tkinter.ttk.LabelFrame.__init__(self,parent,text='Giunto')
		fl=tkinter.ttk.Frame(self)
		fl.pack(side=tk.LEFT,expand=1,fill='x',padx=1,pady=1)
		f=tkinter.ttk.Frame(fl)
		f.pack(expand=1,fill='x')
		tkinter.ttk.Label(f,text='Name').pack(side=tk.LEFT)
		self.name=tkinter.ttk.Entry(f)
		self.name.pack(side=tk.LEFT)
		f=tkinter.ttk.Frame(fl)
		f.pack(fill='x',expand=1)
		self.acciaio=AcciaioGui(f)
		self.acciaio.pack(fill='x',expand=1,padx=2,pady=10)
		self.bullone=BulloneGui(f)
		self.bullone.pack(fill='x',expand=1,padx=2,pady=10)
		self.geometria=GeometriaGui(f)
		self.geometria.pack(fill='x',expand=1,padx=2,pady=10)
		self.lamiera=LamieraGui(f)
		self.lamiera.pack(fill='x',expand=1,padx=2,pady=10)
		self.sol=SollecitazioniGui(self)
		self.sol.pack(side=tk.RIGHT)

	def __str__(self):
		npt=self.bullone.entrys['npt'].get()
		nlam=self.lamiera.values['nlam'].get()
		name_=self.name.get()
		return '''ac={acciaio}
bul={bullone}
geo={geometria}
lam={lamiera}
'''.format(**vars(self)) +'Giunto(geo,sol,bul,{npt},{nlam},lam,{name_})'''.format(**locals())

	
if __name__=='__main__':
	win=tk.Tk()
	win.title('TestFrame')
	win.style=tkinter.ttk.Style()
	win.style.theme_use('clam')
	win.geometry('{}x{}'.format(700,500))
	g=GiuntoGui(win)
	g.pack()

	def onTest():
		print(g)
		sys.stdout.flush()
	tkinter.ttk.Button(win,text='test',command=onTest).pack()
	from gueng.utils import Verifiche
	from io import StringIO as sio
	import sys
	curout=sys.stdout
	no=sio()
	try:
		sys.stdout=no
		v=Verifiche()
		v(1,1,2,'test',1)
		sys.stdout=curout
		print('ok')
	except e:
		sys.stdout=curout
		print(e)
		print('problemi')
	no.seek(0)
	st=no.getvalue()
	print(st)
	g.sol.text.insert('end',st)

		
	win.mainloop()

