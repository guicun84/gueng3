#-*-coding:latin-*-

from gueng.utils import Verifiche,Tabella
from pylab import *
from scipy import *
from scipy.linalg import norm
#from gueng.OdfDoc import *
from tempfile import mktemp
import os
from io import StringIO,BytesIO
import pandas as pa


class Giunto:
	out_libo=False #da impostare con il file su cui scrivere
	gamma_m=1.25
	ver_out='ptp'
	rtfLevelAdd=0 #valore da sommare al livello dei titoli per la relazione
	def __init__(self,geom,sol,bul,npt,nlam,l,Mr=eye(3),b=r_[0,0,0],name=None):
		'''geom= classe geometria del giunto
		sol=sollecitazioni da verificare Fx,Fy,Fz,Mx,My,Mz[,cond,elem]
		bul=bullone impiegato
		npt=piani di taglio
		nlam=numero lamiere da considerare
		l=lamiera
		Mr=matrice di orientamento del giunto nello spazio (da globale a locale (versori per righe)) 
		b=bracci per momenti parassiti da centro bullonatura a punto applicazione forze
		name= nome del giunto per output'
		out_libo: classe Odt, per scrivere tutti i risultati su un unico file
			impostare la variabile di classe, altrimenti definirne una per ogni
			giunto'''
		vars(self).update(locals())
		self.elabora_sollecitazioni()
		self.calcola()

	def elabora_sollecitazioni(self):
		'''ruota le sollecitazioni e calcola momenti parassiti'''
		self.sol_rot=zeros((self.sol.shape[0],8),dtype=float)
		self.Mpar=zeros((self.sol.shape[0],3),dtype=float)
		self.Tsd=zeros((self.sol.shape[0],7),dtype=float)
		#il vettore contiene in prima colonna il valore finale del calcolo,
		#1 2 tx ty  dovuta a forza nodale
		#3 4 tx ty  dovuta a momento
		#5 6 tx ty  dovuta a momento marassita 
		self.Nsd=zeros((self.sol.shape[0],6),dtype=float)
		#il vettore contiene in prima colonna il valore finale del calcolo,
		#1 trazione dovuta a forza nodale
		#2 3 trazione dovuta a momento x e y
		#4 5 trazione dovuta a momento marassita  x e y
		if self.sol.shape[1]==8:
			self.sol_rot[:,6:]= self.sol[:,6:]
		else:
			self.sol_rot[:,6:]= hstack((arange(self.sol.shape[0]).reshape((-1,1)),zeros((self.sol.shape[0],1))))
		for n,s in enumerate(self.sol):
			V=dot(self.Mr,s[:3])
			M=dot(self.Mr,s[3:6])
#			self.sol_rot=vstack([self.sol_rot,r_[V,M,s[6:]]])
			self.sol_rot[n,:6]=r_[V,M]
			self.Mpar[n]=cross(self.b,V) #momento parassita complessivo
			#sollecitazioni taglianti
			self.Tsd[n,1:3]=abs(V[:2]/self.geom.nbul/self.npt) #dovuto alle forze
			self.Tsd[n,3:5]=abs(M[2]/self.geom.Wz*self.geom.e/self.npt) #dovuto al momento torcente
			self.Tsd[n,5:7]=abs(self.Mpar[n,2]/self.geom.Wz*self.geom.e/self.npt) #dovuto al momento parassita torcente
			self.Tsd[n,0]=norm(self.Tsd[n,1:3]+self.Tsd[n,3:5]+self.Tsd[n,5:7])
			#sollecitazioni di trazione
			self.Nsd[n,1]=V[2]/self.geom.nbul
			if self.geom.Wx!=0:
				self.Nsd[n,2]=abs(M[0]/self.geom.Wx)
				self.Nsd[n,4]=abs(self.Mpar[n,0]/self.geom.Wx)
			if self.geom.Wy!=0:
				self.Nsd[n,3]=abs(M[1]/self.geom.Wy)
				self.Nsd[n,5]=abs(self.Mpar[n,1]/self.geom.Wy)
			self.Nsd[n,0]=self.Nsd[n,1:].sum()
			if self.Nsd[n,0]<0: self.Nsd[n,0]=0

	def calcola(self):
		self.ver=[Verifiche('Tagio bullone'),
			 Verifiche('Trazione bullone'),
			 Verifiche('Taglio-Trazione bullone'),
			 Verifiche('Rifollamento lamiera'),
			 Verifiche('Punzonamento lamiera'),
			 Verifiche('Taglio lamiera X'),
			 Verifiche('Taglio lamiera Y'),
			 Verifiche('Taglio lamiera XY')]

		self.tab=Tabella('cmb ide V[N] M[Nmm] Mpar[Nmm] Tsd1[N] Tsd2[N] Tsd3[N] Tsd[N] cu0 Nsd1[N] Nsd2[N] Nsd3[N]  Nsd4[N]  Nsd5[N] Nsd[n] cu1 cu2 Tsd_[N] cu3 cu4 cu5 cu6 cu7')


		Fvrd=self.bul.F_vrk/self.gamma_m
		Ftrd=self.bul.F_trk/self.gamma_m
		Fbrd=self.l.Fb_rk(self.bul)[0]/self.gamma_m
		Bprd=self.l.Bp_rk(self.bul)/self.gamma_m
		Vbtrkx,nMecX= self.l.Vbtrkx()
		if Vbtrkx is None: Vbtrdx=None
		else: Vbtrdx=Vbtrkx/self.gamma_m 
		Vbtrky,nMecY= self.l.Vbtrky()
		if Vbtrky is None: Vbtrdy=None
		else: Vbtrdy=Vbtrky/self.gamma_m 
#		self.sol_rot=zeros(8)
		for n,s in enumerate(self.sol_rot):
#			if s.size==6:s=r_[s,n,0]
			cmb=int(s[-2])
			e=int(s[-1])
			ide='Combinazione:%(cmb)d'%locals()
#
#			V=dot(self.Mr,s[:3])
#			M=dot(self.Mr,s[3:6])
##			self.sol_rot=vstack([self.sol_rot,r_[V,M,s[6:]]])
#			self.sol_rot[n,:6]=r[V,M]
#			Mpar=cross(self.b,V) #momento parassita complessivo
			#verifiche a taglio bullone
			V=s[:3]
			M=s[3:6]
			Mpar=self.Mpar[n]
#			Tsd1=abs(V[:2]/self.geom.nbul/self.npt)
#			Tsd2=abs(M[2]/self.geom.Wz*self.geom.e/self.npt)
#			Tsd3=abs(Mpar[2]/self.geom.Wz*self.geom.e/self.npt) #dovuto al momento parassita
#			Tsd=norm(Tsd1+Tsd2+Tsd3)
			Tsd=self.Tsd[n]
			Tsd,Tsd1,Tsd2,Tsd3=Tsd[0],Tsd[1:3],Tsd[3:5],Tsd[5:7]
			cu0=Tsd/Fvrd
			self.ver[0](Tsd,1,Fvrd,ide)
			#Verifica a trazione bullone:
#			Nsd1=V[2]/self.geom.nbul
#			if self.geom.Wx!=0:
#				Nsd2=abs(M[0]/self.geom.Wx)
#				Nsd4=abs(Mpar[0]/self.geom.Wx)
#			else:
#				Nsd2=0
#				Nsd4=0
#			if self.geom.Wy!=0:
#				Nsd3=abs(M[1]/self.geom.Wy)
#				Nsd5=abs(Mpar[1]/self.geom.Wy)
#			else:
#				Nsd3=0
#				Nsd5=0
#			Nsd=Nsd1+Nsd2+Nsd3+Nsd4+Nsd5
#			if Nsd<0: Nsd=0
			Nsd,Nsd1,Nsd2,Nsd3,Nsd4,Nsd5=self.Nsd[n]
			cu1=Nsd/Ftrd
			self.ver[1](Nsd,1,Ftrd,ide) #verifica a trazione
			cu2=cu0+cu1/1.4
			self.ver[2](cu2,1,1,ide)# verifica a taglio trazione
			#Verifica Lamiera:
			Tsd_=Tsd*self.npt/self.nlam
			cu3=Tsd_/Fbrd
			self.ver[3](Tsd_,1,Fbrd,ide)
			cu4=Nsd/Bprd
			self.ver[4](Nsd,1,Bprd,ide)
			#verifiche a taglio della lamiera:
			if Vbtrdx is not None:
				cu5=abs(abs(V[0])/self.nlam/Vbtrdx)
				self.ver[5](abs(V[0]/self.nlam),1,Vbtrdx,'{} meccanismo {}'.format(ide,nMecX))
			else: 
				cu5=None
				self.ver[5](0.,1,1,ide)
			if Vbtrdy is not None:
				cu6=abs(abs(V[1]/self.nlam)/Vbtrdy)
				self.ver[6](abs(V[1])/self.nlam,1,Vbtrdy,'{} meccanismo {}'.format(ide,nMecY))
				cu7=cu5+cu6
				self.ver[7](cu7,1,1,ide)
			else:
				cu6=None
				cu7=None
				self.ver[6](0.,1,1,ide)
				self.ver[7](0.,1,1,ide)
			self.tab()


	def __str__(self):
		st=[]
		if self.name:
			st.append(self.name+'\n')
		for v in self.ver:
			old_out=v.outType
			v.outType=self.ver_out
			st.append('%s'%v)
			v.outType=old_out
			st.append('')

		st.append('#'*60 + '\nTutto ok: ' + {True:'Si' , False:'No'}[all([v.all() for v in self.ver])])
		st_out= '\n'.join(st)

		if self.out_libo: self.to_out_libo() #scrive output su ODT
		if hasattr(self,'out_rtf'): self.to_out_rtf()
		return st_out	


	def to_out_md(self):
		bk_stformat=[i.stformat for i in self.ver]
		for i in self.ver:i.stformat='md'
		soglia_min=1e-3
		liv1,liv2,liv3=['\n\n'+'#'*(i+1+self.rtfLevelAdd) for i in range(3)]
		doc=StringIO()
		if self.name: doc.write('{liv1} {}  '.format(self.name,liv1=liv1))
		else: doc.write('{liv1} Bullonatura  '.format(liv1=liv1))
		doc.write('{liv2} Dati bullonatura  '.format(liv2=liv2))
		#caratteristiche bulloni
		doc.write('\n{}'.format(self.bul))
		dat='Geometria della Bullonatura  \nCoordinata\t'+'\t'.join(['bul{}'.format(i+1) for i in range(self.geom.nbul)])+'  \n'
		dat+='X[mm]\t'+'\t'.join(['%.4g'%i[0] for i in self.geom.pos]) +'  \n'
		dat+='  \nY[mm]\t'+'\t'.join(['%.4g'%i[1] for i in self.geom.pos])+'  \n'
		doc.write(dat)
		if hasattr (Giunto,'figure'):figure(self.figure.number)
		else: Giunto.figure=figure()
		clf()
		self.geom.plot()
		buf=BytesIO()
		savefig(buf,format='png')
		doc.write('''W~x~=%.4g mm , W~y~=%.4g mm , W~z~=%.4g mm  
Numero piani di taglio= %d , numero bulloni= %d  \n'''%(self.geom.Wx,self.geom.Wy,self.geom.Wz,self.npt,self.geom.pos.shape[0]))
		doc.write('''Bracci per momenti parassiti:  
b~x~=%.4g mm, b~y~=%.4g mm, b~z~=%.4g mm  \n'''%tuple(self.b))
		#caratteristiche lamiera		 
		doc.write('{liv2} Caratteristiche lamiera  \n'.format(liv2=liv2))
		doc.write('Numero lamiera considerate= {}  \n'.format(self.nlam))
		gam=self.l.mat.fyk/self.l.mat.fyd
		doc.write('&gamma;~s~= %.4g  \n'%gam)
		doc.write('''f~yk~= %(fyk).4g MPa --> f~yd~= %(fyd).4g MPa  
f~tk~= %(ftk).4g MPa  \n'''%vars(self.l.mat))
		doc.write('''Spessore lamiera t= %(t).4g mm  
Distanze tra i bulloni:  
e~1~= %(e1).4g mm , e~2~= %(e2).4g mm , p~1~= %(p1).4g mm , p~2~= %(p2).4g mm  \n'''%vars(self.l))
		doc.write('&gamma;~m~= %.4g  \n'%self.gamma_m)
		dat2=self.l.Fb_rk(self.bul)
		doc.write('''Resistenza a rifollamento:  
&alpha;=%(alpha).4g , &Oslash;~0~= %(d0).4g mm , k= %(k).4g  \n'''%dat2[1])
		doc.write('F~b,rk~= %.4g N --> F~b,rd~= %.4g  \n'''%(dat2[0],dat2[0]/self.gamma_m))
		dat2=self.l.Bp_rk(self.bul)
		doc.write('''Resistenza a punzonamento:  
B~p,rk~= %.4g N -->  B~p,rd~= %.4g N  \n'''%(dat2,dat2/self.gamma_m))

		doc.write('%s'%self.l.st_block_tearing)

		def esplica_azioni(dat):
			doc.write('{liv3} Azioni sul nodo  \n'.format(liv3=liv3))
			doc.write('Combinazione di carico %d  \n'%int(dat['cmb']))
			doc.write('F~x~=%.4g N , F~y~=%.4g N , F~z~=%.4g N  \n'%tuple(dat['V']))
			doc.write('M~x~=%.4g Nmm , M~y~=%.4g Nmm , M~z~=%.4g Nmm  \n'%tuple(dat['M'].tolist()))
			doc.write('''Momenti parassiti:  
M~x,par~=%.4g Nmm , M~y,par~=%.4g Nmm , M~z,par~=%.4g Nmm  \n'''%tuple(dat['Mpar']))

		def esplica_tagli(dat):
			heading='''Di seguito sono riportate le componenti in X e Y della forza di taglio agente sul bullone pi� sollecitato distinte per causa:  

|Componente	|Forze nodali	|M~z~|	M~z,par~|	Totale| 
|---------- |------------   |----|----------|---------|
'''
			temp=dat['Tsd1']+dat['Tsd2']+dat['Tsd3']
			dati='|F~x ~[N]|'+'|'.join(['%.4g'%i[0] for i in [dat[j] for j in 'Tsd1 Tsd2 Tsd3'.split()]]) + '|%.4g'%temp[0]+'|'
			dati+='  \n|F~y ~[N]|'+'|'.join(['%.4g'%i[1] for i in [dat[j] for j in 'Tsd1 Tsd2 Tsd3'.split()]]) + '|%.4g'%temp[1] +'|  \n'
			dati+='\n'
			doc.write(heading+dati)
#			doc.write(dati)
			doc.write('''Taglio totale sul bullone V~sd~=%.4g N  \n'''%dat['Tsd'])


		#verifica a taglio
		n=list(self.ver[0].max())
		n[1]=self.ver[0].strf(n[0])
		if self.ver[0].max()[1] >=soglia_min:
			doc.write('{liv2} Verifica a taglio bulloni  \n'.format(liv2=liv2))
			doc.write('Verifiche condotte su %d combinazioni di carico  \n'%len(self.ver[0]))
			dat=self.tab.values[n[0]]

			esplica_azioni(dat)


			doc.write('{liv3} Tagli sul bullone pi� sollecitato  \n'.format(liv3=liv3))
			esplica_tagli(dat)
			doc.write('Verifica: V~sd~ /F~vrd~ <1  \n')
			doc.write(n[1])

		def esplica_trazioni(dat):
			heading= '''Di seguito � riportata l'azione normale agente sul bullone pi� sollecitato distinta per causa:  

|Causa	|F~z~	|M~x~	|M~y~	|M~x,par~	|M~y,par~|  
|----	|----	|----	|---	|-------	|--------|  
'''

			dati='|Trazione [N]\t|'+'\t|'.join(['%.4g'%i for i in [dat[j] for j in 'Nsd1 Nsd2 Nsd3 Nsd4 Nsd5'.split()]]) +'|  \n'
			dati+='\n'
			doc.write(heading+dati)
#			doc.write(dati)
			N=sum([dat[i] for i in 'Nsd1 Nsd2 Nsd3 Nsd4 Nsd5'.split()])
			if N<0:
				doc.write('la sommatoria delle trazioni sul bullone � %.4g N, pertanto esso non risulta teso, si impone quindi N~sd~=0  \n'%N)
			doc.write('''Trazione Totale sul bullone pi� sollecitato:  
N~sd~=%.4g N  \n'''%dat['Nsd'])

		#Verifica a Trazione
		n2=list(self.ver[1].max())
		n2[1]=self.ver[1].strf(n2[0])
		n3=list(self.ver[2].max())
		n3[1]=self.ver[2].strf(n3[0])
		if self.ver[1].max()[1]>=soglia_min :
			doc.write('{liv2} Verifica a Trazione bullone  \n'.format(liv2=liv2))
			doc.write('Verifiche condotte su %d combinazioni di carico  \n'%len(self.ver[1]))
			dat=self.tab.values[n2[0]]
			if n2!=n:
				esplica_azioni(dat)
			else:
				doc.write('Combinazione %d  \n'%int(dat['cmb']))
				doc.write('Per le azioni sul nodo vedere verifica a Taglio  \n')


			doc.write('{liv3} Trazioni sul bullone pi� sollecitato  \n'.format(liv3=liv3))
			esplica_trazioni(dat)
			doc.write('Verifica: N~sd~/F~trd~<1:  \n')
			doc.write(n2[1])

			#Verifica a taglio trazione 
			dat=self.tab.values[n3[0]]
			
			doc.write('{liv2} Verifica a Taglio-Trazione  \n'.format(liv2=liv2))
			doc.write('Verifiche condotte su %d combinazioni di carico  \n'%len(self.ver[2]))
			if n3[0]==n[0]:
				doc.write('Combinazione %d  \n'%int(dat['cmb']))
				doc.write('Per le azioni sul nodo vedere verifica a Taglio  \n')
			elif n3[0]==n2[0]:
				doc.write('Combinazione %d  \n'%int(dat['cmb']))
				doc.write('Per le azioni sul nodo vedere verifica a Trazione  \n')
			else:
				esplica_azioni(dat)

			if n3[0]==n[0]:
				doc.write('Considero azione tagliante sul bullone pi� sollecitato della verifica a taglio  \n')
			else:
				doc.write('{liv3} Taglio su bullone pi� sollecitato  \n'.format(liv3=liv3))
				esplica_tagli(dat) 

			if n3[0]==n2[0]:
				doc.write('Considero azione normale sul bullone pi� sollecitato della verifica a trazione  \n')
			else:
				doc.write('{liv3} Trazione su bullone pi� sollecitato  \n'.format(liv3=liv3))
				esplica_trazioni(dat)
			doc.write('Verifica a taglio trazione:  V~sd~ / F~vrd~ + N~sd~ / (1.4 F~trd~ ) <=1  \n')
			doc.write(n3[1])

		#rifollamento lamiera
		n4=list(self.ver[3].max())
		n4[1]=self.ver[3].strf(n4[0])
		if self.ver[3].max()[1]>=soglia_min:
			doc.write('{liv2} Verifica a rifollamento lamiera  \n'.format(liv2=liv2))
			doc.write('Verifiche condotte su %d combinazioni di carico  \n'%len(self.ver[3]))
			dat=self.tab.values[n4[0]]
			if n4[0]==n[0]:
				doc.write('Combinazione %d  \n'%int(dat['cmb']))
				doc.write('considero sollecitazioni e azioni taglianti della verifica a taglio bullone  \n')
			else:
				esplica_azioni(dat)
				esplica_tagli(dat)
			doc.write('''Azione sulla lamiera in corrispondenza del bullone pi� sollecitato:  
V~sd~= %(Tsd_).4g N  \n'''%dat)
			doc.write('''Verifica a Rifollamento: V~sd~ / F~b,rd~ <=1  \n''')
			doc.write(n4[1])

		#punzonamento lamiera:
		n5=list(self.ver[4].max())
		n5[1]=self.ver[4].strf(n5[0])
		if self.ver[4].max()[1]>=soglia_min: 
			doc.write('{liv2} Verifica a punzonamento lamiera  \n'.format(liv2=liv2))
			doc.write('Verifiche condotte su %d combinazioni di carico  \n'%len(self.ver[4]))
			dat=self.tab.values[n5[0]]
			if n5[0]==n2[0]:
				doc.write('Combinazione %d  \n'%int(dat['cmb']))
				doc.write('Considero sollecitazione e azioni della verifica a trazione bullone  \n')
			else:
				esplica_azioni(dat)
				esplica_trazioni(dat)
			doc.write('''Azione sulla lamiera in corrispondenza del bullone pi� sollecitato:  
N~sd~= %(Nsd).4g N  \n'''%dat)
			doc.write('''Verifica a Rifollamento: N~sd ~/ B~p,rd~ <=1  \n''')
			doc.write(n5[1])
		
		#block-tearing direzione X
		n6=list(self.ver[5].max())
		n6[1]=self.ver[5].strf(n6[0])
		if self.ver[5].max()[1]>=soglia_min:
			dat=self.tab.values[n6[0]]
			if dat['cu5'] is not None:
				doc.write('{liv2} Verifica a Block-tearing in direzione X  \n'.format(liv2=liv2))
				doc.write('Verifiche condotte su %d combinazioni di carico  \n'%len(self.ver[5]))
				if n6[0] in [i[0] for i in (n,n2,n3,n4,n5)]: #controllare....
					doc.write('Combinazione %d  \n'%int(dat['cmb']))
					doc.write('Considero sollecitazione e azioni gi� mostrate nelle verifiche precedenti  \n')
				else:
					esplica_azioni(dat)
				doc.write('V~sd~ ={:.4f} N azione sollecitante block-tearing direzione X  \n'.format(dat['V'][0]))
				doc.write(n6[1])

		#block-tearing direzione Y
		n7=list(self.ver[6].max())
		n7[1]=self.ver[6].strf(n7[0])
		if self.ver[6].max()[1]>=soglia_min:
			dat=self.tab.values[n7[0]]
			if dat['cu6'] is not None:
				doc.write('{liv2} Verifica a Block-tearing in direzione Y  \n'.format(liv2=liv2))
				doc.write('Verifiche condotte su %d combinazioni di carico  \n'%len(self.ver[5]))
				if n7[0] in [i[0] for i in (n,n2,n3,n4,n5,n6)]: #controllare....
					doc.write('Combinazione %d  \n'%int(dat['cmb']))
					doc.write('Considero sollecitazione e azioni gia mostrate nelle verifiche precedenti  \n')
				else:
					esplica_azioni(dat)
				doc.write('V~sd~ ={:.4f} N azione sollecitante block-tearing direzione Y  \n'.format(dat['V'][0]))
	#			doc.write(Verifiche.uncolorize(n7[1]))
				doc.write(n7[1])
		
		#block-tearing direzione XY
		n8=list(self.ver[7].max())
		n8[1]=self.ver[7].strf(n8[0])
		if self.ver[7].max()[1]>=soglia_min:
			dat=self.tab.values[n8[0]]
			if dat['cu7'] is not None:
				doc.write('{liv2} Verifica a Block-tearing in direzione XY  \n'.format(liv2=liv2))
				doc.write('Verifiche condotte su %d combinazioni di carico  \n'%len(self.ver[5]))
				if n8[0] in [i[0] for i in (n,n2,n3,n4,n5,n6,n7)]: #controllare....
					doc.write('Combinazione %d  \n'%int(dat['cmb']))
					doc.write('Considero sollecitazione e azioni gi� mostrate nelle verifiche precedenti  \n')
				else:
					esplica_azioni(dat)
				doc.write('V~sd~ ={:.4f} N azione sollecitante block-tearing direzione XY  \n'.format(dat['V'][0]))
				doc.write(n8[1])
				
		for i,j in zip(bk_stformat,self.ver):
			j.stformat=i
		return doc.getvalue()

	def all(self):
		return all([v.all() for v in self.ver])

	def __bool__(self):
		return bool(self.all())



	
#	def to_out_libo(self):
#		dire=[i for i in os.listdir('.') if os.path.isdir(os.path.join(os.getcwd(),i))]
#		if 'images' not in dire: os.mkdir('images')
#		doc=self.out_libo
#		if self.name: doc.h1(self.name)
#		else: doc.h1('Bullonatura')
#		doc.h2('Dati bullonatura')
#		#caratteristiche bulloni
#		doc.p('''Bulloni M%d classe %.2g
#				 <#%%gamma_m#>=%.4g
#				 <#F_vrk#>= %.4g N <#rightarrow F_vrd#>=%.4g N
#				 <#F_trk#>=%.4g N <#rightarrow F_trd#>=%.4g N'''%(self.bul.d , self.bul.mat.cl , self.bul.gamma_m , self.bul.F_vrk , self.bul.F_vrk/self.bul.gamma_m , self.bul.F_trk , self.bul.F_trk/self.bul.gamma_m))
#		dat='X[mm]\t'+'\t'.join(['%.4g'%i[0] for i in self.geom.pos])
#		dat+='\nY[mm]\t'+'\t'.join(['%.4g'%i[1] for i in self.geom.pos])
#		doc.table(dat)
#		if hasattr (Giunto,'figure'):figure(self.figure.number)
#		else: Giunto.figure=figure()
#		clf()
#		self.geom.plot()
#		temp_img=mktemp(suffix='.png',dir='images')
#		savefig(temp_img)
#		doc.image(temp_img)
#		doc.p('''<#W_x#>=%.4g mm , <#W_y#>=%.4g mm , <#W_z#>=%.4g mm
#				 Numero piani di taglio= %d , numero bulloni= %d'''%(self.geom.Wx,self.geom.Wy,self.geom.Wz,self.npt,self.geom.pos.shape[0]))
#		doc.p('''Bracci per momenti parassiti:
#				 <#b_x#>=%.4g mm, <#b_y#>=%.4g mm, <#b_z#>=%.4g mm'''%tuple(self.b))
#		#caratteristiche lamiera		 
#		doc.h2('Caratteristiche lamiera')
#		doc.p('Numero lamiera considerate= {}'.format(self.nlam))
#		gam=self.l.mat.fyk/self.l.mat.fyd
#		doc.p('<#%%gamma_s#>= %.4g'%gam)
#		doc.p('''<#f_yk#>= %(fyk).4g MPa <#rightarrow f_yd#>= %(fyd).4g MPa
#			     <#f_tk#>= %(ftk).4g MPa'''%vars(self.l.mat))
#		doc.p('''Spessore lamiera <#t#>= %(t).4g mm
#				 Distanze tra i bulloni:
#		         <#e_1#>= %(e1).4g mm , <#e_2#>= %(e2).4g mm , <#p_1#>= %(p1).4g mm , <#p_2#>= %(p2).4g mm'''%vars(self.l))
#		doc.p('<#%%gamma_m#>= %.4g'%self.gamma_m)
#		dat2=self.l.Fb_rk(self.bul)
#		doc.p('''Resistenza a rifollamento:
#				  <#%%alpha#>=%(alpha).4g , <#%%varphi_0#>= %(d0).4g mm , <#k#>= %(k).4g'''%dat2[1])
#		doc.p('<#F_{b,rk}#>= %.4g N <#rightarrow F_{b,rd}#>= %.4g'''%(dat2[0],dat2[0]/self.gamma_m))
#		dat2=self.l.Bp_rk(self.bul)
#		doc.p('''Resistenza a punzonamento:
#		         <#B_{p,rk}#>= %.4g N <#rightarrow B_{p,rd}#>= %.4g N'''%(dat2,dat2/self.gamma_m))
#
#		doc.p('%s'%self.l.st_block_tearing)
#
#
#
#		#verifica a taglio
#		doc.h2('Verifica a taglio bulloni')
#		doc.p('Verifiche condotte su %d combinazioni di carico'%len(self.ver[0]))
#		n=list(self.ver[0].max())
#		n[1]=self.ver[0].strf(n[0])
#		dat=self.tab.values[n[0]]
#		def esplica_azioni(dat):
#			doc.h3('Azioni sul nodo')
#			doc.p('Combinazione di carico %d'%int(dat['cmb']))
#			doc.p('<#F_x#>=%.4g N , <#F_y#>=%.4g N , <#F_z#>=%.4g N'%tuple(dat['V']))
#			doc.p('<#M_x#>=%.4g Nmm , <#M_y#>=%.4g Nmm , <#M_z#>=%.4g Nmm'%tuple(dat['M'].tolist()))
#			doc.p('''Momenti parassiti:
#					 <#M_{x,par}#>=%.4g Nmm , <#M_{y,par}#>=%.4g Nmm , <#M_{z,par}#>=%.4g Nmm'''%tuple(dat['Mpar']))
#
#		esplica_azioni(dat)
#
#		def esplica_tagli(dat):
#			heading='Componente;Dovuto alle forze nodali;Dovuto a <#M_z#>;Dovuto a <#M_{z,par}#>; Totale sul bullone'.split(';')
#			temp=dat['Tsd1']+dat['Tsd2']+dat['Tsd3']
#			dati='<#F_x#> [N]\t'+'\t'.join(['%.4g'%i[0] for i in [dat[j] for j in 'Tsd1 Tsd2 Tsd3'.split()]]) + '\t%.4g'%temp[0]
#			dati+='\n<#F_y#> [N]\t'+'\t'.join(['%.4g'%i[1] for i in [dat[j] for j in 'Tsd1 Tsd2 Tsd3'.split()]]) + '\t%.4g'%temp[1]
#			doc.table(dati,heading)
#			doc.p('''Taglio totale sul bullone <#V_sd#>=%.4g N'''%dat['Tsd'])
#
#		doc.h3('Tagli sul bullone pi� sollecitato')
#		esplica_tagli(dat)
#		doc.p('Verifica: <#V_sd /F_vrd #><1')
#		doc.p(Verifiche.uncolorize(n[1]))
#
#		#Verifica a Trazione
#		doc.h2('Verifica a Trazione bullone')
#		doc.p('Verifiche condotte su %d combinazioni di carico'%len(self.ver[1]))
#		n2=list(self.ver[1].max())
#		n2[1]=self.ver[1].strf(n2[0])
#		dat=self.tab.values[n2[0]]
#		if n2!=n:
#			esplica_azioni(dat)
#		else:
#			doc.p('Combinazione %d'%int(dat['cmb']))
#			doc.p('Per le azioni sul nodo vedere verifica a Taglio')
##		def esplica_trazioni(dat):
##			doc.p('''Trazione dovuta a <#F_z#>= %.4g N
##					 Trazione dovuta a <#M_x#>= %.4g N
##					 Trazione dovuta a <#M_y#>= %.4g N
##					 Trazione dovuta a <#M_{x,par}#>= %.4g N
##					 Trazione dovuta a <#M_{y,par}#>= %.4g N'''%tuple([dat[i] for i in 'Nsd1 Nsd2 Nsd3 Nsd4 Nsd5'.split()]))
##			N=sum([dat[i] for i in 'Nsd1 Nsd2 Nsd3 Nsd4 Nsd5'.split()])
##			if N<0:
##				doc.p(u'la sommatoria delle trazioni sul bullone � %.4g N, pertanto esso non risulta teso, si impone quindi <#N_sd#>=0'%N)
##			doc.p(u'''Trazione Totale sul bullone pi� sollecitato:
##					  <#N_sd#>=%.4g N'''%dat['Nsd'])
##
#
#		def esplica_trazioni(dat):
#			heading= ' ;Dovuta a <#F_z#>;Dovuta a <#M_x#>;Dovuta a <#M_y#>;Dovuta a <#M_{x,par}#>;Dovuta a <#M_{y,par}#>'.split(';')
#			dati='Trazione [N]\t'+'\t'.join(['%.4g'%i for i in [dat[j] for j in 'Nsd1 Nsd2 Nsd3 Nsd4 Nsd5'.split()]])
#			doc.table(dati,heading)
#			N=sum([dat[i] for i in 'Nsd1 Nsd2 Nsd3 Nsd4 Nsd5'.split()])
#			if N<0:
#				doc.p('la sommatoria delle trazioni sul bullone � %.4g N, pertanto esso non risulta teso, si impone quindi <#N_sd#>=0'%N)
#			doc.p('''Trazione Totale sul bullone pi� sollecitato:
#					  <#N_sd#>=%.4g N'''%dat['Nsd'])
#
#		doc.h3('Trazioni sul bullone pi� sollecitato')
#		esplica_trazioni(dat)
#		doc.p('Verifica: <#N_sd/F_trd#><1:')
#		doc.p(Verifiche.uncolorize(n2[1]))
#
#		#Verifica a taglio trazione 
#		n3=list(self.ver[2].max())
#		n3[1]=self.ver[2].strf(n3[0])
#		dat=self.tab.values[n3[0]]
#		
#		doc.h2('Verifica a Taglio-Trazione')
#		doc.p('Verifiche condotte su %d combinazioni di carico'%len(self.ver[2]))
#		if n3[0]==n[0]:
#			doc.p('Combinazione %d'%int(dat['cmb']))
#			doc.p('Per le azioni sul nodo vedere verifica a Taglio')
#		elif n3[0]==n2[0]:
#			doc.p('Combinazione %d'%int(dat['cmb']))
#			doc.p('Per le azioni sul nodo vedere verifica a Trazione')
#		else:
#			esplica_azioni(dat)
#
#		if n3[0]==n[0]:
#			doc.p('Considero azione tagliante sul bullone pi� sollecitato della verifica a taglio')
#		else:
#			doc.h3('Taglio su bullone pi� sollecitato')
#			esplica_tagli(dat)
#
#		if n3[0]==n2[0]:
#			doc.p('Considero azione normale sul bullone pi� sollecitato della verifica a trazione')
#		else:
#			doc.h3('Trazione su bullone pi� sollecitato')
#			esplica_trazioni(dat)
#		doc.p('Verifica a taglio trazione: <# V_sd over F_vrd + N_sd over {1.4 F_trd} <=1 #>')
#		doc.p(Verifiche.uncolorize(n3[1]))
#
#		#rifollamento lamiera
#		doc.h2('Verifica a rifollamento lamiera')
#		doc.p('Verifiche condotte su %d combinazioni di carico'%len(self.ver[3]))
#		n4=list(self.ver[3].max())
#		n4[1]=self.ver[3].strf(n4[0])
#		dat=self.tab.values[n4[0]]
#		if n4[0]==n[0]:
#			doc.p('Combinazione %d'%int(dat['cmb']))
#			doc.p('considero sollecitazioni e azioni taglianti della verifica a taglio bullone')
#		else:
#			esplica_azioni(dat)
#			esplica_tagli(dat)
#		doc.p('''Azione sulla lamiera in corrispondenza del bullone pi� sollecitato:
#				 <#V_sd#>= %(Tsd_).4g N '''%dat)
#		doc.p('''Verifica a Rifollamento: <#V_sd/F_{b,rd}#> <=1''')
#		doc.p(Verifiche.uncolorize(n4[1]))
#
#		#punzonamento lamiera:
#		doc.h2('Verifica a punzonamento lamiera')
#		doc.p('Verifiche condotte su %d combinazioni di carico'%len(self.ver[4]))
#		n5=list(self.ver[4].max())
#		n5[1]=self.ver[4].strf(n5[0])
#		dat=self.tab.values[n5[0]]
#		if n5[0]==n2[0]:
#			doc.p('Combinazione %d'%int(dat['cmb']))
#			doc.p('Considero sollecitazione e azioni della verifica a trazione bullone')
#		else:
#			esplica_azioni(dat)
#			esplica_trazioni(dat)
#		doc.p('''Azione sulla lamiera in corrispondenza del bullone pi� sollecitato:
#				 <#N_sd#>= %(Nsd).4g N '''%dat)
#		doc.p('''Verifica a Rifollamento: <#N_sd/B_{p,rd}#> <=1''')
#		doc.p(Verifiche.uncolorize(n5[1]))
#		
#		#block-tearing direzione X
#		n6=list(self.ver[5].max())
#		n6[1]=self.ver[5].strf(n6[0])
#		dat=self.tab.values[n6[0]]
#		if dat['cu5'] is not None:
#			doc.h2('Verifica a Block-tearing in direzione X')
#			doc.p('Verifiche condotte su %d combinazioni di carico'%len(self.ver[5]))
#			if n6[0] in [i[0] for i in (n,n2,n3,n4,n5)]: #controllare....
#				doc.p('Combinazione %d'%int(dat['cmb']))
#				doc.p('Considero sollecitazione e azioni gi� mostrate nelle verifiche precedenti')
#			else:
#				esplica_azioni(dat)
#			doc.p('<#V_sd#> ={:.4f} N azione sollecitante block-tearing direzione X'.format(dat['V'][0]))
#			doc.p(Verifiche.uncolorize(n6[1]))
#
#		#block-tearing direzione Y
#		n7=list(self.ver[6].max())
#		n7[1]=self.ver[6].strf(n7[0])
#		dat=self.tab.values[n7[0]]
#		if dat['cu6'] is not None:
#			doc.h2('Verifica a Block-tearing in direzione Y')
#			doc.p('Verifiche condotte su %d combinazioni di carico'%len(self.ver[5]))
#			if n7[0] in [i[0] for i in (n,n2,n3,n4,n5,n6)]: #controllare....
#				doc.p('Combinazione %d'%int(dat['cmb']))
#				doc.p('Considero sollecitazione e azioni gia mostrate nelle verifiche precedenti')
#			else:
#				esplica_azioni(dat)
#			doc.p('<#V_sd#> ={:.4f} N azione sollecitante block-tearing direzione Y'.format(dat['V'][0]))
#			doc.p(Verifiche.uncolorize(n7[1]))
#		
#		#block-tearing direzione XY
#		n8=list(self.ver[7].max())
#		n8[1]=self.ver[7].strf(n8[0])
#		dat=self.tab.values[n8[0]]
#		if dat['cu7'] is not None:
#			doc.h2('Verifica a Block-tearing in direzione XY')
#			doc.p('Verifiche condotte su %d combinazioni di carico'%len(self.ver[5]))
#			if n8[0] in [i[0] for i in (n,n2,n3,n4,n5,n6,n7)]: #controllare....
#				doc.p('Combinazione %d'%int(dat['cmb']))
#				doc.p('Considero sollecitazione e azioni gi� mostrate nelle verifiche precedenti')
#			else:
#				esplica_azioni(dat)
#			doc.p('<#V_sd#> ={:.4f} N azione sollecitante block-tearing direzione XY'.format(dat['V'][0]))
#			doc.p(Verifiche.uncolorize(n7[1]))




