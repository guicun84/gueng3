#-*-coding:utf-8-*-

from scipy import *
from scipy.linalg import solve, norm

class punto:
	def __init__(self,coord):
		#il progetto è ambizioso e vorrebbe funzionare in 3d, quindi riporto tutti i punti in coordinate 3d anche quelli 2d con z=0'''
		if size(coord)==2:
			coord=r_[coord[0],coord[1],0.]
		self.co=coord.copy()
	
class retta:
	def __init__(self,i,j):
		if not  isinstance(i,punto):
			i=punto(i)
		if not isinstance(j,punto):
			j=punto(j)
		self.i=i
		self.j=j
		self.e1=self.j.co-self.i.co
		self.e2=cross(r_[0,0,1.],self.e1)
		if	not norm(self.e2) :
			self.e2=cross(r_[0,1,0],self.e1)
		self.e3=cross(self.e1,self.e2)
		self.l=norm(self.e1)
		self.l2=norm(self.e2)
		self.l3=norm(self.e3)

	def interseca_(self,r,aus=None):
		'''calcola la soluzione del sistema lineare che restituisce le lunghezze dei segmenti al putno di intersezione'''
		if issubclass(r.__class__,retta):	
			#controllo immediato del parallelismo delle rette
			a=cross(self.e1,r.e1)
			if not norm(a): return None #sono parallele
			if not aus:
				aus=a
			M=c_[self.e1,-r.e1,-aus]
			b=self.i.co-r.i.co
			x=solve(M,-b)
			return x
		else:
			raise TypeError
			
	def interseca(self,r,aus=None):
		'''calcola se isiste il punto di intersezione tra rette'''
		x=self.interseca_(r,aus)
		if x is None : return None
		if  x[2]: return None
		if not (x[0] in self and x[1] in r): return None
		return self.i.co+self.e1*x[0]
	
	def offset_(self,d,ax=None):
		'''calcola la posizione dei punti di definizione con un dato offset'''
		if not ax:
			ax=self.e2
			n=self.l2
		else:
			n=norm(ax)
		v=d*ax/n
		p1=self.i.co+v
		p2=self.j.co+v
		return p1,p2

	def offset(self,d,ax=None):
		'''restituisce un oggetto della stessa classe con il dato offset'''
		val=self.offset_(d,ax)
		return self.__class__(*val)

	def proietta_locale(self,p):
		'''calcola proiezioni punto p su sistema locale'''
		if issubclass(p.__class__,punto):
			v=p.co-self.i.co
		else:
			p=punto(p)
			v=p.co-self.i.co
		M=c_[self.e1,self.e2,self.e3]
		return solve(M,v)

	def contiene_punto(self,p):
		'''controlla che un punto appartenga alla retta'''
		x=self.proietta_locale(p)
		if not any(x[1:]):
			return self.contiene_distanza(x[0])
		else:
			return False

	def contiene_distanza(self,d):
		'''controlla che un punto ad una distanza d*norm(e1) dal punto di origine lungo e1 sia aoncora sulla retta'''
		#controllo ovvio su una retta, meno su semirette e segmenti...
		return True 

	def __contains__(self,val):
		if issubclass(val.__class__,punto) or val.__class__.__name__=='ndarray':
			return self.contiene_punto(val)
		else:
			return self.contiene_distanza(val)

#devo aggiungere il controllo legato al fatto che il punto di intersezione appartenga ad entrambe i segmenti nelle intersezioni, per ora  l'intersezione con segmenti e semirette è errato.

class semiretta(retta):
	def contiene_distanza(self,d):
		'''controlla che un punto ad una distanza d*norm(e1) dal punto di origine lungo e1 sia aoncora sulla retta'''
		return d>0


class segmento(retta):
	
	def contiene_distanza(self,d):
		'''controlla che un punto ad una distanza d*norm(e1) dal punto di origine lungo e1 sia aoncora sulla retta'''
		return 0<=d<=1


class pline:
	def __init__(self,*args):
		if all([issubclass(i.__class__,punto) for i in args]):
			self.carica_punti(args)
		elif all([issubclass(i.__class__,segmento) for i in args]):
			self.carica_segmenti(args)
		elif all([issubclass(i.__class__,ndarray) for i in args]):
			self.carica_vettori(args)

	def carica_punti(self,args):
		self.punti=args
		self.segmenti=[]
		for i in range(1,len(self.punti)):
			self.segmenti.append(segmento(self.punti[i-1],self.punti[i]))

	def carica_segmenti(self,args):
		self.segmenti=args
		punti=[]
		for i in self.segmenti:
			punti.append(i.i)
			punti.append(i.j)
		self.punti=[i for i in set(punti)]

	def carica_vettori(self,args):
		args=[punto(i) for i in args]
		self.carica_punti(args)

class area(pline):
	def carica_punti(self,args):
		pline.carica_punti(self,args)
		self.segmenti.append(segmento(self.punti[-1],self.punti[0]))

