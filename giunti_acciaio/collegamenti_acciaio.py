#-*-coding:utf-8-*-

from scipy import *
from scipy.linalg import norm
import gueng.materiali
from gueng.materiali import Acciaio_bul,Acciaio
from gueng.utils import Verifiche,rotsol
from gueng.profilario import Bullone as Bullone_vals
#import gueng.geometria as geom
#import geometria

set_printoptions(precision=4)

class Bullone():
	gamma_m=1.25
	gamma_m7=1.1 # per precarico attrito
#	Aree_nette={8:36.6,
#		10:58.,
#		12:84.,
#		14:115.,
#		16:157.,
#		18:192.,
#		20:245.,
#		22:303.,
#		24:353.,
#		27:459.,
#		30:561.}
	dati=Bullone_vals.dati.copy()
	Aree_nette=dict(list(zip(dati.m,dati.Ares)))

	_nu={'sab':.45,#per superficie sabbiata
	'gen':.3} #per altri casi
	def __init__(self,d,mat,nu=None,N=0):
		'''d= diametro bullone
		mat=materiale bullone es 8.8
		nu: coefficiente di attrito, None se a taglio
		N=Trazione del bullone oltre al precarico'''
		vars(self).update(locals())
		if not isinstance( mat, Acciaio_bul):
			self.mat=Acciaio_bul(mat)
		self.dati=Bullone.dati[Bullone.dati.m==d].iloc[0]
		self.An=self.dati.Ares
		if nu is not None:
			if type(nu)==str:
				self.nu=self._nu[nu]

	@property
	def F_vrk(self):
		'resistenza a taglio o ad attrito a seconda del falg iniziale'
		if self.nu is None: return self.F_vrk_T
		else: return self.F_vrk_at()

	@property
	def F_vrk_T(self):
		'resistenza a taglio'
		if self.mat.cl<=8.8:
			#gammam=1.25
			return .6*self.mat.ftk*self.An
		else:
			return .5*self.mat.ftk*self.An	

	
	def F_vrk_at(self,N=0):
		'N=trazione del bullone oltra al precarico'
		FpC=.7*self.An*self.mat.ftk/self.gamma_m7
		return self.nu*FpC
			
	@property
	def F_trk(self):
		'resistenza a trazione'
		return .9*self.mat.ftk*self.An
	
	@property 
	def passi(self):
		d=self.d
		return {'e1':1.2*d, 'e2':1.2*d, 'e3':2.2*d, 'e4':2.4*d}

	def __str__(self):
		st='''Bullone M{d} classe {mat.cl:.1f} ,  &gamma;~m2~={gamma_m:.2f}  
Area netta={An:.4g} mm^2^  \n'''.format(gamma_m=self.gamma_m,**vars(self))
		if self.nu is None:
			st+='F~vrk~: {:.4g} N --> F~vrd~: {:.4g} N resistenza  a taglio  \n' .format(*r_[1,1/self.gamma_m]*self.F_vrk)
		else:
			st+=''' &gamma;~m7~={g7:.2f}  
F~prC~= {:.4g} N Precarico del bullone  \n'''.format(.7*self.An*self.mat.ftk/self.gamma_m7,g7=self.gamma_m7)
			if self.N !=0:
				st+='N={:.4g} N trazione sul bullone '.format(self.N)
			st+='''&mu;= {nu:.4f} coefficiente attrito superfici  
F~vrk~: {0:.4g} N --> F~vrd~: {1:.4g} N resistenza per attrito  \n'''.format(*r_[1,1/self.gamma_m]*self.F_vrk , **vars(self))
		return st

class Bullonatura:
	gamma={'m2':1.25, 'm3':1.25 ,'m3u':1.25 ,'m3e':1.1, 'm6ser':1.,'m7':1.1}
	def __init__(self, profilo ,N,T,M,con,dist=r_[0,0],i=r_[0,0,0],j=r_[1,0,0]):
		'''con=connettore
		i= punto su cui eseguire il giunto
		j= punto per direzione elemento'''	
		vars(self).update(locals())
#		controllo che siano iterabili per le varie combinazioni.
		if '__iter__' not in dir(self.N):
			self.N=r_[N]
			self.T=r_[T]
			self.M=r_[M]
		elif type(N) in [list,tuple]:
			self.N=array(N)
			self.T=array(T)
			self.M=array(M)
		self.xg=0
		self.yg=0
		if type(dist) in (int,float):
			self.dist=r_[dist,0]

	def passi(self):
		out={'e1':1.2*self.con.d,
		     'e2':1.2*self.con.d,
			 'p1':2.2*self.con.d,
			 'p10':1.2*self.con.d,
			 'p1i':1.2*self.con.d,
			 'p2':2.4*self.con.d}
		return out
	
	def passi_(self):
		out={}
		for i in 'e1,e2,p1,p2'.split(','):
			out[i]=vars(self)[i]
		return out

	def bullonatura(self,n,m,e1,e2,p1,p2):
		'''n: numero di colonne T asse
m: numero di righe // asse
e1..p2 distanze tra i bulloni'''
		vars(self).update(locals())
		self.n_tot=n*m
		self.xg=self.e1+(self.n-1)/2*self.p1
		self.yg=(self.m-1)/2*self.p2
		#calcolo momento bullonatura
		self.w=0
		dm=0
		self.vers=r_[1,0]
		self.w_ax=self.p2*linspace(-(m-1)/2,(m-1)/2,m)
		if any(self.w_ax):
			self.w_ax=abs(self.n*sum(self.w_ax**2)/max(self.w_ax))
		else:
			self.w_ax==0
		if n>1 and m>1:
			for i in linspace(-(self.n-1)/2,(self.n-1)/2,self.n):
				for j in linspace(-(self.m-1)/2,(self.m-1)/2,self.m):
					vers=r_[i*self.p1,j*self.p2]
					d=norm(vers)
					if d>dm:
						dm=d
						self.vers=vers/d #salvo il vettore verso del bullone estreme
					self.w+=d**2
			self.w/=dm
			self.vers=r_[self.vers[1],self.vers[0]]
		if n==1 and m>1:
			for i in linspace(-(self.m-1)/2,(self.m-1)/2,self.m):
				vers=r_[0,i*self.p2]
				d=norm(vers)
				if d>dm:
					dm=d
					self.vers=vers/d #salvo il vettore verso del bullone estremo
				self.w+=d**2
			self.w/=dm
			self.vers=r_[self.vers[1],self.vers[0]]
		if n>1 and m==1:
			for i in linspace(-(self.n-1)/2,(self.n-1)/2,self.n):
				vers=r_[i*self.p1,0]
				d=norm(vers)
				if d>dm:
					dm=d
					self.vers=vers/d #salvo il vettore verso del bullone estremo
				self.w+=d**2
			self.w/=dm
			self.vers=r_[self.vers[1],self.vers[0]]

	def Ved(self):
		#calcolo momenti parassiti:
		MpT=abs(self.T*(self.dist[0]+self.xg))
		MpN=abs(self.N)*self.dist[1]
		segni=sign(self.M)
		segni=where(segni==0,ones_like(segni),segni)
		MpT*=segni
		MpN*=segni
		Med=self.M+MpT+MpN
		#componente dovuta allo sforzo normale sul singolo bullone
		N=self.N/self.n_tot 
		#componente dovuta al taglio:
		T=self.T/self.n_tot 
		#sollecitazioni composte:
		s=abs(c_[[r_[i,j] for i,j in zip(N,T)]])
		#calcolo componente dovuta a momento sul bullone più sollecitato:
		tm=Med/self.w 
		tm=r_[[abs(self.vers)*i for i in tm]]
		#sommo tutte le sollecitazioni
		s+=tm
		V=r_[[norm(i) for i in s]]
		out=locals()
		for i in ['self','i','j']:
			del out[i]
		return V,out
	

	def Ned(self):
		'''self.dist 3 componenti: 2 dx,dy,dz: dx e dy sono come nel caso di solo taglio, la terza componente è tra inizio bullonatura e piano medio trave collegata'''
		#calcolo trazioni sul bullone:
		N=self.N/self.n_tot
		MpT=abs(self.T*(self.dist[2])) #momento parassita per taglio
		MpN=abs(self.N)*self.dist[1]	#momento parassita per trazione
		segni=sign(self.M)
		segni=where(segni==0,ones_like(segni),segni)
		MpT*=segni
		MpN*=segni
		Med=self.M+MpT+MpN
		nm=Med/self.w_ax
		Ned=N+nm
		#valuto ora i tagli sui bulloni:
		T=self.T/self.n_tot
		MpTT=abs(self.T*(self.dist[0]+self.xg))
		s=c_[[r_[0,i] for i in T]]
		tm=MpTT/self.w
		s+=c_[[abs(self.vers)*i for i in tm]]
		Ved=r_[[norm(i) for i in s]]
		out=locals()
		for i in 'i,self'.split(','):
			del out[i]
		return c_[Ned,Ved],out
	

class Lamiera:
	__version__='1.1'
	'''secondo DM 14/1/2008'''
	gamma={'m2':1.25}
	def __init__(self,mat,t,e1=None,e2=None,p1=None,p2=None,lx=None,ly=None,bordi=[True,True]):
		'''in input
fyk=res a rottura lamiera
t= spessore
e1---p2 passi delle bullonature
lx,ly= lunghezze complessive per verifiche block-tearing lungo x e y, i possono identificare più percorsi da verificare e da indicare con 2 valori: la lunghezza complessiva del meccanismo resistente per taglio e quella resistente per trazione  [[l_tau1, l_fy1],[l_tau2,lfy2],...] se viene fornito un solo valore scalare si considera solo resistente a taglio (equivale a [[scalare,0]]
bordo: 'considera la posizione del bullone rispetto ai bordi
		in direzione parallela alla forza e perpendicolare alla forza'''
		vars(self).update(locals())
		if type(self.e1)==dict:	#posso passare tutti i passi in un dizionario
			vars(self).update(self.e1)
		#metto a posto lx e ly in modo da averle sempre come ((ltau,lfy),):
		if lx is not None:
			if not hasattr(lx,'__len__'):
				self.lx=((lx,0),)
			else:
				if not hasattr(lx[0],'__len__'):
					self.lx=(lx,)
		if ly is not None:
			if not hasattr(ly,'__len__'):
				self.ly=((ly,0),)
			else:
				if not hasattr(ly[0],'__len__'):
					self.ly=(ly,)

	def update_passi(self,e1,e2,p1=None,p2=None):
		vars(self).update(locals())

	def Fb_rk(self,bul,offset=1.,full_out=True):
		'''calcolo della resistenza a rifollamento della lamieraì
offset= diametro bullone - diametro foro
'''
		d0=bul.d+offset
		if self.bordi[0]:
			alpha=min([self.e1/(3*d0) , bul.mat.ftk/self.mat.ftk,1.])
		else:
			alpha=min([self.p1/(3*d0)-.025 , bul.mat.ftk/self.mat.ftk , 1.])
		if self.bordi[1]:
			k=min([2.8*self.e2/d0-1.7 ,2.5])			
		else:
			k=min([1.4*self.p2/d0-1.7 , 2.5])

		F_rk= k*alpha*self.mat.ftk*bul.d*self.t
		if full_out:
			d={}
			for i in ['alpha','k','d0']:
				d[i]=locals()[i]
			return F_rk,d
		else:
			return F_rk

	def Bp_rk(self,bul):
		return .6*pi*bul.d*self.t*self.mat.ftk

	@staticmethod
	def Vbtrk(ltau,lfy,t,fyk):
		'''calcola la resistenza caratteristica per block-tearing dati
		ltau= lunghezza totale resistetne per taglio
		lfy= lunghezza totale resistente per trazione
		t=spessore lamiera
		fyk= resistenza a trazione
		'''
		tauk=fyk/sqrt(3)
		return t*(ltau/1.5*tauk + lfy*fyk)


	def Vbtrkx(self):
		'resistenza minima per block-tearing lungo x'
		if self.lx is  None: return None,None
		if not hasattr(self.lx,'__len__'): self.lx=[[self.lx,0]] #considero solo taglio
		Vbtrks=r_[[self.Vbtrk(i[0],i[1],self.t,self.mat.ftk) for i in self.lx]]
		ind=argsort(Vbtrks)[0] #recupero meccanismo peggiore
		return Vbtrks[ind],ind
		
	def Vbtrky(self):
		'resistenza a taglio della lamiera'
		if self.ly is  None: return None,None
		if not hasattr(self.ly,'__len__'): self.ly=[[self.ly,0]] #considero solo taglio
		Vbtrks=r_[[self.Vbtrk(i[0],i[1],self.t,self.mat.ftk) for i in self.ly]]
		ind=argsort(Vbtrks)[0] #recupero meccanismo peggiore
		return Vbtrks[ind],ind

	@property
	def st_block_tearing(self):
		if self.lx is None and self.ly is None: return ''
		st='''Resistenze per Block-tearing  
s={t:.4f} mm spessore  
percorsi per block-Tearing in direzione X:  
l~&tau;~=lunghezza reagente per taglio  
l~fy~= lunghezza reagente per trazione  
  '''.format(**vars(self))
		def stblt(n,i):
			n=n
			ltau=i[0]
			lfy=i[1]
			vr=self.Vbtrk(ltau,lfy,self.t,self.mat.ftk)
			return '{n: 3d} | l~&tau;~= {ltau:.4f} mm |\t l~fy~= {lfy:.4f} mm |\t V~btrk~={vr:.4f} N  '.format(**locals())
		st+='\n'.join([stblt(n,i) for n,i in enumerate(self.lx)])
		st+='''
Vbrtkx ={:.4f} N resistenza per block tearing in direzione X  

percorsi per block-Tearing in direzione Y:  
'''.format(self.Vbtrkx()[0])
		st+='\n'.join([stblt(n,i) for n,i in enumerate(self.ly)])
		st+='''  
Vbrtkx ={:.4f} N resistenza per block tearing in direzione Y  '''.format(self.Vbtrky()[0])
		return st

###############################

class Saldatura:
	#nb: per ora non considero saldature per elementi con effetti torcenti.
	def __init__(self,N,T,M,cordoni,dist=r_[0,0],dire=r_[1,0,0]):
		vars(self).update(locals())
#		controllo che siano iterabili per le varie combinazioni.
		if '__iter__' not in dir(self.N):
			self.N=r_[N]
			self.T=r_[T]
			self.M=r_[M]
		elif type(N) in [list,tuple]:
			self.N=array(N)
			self.T=array(T)
			self.M=array(M)
		if type(dist) in (int,float):
			self.dist=r_[dist,0]

	def Ved(self):
		#calcolo momenti parassiti:
		MpT=abs(self.T)*self.dist[0]
		MpN=abs(self.N)*self.dist[1]
		segni=sign(self.M)
		segni=where(segni==0,ones_like(segni),segni)
		MpT*=segni
		MpN*=segni
		Med=self.M+MpT+MpN
		#componente dovuta allo sforzo normale sul singolo bullone
		N=self.N/self.n_tot 
		#componente dovuta al taglio:
		T=self.T/self.n_tot 
		#sollecitazioni composte:
		s=abs(c_[[r_[i,j] for i,j in zip(N,T)]])
		#calcolo componente dovuta a momento sul bullone più sollecitato:
		tm=Med/self.w 
		tm=r_[[abs(self.vers)*i for i in tm]]
		#sommo tutte le sollecitazioni
		s+=tm
		V=r_[[norm(i) for i in s]]
		out=locals()
		for i in ['self','i','j']:
			del out[i]
		return V,out
	
	def f_ed(self):
		pass
		
	

class Cordone:
	numero_verifiche=['4.2.78','4.2.79'] #numero delle verifiche nel dm
	def __init__(self,mat,a,l,par=None,perp=None,M=None,dist=r_[0,0],num=1,name='',an=90.,gamma_m2=1.25,sol=None,t=None,Mrot=None):
		'''a: Area di gola (non lato)		
par: forza parallela al cordone
perp=forza perpendicolare al cordone
M=momento agente sul cordone
NOTA: si possono non dichiarare si si usa sol
dist = bracci della componente parallela e perpendicolare della forza
num=numero di cordoni
an=angolo tra le piastre in gradi
sol=matrice con le sollecitazioni 
t=spessore piastra se presenti 2 cordoni
Mrot matrice di rotazione per passare al sistema dell cordone:versore 1 parallelo al cordone'''
		vars(self).update(locals())
		#controllo dati iniziali:
		self.testDati=all([a>0,l>0])
		#se ho gia passato le sollecitazioni principali agenti sul cordone
		if sol is None:
			if '__iter__' not in dir(self.par):
				self.par=r_[par]
				self.perp=r_[perp]
				self.M=r_[M]
			elif type(par) in [list,tuple]:
				self.par=array(par)
				self.perp=array(perp)
				self.M=array(M)
		else: #se ho passato vettore sol e devo calcolarle
			self.sol_rot=rotsol(Mrot,sol)
			self.par=self.sol_rot[:,0]
			self.perp=abs(self.sol_rot[:,1]) + abs(self.sol_rot[:,2])
			self.M=self.sol_rot[:,5]
			if self.t is not None:
				br=self.t+self.a
			else:
				br=self.a
			self.perp+=abs(self.sol_rot[:,4])/br
			self.perp+=abs(self.sol_rot[:,3])/(self.l*2/3)

		self.ver=(Verifiche('Verifica cordone 4.2.78 '+name),Verifiche('Verifica cordone 4.2.79 '+name))
		self.verif1Save=True
		self.verif2Save=True
		self.verif_1()
		self.verif_2()

	@property
	def lato(self):
		return self.a*2**.5
	
	def tau_par(self):
		par=abs(self.par)/self.l/self.a/self.num
		return par,{'par':par}

	def tau_perp(self):
		perp=abs(self.perp)/self.l/self.a/self.num
		M=abs(self.M)/self.l**2*6/self.a/self.num #componente dovuta al momento applicato alla saldatura
		m_par=abs(self.par*self.dist[0]+self.perp*self.dist[1]) #calcolo del momento parassita
		fm_par=abs(m_par)/self.l**2*6/self.a/self.num#calcolo della tensione dovuta al momento parassita
		out=locals()
		del out['self'] 
		return perp+M+fm_par,out

	def verif_0(self):
		'''verifica 4.2.75 sull'area di gola della saldatura effettiva
		assi:	1 asse saldatura
				2 asse forza perpendicolare
				3 asse forza tagliante
		'''
		print('metodo Cordone.verif_0 da completare!!!'.upper())
		an=deg2rad(self.an)/2
		lato=self.a*sqrt(2) #lato della saldatura
		ag=lato*sin((pi-an*2)/2) #area di gola effettiva
		Mr=r_[[[1,0,0],[0,cos(an),-sin(an)],[0,sin(an),cos(an)]]] #da globale a ruotato
		s21=self.tau_par()[0]/sqrt(2) #moltiplico per sqrt(2) x avere le tensioni sul lato
		s23=self.tau_perp()[0]/sqrt(2)
		#solo taglio par. e perp. su una faccia della saldatura
		T1=r_[[	[0   , 0   , -s21] ,
				[0   , 0   , -s23] ,
				[-s21 , -s23 , 0]]]
		#taglio par. e compressione perp su altra faccia saldatura
		T2=r_[[ [0,s21,0],
				[s21,s23,0],
				[0,0,0]]]
		#sommo stati tensionali e adeguo all'area di gola effettiva
		T=(T1+T2)*lato/ag*sin(an)

		n=Mr[:,2]
		f=dot(T,n)
		sn=dot(f,n)
		ta=f-sn*n
		s=sqrt(sn**2+3*norm(ta)**2)
	
		#calcolo coefficiente beta
		Betas=r_[.8,235,.85,275,.9,355,1,420].reshape(4,2)
		beta=Betas[[Betas[:,1]<=self.mat.fyk]][-1,0]
		cu=s/(self.mat.fyk/(beta/self.gamma_m2))*{True:1,False:-1}[self.testDati]
		ver=1/cu
		return locals()

	def verif_1(self):
		'''Verifica 4.2.78'''
		f=sqrt(self.tau_par()[0]**2+self.tau_perp()[0]**2)
		beta1=self.beta1
		fyk=self.mat.fyk
#		if not f:
#			ver=inf
#		else:
		ver=beta1*fyk/f*{True:1,False:-1}[self.testDati]
		cu=1/ver
		stout='''Verifica 4.2.78
&sigma;~id~={f} tensione ideale di riferimento
&beta;~1~={beta1:.4g}
f~yk~={fyk:.4g}
c~u~=&sigma;~id ~/ ($beta~1~* f~yk~)= {cu}'''.format(**locals())
		out=locals()
		del out['self']
		if self.verif1Save:
			self.verif1Save=False
			for n,i in enumerate(f):
				self.ver[0](i,1,beta1*fyk,'combinazione {}'.format(n))
		return cu,out

	def verif_2(self):
		'''Verifica 4.2.79'''
		f=self.tau_perp()[0]
		beta2=self.beta2
		fyk=self.mat.fyk
#		if not f:
#			ver=inf
#		else:
		ver=beta2*fyk/f*{True:1,False:-1}[self.testDati]
		cu=1/ver
		stout='''Verifica 4.2.79
&tau;~perp~={f} tensione di taglio perpendicolare
&beta;~2~={beta2:.4g}
f~yk~={fyk:.4g}
c~u~=&tau;~perp ~/ ($beta~2~* f~yk~)= {cu}'''.format(**locals())
		out=locals()
		del out['self']
		if self.verif2Save:
			self.verif2Save=False
			for n,i in enumerate(f):
				self.ver[1](i,1,beta2*fyk,'combinazione {}'.format(n))
		return cu,out
	@property	
	def cu1(self):
		return self.verif_1()[0]
	@property
	def cu2(self):
		return self.verif_2()[0]
	@property
	def cumax(self):
		return r_[self.cu1,self.cu2].max()

	def __bool__(self):
		return int(all(r_[self.cu1,self.cu2]<=1))

	def Fwrk(self): #criterio semplificato 4.2.76
		return self.a*self.l*self.mat.ftk/(sqrt(3)*self.betaw)
	@property	
	def betaw(self):
		fyk=self.mat.fyk
		if fyk<=235:
			return .8
		elif fyk<=275:
			return .85
		elif fyk<=355:
			return .9
		else:
			return 1
	@property			
	def beta1(self):
		f=self.mat.fyk
		if f<=235:
			return .85
		elif f<=355:
			return .70
		else: 
			return .62
	@property
	def beta2(self):
		f=self.mat.fyk
		if f<=235:
			return 1.
		elif f<=355:
			return .85
		else:
			return .75

	def __str__(self):
		#da migliorare...
		#dati cordone
		if self.name:st='{}\n'.format(self.name.capitalize())
		else: st=''
		st+='''l={l:.4g} lunghezza cordone ; n={num} numero di cordoni considerati
a={a:.4g} area di gola --> lato={lato:.3f}
f~yk~={fyk:.4g} MPa

Totale verifiche condotte: {nver}

verifica 4.2.78 [peggiore: {num1}]
F~par~={fpar1:.4g} N, F~perp~={fperp1:.4g} N, M~z~={M1:.4g}
&sigma;={sig1:.4g} MPa
&beta;~1~={beta1:.4g}
cu=&sigma;/(f~yk~ &beta;~1~)={cu1:.4g}
{v1}

verifica 4.2.79 [peggiore: {num2}]
F~perp~={fperp2:.4g} N
&sigma;~perp~={sig2:.4g} MPa
&beta;~2~={beta2:.4g}
cu=&sigma;~perp~/ (f~yk~ &beta;~2~)={cu2:.4g}
{v2}
'''
		l=self.l
		lato=self.lato
		a=self.a
		fyk=self.mat.fyk

		dt1=self.verif_1()[1]
		nver=dt1['cu'].size
		num1=where(dt1['cu']==dt1['cu'].max())[0][0]
		fpar1=self.par[num1]
		fperp1=self.perp[num1]
		M1=self.M[num1]
		sig1=dt1['f'][num1]
		beta1=self.beta1
		cu1=dt1['cu'][num1]

		dt2=self.verif_2()[1]
		num2=where(dt2['cu']==dt2['cu'].max())[0][0]
		fperp2=self.perp[num2]
		sig2=dt2['f'][num2]
		beta2=self.beta2
		cu2=dt2['cu'][num2]
#		test={True:'Tutto ok', False:'CONTROLLARE'}[all(r_[cu1,cu2]<=1)]
		v1=self.ver[0].strf(self.ver[0].max()[0])
		v2=self.ver[1].strf(self.ver[1].max()[0])
		num=self.num

		return st.format(**locals())

	def __bool__(self):
		c=r_[self.cu1,self.cu2]
		return int(all((c<=1) & (c>=0)))



#######################################################################
#
#class Giunto:
#	def __init__(self,collegamenti,piastra,prec=1,d=10):
#		'''prec=[n,'v','o'] rappresenta quale elemento deve proseguire fino al giunto: il numero, v per verticale h per orizzontale
#d= distanza da mantenere tra i profili'''
#		vars(self).update(locals())
#	def count_dist_joint(self):
#		#determino a quale distanza devono cominciare gli elementi:
#		p_cent=geom.punto(collegamenti[0].i)
#		p_assi=[geom.punto(i[j]) for i in collegamenti]
#		assi=[linea(p_cent,i) for i in p_ass]
#		#determino quale sia l'asse con precedenza fino al giunto.
#		prec=self.prec
#		if self.prec=='v':
#			self.prec=[n for n,i in enumerate(assi) if norm(cross(i.e1,r_[0,0,1]))==0]
#			if self.prec: self.prec=self.prec[0]
#			else: self.prec=None
#		elif self.prec=='o':
#			self.prec=[n for n,i in enumerate(assi) if dot(i.e1,r_[0,0,1])==0]
#			if self.prec: self.prec=self.prec[0]
#			else: self.prec=None
#		if  not self.prec:
#			raise TypeError ,'non sono stati trovati elementi %s'%{'v':'verticali','o':'orizzontali'}[prec]
#		ordine=[self.prec]+[i for i in xrange(len(assi)) if i!=self.prec] #sequenza di indici che iniziano dall'elemento con precedenza
#		#ordino ora gli elementi in modo antiorario rispetto al primo:
#		angoli=[[arccos(dot(norm(assi[ordine[0]].e1),norm(assi[i].e1))),i] for i in ordine[1:]]
#		angoli.sort()
#		ordine=[ordine[0]]+[i[1] for i in angoli]
#		
