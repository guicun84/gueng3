
import unittest
from . import Tstub
import imp
imp.reload(Tstub)
from .Tstub import *
from gueng.giunti_acciaio import Bullone
bu=Bullone(14,8.8)

class MyTests(unittest.TestCase):
	def test_Flangia(self):
		'''istanzio le vaire classi flangia\n'''
		f=Flangia(12,275/1.15,30,30,30)
		f2=FlangiaIrrigidita(12,275/1.15,30,30,30,30)
		self.assertTrue(True)

	def test_lunghezze_Flangia(self):
		'''controllo calcolo lunghezze flangia\n'''
		f=Flangia(12,275/1.15,30,30,30)
		f.interna()
		f.esterna()
		self.assertTrue(True)
	def test_lunghezze_Flangia_Irrigidita(self):
		'''controllo calcolo lunghezze flangia Irrigidita\n'''
		f=FlangiaIrrigidita(12,275/1.15,30,30,30,30)
		f.finaleVicinoIrrigidimento()
		f.finale()
		f.interna()
		f.vicinoIrrigidimento()
		self.assertTrue(True)
		self.assertTrue(True)

	def test_Ftrd_flangia(self):
		'controllo resistenza a trazione di fila'
		f=Flangia(12,275/1.15,30,30,30)
		f.Ftrd('interna',bu)
		f.Ftrd('esterna',bu)
		self.assertTrue(True)
	def test_Ftrd_flangia_Irrigidita(self):
		'controllo resistenza a trazione per piastra irrigidita'
		f=FlangiaIrrigidita(12,275/1.15,30,30,30,30)
		f.Ftrd('finaleVicinoIrrigidimento',bu)
		f.Ftrd('finale',bu)
		f.Ftrd('vicinoIrrigidimento',bu)
		f.Ftrd('interna',bu)
		self.assertTrue(True)

if __name__=='__main__': unittest.main(verbosity=2)
