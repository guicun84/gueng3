
from scipy import *
import pickle as pickle
from gueng.utils import Graph

g=Graph('alpha_T_stub.png')
if False:
	g.inizializza()
	print(vars(g))
else:
	g.carica(array([299.4, 1893]),array([1547, 1893]), array([296.7, 223.2]), 0.9, 1.4)

if False:
	pts={} 
	for i in 8,7,2*pi,6,5.5,5,4.75,4.5,4.45:
		print('curva {i:.2g}'.format(i=i))
		g.show()
		pts[i]=g.getPunti()
	with open('punti.pck','wb') as fi:
		pickle.dump(pts,fi,2)
		fi.close()
else:
	pts=pickle.load(open('punti.pck','rb'))
g.show()
for i,j in list(pts.items()):
	j=g.toImage(j)

	plot(j[:,0],j[:,1],'x')
	text(j[0,0],j[0,1],'{}'.format(i))
g.relim()
draw()

x=empty(0,float)
y=empty(0,float)
z=empty(0,float)
for i,j in list(pts.items()):
    x=r_[x,j[:,0]]
    y=r_[y,j[:,1]]
    z=r_[z,ones(j.shape[0])*i]
#from scipy.interpolate import SmoothBivariateSpline as intefun
#from scipy.interpolate import LSQBivariateSpline as intefun
from scipy.interpolate import LinearNDInterpolator as intefun
alpha=intefun(c_[x,y],z)
X,Y=meshgrid(r_[0:.9:100j],r_[0:1.4:100j])
pt=c_[X.flat,Y.flat]
Z=r_[[alpha(*i) for i in pt]].reshape(X.shape)
ptt=g.toImage(pt)
#g.show()
XX=ptt[:,0].reshape(X.shape)
YY=ptt[:,1].reshape(Y.shape)
k=list(pts.keys())
k.sort()
contour(XX,YY,Z,k)
colorbar()
contourf(XX,YY,Z,255,alpha=.4)
draw()
