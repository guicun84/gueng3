from scipy import r_,c_,ones,empty
from pickle import load
from scipy.interpolate import LinearNDInterpolator as inter
import os
dire=os.path.dirname(__file__)

x=empty(0,float)
y=x.copy()
z=x.copy()
pts=load(open(os.path.join(dire,'punti.pck'),'rb'),encoding='latin')
for i,j in list(pts.items()):
    x=r_[x,j[:,0]]
    y=r_[y,j[:,1]]
    z=r_[z,ones(j.shape[0])*i]
alpha=inter(c_[x,y],z)

del r_,c_,ones,load,inter,pts,x,y,z
