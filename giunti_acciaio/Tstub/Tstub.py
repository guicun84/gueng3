#-*-coding:latin-+-

from .alpha_t_stub import alpha
from scipy import pi,atleast_1d,r_,empty,append
import re

class TStub:
	pass

class Piastra:
	def __init__(self,t,fyd,m,e,e1=None,m2=None,p=None,funlrow=min):
		'''
		Piastra generica (specializzata nelle altre classi)
		t=spessore lamiera
		fyd=resistenza a snervamento
		m=distanza anima trave - bullone *1
		e=distanza bullone bordo perpendicolare asse trave *1
		e1=distanza bullone bordo parallelo asse trave
		m2= distanza bullone - irrigidimento parallelo asse trave
		p=passo bulloni
		funlrow=min|mean funzione da usare per calcolo delle lunghezze efficaci quando ci sono pi� valori di m
		
		*1 se ci sono piu di 2 bulloni su una fila, si possono dare le vaire distanze rispetto all'anima (da + interna a + esterna) in un array, la distanza e � intesa dell'ultimo bullone'''
		m=atleast_1d(m)
		self.__e1=e1
		self.__m2=m2
		self.__p=p
		if e1 is None:e1=max(r_[m,e])*100
		if m2 is None:m2=max(r_[m,e])*100
		if p is None:p=max(r_[m,e])*100
		vars(self).update(locals())
		if self.m2:
			self.l1=m/(e+m)
			self.l2=m2/(e+m)
			self.a=[alpha(i,j) for i,j in zip(self.l1,self.l2)]
			self.a=min(self.a)
		self.n=r_[[min((self.e,1.25*i)) for i in m]]

	def __str__(self):
		st='''s={t:.4g} mm spessore piastra
$$f_yd$$={fyd:.4g} MPa 
e={e:.4g} mm distanza bordo esterno - bullone interno
m={m} mm distanza bullone interno - inizio anima'''.format(**vars(self))
		if self.__e1 is not None: st+='\n$$e_1$$={:.4g} mm distanza bullone bordo trasversale'.format(self.__e1)
		if self.__m2 is not None: st+='\n$$m_2$$={:.4g} mm distanza bullone irrigidimento'.format(self.__m2)
		if self.__p is not None: st+='\np={:.4g} mm passo bullonatura'.format(self.__p)
		return st

	def Ftrd(self,rowname,bul,t2=None,fyd2=None,gruppo=False,dw=None,metodo=1):
		'''calcolo delle resistenze per Tstub:
		row=funzione per ottenere riga
		bul=Istanza Bullone con F_trk
		t2=spessore per contropiastra di rinforzo
		fyd2=resistenza a snervamento contropiastra
		gruppo=[True False] se true usa lungezza di gruppo
		dw=diametro rondella (necessario per metodo 2)
		metodo=1,2 metodo di calcolo usato'''

		nbul=2*self.m.size
		row=getattr(self,rowname)()
		vars(self).update(locals())
		Mp1={True:row.lg1,False:row.l1}[gruppo]*self.fyd*self.t**2/4
		Mp2={True:row.lg2,False:row.l2}[gruppo]*self.fyd*self.t**2/4
		if t2:
			if fyd2 is None: fyd2=self.fyd
			Mbp2={True:row.lg1,False:row.l1}[gruppo]*fyd2*t2**2/4
		else:
			Mbp2=0
		if metodo==1:
			Ft1rd=(4*Mp1 + 2*Mbp2)/self.m.min() #solo piastra
		else:
			ew=dw/4
			Ft1rd=((8*self.n.min()-2*ew)*Mp1 + 4*self.n.min()*Mbp2)/(2*self.n.min()*self.m.min()- ew*(self.m.min()+self.n.min()))
		#piastra e bulloni
		if self.m.size==1:
			Ft2rd=(2*Mp2 + self.n*nbul*bul.F_trk/bul.gamma_m)/(self.m+self.n)
		else:
		#piastre e bulloni generalizzato sul caso di pi� bulloni
			Ft2rd=empty(0,float)
			ds=max(self.m)+self.e-self.m #distanze da polo di rotazione
			ds.sort()
			ds=r_[0,ds]
			for num in range(ds.size-1):
				tin=int(num!=0)
				print('test iniziale',tin)
				d=ds[num+1:]-ds[num]+self.m.min()
				Ft2rd=append(Ft2rd,((2+tin*2)*Mp2 +2*tin*Mbp2 + 2*d.sum()*bul.F_trk/bul.gamma_m)/d.max())
			
		#solo bulloni
		Ft3rd=nbul*bul.F_trk/bul.gamma_m
		Ftrd=min(r_[Ft1rd,Ft2rd,Ft3rd])
		rowname_=re.sub('([A-Z])',' \\1',rowname).lower()
		gruppo_={True:'si',False:'no'}[gruppo]
		stout='''
{self}
tipo di linea: {rowname_}
{row}
considero in gruppo= {gruppo_}
momenti plastici piastra:
$$M_{{pl1}}$$={Mp1:.4g} Nmm
$$M_{{pl2}}$$={Mp2:.4g} Nmm
'''.format(t=self.t,**locals())
		if t2 :
			stout+='''spessore contropiastra={t2:.4g} mm , $$f_{{yd}}$$={fyd2:.4g} Mpa
momento plastico contropiastra:
$$M_{{bp2}}$$={Mbp2:.4g} Nmm
'''.format(**locals())
		stout+='''Utilizzo metodo {metodo} per il calcolo del modo di collasso 1'''.format(**locals())
		if metodo==2:
			stout+='''
diametro rondella={dw:.4g} mm'''.format(**locals())
		stout+='''
resistenze dei vari modi di collasso:
$$F_{{t1rd}}$$= {Ft1rd:.4g} N
$$F_{{t2rd}}$$= {Ft2rd} N
$$F_{{t3rd}}$$= {Ft3rd:.4g} N
Resistenza di progetto:
$$F_{{trd}}$$={Ftrd:.4g}N'''.format(**locals())
		return Ftrd,locals()

class Row:
	def __init__(self,lc,lnc,lgc,lgnc):
		'lunghezze efficaci per la riga di bullonatura'
		vars(self).update(locals())
	@property
	def l1(self):
		return min((self.lc,self.lnc))
	@property
	def l2(self):
		return self.lnc
	@property
	def lg1(self):
		return min((self.lgc,self.lgnc))
	@property
	def lg2(self):
		return self.lgnc
	def __str__(self):
		return '''Lunghezze efficaci per riga singola:
$$l_{{1}}$$= {l1:.4g} mm , $$l_{{2}}$$= {l2:.4g} mm
Lunghezze efficaci per riga considerata in gruppo:
$$l_{{g1}}$$= {lg1:.4g} mm , $$l_{{g2}}$$= {lg2:.4g} mm'''.format(l1=self.l1,l2=self.l2,lg1=self.lg1,lg2=self.lg2)
		
class Ala(Piastra):
	def __init__(self,t,fyd,m,e,e1=None,p=None,funlrow=min):
		'''
		ala trave/colonna non irrigidita
		t=spessore lamiera
		fyd=resistenza a snervamento
		m=distanza asse trave e bullone
		e=distanza bullone bordo perpendicolare asse trave
		e1=distanza bullone bordo parallelo asse trave
		m2= distanza bullone irrigidimento parallelo asse trave
		p=passo bulloni
		funlrow=min|mean funzione da usare per calcolo delle lunghezze efficaci quando ci sono pi� valori di m
		'''
		Piastra.__init__(self,t,fyd,m,e,e1,None,p,funlrow)

	def interna(self):
		lc=2*pi*self.funlrow(self.m)
		lnc=4*self.funlrow(self.m) +1.25*self.e
		lgc=2*self.p
		lgnc=self.p
		self.lunghezze=locals()
		return Row(lc,lnc,lgc,lgnc)

	def esterna(self):
		lc=min((2*pi*self.funlrow(self.m) , pi*self.funlrow(self.m)+2*self.e1))
		lnc=min((4*self.funlrow(self.m)+1.25*self.e , 2*self.funlrow(self.m)+.625*self.e + self.e1))
		lgc=min((pi*self.funlrow(self.m)+self.p , 2*self.e1+self.p))
		lgnc=min((2*self.funlrow(self.m) + .625*self.e +.5*self.p , self.e1 +.5*self.p))
		self.lunghezze=locals()
		return Row(lc,lnc,lgc,lgnc)
		
class AlaIrrigidita(Piastra):
	def __init__(self,t,fyd,m,e,e1=None,m2=None,p=None,funlrow=min):
		'''Ala trave/colonna irrigidita
		t=spessore lamiera
		fyd=resistenza a snernvamento
		m=distanza asse trave e bullone
		e=distanza bullone bordo perpendicolare asse trave
		e1=distanza bullone bordo parallelo asse trave
		m2= distanza bullone irrigidimento parallelo asse trave
		p=passo bulloni
		funlrow=min|mean funzione da usare per calcolo delle lunghezze efficaci quando ci sono pi� valori di m
		'''
		Piastra.__init__(self,t,fyd,m,e,e1,m2,p,funlrow)

	def vicinoIrrigidimento(self):
		lc=2*pi*self.funlrow(self.m)
		lnc=self.a*self.funlrow(self.m)
		lgc=pi*self.funlrow(self.m) + self.p
		lgnc=.5*self.p + self.a*self.funlrow(self.m) -(2*self.funlrow(self.m) + .625*self.e)
		self.lunghezze=locals()
		return Row(lc,lnc,lgc,lgnc)
	
	def interna(self):
		lc=2*pi*self.funlrow(self.m)
		lnc=4*self.funlrow(self.m) + 1.25*self.e
		lgc=2*self.p
		lgnc=self.p
		self.lunghezze=locals()
		return Row(lc,lnc,lgc,lgnc)

	def finale(self):
		lc=(2*pi*self.funlrow(self.m), pi*self.funlrow(self.m)+2*self.e1)
		lnc=(4*self.funlrow(self.m) +1.25*self.e , 2*self.funlrow(self.m) + .625*self.e + self.e1)
		lgc=(pi*self.funlrow(self.m)+self.p,2*self.e1+self.p)
		lgnc=(2*self.funlrow(self.m) + .625*self.e + 0.5*self.p, self.e1 + .5*self.p)
		self.lunghezze=locals()
		return Row(min(lc),min(lnc),min(lgc),min(lgnc))

	def finaleVicinoIrrigidimento(self):
		lc=(2*pi*self.funlrow(self.m), pi*self.funlrow(self.m) + 2*self.e1)
		lnc=self.e1 +self.a*self.funlrow(self.m)-(2*self.funlrow(self.m) + .625*self.e)
		lgc=0
		lgnc=0
		self.lunghezze=locals()
		return Row(max(lc),lnc,lgc,lgnc)

class PiastraFinale(Piastra):
	def __init__(self,t,fyd,m,e,e1=None,m2=None,bp=None,p=None,funlrow=min):
		'''Flangia di testa 
		t=spessore lamiera
		fyd=resistenza a snernvamento
		m=distanza asse trave e bullone [su norma � w]
		e=distanza bullone bordo perpendicolare asse trave
		e1=distanza bullone bordo parallelo asse trave
		m2= distanza bullone irrigidimento parallelo asse trave
		bp=larghezza totale piastra
		p=passo bulloni
		funlrow=min|mean funzione da usare per calcolo delle lunghezze efficaci quando ci sono pi� valori di m
		'''
		Piastra.__init__(self,t,fyd,m,e,e1,m2,p,funlrow)
		self.bp=bp
		self.n=min(r_[self.e1,1.25*self.m])

	def primaEsterna(self):
		lc=((2*pi*self.m2,
				pi*self.m2+2*self.funlrow(self.m),
				pi*self.m2+2*self.e))
		lnc=((4*self.m2 + 1.25*self.e1,
				self.e +2*self.m2 + .625*self.e1,
				.5*self.bp,
				self.funlrow(self.m)+2*self.m2+.625*self.e1))
		lgc=0
		lgnc=0
		self.lunghezze=locals()
		return Row(min(lc),min(lnc),lgc,lgnc)

	def primaInterna(self):
		lc=2*pi*self.funlrow(self.m)
		lnc=self.a*self.funlrow(self.m)
		lgc=pi*self.funlrow(self.m)+self.p
		lgnc=0.5*self.p+self.a*self.funlrow(self.m)-(2*self.funlrow(self.m)+.625*self.e)
		self.lunghezze=locals()
		return Row(lc,lnc,lgc,lgnc)

	def interna(self):
		lc=2*pi*self.funlrow(self.m)
		lnc=4*self.funlrow(self.m) + 1.25*self.e
		lgc=2*self.p
		lgnc=self.p
		self.lunghezze=locals()
		return Row(lc,lnc,lgc,lgnc)
	
	def esterna(self):
		lc=2*pi*self.funlrow(self.m)
		lnc=4*self.funlrow(self.m) + 1.25*self.e
		lgc=pi*self.funlrow(self.m) +self.p
		lgnc=2*self.funlrow(self.m) + .625*self.e + .5*self.p
		self.lunghezze=locals()
		return Row(lc,lnc,lgc,lgnc)

	def Ftrd(self,row,bul,t2=None,fyd2=None,gruppo=False,dw=None,metodo=1):
		'''calcolo resistenza, la ridefinisco xche se la fila � la prima esterna devo girare e1 con e ed m con m2 altrimenti no'''
		if row !='primaEsterna': return Piastra.Ftrd(self,row,bul,t2,fyd2,gruppo,dw,metodo)
		else:
			#devo girare e con e1 ed m con m2
			e=self.e
			e1=self.e1
			m=self.m
			m2=self.m2
			self.e=e1
			self.e1=e
			self.m2=min(self.m)
			self.m=r_[m2]
			dats=Piastra.Ftrd(self,row,bul,t2,fyd2,gruppo,metodo)
			self.e=e
			self.e1=e1
			self.m2=m2
			self.m=m
			return dats



	
