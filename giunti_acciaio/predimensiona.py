#-*-coding:latin-*-

from pylab import *
from gueng.profilario import prof
from gueng.giunti_acciaio import *
from scipy import *
from deap import base
from deap import creator
from deap import tools
from itertools import cycle
from scipy import *
from numpy.random import randint,uniform
from copy import copy as ccopy

class Predimensiona:
	def __init__(self,sol,cl,npt,nlam,lam,br,pr,npr,ac,tpr=None,cuamm=.90,dmax=None,dmin=.75,nmin=2,nmax=6,blam=None,nu=None,admitTest=[],nr=1,prep=None,pt=None,drelmax=20):
		'''sol=sollecitazioni,
		cl=classe bulloni
		npt=numero piani di taglio
		nlam=numero lamiere
		lam= spessore lamiera
		br=braccio forze
		pr=prifilo utilizzato
		ac=Acciao utilizzato
		tpr=spessore profilo mm
		cuamm= massimo coefficiente d'uso da raggiungere
		dmax,dmin=diametri massimi e minimi, se 0<dmin<1.: considero dmin come % di dimax
		nmin,nmax=numero minimo/massimo di bulloni,
		blam=altezza lamiera, se None considera quella del profilo
		nu: per giunti ad attrito: None giunto a taglio | 'sab|gen' superficie sabbiata o generica| valore numerico
		admitTest=lista aggiuntiva di test da eseguire sull'individuo devono restituire valore boleano
		nr=numero righe bullonatura
		prep=funzione di preprocessione dati: deve prendere in input l'individuo e ritornare un dizionario i cui valori sostituiranno quelli dell'istanza
		pt=None: posizione dei bulloni
		drelmax=massima distanza relativa tra bulloni'''
		vars(self).update(locals())	
		if self.dmax is None:
			if hasattr(pr,'phiy'):
				self.dmax=int(pr.phiy[1:])
			else:
				self.dmax=max(Bullone.Aree_nette.keys())
				if hasattr(pr,'phiy'):
					self.dmax=int(pr.phiy[1:])
		if 0<=dmin<=1:
			self.dmin=self.dmax*dmin
			self.dmin=list(Bullone.Aree_nette.keys())[max(where(r_[list(Bullone.Aree_nette.keys())]<=self.dmin)[0])]
		if blam is None:
			self.blam=pr.b
		if tpr is None:
			self.tpr=pr.t

	def genera(self):
		d=list(Bullone.Aree_nette.keys())
		d=[i for i in d if i>=self.dmin and i<=self.dmax]
		d=d[randint(len(d))]
		p=d*uniform(2.4,self.drelmax)
		p=ceil(p//5)*5
		p=max(2.4*d,p)
		n=randint(self.nmin,self.nmax)
		if self.nr!=1:
			pr=d*uniform(2.4,20)
			pr=(pr//5)*5
			pr=max(2.5*d,pr)
			return d,p,n,pr #diametro,passo,numero,passo_righe	
		else:
			return d,p,n #diametro,passo,numero	

	@staticmethod
	def muta(c,cc):
		'''c individuo di partenza
		cc individuo clonato'''
		cc[0]+=2*{True:1,False:-1}[rand()<=.5]*(rand()<.3)
		cc[1]+=5*{True:1,False:-1}[rand()<=.5]*(rand()<.3)
		cc[2]+=1*{True:1,False:-1}[rand()<=.5]*(rand()<.3)
		if len(cc)==4:
			cc[3]+=5*{True:1,False:-1}[rand()<=.5]*(rand()<.3)
		#controllo esistenza diametro, se no ne prendo uno valido
		if cc[0] not in list(Bullone.Aree_nette.keys()):
			d=sign(cc[0]-c[0])
			diametri=list(Bullone.Aree_nette.keys())
			if d>0:
				if cc[0]>diametri[-1]: cc[0]=diametri[-1]
				else: cc[0]=[i for i in list(Bullone.Aree_nette.keys()) if i>cc[0]][0]
			else:
				if cc[0]<diametri [0]: cc[0]=diametri[0]
				else: cc[0]=[i for i in diametri if i<cc[0]][-1]
		if cc[1]<cc[0]*2.4:
			cc[1]=5*((cc[0]*2.4)//5+1)
		if cc[2]<2: cc[2]=2
		if len(cc)==4:
			if cc[3]<cc[0]*2.4:
				cc[3]=5*((cc[0]*2.4)//5+1)
		return cc

	def verifica(self,x,fullout=True):
		#geneoro il giunto di verifica
		datprep={}
		if self.prep is not None:
			datprep.update(self.prep(x))
		if 'pos' in list(datprep.keys()):
			geo=Geometria(datprep['pos'])
		else:
			if len(x)==3:
				geo=Geometria(c_[arange(x[2])*x[1], zeros(x[2])])
			else:
				pos=empty((0,2),float)
				for i in range(self.nr):
					for j in range(int(x[2])):
						pos=vstack((pos,[j*x[1],i*x[3]]))
				geo=Geometria(pos)
		b=Bullone(x[0],self.cl,nu=self.nu)
		if self.lam*self.nlam<self.npr*self.tpr:
			t=self.lam
			nlam=self.nlam
		else:
			t=self.tpr
			nlam=self.npr

		if 'passi' in list(datprep.keys()):
			e1_,e2_,p1_,p2_=datprep[passi]
		else:
			if len(x)==3:
				e1_,e2_,p1_,p2_=x[1],x[1],x[1],x[1]
			else:
				e1_,e2_,p1_,p2_=x[1],x[1],x[1],x[3]
		p1= ceil((x[0]*2.4)/5)*5 #passo minimo
		if 'lx' in list(datprep.keys()):
			lx=datprep['lx']
		else:
			lx=	((2*((x[2]-1)*x[1] + p1) ,0),  #espulsione blocco dei bulloni
				((x[1]-x[0])*(x[2]-1),self.blam-x[0]),
				((x[1]-x[0])*(x[2]),self.blam/2) )
		if 'ly' in list(datprep.keys()):
			ly=datprep['ly']
		else:
			ly=self.blam-x[0]

		l=Lamiera(self.ac,t,e1_,e2_,p1_,p2_,lx=lx,ly=ly)
		if 'br' in list(datprep.keys()): br=datprep['br']
		else: br=self.br
		gi=Giunto(geo,self.sol,b,self.npt,nlam,l,b=br)
		#eseguo le verifiche
		cu=[i.max()[1] for i in gi.ver]
		cumax=max(cu)
		if self.pr is not None:
			Nrid=(self.pr.A-(x[0] +1)*self.tpr)*self.ac.ftk/1.25
			cuNrid=abs(self.sol[:,0]).max()/Nrid/self.npr
			cu.append(cuNrid)
		Nlam=(self.blam - x[0]-1)*self.lam*self.ac.ftk/1.25
		cuNlam=abs(self.sol[:,0]).max()/Nlam/self.nlam
		cu.append(cuNlam)
		cu=array(cu)
#		cu=[i for i in cu if i>1e-1]
		try: cumin=min([i for i in cu if i>=1e-1])
		except: cumin=.1
		cumed=mean(cu)
		return locals()

	def admit(self,x):
		#test di ammissibilitÓ delle caratteristiche delle bullonature
		tests=[	x[1]>=2.5*x[0], #passo minimo
				x[0]<=self.dmax ,#diametro massimo
				x[0]>=self.dmin ,#diametro massimo
				x[2]>=2 #numero minimo bulloni
				] + [i(x) for i in self.admitTest]
		return tests

	
	def fit(self,x):
		dat=self.verifica(x)
		admit=self.admit(x)
#		l=x[1]*x[2]
#		out=r_[cu[0],x[0]/int(self.pr.phiy[1:]),x[2]]
#		out=r_[cu,x[2]]
#		out=r_[dat['cumax'],dat['cumed'],x[2],x[1]*(x[2]-1)]
		if len(x)==3:
			out=r_[x[1]*(x[2]-1) , x[2] , dat['cumax']]#dat['cumax'] , dat['cumed']]
		else:
			out=r_[x[1]*(x[2]-1)*self.nr*x[3] , x[2] , dat['cumax']]#dat['cumax'] , dat['cumed']]
		if all(dat['cu']<=self.cuamm) and all(admit): return out
#		else: return r_[100,100,-100,-100]*out
		else: return r_[100,100,-100]*out
		
	def ricerca(self,verbose=False):
		creator.create('Fitness',base.Fitness,weights=[-1,-1,.5])
		creator.create('Individual',ndarray,fitness=creator.Fitness)

		tb=base.Toolbox()
		tb.register('genera',self.genera)
		tb.register('individual',tools.initIterate,creator.Individual,tb.genera)
		tb.register('select',tools.selNSGA2)
		tb.register('mate',tools.cxTwoPoint)
#		tb.register('mutate',tools.mutUniforInt)
		tb.register('eval',self.fit)

		npop=100
		nchild=75
		nnew=5
		nbest=npop-nchild
		pmate=.3
		pmut=.1
		ngen=100

		tmp=[0,10000]
		pop=[]
		bests=[]
		while len(pop)<npop:
			tmp[0]+=1
			i=tb.individual()
			if tmp[0]<tmp[1]:
				if all(self.admit(i)):
					pop.append(i)
			else:
				pop.append(i)

#		pop=[tb.individual() for i in xrange(200)]
		fit=list(map(tb.eval,pop))
		for i,f in zip(pop,fit): i.fitness.values=f
		self.calc=len(pop)
		self.pop0=[tb.clone(i) for i in pop]

		if verbose=='plot':
			figure()
			clf()
			show(0)
			nn=len(fit[0])
			li=[]
			for i in range(nn):
				subplot(nn,1,i+1)
				li.append(plot([],[])[0])
				grid(1)

		popstd=None
		nTestExit=0
		for g in range(ngen):
			child=[tb.clone(i) for i in pop]
			best=[tb.clone(i) for i in tb.select(pop,nbest)]
			best=[tb.clone(j) for i,j in zip(child,cycle(best))]
			for c1,c2 in zip(child,best):
				if rand()<=pmate:
					tb.mate(c1,c2)
					del c1.fitness.values
					del c2.fitness.values
			#eseguo delle mutazioni... a mano...
			for n,c in enumerate(child):
				if rand()<=pmut:
					cc=tb.clone(c)
					cc=self.muta(c,cc)
					if all(c==cc):continue
					del cc.fitness.values
					del c.fitness.values
					child[n]=cc
			child.extend([tb.individual() for i in range(nnew)])
			modif=[i for i in child if not i.fitness.valid]
			modif.extend([tb.individual() for i in range(nnew)])
			kk=len(modif)
			for i in modif:
				for j in pop: 
					if all(i==j):
						i.fitness.values=copy(j.fitness.values)
					break
			modif=[i for i in modif if not i.fitness.valid]
			self.calc+=len(modif)
			fit=list(map(tb.eval,modif))
			for i,f in zip(modif,fit):
				i.fitness.values=f
			pop.extend(modif)
			pop=tb.select(pop,npop)
			bests.append(tb.clone(tb.select(pop,1)[0]))
			if verbose:
				tbe=bests[-1]
				print(('gen {} | ' + '{:.0f} '*len(tbe) +'| ' + '{:.3f} '*len(tbe) +'| '+ '{:-3f} '*len(tbe.fitness.values) +'| {:.0f} {:.0f}').format(*r_[g,tbe,r_[pop].std(0),tbe.fitness.values,len(modif),self.calc]))
				if verbose=='plot':
					for i in range(nn):
						subplot(nn,1,i+1)
						li[i].set_xdata(append(li[i].get_xdata(), g))
						li[i].set_ydata(append(li[i].get_ydata(), tbe.fitness.values[i]))
						gca().relim()
						gca().autoscale_view(True,True,True)
						text(li[i].get_xdata().max(),li[i].get_ydata().min(),r'{2}$\phi${0}/{1}'.format(*tbe),rotation=90,fontsize=8,verticalalignment='bottom')
					gcf().canvas.draw()
					gcf().canvas.flush_events()
					draw()
					pause(.001)

			#in caso popolazione stagnante concludo la ricerca
			if popstd is None:
				popstd=r_[pop].std(0)
			else:
				tmp=r_[pop].std(0)
				if all(popstd == tmp): nTestExit+=1
				if nTestExit==3:break
				else: popstd=tmp
		best=tb.select(pop,1)[0]
		return best

if __name__=='__main__':
	sol_=r_[[[20e4,0,0,0,0,0,1,1]]]
	pr_=prof.l80x8
	from gueng.materiali import Acciaio
	ac_=Acciaio(275,430)
	pred=Predimensiona(sol_,8.8,2,1,10,r_[40,0,0],pr_,1,ac_)	
	b=pred.ricerca()
	print('{2}xM{0}/{1}'.format(*b))
	dat=pred.verifica(b)
	print(dat['gi'])
	print(dat['cuNrid'])
	


