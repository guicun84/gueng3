#-*- coding:utf-8-*-

import os,sqlite3

class Muratura:
	'''Valuta la resisetnza caratteristica della muratura nota
	tipo: elementi utilizzati (laterizio,pietra)
	fbk: resistenza dell'elemento a compressione
	malta: resistenza a compressine della malta'''
	dire=os.path.dirname(__file__)
	db=sqlite3.connect(os.path.join(dire,'murature.sqlite'))
	
	def __init__(self,tipo,fbk,malta):
		'''tipo=pietre/laterizzi
		fbk=resistenza a acompressione del bolcco
		malta=codice numerico che identifica la malta'''
		vars(self).update(locals())
		if self.tipo=='laterizio':
			self.tipo='laterizzi'
		if self.tipo=='pietra':
			self.tipo='pietre'

	@property
	def fk(self):
		if not self.tipo in ['laterizzi','pietre']:
			return 0
#		recupero i 4 punti piu vicini
		v1=self.db.execute('select fbk,malta,fk from muratura_%(tipo)s where malta <= :malta and fbk <=:fbk order by malta desc,fbk desc limit 1'%vars(self),vars(self)).fetchone()
		v2=self.db.execute('select fbk,malta,fk from muratura_%(tipo)s where malta >=:malta and fbk <=:fbk order by malta,fbk desc limit 1'%vars(self),vars(self)).fetchone()
		v3=self.db.execute('select fbk,malta,fk from muratura_%(tipo)s where malta <= :malta and fbk >=:fbk order by malta desc,fbk limit 1'%vars(self),vars(self)).fetchone()
		v4=self.db.execute('select fbk,malta,fk from muratura_%(tipo)s where malta >= :malta and fbk >=:fbk order by malta,fbk limit 1'%vars(self),vars(self)).fetchone()
#		print v1,v2,v3,v4
		if v3[0]==v1[0]:
			fk1=v1[2]
		else:
			fk1=v1[2]+ (v3[2]-v1[2]) / (v3[0]-v1[0]) * (self.fbk-v1[0]) 
		if v4[0]==v2[0]:
			fk2=v2[2]
		else:
			fk2=v2[2]+ (v4[2]-v2[2]) / (v4[0]-v2[0]) * (self.fbk-v2[0]) 
		if v2[1]==v1[1]:
			fk=fk1
		else:
			fk=fk1+ (fk2-fk1)/(v2[1]-v1[1])*(self.malta-v1[1])
		return fk
