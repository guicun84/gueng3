#-*-coding:latin-*-

from scipy import exp,polyval

class MuraturaIQM:
	__tabVerticale=dict(
		(('or_',dict(nr=0,pr=1,r=2)),
		('del_',dict(nr=0,pr=.5,r=1))),
		pd=dict(nr=0,pr=1,r=2),
		fel=dict(nr=0,pr=1.5,r=3),
		sg=dict(nr=0,pr=.5,r=1),
		ma=dict(nr=0,pr=.5,r=2),
		reel=dict(nr=0.3,pr=0.7,r=1)
	)
	__tabFuoriPiano=dict(
		(('or_',dict(nr=0,pr=1,r=2)),
		('del_',dict(nr=0,pr=.5,r=1))),
		pd=dict(nr=0,pr=1.5,r=3),
		fel=dict(nr=0,pr=1,r=2),
		sg=dict(nr=0,pr=.5,r=1),
		ma=dict(nr=0,pr=.5,r=1),
		reel=dict(nr=0.5,pr=0.7,r=1)
	)
	__tabNelPiano=dict(
		(('or_',dict(nr=0,pr=.5,r=1)),
		('del_',dict(nr=0,pr=.5,r=1))),
		pd=dict(nr=0,pr=1,r=2),
		fel=dict(nr=0,pr=1,r=2),
		sg=dict(nr=0,pr=1,r=2),
		ma=dict(nr=0,pr=1,r=2),
		reel=dict(nr=0.3,pr=0.7,r=1)
	)
	__r=dict(v=dict(nr=.2,pr=.6,r=1),
			fp=dict(nr=.1,pr=.1,r=1),
			np=dict(nr=.1,pr=.7,r=1))

	def __init__(self,blocco=None,or_=None,sg=None,pd=None,ma=None,fel=None,del_=None,reel=None,):
		'''
		blocco= nauturale|artificiale
		per ogni parametro seguente esprimere un giudizio di Regolare [1], Parzialmente regolare[2] Non reoglare [3]
		or_: orizzontalità dei filari
		sg: sfalsamento dei giunti
		ma:qualità della malta efficace contatto tra elementi 
		pd: presenza di diatoni
		fel: forma degli elementi
		del_: dimensioni degli elementi
		reel: resistenza degli elementi '''
		vars(self).update(locals())

	def __valtab(self,tab):
		dat=dict()
		for k in list(tab.keys()):
			dat[k]=tab[k][dict(list(zip((2,1,0),'r pr nr'.split())))[vars(self)[k]]]
		return dat
	@property
	def valVerticale(self):
		tab= MuraturaIQM.__tabVerticale
		return self.__valtab(tab)
	@property
	def valFuoriPiano(self):
		tab= MuraturaIQM.__tabFuoriPiano
		return self.__valtab(tab)
	@property
	def valNelPiano(self):
		tab= MuraturaIQM.__tabNelPiano
		return self.__valtab(tab)

	def __IQMgen(self,metodo):
		'''metodo=v,fp,np'''
		if metodo=='v':
			val=self.valVerticale
		elif metodo=='fp':
			val=self.valFuoriPiano
		elif metodo=='np':
			val=self.valNelPiano
		else: raise ValueError('metodo "{}" non supportato'.format(metodo))
		if self.blocco=='naturale': r=1
		else:
			r=MuraturaIQM.__r[metodo][dict(list(zip((2,1,0),'r pr nr'.split())))[self.ma]]
		return r*val['reel']*sum((v for k,v in list(val.items()) if k!='reel'))

	@property
	def IQMv(self):
		return self.__IQMgen('v')
	@property
	def IQMfp(self):
		return self.__IQMgen('fp')
	@property
	def IQMnp(self):
		return self.__IQMgen('np')

	def __catgen(self,tipo):
		lim=dict(v=[5,2.5],fp=[7,4],np=[5,3])
		if tipo=='v': iqm=self.IQMv
		elif tipo=='fp': iqm=self.IQMfp
		elif tipo=='np': iqm=self.IQMnp
		else: raise ValueError('tipo {} non conosciuto'.format(tipo))
		if iqm>=lim[tipo][0]: return'A'
		elif iqm>=lim[tipo][1]: return 'B'
		else:return 'C'
	@property
	def catv(self):
		return self.__catgen('v')
	@property
	def catfp(self):
		return self.__catgen('fp')
	@property
	def catnp(self):
		return self.__catgen('np')

	@staticmethod
	def fm_(val):
		'risultato in MPa'
		return 133.77*exp(0.2135*val)/100
	@property
	def fm(self):
		return self.fm_(self.IQMv)
	@staticmethod
	def tau0_(val):
		'risultato in MPa'
		return 2.5064*exp(0.2068*val)/100
	@property
	def tau0(self):
		return self.tau0_(self.IQMnp)
	@staticmethod
	def E_(val):
		'risultato in MPa'
		return 689.67*exp(0.1736*val)
	@property
	def E(self):
		return self.E_(self.IQMv)
	@staticmethod
	def G_(val):
		'risultato in MPa'
		return 242.62*exp(0.162*val)
	@property
	def G(self):
		return self.G_(self.IQMnp)
	
	@staticmethod
	def erfm(fm,tipo):
		if tipo=='naturale': return polyval([0.1047, .2649],fm)
		else:return polyval([0.2535, -.2076],fm)
	@staticmethod
	def ertau0(fm,tipo):
		if tipo=='naturale': return polyval([0.1102, .002963],fm)
		else:return polyval([0.1357,.001859],fm)
	@staticmethod
	def erE(fm,tipo):
		if tipo=='naturale': return polyval([0.1169, 61.53],fm)
		else:return polyval([0.2338, -127.1],fm)
	@staticmethod
	def erG(fm,tipo):
		if tipo=='naturale': return polyval([0.03802, 50.99],fm)
		else:return polyval([0.221, -25.38],fm)

	@property
	def fmmin(self):
		fm=self.fm
		return fm-self.erfm(fm,self.blocco)
	@property
	def fmmax(self):
		fm=self.fm
		return fm+self.erfm(fm,self.blocco)
	@property
	def tau0min(self):
		tau0=self.tau0
		return tau0-self.ertau0(tau0,self.blocco)
	@property
	def tau0max(self):
		tau0=self.tau0
		return tau0+self.ertau0(tau0,self.blocco)
	@property
	def Emin(self):
		E=self.E
		return E-self.erE(E,self.blocco)
	@property
	def Emax(self):
		E=self.E
		return E+self.erE(E,self.blocco)
	@property
	def Gmin(self):
		G=self.G
		return G-self.erG(G,self.blocco)
	@property
	def Gmax(self):
		G=self.G
		return G+self.erG(G,self.blocco)



	
	def __str__(self):
		st='Tipologia blocco:{}\n'.format(self.blocco)
		ks='or_ sg ma pd fel del_ reel'.split()
		desc=dict(list(zip(ks,[
						'Orizzontalità dei filari',
						'Sfalsamento dei giunti',
						'Qualità malta /contatto efficace',
						'Presenza di diatoni',
						'Forma degli elementi',
						'Dimensione degli elementi',
						'Resistenza elementi',
						])))
		valutazioni=dict(list(zip((2,1,0),('regolare','parzialmente regolare','non regolare'))))
		valv=self.valVerticale
		valfp=self.valFuoriPiano
		valnp=self.valNelPiano
		st+='''Punteggi Muratura  

|        Caratteristica          |     Valutazione     | v  | fp | np |
|--------------------------------|---------------------|----|----|----|
'''

		for k in ks: 
			if k=='or_':
				ts='|{:33}|{:21}|{:4.1f}|{:4.1f}|{:4.1f}|\n'.format(desc[k],valutazioni[vars(self)[k]],valv[k],valfp[k],valnp[k])
			else:
				ts='|{:32}|{:21}|{:4.1f}|{:4.1f}|{:4.1f}|\n'.format(desc[k],valutazioni[vars(self)[k]],valv[k],valfp[k],valnp[k])
			st+=ts
		st+='\n\n'
		st+='''Valori di IQM:
IQM~v~={:.2f} , IQM~fp~={:.2f} , IQM~np~={:.2f}

Caratteristiche muratura:
fm={:.3f} MPa , min={:.3f}MPa , max={:.3f} MPa  
&tau;~0~={:.3f} MPa , min={:.3f}MPa , max={:.3f} MPa  
E={:.3f} MPa , min={:.3f}MPa , max={:.3f} MPa  
G={:.3f} MPa  , min={:.3f}MPa , max={:.3f} MPa  
'''.format(self.IQMv,self.IQMfp,self.IQMnp,
	self.fm,self.fmmin,self.fmmax,
	self.tau0,self.tau0min,self.tau0max,
	self.E,self.Emin,self.Emax,
	self.G,self.Gmin,self.Gmax)


		return st

	def to_html(self):
		return '{}'.format(self).decode('utf8')

