#-*-coding:utf-8-*-

from pylab import *
from scipy import *
from scipy.interpolate import interp2d
from collections import namedtuple
from gueng.utils import Verifiche
from itertools import cycle

class Maschio:
	'''esegue le verifiche di un maschio murario'''
	lam=r_[0,5,10,15,20]
	m=r_[0,.5,1,1.5,2]
	lam,m=meshgrid(lam,m)
	PSI=r_[[[1,.74,.59,.44,.33],
	[.97,.71,.55,.39,.27],
	[.86,.61,.45,.27,.16],
	[.69,.48,.32,.17,0],
	[.53,.36,.23,0,0]]]
	__psi=interp2d(lam,m,PSI.T,kind='linear',fill_value=0)


	def __init__(self,l,h,t,mat,isolato=False,name=None):
		'''l= mm lunghezza paramento, usata per calcolo di fattore laterale di vincolo rho
		   h= mm altezza paramento
		   t= mm spessore paramento
		   mat=materiale
		   isolato=[True false] per considerare il paramento comunque isolato (rho=1)
		   
		   per le sollecitazioni occorre aggiungere alle seguenti proprietà:
		   self.N1=0 #carico del muro sovrastante
		   self.d1=0 #eccentricità di N1
		   self.N2=r_[0] #carico trasmesso dai solai [dx sx]
		   self.d2=r_[0] #eccentricità dei carichi [dx sx]
		   self.Mv=0  #momento dovuto alle azioni orizzontali
		   '''
		vars(self).update(locals())
		self.N1=0 #carico del muro sovrastante
		self.d1=0 #eccentricità di N1
		self.N2=r_[0] #carico trasmesso dai solai
		self.d2=r_[0] #eccentricità dei carichi
		self.Mv=0  #momento dovuto alle azioni orizzontali
		self.V=0 #taglio 
		self.gamma_rho=1.3 #gamma da applicare al calcolo del peso proprio
		self.ver=Verifiche(name)

	def rho_(self,l=None):
		'''fattore laterale di vincolo tab 5.4.IV'''
		if l is None: l=self.l
		r=1
		if self.isolato: return {'r':r}
		else:
			t=self.h/l
			if t<=.5: r= 1
			elif 0.5<t<=1: r= 3/2-t
			else: r= 1/(1+t**2)
		return locals()

	@property
	def rho(self):
		return self.rho_()['r']
	
	def psi(self,lam,m):
		'''coefficiente di riduzione della resistenza tab 4.5.III'''
		l2,m2=meshgrid(lam,m)
#		ps=array(map(self.__psi,zip(cycle([self]),cycle([lam]),m)))
		ps=r_[[self.__psi(lam,i) for i in m]]
		ps[l2<0]=0
		ps[l2>20]=0
		ps[m2<0]=0
		ps[m2>2]=0
		return ps
	@property
	def es1(self):
		'''ecc. risultante dei carichi trasmessi dai piani sup formula 4.5.8'''
		if self.N1 + self.N2.sum()==0: return 0
		return self.N1*self.d1/(self.N1+self.N2.sum())

	@property
	def es2(self): 
		'''ecc. reazioni di appoggio solai formula 4.5.8'''
		#devo fare una considerazione: le varie eccentricità sono sempre considerate postive,se ho i solai uno con ec. negativa e uno con ec. positiva devo far variare i segni per trovare il caso peggiore
		if self.N1+self.N2.sum()==0:return 0
		return abs((self.N2*self.d2).sum()/(self.N1+self.N2.sum()))
	@property
	def es(self): 
		'''eccentricità totale dei carichi formula 4.5.8'''
		return self.es1+self.es2
	@property
	def ea(self): 
		'''eccentricità  di esecuzione formula 4.5.9'''
		return self.h/200
	@property
	def ev(self): 
		'''ecc. dovuta azioni orizzontali formula 4.5.10'''
		if self.Mv==0: return 0
		return self.Mv/self.Ntot()
	@property
	def e1(self): 
		'''eccentricità per verifica sezione di estremità formula 4.5.11'''
		return abs(self.es)+self.ea
	@property
	def e2(self): 
		'''eccentricità per verifica sezione massimo M formula 4.5.11'''
		e2=self.e1/2+abs(self.ev)
		if e2<self.ea:
			e2=self.ea
		return e2
	def verificaEccentricita(self):
		'''esegue la verifica di eccentricità formula 4.5.12
		   NB= la formula dice chiaramente che e<t/3.... ma la verifica di eccentricità solitamente deve prevedere che e<t/6 per evitare trazione nella muratura (famoso terzo medio) ... io eseguo questa'''
		# ver=r_[self.e1,self.e2]/(self.t*.33) X( per stare nel terzo medio deve essere la metà... questa penso sia proprio sbagliata
#		ver=r_[self.e1,self.e2]/(self.t/6) # adotto questa che è il famoso terzo medio
		if 'verifica di eccentricità formula 4.5.12(corretta al 3 medio)' not in self.ver.desc:
			self.ver(r_[self.e1,self.e2],1,self.t/6,'verifica di eccentricità formula 4.5.12(corretta al 3 medio)')
		return self.ver.cu[-1]
	@property
	def m(self):
		'''coefficiente di eccentricità formula 4.5.7'''
		#primo per estremità secondo per momento massimo(base)
		return 6*r_[self.e1,self.e2]/self.t
	@property	
	def lam(self):
		'''snellezza convenzionale formula 4.5.1'''
		return self.h/self.t*self.rho
	
	def gk(self,h=0): 
		'''calcolo del peso proprio'''
		if hasattr (self.mat,'rho'):
			if h<self.h:
				return (self.h-h)*self.l*self.t*self.mat.rho/1e8
	def Ntot(self):
		'''calcola il carico ve=rticale totale'''
		return self.N1+self.N2.sum()+self.gk()	

	def Ntotd(self,gam=None):
		'''calcola il carico ve=rticale totale con coefficienti'''
		if gam is None:
			gam=self.gamma_rho
		return self.N1+self.N2.sum()+self.gk()*gam	

	def verificaPressoFlessione(self,h=None):
		'h= altezza per verifica su massima eccentricità, se numero complesso=%dell''altezza'
		if imag(h)!=0: h=self.h*imag(h)
		rho=self.rho
		lam=self.lam
		m=self.m
		psi=self.psi(lam,m).flatten()
		fdrid=(self.mat.fd*psi).flatten()
		#sezione di estremità:
		Ne=self.Ntotd()
		Me=Ne*self.e1
		A=self.l*self.t
		W=1/6*self.l*self.t**2
		sc_e=Ne/A+abs(Me)/W
		cu_e=sc_e/fdrid[0]
		if 'pressoflessione minima eccentricità' not in self.ver.desc:
			self.ver(sc_e,1,fdrid[0],'pressoflessione minima eccentricità')
		#sezione di	massimo M [media] :
		if h is not None:
			Nb=self.Ntotd()-(self.gk()-self.gk(h))*self.gamma_rho
		else:
			if self.Mv!=0 : #se ho momento applicato e non specifico altezza considero sempre sezione di base
				h=0
				Nb=self.Ntotd()
			else : #se non ho forze orizzontali considero sezione di mezzeria
				h=self.h*.5
				Nb=self.Ntotd()-(self.gk()-self.gk(h))*self.gamma_rho
		Mb=Nb*self.e2
		sc_b=Nb/A+abs(Mb)/W
		cu_b=sc_b/fdrid[1]
		if  'pressoflessione massima eccentricità' not in self.ver.desc:
			self.ver(sc_b,1,fdrid[1],'pressoflessione massima eccentricità')
		return r_[cu_e,cu_b],locals()

	def verificaCompressioneSemplice(self):
		N=self.Ntotd()
		A=self.l*self.t
		sc=N/A
		cu=sc/self.mat.fd
		if 'verifica a compressione semplice' not in self.ver.desc:
			self.ver(sc,1,self.mat.fd,'verifica a compressione semplice')
		return cu,locals()

	@property
	def fvk(self):
		return self.mat.tau0+0.4* (self.Ntot()/self.l/self.t)

	@property
	def fvd(self):
		return self.fvk/self.mat.gamma

	@property
	def Vrk(self):
		return self.l*self.t*self.fvk
	@property
	def Vrd(self):
		return self.l*self.t*self.fvd

	def verificaTaglio(self):
		Vsd=self.V #taglio di progetto
		sn=self.Ntot()/self.l/self.t #sigma di compressione
		fvk=self.fvk
		Vrk=self.Vrk
		Vrd=self.Vrd
		cu=Vsd/Vrd
		if 'verifica a taglio' not in self.ver.desc:
			self.ver(Vsd,1,Vrd,'verifica a taglio')
		return cu,locals()


	def verifica(self,h=.5j):
		'''h=altezza a cui condurre verifica per massimo momento flettente'''
		self._hver=h
		self.ver=Verifiche(self.name)
		eccentricita=self.verificaEccentricita()
		compressione=self.verificaCompressioneSemplice()
		pressoflessione=self.verificaPressoFlessione(h)
		taglio=self.verificaTaglio()
		test=all(r_[eccentricita<=.33,compressione[0]<=1,pressoflessione[0].flatten()<=1,taglio[0]<=1])
		return test,locals()

	def __bool__(self):
		flag=self.verifica()[0]
		if flag: return 1
		else: return 0

	def __str__(self):
		#imposto output array a 4 cifre 
		__oldopt__=get_printoptions()	
		set_printoptions(formatter={'float':lambda x:'{:.4g}'.format(x)})

		st='''### Dati maschio:
h={h:.1f}, s={t:.1f}, l={l:.1f} 

### Materiale
{mat[0]}
{mat_}

### Carichi
Carichi verticali sovrastanti:
{N1:.1f} N
eccentricità carichi sovrastanti: {d1:.1f}
carichi dei solai :
{N2} N
eccentricità carichi solai: {d2}
Momento ribaltante forze orizzontali:
{Mv:.1f} Nmm
'''.format(mat_=self.mat[1:],**vars(self))
		
		#verifica delle eccentricità:
		test_e=r_[self.e1,self.e2]<=self.t/6
#		ve1={True:'Verificato', False:'NON verificato'}[self.e1<=self.t/6]
#		ve2={True:'Verificato', False:'NON verificato'}[self.e2<=self.t/6]
		ve1=self.ver.st[0]
		st+='''

### Eccentricità:
es1={0:.1f} \t eccentricità totale carichi superiori, 
es2={1:.1f} \t eccentricità totale carichi solai
es={2:.1f} \t eccentricità totale dei carichi

ea={3:.1f} \t eccentricità di esecuzione
ev={4:.1f} \t eccentricità azioni orizzontali

### Eccentricità di progetto
e1={5:.1f} \t verifica sezione di estremità
e2={6:.1f} \t verifica sezione massimo M

### Verifica eccentricità
e &lt; s/6 &rarr; e&lt;{7:.1f}
{8}
'''.format(self.es1 , self.es2 , self.es , self.ea , self.ev , self.e1 , self.e2 , self.t/6 , ve1 )
#{5:.1f}<{7:.1f} : {8}
#{6:.1f}<{7:.1f} : {9}
#0                  , 1        , 2       , 3       , 4       , 5       , 6       , 7        , 8   , 9    #  ;-)
		#verifica a compressione
		test_c,dat=self.verificaCompressioneSemplice()
		test_c=test_c<=1
		
		st+='''
### Verifica a compressione semplice
ntotd= {N:.1f} carico totale
A= {A:.1f} area
&sigma;~c~= {sc:.4f} tensione di compressione
{}'''.format(self.ver.strf(1),**dat)
#{sc:.4f}/{:.4f} = {cu:.4f} --> {}

		#verifica a instabilità pressoflessionale
		test_f,dat=self.verificaPressoFlessione(self._hver)
		dat['tg']=self.verificaTaglio()
		dat['vertg']=self.ver.st[-1]
		test_f=test_f<=1
		st+='''
### Verifica a instabilità presso-flessionale
&rho;={rho:.4g} fattore laterale di vincolo
&lambda;= {lam:.4g} snellezza
i prossimi tre valori sono riferiti rispettivamente alla sezione di estremità e alla sezione dove è massimo il valore di Mv:
m={m} coefficiente di eccentricità
&psi;={psi} coefficiente di riduzione
f~drid~= {fdrid} resistenza di progetto ridotta 

Dati della sezione
A= {A:.1f}
W= {W:.1f}

sezione minima eccentricità:
N= {Ne:.1f} compressione totale
M= {Me:.1f} momento flettente
&sigma;~max~= {sc_e:.4f} tensione di compressione
cu=&sigma;~max~/f~drid~= {cu_e:.4f} #coefficiente d'uso
{}

sezione massima eccentricità
h={h:.1f} altezza della sezione di verifica
N={Nb:.1f} compressione totale
M={Mb:.1f} Momento totale
&sigma;~max~= {sc_b:.4f} tensione di compressione
cu={cu_b:.1f} coefficiente d'uso
{}

### Verifica a taglio
V~sd~={tg[1][Vsd]:.4g} N taglio di progetto
&sigma;~N~={tg[1][sn]:.4g} Mpa tensione di compressione setto
f~vk~={tg[1][fvk]:.4g} Mpa tensione resistente caratteristica a taglio
V~rk~={tg[1][Vrk]:.4g} N resistenza caratteristica setto
V~rd~={tg[1][Vrd]:.4g} N resistenza di progetto setto
{vertg}

### RESOCONTO
'''.format(*[self.ver.strf(i) for i in (2,3)],**dat)
		tot_test=r_[test_e,test_c,test_f]
		temp=self.ver.outType
		self.ver.outType='err'
		tst='\n'.join(('{}'.format(self.ver)).split('\n')[2:])

		st+=f'{self.ver}'
#		'''
#{}'''.format({True:'tutte le verifiche SUPERATE',False:'NON tutte le verifiche superate'}[all(tot_test)])
#		if not all(tot_test):
#			st+=('{}'.format(tot_test))
		set_printoptions(**__oldopt__)
		return st

	def __repr__(self):
		try:
			t,dat=self.verifica()
			test=r_[dat['eccentricita']/.33,
					dat['compressione'][0],
					dat['pressoflessione'][0]]
			st='''<<{0.__class__} instance at {1}>
	< {2}>>'''.format(self,id(self),test)
		except Exception as e:
			return f'problemi {e}'
		return st


if __name__=='__main__':
	materiale=namedtuple('materiale','fd rho')
	mat=materiale(2.3,1500e-6)
	mas=maschio(1.4,5,.15,mat)
	mas.N1=10
	mas.N2=r_[12]
	print(mas.verificaCompressioneSemplice())
