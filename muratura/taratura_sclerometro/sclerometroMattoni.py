
from os.path import dirname,join as djoin
from scipy import *
from scipy.interpolate import interp1d
import pickle

dire=dirname(__file__)
finame=djoin(dire,'punti_scalati.pck')
print(finame)
with open(finame) as fi:
	punti=pickle.load(fi)

sclerometroMattoniPieniInterp=interp1d(punti[:,0],punti[:,1],kind='cubic')


with open(djoin(dire,'parametri.pck')) as fi:
	var=pickle.load(fi)

def sclerometroMattoniPieni(x):
	'''x=valore letto da sclerometro
	ritoran il valore di resistenza associato alla cura di taratura per mattoni pieni:'''
	a,b,c=var
	return a*exp(b*x)+c
	

