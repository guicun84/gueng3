
import pickle
from pylab import *
from scipy import *
from scipy.interpolate import interp1d
from os.path import dirname,join as djoin


dire=dirname(__file__)

figure(1)
clf()
show(0)
im=imread(djoin(dire,'taratura_sclerometro.png'))
imshow(im)
draw()
num=30
pts=[]
xl=xlim()
yl=ylim()
if False:
	for i in range(num):
		pt=ginput()[0]
		plot(pt[0],pt[1],'xr')
		xlim(xl)
		ylim(yl)
		draw()
		pts.append(pt)
	with open(djoin(dire,'punto.pck','w')) as fi:
		pickle.dump(pts,fi)
else:
	with open (djoin(dire,'punto.pck'),'r') as fi:
		pts=pickle.load(fi)
	plot(r_[pts][:,0],r_[pts][:,1],'xr')
	draw()
pts=r_[pts]
ind=argsort(pts[:,0])
pts=pts[ind]
plot(pts[:,0],pts[:,1])
xlim(xl)
ylim(yl)
draw()
scalax=(70-20)/(1060.27-89.45)
x0=20
scalay=50/(842.145-19.4316)
y0=0
pts[:,0]=(pts[:,0]-89.45)*scalax+x0
pts[:,1]=(842.145-pts[:,1])*scalay+y0
with open (djoin(dire,'punti_scalati.pck'),'w') as fi:
	pickle.dump(pts,fi)
taraturaSclerometro=interp1d(pts[:,0],pts[:,1],kind='cubic')
figure(2)
clf()
show(0)
plot(pts[:,0],pts[:,1],'xr')
x=linspace(pts[:,0].min(),pts[:,0].max(),1000)
plot(x,taraturaSclerometro(x),'-')
grid(1)
draw()

def fun(var,x):
		a,b,c=var
		return a*exp(b*x)+c


if False:
	from scipy.optimize import leastsq

	
	var=leastsq(lambda x:fun(x,pts[:,0])-pts[:,1],r_[9,.01,-10.])[0]
	with open(djoin(dire,'parametri.pck'),'w') as fi:
		pickle.dump(var,fi)
else:
	with open(djoin(dire,'parametri.pck'),'r') as fi:
		var=pickle.load(fi)
	

plot(x,fun(var,x),'g')
draw()
def sclerometroMattoniPieni(x):
	'''x=valore letto da sclerometro
	ritoran il valore di resistenza associato alla cura di taratura per mattoni pieni:'''
	return fun(var,x)



