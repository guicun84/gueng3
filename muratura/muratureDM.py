#-*-coding:latin-*-

import pandas as pa
from scipy import *
import os,re
from warnings import warn
basedir=os.path.dirname(__file__)

class ErroreImplementazione(Exception):
	def __str__(self):
		return'Funzionalitą ancora da implementare'

class WarningImplementazione(UserWarning):
	def __init__(self,value):
		self.value=value
	def __str__(self):
		return '''funzionalitą in sviluppo 
{}'''.format(self.value)


class MuraturaDM:
	muratureDM=pa.read_csv(os.path.join(basedir,'muratura_tab21_MPA-Kg.csv'),skiprows=1,index_col=0,sep=' +\| +',engine='python')

	correzioniDM=pa.read_csv(os.path.join(basedir,'muratura_tab22.txt'),skiprows=1,sep=' +\| +',engine='python')
	#maschera per modificare i parametri di deformabilitą in base alle corezioni del caso

	Emasks=[list(correzioniDM.keys())[1:],[1,1,0,0,1,1,1]]
	Emasks=pa.DataFrame(Emasks).T
	Emasks.columns='Tipo coefficiente'.split()

	str_level=1 #numero di capitolo da aggiungere a quello standard

	
#	masks=r_[	1,1,1,1,1,1,1,1,
#				1,1,1,1,1,1,1,1,
#				1,1,1,1,0,0,0,0,
#				1,1,1,1,0,0,0,0,
#				1,1,1,1,1,1,1,1,
#				1,1,1,1,1,1,1,1].reshape(6,8)
#
	def __init__(self,tipo,LC=1,correzioni=None,gamma=3):
		'''tipo= indice del tipo di muratura base
		   LC=Livello di conoscenza
		   correzione=lista di indici delle correzioni '''
		self.muratura0=self.muratureDM.iloc[tipo].copy()
		self.muratura0['gamma']=gamma
		self.muratura1=self.muratura0.copy()
		self.LC=LC
		if tipo <6 and correzioni is not None:
			self.coefs=self.correzioniDM.iloc[tipo][1:][correzioni]
			self.Emask=self.Emasks.iloc[correzioni]['coefficiente']
			self.coefR=prod(self.coefs)
			self.coefE=self.coefs.values*self.Emask.values
			self.coefE[self.coefE==0]=1
			self.coefE=prod(self.coefE)
			self.muratura1['fm_min']*=self.coefR	
			self.muratura1['fm_max']*=self.coefR	
			self.muratura1['tau0_min']*=self.coefR
			self.muratura1['tau0_max']*=self.coefR
			self.muratura1['E_min']*=self.coefE
			self.muratura1['E_max']*=self.coefE
			self.muratura1['G_min']*=self.coefE
			self.muratura1['G_max']*=self.coefE
			self.muratura1['tipo']+=' + '+ ' + '.join(self.coefs.index) 
			

		else:
			self.coefs=None
			self.coef=None
			self.mask=None

		self.muratura=pa.Series()
		self.muratura['tipo']=self.muratura1.tipo
		{1:self._LC1 , 2:self._LC2,3:self._LC3}[LC]()
		self.muratura['gamma']=self.muratura1.gamma
		self.muratura['rho']=self.muratura1.rho
		self.muratura['fd']=self.muratura.fm/self.muratura.gamma/self.muratura.FC
		self.muratura['tau0d']=self.muratura.tau0/self.muratura.gamma/self.muratura.FC
		self.muratura=self.muratura['tipo fm tau0 FC gamma fd tau0d E G rho'.split()]
	@staticmethod
	def tipi():
		'restituisce la lista delle tipologie murarie'
		return MuraturaDM.muratureDM.tipo

	@staticmethod
	def correzioni():
		'restituisce la lista delle correzioni possibili'
		return pa.DataFrame(MuraturaDM.correzioniDM.T.index[1:],columns=['correzioni'])

	def _LC1(self):
		self.muratura['tipo']+=' + LC1'
		self.muratura['FC']=1.35
		self.muratura['fm']=self.muratura1.fm_min
		self.muratura['tau0']=self.muratura1.tau0_min
		self.muratura['E']=mean(self.muratura1['E_max E_min'.split()])
		self.muratura['G']=mean(self.muratura1['G_max G_min'.split()])

	def _LC2(self):
		warn(WarningImplementazione('''funzionalitą da completare:
attualmente prende i valori delle tabelle, ma in base alle prove fatte tali valori possono essere aumentati o ridotti, questa parte decisionale manca completamente'''))
		self.muratura['tipo']+= ' + LC2'
		self.muratura['FC']=1.25
		self.muratura['fm']=mean(self.muratura1['fm_min fmax'.split()])
		self.muratura['tau0']=mean(self.muratura1['tau0_min tau0_max'.split()])
		self.muratura['E']=mean(self.muratura1['E_max E_min'.split()])
		self.muratura['G']=mean(self.muratura1['G_max G_min'.split()])

	def _LC3(self):
		raise ErroreImplementazione('implementazione non svolta')

	def __str__(self):
		k='#'*self.str_level
		def series2string(series,muratura=True):
			'''muratura=True per trattare in modo diverso la prima riga'''
			if series is None: return ''
			if muratura:
				st='''{}'''.format(series[1:])
				st=st.split('\n')
				st='  \n'.join(st[:-1])
				st=re.sub('_(.*?)\ ','~\\1~ ',st)
				st=re.sub(r'([^ ])\s+(\d)',r'\1 = \2',st)
				st=st.replace('tau','&tau;')
				st=st.replace('rho','&rho;')
				st=st.replace('gamma','&gamma;')
				st=re.sub('(f.*\d)',r'\1 MPa',st)
				st=re.sub('(&t.*\d)',r'\1 MPa',st)
				st=re.sub('(E.*\d)',r'\1 MPa',st)
				st=re.sub('(G.*\d)',r'\1 MPa',st)
				st=re.sub('(&r.*\d)',r'\1 Kg/m^3^',st)
				st=re.sub(r'(\d\.\d{4})\d+',r'\1',st)
				st='{}  \n{}'.format(series[0],st)
			else: 
				st='{}'.format(series)
				st=st.split('\n')
				st='  \n'.join(st[:-1])
				st=re.sub(r'([^ ])\s+(\d)',r'\1 : \2',st)

			return st
			
			

		if self.LC in (1,2):
			dat=['muratura0','muratura1','muratura']
			dat=dict(list(zip(dat,[series2string(vars(self)[i]) for i in dat])))
			dat['coefficienti']=series2string(self.coefs,False)
			return'''
{k}# Muratura di base

di seguito sono riportati i valori caratteristici della muratura di base considerata  
{muratura0}

{k}# Muratura corretta

di seguito sono riportati i valori caratteristici della muratura prendendo in considerazione i valori correttivi da applicare ad essa  
coefficienti:
{coefficienti}

{muratura1}

{k}# Muratura di progetto
di seguito si riportano i valori della muratura di progetto considerati nei calcoli  
{muratura}'''.format(k=k,**dat)
		
		else:
			return '''
{k}# Muratura di progetto

di seguito si riportano i valori della muratura di progetto considerati nei calcoli  
{muratura}'''.format(k=k,muratura=series2string(self.muratura))

	def __repr__(self):
		return'<{} instance at {}>\n{}'.format(self.__class__,id(self),self.muratura.tipo)
