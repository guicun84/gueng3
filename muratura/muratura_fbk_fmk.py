
from scipy import *
from scipy.interpolate import interp2d

fbks=r_[2,3,5,7.5,10,15,20,30,40]
fmks=r_[2.5,5,10,15]
fks=r_[ [[1.2 , 1.2  , 1.2 , 1.2],
		[2     , 2.2  , 2.2 , 2.],
		[3     , 3.3  , 3.4 , 3.5],
		[3.5   , 4.1  , 4.5 , 5  ],
		[4.1   , 4.7  , 5.3 , 6.2],
		[5.1   , 6    , 6.7 , 8.],
		[6.1   , 7    , 8   , 9.],
		[7.2   , 8.   , 10  , 12 ],
		[5     , 10.5 , 12  , 14.3]]]

print(fbks.shape)
print(fmks.shape)
print(fks.shape)
fkMattoniInterp=interp2d(fbks,fmks,fks.T)
