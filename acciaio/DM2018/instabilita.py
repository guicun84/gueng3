
from scipy import *
from scipy.linalg import norm
from scipy.optimize import minimize
import re

class Chi(float):
	'''pr=dati sezione in pandas.Series
	li=luce libera
	fyk=resistenza materiale
	direz=direzione per verifica y|z|u|v|min
	alpha=curva di imperfezione Tab 4.2.VIII
	E=modulo elastico'''
	alpha=dict(a0=0.13,a=0.21,b=0.34,c=0.49,d=0.76)
	def __new__(cls,pr,li,fyk,direz='min',alpha=None,E=210e3):
		'''pr=dati sezione in pandas.Series
		li=luce libera
		fyk=resistenza materiale
		direz=direzione per verifica y|z|u|v|min
		alpha=curva di imperfezione Tab 4.2.VIII
		E=modulo elastico'''
		i=float.__new__(cls,0)
		i.__init__(pr,li,fyk,direz,alpha,E,True)
		j=float.__new__(cls,i.chi)
		j.__init__(pr,li,fyk,direz,alpha,E)
		vars(j).update(vars(i))
		del i
		return j


	def __init__(self,pr,li,fyk,direz='min',alpha=None,E=210e3,runcalc=False):
		if runcalc:
			vars(self).update(locals())
			del self.runcalc
			self.Ncr=self.calcolaNcr()
			self.lam=self.calcolaLam()
			self.alpha=self.cercaCurva()
			self.phi=self.calcolaPhi()
			self.chi=self.calcolaChi()
	
	def __str__(self):
		st=f'Ncr={self.Ncr:.5g} , lam={self.lam:.5g}, alpha={self.alpha}, phi={self.phi:.5g} chi={self.chi:.5g}'
		return st

	def calcolaChi(self):
		return min((1,1/(self.phi+sqrt(self.phi**2-self.lam**2)))) 
	
	def calcolaPhi(self):
		self.alpha_=Chi.alpha[self.alpha]
		return 0.5*(1+self.alpha_*(self.lam-0.2)+self.lam**2)
	
	def calcolaLam(self):
		return sqrt(self.pr.A*self.fyk/self.Ncr)

	def calcolaNcr(self):
		if self.direz is None or self.direz=='min':
			Jmin=min((self.pr.Jy,self.pr.Jz))
			if 'Ju' in list(self.pr.keys()):
				Jmin=min((Jmin,self.pr.Ju,self.pr.Jv))
		else:
			if self.direz=='z':
				Jmin=self.pr.Jz
			elif self.direz=='y':
				Jmin=self.pr.Jy
			elif self.direz=='u':
				Jmin=self.pr.Ju
			elif self.direz=='v':
				Jmin=self.pr.Jv
		return pi**2*self.E*Jmin/self.li**2

	def cercaCurva(self):
		if self.alpha in 'a b c d'.split():
			return self.alpha

		if self.alpha is None:
			if 'name' in list(self.self.pr.keys()):
				n=self.self.pr['name']
				if  any([i in n for i in ['IPE','HE','IPN']]):
					self.alpha='laminata'
				elif 'L' in n:
					self.alpha='L'
				elif 'UPN' in n:
					self.alpha='UT'
			else: return 'd'
		if self.alpha == 'laminata'  :
			#sezione laminate dop sim
			tf=self.self.pr.tf
			r=self.self.pr.h/self.pr.b
			if r>1.2:
				if tf<40:
					if self.direz in ['min','z']:
						return 'b'
					else:
						return 'a'
				else:
					if self.direz in ['min','z']:
						return 'c'
					else:
						return 'b'
			else:
				if tf<=100:
					if self.direz in ['min','z']:
						return 'c'
					else:
						return 'b'
				else:
					return 'd'
			
		elif self.alpha=='I saldata':
			if self.self.pr.tf<40:
				if self.direz in ['min','z']:
					return 'c'
				else: return 'b'
			else:
				if self.direz in ['min','z']:
					return 'd'
				else: return 'c'

		elif self.alpha=='cava' :
			return 'a'

		elif self.alpha=='cava freddo':
			return 'c'

		elif self.alpha=='scatolare saldata':
			return 'b'
		elif self.alpha=='scatolare saldata spessa':
			return 'c'

		elif self.alpha=='UT':
			return 'c'

		elif self.alpha=='L':
			return 'b'

		else: 
			raise IOError('sezione sconosciuta')

class ChiLT(float):
	def __new__(cls,pr,Lcr,fyk,M=r_[0.],tipo='laminata',grafico=None,info=None,x=None,E=210e3,G=81e3,plastica=False,direz='z'):
		i=float.__new__(cls,0)
		i.__init__(pr,Lcr,fyk,M,tipo,grafico,info,x,E,G,plastica,direz,runcalc=True)
		j=float.__new__(cls,i.chiLT)
		j.__init__(pr,Lcr,fyk,M,tipo,grafico,info,x,E,G,plastica,direz)
		vars(j).update(vars(i))
		del i
		return j

	def __init__(self,pr,Lcr,fyk,M=r_[0.],tipo='laminata',grafico=None,info=None,x=None,E=210e3,G=81e3,plastica=False,direz='z',runcalc=False):
		if runcalc:
			vars(self).update(locals())
			del self.runcalc
			self.Mcr=Mcr(pr,Lcr,M,E,G,direz)
			self.lamlt=self.calcolaLamlt()
			self.lamlt0=0.2
			self.beta=1 #a favore di sicurezza
			self.curva=self.curvaStabilita()
			self.alphalt=dict(a=.21,b=.34,c=.49,d=.76)[self.curva]
			self.philt=self.calcolaPhilt()
			self.kc=self.calcolaKc()
			self.f=self.calcolaF()
			self.Kchi=self.calcolaKchi()
			self.chiLT=self.calcolaChiLT()

	def calcolaBeta(self):
		if 'name' in list(self.pr.keys()):
			if any([i in self.pr['name'] for i in ['IPE','HE','IPN']]):
				return 0.75
		return 1		

	def calcolaChiLT(self):
		return min((1/self.f * 1/(self.philt+sqrt(self.philt**2 - self.beta*self.lamlt**2)),self.Kchi))
	
	def calcolaPhilt(self):
		return 0.5*(1+self.alphalt*(self.lamlt-self.lamlt0) + self.beta*self.lamlt**2)
	
	def calcolaLamlt(self):
		if self.plastica:
			return sqrt(self.pr.Wply*self.fyk/self.Mcr)
		else:
			return sqrt(self.pr.Wy*self.fyk/self.Mcr)

	def curvaStabilita(self):
		if self.tipo=='laminata':
			if self.pr.h/self.pr.b<=2:return 'b'
			else: return 'c'
		elif self.tipo=='saldata':
			if self.pr.h/self.pr.b<=2: return 'c'
			else: return 'd'
		else: return 'd'

	def calcolaF(self):
		return 1-0.5*(1-self.kc)*(1-2*(self.lamlt-0.8)**2)

	def calcolaKchi(self):
		if not 'name' in list(self.pr.keys()): return 1
		if any([i in self.pr['name'] for i in ['IPE','IPN','HE']]):
			return min((1,1/self.f/self.lamlt**2))
		else: return 1
		

	def calcolaKc(self):
		'''tab 4.2.X
		self.M=valori del momento per ricercare la curva
		self.psi= valore di self.psi per variazione lineare
		self.grafico= costante|
			quadratico|quadratico inversione|quadratico eccentrico|
			lineare|lineare inversione|lineare eccentrico|
		self.info: informazioni aggiuntive per ricerca grafico
		self.x=punti di M se None li genera equispaziati
			'''
		if self.grafico is not None:
			if self.grafico=='costante':out= 1.
			elif self.grafico=='quadratico':out= .94
			elif self.grafico=='quadratico inversione':out= .9
			elif self.grafico=='quadratico eccentrico': out= .91
			elif self.grafico=='bilineare': out= .86
			elif self.grafico=='bilineare inversione': out= .77
			elif self.grafico=='bilineare eccentrico': out= .82
			if 'out' in locals().keys():
				return out
		
		self.M=atleast_1d(self.M).astype(float)
		if len(self.M)==2 or self.grafico=='lineare':
			if self.M[-1]!=0 and self.M[0]!=0:
				self.psi=self.M[0]/self.M[-1]
			else:self.psi=0
			if abs(self.psi)>1: self.psi=self.M[1]/self.M[0]
			self.grafico='lineare'
			out= 1/(1.33-.33*self.psi)
			return out
		else:
			if self.x is None: self.x=arange(len(self.M))
			self.x=atleast_1d(self.x)
			#valuto interpolazioni
			errori=[]
			if self.info=='': self.info=None
			if self.info is None  or self.info in ['lineare','lin'] :
				if len(self.M)==1:
					self.M=r_[self.M[0],self.M[0]]
					self.x=r_[0,1]
				linea=polyfit(self.x,self.M,1)
				errori.append([norm(polyval(linea,self.x)-self.M),linea])
			if self.info in ['quadratico','quad'] or self.info is None and  len(self.M)>=3 :
				parabola=polyfit(self.x,self.M,2)
				errori.append([norm(polyval(parabola,self.x)-self.M),parabola])
			if self.info in ['bilineare','bilin'] or self.info is None and len(self.M)>=3:
				def bl(param,x):
					y0,p0,x1,p1=param
					v1=y0+p0*x
					v2=y0+p0*x1 + (x-x1)*p1
					return where(x<=x1,v1,v2)
				self.xstart=r_[
					self.M[0],
					diff(self.M[:2])/diff(self.x[:2]),
					self.x.mean(),
					diff(self.M[-2:])/diff(self.x[-2:]),
					]
				bilinea=minimize(lambda param:norm(bl(param,self.x)-self.M),self.xstart)
				errori.append([norm(bl(bilinea.x,self.x)-self.M),bilinea.x])
#			errori.sort()
			indici=argsort([i[0] for i in errori])
			errori_=[errori[i] for i in indici]
			errori=errori_
			funzione=None
			#controllo errore tra bilineare e quadratica, nel caso prediligo bilineare
			if len(errori)>1:
				if len(errori[0][1])==3:
					if len(errori[1][1])==4:
						if errori[1][0]<=errori[0][0]*1.05:
							funzione=errori[1][1]
			if funzione is None:
				funzione=errori[0][1]
			if len(funzione)==2:
				#lineare
				if funzione[0]==0:return 1
				v=polyval(funzione,(self.x.min(),self.x.max()))
				self.psi=v[0]/v[1]
				if not -1<=self.psi<=1: 
					self.psi=1/self.psi
				self.grafico='lineare'
				return self.calcolaKc()
			elif len(funzione)==3:
				#quadratica
				v=polyval(funzione,(self.x.min(),self.x.mean(),self.x.max()))
				if max(abs(r_[v[0],v[-1]]/v[1]))<1e-2:
					self.grafico='quadratico'
					return self.calcolaKc()
				elif min(abs(r_[v[0],v[-1]]/v[1]))<1e-2:
					self.grafico='quadratico eccentrico'
					return self.calcolaKc()
				else:
					self.grafico='quadratico inversione'
					return self.calcolaKc()
			elif len(funzione)==4:
#				bilineare
				v=bl(funzione,r_[self.x.min(),self.x.mean(),self.x.max()])
				if max(abs(r_[v[0],v[-1]]))/mean(abs(v))<1e-2:
					self.grafico='bilineare'
					return self.calcolaKc()
				elif abs(v[0]-v[1])/mean(abs(v))<1e-2:
					self.grafico='bilineare inversione'
					return self.calcolaKc()
				else:
					self.grafico='bilineare eccentrico'
					return self.calcolaKc()

		if all(self.M==self.M[0]):
			self.grafico='costante'
			return self.calcolaKc()

	def __str__(self):
		st= f'Mcr={self.Mcr:.5g} grafico={self.grafico} '
		if hasattr(self,'psi'):
			st+=f'psi={self.psi:.3f} '
		st+=f'kc={self.kc:.3f} chiLT={self.chiLT:.3f}'
		return st


class Mcr(float):
	def __new__(cls,pr,Lcr,M,E,G,direz='z'):
		i=float.__new__(cls,0)
		i.__init__(pr,Lcr,M,E,G,direz,runcalc=True)
		j=float.__new__(cls,i.Mcr)
		j.__init__(pr,Lcr,M,E,G,direz,runcalc=False)
		del i
		return j

	def __init__(self,pr,Lcr,M,E,G,direz,runcalc=False):
		if runcalc:
			vars(self).update(locals())
			del self.runcalc
			self.psi=self.calcolaPsi()
			self.Mcr=self.calcolaMcr()

	def calcolaMcr(self):
		if 'Jt' in list(self.pr.keys()): Jt=self.pr.Jt
		elif 'IT' in list(self.pr.keys()): Jt=self.pr.IT
		else: Jt=0
		if 'Jw' in list(self.pr.keys()):Jw=self.pr.Jw
		elif 'Iwx' in list(self.pr.keys()):Jw=self.pr.Iwx
		else: Jw=0
		if self.direz=='z':
			J=self.pr.Jz
		elif self.direz=='y':
			J=self.pr.Jy
		return self.psi*pi/self.Lcr * sqrt(self.E*J*self.G*Jt) * sqrt(1+(pi/self.Lcr)**2*self.E*Jw/(self.G*Jt))

	def calcolaPsi(self):
		if self.M is None: ps=1
		elif self.M[0]==self.M[-1]: ps=1
		elif self.M[0]==0 or self.M[-1]==0: ps=0
		else:ps=self.M[0]/self.M[-1]
		if not -1<=ps<=1:ps=1/ps
		psi_= 1.75-1.05*(ps)+0.3*(ps)**2
		return psi_
