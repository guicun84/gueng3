from scipy import atleast_1d,atleast_2d,r_,c_,where,zeros,ones,zeros_like,hstack,vstack
from .instabilita import Chi,ChiLT
import pandas as pa

class Verifica:
	solindex=[0,1,2,3,4,5,]

	def __init__(self,pr,soltot,fyk,ftk,l=None,E=210e3,gammas=1.15,FC=1,*args,**kargs):
		'''pr =profilo
		   sol=sollecitazioni di interesse
		   fyk,ftk=resistenze materiali
		   l=luce di calcolo (se necessaria),
		   E=modulo elastico
		   gammas=coefficiente parziale di sicurezza
		   FC=fattore di confidenza
		   '''
		pr=pr.copy()
		soltot=atleast_2d(soltot)
		vars(self).update(locals())
		self.fyd=self.fyk/self.gammas/self.FC
#		print(repr(self))
#		print(pr)
#		print(sol,sol.shape,any(sol.flatten()!=0))
		if self.necessaria:
			self.calcolaEsd()
			self.calcolaRsd()
			self.cu=self.Esd/self.Rsd
		else:
			self.cu=zeros(self.sol.shape[0])
			self.Esd=zeros(self.sol.shape[0])
			self.Rsd=1
		self.cumax=self.cu.max()
	
	@property
	def necessaria(self):
		return any(self.sol!=0)
	
	@property
	def sol(self):
		'''metodo per ritornare le sollecitazioni principali per la verifica, reso necessario per non riscrivere tutto ma si può togliere andando a specificare caso per caso quale sollecitazione serve'''
		return atleast_2d(self.soltot[:,self.solindex])
	
	def calcolaEsd(self):
		'''calcolo delle azioni di progetto'''
		self.Esd=abs(self.sol).flatten()

	def calcolaRsd(self):
		'''calcolo della resistenza di progetto'''
		self.Rsd=0

	def __bool__(self):
		if self.cumax.flatten().max()<=1: return True
		return False

	def __str__(self):
		if hasattr(self,'nome'):
			st=f'{self.nome.upper()}\n'
		else:
			st=''
		if self.cumax!=0:
			Esd=self.Esd[self.cu==self.cumax][0]
			st+=f'Esd / Rsd = {Esd:.4g} / {self.Rsd:.4g} = {self.cumax:.3f} '
			if self: st+='<= 1 VERIFICATO'
			else:st+= '> 1 NON verificato'
		else: st+= 'verifica non necessaria'
		return st

	@property
	def plastica(self):
		if 'plastica' in self.kargs.keys():
			return self.kargs['plastica']
		return False


class AzioneNormale(Verifica):
	nome='compressione'
	solindex=[0]

	def calcolaRsd(self):
		self.Rsd=self.self.fyd*self.pr.A

class FlessioneY(Verifica):
	nome='flessione asse y'
	solindex=[5]
	def calcolaRsd(self):
		if self.plastica: W=self.pr.Wply
		else: W=self.pr.Wy
		self.Rsd=self.self.fyd*W

class FlessioneZ(Verifica):
	solindex=[4]
	nome='flessione asse Z'
	def calcolaRsd(self):
		if self.plastica: W=self.pr.Wplz
		else: W=self.pr.Wz
		self.Rsd=self.self.fyd*W

class TaglioY(Verifica):
	nome='taglio asse y'
	solindex=[1]
	def calcolaRsd(self):
		if self.plastica:
			if 'Avply' in self.pr.keys():
				self.Av=self.pr.Avply
			else:
				self.Av=self.pr.A - 2*self.pr.b*self.pr.tf + (self.pr.tw+2*self.pr.r)*self.pr.tf
		else:
			if 'Avy' in self.pr.keys():
				self.Av=self.pr.Avy
			else:
				self.Av=(self.pr.h-2*self.pr.tf)*self.pr.tw/1.5
		self.Rsd=self.Av*self.self.fyd/3**.5


class TaglioZ(Verifica):
	nome='taglio asse z'
	solindex=[2]
	def calcolaRsd(self):
		if self.plastica:
			if 'Avplz' in self.pr.keys():
				self.Av=self.pr.Avplz
			else:
				self.Av=self.pr.A - self.pr.h*self.pr.tw + (2*self.pr.r + self.pr.tf)*self.pr.tw
		else:
			if 'Avz' in self.pr.keys():
				self.Av=self.pr.Avz
			else:
				self.Av=2*(self.pr.b-self.pr.tw)*self.pr.tf/1.5
		self.Rsd=self.Av*self.self.fyd/3**.5

class Torsione(Verifica):
	nome='torsione'
	solindex=[3]
	def calcolaRsd(self):
		#se ho gia Wt non devo calcolarlo
		if 'Wt' in self.pr.keys():
			W=self.pr.Wt
		else:
			#controllo se sezione chiusa o aperta
			chiusa=True
			for nome in ['HE','IPE','UPN','L','IPN']:
				if nome in self.pr['name']:
					chiusa=False
					break
			if chiusa:
				if 'b' in self.pr.keys() :t=.5*(self.pr.b**2 + self.pr.h**2)**.5 #sezione tipo rettangolare
				elif 'd' in self.pr.keys(): t=.5*self.pr.d
			else :
				if 'tf' in self.pr.keys():t=self.pr.tf
				elif 't' in self.pr.keys(): t=self.pr.t
				elif 's' in self.pr.keys(): t=self.pr.s
			W=self.pr.IT/t
		self.Rsd=W*self.fyd/3**.5


class InstabilitaN(Verifica):
	nome='Instabilità euleriana'
	solindex=[0]
	def __init__(self,*args,**kargs):

		'''''oltre a quanto necessario per Verifica: opzionali
		direz y|z|u|v|min (min default)
		alpha curva di imperfezione tab 4.2.VIII
		li ly lz lunghezze di instabilità [considera la massima delle 3]
		fullout'''
		li=dict(li=0,ly=0,lz=0)
		li.update({i:j for i,j in kargs.items() if i in li.keys()})
		self.li=max(li.values())
		l=args[4]
		if self.li==0: self.li=l
		self.inp=dict(direz='min',alpha='b')
		self.inp.update({i:j for i,j in kargs.items() if i in self.inp.keys()})
		Verifica.__init__(self,*args,**kargs)

	def calcolaEsd(self):
		self.Esd=where(self.sol>0,0,abs(self.sol)).flatten()
	def calcolaRsd(self):
		self.chi=Chi(self.pr,self.li,self.fyk,**self.inp)
		self.Rsd=self.self.fyd*self.pr.A*self.chi

class InstabilitaFlexY(Verifica):
	nome='Instabilità flesso torsionale asse Y'
	solindex=[5]
	direzN='y'
	def __init__(self,*args,**kargs):
		self.soltot=args[1]
		''' oltre quanto necessario per verifica sono opzioanli
		M 
		tipo 
		grafico
		info
		x 
		fullout
		E 
		G
		plastica
		direz
		ly
		correggiN : True per default, corregge il calcolo considerando lo sforzo normale, se False non lo corregge'''

		self.inp=dict()
		for i in 'M tipo grafico info x fullout E G plastica direz'.split():
			if i in kargs.keys():
				self.inp[i]=kargs[i]
		if 'M' not in self.inp.keys():
			self.inp['M']=self.sol.flatten()

		l=args[4]


		if 'ly' in kargs.keys():
			self.ly=kargs['ly']
			if self.ly==0:
				self.ly=l
		else:
			self.ly=l

		if 'lz' in kargs.keys():
			self.lz=kargs['lz']
			if self.lz==0:
				self.lz=l
		else:
			self.lz=l

		#per correggere la resistenza in funzione del carico normale devo calcolare Nsd/Ncr
		self.correggiN=True
		if 'correggiN' in kargs.keys():
			self.correggiN=kargs['correggiN']
		if self.correggiN is None: self.correggiN=True
		if self.correggiN:
			inp={i:j for i,j in kargs.items() if i in 'alpha ly lz'}
			if self.direzN=='y' and 'ly' in inp.keys():
				inp['li']=inp['ly']
			if self.direzN=='z' and 'lz' in inp.keys():
				inp['li']=inp['lz']
			if 'li' not in inp.keys():
				inp['li']=l
			inp['direz']=self.direzN
			self.vN=InstabilitaN(*args,**inp)
			if not self.vN.necessaria:
				self.vN.calcolaRsd()

		Verifica.__init__(self,*args,**kargs)

	def calcolaRsd(self):
		self.chiLT=ChiLT(self.pr,self.ly,self.fyk,**self.inp)
		if self.plastica:W=self.pr.Wply
		else: W=self.pr.Wy
		self.Rsd=self.self.fyd*W*self.chiLT
		#correggo per considerare eventuale compressione
		if self.correggiN:
			N=self.soltot[:,0].copy().flatten()
			N[N>0]=0
			N=abs(N)
			if any(N>0):
				self.Rsd*=(1-N/self.vN.chi.Ncr)


class InstabilitaFlexZ(InstabilitaFlexY):
	solindex=[4]
	nome='Instabilità flesso torsionale asse Z'
	direzN='z'
	def calcolaRsd(self):
		self.chiLT=ChiLT(self.pr,self.lz,self.fyk,**self.inp)
		if self.plastica:W=self.pr.Wplz
		else: W=self.pr.Wz
		self.Rsd=self.self.fyd*W*self.chiLT
		#correggo per considerare eventuale compressione
		if self.correggiN:
			N=self.soltot[:,0].copy()
			N[N>0]=0
			N=abs(N)
			self.Rsd*=(1-N/self.vN.chi.Ncr)

class Deformazione(Verifica):
	nome='deformazioni'
	solindex=[0]
	def __init__(self,pr,l,dsle,r=500,qslu=None,qsle=None,E=210e3,gammas=1.15,FC=1,djsle=False,*args,**kargs):
		'djsle=True false o asse da usare'
		dsle=atleast_1d(dsle)
		dsle=dsle.reshape(-1,1)
		if qslu is not None: #nel caso avessi usato djslu lo correggo con qslu e qsle
			dsle=dsle/qslu*qsle
		vars(self).update(locals())
		super().__init__(pr,dsle/{True:self.pr.Jy,False:1,'z':self.pr.Jz,'y':self.pr.Jy}[djsle],0,0,l,E,gammas,FC,*args,**kargs)

	def calcolaEsd(self):
		self.Esd= self.sol.flatten()
	def calcolaRsd(self):
		self.Rsd=self.l/self.r

	def __str__(self):
		st=''
		st=super().__str__()
		st+=f'''
relativo:{self.Esd[self.cu==self.cumax][0]:.4g}/{self.l:.1f}  = 1/{self.l/(self.Esd[self.cu==self.cumax][0]):.0f} < 1/{self.r} '''
		if self.cumax<=1: st+=' VERIFICATO'
		else: st+=' NON verificato'
		return st

################################################################################
		
class VerificaComposta(Verifica):
	nome='verifica composta'
	verifiche=[]
	def __init__(self,pr,soltot,fyk,ftk,l=None,E=210e3,gammas=1.15,FC=1,*args,**kargs):
		soltot=atleast_2d(soltot)
		vars(self).update(locals())
		self.verifiche=[i(pr,self.sol,fyk,ftk,l,E,gammas,FC,*args,**kargs) for i in self.verifiche]
		self.calcolaCu()

	def calcolaCu(self):
		#self.cu=c_[[i.cu for i in self.verifiche]].sum(0) #brutalmente li sommo senza tante finezze
		self.cu=self.verifiche[0].cu.copy()
		for ver in self.verifiche[1:]:
			self.cu+=ver.cu
		self.cumax=self.cu.max()

	def __str__(self):
		st=''
		if self.nome: st+=f'{self.nome.upper()}'
		idx=where(self.cu==self.cumax)[0][0]
		for i in self.verifiche:
			if i.cumax!=0:
				if not hasattr(i.Rsd,'__len__'):
					st+=f'\n    {i.nome} Esd/Rsd = {i.sol[idx,0]:.4g} / {i.Rsd:.4g} = {i.cu[idx]:.3f}'
				else:
					st+=f'\n    {i.nome} Esd/Rsd = {i.sol[idx,0]:.4g} / {i.Rsd[idx]:.4g} = {i.cu[idx]:.3f}'
			else:
				st+=f'\n    {i.nome} : verifica non necessaria'
		st+=f'\n    TOTALE = {self.cumax:.3f} '
		if self:st+='<= 1 VERIFICATO'
		else: st+= '> 1 NON verificato'
		return st

	def __repr__(self):
		return f'{self.cumax}'

class TaglioTorsione(VerificaComposta):
	'''a favore di sicurezza sommo tutto, in realtà sarebbe da fare TaglioY+torsione e TaglioZ+torsione visto che TaglioY e TaglioZ si riferiscono a punti differenti della sezione'''
	nome='taglio torsione'
	verifiche=[TaglioY,TaglioZ,Torsione]


class PressoFlessione(VerificaComposta):
	nome='presso tenso flessione'
	verifiche=[AzioneNormale,FlessioneY,FlessioneZ]

class TaglioFlessione(VerificaComposta):
	pass

class InstabilitaPressoFlessione(VerificaComposta):
	nome='instabilità presso tenso flessione'
	verifiche=[InstabilitaN,InstabilitaFlexY,InstabilitaFlexZ]

################################################################################

class VerificaProfilo:
	nome='verifica profilo'
	def __init__(self,pr,sol,fyk,ftk,l=None,E=210e3,gammas=1.15,FC=1,*args,**kargs):
		'''pr =profilo
		   sol=sollecitazioni
		   fyk,ftk=resistenze materiali
		   l=luce di calcolo (se necessaria),
		   E=modulo elastico
		   gammas=coefficiente parziale di sicurezza
		   FC=fattore di confidenza
		   
		   opzionali

		   direz,alpha,fullout per verifiche a instabilità euleriana
		   M,tipo,grafico,info,x,fullout,E,G,plastica per instabilità flessionale
		   dsle,qslu,qsle,r,djsle per deformazioni
		   '''
		sol=atleast_2d(sol)
		vars(self).update(locals())
		self.pr=pr.copy()
		self.verifiche=[
			PressoFlessione(pr,sol,fyk,ftk,l,E,gammas,FC,*args,**kargs),
			TaglioTorsione(pr,sol,fyk,ftk,l,E,gammas,FC,*args,**kargs),
			InstabilitaPressoFlessione(pr,sol,fyk,ftk,l,E,gammas,FC,*args,**kargs),
		]
		if 'dsle' in kargs.keys():
			dat=dict(dsle=0,qsle=None,qslu=None,r=500,E=E,djsle=False)
			dat.update({i:j for i,j in kargs.items() if i in dat.keys()} )
			self.verifiche.append(Deformazione(pr,l,**dat))
		self.cumax=max([i.cumax for i in self.verifiche])
	
	def __bool__(self):
		if self.cumax<=1:return True
		return False

	def __str__(self):
		if 'name' in self.pr.keys():
			st=f'PROFILO {self.pr["name"]}\n'
		elif 'nome' in self.pr.keys():
			st=f'PROFILO {self.pr["nome"]}\n'
		else:
			st='PROFILO ___\n'
		st+='\n'.join([f'{i}' for i in self.verifiche])
		st+=f'\nMASSIMO={self.cumax:.3f}'
		if self: st+=' <=1 VERIFICATO'
		else: st+=' >1 NON Verificatoa'
		return st

	def __repr__(self):
		return f'{self.cumax}'

################################################################################

class Predimensiona:
	def __init__(self,pr,sol,fyk,ftk,l=None,E=210e3,gammas=1.15,FC=1,cumax=1,*args,**kargs):
		prs=pr.copy()
		sol=atleast_2d(sol)
		vars(self).update(locals())
		self.prs['ver']=self.prs.apply(lambda x: VerificaProfilo(x,sol,fyk,ftk,l,E,gammas,FC,*args,**kargs),1)
		self.prs['cu']=self.prs.apply(lambda x:x.ver.cumax,1)

	def __bool__(self):
		if self.prs.shape[0]: return True
		return False
		
	@property
	def prs_(self):
		return self.prs[self.prs.cu<=self.cumax]

	@property
	def best(self):
		if self:
			k=self.prs_.G*self.prs.h
			k=k==k.min()
			return self.prs[k].iloc[0]
	@property
	def bestG(self):
		if self:
			return self.prs_.sort_values('G').iloc[0]
	@property
	def bestH(self):
		if self:
			return self.prs_.sort_values('h').iloc[0]
	@property
	def bestB(self):
		if self:
			return self.prs_.sort_values('b').iloc[0]



	

