
from scipy import *
from scipy.linalg import norm
from scipy.optimize import curve_fit,minimize
import pandas as pa
import os

class Curva():
	'''classe che rappresenta una tipologia di curva con cui fittare di dati a disposizone'''
	def	__init__(self,x,y):
		vars(self).update(locals())
		self.x0=self.x-self.x[0]
		self.fit()
	
	@staticmethod
	def fun(x,*args):
		'''funzione da calcolare'''
		pass

	def __call__(self,x=None):
		if x is None: x=self.x0
		else: x=x-self.x[0]
		return self.fun(x,*self.param)

	def p0(self):
		'''valuta valore di guess iniziale per fit'''
		pass

	def fit(self):
		v=curve_fit(self.fun,self.x0,self.y,self.p0())
		self.param=v[0] #parametri di ottimizzazione
		self.param_covariance=v[1] #matrice covarianza parametri
		self.er=norm(self()-self.y) #errore di ottimizzazione

class Quad(Curva):
	'''curva quadratica'''
	@staticmethod
	def fun(x,*args):
		v0,vm,l=args
		x_=r_[0,.5,1]*l
		y_=r_[v0,vm,v0]
		p=polyfit(x_,y_,2)
		return polyval(p,x)

	def p0(self):
		v0=self.y[0]
		vm=self.y[self.y.size//2]
		l=self.x0[-1]
		return r_[v0,vm,l]


class Bilin(Curva):
	'''curva bilineare'''
	@staticmethod
	def fun(x,*args):
		y0,p,lm=args 
		'''y0=valore iniziale
		p=pendenza
		xm=x per M=Mmax'''
		return (y0+p*x)*(x<=lm) + (y0+p*lm-p*(x-lm))*(x>lm)
	
	def p0(self):
		y0=self.y[0]
		l=self.x0[-1]
		lm=l/2
		pe=(self.y[self.y.size/2]-y0)/lm
		return r_[y0,pe,lm]


class Trilin(Curva):
	'''curva trillineare'''
	@staticmethod
	def fun(x,*args):
		y0,p,lm1,lm2=args 
		'''y0=valore iniziale
		p=pendenza
		xm=x per M=Mmax'''
		return (y0+p*x)*(x<=lm1) + (y0+p*lm1)*((x>lm1)&(x<lm2)) + (y0+p*lm1-p*(x-lm2))*(x>lm2)

	def p0(self):
		y0=self.y[0]
		l=self.x0[-1]
		lm=l/3
		pe=(self.y[self.y.size/3]-y0)/lm
		return r_[y0,pe,lm,2/3*l]
	
class Lin(Curva):
	@staticmethod
	def fun(x,*args):
		return polyval(args,x)
	def p0(self):
		return r_[self.y[0],(self.y[-1]-self.y[0])/self.x.ptp()]


class CurveFinder:
	def __init__(self,x,M,curve=[]):
		'''x= [array] ascissa
		M= [array] momento
		curve=[int]numero per indicare le curve 
			[quad,bilin,trilin]--> 101=5 usa solo quad e trilin
		output=[str]definisce tipo di output dare:
			best=restituisce il valore della curva migliore
			wheight: pesa i valori con l'inverso degli errori
			mean: valore medio 
			list: resitiuisce la lista dei valori e degli errori'''
		if len(x)<7 and curve==7:
			print('si consiglia un campionamento minimo di 7 punti o di limitare le tipologie di curve da usare')
		vars(self).update(locals())
		self.x0=self.x-self.x[0]
		if not self.curve:
			self.curve=[Quad,Bilin,Trilin,Lin]
		self.cercaCurva(self.curve)
	def cercaCurva(self,curve=None):
		if curve is None:
			curve=self.curve
		curve_inst=[i(self.x,self.M) for i in curve]
		if len(self.x)<(3):
			raise ValueError('x deve avere almeno 3 punti')
	
		self.er=r_[[i.er for i in curve_inst]]
			
		self.k=where(self.er==self.er.min())[0]
		self.fun=curve_inst[self.k]

