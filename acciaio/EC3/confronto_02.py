
from gueng.acciaio.CNR import omega_1
#confronto tra cnr e EC3 su trave con 2 carichi applicati

P=10e3 #N


pr=prof.ipe300
ls=linspace(4,9)*1e3
#calcolo momento resistente con CNR
cu_CNR=zeros_like(ls)
for n,l in enumerate(ls):
	Mmax=P*l/3
	Mm=Mmax*(l/3+2*l/3/2)/l
	Meq=max([.75*Mmax,min([1.3*Mm,Mmax])])
	om1=omega_1(275,pr.h,l,pr.b,pr.tf)
	cu_CNR[n]=Meq*om1*1.4/(pr.Wy*275/1.15)


############################################################
#calcolo MRd con EC3
#ottengo i coefficienti:
from tab_F1_2_r1 import F12
Mcr_EC=zeros_like(ls)
Mrd_EC=zeros_like(ls)
cu_EC=zeros_like(ls)
dati_EC=[]
from momenti_critici_02 import IShape
for n,l in enumerate(ls):
	Mmax=P*l/3
	x=linspace(0,l,7)
	y=x*(x<=l/3) + (x[2] *((l/3<x) & (x<=2*l/3)))+ ((x[-1]-x)*(x>2*l/3))
	y=y/y.max()*Mmax
	i=IShape(pr,x,y,l)
	Mcr_EC[n]=i.Mcr
	Mrd_EC[n]=i.Mbrd
	cu_EC[n]=Mmax/i.Mbrd
	
import pandas as pa
dat=pa.DataFrame(c_[[ls,cu_CNR,cu_EC]].T)
dat.columns='l cu_CNR cu_EC'.split()
dat['delta']=dat.cu_EC - dat.cu_CNR
dat['deltap']=dat.delta/dat.cu_CNR

print(dat)
