#-*- coding:latin-*-

from scipy import pi,sqrt


def Mcr4(pr,l,c1,c2,c3,k=1,kw=1,zg=None,E=210e3,ni=.25):
	'''Travi con sezioni trasversali uniformi doppiamente simmetriche
	Per sezioni trasversali doppiamente simmetriche � zj = 0
	pr=profilo utilizzato [ipe he ipn] per upn da vedere....
	l=luce 
	c1,c2,c3 coefficienti per calcolo 
	k,kw= coefficienti lunghezza efficace per flex e warp
	zg= coordinta di applicazione carico rispetto la centro, di default bordo superiore
	E,Ni young e poissont acciaio'''
	G=E/(2*(1+ni))
	Iz=pr.Jz
	Iw=pr.Iwx
	It=pr.IT
	if zg is None:
		zg=pr.h/2
	v1=c1*pi**2*E*Iz/(k*l)**2
	v2=(k/kw)**2*Iw/Iz
	v3=(k*l)**2*G*It/(pi**2*E*Iz)
	v4=(c2*zg)**2
	v5=c2*zg
	v6=v1*(sqrt(v2+v3+v4)-v5)
	return c1*pi**2*E*Iz/(k*l)**2* ( sqrt( (k/kw)**2*Iw/Iz + (k*l)**2*G*It/(pi**2*E*Iz) + (c2*zg)**2) - c2*zg),locals()



def Mbrd(chilt,Wpl,fy,betaw=1,gam=1.15):
	'''calmolo del momento resistente della sezione
	chilt=coefficiente di riduzione per instabilit� flextor
	Wpl modulo resistenza plastica
	fy fy acciaio
	betaw: W/Wpl con da prendere in funzione della classe del  1e2:Wpl , 3:Wel , 4:Weff
	gam=gamma acciaio'''
	return chilt*betaw*Wpl*fy/gam


def chilt(lamlt_,alphalt=.21):
	'''lamlt_=senellezza adimensionale 
	alphalt= coefficiente per tipologia sezioen:
		laminata .21
		saldata .49'''
	philt_=philt(lamlt_,alphalt)
	return min([1,1/(philt_ + sqrt(philt_**2-lamlt_**2))])


def philt(lamlt_,alphalt=.21):
	'''lamlt_=senellezza adimensionale 
	alphalt= coefficiente per tipologia sezioen:
		laminata .21
		saldata .49'''
	return .5*(1+alphalt*(lamlt_-.2)*lamlt_)


def lamlt_(Wply,fy,Mcr,betaw=1):
	return sqrt(betaw*Wply*fy/Mcr)
