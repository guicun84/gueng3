
from scipy import *
from scipy.linalg import norm
from scipy.optimize import curve_fit,minimize
import pandas as pa
import os


class F12:
	tab=pa.read_csv(os.path.join(os.path.dirname(__file__),'tab_F1.2.csv'),sep='\s+\|\s+')

	def __init__(self,x,M,k=1,curve=7):
		'''x= ascissa
		M=momento
		k=coefficiente per luce libera
		curve=numero per indicare le curve [quad,bilin,trilin]--> 101=5 usa solo quad e trilin'''
		if len(x)<7 and curve==7:
			print('si consiglia un campionamento minimo di 7 punti o di limitare le tipologie di curve da usare')
		vars(self).update(locals())
		self.x0=self.x-self.x[0]
		tmp='{:b}'.format(curve)

		curves=[F12.quad,F12.bilin,F12.trilin]
		self.curve=[]
		for n,i in enumerate(tmp): 
			if i=='1':self.curve.append(curves[n])
		
		self.cercaCurva(self.curve)
		self.cercaIncastro()

	@property
	def c1(self):
		return self.tab.c1[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]

	@property 
	def c2(self):
		return self.tab.c2[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]

	@property
	def c3(self):
		return self.tab.c3[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]


	@staticmethod
	def quad(x,*param):
		#i grafici delle tabelle sono simmetrici, quindi devo imporre parabola simmetrica
		v0,vm,l=param
		x_=r_[0,.5,1]*l
		y_=r_[v0,vm,v0]
		p=polyfit(x_,y_,2)
		return polyval(p,x)

	def valutaQuad(self):
		v0=self.M[0]
		vm=self.M[self.M.size//2]
		l=self.x0[-1]
		p=curve_fit(F12.quad,self.x0,self.M,r_[v0,vm,l])[0]
		er=norm(F12.quad(self.x0,*p)-self.M)
		return p,er

	@staticmethod
	def bilin(x,*param):
		y0,p,lm=param 
		'''y0=valore iniziale
		p=pendenza
		xm=x per M=Mmax'''
		return (y0+p*x)*(x<=lm) + (y0+p*lm-p*(x-lm))*(x>lm)

	def valutaBilin(self):
		y0=self.M[0]
		l=self.x0[-1]
		lm=l/2
		pe=(self.M[self.M.size/2]-y0)/lm
		p=curve_fit(F12.bilin,self.x,self.M,r_[y0,pe,lm])[0]
		er=norm(F12.bilin(self.x,*p)-self.M)
		return p,er

	@staticmethod
	def trilin(x,*param):
		y0,p,lm1,lm2=param 
		'''y0=valore iniziale
		p=pendenza
		xm=x per M=Mmax'''
		return (y0+p*x)*(x<=lm1) + (y0+p*lm1)*((x>lm1)&(x<lm2)) + (y0+p*lm1-p*(x-lm2))*(x>lm2)

	def valutaTrilin(self):
		y0=self.M[0]
		l=self.x0[-1]
		lm=l/3
		pe=(self.M[self.M.size/3]-y0)/lm
		p=curve_fit(F12.trilin,self.x0,self.M,r_[y0,pe,lm,2/3*l])[0]
		er=norm(F12.trilin(self.x0,*p)-self.M)
		return p,er

	valuta={'quad':valutaQuad , 'bilin':valutaBilin,  'trilin':valutaTrilin}

	def cercaCurva(self,fun=None):
		if fun is None:
			fun=[F12.quad,F12.bilin,F12.trilin]
		if len(self.x)<(3):
			raise ValueError('x deve avere almeno 3 punti')
		if len(self.x)==3:
			#per eseguire il fit di una funzione generica necessito di almeno 4 punti, se non li ho provo solo con la quad
			if F12.quad in fun:
				fun=[F12.quad]
		er=[]
		p=[]
		for i in fun:
			p_,e_=F12.valuta[i.__name__](self)
			er.append(e_)
			p.append(p_)
			
		k=er.index(min(er))
		self._fun=fun[k]
		self._p=p[k]




#	def cercaCurva_old(self,fun=None):
#		if fun is None:
#			fun=[F12.quad,F12.bilin,F12.trilin]
#		if len(self.x)<(3):
#			raise ValueError, 'x deve avere almeno 3 punti'
#		if len(self.x)==3:
#			#per eseguire il fit di una funzione generica necessito di almeno 4 punti, se non li ho provo solo con la quad
#			if F12.quad in fun:
#				fun=[F12.quad]
#		l=self.x.ptp()
#		p=[]
#		er=[]
#		#ricerco errore per quadratica
#		if F12.quad in fun:
#			v0=self.M[0]
#			vm=self.M[self.M.size//2]
#			p.append(curve_fit(F12.quad,self.x,self.M,r_[v0,vm,l])[0])
#			er.append(norm(F12.quad(self.x,*p[-1])-self.M))
#		#ricerco errore per bilineare
#		if F12.bilin in fun:
#			y0=self.M[0]
#			lm=l/2
#			pe=(self.M[self.M.size/2]-y0)/lm
#			p.append(curve_fit(F12.bilin,self.x,self.M,r_[y0,pe,lm])[0])
#			er.append(norm(F12.bilin(self.x,*p[-1])-self.M))
#
#		#ricerco errore per trilineare
#		if F12.trilin in fun:
#			y0=self.M[0]
#			lm=l/3
#			pe=(self.M[self.M.size/3]-y0)/lm
#			p.append(curve_fit(F12.trilin,self.x,self.M,r_[y0,pe,lm,2/3*l])[0])
#			er.append(norm(F12.trilin(self.x,*p[-1])-self.M))
#
#		k=er.index(min(er))
#		self._fun=fun[k]
#		self._p=p[k]
#
	@property
	def funName(self):
		return self._fun.__name__
		

	def fun(self,x=None):
		if x is None:x=self.x
		return self._fun(x-self.x[0],*self._p)

	def cercaIncastro(self):
		'ricerco se siamo in condizione di vincolo rigido o meno agli estremi'
		fe=self.fun(r_[self.x[0],self.x[-1]])
		fi=minimize(lambda x: -self.fun(x)**2,r_[self.x.mean()],method='tnc',bounds=((self.x[0],self.x[-1]),))
		fi=sqrt(fi.fun)[0]
		if fi!=0:
			self.phi=min(abs(fe/fi))
			self.incastro=self.phi>=1
		else:
			self.phi=inf
			self.incastro=1
		if self.incastro and self.funName!='trilin':
			self.diagramma=self.funName+'i'
		else:
			self.diagramma=self.funName+'a'

if __name__=='__main__':
	n=7
	import unittest
	class MyTest(unittest.TestCase):
		def test_quada(self):
			x=linspace(-1,1,n)
			y=x**2
			y-=y[0]
			x+=1
			f=F12(x,y)
			vref=[1.132 , 0.459 , 0.525]
			self.assertEqual(f.diagramma,'quada')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)
		
		def test_quadi(self):
			x=linspace(-1,1,n)
			y=x**2
			x+=1
			y-=y[0]/(2/.99)
			f=F12(x,y)
			vref=[1.285 , 1.562 , 0.753]
			self.assertEqual(f.diagramma,'quadi')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)

		def test_blina(self):
			x=linspace(-1,1,n)
			y=abs(x)
			x+=1
			y-=y[0]
			f=F12(x,y)
			vref=[1.365 , 0.553 , 1.730]
			self.assertEqual(f.diagramma,'bilina')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)
		
		def test_blini(self):
			x=linspace(-1,1,n)
			y=abs(x)
			x+=1
			y-=y[0]/2*.99
			f=F12(x,y)
			vref=[1.565 , 1.267 , 2.64]
			self.assertEqual(f.diagramma,'bilini')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)


	unittest.main(verbosity=2)
