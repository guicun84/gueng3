
from scipy import *

def G(E,ni):
	return E/(2*(1+ni))

def Mcr1():
	'''Il momento critico elastico per instabilità flesso-torsionale di una trave, avente sezione trasversale simmetrica uniforme con ali uguali, sotto condizioni normali di vincolo a ciascun estremo, caricata attraverso il suo centro di taglio e soggetta ad un momento uniforme,'''
	return (pi**2*E*Iz)/l**2 * sqrt( Iw/Iz + ((1**2*G*It)/(pi**2*E*Iz) )) #formula sbagliata: 1**2 nn so cosa sia...


def Mcr2():
	''' Nel caso di una trave avente sezione trasversale uniforme simmetrica rispetto allasse minore, per flessione rispetto allasse maggiore'''
	return c1*(pi**2*E*Iz)/(k*l**2) * ( sqrt( (k/kw)**2*(Iw/Iz) + ((k*l)**2*G*It)/(pi**2*E*Iz) + (C2*zg - c3*zj)**2))

def Mcr4():
	'''Travi con sezioni trasversali uniformi doppiamente simmetriche
	Per sezioni trasversali doppiamente simmetriche è zj = 0, cosicché:'''
	return c1*pi**2*E*Iz/(k*l)**2* ( sqrt( (k/kw)**2*Iw/Iz + (k*l)**2*G*It/(pi**2*E*Iz) + (c2*zg)**2) - c2*zg)

def Mcr5():
	'''Travi con sezioni trasversali uniformi doppiamente simmetriche
	Per sezioni trasversali doppiamente simmetriche è zj = 0, cosicché:
	Per la condizione di carico di momento agli estremi è C2 = 0 e per carichi trasversali applicati
	nel centro di taglio è zg = 0'''
	return c1*pi**2*E*Iz/(k*l)**2*  sqrt( (k/kw)**2*Iw/Iz + (k*l)**2*G*It/(pi**2*E*Iz) )

def Mcr6():
	'''Travi con sezioni trasversali uniformi doppiamente simmetriche
	Per sezioni trasversali doppiamente simmetriche è zj = 0, cosicché:
	Per la condizione di carico di momento agli estremi è C2 = 0 e per carichi trasversali applicati
	nel centro di taglio è zg = 0
	quando k==kw==1'''
	return c1*pi**2*E*Iz/l**2*  sqrt(Iw/Iz + (k*l)**2*G*It/(pi**2*E*Iz) )

