
from scipy import *
import re

class curva:
	counter=1
	def __init__(self,x,y):
		vars(self).update(locals())
		self.ide=curva.counter
		curva.counter+=1
	def plot(self,*args,**kargs):
		plot(self.x,self.y,*args,**kargs)
		text(self.x[0],self.y[0],'{}'.format(self.ide))

class Svg:
	def __init__(self,name):
		self.name=name
		self.doc=etree.parse(name)
		self.root=svg.getroot()
		self.path=root.findall('.//svg:path',root.nsmap)
		self.curve=[]
		for i in self.path:
			v,j=parsed(i)
			for h in v:
				if h.size:
					self.curve.append(curva(h[:,0],h[:,1]))

	def plot(self):
		for i in self.curve:
			i.plot()
	
	def parsed(self,el):
		d=el.attrib['d']
		#ricerco la matrice
		matrix=eye(3)
		while el.tag!='{{{}}}{}'.format(el.nsmap['svg'],'svg'):
			if 'transform' in list(el.attrib.keys()):
				print('trovata trasformazione')
				st=re.search('[0-9\-\.,]+',el.attrib['transform']).group()
				st=st.split(',')
				st=[float(i) for i in st]
				if 'matrix' in el.attrib['transform']:
					print('matrix')
					mat=r_[st[0],st[1],0,st[2],st[3],0,st[4],st[5],1].reshape(3,3).T
				elif 'translate' in el.attrib['transform']:
					print('translate')
					mat=r_[1,0,st[0],0,1,st[1],0,0,1.].reshape(3,3)

				matrix=dot(matrix,mat)
			el=el.getparent()

		print(matrix)
		dat=[]
		k=spezza(d)
		dat=elabora(k)
		for m,i in enumerate(dat):
			for n,j in enumerate(i):
				dat[m][n]=dot(matrix,r_[j,1])[:2]
				dat[m][n][1]*=-1

		return dat,locals()


	@staticmethod
	def spezza(st):
		#ricerca tutte le curve contenute in unsingolo path
		ns=0
		blk=[]
		for n,i in enumerate(st):
			if re.match('[a-zA-z]',i):
				blk.append(st[ns:n])
				ns=n
		blk.append(st[ns:n])
		m=0
		for n,i in enumerate(blk):
			if i=='':
				del blk[n-m]
				m+=1
		return blk

	@staticmethod
	def elabora(blk):
		#calcola le coordinate globali dei punti del path
		out=[]
		bl=[]
		cur=r_[0,0.]
		for i in blk:
			j=i.split(' ')
			j=[k.split(',') for k in j[1:]]
			j=[r_[float(k[0]),float(k[1])] for k in j[:-1]]
			if not i: continue
			if i[0]=='m':
				if all(j[0]!=r_[0,0]):
					bl=r_[bl]
					out.append(bl)
					bl=[]
					cur+=j[0]
					bl.append(cur.copy())
				for k in j[1:]:
					cur+=k
					bl.append(cur.copy())
			elif i[0]=='M':
				if all(j[0]!=r_[0,0]):
					bl=r_[bl]
					out.append(bl)
					bl=[]
					cur=j[0]
					bl.append(cur)
				for k in j[2:]:
					cur+=k
					blk.append(cur.copy())
			elif i[0]=='l':
				for k in j:
					cur+=k
					bl.append(cur.copy())
			elif i[0]=='L':
				for k in j:
					cur=k
					bl.append(cur.copy())

		bl=r_[bl]
		out.append(bl)
		return out


from lxml import etree
svg=Svg('SN003a-EN-EU_01.svg')
figure(1)
show(0)
clf()
svg.plot()
draw()
