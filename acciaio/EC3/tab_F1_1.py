#-*-coding:latin-*-

from scipy import *
import pandas as pa
import os

class F11:
	tab=pa.read_csv(os.path.join(os.path.dirname(__file__),'tab_F1.1.csv'),sep='\s+\|\s+')

	def __init__(self,M,k=1):
		'''M=vettore dei momenti flettenti lungola trave
		k=coefficiente per luce libera'''
		vars(self).update(locals())
		M=r_[M[0],M[-1]]
		ind=argsort(abs(M))
		M=M[ind]
		self.psi=M[0]/M[1]
		self.c2=0 #c2 solo per carichi distribuiti, se diagrammi M lineari non c'� carico distribuito quindi c2=0

	@property
	def c1(self):
		v1=self.tab[(self.psi <= self.tab.psi) & (self.tab.k==self.k)].iloc[-1]
		v2=self.tab[(self.psi>= self.tab.psi) & (self.tab.k==self.k)].iloc[0]
		if v1.c1==v2.c1: return v1.c1
		else:return interp((v2.psi,v1.psi),(v2.c1,v2.c1),self.psi)
	@property
	def c3(self):
		v1=self.tab[(self.psi <= self.tab.psi) & (self.tab.k==self.k)].iloc[-1]
		v2=self.tab[(self.psi>= self.tab.psi) & (self.tab.k==self.k)].iloc[0]
		if v1.c3==v2.c3: return v1.c3
		else:return interp((v2.psi,v1.psi),(v2.c3,v2.c3),self.psi)




