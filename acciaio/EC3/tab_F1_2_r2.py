
from scipy import *
from scipy.linalg import norm
from scipy.optimize import curve_fit,minimize
import pandas as pa
from .curveFinder import CurveFinder
import os


class F12:
	tab=pa.read_csv(os.path.join(os.path.dirname(__file__),'tab_F1.2.csv'),sep='\s+\|\s+')

	def __init__(self,x,M,k=1,curve=[],output='best'):
		'''x= [array] ascissa
		M= [array] momento
		k= [float] coefficiente per luce libera
		curve=[int]curve da considerare
		output=[str]definisce tipo di output dare:
			best=restituisce il valore della curva migliore
			wheight: pesa i valori con l'inverso degli errori
			mean: valore medio 
			list: resitiuisce la lista dei valori e degli errori'''
		if len(x)<7 and curve==7:
			print('si consiglia un campionamento minimo di 7 punti o di limitare le tipologie di curve da usare')
		vars(self).update(locals())
		self.x0=self.x-self.x[0]
		if curve.__class__.__name__=='CurveFinder':
			self.curveFinder=curve
		else:
			self.curveFinder=CurveFinder(self.x,self.M,self.curve)	
			self.curveFinder.cercaCurva()
		self._fun=self.curveFinder.fun
		self.cercaIncastro()

	@property
	def c1(self):
		return self.tab.c1[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]

	@property 
	def c2(self):
		return self.tab.c2[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]

	@property
	def c3(self):
		return self.tab.c3[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]

	@property
	def funName(self):
		return self._fun.__class__.__name__.lower()
		

	def fun(self,x=None):
		if x is None:x=self.x
		return self._fun(x)

	def cercaIncastro(self):
		'ricerco se siamo in condizione di vincolo rigido o meno agli estremi'
		fe=self.M[0],self.M[-1]
		fi=self.M[self.M.size//2]
		if fi!=0:
			self.phi=min(abs(fe/fi))
			self.incastro=self.phi>=1
		else:
			self.phi=inf
			self.incastro=1
		if self.incastro and self.funName!='trilin':
			self.diagramma=self.funName+'i'
		else:
			self.diagramma=self.funName+'a'

if __name__=='__main__':
	n=7
	import unittest
	class MyTest(unittest.TestCase):
		def test_quada(self):
			x=linspace(-1,1,n)
			y=x**2
			y-=y[0]
			x+=1
			f=F12(x,y)
			vref=[1.132 , 0.459 , 0.525]
			self.assertEqual(f.diagramma,'quada')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)
		
		def test_quadi(self):
			x=linspace(-1,1,n)
			y=x**2
			x+=1
			y-=y[0]/(2/.99)
			f=F12(x,y)
			vref=[1.285 , 1.562 , 0.753]
			self.assertEqual(f.diagramma,'quadi')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)

		def test_blina(self):
			x=linspace(-1,1,n)
			y=abs(x)
			x+=1
			y-=y[0]
			f=F12(x,y)
			vref=[1.365 , 0.553 , 1.730]
			self.assertEqual(f.diagramma,'bilina')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)
		
		def test_blini(self):
			x=linspace(-1,1,n)
			y=abs(x)
			x+=1
			y-=y[0]/2*.99
			f=F12(x,y)
			vref=[1.565 , 1.267 , 2.64]
			self.assertEqual(f.diagramma,'bilini')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)


	unittest.main(verbosity=2)
