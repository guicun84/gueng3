
from scipy import *
from .instabilita import dati,dati2

k=dati.k.unique()
k.sort()
psi=dati.psi.unique()
psi.sort()

PSI,K=meshgrid(psi,k)
C=zeros_like(PSI).flatten()
for n,(i,j) in enumerate(zip(PSI.flat,K.flat)):
	C[n]=dati.query('psi==@i and k==@j').C1.iloc[0]

figure(1)
clf()
C=C.reshape(PSI.shape)
contourf(PSI,K,C,1000)
colorbar()
grid('on')


