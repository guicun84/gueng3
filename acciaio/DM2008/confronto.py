
from gueng.acciaio.CNR import omega_1
pr=prof.ipe300
ls=linspace(4,8)*1e3
#calcolo momento resistente con CNR
Mrd_CNR=zeros_like(ls)
for n,l in enumerate(ls):
	om1=omega_1(275,pr.h,l,pr.b,pr.tf)
	Mrd_CNR[n]=pr.Wy*275/1.15/om1/1.4

############################################################
#calcolo MRd con EC3
#ottengo i coefficienti:
from .tab_F1_2_r1 import F12
x=linspace(-1,1)
y=x**2
y-=y[0]
f=F12(x,y)
c1,c2,c3=f.c1,f.c2,f.c3
print('coefficienti c1={c1:.4g} , c2={c2:.4g} , c3={c3:.4g}'.format(**locals()))
Mcr_DM08=zeros_like(ls)
Mrd_DM08=zeros_like(ls)
dati_EC=[]
from .momenti_critici_02 import Ishape
for n,l in enumerate(ls):
	i=Ishape(pr,x,y,l)
	Mcr_DM08[n]=i.Mcr
	Mrd_DM08[n]=i.Mbrd
	
import pandas as pa
dat=pa.DataFrame(c_[[ls,Mrd_CNR,Mrd_DM08]].T)
dat.columns='l Mrd_CNR Mrd_DM08'.split()
dat['delta']=dat.Mrd_DM08 - dat.Mrd_CNR
dat['deltap']=dat.delta/dat.Mrd_CNR

print(dat)

i=Ishape(pr,6000)
