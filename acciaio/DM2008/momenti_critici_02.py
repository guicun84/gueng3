#-*- coding:latin-*-

from scipy import pi,sqrt,linspace
from .tab_F1_2_r1 import F12

class IShape:
	'''Travi con sezioni trasversali uniformi doppiamente simmetriche
	Per sezioni trasversali doppiamente simmetriche � zj = 0'''
	def __init__(self,pr,x,M=None,l=None,k=1,kw=1,zg=None,fyk=275,E=210e3,ni=.25,gam=1.05,betaw=None,alphalt=.21):
		'''pr=profilo utilizzato [ipe he ipn] per upn da vedere....
		   x,M= grafico qulitativo del momento, se M=None considere semplice appoggio quradratico
		   l=luce 
		   c1,c2,c3 coefficienti per calcolo 
		   k,kw= coefficienti lunghezza efficace per flex e warp
		   zg= coordinta di applicazione carico rispetto la centro, di default bordo superiore
		   fyk,E,Ni : resistenza young e poissont acciaio
		   gam=gamma acciaio
		   betaw: W/Wpl con da prendere in funzione della classe del  1e2:Wpl , 3:Wel , 4:Weff, di default considera sez cl 3
		   alphalt= coefficiente per tipologia sezioen:
		   laminata .21
		   saldata .49
	  '''
		vars(self).update(locals())
		self.G=E/(2*(1+ni))
		if l is None: 
			if M is None:
				self.l=x
				self.x=linspace(-1,1,7)
				self.M=self.x**2
				self.M-=self.M[0]
			else:
				self.l=x.ptp()
		if betaw is None:
			self.betaw=pr.Wy/pr.Wply
		if self.zg is None: self.zg=self.pr.h/2
		self.f12=F12(self.x,self.M,self.k)
		self.Mcr=self.Mcr_()
		self.lamlt=self.lamlt_()
		self.philt=self.philt_()
		self.chilt=self.chilt_()
		self.Mbrd=self.Mbrd_()



	def Mcr_(self):
		'''Travi con sezioni trasversali uniformi doppiamente simmetriche
		Per sezioni trasversali doppiamente simmetriche � zj = 0
		pr=profilo utilizzato [ipe he ipn] per upn da vedere....
		l=luce 
		c1,c2,c3 coefficienti per calcolo 
		k,kw= coefficienti lunghezza efficace per flex e warp
		zg= coordinta di applicazione carico rispetto la centro, di default bordo superiore
		E,Ni young e poissont acciaio'''
		Iz=self.pr.Jz
		Iw=self.pr.Iwx
		It=self.pr.IT
		c1,c2,c3=self.f12.c1,self.f12.c2,self.f12.c3
		return c1*pi**2*self.E*Iz/(self.k*self.l)**2* ( sqrt( (self.k/self.kw)**2*Iw/Iz + (self.k*self.l)**2*self.G*It/(pi**2*self.E*Iz) + (c2*self.zg)**2) - c2*self.zg)



	def Mbrd_(self):
		'''calmolo del momento resistente della sezione
		chilt=coefficiente di riduzione per instabilit� flextor
		Wpl modulo resistenza plastica
		fy fy acciaio
		betaw: W/Wpl con da prendere in funzione della classe del  1e2:Wpl , 3:Wel , 4:Weff
		gam=gamma acciaio'''
		return self.chilt*self.betaw*self.pr.Wply*self.fyk/self.gam


	def chilt_(self):
		'''lamlt_=senellezza adimensionale 
		alphalt= coefficiente per tipologia sezioen:
			laminata .21
			saldata .49'''
		return min([1,1/(self.philt + sqrt(self.philt**2-self.lamlt**2))])


	def philt_(self):
		'''lamlt_=senellezza adimensionale 
		alphalt= coefficiente per tipologia sezioen:
			laminata .21
			saldata .49'''
		return .5*(1+self.alphalt*(self.lamlt-.4)+self.lamlt**2)


	def lamlt_(self):
		return sqrt(self.betaw*self.pr.Wply*self.fyk/self.Mcr)
