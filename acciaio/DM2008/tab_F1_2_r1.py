
from scipy import *
from scipy.linalg import norm
from scipy.optimize import curve_fit,minimize
import pandas as pa
import os

class Curva():
	'''classe che rappresenta una tipologia di curva con cui fittare di dati a disposizone'''
	def	__init__(self,x,y):
		vars(self).update(locals())
		self.x0=self.x-self.x[0]
		self.fit()
	
	@staticmethod
	def fun(x,*args):
		'''funzione da calcolare'''
		pass

	def __call__(self,x=None):
		if x is None: x=self.x0
		else: x=x-self.x[0]
		return self.fun(x,*self.param)

	def p0(self):
		'''valuta valore di guess iniziale per fit'''
		pass

	def fit(self):
		v=curve_fit(self.fun,self.x0,self.y,self.p0())
		self.param=v[0] #parametri di ottimizzazione
		self.param_covariance=v[1] #matrice covarianza parametri
		self.er=norm(self()-self.y) #errore di ottimizzazione

class Quad(Curva):
	'''curva quadratica'''
	@staticmethod
	def fun(x,*args):
		v0,vm,l=args
		x_=r_[0,.5,1]*l
		y_=r_[v0,vm,v0]
		p=polyfit(x_,y_,2)
		return polyval(p,x)

	def p0(self):
		v0=self.y[0]
		vm=self.y[self.y.size//2]
		l=self.x0[-1]
		return r_[v0,vm,l]


class Bilin(Curva):
	'''curva bilineare'''
	@staticmethod
	def fun(x,*args):
		y0,p,lm=args 
		'''y0=valore iniziale
		p=pendenza
		xm=x per M=Mmax'''
		return (y0+p*x)*(x<=lm) + (y0+p*lm-p*(x-lm))*(x>lm)
	
	def p0(self):
		y0=self.y[0]
		l=self.x0[-1]
		lm=l/2
		pe=(self.y[self.y.size/2]-y0)/lm
		return r_[y0,pe,lm]


class Trilin(Curva):
	'''curva trillineare'''
	@staticmethod
	def fun(x,*args):
		y0,p,lm1,lm2=args 
		'''y0=valore iniziale
		p=pendenza
		xm=x per M=Mmax'''
		return (y0+p*x)*(x<=lm1) + (y0+p*lm1)*((x>lm1)&(x<lm2)) + (y0+p*lm1-p*(x-lm2))*(x>lm2)

	def p0(self):
		y0=self.y[0]
		l=self.x0[-1]
		lm=l/3
		pe=(self.y[self.y.size/3]-y0)/lm
		return r_[y0,pe,lm,2/3*l]




class F12:
	tab=pa.read_csv(os.path.join(os.path.dirname(__file__),'tab_F1.2.csv'),sep='\s+\|\s+')

	def __init__(self,x,M,k=1,curve=7,output='best'):
		'''x= [array] ascissa
		M= [array] momento
		k= [float] coefficiente per luce libera
		curve=[int]numero per indicare le curve 
			[quad,bilin,trilin]--> 101=5 usa solo quad e trilin
		output=[str]definisce tipo di output dare:
			best=restituisce il valore della curva migliore
			wheight: pesa i valori con l'inverso degli errori
			mean: valore medio 
			list: resitiuisce la lista dei valori e degli errori'''
		if len(x)<7 and curve==7:
			print('si consiglia un campionamento minimo di 7 punti o di limitare le tipologie di curve da usare')
		vars(self).update(locals())
		self.x0=self.x-self.x[0]
		curves=[Quad,Bilin,Trilin]
		self.curve=[]
		for n in range(len(curves)): 
			if curve&(2**n):self.curve.append(curves[-(n+1)](self.x,self.M))
		
		self.cercaCurva(self.curve)
		self.cercaIncastro()

	@property
	def c1(self):
		return self.tab.c1[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]

	@property 
	def c2(self):
		return self.tab.c2[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]

	@property
	def c3(self):
		return self.tab.c3[(self.tab.diagramma==self.diagramma) & (self.tab.k==self.k)].values[0]

	def cercaCurva(self,curve=None):
		if curve is None:
			curve=self.curve
		if len(self.x)<(3):
			raise ValueError('x deve avere almeno 3 punti')
	
		self.er=r_[[i.er for i in curve]]
			
		k=where(self.er==self.er.min())[0]
		self._fun=curve[k]


	@property
	def funName(self):
		return self._fun.__class__.__name__.lower()
		

	def fun(self,x=None):
		if x is None:x=self.x
		return self._fun(x)

	def cercaIncastro(self):
		'ricerco se siamo in condizione di vincolo rigido o meno agli estremi'
		fe=self.M[0],self.M[-1]
		fi=self.M[self.M.size//2]
		if fi!=0:
			self.phi=min(abs(fe/fi))
			self.incastro=self.phi>=1
		else:
			self.phi=inf
			self.incastro=1
		if self.incastro and self.funName!='trilin':
			self.diagramma=self.funName+'i'
		else:
			self.diagramma=self.funName+'a'

if __name__=='__main__':
	n=7
	import unittest
	class MyTest(unittest.TestCase):
		def test_quada(self):
			x=linspace(-1,1,n)
			y=x**2
			y-=y[0]
			x+=1
			f=F12(x,y)
			vref=[1.132 , 0.459 , 0.525]
			self.assertEqual(f.diagramma,'quada')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)
		
		def test_quadi(self):
			x=linspace(-1,1,n)
			y=x**2
			x+=1
			y-=y[0]/(2/.99)
			f=F12(x,y)
			vref=[1.285 , 1.562 , 0.753]
			self.assertEqual(f.diagramma,'quadi')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)

		def test_blina(self):
			x=linspace(-1,1,n)
			y=abs(x)
			x+=1
			y-=y[0]
			f=F12(x,y)
			vref=[1.365 , 0.553 , 1.730]
			self.assertEqual(f.diagramma,'bilina')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)
		
		def test_blini(self):
			x=linspace(-1,1,n)
			y=abs(x)
			x+=1
			y-=y[0]/2*.99
			f=F12(x,y)
			vref=[1.565 , 1.267 , 2.64]
			self.assertEqual(f.diagramma,'bilini')
			self.assertAlmostEqual(f.c1,vref[0],4)
			self.assertAlmostEqual(f.c2,vref[1],4)
			self.assertAlmostEqual(f.c3,vref[2],4)


	unittest.main(verbosity=2)
