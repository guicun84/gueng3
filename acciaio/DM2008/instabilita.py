#-*-coding:latin-*-

from scipy import *
from scipy.optimize import minimize,curve_fit
import pandas as pa
import io,re

############################################################
#instabilit� per compressione
def Ncr(J,L,E=210e3):
	'''calcolo momento critico euleriano
	J=modulo di inerzia
	L=luce libera
	E=Modulo di young'''
	return pi**2*E*J/L**2

def phi(lam,alpha=.34):
	'''lam=snellezza [sqrt(Nrk/Ncr)]
	alpha= fattore di imperfezione tabella 4.2VI default curva b'''
	return .5*(1+alpha*(lam-.2)+lam**2)

def chi_(phi,lam):
	return min([ 1/(phi+sqrt(phi**2-lam**2)) , 1])

def chi(J,L,A,fyk=275,E=210e3,alpha=.34):
	'''calcolo di chi per verifica instabilit�
	J= modulo di inerzia
	L=luce libera
	A=Area sezione
	fyk=resistenza caratteristica
	E=Modulo di Young
	alpha= fattore di imperfezione tabella 4.2VI default curva b'''
	Ncr_=Ncr(J,L,E)
	lam=sqrt(A*fyk/Ncr_)
	phi_=phi(lam,alpha)
	return chi_(phi_,lam)

############################################################
#instabilit� flessionale:
def psi(Ma,Mb):
	'''Ma,Mb: momenti agli estremi del profilo'''
	M=abs(r_[Ma,Mb])
	M_=M.min()/M.max()
	return 1.75-1.05*M_+.3*M_**2

def Mcr(psi,Lcr,J,Jt=None,Jw=None,E=210e3,G=210e3/2/(1+.3)):
	'''psi: psi dovuto ai momenti
	Lcr=distanza tra ritegni torsionali
	J=momento di inerzia asse instabilit� oppure profilo
	Jt= momento di inerzia torsionale oppure 'y,z[debole]' per modulo da usare
	Jw= modulo di imbozzamento
	E= modulo elastico
	G=Modulo di taglio

	se J � un profilo Jw e Jt sono pre4si da esso'''
	if Jw is None: 
		pr=J
		if Jt=='y':J=pr.Jy
		else: J=pr.Jz
		Jt=pr.IT
		Jw=pr.Jw
	return psi*pi/Lcr * sqrt( E*J * G*Jt) * sqrt(1+(pi/Lcr)**2*E*Jw/(G*Jt))

def philt (lamlt,alphalt=.34,lamlt0=.4,beta=.75):
	'''calcolo phi per instabilit� flessionale
	lamlt= snellezza flessionale [Sqrt(Mrk/Mcr)]
	alpha= fattore di imperfezione tabella 4.2VI default curva b
	lamlt0: in genere .2, sempre minore di .4 (.4 � valore consigliato per ezioni laminati e saldati)
	beta: consigliato 1, sempre maggiore di .75(.75 � valore consigliato per sezioni laminate e saldate)'''
	return .5*(1 + alphalt * (lamlt-lamlt0) +beta*lamlt**2)

def f(lamlt,kc=1):
	'''fattore correttivo
	lamlt=snellezza flessionale
	kc:fattore correttivo per momento'''
	return 1-0.5*(1-kc)*(1-2*(lamlt-.8)**2)

def kc(M,soglia=1e-3):
	#interpolazione lineare:
	if not (all((diff(M)/diff(M)[0]).std()<=soglia) or all(M==M[0])): #caso dichiaratamente lineare:
		x=arange(M.size)
		pl=polyfit(x,M,1)
		erl=(M-polyval(pl,x)).std()
		#interpolazione quadratica
		pq=polyfit(x,M,2)
		erq=(M-polyval(pq,x)).std()
		#interpolazione bilineare
		def f(x,p1,v0,p2,x_):
			if x<=x_: return v0+p1*x
			else: return v0+p1*x_ + p2*(x-x_)
		f=vectorize(f)
		v2=M[-1]-(M[-1]-M[-2])*x.max()
		inc=(M[0]-v2)/((M[-1]-M[-2]) - (M[1]-M[0]))
		initial_guess=r_[M[0] , M[1]-M[0] , M[-1]-M[-2],inc]
		val=curve_fit(f,x,M,initial_guess)
		
		erbl=(M-f(x,*val[0])).std()
		emin=min([erl,erq,erbl])	
		caso={erl:'l', erq:'q', erbl:'bl'}[emin]
	else: caso='l'
	if caso=='l':
		try: psi=M[0]/M[-1] #nel caso un momento sia nullo
		except :psi=M[-1]/M[0]
		if abs(psi)>1: psi=1/psi
		k= 1/(1.33-.33*psi)
	if caso =='q':
		if all(abs( M[[0,-1]]/abs(M).max())<=soglia): k= .94 #momenti incastro nulli
		elif any(abs(M[[0,-1]]/abs(M).max())<=soglia): k= .91 #1 momento incastro nullo
		else: k= .9 #momenti di incastro
	if caso =='bl':
		if all(abs(M[[0,-1]]/abs(M).max())<=soglia): k= .86 #momenti incastro nulli
		elif any(abs(M[[0,-1]]/abs(M).max())<=soglia): k= .82 #1momento incastro nullo
		else: k= .77 #momenti di incastro
	return k


def chilt_(philt,lamlt,beta=1,f=.77,lamlt0=.4):
	'''philt= coefficiente per calcolo fattore di riduzine a flessione
	lamlt =snellezza flessionale
	beta=1 valore consigliato per sezioni laminate e saldate
	f=.77 fattore correttivo per distribuzione momento(bilineare con momento di incastro)
	lamlt0: in genere .2, sempre minore di .4 (.4 � valore consigliato per ezioni laminati e saldati)'''
	if lamlt<=lamlt0**2: chi=1.
	else: chi= 1/f*1/(philt + sqrt(philt**2-beta*lamlt**2))
	return min([1,1/f/lamlt**2,chi])
#	return chi

def chilt(Lcr,W,J=None,Jt=None,Jw=None,M=None,fyk=275,F=.77,alphalt=.34,lamlt0=.4,beta=.75,E=210e3,G=210e3/2/(1+.3)):
	'''calcola chilt per instabilit� flessionale
	Lcr=luce tra ritegni torsionali
	W=W della sezione [o profilo]
	J=momento di inerzia della sezione
	Jt=momento di inerzia torcente della sezione
	Jw= costante di ingobbamento della sezione
	M=Momenti lungo la trave
	fyk= resistenza caratteristica
	F=coefficiente di riduzione momento se 0 lo calcola
	alphalt= coefficiente di imperfezione sezione
	lamlt0=.4 consigliato per sezioni laminate o saldate
	beta=.75 consigliato per sezioni laminate o saldate

	Se W � un profilo e A � z,y i valori relativi alla sezione sono caricati automaticamente'''
	if Jt is None:
		pr=W
		if J=='y':
			J=pr.Jy
			W=pr.Wy
		elif J=='z': 
			J=pr.Jz
			W=pr.Wz
		Jt,Jw=pr.IT,pr.Iwx
	if M is None:M=r_[0,1.] #...cosi facendo psi=psi.max
	psi_=psi(M[0],M[1])
	Mcr_=Mcr(psi_,Lcr,J,Jt,Jw,E,G)
	lamlt=sqrt(W*fyk/Mcr_)
	philt_=philt(lamlt,alphalt,lamlt0,beta)
	if F==0: 
		kc_=kc(M)
		F=f(lamlt,kc_)
	chilt=chilt_(philt_,lamlt,beta,F)
	return locals()

dati_='''
psi   | k   | C1    | C3
1.000 | 1.0 | 1.000 | 1.000
1.000 | 0.7 | 1.000 | 1.113
1.000 | 0.5 | 1.000 | 1.144
0.750 | 1.0 | 1.141 | 0.998
0.750 | 0.7 | 1.270 | 1.565
0.750 | 0.5 | 1.305 | 2.283
0.500 | 1   | 1.323 | 0.992
0.500 | 0.7 | 1.473 | 1.556
0.500 | 0.5 | 1.514 | 2.271
0.25  | 1   | 1.563 | 0.977
0.25  | .7  | 1.739 | 1.531
0.25  | .5  | 1.788 | 2.235
0.00  | 1   | 1.879 | 0.939
0.00  | .7  | 2.092 | 1.473
0.00  | .5  | 2.15  | 2.15
-.25  | 1   | 2.281 | .855
-.25  | .7  | 2.538 | 1.34
-.25  | .5  | 2.609 | 1.957
-.5   | 1   | 2.704 | 0.676
-.5   | .7  | 3.009 | 1.059
-.5   | .5  | 3.093 | 1.546
-.75  | 1   | 2.927 | 0.366
-.75  | .7  | 3.009 | .575
-.75  | .5  | 3.093 | 0.873
-1    | 1   | 2.752 | 0
-1    | .7  | 3.063 | 0
-1    | .5  | 3.149 | 0 '''
dati=io.StringIO()
dati.write(re.sub(' +','',dati_))
dati.seek(0)
dati=pa.read_csv(dati,sep='|',header=0,skiprows=1)

dati2_='''
tipo   | k   | C1    | C2     | C3
quad00 | 1.0 | 1.127 | 0.454  | 5.25
quad00 | 0.5 | 0.972 | 0.404  | 0.98
bl00   | 1.0 | 1.348 | 0.630  | 1.730
lb00   | 0.5 | 1.070 | 0.432  | 3.05
tl     | 1.0 | 1.046 | 0.430  | 1.12
tl     | 0.5 | 1.01  | 0.41.0 | 1.89
quad11 | 1.0 | 2.578 | 1.554  | 0.753
quad11 | 0.5 | 0.712 | 0.652  | 0.107
bl11   | 1.0 | 1.683 | 1.645  | 2.640
bl11   | 0.5 | 0.938 | 0.715  | 4.8'''
dati2=io.StringIO()
dati2.write(re.sub(' +','',dati2_))
#dati2.write(dati2_)
dati2.seek(0)
dati2=pa.read_csv(dati2,sep='|',header=0,skiprows=1)




def Meq(M,soglia=1e3):
	'''Calcolo momento equivalente per verifica instabilit� pressoflessionale
	M=vettore con i momenti lungo la trave'''
	#caso trave appoggiata:
	Mm=M.mean()
	Meq=min([max([Mm,.75*abs(M).max()]),abs(M).max()])
	if not all (abs(M[0,-1]/Meq))<soglia: #considero momenti di incastro
		M_=abs(M[0,-1])
		M_.sort()
		Meq=max([.6*M_[1]-.4*M[0],.4*M[1]])
	return Meq

if __name__=='__main__':
	import unittest
	class Test_kc(unittest.TestCase):
		def test_costante(self):
			M=ones(5)
			k=kc(M)
			self.assertEqual(k['k'],1)
			self.assertEqual(k['caso'],'l')

		def test_lineare(self):
			M=linspace(1,0,5)
			k=kc(M)
			self.assertEqual(k['k'],1/1.33)
			self.assertEqual(k['caso'],'l')

		def test_quad_00(self):
			M=arange(5)-2
			M=M**2
			M-=M[0]
			k=kc(M)
			self.assertEqual(k['k'],.94)
			self.assertEqual(k['caso'],'q')

		def test_quad_11(self):
			M=arange(5)-2
			M=M**2
			k=kc(M)
			self.assertEqual(k['k'],.9)
			self.assertEqual(k['caso'],'q')

		def test_quad_01(self):
			M=arange(5)-1
			M=M**2
			M-=M[0]
			k=kc(M)
			self.assertEqual(k['k'],.91)
			self.assertEqual(k['caso'],'q')

		def test_blin_00(self):
			M=r_[0,1,2,1,0]
			k=kc(M)
			self.assertEqual(k['k'],.86)
			self.assertEqual(k['caso'],'bl')

		def test_blin_11(self):
			M=r_[0,1,2,1,0]-1
			k=kc(M)
			self.assertEqual(k['k'],.77)
			self.assertEqual(k['caso'],'bl')

		def test_blin_01(self):
			M=r_[0,1,0,-1,-2]
			k=kc(M)
			self.assertEqual(k['k'],.82)
			self.assertEqual(k['caso'],'bl')


	unittest.main(verbosity=2)
