from numpy import pi
from pandas import Series
class AstaComposta:
	'calcola la calastrellatura di 2 correnti'
	def __init__(self,pr,h0,L,Ad,Av,Jv,a,d=None,schema=4,E=210e3,multLch=1,n=1):
		'''pr=profilo montante
		   h0=distanza tra baricentri correnti
		   l=luce elemento
		   Ad=Av=area diagonali,area calastrelli
		   Jv=modulo inerzia calastrelli
		   a=interasse calastrelli
		   d=lunghezza puntone inclinato(se presente)
		   schema=[1|2|3|4] figura C4.2.7 aste composte pag 102 circolare 2019
		   E=modulo elastico
		   multLch=1 vedi figura C4.2.8 pag 103 circolare 2019
		   n=numero di piani di tralicciatura

		   schema 1 tirante,traverso,puntone [reticolare alternata con traverso]
		   schema 2 tirante,puntone [reticolare alternata senza traverso]
		   schema 3 tirante,traverso (oppure punone traverso) [reticolare]
		   schema 4 datraverso,traverso [calastrelli]
		'''
		vars(self).update(locals())
		self.e0=self.L/500
		if d is None:
			self.d=(a**2+h0**2)**.5
		self.Ac=pr.A #area corrente
		self.Jc=min((pr.Jz,pr.Jy)) #Jcorrente
		if 'Jv' in pr.keys():
			self.Jc=min((self.Jc,pr.Jv,pr.Ju))
		self.lamb= self.L*(2*self.Ac/(0.5*self.h0**2*self.Ac+2*self.Jc))**.5 #snellezza
		#coefficiente di efficienza
		if self.lamb<75: self.mu=1
		elif self.lamb>150:self.mu=0
		else:self.mu=interp([75,150],[1,0],self.lamb)
		#momento efficace sezione composta
		self.Jeff=0.5*self.h0**2*self.Ac+2*self.mu*self.Jc
		self.Ncr= pi**2*E*self.Jeff/self.L**2
		#calcolo rigidezza a taglio della calastrellatura
		if self.schema==1:
			self.Sv= self.n*self.E*self.Ad*self.a*self.h0**2/self.d**3
		elif self.schema==2:
			self.Sv= self.n*self.E*self.Ad*self.a*self.h0**2/self.d**3/2
		elif self.schema==3:
			self.Sv= self.n*self.E*self.Ad*self.a*self.h0**2/(self.d**3*(1+self.Ad*self.h0**3/self.Av/self.d**3))
		elif self.schema==4:
			self.Sv= min((
				24*self.E*self.Jc/self.a**2/(1+2*self.Jc*self.h0/self.n/self.Jv/self.a),
				2*pi**2*self.E*self.Jc/self.a**2))
		else: raise ValueError(f'self.schema deve essere compreso tra 1 e 4')
	def Nced(self,Ned,Med1):
		'calcolo azione normale di progetto su asta composta'
		return 0.5*Ned+self.Med(Ned,Med1)*self.h0*self.Ac/2/self.Jeff
	
	def Med(self,Ned,Med1):
		'calcolo momento di progetto su asta composta'
		return (Ned*self.e0 + Med1)/(1-Ned/self.Ncr-Ned/self.Sv)

	def Ved(self,Ned,Med1):
		return pi*self.Med(Ned,Med1)/self.L

	@property
	def sezione_eq(self):
		out= Series(dict(
			A=self.Ac*2,
			Jy=self.Jeff,
			Jz=2*self.Jc,
		))
		return out


