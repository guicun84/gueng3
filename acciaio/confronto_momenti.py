
from gueng.profilario import prof
from .CNR import omega_1
from .EC3.momenti_critici_02 import Ishape as Iec
from .DM2008.momenti_critici_02 import Ishape as Idm
from gueng.utils import toDict
from scipy import linspace
import pandas as pa
pr=prof.ipe300

fy=275

x=linspace(5,7)*1e3
dat=pa.DataFrame()
var='l M_cnr M_ec M_dm'
for l in x:
	om1=omega_1(fy,pr.h,l,pr.b,pr.tf)
	M_cnr=pr.Wply*275/1.15/1.4/om1/(1.3*2/3) #considero Meq non M nelle verifiche...
 
	i=Iec(pr,l)
	M_ec=i.Mbrd

	i=Idm(pr,l)
	M_dm=i.Mbrd
	dat=dat.append(toDict(var),ignore_index=1)

dat=dat[var.split()]

dat['dm_cnr']=(dat.M_dm-dat.M_cnr)/dat.M_cnr
dat['ec_cnr']=(dat.M_ec-dat.M_cnr)/dat.M_cnr
dat['dm_ec']=(dat.M_dm-dat.M_ec)/dat.M_ec

figure(1)
show(0)
clf()
dat.plot('l','M_cnr M_dm M_ec'.split())
legend()
draw()

figure(2)
show(0)
clf()
dat.plot('l','dm_cnr ec_cnr dm_ec'.split())
legend()
draw()
