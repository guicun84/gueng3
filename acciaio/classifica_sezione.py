
from scipy import *


class IClassifier:
	"classe per classificare i profili a I"
	def __init__(self,pr=None,fyk=275,bi=None,hi=None,tf=None,tw=None):
		'''pr=profilio se non si possono passare i valori bi ... tw
		   fyk= tensione caratteristica snervametnto
		   bi ... tw : base altezza spessore ala e anima di profilo generico
		   '''
		vars(self).update(locals())
		self.ep=sqrt(235/self.fyk)
		if pr is not None:
			#controllo se è upn:
			if type(pr.name)==str: self.isupn='UPN' in pr.name
			else: self.isupn='UPN' in pr['name']
			if self.isupn:
				self.bi=(pr.b-pr.r1-pr.tw)
				self.hi=pr.h-2*(pr.r1+pr.tf)
			else:
				self.bi=(pr.b-2*pr.r-pr.tw)/2
				self.hi=pr.h-2*(pr.r+pr.tf)
			self.tf=pr.tf
			self.tw=pr.tw
			
		self.refAnim=self.hi/self.tw
		self.refAla=self.bi/ self.tf

	def classificaAnima(self,M=1,N=0):
		if N==0: 
			#solo momento flettente
			limiti=r_[72,83,124]*self.ep
			out=where(self.refAnim<=limiti)
			if out[0].size: return min(out[0])+1
			else: return 4
		elif M==0:
			#sola compressione 
			limiti=r_[33,38,42]*self.ep
			out=where(self.refAnim<=limiti)
			if out[0].size: return min(out[0])+1
			else: return 4
		else:
			#presso flessione
			pass
	
	def classificaAla(self,Ni=1,Mi=0):
		'''Ni= compressione sull'ala anche dovuta a flessione
		   Mi= momento su ala dovuto a flessione su asse debole'''
		if Ni==0: #solo flessione
			pass
		elif Mi==0: #solo comporessione
			limiti=r_[9,10,14]*self.ep
			out=where(self.refAla<=limiti)[0]
			if out.size:return min(out)+1
			else: return 4

		else: #pressoflessione
			pass

	def classifica(self,M=1,N=0,Ni=1,Mi=0):
		return max([self.classificaAnima(M,N),self.classificaAla(Ni,Mi)])
	
	@property
	def clFlex(self):
		return self.classifica(1,0,1,0)

	@property
	def clComp(self):
		return self.classifica(0,1,1,0)
