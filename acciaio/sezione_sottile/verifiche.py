#-*- coding:latin

from scipy import *

gamma_m0=1.1
gamma_m1=1.25
gamma_m2=1.25

def Vwrd(parte,irg=False):
	'''calcolo della resistenza a taglio della parte
	parte = lato da valutare
	irg= irrigidimento bool'''
	eps=sqrt(235/parte.fyb) #pag 22 ec3 parte 3
	hw=abs(parte.i.co[1]-parte.j.co[1]) #altezza anima
	dx=abs(parte.i.co[0]-parte.j.co[0]) #delta x tra inizio e fine
	sw=parte.lunghezza #lunghezza
	t=parte.s #spessore
	testPlastic=sw/t<=72*eps #se vero la resistenza all'instabilità per taglio va verificata
#	assert not testPlastic ,'controllo instabilità a taglio non ancora implementato'
	phi=arctan2(hw,dx) #in verticale deve essere 90°


	if not irg:
		lamw=sw/(t*86.3*eps) #snellszza relativa anima
	#valutazione resistenza ad instabilità per taglio
	if lamw<1.4:
		fbv=.48*parte.fyb/lamw
	else:
		fbv=.48*parte.fyb/lamw**2

	Vbrd=(hw/sin(phi))*t*fbv/gamma_m1 #resistenza elastica (per instabilità) Vbrd
	Vplrd=(hw/sin(phi))*t*(parte.fyb/sqrt(3))/gamma_m0
	return min((Vbrd,Vplrd)),locals()

def Rwrd(parti,Vsd1,Vsd2,ss,c=None,e=None,r=None,isLamiera=False,irg=False,checkTest=False,E=210e3):
	'''calcolo della resistenza locale dell'anima a taglio
	parti= lista delle parti da calcolare
	Vsd1,Vsd2 = valore del taglio nelle sezioni a cavallo della forza concentrata
	c=distanza dall'appoggio [min 40mm]
	e=distanza tra due zone di carico concentrato
	ss=larghezza zona di appoggio
	isLamiera=False  true se si tratta di grecata
	irg= bool irrigidito
	checkTest=False: controlla parametri dimensionali minimi
	E=modulo Elastico'''
	if not hasattr( parti,'__len__'):
		parti[parti]
#	------------------------------------------------------------
#			 ricerca del'altezza della sezione
	hws=r_[[abs(p.i.co[1]-p.j.co[1]) for p in parti]] #altezze
	ts=r_[[p.s for p in parti]] #spessori
	fybs=r_[[p.fyb for p in parti]]
	fyb=min(fybs)
	t=min(ts)
	hw=max(hws)
	phis=[arctan2(abs(p.i.co[1]-p.j.co[1]) , abs(p.i.co[0]-p.j.co[0])) for p in parti] #angoli
	phi=max(phis)
	if r is None: r=4*ts.max()

	Vsd1=abs(Vsd1)
	Vsd2=abs(Vsd2)
	if Vsd2>Vsd1: 
		Vsd1,Vsd2=Vsd2,Vsd1
	betaV=(Vsd1-Vsd2)/(Vsd1+Vsd2)
	#------------------------------------------------------------
	#classifico il tipo di forza concentrata
	if e is not None:
		if e<=1.5*hw: cat=1
		else: cat=2
	elif c is not None:
		if c<=1.5*hw: cat=1
		else: cat=2
	else:
		cat=2 #appoggio intermedio

	#----------------------------------------
		    #controllo numero parti
	if len(parti)==1:
		pass
	else:
		test=all(hws/ts <= 200*sin(phis)) & all(phis<=ones_like(phis)*pi/2) & all(phis>=ones_like(phis)*pi/4)
		if not test:
			print('criteri iniziali non soddisfatti')
			print(hws/ts ,'<=', 200*sin(phis))
			print(phis ,'<=',pi/2)
			print(phis ,'>=', pi/4)
			if checkTest:raise ValueError


	#	alpha= coefficiente tab 5.11
		if cat ==1:
			if isLamiera: alpha=.075
			else:alpha=.057
		else:
			if isLamiera: alpha=.15
			else: alpha=.115
	#	r=raggio di curvatura
	#	la=lunghezza portate efficace
		if cat==1: la=10
		else:
			if betaV<=.2: la=ss
			elif betaV>=.3: la=10
			else: interp((.2,.3),(ss,10),betaV)

		Rwrds=r_[[alpha*t**2*sqrt(fyb*E)*(1-0.1*sqrt(r/t))*(0.5+sqrt(.02*la/t))*2.4+(phi/(pi/2))**2/gamma_m1 for t,fyb,phi in zip(ts,fybs,phis)]]
		Rwrd=Rwrds.sum()
		

	return Rwrd,locals()





