#-*-coding:utf-8-*-

from gueng.utils import *
from pylab import *
from scipy.linalg import norm
from scipy.optimize import minimize_scalar as minimize
from scipy import *
from copy import deepcopy
'''implemento il metodo semplificato che per l'assottigliament non calcola l'effettiva instabilità degli irrigidimenti ma la approssima senza considerare la lunghezza del profilo stesso'''

class Nodo:
	def __init__(self,x,y):
		self.co=r_[x,y].astype(float)

class Rect:
	'''definisce un lato del profilo'''
	def __init__(self,i,j,s):
		'''i,j nodo di inizio e fine
		s=spessore'''
		vars(self).update(locals())
		self.l=norm(self.j.co-self.i.co)
		self.e1=self.j.co-self.i.co		
		self.e1/=norm(self.e1)
		self.e2=cross(r_[0,0,1.],r_[self.e1,0])[:2]
		self.e2/=norm(self.e2)
		self.definisci()

	def definisci(self):
		'''noti i punti di inizio e fine e lo spessore definisce gli estremi del rettagngolo'''
		self.punti=r_[[self.i.co-self.e2*self.s/2. , self.i.co-self.e2*self.s/2.+self.e1*self.l , self.i.co+self.e2*self.s/2.+self.e1*self.l , self.i.co+self.e2*self.s/2.]]
		self.punti=r_[self.punti,[self.punti[0,:]]]

	@property
	def Sx(self):
		'''momento statico rispetto asse x'''
		S=0
		for n in range(self.punti.shape[0]-1):
			i=self.punti[n]
			j=self.punti[n+1]
			b=j[0]-i[0]
			h1=i[1]
			h2=j[1]-i[1]
			S+=b*h1**2/2 + b*h2/2*(h1+h2/3)
		return -S
	
	@property
	def A(self):
		a=0
		for n in range(self.punti.shape[0]-1):
			i=self.punti[n]
			j=self.punti[n+1]
			b=j[0]-i[0]
			h1=i[1]
			h2=j[1]-i[1]
			a+=b*h1+b*h2/2
		return -a
	
	@property
	def Yg(self):
		return self.Sx/self.A

	def plot(self,*args,**kargs):
		x,y=list(zip(*self.punti))
		plot(x,y,*args,**kargs)
		axis('equal')

	@property
	def Jx_(self):
		'''calcolo il momento di inerzia rispetto all'asse x'''
		jx=0
		for n in range(self.punti.shape[0]-1):
			i=self.punti[n]
			j=self.punti[n+1]
			b=j[0]-i[0]
			h1=i[1]
			h2=j[1]-i[1]
			jx+=b*h1**3/12 + b*h1*(h1/2)**2 + b*h2**3/36 + b*h2/2*(h1+h2/3)**2
		return -jx

class Parte(Rect):
	def __init__(self,parent,i,j,s):
		Rect.__init__(self,i,j,s)
		self.parent=parent
		
			
class Lato:
	def __init__(self,i,j,s,fyb):
		vars(self).update(locals())
		self.e1=self.j.co-self.i.co		
		self.e1/=norm(self.e1)
		self.e2=cross(r_[0,0,1],r_[self.e1,0])[:2]
		self.e2/=norm(self.e2)
		self.l=zeros(6) #distanze progressive dal bordo iniziale dei vari tratti
		#quelle pari sono vuoti quelle dispari piene
		self.l[1]=norm(self.j.co-self.i.co)
		self.lunghezza=self.l[1]
		self.componi()

	def componi(self):
		self.parti=[]
		for i in range(1,6,2):
			if self.l[i]:
				s=self.i.co+self.e1*self.l[i-1]
				e=self.i.co+self.e1*self.l[i]
				self.parti.append(Parte(self,Nodo(*s),Nodo(*e),self.s))

	@property
	def Sx(self):
		'''momento statico rispetto asse x'''
		return sum([i.Sx for i in self.parti])
		
	@property
	def A(self):
		return sum([i.A for i in self.parti])

	@property	
	def Yg(self):
		return self.Sx/self.A
	
	@property
	def Jx_(self):	
		'''momenti di inerzia rispetto all'asse x (non baricentrico)'''
		return sum([i.Jx_ for i in self.parti])

	def plot(self,*args,**kargs):
		for i in self.parti:
			i.plot(*args,**kargs)

	def seziona_1_2(self,si,sj,sig_com_ed=None):
		'''si,sj=tensioni agli estremi del tratto'''
		#valutazione del coefficiente ksigma
		if any(r_[si,sj]<=0):
			#ho della compressione e devo dividere il segmento in parti
			si=-si
			sj=-sj
			s1=max([si,sj])
			s2=min([si,sj])
			psi=s2/s1
			if isnan(psi):psi=1
			if psi==1:
				k_sig=4
			elif 1>psi>0:
				k_sig=8.2/(1.05+psi)
			elif psi==0:
				k_sig=7.81
			elif 0>psi>-1:
				k_sig=7.81-6.29*psi+9.78*psi**2
			elif psi==-1:
				k_sig=23.9
			elif -1>psi:
				k_sig=5.98*(1-psi)**2
			#valutazione del coefficiente rho
			if not sig_com_ed:
				sig_com_ed=max([s1,s2])
#				print 's1={s1},s2={s2},sig_com_ed={sig_com_ed}'.format(**locals())
			ep=sqrt(235/self.fyb)
			lam_p=self.lunghezza/self.s/28.4/ep/sqrt(k_sig) 
			lam_p_red=lam_p*(sqrt(sig_com_ed/self.fyb*1.25))
			#formule dell'alternativa 2 ,se lam_p==lam_p_red il secondo membro della #a si annulla e torna tutto ok ;)
			if lam_p_red<=.673:
				rho=1.
			elif lam_p_red>.673:
				rho=(1-.22/lam_p_red)/lam_p_red + 0.18*(lam_p-lam_p_red)/(lam_p-0.6) #a
			else:
				print('WARNING lam_p_red ={:.4g}<0'.format(lam_p_red))
				rho=1
			rho=min([1.,rho])
			#calcolo le lunghezze efficaci:
			if psi<1:# ho inversione di tensione devo trovare la base compressa:
				b=self.lunghezza/(abs(s1)+abs(s2))*abs(s1)
			else:
				b=self.lunghezza
			beff=rho*b
			if psi==1:
				be1=.5*beff
				be2=.5*beff
			elif 1>psi>=0:
				be1=2*beff/(5*psi)
				be2=beff-be1
			elif 0>psi:
				be1=.4*beff
				be2=.6*beff
			#finite di calcolare le lunghezze efficaci devo determinare le lunghezze dei rettangoli da utilizzare:
			#in prima baattuta controllo che siano girate nell'ordine effettivo del lato:
			#controllo che la somma delle basi sia minore di quella del lato:
#			if be1+be2==self.lunghezza:
#				self.l[0:2]=r_[0,self.lunghezza]
#			else:
			self.l[0:4]=r_[0,be1,b-be2,self.lunghezza]
			if si<sj:
				self.l[0:4]=-self.l[3::-1]+self.lunghezza

	def seziona_1_1(self,si,sj,p,sig_com_ed=None):
		'''si,sj tesnioni agli estremi del lato'''
		if all(r_[si,sj]<0): #se ho compressione
			si=-si
			sj=-sj
			s1=max([si,sj])
			s2=min([si,sj])
			psi=s2/s1

			#devo definire se la tensione aumenta o decresce dal bordo vincolato:
			if p==self.i:
				sv=si
				sl=sj
			else:
				sv=sj
				sl=si
			cr=sign(sl-sv)

			if cr>1:
				if psi==1:
					k_sig=.43
				elif psi==0:
					k_sig=.57
				elif psi==-1:
					ksig==.85
				elif 1>=psi>=-1:
					ksig=.57-.21*psi+.07*psi**2
					
			else:
				if psi==1:
					k_sig=.43
				elif 1>psi>0:
					k_sig=.578/(psi+.34)
				elif psi==0:
					k_sig=1.7
				elif 0>psi>-1:
					k_sig=1.7-5*psi+17.1*psi**2
				elif psi==1:
					k_sig=23.8

			if not sig_com_ed:
				sig_com_ed=max([s1,s2])
			ep=sqrt(235/self.fyb)
			lam_p=self.lunghezza/self.s/28.4/ep/sqrt(k_sig) 
			lam_p_red=lam_p*(sqrt(sig_com_ed/self.fyb*1.25))
			#formule dell'alternativa 2 ,se lam_p==lam_p_red il secondo membro della #A si annulla e torna tutto ok ;)
			if lam_p_red<=.673:
				rho=1.
			else:
				rho=(1-.22/lam_p_red)/lam_p_red + 0.18*(lam_p-lam_p_red)/(lam_p-0.6) #A
			#calcolo le lunghezze efficaci:
			if psi<1:# ho inversione di tensione devo trovare la base compressa:
				b=self.lunghezza/(abs(s1)+abs(s2))*abs(s1)
			else:
				b=self.lunghezza
			beff=rho*b
			if psi>=0 and cr>=0:
				self.l[:2]=r_[0,beff]
			elif psi<0 and cr>=0:
				self.l[:2]=r_[0,beff+self.lunghezza-b+beff]
			elif psi>=0 and cr<0:
				self.l[0:2]=r_[0,beff]
			elif psi<0 and cr<0:
				self.l[:4]=r_[0,beff,self.lunghezza-b,self.lunghezza]

			#devo ora valutare se eseguire lo switch
			if p==self.j:
				if psi<0 and cr<0:
					self.l[:4]=self.lunghezza-self.l[3::-1]
				else:
					self.l[:2]=self.lunghezza-self.l[1::-1]

			
class Sezione:
	def __init__(self,punti,s,fyb=275,N=0,M=0):
		'''punti= lista di Nodi della sezione
		s=spessore sezione
		fyb= tensione di snervamento 
		N=sforzo normale su sezione
		M=Momento flettente su sezione'''
		vars(self).update(locals())
		self.inizializza_lati()
		self.componi()
	
	def inizializza_lati(self):
		self.lati=[]
		for i in range(len(self.punti)-1):
			self.lati.append(Lato(self.punti[i],self.punti[i+1],self.s,self.fyb))

	def componi(self):
		for i in self.lati:
			i.componi()
	
	def plot(self,*args,**kargs):
		for i in self.lati:
			i.plot(*args,**kargs)
	
	@property
	def Sx(self):
		'''momento statico rispetto asse x'''
		return sum([i.Sx for i in self.lati])
	
	@property
	def A(self):
		return sum([i.A for i in self.lati])
	
	@property
	def Yg(self):
		return self.Sx/self.A
		
	@property
	def Jx_(self):	
		'''calcolo momento di inerzia rispetto ad asse x'''
		return sum([i.Jx_ for i in self.lati])
	
	@property
	def Jx(self):
		'''calcolo momento di inierzia baricetrico'''
		#decurto dal momento di inerzia sull'asse x quello di trasporto
		return self.Jx_ - self.A*self.Yg**2

	@property
	def h(self):
		return r_[[i.co[1] for i in self.punti]].ptp()	
	
	@property
	def Wx(self):
		'''modulo di resistenza minimo'''
		y=r_[[i.co[1] for i in self.punti]]	
		h=abs(r_[y.min(),y.max()]-self.Yg).max()
		return self.Jx/h

	def sigma(self,y):	
		return self.M/self.Jx*(self.Yg-y) + self.N/self.A
	
	def conta(self,nodo):
		'''conata quanti lati concorrono in un nodo'''
		return len([i for i in self.lati if nodo in [i.i,i.j]])
	

	def seziona_1(self,force_sig_com=False):	
		'''calcola le lunghezze efficaci dei bordi'''
		if force_sig_com:
			sig_com_ed=self.fyb/1.25
		else:
			sig_com_ed=None
		for lato in self.lati:
			yi=lato.i.co[1]
			yj=lato.j.co[1]
			si=self.sigma(yi)
			sj=self.sigma(yj)
			ni=self.conta(lato.i)
			nj=self.conta(lato.j)
			if all(r_[ni,nj]==2): #su entrambi i nodi sono presenti irrigidimenti
				lato.seziona_1_2(si,sj,sig_com_ed)
			else: #su uno dei due nodi è presente un irrigidimento
				if ni==2: 
					vincolo=lato.i
				else:
					vincolo=lato.j
				lato.seziona_1_1(si,sj,vincolo,sig_com_ed)
		self.componi()

	def cerca_blocchi(self,eps=1e-6):
		'''cerco i blocchi irrigiditi della sezione:'''
		self.blocchi=[]
		parti=[]
		for nodo in self.punti:
			#controllo che il nodo sia compresso se non non ha senso assottigliare successivamente il tutto.
			if self.sigma(nodo.co[1])>0:
				self.blocchi.append(parti)
				parti=[]
				continue
			if self.conta(nodo)>1: #ha senso se nel nodo ci sono più di un elemento:
				lj=[lato for lato in self.lati if lato.j==nodo][0]
				li=[lato for lato in self.lati if lato.i==nodo][0]
				parti.extend([lj.parti[-1],li.parti[0]])
				l_tot=sum([part.l for part in li.parti])
				if l_tot/li.lunghezza<1-eps:
#					print 'parti non continue passo a nuovo blocco'
#					print l_tot/li.lunghezza
					self.blocchi.append(parti)
					parti=[]
		self.blocchi.append(parti)
		self.blocchi=[i for i in self.blocchi if i]
		for n,i in enumerate(self.blocchi):
			self.blocchi[n]=Blocco(i,self.fyb)
		
	def assottiglia(self):
		'''applico i metodi semplificati che trascurano il calcolo esatto delle rigidezze degli irrigidimenti'''
		self.cerca_blocchi()
		for Blocco in self.blocchi:
			Blocco.assottiglia()

	def ruota(self,ang):
		punti=[]
		mrot=r_[[[cos(ang),-sin(ang)],[sin(ang),cos(ang)]]]
		for i in self.punti:
			punti.append(dot(mrot,i.co))
		punti=[Nodo(*i) for i in punti]
		return Sezione(punti,self.s,self.fyb,self.N,self.M)
		
		



class Blocco:
	def __init__(self,parti,fyb=275):
		vars(self).update(locals())
		self.s=parti[0].s

	def is_parete(self):
		''' controlla se il blocco rappresenta una porzione di parete o un bordo'''
		ei=self.parti[0].e1
		ef=self.parti[-1].e2
		return	all(ei==ef)	
	
	def ruota(self,ang):
		'''creo un nuovo blocco ruotato :'''
		M=r_[c_[cos(ang),-sin(ang)],c_[sin(ang),cos(ang)]]
		parti=[]
		for parte in self.parti:
			s=parte.s
			i=Nodo(*dot(M,parte.i.co))
			j=Nodo(*dot(M,parte.j.co))
			parti.append(Rect(i,j,s))
		return Blocco(parti)

	def assottiglia(self):
		'''uso J sull'asse perpendicolare al lato più lungo'''
		if self.is_parete():
			#per irrigidimenti di parete
			lunghezze=r_[[i.l for i in self.parti]] 
			b=lunghezze[0]+lunghezze[-1]
			asse=self.parti[0].e1
			angolo=arccos(dot(asse,r_[1,0]))
			J=self.ruota(angolo).Jx
			lim1=.016*(self.fyb/210000)**2*(b/self.s)**3*self.A
			lim2=.24*(self.fyb/210000)**2*(b/self.s)**3*self.A

		else:
			#per irrigidimenti bordo
			lunghezze=r_[[i.parent.lunghezza for i in self.parti]]
			l=lunghezze.max()
			#recupero l'asse del lato maggiore
			asse=self.parti[where(lunghezze==l)[0][0]].e1
			angolo=arccos(dot(asse,r_[1,0]))
			M=r_[c_[cos(angolo),sin(angolo)],c_[-sin(angolo),cos(angolo)]]
			coords=[dot(M,part.i.co)[1] for part in self.parti]
			coords.extend([dot(M,part.i.co)[1] for part in self.parti])
			h=r_[coords].ptp()
			J=self.ruota(angolo).Jx
			lim1=.31*(1.5+h/l)*(self.fyb/210000)**2*(l/self.s)**3*self.A
			lim2=4.86*(1.5+h/l)*(self.fyb/210000)**2*(l/self.s)**3*self.A
		if J<lim1:
			raise ValueError('J minore del minimo')
			chi=0
		elif lim1<=J<lim2:
			chi=.5
		else:
			chi=1
		self.s*=chi
		for i  in self.parti:
			i.s*=chi
			i.definisci()
#		print 'calcolo irrigidimento'
#		print_out(locals(),'lim1,lim2,J')
#		print 	
#			
#
	@property
	def A(self):
		return sum([i.A for i in self.parti])
	@property
	def Sx(self):
		'''momento statico rispetto asse x'''
		return sum([i.Sx for i in self.parti])
	@property
	def Yg(self):
		return self.Sx/self.A
	@property
	def Jx_(self):
		'''momento di inerzia rispetto asse x'''
		return sum([i.Jx_ for i in self.parti])
	@property
	def Jx(self):
		'''momento di inerzia baricentrico'''
		return self.Jx_-self.A*self.Yg**2

	def J_ang(self,ang):
		'''calcolo del modulo di inerzia ruotando la sezione di un dato angolo'''
		return self.ruota(ang).Jx
	
	@property
	def ang_J_min(self):
		'''ricerca dell'angolo per cui si minimizza J'''
		ang=minimize(self.J_ang)#,.1,options={'disp':False})
		return ang.x
	@property
	def ang_J_max(self):
		'''ricerca dell'angolo per cui si massimizza J'''
		ang=minimize(lambda x:-1*self.J_ang(x))#,.1,options={'disp':False})
		return ang.x

	@property
	def J_min(self):
		'''ricerca del J minimo ruotando la sezione'''
		return self.ruota(self.ang_J_min).Jx		
	
	@property
	def J_max(self):
		'''ricerca del J massimo ruotando la sezione'''
		return self.ruota(self.ang_J_max).Jx	
	
	def plot(self,*args):
		for i in self.parti:
			i.plot(*args)
		

if __name__=='__main__':				

	ion()
	close('all')
	fyk=235
	fyd=fyk/1.25

	b=50.
	h=325.
	h2=25.
	s=3
	punti=r_[[[0,0],
			  [0,-h2],
			  [-b,-h2],
			  [-b,h-h2],
			  [0,h-h2],
			  [0,h-2*h2]]]

	punti=[Nodo(*i) for i in punti]

	sez=Sezione(punti,s,fyk)
	sez.plot('b')
	import sys
	print('J_intero:',sez.Jx)
	sez.seziona_1(True)
	sez.assottiglia()
	print('J_ridotto:',sez.Jx)
	sez.plot('r')
	axhline(sez.Yg,color='k')
	print()
	test_J=sez.Jx/Jmin
	test_W=sez.Wx/Wmin
	test_A=sez.A/Amin
	#print_out(locals(),'test_J,test_W,test_A')


	#verifiche della sezione
	print('Verifica a flessione')
	Wx=sez.Wx
	Mcrd=fyk/gamma_m1*Wx
	isM=Mcrd/M
	print_out(locals(),'Wx,Mcrd,isM')

	print('\nVerifica a taglio')
	lamw=h/s/86.4/235*275
	if lamw>=1.4:
		fbv=.67*fyk/lamw**2
	else:
		fbv=.48*fyk/lamw
	Vbrd=2*h*s*fbv/gamma_m1
	Vprd=2*h*s*fyk/sqrt(3)/gamma_m1
	Vwrd=min([Vbrd,Vprd])
	isV=Vwrd/T
	print_out(locals(),'lamw,Vbrd,Vprd,Vwrd,isV')

	print('\nVerifica a carichi concentrati')
	la=10
	alpha=.057
	r=3 #raggio di piegatura
	phi=90 #inclinazione anima
	Rwrd=2*alpha*s**2*sqrt(fyk*210000)*(1-.1*sqrt(r/s))*(.5+sqrt(.002*la/s))*(2.4+(phi/90)**2)/gamma_m1
	isR=Rwrd/T
	print_out(locals(),'la,alpha,r,phi,Rwrd,isR')


	print('\nInstabilità laterale')
	#calcolo del momento critico
	#per ora qui mi areno, servono:
	#a forza devo ottenere qualcosa di ruotabile per avere i momenti di inerzia...
	parti=[]
	for i  in sez.lati:
		parti.extend(i.parti)
	tmp_bl=Blocco(parti,fyk)


	Iw=0 #costante di wrap(imbozzamento) non so da dove pescarla se la annullo è a favore di sicurezza
	It=tmp_bl.Jx+tmp_bl.ruota(pi/2).Jx #momento torsionale sezione posso calcolarlo
	Iz=tmp_bl.J_min
	za=h#quota applicazione carico
	zs=h/2 #quota centro di taglio posso calcolarla precisa ma non siamo lontani
	k=1  #luce/luce libera
	kw=1 #1 se non meglio definito
	#C[1,2,3] da tabelle
	C=r_[1.132,0.459,0.525]
	E=210000


	zg=za-zs
	#valutazione approssimata di zj:
	Ifc=1/12*s*((2*b2+b)**3-b**3)
	Ift=1/12*s*b**3
	betaf=Ifc/(Ifc+Ift)
	if beta>=0.5:
		zj=.8*(2-betaf-1)*h/2
	else:
		zj=1*(2*betaf-1)*h/2
	G=E/(2*(1-.3))

	Mcrit= C[0]*pi**2*E*Iz/(k*l)**2* ( sqrt( (k/kw)**2*Iw/Iz + (k*l)**2*G*It/(pi**2*E*Iz) + (C[1]*zg-C[2]*zj)**2) - (C[1]*zg-C[2]*zj))
	lamlt=sqrt(fyk*Wx/Mcrit)

	alphalt=.21
	psilt=.5*(1-alphalt*(lamlt-.2)*lamlt**2)
	xilt=1/(psilt+(psilt**2-lamlt**2)**.5)
	xilt=min([1,xilt])

	Mbrd=xilt*Wx*fyk/gamma_m1
	isMb=Mbrd/M
	ver=all(r_[isM,isV,isR,isMb]>1)

	################################################################################

	print_out(locals(),'Iw,It,Iz,za,zs,k,kw,E,zg,Ifc,Ift,betaf,zj,G,Mcrit,lamlt,alphalt,psilt,xilt,Mbrd,isMb')
	print('\nSuperamento delle verifiche:',ver)
