#-*-coding:latin-*-

from scipy import *
from .instabilita import omega,omega_1

def verificaCompressione(N,pr,fyd,ver=None):
	'''N sforzo normale
	pr=profilo usato
	fyd=resistenza di progetto
	ver=classe Verifica'''
	Nrd=pr.A*fyd
	if ver is not None:
		ver(abs(N),1,Nrd,'compressione semplice')
		cu=ver.cu[-1]
	else:
		cu=abs(N)/Nrd
	stout='''# Verifica a sforzo Normale  
N= {NN} N sforzo normale  
N~rd~= {Nrd} N resistenza  
{vv}  
'''
	if ver is not None: stout=stout.format(NN=max(abs(N)),vv=ver.st[-1],**locals())
	else: stout=stout.format(NN=max(abs(N)),vv='cu= {} coefficiente d\'uso'.format(max(cu)),**locals())

	return locals()

def verificaFlessione(My,Mz,pr,fyd,ver=None):
	'''My momenot su asse y 
	Mz momenot su asse z
	pr=profilo usato
	fyd=resistenza di progetto
	ver=classe Verifica'''
	Mrd=r_[pr.Wy,pr.Wz]*fyd
	if ver is not None:
		ver(abs(My),1,Mrd[0],'flessione su Y')
		ver(abs(Mz),1,Mrd[1],'flessione su Z')
		ver(ver.cu[-2] + ver.cu[-1] ,1,1,'flessione deviata')
		cu=ver.cu[-3:]
	else:
		cu=[abs(My)/Mrd[0],abs(Mz)/Mrd[1]]
		cu.append(cu[0]+cu[1])

	stout='''# Verifica a Flessione  
M~y~={mmy} Nmm momento asse forte  
M~z~={mmz} Nmm momento asse debole  
M~yrd~= {Mrd[0]} Momento resistenta asse forte  
M~zrd~= {Mrd[1]} Momento resistenta asse debole  
'''.format(mmy=max(abs(My)),mmz=max(abs(Mz)),**locals())
	if ver is not None: stout='  \n'.join([stout,ver.st[-3],ver.st[-2],ver.st[-1]])
	else:
		stout+='''  
cu~1~= {cu[0]} coefficiente d'uso asse forte  
cu~2~= {cu[1]} coefficiente d'uso asse debole  
cu~3~= {cu[2]} coefficiente d'uso flessione deviata  
'''.format(cu=[cu[0].max(),cu[1].max(),cu[2].max()])

	return locals()

def verificaPressoFlessione(*args,**kargs):
	'''*args,kargs:
	out1=output verificaCompressione
	out2=output verificaFlessione
	-------------------------------
	N sforzo normale
	My,Mz momenti flettenti asse y e z
	pr=profilo usato
	fyd=resistenza di progetto
	ver=classe Verifica'''
	if args:
		if len(args)==2:
			caso=1
			out1,out2=args
		else:
			caso=2
			v='N,My,Mz,pr,fyd,ver'.split()
			args+=(None)*(6-len(args))
			locals().update(dict(list(zip(v,args))))
			del v
	if kargs:
		locals().update(kargs)
		if any([i in list(kargs.keys()) for i in 'out1 out2'.split()]):
			caso=1
		else:
			caso=2
	if not 'caso' in list(locals().keys()): raise IOError
	del args,kargs
	if caso==2:
		out1=verificaCompressione(N,pr,fyd,ver)
		out1=verificaFlessione(My,Mz,pr,fyd,ver)
	cu=out1['cu']+out2['cu'][-1]
	if out1['ver'] is not None:
		out1['ver'](cu ,1,1,'pressoflessione')
	del caso
	stout='''{}  
{}  
# Verifica a pressoflessione  
'''.format(out1['stout'],out2['stout'])
	if out1['ver'] is not None:
		stout+='\n{}'.format(out1['ver'].st[-1])
	else:
		stout+='''  
  
cu= {} coefficiente d'uso a pressoflessione  
'''.format(max(cu))
	return locals()

def verificaInstabilita(N,pr,fyk,fyd,li,curva='b',ver=None,E=None):
	'''N sforzo normale
	pr=profilo usato
	fyk=resistenza caratteristica
	fyd=resistenza di progetto
	li=luce per instabilita: li | [lz,ly] unico o distinto asse debole e forte 
	curva=curva di instabilita'
	ver=classe Verifica
	E=modulo elastico'''
	if not E:E=210e3
	li=atleast_1d(li)
	if li.size==1:li=r_[1,1]*li
	if 'iv' in list(pr.keys()):
		lam=max(li/r_[pr.iv,pr.iu])
	else:
		lam=max(li/r_[pr.iz,pr.iy])
	om=omega(lam,fyk,curva,E)
	Nrd=pr.A*fyd/om
	N_=N.copy()
	N_[N_>0]=0
	N_=abs(N_)
	if ver is not None:
		ver(N_,1,Nrd,'instabilita\' euleriana')
		cu=ver.cu[-1]
	else:
		cu=N_/Nrd
	stout='''# Verifica a instabilita' euleriana  
l~i~= {li} mm luce libera inflessione  
curva= {curva} : curva di instabilita'  
&lambda;= {lam} snellezza elemento  
&omega;= {om} coefficiente omega  
N= {nn} sforzo di compressione  
N~rd~= {Nrd} resistenza  
'''.format(nn=-min(N),**locals())
	if ver is not None:
		stout+='\n{}  '.format(ver.st[-1])
	else:
		stout+='\ncu= {} coefficiente d\'uso  '.format(max(cu))
	return locals()

def verificaInstabilitaFlessionale(My,Mz,pr,fyk,fyd,li,mult=1.4,ver=None,E=None):
	'''M[yz] momento flettente per instabilita'
	pr=profilo usato
	fyk=resistenza caratteristica
	fyd=resistenza di progetto
	li=luce per instabilita' l1 | [lz,ly]
	mult=1.4|1 a seconda che il carico sia stabilizzante o meno
	ver=classe Verifica'''
	if not E:E=214067.27828746176
	li=atleast_1d(li)
	om1=max([omega_1(fyk,pr,x) for x in li])
	om1_=om1*mult
	Mrd=r_[pr.Wy,pr.Wz]*fyd/om1_
	if ver is not None:
		ver(abs(My),1,Mrd[0],'instabilita\' flessione su Y')
		ver(abs(Mz),1,Mrd[1],'instabilita\' flessione su Z')
		ver(ver.cu[-1]+ver.cu[-2],1,1,'instabilita\' flessione deviata')
		cu=ver.cu[-2:]
	else:
		cu=[abs(My)/Mrd[0],abs(Mz)/Mrd[1]]
		cu.append(cu[0]+cu[1])
	stout='''# Verifica a instabilita' flessionale  
M~y~= {my} Nmm momento asse forte  
M~z~= {mz} momento asse debole  
&omega;~1~= {om1} coefficiente &omega;~1~  
m= {mult} moltiplicatore per carico instabilizzante  
M~yrd~= {Mrd[0]} Nmm momento resistente asse forte  
M~zrd~= {Mrd[1]} Nmm momento resistente asse debole  
'''.format(mz=max(abs(Mz)),my=max(abs(My)),**locals())
	if ver is not None:
		stout='  \n'.join([stout,ver.st[-3],ver.st[-2],ver.st[-1]])
	else:
		stout+='''
cu~1~= {cu[0]} coefficiente d'uso asse forte  
cu~2~= {cu[1]} coefficiente d'uso asse forte  
cu~3~= {cu[2]} coefficiente d'uso flessione deviata  
'''.format(cu=[cu[0].max(),cu[1].max(),cu[2].max()])
	return locals()


def verificaInstabilitaPressoflex(*args,**kargs):
	'''*args,kargs:
	out1=output verificaInstabilita
	out2=output verificaInstabilitaFlessionale
	-------------------------------
	N sforzo normale
	My,Mz momento flettente per instabilita'
	pr=profilo usato
	fyk=resistenza caratteristica
	fyd=resistenza di progetto
	li=luce per instabilita: li | [lz,ly] unico o distinto asse debole e forte 
	curva='curva di instabilita'
	mult=1.4|1 a seconda che il carico sia stabilizzante o meno
	ver=classe Verifica
	E=modulo elastico'''
	if args:
		if len(args)==2:
			caso=1
			out1,out2=args
		else:
			caso=2
			v='N','My','Mz','pr','fyk','fyd','li','curva','mult','ver','E'
			args=args+(None,)*(len(v)-len(args))
			for i,j in zip(v,args):
				if i not in list(locals().keys()): exec('{}=None'.format(i))
				locals()[i]=j
			print(v)
			print(args)
			print(list(locals().keys()))
			if not locals()['curva']: curva='b'
			if not locals()['mult']: mult=1.4
			del v
	if kargs:
		locals().update(kargs)
		if any([i in list(kargs.keys()) for i in 'out1 out2'.split()]):
			caso=1
		else:
			caso=2
	if not 'caso' in list(locals().keys()): raise IOError
	del args,kargs
	if caso==2:
		out1=verificaInstabilita(N,pr,fyk,fyd,li,curva,ver,E)
		out2=verificaInstabilitaFlessionale(My,Mz,pr,fyk,fyd,li,mult,ver,E)
	cu=out1['cu']+r_[out2['cu']].max(0)
	if out1['ver'] is not None:
		out1['ver'](cu ,1,1,'instabilita\' a pressoflessione')
	stout='''{}  
{}  
# verifica instabilita' pressoflessionale  
'''.format(out1['stout'],out2['stout'])
	if out1['ver'] is not None:
		stout+='\n  '
		stout='\n  '.join([stout,out1['ver'].st[-1]])
	else:
		stout+='''  \ncu= {cu} coefficiente d'uso instabilita' pressoflessionale  
'''.format(cu=max(abs(cu)))
	return locals()

def verificaTaglio(T2,T3,pr,fyd,ver=None):
	'''T2 T3 tagli lungo gli assi
	pr,profilo da verificare
	fyd resistenza
	ver=classe Verifiche'''
	#calcolo aree di taglio
	if 'tf' in list(pr.keys()):
		Ay=pr.h*pr.tw/1.5
		Az=pr.b*pr.tf/1.5*2
	t=fyd/sqrt(3)
	Trd=r_[Ay,Az]*t
	cu=abs(c_[T2,T3])/Trd
	T2_=max(abs(T2))
	T3_=max(abs(T3))
	stout='''# Verifica a Taglio  
Tagli di progetto:  
T~y~={T2_} N , Tz={T3_} N  
Aree resistenti:  
A~y~={Ay} mm^2^ Az={Az} mm^2^  
f~yd~={fyd} MPa --> t= {t}Mpa  
Tagli resistenti:  
T~yrd~={Trd[0]} N , Tz~rd~={Trd[1]} N  
{v0}  
{v1}  
'''
	if ver:
		ver(abs(T2),1,Trd[0],'Verifica a taglio su y')
		ver(abs(T3),1,Trd[1],'Verifica a taglio su z')
		v0=ver.st[-2]
		v1=ver.st[-1]
	else:
		ve=Verifiche(red=1)
		ve(abs(T2),1,Trd[0],'Verifica a taglio su y')
		ve(abs(T3),1,Trd[1],'Verifica a taglio su z')
		v0=ver.st[-2]
		v1=ver.st[-1]
		del ve
	stout=stout.format(**locals())
	return locals()



from inspect import getargspec
from gueng.utils import Verifiche
class VerificheSLU:
	def __init__(self,sol,pr,fyk,fyd,li,curva,mult=1.4,E=None,ver=None,red=True,stformat='termcolor'):
		'''sol=sollecitazioni [x asse trave, y asse debole, z asse forte]
			pr=profilo
			fyk,fyd caratteristiche acciaio
			li=luce libera inflesione
			curva=curva instabiitia' a:scatolari, b:I(anche rinforzati) scatolari saldati,c:generiche t<40 , d:generiche t>40
			mult=moltiplicatore instabilta' pressoflex
			E=modulo elastico
			ver=classe Verifiche su cui aggiungere
		'''
		vars(self).update(locals())
		if ver is None:
			self.ver=Verifiche(red=red,stformat=stformat)
		self.N=sol.T[0]
		self.T2=sol.T[1]
		self.T3=sol.T[2]
		self.M1=sol.T[3]
		self.Mz=sol.T[4] #sollecitazioni su asse debole
		self.My=sol.T[5] #sollecitazioni su asse forte
		self.log={}

	def reset(self):
		self.log={}

	def mymethod (self,fun):
		def wrap(*args_,**kargs):
			if not args_:
				args_=[vars(self)[i] for i in getargspec(fun).args]
			return fun(*args_,**kargs)
		return wrap

	def logger(fun):
		def wrapper(self,*args,**kargs):
			if not fun.__name__ in list(self.log.keys()): 
				self.log[fun.__name__]=fun(self,*args,**kargs)
			return self.log[fun.__name__]
		return wrapper
	
#	verificaCompressione_=mymethod(verificaCompressione)
#	verificaFlessione_=mymethod(verificaFlessione)

	@logger
	def verificaCompressione(self):
		return self.mymethod(verificaCompressione)()
	@logger
	def verificaFlessione(self):
		return self.mymethod(verificaFlessione)()
	
	@logger
	def verificaPressoFlessione(self):
		o1=self.verificaCompressione()
		o2=self.verificaFlessione()
		return verificaPressoFlessione(o1,o2)
		
#	verificaInstabilita=mymethod(verificaInstabilita)
#	verificaInstabilitaFlessionale=mymethod(verificaInstabilitaFlessionale)
	@logger
	def verificaInstabilita(self):
		return self.mymethod(verificaInstabilita)()
	@logger
	def verificaInstabilitaFlessionale(self):
		return self.mymethod(verificaInstabilitaFlessionale)()
	
	@logger
	def verificaInstabilitaPressoflex(self):
		o1=self.verificaInstabilita()
		o2=self.verificaInstabilitaFlessionale()
		return verificaInstabilitaPressoflex(o1,o2)
	
	@logger
	def verificaTaglio(self):
		return self.mymethod(verificaTaglio)()

	def verifica(self):
		'''esegue tutte le verifiche'''
		self.verificaPressoFlessione()
		self.verificaInstabilitaPressoflex()
		self.verificaTaglio()

	def __str__(self):
		if not self.log:
			self.verifica()
		st='{}\n{}\n{}'.format(self.log['verificaPressoFlessione']['stout'] , self.log['verificaInstabilitaPressoflex']['stout'],self.log['verificaTaglio']['stout'])
		return st

	def __non_zero__(self):
		if not self.log: self.verifica()
		cu=r_[[i['cu'] for i in self.log]].flatten()
		if all(cu<=1): return -1
		else: return 0


if  __name__=='__main__':
	sol=r_[[[10,0,0,0,23,32],
		[-10,0,0,0,23,32]]]
	from gueng.profilario import prof
	pr=prof.ipe.query('h==80').iloc[0]
	l=3000
	fyk=275
	fyd=fyk/1.1
	curva='b'
	v=VerificheSLU(sol,pr,fyk,fyd,l,curva)
	print(v.verificaCompressione()['cu'])
	print(v.verificaFlessione()['cu'])
	print(v.verificaPressoFlessione()['cu'])
	print(v.verificaInstabilita()['cu'])
	print(v.verificaInstabilitaFlessionale()['cu'])
	print(v.verificaInstabilitaPressoflex()['cu'])
