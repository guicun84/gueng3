#-*- coding:utf-8-*-


from scipy import *

def omega(lam,f,cur='b',E=210e3,verbose=1):
	'''lam=lambda
	f=fyk [MPa]
	cur=coefficiente in funzione delle curve: a:.158, b: .281 , c:.384 , d:.587
	E=modulo elastico
	verbose =[true false] avverte se lambda è eccessivo
	'''
	coef={'a':.158, 'b': .281 , 'c':.384 , 'd':.587}
	a=coef[cur]
	lamcr=pi*sqrt(E/f)
	lam_=lam/lamcr
	if lam_<=.2:
		om=1
	else:
		lam2=lam_**2
		iom=(1+a*sqrt(lam2-0.04)+lam2)/(2*lam2) - 1/(2*lam2) * sqrt( (1+a*sqrt(lam2-0.04)+lam2)**2 -4*lam2)
		om=1/iom
	if lam_>3.5 and verbose: print('snellezza eccessiva')
	return om

def omega_1(fy,h,l,b=None,tf=None,E=210e3*10/9.81,m=1.4):
	'''fy=resistenza materiale
	h=altezza trave oppure profilo
	l=lunghezza tra 2 ritegni torsionali
	b=base trave
	tf=spessore ala
	E=modulo elastico,
	m=coefficiente moltiplicativo per carico su lembo superiore'''
	if b is None:
		pr=h
		h=pr.h
		b=pr.b
		tf=pr.tf
	return max([1,m*fy/(.585*E)*(h*l)/(b*tf)])


def Ncr(J,l,E=210e3*.98):
	'''J=modulo di inerzia
	l=distanza tra due ritegni torsionali'''
	#il calcolo di cnr è stato fatto con E=2100000 K/cm^2 e convertito usando G correttamente...
	return pi**2*E*J/l**2

	
