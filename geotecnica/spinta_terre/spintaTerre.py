from scipy import rad2deg,deg2rad,sin,cos,tan,arctan,sqrt,arange,r_
from scipy.integrate import trapz
from .. import Stratigrafia

class SpintaLitostatica():
	def __init__(self,stratigrafia):
		self.stratigrafia=stratigrafia
	@property
	def h(self):
		if not hasattr(self,'_h'):
			print('manca')
			print(vars(self).keys())
			return 0
		return self._h
	
	@h.setter
	def h(self,val):
		self.reset()
		self._h=val

	@property
	def z(self):
		if not hasattr(self,'_z'):
			self._z=arange(0,self.h,10)
			if self._z[-1]!=self.h:
				self._z=r_[self._z,self.h]
		return self._z

	def __sigma(self,tipo='A'):
		'''tipo=a|p|(o|0) attiva passiva o a riposo'''
		if tipo==0:tipo='o'
		tipo=tipo.lower()
		sigma=dict(a='sigmaA',p='sigmaP',o='sigma0')[tipo]
		sigma_=list(map(lambda x: self.stratigrafia.interrogaStrato(sigma,x),self.z))
		return sigma_

	@property
	def sigmaA(self):
		if not hasattr(self,'_sigmaA'):
			self._sigmaA=self.__sigma('a')
		return self._sigmaA
	@property
	def sigmaP(self):
		if not hasattr(self,'_sigmaP'):
			self._sigmaP=self.__sigma('p')
		return self._sigmaA
	@property
	def sigma0(self):
		if not hasattr(self,'_sigma0'):
			self._sigma0=self.__sigma('o')
		return self._sigma0

	def reset(self):
		var='_z','_sigmaA','_sigmaP','_sigma0','_h'
		for i in var:
			if hasattr(self,i):
				del vars(self)[i]
	
	def __F(self,tipo='a'):
		'''tipo=a|p|(o|0) attiva passiva o a riposo'''
		if tipo==0:tipo='o'
		tipo=tipo.lower()
		if tipo=='a': sigma=self.sigmaA
		elif tipo=='p': sigma=self.sigmaP
		elif tipo=='o': sigma=self.sigma0
		else:raise ValueError(f'tipo {tipo} non conosciuto deve essere a,p,o')
		F=trapz(sigma,self.z)*1e3
		return F
	@property
	def FA(self):
		return self.__F('a')
	@property
	def FP(self):
		return self.__F('p')
	@property
	def F0(self):
		return self.__F('o')

	def __M(self,tipo='a'):
		'''tipo=a|p|(o|0) attiva passiva o a riposo'''
		if tipo==0:tipo='o'
		tipo=tipo.lower()
		if tipo=='a': sigma=self.sigmaA
		elif tipo=='p': sigma=self.sigmaP
		elif tipo=='o': sigma=self.sigma0
		else:raise ValueError(f'tipo {tipo} non conosciuto deve essere a,p,o')
		M=trapz(sigma*(self.h-self.z),self.z)*1e3
		return M
	@property
	def MA(self,):
		return self.__M('a')
	@property
	def MP(self,):
		return self.__M('p')
	@property
	def M0(self,):
		return self.__M('o')

class MononobeOkabe:
	def __init__(self,ter,kh=0,beta=0,i=0,delta=None,kv=None,deg=True):
		'''ter=terreno di base 
		kh=coefficiente kh,
		kv=coefficiente kv
		beta=pendenza paramento rispetto verticale
		i=pendenza terreno di monte rispetto orizzontale'
		deg = true se gli angoli passati sono in gradi'''
		if kv is None:kv=.5*kh
		vars(self).update(locals())
		self.phi=ter.phi
		if delta is None: self.delta=self.phi*2/3
		self._theta=arctan(kh/(1-kv))
		self.theta=rad2deg(self._theta)
		if deg==True:
			fun=deg2rad
		else:
			fun=lambda x:x
		for i in 'phi beta i delta'.split():
			vars(self)['_{}'.format(i)]=fun(vars(self)[i])

	@property
	def KAE(self):
		return cos(self._phi-self._beta-self._theta)**2/(cos(self._beta)**2*cos(self._theta)*cos(self._delta+self._beta+self._theta)*(1+sqrt(sin(self._phi+self._delta)*sin(self._phi-self._i-self._theta)/(cos(self._delta+self._beta+self._theta)*cos(self._i-self._beta))))**2)

	@property 
	def alphaAE(self):
		C1E=sqrt(tan(self._phi-self._theta-self._i)*(tan(self._phi-self._tehta-self._i)+1/tan(self._phi-self._theta-self._beta))*(1+tan(self._delta+self._theta+self._beta)/tan(self._phi-self._theta-self._beta)))
		C2E=1+tan(self._delta+self._theta+self._beta)*(tan(self._phi-self._theta-self._i)+1/tan(self._phi-self._theta-self._beta))
		return self._phi-self._tehta+arctan((tan(self._phi-self._teta-self._i)+C1E)/C2E)

	@property
	def KPE(self):
		return cos(self._phi+self._beta-self._theta)**2/(cos(self._beta)**2*cos(self._theta)*cos(self._delta-self._beta+self._theta)*(1+sqrt(sin(self._phi+self._delta)*sin(self._phi+self._i-self._theta)/(cos(self._delta-self._beta+self._theta)*cos(self._i-self._beta))))**2)

	@property 
	def alphaPE(self):
		C3E=sqrt(tan(self._phi-self._theta+self._i)*(tan(self._phi-self._tehta+self._i)+1/tan(self._phi-self._theta+self._beta))*(1+tan(self._delta+self._theta-self._beta)/tan(self._phi-self._theta+self._beta)))
		C4E=1+tan(self._delta+self._theta-self._beta)*(tan(self._phi-self._theta+self._i)+1/tan(self._phi-self._theta+self._beta))
		return -self._phi+self._tehta+arctan((tan(self._phi+self._teta+self._i)+C3E)/C4E)
	
if __name__=='__main__':
	phis=25,30,35,40
	kh=linspace(0,.5)
	from gueng.geotecnica import Coulomb
	figure(1)
	show(0)
	clf()
	ka={}
	for phi in phis:
		t=Coulomb(1800,phi)
		ka[phi]=zeros_like(kh)
		for n,k in enumerate(kh):
			m=MononobeOkabe(t,k,kv=0)
			ka[phi][n]=m.KAE
		plot(kh,ka[phi],label='{}'.format(phi))
	grid(1)
	legend()
	xlabel('kh')
	ylabel('KAE')
	draw()

