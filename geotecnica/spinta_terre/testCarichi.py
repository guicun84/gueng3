#-*-coding:latin-*-

from . import carichiNonUniformi
import imp
imp.reload(carichiNonUniformi)
from .carichiNonUniformi import *
from colorsys import hsv_to_rgb as hsv2rgb

#caso studio: ponteggio su bordo muro
H=1.5 #altezza muro m

#dati del ponteggio
np=2 #piani di ponteggi
qa=150 #Kg/m^2 da UNI HD 1000 ARMONIZZATA per manutenzione, per costruzione 300
qp=40 #Kg/m2 #carico ponteggio a m^2
b=.12 #spessore nastro
inte=1.05#larghezza ponteggio
inte2=1.8
d=.2 #distanza primo carico da muro


qt=(1.*qp)*np + 1.*qa*1.5 #carico totale ponteggio a m^2, considero qa su un ponteggio e meta del sottostante
q=qt*inte/2 #carico lineare Kg/m

#carico della parete 
#qpar=1600*5*.6
qpar=1e-10
def fun(l1,l2,l3,tit):
	z=linspace(0,H)
	p1=l1.pa(z)
	p2=l2.pa(z)
	p3=l3.pa(z)
	pt=p1+p2+p3
	title(tit)
	plot(p1,z,label='carico q={:.4g} x={:.4g}'.format(l1.q,l1.x))
	plot(p2,z,label='carico q={:.4g} x={:.4g}'.format(l2.q,l2.x))
	plot(p3,z,label='carico q={:.4g} x={:.4g}'.format(l3.q,l3.x))
	plot(pt,z,label='carico totale')
	ylim(ylim()[-1::-1])
	grid(1)
	legend(loc='best',fontsize=12)
	xlabel('pressione Kg/mq')
	ylabel('profonditÓ m')
	F1=trapz(p1,z)
	F2=trapz(p2,z)
	F3=trapz(p3,z)
	F=F1+F2+F3
	z1=trapz(p1*z,z)/F1
	z2=trapz(p2*z,z)/F2
	z3=trapz(p3*z,z)/F3
	zt=trapz(pt*z,z)/F
	print('''F1= {F1:.4g} Kg/m ,z1= {z1:.4g} m
F2= {F2:.4g} Kg/m, z2={z2:.4g} m
F3= {F2:.4g} Kg/m, z3={z3:.4g} m
Ftot= {F:.4g} Kg/m, ztot {zt:.4g} m'''.format(**locals()))
	def myarr(z,val,desc,color):
		arrow(20,z,-15,0,head_length=5,color=color)
		text(20,z,'{}:{:.4g} Kg/m, z={:.4g}m'.format(desc,val,z),color=color,fontsize=12)

	myarr(z1,F1,'F1','b')
	myarr(z2,F2,'F2','g')
	myarr(z3,F3,'F3','r')
	myarr(zt,F,'Ftot','c')

figure(1)
clf()
show(0)
l1=CaricoLineare(H,d,q=q)
l2=CaricoLineare(H,d+inte,q=q)
l3=CaricoLineare(H,d+inte+.2+.3,q=qpar)
fun(l1,l2,l3,tit='ponteggio lineare')
draw()

n1=CaricoNastriforme(H,b,d,q=q/b)
n2=CaricoNastriforme(H,b,d+inte,q=q/b)
n3=CaricoNastriforme(H,.6,d+inte+.2+.3,q=qpar/.6)
figure(2)
clf()
show(0)
fun(n1,n2,n3,tit='ponteggio nastriforme')
draw()



############################################################
#valutazione della pressione per carico concentrato sotto al piedritto

F=q*inte2
c1=CaricoPuntuale(H,d,P=F)
c2=CaricoPuntuale(H,d+inte,P=F)

z=linspace(0,H,100)
y=linspace(-.9,.9,200)
dA=(diff(z[:2])*diff(y[:2]))[0]
alpha=[arctan2(i,c1.x) for i in y]
Alpha,Z=meshgrid(alpha,z)
Y,Z=meshgrid(y,z)
p1=empty(0,float)
p2=empty(0,float)
for i,j in zip(Alpha.flat,Z.flat):
	p1=append(p1,c1.pap(j,alpha=i,deg=0))
	p2=append(p2,c2.pap(j,alpha=i,deg=0))
p1=p1.reshape(A.shape)
p2=p2.reshape(A.shape)
p=p1+p2
mp1=ma.masked_array(p1,mask=p1<.01*p1.max())
mp2=ma.masked_array(p2,mask=p2<.01*p2.max())
mp=ma.masked_array(p,mask=p<.01*p.max())
Fh1=dA*p1.sum()
z1=(dA*p1.flat*Z.flatten()**2).sum()/Fh1
Fh2=dA*p2.sum()
z2=(dA*p2.flat*Z.flatten()**2).sum()/Fh2
Fh=dA*p.sum()
z=(dA*p.flat*Z.flatten()**2).sum()/Fh

figure(3)
clf()
show(0)
subplot(2,2,1)
numcol=100
contourf(Y,Z,mp1,numcol)
def decora(p,zg):
	ylim(ylim()[-1::-1])
	axis('equal')
	grid(1)
	colorbar(ticks=linspace(p.min(),p.max(),10))
	axvline(-.9,color='k')
	axvline( .9,color='k')
	axhline( zg,color='r')
	xlabel('x m')
	ylabel('profonditÓ m')
	draw()
decora(mp1,z1)
subplot(2,2,2)
contourf(Y,Z,mp2,numcol)
decora(mp2,z2)
subplot(2,2,3)
contourf(Y,Z,mp,numcol)
decora(mp,z)
Fh=p.sum()*dA
print('''spinta primo montante:{Fh1:.4g} Kg , z={z1:.4g} m
spinta secondo montatne:{Fh2:.4g} Kg , z={z2:.4g} m
spinta totale montanti:{Fh:.4g} Kg , z={z:.4g} m'''.format(**locals()))

############################################################
s=.25
P=s*1800*inte2*H
Mr=Fh*(H-z)
Ms=P*s/2
Mtot=Ms-Mr
assert Mtot>0
def NM1(xxx_todo_changeme):
	(s,x) = xxx_todo_changeme
	N=s*x/2*inte2
	M=N*(x/3)
	return r_[N,M]

def NM2(xxx_todo_changeme1):
	(s1,s2) = xxx_todo_changeme1
	N=inte2*(s1+s2)*s/2
	M=inte2*(s2*s*s/2 + (s1-s2)*s*s/3)
	return r_[N,M]

e=Mtot/P
s0=P/inte2/s
from scipy.optimize import root,minimize
if e<s/6:
	NM=NM2
	x0=r_[s0,s0]
else:
	NM=NM1
	x0=r_[s0,s/2]
r=root(lambda x:NM(x)/r_[P,Mtot]-r_[1,1],x0,options={'xtol':1e-8})
assert r.success

if e<s/6:
	print('''diagramma trapezio
sigma a valle={:.4g} Kg/m^2 sigma a monte={:.4g} Kg/m'''.format(*r.x))
else:
	print('''diagramma triangolare
sigma a valle={:.4g} Kg/m^2 estensione area carica: {:.4} m'''.format(*r.x))


