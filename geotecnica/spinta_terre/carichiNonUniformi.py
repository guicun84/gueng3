#-*-coding:latin-*-

from scipy import deg2rad,arctan2,pi,sin,cos

#metodi dell'equilibrio limite globale derivanti da teoria elasticitą di Bousinesq con soluzioni di Terzaghi
#coefficienti corretti da base sperimentale di Spangler e Gerber

class CaricoNastriforme:
	def __init__(self,H,b,x=None,m=None,q=1):
		'''H=altezza paramento
		b=larghezza striscia di carico
		x=distanza dal bordo dell'asse del carico nastrifomre
		m=x/H
		q=carico nastrifomre'''
		vars(self).update(**locals())
		if m is None: self.m=x/H
		if x is None: self.x=m*H

	def alpha(self,z):
		'''angolo tra paramento e asse nastro di carico
		z=profonditą
		ritorna alpha in radianti'''
		return arctan2(self.x,z)

	def beta(self,z):
		'''angolo di apertura del nastro di carico
		z=profonidtą
		ritorna beta in radianti'''
		a1=arctan2(self.x+self.b/2,z)
		a2=arctan2(self.x-self.b/2,z)
		return a1-a2

	def pa(self,z):
		b=self.beta(z)
		a=self.alpha(z)
		'''pressione sulla parete
		z=quota del punto'''
		return 2*self.q/pi*(b+sin(b)*sin(a)**2 - sin(b)*cos(a)**2)


class CaricoLineare:
	def __init__(self,H,x=None,m=None,q=1.):
		'''H=altezza paramento
		x=distanza orizzontale tra paramento e linea di carico
		m=x/H
		q=carico lineare'''
		vars(self).update(locals())
		if m is None: self.m=x/H

	def pa(self,z=None,n=None):
		'''pressione su paramento (formula di Terzaghi)
		z=profonditą del punto rispetto a piano carico
		n=z/h'''
		if n is None: n=z/self.H
		if self.m>0.4:
			return 4/pi*self.m**2*n/(self.m**2+n**2)**2/self.H*self.q
		else:
			return 0.203*n/(0.16+n**2)**2/self.H*self.q

class CaricoPuntuale(CaricoLineare):
	def __init__(self,H,x=None,m=None,P=1.):
		'''H=altezza paramento
		x=distanza orizzontale tra paramento e linea di carico
		m=x/H
		P=carico concentrato'''
		CaricoLineare.__init__(self,H,x,m)
		self.P=P
		del self.q

	def pa(self,z=None,n=None):
		'''pressione sul asse di minima distanza tra carico e paramento
		z=profonditą del punto rispetto a piano carico
		n=z/h'''
		if n is None: n=z/self.H
		if self.m>0.4:
			return 1.77*self.m**2*n**2/(self.m**2+n**2)**3/self.H**2*self.P
		else:
			return 0.28*n**2/(0.16+n**2)**3/self.H**2*self.P
	
	def pap(self,z=None,n=None,alpha=0,deg=True):
		'''pressione su paramento su punto generico
		z=profonditą del punto rispetto a piano carico
		n=z/h
		alpha=angolo nel piano orizontale tra linea che dal punto va al paramento e punto in cui si vuole considerare la pressione
		deg=[True,False] angolo true gradi,false rad'''
		if deg:alpha=deg2rad(alpha)
		return self.pa(z,n)*cos(1.1*alpha)**2
