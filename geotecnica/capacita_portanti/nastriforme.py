#-*-coding:latin-*-

from scipy import *
import pandas as pa

class Nastriforme:
	def __init__(self,B,L,terreno,V=0,M=0,H=0,D=0,q=None,alpha=0,om=0,deg=True):
		'''B= base della fondazione [mm]
		L=lunghezza fondazione [mm]
		V=forza normale alla fondazine [N]
		M= momento flettente su fondazione [Nmm]
		H= forza orizzontale su fondazine [N]
		terreno= per ora funziona con terreno tipo Coulomb
		D = profonditÓ fondazione [mm]
		q= carico laterale fondazioe [N/mm^2]
		alpha= inclinazione piano posa
		om= inclinazione piano campagna
		deg=True se angoli in gradi, false se angoli in rad

		Nota: viene salvata per gli angoli l'omonima variabile nascosta in radianti e nei conti si usa sempre quella, mentre la variabile visibile Ŕ convertita in gradi
		'''

		vars(self).update(locals())
		if deg:
			tmp=deg2rad
			tmp2=lambda x:x
		else:
			tmp= lambda x:x
			tmp2=rad2deg

		self._om=tmp(om)
		self._alpha=tmp(alpha)
		self.om=tmp2(om)
		self.alpha=tmp2(om)
		if self.q is None:
			self.q=self.terreno._gamma*self.D
		self.B_=self.B
		self.L_=L

	def qlimVesic(self,D=None,q=None,gamma=None,fullout=False):
		'''calcolo qlim secodno vesich e brinch hansen
		D=profonditÓ fondazione [mm]
		q=sovraccarico fondazione [N/mm^2]
		gamma=densitÓ terreno, di default prende quella del terreno
		fullout=ture restituisce locals() flase solo qlim'''
		#recupero dei dati mancanti
		if gamma==None:
			gamma=self.terreno._gamma #inN/mm^3
		if D==None: D=self.D
		if q==None: q=self.q
		phi=self.terreno._phi
		c=self.terreno.c0
		alpha=self._alpha
		om=self._om
		B=self.B
		L=self.L

		H=self.H
		N=self.V
		e=self.M/N
		if hasattr(e,'__len__'):
			if len(e)==2:
				B_=B-2*e[0]
				L_=L-2*e[1]
			else:		
				B_=B-2*e
				L_=L #per ora non considero momento longitudinale
		else:		
			B_=B-2*e
			L_=L #per ora non considero momento longitudinale
		
		#inizio calcolo dei coefficienti
		Nq=(1+sin(phi)) /(1-sin(phi))* exp(pi*tan(phi))
		Nc=(Nq-1)/tan(phi)
		Ngam=2*(Nq+1)*tan(phi)

		#per fondazione rettangolare	
		sc=1+B_/L_*Nq/Nc
		sq=1+B_/L_*tan(phi)
		sgam=1-0.4*B_/L_

		m=(2+(B_/L_)) / (1+B_/L_)
		iq=(1-H/(N+B_*L_*c/tan(phi)))**m
		igam=(1-H/(N+B_*L_*c/tan(phi)))**(m-1)
		ic=iq-(1-iq)/(Nc*tan(phi))

		gq=(1-tan(om))**2 #omega inclinazione piano campagna
		ggam=gq
		gc=gq-(1-gq)/(Nc*tan(phi))

		bq=(1-alpha*tan(phi))**2 #alpha inclinazione piano posa
		bgam=bq
		bc=bq-(1-bq)/(Nc*tan(phi))

		
		if D>B:
			dq=1+2*D/B*tan(phi)*(1-sin(phi**2))
		else:
			dq=1+2/tan(D/B)*tan(phi)*(1-sin(phi))**2
		dq=min([dq,1.23])
		dgam=1
		dc=dq-(1-dq)/(Nc*tan(phi))


		qlim=c*Nc*sc*dc*ic*bc*gc  + 1/2*gamma*B_*Ngam*sgam*dgam*igam*bgam*ggam + q*Nq*sq*dq*iq*bq*gq

		if fullout:
			out=pa.Series(locals())
			ord='gamma D q phi c alpha om B L H N e B_ L_ Nq Nc Ngam sc sq sgam m iq igam ic gq ggam gc bq bgam bc dq dgam dc qlim'.split()
			out=out[ord]
			return out
		else:
			return qlim

	def qlimMeyerof(self,D=None,q=None,gamma=None,fullout=False):
		'''calcolo qlim secodno vesich e brinch hansen
		D=profonditÓ fondazione [mm]
		q=sovraccarico fondazione totale (compreso terreno)  [N/mm^2]
		gamma=densitÓ terreno, di default prende quella del terreno
		fullout=ture restituisce locals() flase solo qlim
		NOTA:
		se q=None viene calcolato come gamma*D'''
		#recupero dei dati mancanti
		if gamma==None:
			gamma=self.terreno._gamma #inN/mm^3
		if D==None: D=self.D
		if q==None: q=self.q
		phi=self.terreno._phi
		c=self.terreno.c0
		alpha=self._alpha
		om=self._om
		B=self.B
		L=self.L

		H=self.H
		N=self.V
		M=self.M
		if N==0:N=1
		i=H/N
		e=M/N
		self.e=e
		if hasattr(e,'__len__'):
			if len(e)==2:
				B_=B-2*e[0]
				L_=L-2*e[1]
			else:		
				B_=B-2*e
				L_=L 
		else:		
			B_=B-2*e
			L_=L 
		Kp=(1+sin(phi)) /(1-sin(phi))
		#inizio calcolo dei coefficienti
		Nq=tan(pi/4+phi/2)**2 * exp(pi*tan(phi))
		Ngam=(Nq-1)*tan(1.4*phi)
		Nc=(Nq-1)/tan(phi)

		#per fondazione rettangolare	
		sc=1+.2*Kp*B/L
		sq=1+.1*Kp*B/L
		sgam=sq

		iq=(1-arctan(i)/(pi/2))**2
		ic=iq
		igam=(1-arctan(i)/phi)**2


		dc=1+.2*Kp**.5*D/B
		dq=1+.1*Kp**.5*D/B
		dgam=dq

		qlim=c*Nc*sc*dc*ic  + 1/2*gamma*B_*Ngam*sgam*dgam*igam + q*Nq*sq*dq*iq #pressione massima
		qlim=atleast_1d(qlim)[0]

		self.qlim=qlim
		self.B_=B_
		self.L_=L_


		if fullout:
			out=pa.Series(locals())
			ord='gamma D q phi c alpha om B L H N e B_ L_ Kp Nq Nc Ngam sc sq sgam iq igam ic dq dgam dc qlim'.split()
			out=out[ord]
			if hasattr(M,'__len__'):
				M_='{}'.format(M)
			else:
				M_='{:.4g}'.format(M)
			stout='''
# Terreno:
{terreno}

# Sollecitazioni in sommitÓ:
V= {V:.4g} N Verticale
H= {H:.4g} N orizzontale
M= {M_} Nmm momento flettente

B= {B}mm , L= {L}mm 
D= {D_} mm profonditÓ piano
q= {q_:.4g} N/mm^2^ carico laterale fondazione
&alpha;= {alpha}░ inclinazione piano di posa
&omega;= {om}░ inclinazione piano campagna'''.format(M_=M_,D_=D,q_=q,**vars(self)) + '''

fattori di capacitÓ portante
N~q~={Nq:.4g}
N~c~={Nc:.4g}
N~&gamma;~={Ngam:.4g}

fattori di forma
s~q~={sq:.4g}
s~c~={sc:.4g}
s~&gamma;~={sgam:.4g}

fattori di profonditÓ
d~q~={dq:.4g}
d~c~={dc:.4g}
d~&gamma;~={dgam:.4g}

fattori di inclinazione
i~q~={iq:.4g}
i~c~={ic:.4g}
i~&gamma~={igam:.4g}

q~lim~={qlim:.4g} MPa '''.format(**locals())
			del M_
			return out,stout
		else:
			return qlim

	def k(self,D=None,q=None,gamma=None,fun=qlimMeyerof):
		'''modulo di reazione del terreno secondo Bowles in N/mm^3
		D=profonditÓ fondazione [mm]
		q=sovraccarico fondazione N/mm^2
		gamma=densitÓ terreno, di default prende quella del terreno
		'''
#		calcolo rigidezza
		ql=fun(self,D=D,q=q,gamma=gamma)
		return ql/25.4

	@property
	def Nrd(self):
		return self.B_*self.L_*self.qlim
