#-*-coding:latin-*-

from scipy import *
from pandas import Series
from gueng.utils import toDict,toString

class Plinto:
	def __init__(self,b,l,d,ter,aq=0,af=0,apc=0):
		'''b,l,d=base lunghezza e profondità plitno
		ter= oggetto terreno [classe]
		aq=inclinazione carico rispetto verticale
		af=inclinazione piano fondazione su orizzontale
		apc= inclinazione piano campagna su orizzontale'''
		vars(self).update(locals())
		if b>l: #controllo che b<l
			self.b=l
			self.l=b
		self.A=self.b*self.l
		self.st=False
	
	def brinchHansen(self):
		Nq=exp(pi*tan(self.ter.phi)) * tan(pi/4 + self.ter.phi/2)**2
		Ngam=1.5*(Nq-1)*tan(self.ter.phi)
		Nc=(Nq-1)/tan(self.ter.phi)
		et=self.b/6
		el=self.l/6
		bp=self.b-(2*et)

		#fattori di forma
		sgam=1-0.4*bp/self.l
		sq=1+bp/self.l*self.ter.phi
		sc=1+Nq/Nc*bp/self.l
		
		#fattori di profondità
		dgam=1
		dq=1+2*tan(self.ter.phi)*(1-sin(self.ter.phi))**2*(self.d/self.b)
		dc=1+0.4*(self.d/self.b)

		#fattori di inclinazione carico
		#devo dare inclinazione carico
		igam=1
		iq=1
		ic=1
		
		#fattori inclinazione fondazione
		#devo passare inclinazone fondazione
		bgam=1
		bq=1
		bc=1

		#fattori inclinazione piano di campagna
		#devo passare piano di campagna
		ggam=1
		gq=1
		gc=1
		out='Nq,Ngam,Nc,bp[mm],et[eccentricità],el,sgam[coeff. di forma],sq,sc,dgam[coeff. di profonidtà],dq,dc,igam[coef. inclnazione carico],iq,ic,bgam[coef. inclinazione fondazione],bq,bc,ggam[coef. inclinazione piano campagna],gq,gc,Qlim[MPa portata]'
		Qlim=(self.ter.c*Nc*sc*dc*ic*bc*gc) + (self.ter.gamma*self.d*Nq*sq*dq*iq*bq*gq) + (0.5*self.ter.gamma*bp*Ngam*sgam*dgam*igam*bgam*ggam)
		self.st=toString(out.split(','))
		self.st='Metodo di Brinch Hansen\n'+self.st+'\nNtot={:.4g} N carico ultimo'.format(Qlim*self.A)
		self.Qlim=Qlim
		return Qlim,locals()
		

	def __str__(self):
		st='''Plinto:
b={b:.4g} mm base; l={l:.4g} mm lunghezza; d={d:.4g} mm profondità
'''.format(**vars(self))
		if self.st:
			st+=self.st
		return st




