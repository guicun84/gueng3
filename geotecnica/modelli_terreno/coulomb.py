#-*- coding:latin-*-

from scipy import *
from copy import copy as cp

class Coulomb:
	def __init__(self,gamma,phi,c0=0,ocr=1,q0=0,deg=True):
		'''gamma=densit� Kg/m^3
		phi=angolo attrito in gradi se deg=True , radianti se deg=False
		co=coesione in MPa
		ocr=grado di sovraconsolidazione sig'_max/sig'
		q0=sovraccarico'''
		vars(self).update(locals())
		self._gamma=gamma*10/1e9 #convertito in N/mm^3
		#controllo unit� di phi e converto in radianti
		if deg: self._phi=deg2rad(phi)
		else: self._phi=phi
		#valutazione coefficiente di spinta a riposo per calcolare sigmaH
		self.K0_nc=1-sin(self._phi) #
		self.K0=self.K0_nc*ocr**.5
		self.KA=(1-sin(self._phi))/(1+sin(self._phi))
		self.KP=(1+sin(self._phi))/(1-sin(self._phi))


	def sigmaV(self,z):
		'''z in mm 
		ritora tensione verticale in MPa'''
		return self._gamma*z+self.q0

	def sigmaH(self,z):
		return self.sigmaV(z)*self.K0

	def sigmaA(self,z):
		'''ritorna ressione attiva in MPa
		z in mm'''
		sig= self.sigmaV(z) *self.KA -2* self.c0 *sqrt(self.KA)
		if sig>0:return sig
		else: return 0.

	def sigmaP(self,z):
		'''ritorna ressione attiva in MPa
		z in mm'''
		return self.sigmaV(z) *self.KP + 2* self.c0 *sqrt(self.KP)
	
	def tau(self,z):
		return self.c0+tan(self._phi)*self.sigamV(z)

	def __str__(self):
		st= '''&phi;= {phi} �
&gamma;={gamma} Kg/mc
c~0~= {c0} MPa'''.format(**vars(self))

		if self.ocr!=1:
			st+='\nOCR={}'.format(self.ocr)
		if self.q0:
			st+='q~0~={}'.format(self.q0)
		return st

	def M1(self):
		return cp(self)

	def M2(self):
		''''modifica i valori dell'istanza con coefficienti M2'''
		new=Coulomb(self.gamma,rad2deg(arctan(tan(deg2rad(self.phi)/1.25))),self.c0/1.25,self.ocr,self.q0)
		return new

