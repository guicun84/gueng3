#-*-coding:latin-*-

from scipy import *

class Strato:
	def __init__(self,terreno,h1,h2=None,q0=0):
		'''terreno =tipo di terreno
		h1,h2 [in mm]= se solo h1 spessore
			se h1e h2 quote di inizio e fine strato'''
		self.terreno=terreno
		self.terreno.q0=q0
		if h2 is None:
			self.spessore=h1
		else:
			spessore=abs(h2-h1)
	

	def qv(self,z):
		'''ritorna la pressione litostatica 
		z=provondit� relativa allo strato'''
#		return self.terreno.q0 + self.terreno._gamma*z
		return self.terreno.sigmaV(z)
	def qh(self,z):
		'''ritorna la pressione orizzontale 
		z=provondit� relativa allo strato'''
#		return self.qv(z)*self.terreno.KA
		return self.terreno.sigmaH(z)
	def __str__(self):
		return '''h={:.4g}
terreno
{}'''.format(self.spessore,self.terreno)

class Stratigrafia:
	def __init__(self,li=None,q0=0):
		'''li=lista di strati da considerare
		q0=sovraccarico [MPa]'''
		self.strati=[]
		self.quote=r_[0]
		self.q0=q0
		self._aggiornato=False
		if li:
			for i in li:self.append(i)
			self.aggiorna()
	
	def append(self,s):
		'''aggiunge uno strato'''
		self.strati.append(s)
		self.quote=r_[self.quote,self.quote[-1]+s.spessore]
		self._aggiornato=False

	def aggiorna_q0(self):
		'''aggiorna il valore del sovraccarico per ogni strato'''
		q0=0
		for s in self.strati:
			s.terreno.q0=q0
			q0+=s.terreno._gamma*s.spessore

	def aggiorna(self):
		if not self._aggiornato:
			self.aggiorna_q0()
			self._aggiornato=True

	def cercaStrato(self,z):
		self.aggiorna()
		'''ricerca quale strato � presente ad una quota e ritorna lo strato e la quota interna allo strato
		z=quota da investigare
		callback=metodo detl tipo di terreno da richiamare'''
		ind=where(self.quote<=z)[0][-1]
		if ind==len(self.strati): ind-=1
		return self.strati[ind],z-self.quote[ind],ind

	def interrogaStrato(self,callback,z,*args):
		'''z=quota a cui fare la ricerca
		callback= metodo o parametro del terreno di interesse'''
		st,zz,ind=self.cercaStrato(z)
		if hasattr(st.terreno,callback):
			v=getattr(st.terreno,callback)
			if callable(v):
				return v(zz,*args)
			else: return v
		else:
			raise IOError('{} non trovato in strato'.format(callback))

	def __str__(self):
		return 'q0={:.4g} sovraccarico\n'.format(self.q0) + '\n\n'.join( ['''Strato {n} -- quota={q:.4g}
{j}'''.format(n=n,j=j,q=self.quote[n]) for n,j in enumerate (self.strati)])


