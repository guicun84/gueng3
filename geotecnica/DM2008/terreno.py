#-*-coding:latin-*-

from scipy import *

class Terreno:
	tab62II={'m1':{'phi':1,'c':1,'cu':1,'gamma':1},
			'm2':{'phi':1.25,'c':1.25,'cu':1.4,'gamma':1}}

	def __init__(self,gamma,phi,c,cu=0,stato='m2'):
		self.stato=stato
		self._gamma=gamma
		self._phi=phi
		self._c=c
		self._cu=cu

	@property
	def gamma(self):
		return self._gamma
	
	@property
	def phi(self):
		if self.stato in list(Terreno.tab62II.keys()):
			k=Terreno.tab62II[self.stato]['phi']
		else:
			k=1
		return arctan(tan(self._phi)/k)

	@property
	def c(self):
		if self.stato in list(Terreno.tab62II.keys()):
			k=Terreno.tab62II[self.stato]['c']
		else:
			k=1
		return self._c/k
	
	@property
	def cu(self):
		if self.stato in list(Terreno.tab62II.keys()):
			k=Terreno.tab62II[self.stato]['cu']
		else:
			k=1
		return self._cu/k

	def __str__(self):
		return ''' condizione {stato}
&phi;={p:.4g} gradi
c={c:.4g} MPa
c~u~={cu:.4g} MPa
&gamma;={gamma:.4g} Kg/m^3'''.format(stato=self.stato,p=rad2deg(self.phi),c=self.c,cu=self.cu,gamma=self.gamma*1e8)
