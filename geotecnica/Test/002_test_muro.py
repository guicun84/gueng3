import unittest
from gueng.geotecnica.modelli_terreno.coulomb import Coulomb
from gueng.geotecnica.stratigrafia import Strato,Stratigrafia
from gueng.geotecnica.spinta_terre import SpintaLitostatica
from scipy import *
class MyTest(unittest.TestCase):
	def setUp(self):
		ter1=Coulomb(1800,25)
		ter2=Coulomb(1900,29)
		strati=[
			Strato(ter1,2e3),
			Strato(ter2,5e4),
		]
		stratigrafia=Stratigrafia(strati)
		lito=SpintaLitostatica(stratigrafia)
		vars(self).update(locals())

	def testSigmaV(self):
		tg=self.ter1.gamma*1*10/1e6
		test=self.ter1.sigmaV(1e3) #MPa
		self.assertTol(test,tg,)

	def testSigmaV2(self):
		s1=self.ter1.gamma*2*10/1e6
		s2=self.ter2.gamma*2*10/1e6
		tg=s1+s2
		test=self.stratigrafia.interrogaStrato('sigmaV',4e3)	
		self.assertTol(test,tg,)

	def testQh(self):
		#valore teorico su 4 m di fronte
		tg=self.ter1.gamma*2**2/2*self.ter1.K0*10
		#valore calcolato:
		z=linspace(0,2e3,100)
		f=lambda x:self.stratigrafia.interrogaStrato('sigmaH',x)
		sig=list(map(f,z))
		test=trapz(sig,z)*1e3
		self.assertTol(test,tg,)

	def testQh2(self):
		#valore teorico su 4 m di fronte
		tg=self.ter1.gamma*2**2/2*self.ter1.K0*10
		tg+=self.ter2.gamma*2**2/2*self.ter2.K0*10
		tg+=self.ter1.gamma*2*2*self.ter2.K0*10
		#valore calcolato:
		z=linspace(0,4e3,100)
		f=lambda x:self.stratigrafia.interrogaStrato('sigmaH',x)
		sig=list(map(f,z))
		test=trapz(sig,z)*1e3
		self.assertTol(test,tg,)

	def testQA(self):
		#valore teorico su 4 m di fronte
		tg=self.ter1.gamma*2**2/2*self.ter1.KA*10
		#valore calcolato:
		self.lito.h=2e3
		test=self.lito.FA
		self.assertTol(test,tg,)

	def testQA2(self):
		#valore teorico su 4 m di fronte
		tg=self.ter1.gamma*2**2/2*self.ter1.KA*10
		tg+=self.ter2.gamma*2**2/2*self.ter2.KA*10
		tg+=self.ter1.gamma*2*2*self.ter2.KA*10
		#valore calcolato:
		self.lito.h=4e3
		test=self.lito.FA
		self.assertTol(test,tg,)
	
	def assertTol(self,test,tg,tol=5e-3):
		print(f'test sigmah:test={test},tg={tg}')
		err=(test-tg)/tg
		self.assertGreater(err,-tol)
		self.assertLess(err,tol)

if __name__=='__main__':
	unittest.main(verbosity=2)
