
import os

#curdir=os.path.dirname(__file__)
#if curdir=='':
#	curdir=os.getcwd()
#os.chdir(curdir)
#os.chdir(os.path.join('..','modelli_terreno'))
#import coulomb
#reload(coulomb)
#from coulomb import Coulomb
#os.chdir(curdir)
#os.chdir(os.path.join('..','capacita_portanti'))
#import nastriforme
#reload(nastriforme)
#from nastriforme import Nastriforme
#os.chdir(curdir)

import  gueng.geotecnica.modelli_terreno.coulomb as coulomb
import imp
imp.reload (coulomb)
Coulomb=coulomb.Coulomb
import gueng.geotecnica.capacita_portanti.nastriforme as nastriforme
imp.reload(nastriforme)
Nastriforme=nastriforme.Nastriforme

ter=Coulomb(1900,24)
fond=Nastriforme(1750,10.8e3,ter,q=ter._gamma*1200,D=1200)
dat=fond.qlimMeyerof(fullout=True)
print(dat)
############################################################
ter=Coulomb(1900,22)
inte=.6
h=2.0
Fh=ter.KA*ter.gamma*10*h**2/2*inte
M=Fh*1/3*h

z=linspace(0,6e3,1e2)
sh=ter.sigmaH(z)
k=zeros_like(sh)
sp=ter.KP*ter.sigmaV(z)
f=Nastriforme(200,10e3,terreno=ter)
for n,i in enumerate(sh):
#	ql=f.qlimMeyerof(q=i)
	k[n]=f.k(q=i)

dx=diff(z)
dx=(r_[0,dx]+r_[dx,0])/2
k=k*200*dx
soglie=sp*200*dx
soglie[0]=soglie[1]/10


from gueng.FEM import *
Nodo.initType='d2'
n1=Nodo(0,0)
n1.gdl['vx'].f=Fh
n1.gdl['rz'].f=-M
n2=Nodo(0,-z[-1])
d=200
A=d**2*pi/4
J=d**4*pi/64
from gueng.materiali import Calcestruzzo
cl=Calcestruzzo(Rck=25)
tr=TraveMaster(n1,n2,A=A,Jz=J,E=cl.Ecm/2,part=len(z))
for i in tr.nodi: i.gdl['vy'].d=0 #tolgo i gradi di libertà verticali xche in questo esempio non mi interessano (accelero caclolo)
molle=[MollaVx(tr.nodi[n],i,soglia=soglie[n]) for n,i in enumerate(k)]
els=[]
els.extend(tr.parti)
els.extend(molle)
st=Struttura(els)
st.risolviSparse()

figure(1)
clf()
show(0)

subplot(1,2,1)
plot(soglie/dx/200,z,label=r'$\sigma_p+$')
plot(-soglie/dx/200,z,label=r'$\sigma_p-$')
ylim(r_[ylim()][-1::-1])
title('Tensione')
xlabel('$\sigma$ [MPa]')
ylabel('profondità [mm]')
grid(1)
f=r_[[i.Fl() for i in molle]].reshape(100)
f=f/200/dx
plot(f,z,label='0')
axvline(0,color='k')

subplot(1,2,2)
title('Spostamento')
xlabel('spostamento [mm]')
#ylabel(u'profondità')
v=vstack([i.plot('d') for i in tr.parti])
plot(v[:,0],v[:,1],label='0')
axvline(0,color='k')
grid(1)
draw()

import sys
n=1
while True:
	tests=[i.testPlastico(fullout=True) for i in molle]
	test=all([i['test'] for i in tests])
	st.M=None
	st.risolviSparse()
	n+=1
	if test or n>1e2: break

	er=[i['er'] for i in tests]
#	subplot(1,2,1)
#	f=r_[[i.Fl() for i in molle]].reshape(100)
#	f=f/200/dx
#	plot(f,z)
#	subplot(1,2,2)
#	v=vstack([i.plot('d') for i in tr.parti])
#	plot(v[:,0],v[:,1])
#	draw()
	sys.stdout.write('\r{} {} {}'.format(n,max(er),min(er)))
	sys.stdout.flush()

subplot(1,2,1)
f=r_[[i.Fl() for i in molle]].reshape(100)
f=f/200/dx
plot(f,z,linewidth=2,label='{}'.format(n))
legend(loc='best')
subplot(1,2,2)
v=vstack([i.plot('d') for i in tr.parti])
plot(v[:,0],v[:,1],linewidth=2,label='{}'.format(n))
legend(loc='best')
draw()

Fr=(f*200*dx).sum()
print('''controllo equilibrio:
forza orizontale applicata={Fh:.4g} N
risultante molle= {Fr:.4g}'''.format(**locals()))


