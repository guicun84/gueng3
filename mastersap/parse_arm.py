#-*-coding:latin-*-

import re,sqlite3,os
from gueng.utils import floatPath as fp, toDict 
import pandas as pa

class ParseArm:
#inizializzo le variabili:

	grppath=re.compile('Elemento:.*Gruppo:\s+(\d+)')
	descpath=re.compile('Descrizione:\s+(.*)')
	stpath=re.compile('Diametro staffe:\s+(\d+) mm\s+Numero braccia:\s+(\d+)')
	elpath=re.compile('ASTA NUM. (\d+)\s+NI (\d+)\s+NF (\d+)')
	basearmpath=re.compile('armatura base = (\d+) X ({0})'.format(fp))
	addarmpath=re.compile('apost=(.*)aant=(.*)ainf=(.*)asup=(.*)staffe=\s+(\d+) d\s+(\d+)\s+/\s+({0})'.format(fp))
	addarmpath2=re.compile('apost=(.*)aant=(.*)ainf=(.*)asup=\s*({})'.format(fp))
	solpath=re.compile('\s+\d+\s+({})'.format(fp))
	tabpath=re.compile('Tabella:\s*(.*)$')

	def __init__(self,fi,fiOut=None):	
		'''fi= relazione txt delle armature
		fiOut file per il database se none fi+.sqlite'''
		self.fi=fi
		if fiOut is None:
			self.fiOut=os.path.splitext(fi)[0]+'.sqlite'
		else:
			self.fiOut=fiOut
	@staticmethod
	def st2float(st):
			if re.match('\s*-[^\d]*',st): return 0
			else: return float(st)

	def parse(self):
		gr=None
		desc=None
		dst1=None
		nbr=None
		elnum=None
		ni=None
		nf=None
		numbase=None
		Abase=None
		apost=None
		aant=None
		ainf=None
		asup=None
		numst=None
		dst=None
		passo=None
		x=None
		tabver=None
		ordine='gr desc elnum ni nf x dst1 nbr numbase Abase apost aant ainf asup numst dst passo tabver'

		f=open(self.fi,'r')
		self.dats=pa.DataFrame()
		for r in f:
			t=ParseArm.tabpath.search(r)
			if t:
				tabver=t.group(1)

			t=ParseArm.grppath.match(r)
			if t:
				gr=int(t.group(1))
				continue

			t=ParseArm.descpath.match(r)
			if t:
				desc=t.group(1)
				continue

			t=ParseArm.stpath.match(r)
			if t:
				dst1=int(t.group(1))
				nbr=int(t.group(2))
				continue

			t=ParseArm.elpath.match(r)
			if t:
				elnum=int(t.group(1))
				ni=int(t.group(2))
				nf=int(t.group(3))
				continue

			t=ParseArm.basearmpath.match(r)
			if t:
				numbase=int(t.group(1))
				Abase=float(t.group(2))
				continue

			t=ParseArm.addarmpath.match(r)
			if t:
				apost=self.st2float(t.group(1))
				aant=self.st2float(t.group(2))
				ainf=self.st2float(t.group(3))
				asup=self.st2float(t.group(4))
				numst=int(t.group(5))
				dst=int(t.group(6))
				passo=float(t.group(7))
				dat=toDict(ordine)
				self.dats=self.dats.append(dat,ignore_index=1)
				continue

			t=ParseArm.addarmpath2.match(r)
			if t:
				apost=self.st2float(t.group(1))
				aant=self.st2float(t.group(2))
				ainf=self.st2float(t.group(3))
				asup=self.st2float(t.group(4))
				numst=None
				dst=None
				passo=None
				dat=toDict(ordine)
				self.dats=self.dats.append(dat,ignore_index=1)
				continue

			t=ParseArm.solpath.match(r)
			if t:
				x=float(t.group(1))

		self.dats=self.dats[ordine.split()]
		#dati delle armature caricati sul dataframe... il peggio � fatto
		self.db=sqlite3.connect(self.fiOut)
		self.dats.to_sql('armature',self.db,if_exists='append')
		self.db.commit()
