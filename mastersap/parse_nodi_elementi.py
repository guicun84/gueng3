from scipy import *
from gueng.utils import floatPath as fp
import re
import pandas as pa
import sqlite3,os


def log(*args,**kargs):
	if False:
		print(*args,**kargs)

class Parser:
	empty_line=re.compile('^\s*$')
	solpath=re.compile('Lavoro:.*?Intestazione lavoro:.*')

	def __init__(self,finame,dbname=None):
		self.finame=finame
		self.fi=open(finame)
		if dbname is None:
			dbname=os.path.splitext(finame)[0]+'.sqlite'
		self.dbname=dbname

	def elabora(self):
		self.gruppi=[]
		self.elementi_d1=[]
		self.sollecitazioni_d1=[]
		self.fi.seek(0)
		test=True
		while True:
			if test:
				r=self.fi.readline()
			else:
				test=True
			if r=='':break
			if 'NODI DEL SISTEMA' in r: 
				r=self.elabora_nodi()
				test=False
			if 'SEZIONI' in r:
				r=self.elabora_sezioni()
				test=False
			if 'ELEMENTO FINITO ASTA RETICOLARE GRUPPO' in r:
				r=self.elabora_elemento_asta(r)
				test=False
			if 'ELEMENTO FINITO TRAVE GRUPPO' in r:
				r=self.elabora_elemento_trave(r)
				test=False
				continue
			if self.solpath.match(r):
				r=self.elabora_sollecitazioni()
				test=False
		#creo Dataframe per poi salvarli su sqlite
		self.gruppi=pa.DataFrame(self.gruppi,columns=['tipo','numero','descrizione'])
		for n,i in enumerate(self.elementi_d1):
			if i[0]=='asta':
				self.elementi_d1[n]=i[:5]+[0,'Cerniera','Cerniera']+i[6:]
		self.elementi_d1=pa.DataFrame(self.elementi_d1,
			columns=['tipo','gruppo','elemento','ni','nj','nk','vincolo_i','vincolo_j','materiale','sezione']
			)
		self.sollecitazioni_d1=pa.DataFrame(self.sollecitazioni_d1,
			columns=['tipo','gruppo','elemento','combinazione','x','fx','fy','fz','mx','my','mz'])
		self.sezioni=pa.DataFrame(self.sezioni,
			columns=['numero','tipo','dati']
			)
		self.nodi=pa.DataFrame(self.nodi,columns=['numero','x','y','z'])


	def elabora_nodi(self):
		self.nodi=[]
		log('elaboro nodi')
		path=re.compile('\s+'.join([f'({fp})']*4))
		while True:
			r=self.fi.readline()
			if r=='':break
			if 'nodo' in r:continue
			test=path.search(r)
			if not test:break
			nodo=[int(test.group(1))]
			nodo.extend([float(test.group(i)) for i in range(2,5)])
			self.nodi.append(nodo)
		log('fine elaborazione nodi')
		log(f'len(nodi)={len(self.nodi)}')
		return r
	
	def elabora_sezioni(self):
		self.sezioni=[]
		sz=re.compile(f'({fp})\s+([A-Z][a-z])\s+(.*)')
		while True:
			r=self.fi.readline()
			if self.empty_line.match(r):continue
			if 'sez.' in r: continue
			test=sz.search(r)
			if not test:break
			s=[int(test.group(1)),test.group(2),test.group(3)]
			self.sezioni.append(s)
		log('fine sezioni')
		log(f'len(sezioni)={len(self.sezioni)}')
		return r

	def elabora_elemento_asta(self,r0):
		log(r0)
		gruppo=['aste',int(re.search('GRUPPO\s+N\.(\d+)',r0).group(1))]
		r=self.fi.readline()
		gruppo.append(re.search('\s+(.*?)\s*$',r).group(1))
		self.gruppi.append(gruppo)
		epath=re.compile('\s+'.join([f'({fp})']*6))
		fun=[int,int,int,float,int,int]
		while True:
			r=self.fi.readline()
			if self.empty_line.match(r):continue
			if 'asta' in r:continue
			test=epath.search(r)
			if not test:
				log(f'fine gruppo su linea\n{r}')
				break
			elemento=[j(i) for i,j in zip(test.groups(),fun)]
			self.elementi_d1.append(['asta',gruppo[1]]+elemento)
		log(f'fine gruppo {gruppo}')
		log(f'len(gruppi)={len(self.gruppi)}')
		log(f'len(elementi_d1)={len(self.elementi_d1)}')
		return r

	def elabora_elemento_trave(self,r0):
		gruppo=['travi',int(re.search('GRUPPO\s+N\.(\d+)',r0).group(1))]
		r=self.fi.readline()
		gruppo.append(re.search('\s+(.*?)\s*$',r).group(1))
		self.gruppi.append(gruppo)
		epath=re.compile(f'({fp})\s+({fp})\s+({fp})\s+({fp})\s+([^\s]+)\s+([^\s]+)\s+({fp})\s+({fp})')
		skippath=re.compile('\s+[A-Z]:')
		nulfun=lambda x:x
		fun=[int,int,int,int,nulfun,nulfun,int,int]
		while True:
			r=self.fi.readline()
			if r=='':break
			if self.empty_line.match(r):continue
			if 'asta' in r:continue
			if 'nodo' in r:continue
			if skippath.match(r):continue
			test=epath.search(r)
			if not test:
				log(f'fine gruppo su linea\n{r}')
				break
			elemento=[j(i) for i,j in zip(test.groups(),fun)]
			self.elementi_d1.append(['trave',gruppo[1]]+elemento)
		log(f'fine gruppo {gruppo}')
		log(f'len(gruppi)={len(self.gruppi)}')
		log(f'len(elementi_d1)={len(self.elementi_d1)}')
		return r

	def elabora_sollecitazioni(self):
		log('elaboro sollecitazioni')
		r=self.fi.readline()
		tipo=re.search('Elemento:\s+([A-Z]+)',r).group(1).lower()
		r=self.fi.readline()
		gruppo=int(re.search('Gruppo:\s+(\d+)',r).group(1))
		for i in range(6):r=self.fi.readline()
		solpath='\s+'+'\s+'.join(['(\d+[A-Z]?)']+[f'({fp})']*7)
		print(f"'{solpath}'")
		solpath=re.compile(solpath)
		elpath=re.compile('ASTA NUM.\s+(\d+)')
		nulfun=lambda x:x
		fun=[nulfun]+[float]*7
		skip=[
#			self.empty_line,
			re.compile('qy medio cond'),
			re.compile('^$'),
			re.compile('\s+----'),
			re.compile('\s+NC\s+(?:x)|(?:Fx)'),
			re.compile('\s+cm\s+daN'),
			re.compile('\s+daN'),
			re.compile('^[-\s]+$'),
			re.compile('\s+Verifica di STABILITA'),
			re.compile(f'.*Snell\.\s+\''),
		]
		while True:
			r=self.fi.readline()
			if r=='':break
			testskip=False
			for t in skip:
				if t.search(r):
					print('skip')
					print(t)
					try:
						print(f'gruppo {gruppo} elemento{elemento} tipo {tipo}')
					except:
						print(''''.... srvoliamo su gruppo, elemento e tipo...''')
					print(r)
					testskip=True
					break
			if testskip:continue
			test=elpath.match(r)
			if test:
				elemento=int(test.group(1))
				continue
			test=solpath.match(r)
			if test:
				sol=[tipo,gruppo,elemento]+[j(i) for i,j in zip(test.groups(),fun)]
				self.sollecitazioni_d1.append(sol)
				continue
			break
		log(f'fine elaborazione {tipo} {gruppo}\n"{r}"')
		return r

	def to_db(self):
		try: 
			self.db.execute('select * from sqlite_master limit 1')
		except:
			self.db=sqlite3.connect(self.dbname)
		self.nodi.to_sql('nodi',self.db,if_exists='replace')
		self.elementi_d1.to_sql('elementi_d1',self.db,if_exists='replace')
		self.sezioni.to_sql('sezioni',self.db,if_exists='replace')
		self.sollecitazioni_d1.to_sql('sollecitazioni_d1',self.db,if_exists='replace')
		self.gruppi.to_sql('gruppi',self.db,if_exists='replace')
		self.db.commit()

		self.db.executescript('''
		drop view if exists d1co;
		create view if not exists d1co as 
		select d1.[index] as [index]
		,d1.[index]||"mat:"||materiale||"sez:"||sezione as info
		,i.x as xi
		,i.y as yi
		,i.z as zi
		,j.x as xj
		,j.y as yj
		,j.z as zj 
		,i.x+(j.x-i.x)/3 as xn
		,i.y+(j.y-i.y)/3 as yn
		,i.z+(j.z-i.z)/3 as zn
		from elementi_d1 d1 
		inner join nodi i on i.numero=d1.ni
		inner join nodi j on j.numero=d1.nj;

		drop view if exists sollecitazioni_d1_e;
		create view sollecitazioni_d1_e as select
		d1.[index] as elemento,
		combinazione,x,fx,fy,fz,mx,my,mz
		from elementi_d1 d1
		inner join sollecitazioni_d1 sd1 on d1.tipo=sd1.tipo and d1.gruppo=sd1.gruppo and d1.elemento=sd1.elemento
				''')
		self.db.commit()
		
		#oriento gli elementi
		dat=pa.read_sql('select * from d1co',self.db)
		def orienta(x):
			#versori per riga --> da globale a locale
			pi=r_[x.xi,x.yi,x.zi].astype(float)
			pj=r_[x.xj,x.yj,x.zj].astype(float)
			e1=pj-pi
			l=(e1**2).sum()**.5
			e1/=l
			if e1[2]==1:
				t3=r_[0,1.,0]
			else:
				t3=r_[0,0,1.]
			e2=cross(t3,e1)
			e3=cross(e1,e2)
			return pa.Series(r_[x['index'],l,e1,e2,e3],('index',
														'l',
														'r11',
														'r12',
														'r13',
														'r21',
														'r22',
														'r23',
														'r31',
														'r32',
														'r33'))
		res=dat.apply(orienta,1)
		res.to_sql('d1_info',self.db,if_exists='replace',index=False)
		del dat,res
		self.db.commit()
		self.db.executescript('''
		drop view if exists sollecitazioni_d1_e_glob;
		create view sollecitazioni_d1_e_glob as
		select elemento,combinazione,x,
		r11*fx+r21*fy+r31*fz as fx,
		r12*fx+r22*fy+r32*fz as fy,
		r13*fx+r23*fy+r33*fz as fz,
		r11*mx+r21*my+r31*mz as mx,
		r12*mx+r22*my+r32*mz as my,
		r13*mx+r23*my+r33*mz as mz
		from sollecitazioni_d1_e sd1 
		inner join d1_info on sd1.elemento=d1_info.[index]
		''')
		self.db.commit()
