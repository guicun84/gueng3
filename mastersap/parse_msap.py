
import sqlite3,re,os
from io import StringIO
from gueng.utils import floatPath as fp

class Parser:
	def __init__(self,finame):
		vars(self).update(locals())


	def isolaTravi(self):
		Travipath=re.compile('Elemento:\s+TRAVE')
		grppath=re.compile('Gruppo:\s+(\d+)')
		elpath=re.compile('ASTA NUM\.\s+(\d+)\s+NI\s+(\d+)\s+NF\s+(\d+)\s+Lungh\.\s+({fp})\s+cm\s+SEZ\.\s+(\d+)'.format(fp=fp))
		datpath=re.compile('\s+(\d+)\s+(\d+)\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})'.format(fp=fp))
		tests=Travipath,grppath,elpath,datpath
		out=StringIO()
		k1=False
		with open(self.finame,'rb') as fi:
			while True:
				r=fi.readline().decode('utf8')
				if not r:
					fi.close()
					break
				if not k1:
					if Travipath.match(r): k1=True
					print(r)
					continue
				for i in tests:
					if i.match(r):
						out.write(r)
						break
		out.seek(0)
		return out

	def isolaAste(self):
		elemento=re.compile('Elemento:\s+([A-Z]+)')
		grppath=re.compile('Gruppo:\s+(\d+)')
		elpath=re.compile('ASTA NUM\.\s+(\d+)\s+NI\s+(\d+)\s+NF\s+(\d+)\s+Lungh\.\s+({fp})\s+cm\s+SEZ\.\s+(\d+)'.format(fp=fp))
		datpath=re.compile('\s+(\d+)\s+({fp})'.format(fp=fp))
		tests=elemento,grppath,elpath,datpath
		out=StringIO()
		k1=False
		with open(self.finame,'rb') as fi:
			while True:
				r=fi.readline().decode('utf-8')
				if r=='':
					fi.close()
					break
				t=elemento.match(r)
				if t:
					if t.group(1)=='ASTA' and not k1: k1=True
					elif t.group(1)!='ASTA' and k1: break
				for i in tests[1:]:
					if i.match(r):
						out.write(r)
		out.seek(0)
		return out

	def isolaPilastri(self):
		endpath=re.compile('Lavoro:')
		typepath=re.compile('Elemento:\s+PILASTRO')
		elpath=re.compile('ASTA\s+NUM\.\s+(\d+)\s+NI\s+(\d+)\s+NF\s+(\d+)\s+SEZ\.\s+(.*)')
		pilpath=re.compile('PIL\.\s+NUM\.\s+(\d+)')
		datpath=re.compile('\s+(\d+)\s+(\d+)\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})'.format(fp=fp))
		tests=typepath,elpath,datpath,pilpath
		out=StringIO()
		k1=False
		with open(self.finame,'rb') as fi:
			for r in fi:
				if r=='':
					fi.close()
					break
				if k1:
					if endpath.match(r):
						k1=False
						continue
				if not k1:
					if typepath.match(r):
						k1=True
						out.write(r)
						continue
				for i in tests:
					if i.match(r):
						out.write(r)
						continue
		out.seek(0)
		return out

class Loader:
	def __init__(self,relname,dbname=None):
		if dbname is None:
			dbname=os.path.splitext(relname)[0]+'.sqlite'
		vars(self).update(locals())
		self.parser=Parser(self.relname)
		self.db=sqlite3.connect(self.dbname)

	def creadb(self):
		self.db.executescript('''create table if not exists d2(
	ide integer primary key autoincrement,
	type text,
	grp integer,
	num integer,
	i integer,
	j integer,
	sez integer,
	l real);

create table if not exists sollecitazionid2(
	ide integer primary key autoincrement,
	el integer,
	cmb integer,
	x real,
	N float,
	V2 float,
	V3 float,
	M1 float,
	M2 float,
	M3 float);

create table if not exists sezioni(
	ide integer primary key autoincrement,
	name text);

		create index sold2_el_x on sollecitazionid2(el,x);

			''')
	def loadTravi(self):
		fp='-?\d+(?:\.\d+)?(?:e-?\d+)?'
		grppath=re.compile('Gruppo:\s+(\d+)')
		elpath=re.compile('ASTA NUM\.\s+(\d+)\s+NI\s+(\d+)\s+NF\s+(\d+)\s+Lungh\.\s+({fp})\s+cm\s+SEZ\.\s+(\d+)\s+Ps\s+(.+)'.format(fp=fp))
		datpath=re.compile('\s+(\d+)\s+(\d+)\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})'.format(fp=fp))
		d=self.parser.isolaTravi()
		elid=0
		for i in d.readlines():
			t=grppath.search(i)
			if t:
				gr=int(t.group(1))
				continue
			t=elpath.search(i)
			if t:
				num=int(t.group(1))
				ni=int(t.group(2))
				nf=int(t.group(3))
				l=float(t.group(4))
				sez=int(t.group(5))
				sezname=t.group(6)
				if not self.db.execute('select 1 from sezioni where ide=?',(sez,)).fetchone():
					self.db.execute('insert into sezioni values (:sez,:sezname)',locals())
				self.db.execute('insert into d2 values (Null,"trave",?,?,?,?,?,?)',(gr,num,ni,nf,sez,l))
				elid=self.db.execute('select max(ide) from d2').fetchone()[0]
				continue
			t=datpath.search(i)
			if t:
				nc= t.group(1)
				x= t.group(2)
				N= t.group(3)
				V2= t.group(4)
				V3= t.group(5)
				M1= t.group(6)
				M2= t.group(7)
				M3= t.group(8)
				self.db.execute('insert into sollecitazionid2 values (Null,:elid,:nc,:x,:N,:V2,:V3,:M1,:M2,:M3)',locals())
		self.db.commit()

	def loadPilastri(self):
		fp='-?\d+(?:\.\d+)?(?:e-?\d+)?'
		grppath=re.compile('Elemento:\s+PILASTRO\s+Gruppo:\s+(\d+)')
		elpath=re.compile('ASTA\s+NUM\.\s+(\d+)\s+NI\s+(\d+)\s+NF\s+(\d+)\s+SEZ\.\s+(.*)')
		pilpath=re.compile('PIL\.\s+NUM\.\s+(\d+)([A-Z]*)')
		datpath=re.compile('\s+(\d+)\s+(\d+)\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})'.format(fp=fp))
		d=self.parser.isolaPilastri()
		for i in d.readlines():
			t=grppath.search(i)
			if t:
				gr=int(t.group(1))
				continue
			t=elpath.search(i)
			if t:
				num=int(t.group(1))
				ni=int(t.group(2))
				nf=int(t.group(3))
				sez=t.group(4)
				szid=self.db.execute('select ide from sezioni where name=?',(sez,)).fetchone()
				if not szid:
					self.db.execute('insert into sezioni values (Null,?)',(sez,))
					szid=self.db.execute('select ide from sezioni where name=?',(sez,)).fetchone()
				sez=szid[0]

			t=pilpath.search(i)
			if t:
				tipo='pilastro-{}-{}'.format(t.group(1),t.group(2))
				self.db.execute('insert into d2 values (Null,?,?,?,?,?,?,?)',(tipo,gr,num,ni,nf,sez,None))
				elid=self.db.execute('select max(ide) from d2').fetchone()[0]
				continue
			t=datpath.search(i)
			if t:
				nc= t.group(1)
				x= t.group(2)
				N= t.group(3)
				V2= t.group(4)
				V3= t.group(5)
				M1= t.group(6)
				M2= t.group(7)
				M3= t.group(8)
				self.db.execute('insert into sollecitazionid2 values (Null,:elid,:nc,:x,:N,:V2,:V3,:M1,:M2,:M3)',locals())
		self.db.commit()

	def loadAste(self):
		grppath=re.compile('Gruppo:\s+(\d+)')
		elpath=re.compile('ASTA NUM\.\s+(\d+)\s+NI\s+(\d+)\s+NF\s+(\d+)\s+Lungh\.\s+({fp})\s+cm\s+SEZ\.\s+(\d+)\s+Ps\s+(.+)'.format(fp=fp))
		datpath=re.compile('\s+(\d+)\s+({fp})'.format(fp=fp))
		d=self.parser.isolaAste()
		for i in d.readlines():
			t=grppath.search(i)
			if t:
				gr=int(t.group(1))
				continue
			t=elpath.search(i)
			if t:
				num=int(t.group(1))
				ni=int(t.group(2))
				nf=int(t.group(3))
				l=float(t.group(4))
				sez=int(t.group(5))
				sezname=t.group(6)
				if not self.db.execute('select 1 from sezioni where ide=?',(sez,)).fetchone():
					self.db.execute('insert into sezioni values (:sez,:sezname)',locals())
				self.db.execute('insert into d2 values (Null,"asta",?,?,?,?,?,?)',(gr,num,ni,nf,sez,l))
				elid=self.db.execute('select max(ide) from d2').fetchone()[0]
				continue
			t=datpath.search(i)
			if t:
				nc= t.group(1)
				x= 0
				N= t.group(2)
				V2= 0
				V3= 0
				M1= 0
				M2= 0
				M3= 0
				self.db.execute('insert into sollecitazionid2 values (Null,:elid,:nc,:x,:N,:V2,:V3,:M1,:M2,:M3)',locals())
		self.db.commit()

	def loadNodi(self,fi):
		'''fi=file con l'input dei nodi'''
		startpath=re.compile(' nodo   coord X   coord Y   coord Z       temp     uX         uY         uZ         rX         rY         rZ ')
		datpath='({fp})\s+'
		datpath*=11
		datpath=re.compile(('\s+'+datpath).format(fp=fp))
		endpath=re.compile('\s+LEGENDA: descrizione della simbologia adottata per i gradi di liberta')
		test=False
		self.db.execute('''create table if not exists nodi (ide integer primary key autoincrement,
		x real,
		y real,
		z real,
		temp real,
		ux integer,
		uy integer,
		uz integer,
		rx integer,
		ry integer,
		rz integer)''')
		with open(fi) as fi:
			for r in fi:
				if not test:
					if startpath.match(r):
						test=True
						continue
				else:
					if endpath.match(r): break		
					t=datpath.match(r)
					if t:
						x=int,float,float,float,float,int,int,int,int,int,int
						dat=[j(i) for i,j in zip(t.groups(),x)]
						self.db.execute('insert into nodi values (?,?,?,?,?,?,?,?,?,?,?)',dat)
			self.db.commit()

	def __call__(self):
		self.creadb()
		self.loadAste()
		self.loadPilastri()
		self.loadTravi()


def R(pos):
	'''calcola matrice rotazione locale-globale
	pos=xyz nodo i e j'''
	e1=pos[1]-pos[0]
	e1/=norm(e1)
	e2=cross((0,0,1.),e1)
	if not norm(e2):
		e2=cross((0,0,1),(e1))
	e2/=norm(e2)
	e3=cross(e1,e2)
	return c_[e1,e2,e3]

	

		

			

