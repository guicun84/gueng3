#-*- coding:utf-8-*-

from pylab import *
from scipy import *
from scipy.optimize import root
from scipy.interpolate import interp1d
from scipy.integrate import cumtrapz
from gueng.utils import *

class ForzaConcentrata:
	def __init__(self,f,x,gamma=1,parent=None,name=None):
		vars(self).update(locals())
	
	def F(self,y):
		return (self.x>=y)*self.f*self.gamma

	def br(self,y):
		return self.x-y
	
	def M(self,y):
		return self.F(y)*self.br(y)

	def plot(self,scala=1e4):
		arrow(self.x,0,0,self.f/scala)	
		text(self.x,self.f/scala,'Q={:.4g}N'.format(self.f))
	
	def __str__(self):
		if not self.name: n=''
		else: n=self.name
		st='''x={x} mm; F={f:.4g} N; gamma={gamma:.4g}; {}'''.format(n,**vars(self))
		return st
		

class ForzaDistribuita:
	def __init__(self,fi,fj=None,xi=0,xj=None,gamma=1,parent=None,name=None):
		'''fi,fj[=None] : valori della forza agli estremi, se fj=None fj=fi
		   xi[=0],xj[=None] : posizioni dove inizia e finisce la forza, se 
		   						xj=None verra' considerata successivamente la fine 
								della trave per tale valore'''
		vars(self).update(locals())
		if self.fj is None: self.fj=self.fi
		if self.xj is not None:	self.f=interp1d([self.xi,self.xj],[self.fi,self.fj])
		else: self.f=None

	def check_f(self):
		if self.f is None and self.xj is None:
			self.xj=self.parent.l
			self.f=self.f=interp1d([self.xi,self.xj],[self.fi,self.fj])

	def F_(self,y): #calcola la concentrazione della forza
		self.check_f()
		if y>self.xj:
			return 0
		if y<=self.xi:
			return trapz(self.f.y,self.f.x)
		else:
			fi=self.f(y)
			return trapz([fi,self.fj],[y,self.xj])

	def F(self,y):
		return self.F_(y)*self.gamma
	
	def br(self,y):
		if y>=self.xj:#la sezione è a destra del carico
			return 0
		if y<=self.xi:#le sezione è a sinistra del carico
			br=((self.f.x.ptp()*abs(self.f.y).min())*(self.f.x.ptp()/2) + (self.f.x.ptp()/2*self.f.y.ptp())*(self.f.x.ptp()/2+sign(abs(self.fj)-abs(self.fi))*self.f.x.ptp()/6))/abs(self.F_(y))
			return br+(self.xi-y)
		else: #la sezione è a cavallo del carico
			x=r_[y,self.xj]
			y_=self.f([y,self.xj])
			br=(x.ptp()/2 *  x.ptp()*abs(y_).min() + (x.ptp()/2+ sign(abs(y_[1])-abs(y_[0]))*x.ptp()/6)*x.ptp()*y_.ptp()/2)/abs(self.F_(y))
			return br

	def plot(self):
		plot([self.xi,self.xi,self.xj,self.xj],r_[0,self.fi,self.fj,0])
		if self.fi==self.fj: text(r_[self.xi,self.xj].mean(),self.fi/2,'q={:.4g} N/mm'.format(self.fi))
		else: text(r_[self.xi,self.xj].mean(),r_[self.fi,self.fj].mean(),'qi={:.4g},qj={:.4g}'.format(self.fi,self.fj))


	def M(self,y):
		return self.F(y)*self.br(y)

	def __str__(self):
		if not self.name: n=''
		else: n=self.name
		st='''xi={xi} mm; xj={xj} mm; fi={fi:.4g} KN/m; fj={fj:.4g} KN/m; gamma={gamma:.4g}; {}'''.format(n,**vars(self))
		return st

class Momento:
	def __init__(self,M,x,parent=None):
		vars(self).update(locals())

	def M(self,y):
		if y<=self.x:
			return self.M
		else:
			return 0

	def F(self,y):
		return 0

	def convertiCopia(self,br):
		f=self.M/br
		pos=self.x+r_[1,-1]*br/2
		return ForzaConcentrata(f,pos[0]),ForzaConcentrata(-f,pos[1])
	def convertiDistribuito(self,br):
		br_=br*2/3
		f_=self.M/br_
		f=f_*2/(br/2)
		return [ForzaDistribuita(0,f,self.x,self.x+br/2),
			    ForzaDistribuita(-f,0,self.x-br/2,self.x)]

class Trave:
	def __init__(self,l,E=1,J=1,F=None,name=None):
		'''mat =materiale utilizzato
		   t=spessore piastra
		   l=lunghezza piastra
		   b=base piastra
		   da definire meglio 
		   F=forze applicate e punto di applicazione
		   nota: il piano della piastra è x-y con origine in basso a dx, le forze applicate sono //z e il momento è //x'''
		vars(self).update(locals())
		if self.F is None:
			self.F=[]

		self.__equilibrato=False

	def Mv(self,y):
		return sum([i.M(y) for i in self.F])

	Mv=vectorize(Mv)


	def M(self,y):
		if not self.__equilibrato: self.equilibrio()
		out=self.Mv(self,y)
		try: len(out)
		except: out=out.tolist()
		return out

	def Tv(self,y):
		if y==0:y=self.l*1e-12 
		return -1*sum([i.F(y) for i in self.F])
	Tv=vectorize(Tv)

	def T(self,y):
		if not self.__equilibrato: self.equilibrio()
		out= self.Tv(self,y)
		try: len(out)
		except: out=out.tolist()
		return out

	def __add__(self,val):
		self.F.append(val)
		if hasattr(val,'parent'):val.parent=self
	
	def __iadd__(self,val):
		self.F.append(val)
		if hasattr(val,'parent'):val.parent=self
		return self

	
	def equilibrio(self):
		if self.__equilibrato: return None
		self.__equilibrato=True
		M=self.M(0)
		V=sum([i.F(0) for i in self.F])
		fj=-M/self.l
		fi=-V-fj
		self.F.append(ForzaConcentrata(fi,0))
		self.F.append(ForzaConcentrata(fj,self.l))

	def de_equilibra(self):
		if self.__equilibrato:
			del self.F[-2:]
			self.__equilibrato=False
		if hasattr(self,'_v'):
			del self._v

	def riequilibra(self):
		if self.__equilibrato: self.de_equilibra()
		self.equilibrio()
	
	def v(self,y):
		if not self.__equilibrato: self.equilibrio()
		if not hasattr(self,'_v'):
			x=linspace(0,self.l,201)
			M=r_[[self.M(i) for i in x]]
			chi=M/self.E/self.J
			phi=r_[0,cumtrapz(chi,x)]
			v=r_[0,cumtrapz(phi,x)]
			v-=v[-1]/self.l*x
			self._v=interp1d(x,v,fill_value=0)
		return self._v(y)

	def plot(self,*args,**kargs):
#		calcolo scala per carichi concentrati:
		Fmax=max([0,max([i.f for i in self.F if isinstance(i,ForzaConcentrata)])])
		Fmin=min([0,min([i.f for i in self.F if isinstance(i,ForzaConcentrata)])])
		fmax=r_[0,r_[[r_[i.fi,i.fj] for i in self.F if isinstance(i,ForzaDistribuita)]].flatten()].max()
		fmin=r_[0,r_[[r_[i.fi,i.fj] for i in self.F if isinstance(i,ForzaDistribuita)]].flatten()].min()
		if Fmax==Fmin or fmax==fmin:
			scala=1
		else:
			scala=abs((Fmax-Fmin)/(fmax-fmin))
		for i in [j for j in self.F if isinstance(j,ForzaConcentrata)]:
			i.plot(scala)
		for i in [j for j in self.F if isinstance(j,ForzaDistribuita)]:
			i.plot()
		plot([0,self.l],[0,0],linewidth=2,color='k')
		ymax=max([Fmax/scala,fmax])
		ymin=min([Fmin/scala,fmin])
		y=r_[ymin,ymax]
		s=sign(y)
		y=y+s*.1*y.ptp()
		ylim(y)
		xlim([-self.l/10,self.l*1.1])

	def __str__(self):
		if self.name: n=self.name
		else: n=''
		st='''{} l={l:.4g} mm ; E={E:.3g} MPa; J={J:.4g} mm^4
Forze:
'''.format(n,**vars(self))
		if self.__equilibrato:
			f=['{}'.format(i) for i in self.F[:-2]]
		else:
			f=['{}'.format(i) for i in self.F]
		return st+'\n'.join(f)		


	






if __name__=='__main__':
	import unittest
	class test_forze(unittest.TestCase):
		def test_forze_concentrate(self):
			fc=ForzaConcentrata(-1,1)
			self.assertEqual(fc.F(0),-1)
			self.assertEqual(fc.br(0),1)
			self.assertEqual(fc.M(0),-1)
		def test_forze_concentrate_gamma(self):
			fc=ForzaConcentrata(-1,1,2)
			self.assertEqual(fc.F(0),-2)
			self.assertEqual(fc.br(0),1)
			self.assertEqual(fc.M(0),-2)
		def test_forze_distribuit_a_0(self):
			f=ForzaDistribuita(0,-1,1,2)
			self.assertEqual(f.F(0),-.5)
			self.assertAlmostEqual(f.br(0)/(1+2/3),1,6)
			self.assertAlmostEqual(f.M(0)/(.5*(1+2/3)),-1,6)
		def test_forze_distribuite_gamma_a_0(self):
			f=ForzaDistribuita(0,-1,1,2,2)
			self.assertEqual(f.F(0),-1)
			self.assertAlmostEqual(f.br(0)/(1+2/3),1,6)
			self.assertAlmostEqual(f.M(0)/(.5*(1+2/3)),-2,6)
		def test_forze_distribuit_a_1(self):
			f=ForzaDistribuita(0,-1,1,2)
			self.assertEqual(f.F(1),-.5)
			self.assertAlmostEqual(f.br(1)/(2/3),1,6)
			self.assertAlmostEqual(f.M(1)/(.5*(2/3)),-1,6)
		def test_forze_distribuite_gamma_a_1(self):
			f=ForzaDistribuita(0,-1,1,2,2)
			self.assertEqual(f.F(1),-1)
			self.assertAlmostEqual(f.br(1)/(2/3),1,6)
			self.assertAlmostEqual(f.M(1)/(.5*(2/3)),-2,6)
		def test_forze_distribuit_a_15(self):
			f=ForzaDistribuita(0,-1,1,2)
			self.assertEqual(f.f(1.5),-.5)
			self.assertEqual(f.F(1.5),-.375)
			self.assertAlmostEqual(f.br(1.5)/.2777777,1,6)
			self.assertAlmostEqual(f.M(1.5)/(.375*.2777777),-1,6)
		def test_forze_distribuit_gamma_a_15(self):
			f=ForzaDistribuita(0,-1,1,2,2)
			self.assertEqual(f.f(1.5),-.5)
			self.assertEqual(f.F(1.5),-.375*2)
			self.assertAlmostEqual(f.br(1.5)/.2777777,1,6)
			self.assertAlmostEqual(f.M(1.5)/(.375*.2777777),-2,5)
		def test_forze_distribuite_15bis(self):
			f=ForzaDistribuita(-1,-2,1,2,2)
			y=1.5
			self.assertEqual(f.f(y),-1.5)
			self.assertEqual(f.F(y),-1.75)
			self.assertAlmostEqual(f.br(y)/ (0.2619047) ,1,5)		
			M=abs(f.F(y)*f.br(y))
			self.assertAlmostEqual(f.M(y)/ (M) ,-1,5)		


	class test_trave(unittest.TestCase):
		def setUp(self):
			self.tr=Trave(3)
			self.f1=ForzaConcentrata(-1,1)
			self.f2=ForzaDistribuita(-1,-2,1,2)
			for i in self.f1,self.f2:self.tr+i
			self.tr.riequilibra()

		def test_equilibrio_F1(self):
			f1=2/3
			f1+=1/2 + .5*(1+1/3)/3
			self.assertAlmostEqual(self.tr.F[-2].f/f1,1,6)
		def test_equilibrio_F2(self):
			f1=1/3
			f1+=1/2 + .5*(1+2/3)/3
			self.assertAlmostEqual(self.tr.F[-1].f / f1,1,6)
		def test_momento_0(self):
			self.assertEqual(self.tr.M(0),0)
		def test_momento_l(self):
			self.assertEqual(self.tr.M(self.tr.l),0)

		def test_equilibrio_F1_con_gamma(self):
			self.f1.gamma=2
			self.f2.gamma=2
			self.tr.riequilibra()
			f1=2/3
			f1+=1/2 + .5*(1+1/3)/3
			self.assertAlmostEqual(self.tr.F[-2].f/f1,2,6)
		def test_equilibrio_F2_con_gamma(self):
			self.f1.gamma=2
			self.f2.gamma=2
			self.tr.riequilibra()
			f1=1/3
			f1+=1/2 + .5*(1+2/3)/3
			self.assertAlmostEqual(self.tr.F[-1].f / f1,2,6)
		
		def test_momento_estremi_gamma(self):
			self.f1.gamma=2
			self.f2.gamma=2
			self.tr.riequilibra()
			self.assertEqual(self.tr.M(0),0)
			self.assertEqual(self.tr.M(self.tr.l),0)

		def test_momento_mezzeria(self):
			l=self.tr.l/2
			atteso=self.f1.F(l)*self.f1.br(l) + self.f2.F(l)*self.f2.br(l) + self.tr.F[-1].F(l)*self.tr.F[-1].br(l)
			f1=1/3
			f1+=1/2 + .5*(1+2/3)/3
			m1=f1*l
			m2=.5*1.5*.5/2
			m3=.5*.5*1/2*.5*2/3
			atteso=m1-m2-m3
			self.assertAlmostEqual(self.tr.M(l) / atteso,1,6)

		def test_deformazione_distribuito(self):
			tr=Trave(3)
			q=ForzaDistribuita(-1)
			va=-5/384*3**4
			tr+q
			tr.riequilibra
			vc=tr.v(1.5)
			print(vc/va)
			self.assertAlmostEqual(va/vc,1,3)

		def test_deformazione_concentrato(self):
			tr=Trave(3)
			q=ForzaConcentrata(-1,1.5)
			va=-1/48*3**3
			tr+q
			tr.riequilibra
			vc=tr.v(1.5)
			print(vc/va)
			self.assertAlmostEqual(va/vc,1,3)

	unittest.main(verbosity=2)



