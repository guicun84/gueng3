from numpy import sin,cos,tan,arcsin

class RinfiancoVolta:
	#volta circolare
	def __init__(self,rho,c,f=None):
		'''rho densità (kg/m^3)
		c,f= mm corda e freccia volta, se f=None considera a botte
		'''
		if f is None:
			f=c/2
		vars(self).update(locals())
		'''(yc+f)**2=(yc**2+(c/2)**2)
		yc**2+2yc*f+f**2-yc**2-c**2/4=0
		yc=(c**2/4-f**2)/2f'''
		self.yc=((c/2)**2-f**2)/(2*f)
		self.R=self.yc+f
		self.alpha=2*arcsin(c/2/self.R)
		self.A=c*f-(self.R**2*self.alpha/2 - self.c*self.yc/2)
		self.W=self.A*self.rho/1e6
		self.q=self.W/c*1e3

	def __str__(self):
		return f'''Rinfianco volta
corda= {self.c:.1f} mm, freccia= {self.f:.1f} mm, raggio={self.R:.1f} mm
peso a m lineare ={self.W:.1f} kg/m
peso a m^2 = {self.q:.1f} kg/m^2'''


	
