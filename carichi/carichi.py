
from io import StringIO
from copy import copy
import pandas as pa
from scipy import ones

class CasoCarico:
	tipiCarichi='''Categoria A Ambienti ad uso residenziale
Categoria B Uffici
Categoria C Ambienti suscettibili di affollamento
Categoria D Ambienti ad uso commerciale
Categoria E Bibiloteche, archivi, magazzini e ambienti ad uso industriale
Categoria F Rimesse e parcheggi (per autoveicoli di peso <=30KN)
Categoria G Rimesse e parcheggi (per autoveicoli di peso >=30KN)
Categoria H Coperture
Vento
Neve (a quota <=1000m)
Neve (a quota >=1000m)
Variazioni Termiche'''
	__tabPsi=StringIO()
	__tabPsi.write('''
carico   | 0   | 1   | 2
a        | 0.7 | 0.5 | 0.3
b        | 0.7 | 0.5 | 0.3
c        | 0.7 | 0.7 | 0.6
d        | 0.7 | 0.7 | 0.6
v        | 1.0 | 0.9 | 0.8
f        | 0.7 | 0.7 | 0.6
g        | 0.7 | 0.5 | 0.3
h        | 0.0 | 0.0 | 0.0
neve1    | 0.5 | 0.2 | 0.0
neve2    | 0.7 | 0.5 | 0.2
termiche | 0.6 | 0.5 | 0''')
	__tabPsi.seek(0)
	__tabPsi=pa.read_csv(__tabPsi,sep='\s+\|\s+',skiprows=1,header=0,index_col=0)

	__tabGam=StringIO()
	__tabGam.write('''
durata | favorevole | equ | a1  | a2
p      | 1          | 0.9 | 1   | 1
p      | 0          | 1.1 | 1.3 | 1
v      | 1          | 0   | 0   | 0
v      | 0          | 1.5 | 1.5 | 1.3''')
	__tabGam.seek(0)
	__tabGam=pa.read_csv(__tabGam,sep='\s+\|\s+', skiprows=1, header=0)

	tg=__tabGam
	tp=__tabPsi

	def __init__(self,val=None,tipo='a',favorevole=False,durata='p'):
		if tipo in self.__tabPsi.index:
			vars(self).update(locals())
		else: raise KeyError

	def checkOther(self,other):
		if other.__class__.__name__=='CasoCarico':
			if self.checkOther(other.val) and other.tipo==self.tipo: return 1
			else:return 0
		elif not hasattr(other,'__len__'):
			return 2
		elif hasattr(other,'size'):
			if other.size==1:return 2
			elif other.size==self.val.size: return 3
			else: return 0

	def __add__(self,val):
		k=self.checkOther(val)
		if k:
			if k == 1: return self.__addCasoCarico(val)
			elif k in [2,3] : return self.__addValue(val)
		else: raise TypeError
	
	def __iadd__(self,val):
		c=self+val
		self.val=c.val
		return self

	def __addCasoCarico(self,caso):
		c=copy(self)
		c.val+=caso.val
		return c
	
	def __addValue(self,value):
		c=copy(self)
		c.val+=value
		return c

	def __mul__(self,val):
		c=copy(self)
		c.val*=val
		return c

	def __imul__(self,val):
		self.val*=val
		return self

	@property
	def psis(self):
		return self.__tabPsi.query('carico==@self.tipo')
	
	def psi(self,n):
		n='{}'.format(n)
		return self.psis[n].values[0]
	
	@property
	def gammas(self):
		f=int(self.favorevole)
		return self.__tabGam.query('durata==@self.durata and favorevole==@f'.format(self.durata,int(self.favorevole)))

	@property
	def gamma(self):
		return self.gammas['a1'].values[0]


class Combinazioni:
	def __init__(self,casi=[],M=None):
		vars(self).update(locals())
		del self.M
		self.__M=M

	@property 
	def M(self):
		l=len(self.casi)
		if self.__M is None: 
			self.__M= ones((l,l))
		elif self.__M.shape[0]!=l:
			o=ones((l,l))
			o[self.__M.shape[0]:self.__M.shape[1]]=self.__M
			self.__M=o
		return self.__M
	
	def __add__(self,casi):
		c=copy(self)
		c.casi.append(casi)
		return c
	
	def __iadd__(self,casi):
		self.casi.append(casi)
		return self

if __name__=='__main__':
	from scipy import rand
	from unittest import TestCase,main
	class MyTests(TestCase):
		def setUp(self):
			self.caso=CasoCarico(arange(5))
		def test_checkOther_Caso_Carico(self):
			other=CasoCarico(rand(5))
			k=self.caso.checkOther(other)
			self.assertEqual(k,1)
		def test_checkOther_Caso_Carico_differente(self):
			other=CasoCarico(rand(5),'b')
			k=self.caso.checkOther(other)
			self.assertEqual(k,0)
		def test_checkOther_Scalare(self):
			other=4
			k=self.caso.checkOther(other)
			self.assertEqual(k,2)
		def test_checkOther_vettore(self):
			other=rand(5)
			k=self.caso.checkOther(other)
			self.assertEqual(k,3)
		def test_checkOther_caso_sbagliato(self):
			other=CasoCarico(rand(4))
			k=self.caso.checkOther(other)
			self.assertEqual(k,0)
		def test_add_caso(self):
			other=CasoCarico(2*arange(5))
			c=self.caso+other
			self.assertTrue(all(c.val==arange(5)+2*arange(5)))
		def test_add_vettore(self):
			other=2*arange(5)
			c=self.caso+other
			self.assertTrue(all(c.val==arange(5)+2*arange(5)))
		def test_add_scalare(self):
			other=3
			c=self.caso+other
			self.assertTrue(all(c.val==arange(5)+3))
		def test_iadd(self):
			self.caso+=CasoCarico(2*arange(5))
			self.assertTrue(all(self.caso.val==arange(5)+2*arange(5)))
		def test_mult(self):
			c=self.caso*3
			self.assertTrue(all(c.val==arange(5)*3))
		def test_imul(self):
			self.caso*=3
			self.assertTrue(all(self.caso.val==arange(5)*3))
		def test_psi(self):
			self.assertEqual(self.caso.psi(0),.7)
		def test_gamma(self):
			self.assertEqual(self.caso.gamma,1.3)

	main(verbosity=2)

	


