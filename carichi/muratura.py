
from scipy import r_,prod

class Mattone:
	rho=1454
	tipo=dict(pieno=1.15,semipieno=1-.45,forato=1-.55)
	def __init__(self,tipo,dim=None):
		'''tipo=pieno|semipieno|forato
		dim=(s,h,b) se none:[8,15,30]
		'''
		if dim is None:
			dim=r_[.08,.15,.3]
		dim=r_[dim]
		dim.sort()
		vars(self).update(locals())
		self.V=prod(dim)
		self.rho=Mattone.rho*Mattone.tipo[self.tipo]
		self.W=self.rho*self.V
	def __str__(self):
		return '''Mattone {d[0]:.0f}x{d[1]:.0f}x{d[2]:.0f} , rho={r} '''.format(d=self.dim*1000,r=self.rho,W=self.W)

class Malta:
	tipi=dict(calce=1800,cemento=2100,bastarda=1900,gesso=1200)
	def __init__(self,tipo='calce',rho=None):
		if rho is None:
			rho=Malta.tipi[tipo]
		vars(self).update(locals())
	def __str__(self):
		return 'malta rho={}'.format(self.rho)
		
class Strato:
	def __init__(self,s,rho,name):
		vars(self).update(locals())
	@property
	def q(self):
		if not hasattr(self,'_q'):
			self._q=self.s*self.rho
		return self._q
	def __str__(self):
		return '{} {} q={} kg/m^2^'.format(self.__class__.__name__,self.name, self.q)

class Intonaco(Strato,Malta):
	def __init__(self,s,tipo='calce',rho=None,name=''):
		if rho is None:
			rho=Malta.tipi[tipo]
		Strato.__init__(self,s,rho,name)

class ParamentoBlocchi(Strato):
	def __init__(self,blocco,malta,s,gi=.005):
		'''blocco=blocco utilizzato
		s=spessore muro in mattoni
		gi=spessore giunto'''
		vars(self).update(locals())
		#ricerco numero teste:
		teste=s/self.blocco.dim[1]	
		if teste<1: self.intev=self.blocco.dim[1]
		else: self.intev=self.blocco.dim[0]
		W=blocco.rho*self.s*self.intev + malta.rho*gi*self.s
		self._q=W/(self.intev+self.self.gi)

	def __str__(self):
		return '''Paramento: blocchi: {} {} s={:.0f} giunto={:.0f} interasse={:.0f} q={:.2f} kg/m^2^'''.format(self.blocco,self.malta,self.s*1e3,self.gi*1e3,self.intev*1e3,self.q)


class Parete:
	def __init__(self,obj=[]):
		obj2=[i for i in obj if issubclass(i.__class__,Strato)]
		if len(obj2)!=len(obj): raise IOError
		vars(self).update(locals())

	def __add__(self,v):
		if  not issubclass(v.__class__,Strato): raise IOError
		new=copy(self)
		new.obj.append(v)
		return new

	def __iadd__(self,v):
		if  not issubclass(v.__class__,Strato): raise IOError
		self.obj.append(v)
		return self
	
	@property
	def q(self):
		return sum([i.q for i in self.obj])

	def __str__(self):
		return 'Parete\n\n- '+'\n- '.join(['%s'%i for i in self.obj])+'\n\nq={:.2f} kg/m^2^'.format(self.q)

	def incidenzam2(self,A,l1,l2,h1=2.75,h2=None):
		'''Incidenza a metro quadrato
		l1,l2 lunghezza delle pareti e delle aperture se l2<0 si sottraggono i vuoti delle aperture
		h1,h2=altezze pareti e aperture se h2=None considera porte di 2.2
		A=area totale
		misure in m'''
		if h2 is None:
			if l2>0: h2=h1-2.20 #sommo sopraporta
			else: h2=2.2 #sottraggo apertura
		return (l1*h1+l2*h2)*self.q/A

	def incidenzam(self,l1,l2,h1=2.75,h2=None):
		'''Incidenza a metro lineare
		l1,l2 lunghezza delle pareti e delle aperture se l2<0 si sottraggono i vuoti delle aperture
		h1,h2=altezze pareti e aperture se h2=None considera porte di 2.2
		misure in m'''
		if h2 is None:
			if l2>0: h2=h1-2.20 #sommo sopraporta
			else: h2=2.2 #sottraggo apertura
		if l2<0: ltot=l1
		else:ltot=l1
		return (l1*h1+l2*h2)*self.q/ltot

	def incidenzam2_(self,h=2.75):
		'''Incidenza a metro quadrato semplificata
		h=altezza locali'''
		return self.q*h*.4
		


if __name__=='__main__':
	m=Mattone('forato')
	ma=Malta()
	pa=ParamentoBlocchi(m,ma,.08,)
	into=Intonaco(.01)
	par=Parete([into])
	par+=pa
	par+=into
	print(par)







		
