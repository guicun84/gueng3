from importlib import reload
import CNR_DT_206_2007 
reload( CNR_DT_206_2007)
from CNR_DT_206_2007 import Predimensiona,Predimensiona2



from gueng.travi import TraveAppoggiataP as TA
from gueng.materiali import Legno
le=Legno('lam',1,'lunga','gl24c')
g,q=120,120
i=600
l=6000
qslu,qsle,qqp=r_[[
	[1.3,1.5],
	[1,1],
	[1,.3]]]@r_[g,q]
t=TA(l,r_[qslu,0]/1e5/sqrt(2),le.E0m)
#t=TA(l,r_[0,qslu]/1e5,le.E0m,lc=i)


djsle=t.cerca_v()[0]/qslu*qsle
p=Predimensiona(le,t.sol(),djsle,qsle,qqp,t.l,cumaxslu=.9)
#p.filt=p.pre.b.isin([100,120])

print(p.stat)

m=sqrt(2)
#p2=Predimensiona2(le,120/m,120/m,l,cumaxslu=.9,carico='uniforme',lc=i)
p2=Predimensiona2(le,120/m,120/m,l,cumaxslu=.9,carico='triangolare',)
print(p2.stat)
print('sollecitazioni')
print(p.sol)
print(p2.sol)
