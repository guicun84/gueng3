import pandas as pa
import os
from numpy import isnan,vstack,hstack,real,imag,r_,c_
dire=os.path.dirname(__file__)
files=[i for i in os.listdir(dire) if os.path.splitext(i)[1].lower()=='.csv']
files.sort()

def elabora_file(fi):
	dat=pa.read_csv(os.path.join(dire,fi),skiprows=1)
	col=list(dat.columns)
	col[0]='b'
	dat.columns=col
	sez=[]
	h=[100,120,140,160,200,240,280,320,360,400,440,480,520,560]
	for n,i in dat.iterrows():
		for hh in h:
			val=i[f'{hh}']
			if type(val)==str:
				if '*' in val:
					val=1j*int(val[:-1])
				else:
					val=int(val)
			if not isnan(val):
				if imag(val)==0:sez.append([i.b,hh])
				else:sez.append([i.b,1j*hh])

	return vstack(sez)

sezioni={os.path.splitext(i)[0]:elabora_file(i) for i in files}
class Sezioni:
	sezioni=sezioni

	@property
	def standard(self):
		return {i:j[imag(j[:,1])==0].astype('float') for i,j in Sezioni.sezioni.items()}
	@property
	def all(self):
		return {i:c_[j[:,0], real(j[:,1]) + imag(j[:,1])].astype(float) for i,j in Sezioni.sezioni.items()}

sez=Sezioni()
