#-*-coding:latin-*-

from scipy import *
import pandas as pa
from gueng.utils import Verifiche,Rettangolo

class GruppoVerifiche:
	def __init__(self):
		self.vers={}
	
	@property
	def cumax(self):
		return max((i.cumax for i in self.vers.values()))

class VerificaSLU(GruppoVerifiche):
	def __init__(self,sez,le,sol,ly=None,lz=None,Km=1):
		'''Esegue le verifiche a SLU di una sezione in legno secondo CNRDT-206-2007
		sez=sezione con A,Avy,Avz,J[y,z],W[y,z],Jt,Wt: classe o dizionario
		le=materiale legno
		sol=sollecitazioni [n,t2,t3,m1,m2[z],m3[y],[[...,cmb]]
		ly=luce libera inflessione direzione y (momento z)
		lz=luce libera inflessione direzione z (momento y)
		Km=.7 per sezioni rettangolari, 1 per altre sezioni'''

		if lz is None: lz=ly

		if type(sez)==dict:sez=pa.Series(sez)

		sol=atleast_2d(sol)
		if sol.shape[1]==6: #aggiungo numero combinazione
			cmbs=['cmb={}'.format(i) for i in range(sol.shape[0])]
		elif sol.shape[1]==7: 
			cmbs=['cmb={}'.format(i) for i in sol[:,-1]]
		elif sol.shape[1]==8:
			cmbs=['el{}, cmb={}'.format(i,j) for i,j in sol[:,-2:]]
		vars(self).update(locals())
		super().__init__()
		self.calcolaTensioni()
		self.verificaResistenza()
		self.verificaStabilita()


		#controllo su sol

	def calcolaTensioni(self):
		#calcolo delle tensioni
		self.sigN=self.sol[:,0]/self.sez.A
		self.sigm2=abs(self.sol[:,4]/self.sez.Wz)
		self.sigm3=abs(self.sol[:,5]/self.sez.Wy)
		self.tau2=abs(self.sol[:,1]/self.sez.Avy)
		self.tau3=abs(self.sol[:,2]/self.sez.Avz)
		self.tautg=(self.tau2**2 + self.tau3**2)**.5
		self.taut=abs(self.sol[:,3]/self.sez.Wt)

	def verificaResistenza(self):
		#verifica compressone-trazione
		if any(self.sigN!=0):
			self.vers['normale']=Verifiche('sforzo normale') #numero 6.7 o 6.8
			for n,i in enumerate(self.sigN):
				if i>0: self.vers['normale'](i,1,self.le.ft0d,f'trazione {self.cmbs[n]}')
				else: self.vers['normale'](-i,1,self.le.fc0d,f'compressione {self.cmbs[n]}')

		#verifica a flessione
		if any(self.sigm2!=0):
			self.vers['flessione2']=Verifiche('flessione asse 2')
			for n,i in enumerate(self.sigm2):self.vers['flessione2'](abs(i),1,self.le.fmd,self.cmbs[n])
		if any(self.sigm3!=0):
			self.vers['flessione3']=Verifiche('flessione asse 3')
			for n,i in enumerate(self.sigm3):self.vers['flessione3'](abs(i),1,self.le.fmd,self.cmbs[n])

		#verifica a presso-tenso flessione
		if any(self.sigN!=0) and (any(self.sigm2!=0) or any(self.sigm3!=0)):
			self.vers['pressoflex3']=Verifiche('presso-tenso flessione')
			if any(self.sigm3!=0):
				for n,i in enumerate(self.sigN):
					if i>0: self.vers['pressoflex3'](i/self.le.ft0d + self.sigm3[n]/self.le.fmd + self.Km*self.sigm2[n]/self.le.fmd,1,1,self.cmbs[n])
					else: self.vers['pressoflex3']((i/self.le.ft0d)**2 + self.sigm3[n]/self.le.fmd + self.Km*self.sigm2[n]/self.le.fmd,1,1,self.cmbs[n])
			if (self.Km!=1 and any(self.sigm2!=0)) or all(self.sigm3==0):
				self.vers['pressoflex2']=Verifiche('presso-tenso flessione 2')
				for n,i in enumerate (self.sigN):
					if i>0: self.vers['pressoflex2'](i/self.le.ft0d + self.Km*self.sigm3[n]/self.le.fmd + self.sigm2[n]/self.le.fmd,1,1,self.cmbs[n])
					else: self.vers['pressoflex2']((i/self.le.ft0d)**2 + self.Km*self.sigm3[n]/self.le.fmd + self.sigm2[n]/self.le.fmd,1,1,self.cmbs[n])

		#verifica a taglio
		if any(self.tau2!=0):
			self.vers['taglio2']=Verifiche('taglio 2')
			for n,i in enumerate(self.tau2):
				self.vers['taglio2'](i,1,self.le.fvd,self.cmbs[n])
		if any(self.tau3!=0):
			self.vers['taglio3']=Verifiche('taglio 3')
			for n,i in enumerate(self.tau3):
				self.vers['taglio3'](i,1,self.le.fvd,self.cmbs[n])
		if any(self.tau2!=0) and any(self.tau3!=0):
			self.vers['tagliotot']=Verifiche('taglio totale')
			for n,i in enumerate(self.tautg):
				self.vers['tagliotot'](i,1,self.le.fvd,self.cmbs[n])

		#verifica a torsione
		if any(self.taut!=0):
			self.vers['torsione']=Verifiche('torsione')
			for n,i in enumerate(self.taut):
				self.vers['torsione'](i,1,self.le.fvd,self.cmbs[n])

		#verifica taglio e torsione
		if any(self.taut!=0) and any(self.tautg!=0):
			self.vers['tgtor']=Verifiche('taglio e torsione')
			for n,i in enumerate(self.taut):
				self.vers['tgtor'](i/self.le.fvd + (self.tautg[n]/self.le.fvd)**2,1,1,self.cmbs[n])


	@staticmethod
	def Mcr(leff,E0m,E005,Gm,Iz,Itor):
		G005=Gm/E0m*E005
		return (pi/leff)*(E005*Iz*G005*Itor)**.5

	@staticmethod
	def lamrelm(fmk,sigmcr):
		return (fmk/sigmcr)**.5

	@staticmethod
	def kcrit(lamrelm):
		if lamrelm<=.75:return 1
		if lamrelm<1.4:return 1.56-.75*lamrelm
		return 1/lamrelm**2

	@staticmethod
	def sigcrc(l,E005,J,A):
		return pi**2*E/(l**2/(J/A))

	
	def instabilitaFlex(self):
		if self.sez.iy>self.sez.iz:
			self.Mcrz=self.Mcr(self.lz,self.le.E0m,self.le.E005,self.le.Gm,self.sez.Jz,self.sez.Jt)
			self.sigcrm=self.Mcrz/self.sez.Wz
			self.lamrelmz=self.lamrelm(self.le.fmk,self.sigcrm)
			self.kcritmz=self.kcrit(self.lamrelmz)
			self.vers['instabilita_flex']=Verifiche('instabilit flessionale')
			v=self.vers['instabilita_flex']
			v(self.sigm3,1,self.le.fmd/self.kcritmz,'instabilit flessionale asse forte')
			v(self.sigm2,1,self.le.fmd/self.kcritmz,'instabilit flessionale asse debole')
			v(self.sigm2 + self.Km*self.sigm3/self.kcritmz,1,self.le.fmd/self.kcritmz,'instabilit flessione deviata caso 1')
			v(self.Km*self.sigm2 + self.sigm3/self.kcritmz,1,self.le.fmd/self.kcritmz,'instabilit flessione deviata caso 2')

	
	def instabilitaPres(self):
		pass

	def instabilitaPresFlex(self):
		pass


	def verificaStabilita(self):
		self.instabilitaFlex()
		self.instabilitaPres()
		self.instabilitaPresFlex()

				

		
	def __str__(self):
		'''scrive report delle verifiche condotte
		'''

		st=[f'''# Verifica a SLU:
## Dati Sezione:
{self.sez}

## Dati trave:
l~l2~= {self.ly:.4g} mm luce libera direzione 2
l~l3~= {self.lz:.4g} mm luce libera direzione 3''']

		temp='''sigma~cr{n}~ = {sc}
lambda~relm{n}~ = {lam:.4g}
k~critm{n}~ = {k:.4g}'''
		if hasattr(self,'sigcrm') : 
			st.append(f'''
## Dati per instabilit flessionale
M~crz~={self.Mcrz:.4g} Nmm
sigma~crm~={self.sigcrm:.4g} Mpa
lambda~relm~={self.lamrelmz:.4f} --> k~critm~={ self.kcritmz:.4f}''')

		st.append(f'''
## Dati materiale
{self.le}
	''')

		for i in self.vers.keys():
			st.append(f'{self.vers[i]}\n\n')
		return '\n'.join(st)


class VerificaSLE(GruppoVerifiche):
	def __init__(self,sez,le,dsle,dqp,l,rsle=300,rinf=200,dj=False):
		vars(self).update(locals())
		super().__init__()
		if dj:
			self.dsle/=sez.Jy
			self.dqp/=sez.Jy
		self.verificaSpostamenti()

	def verificaSpostamenti(self):
		self.damsle=self.l/self.rsle
		self.daminf=self.l/self.rinf
		v=Verifiche('spostamenti')
		self.vers['spostamenti']=v
		v(self.dsle,1,self.damsle,'spostamento istantaneo',rel=self.l)
		self.dfin=self.dsle + 1/(1+self.le.kdef)*self.dqp
		v(self.dfin,1,self.daminf,'spostamento finale',rel=self.l)

	def __str__(self):
		st=''
		for i in self.vers.values():
			st+=f'{i}\n'
		return st

try:
	from .sezioni_lamellare import sez
except:
	from sezioni_lamellare import sez

class Predimensiona:
	sezioni=sez
	def __init__(self,le,sol,djsle,qsle,qqp,ly,lz=None,Km=1,sezioni=None,gruppo_sezioni='gl24c vista',standard=True,cumaxslu=1,cumaxsle=1,filt=None):
		if cumaxsle is None or cumaxsle==0:cumaxsle=cumaxslu
		vars(self).update(locals())
		if not sezioni:
			if standard:
				self.sez=Predimensiona.sezioni.standard
			else :
				self.sez=Predimensiona.sezioni.all
			self.sez=self.sez[gruppo_sezioni]
		else:
			self.sez=sezioni

		djqp=djsle/qsle*qqp
		self.pre=pa.DataFrame([[
			i[0],
			i[1],
			VerificaSLE(Rettangolo(*i),le,djsle,djqp,ly,dj=True),
			VerificaSLU(Rettangolo(*i),le,sol,ly,lz,Km),
			]
			for i in self.sez],columns='b h sle slu'.split())

		self.pre['A']=self.pre.b*self.pre.h
		self.pre['Jy']=self.pre.b*self.pre.h**3/12
		self.pre['G']=self.pre.A*le.rok/1e6
		self.pre['cuslu']=self.pre.apply(lambda x:x.slu.cumax,1)
		self.pre['cusle']=self.pre.apply(lambda x:x.sle.cumax,1)
		self.pre['cumax']=self.pre.apply(lambda x:max((x.cusle,x.cuslu)),1)
		self.cumask=(self.pre.cuslu<=self.cumaxslu) & (self.pre.cusle<=self.cumaxsle)
		self.pre['r']=self.pre.h/self.pre.b
		self.pre['rr']=abs(1-self.pre.r/sqrt(2))
		self.pre['rc']=(1+self.pre.rr)**2*(self.pre.h/self.pre[self.cumask].h.min())

	
	def stat_(self,):
#		return self.pre[self.cumask][['b','h','rr','cuslu','cusle','cumax','G']].sort_values('cuslu',ascending=False)[:n]
		return self.pre.loc[self.pre[self.pre.cumax<=1].groupby('b').h.idxmin()]['b h cuslu cusle cumax r G'.split()]


	
	@property
	def stat(self):
		return self.stat_()

	def _best(self,key,ascending=True,G=True):
		if G:
			key=[key,'G']
			ascending=[ascending,True]
		if self.filt is None:
			return self.pre[self.cumask].sort_values(key,ascending=ascending).iloc[0]
		else:
			return self.pre[self.cumask & self.filt].sort_values(key,ascending=ascending).iloc[0]

	@property
	def bestG(self):
		return self._best('G')

	@property
	def besth(self):
		return self._best('h')

	@property
	def bestr(self):
		return self._best('rr')
		
	@property
	def bestrc(self):
		return self._best('rc')

	@property
	def bestcu(self):
		return self._best('cumax',False)

from gueng.travi.travi_poly import TraveAppoggiata as TA,TraveIncastroCerniera as TIC, TraveIncastrata as TI,TraveIncastroIncastro as TII

class Predimensiona2(Predimensiona):
	schemi=dict(
		TA=TA,
		TI=TI,
		TIC=TIC,
		TII=TII,
		)
	def __init__(self,le,g,q,ly,lz=None,Km=1,sezioni=None,gruppo_sezioni='gl24c vista',standard=True,cumaxslu=1,cumaxsle=1,filt=None,
		schema='TA',carico='uniforme',lc=1):
		qslu,qsle,qqp=r_[[ [1.3,1.5],[1,1],[1,.3]]]@r_[g,q]
		if carico=='uniforme':
			qtr=r_[0,1]*qslu
		elif carico=='triangolare':
			qtr=r_[1,0]*qslu
		elif carico=='trapezoidale':
			qtr=r_[1,1]*qslu

		self.trave=self.schemi[schema.upper()](ly,qtr/1e5,E=le.E0m,lc=lc)
		djsle=self.trave.cerca_v()[0]/qslu*qsle
		super().__init__(le,self.trave.sol(),djsle,qsle,qqp,ly,lz,Km,sezioni,gruppo_sezioni,standard,cumaxslu,cumaxsle,filt)
		



	
