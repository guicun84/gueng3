#-*-coding:utf-8-*-

from scipy import *

'''PROBLEMATICHE CONOSCIUTE

1) nel calcolo del giunto  viene considerata come larghezza quella totale dell'elemento, cosa sempre vera solo se il connettore è passante (bullone, spinotto non sottomesso)

DA FARE 
1) passare o far calcolare l'effettiva profondità del connettore per eliminare la problematica 1 
'''

class connettore:
	def __init__(self,d,mat):
		vars(self).update(locals())

	def Nrk(self):
		return self.mat.fyd*self.d**2*pi/4	

class bullone(connettore):
	def __init__(self,d,mat,rond=None):
		if rond is None: rond=2*d
		vars(self).update(locals())

	def passi(self,alpha=0):
		'''calcola i passi minimi per bullonature
alpha in radianti'''
		a1=(4+3*abs(cos(alpha)))*self.bul.d
		a2=4*self.bul.d
                a3f=max([7*self.bul.d,80.])
                if 270< rad2deg(alpha) or rad2deg(alpha)<90:
                        a3c=0.
		elif 90<=rad2deg(alpha)<=150:
			a3c=max([(1+6*sin(alpha))*self.bul.d , 4*self.bul.d])
		elif 150<=rad2deg(alpha)<=210:
			a3c=4*self.bul.d
		else:
			a3c=max([(1+6*abs(sin(alpha)))*self.bul.d , 4*self.bul.d])
		if 0<rad2deg(alpha)<180:
			a4f=max([(2+2*sin(alpha))*self.bul.d, 3*self.bul.d])
		else:
                        a4f=max([(2-2*sin(alpha))*self.bul.d, 3*self.bul.d])
		a4c=3*self.bul.d
		out=locals()
		del out['self']
		return out

	def fh0k(self,le):
		'''resistenza caratteristica a rifollamento per alpha=0'''
		return .082*(1-.01*self.d)*le.rok

	def fhak(self,le,alpha=0):
		'''resistenza caratteristica a rifollamento per alpha!=0'''
		return self.fh0k(le)/(self.k90(le)*sin(alpha)**2+cos(alpha)**2)

	def k90(self,le):
		'''coefficiente per calcolo fh0k'''
		if 'c' in le.essenza or 'gl' in le.essenza:
			return 1.35+.015*self.d
		elif 'd' in le.essenza:
			.9+.015*self.d
		else:
			return 1.30+.015*self.d

	@property
	def Myrk(self):
		''' momento ultimo resistente del bullone'''
		zb=1.8*self.d**-.4
		return zb*self.mat.fuk*self.d**3/6
	
	def nef(self,a1,n=1,alpha=0,*args):
		'angoli in radianti'
		ne=min([n,n**.9*(a1/13/self.d)**(1./4)])/n
		return ne+(n-ne)*alpha/(pi/2)

	def Faxrk(self,le,*args):	
		f=[pi*self.d**2/4*self.mat.fyk,
			(self.rond**2-self.d**2)*pi/4*3*le.fc90k]
		return min(f),.25
		
class spinotto(connettore,bullone):
	def passi(self,alpha=0):
		'''calcola le spaziature minime per gli spinotti
alpha in rad'''
                a1=(3+2*abs(cos(alpha)))*self.bul.d
		a2=3*self.bul.d
                a3f=max([7*self.bul.d,80.])
                if 270< rad2deg(alpha) or rad2deg(alpha)<90:
                        a3c=0.
		elif 90<=rad2deg(alpha)<=150:
			a3c=max([(a3f*sin(alpha)), 3*self.bul.d])
		elif 150<=rad2deg(alpha)<=210:
			a3c=3*self.bul.d
		else:
			a3c=max([(a3f*abs(sin(alpha))), 3*self.bul.d])
		if 0<rad2deg(alpha)<180:
			a4f=max([(2+2*sin(alpha))*self.bul.d, 3*self.bul.d])
		else:
                        a4f=max([(2-2*sin(alpha))*self.bul.d, 3*self.bul.d])
		a4c=3*self.bul.d
		out=locals()
		del out['self']
		return out

	def Faxrk(self,*args):
		return 0,0


class vite(connettore,spinotto):#per ora considero solo vidi di diametro gambo > 6 mm
	#in mancanza di dati certi la filettatura interna ha diametro pari a d_ext/1.4
	def __init__(self,mat,d,d_ext,d_int,li,lt,alpha=0):
		'''mat=materiale
d=diametro gambo
d_ext=diametro esterno [con 0 lo setta u d]
d_int=diametro interno [con 0 lo setta a dxet/1.4]
li: lunghezza infissione
lt: lunghezza totale
alpha=angolo infissione rispetto fibre rad'''
		vars(self).update(locals())
		self.lef=self.li-d #tolgo un diametro alla lunghezza efficace
		if not self.d_int:
			self.d_int=self.d_ext/1.4 #valore medio dei passi
		self.d_eff=min([self.d_int*1.1,self.d])

	def faxk(self,le):
		'''resistenza unitaria ad estrazione
le= legno in cui è infissa la vite'''
		return 3.6e-3*le.rok**1.5

	def faxak(self,le):
		'''resistenza unitaria inclinata ad estrazione
le=legno in cui è infissa la vite		'''
		return self.faxk(le)/(sin(self.alpha)**2+1.5*cos(self.alpha)**2)
	
	def Faxak(self,le,*args):
		'''resistenza ad estrazione della vite
le=legno in cui è infissa la vite'''
		return (pi*self.d_ext*self.lef)**.8*self.faxak(le)
	
	def Faxrk(self,le,*args):
		'''per congruenza con gli altri connettori'''
		return self.Faxak(le,*args),1

	def neff_ax(self,n):
		return n**.9/n


class chiodo(connettore): #da completare
	def __init__(self,mat,d,sez='circ',perforatura=False):
		vars(self).update(locals())

	@property
	def Myrk(self):
		wpl={'circ':self.d**3/6 , 'quad':self.d**3/4}[self.sez]
		xi=1.8*self.d**-.4
		return self.mat.fuk*xi*wpl
	
	def fh0k(self,le):
		if self.d<8:
			if self.preforatura:
				return .082*(1-.01*self.d)*le.rok
			else:
				return .082*le.rok*self.d**-.3
		else:
			return bullone.fh0k(self,le)
	
	def fhak(self,le,alpha=0): ##### per ora mutuo quella del bullone
		return bullone.fhak(self,le,alpha)

	def nef(self,a1,*args):
		r=a1/self.d
		if r>=14:
			nef=1
		elif 10<=r<=14:
			nef=.85
		elif 7<=r<10:
			nef=.7
		elif 4<=r<7:
			if self.preforatura:
				nef=.5
			else:	
				nef=0
		else:
			nef=0
		return nef	

	def passi_assali(self):
		#per viti caricate assialmente
		a1=4*self.d
		a2=4*self.d
		a3f=4*self.d
		a3c=2.5*self.d
		a4f=2.5*self.d
		a4c=4*self.d
	
	def passi(self,alpha=0):
		#per i passi delle viti caricate a taglio uso quelli degli spinotti o dei chiodi( che nn ho ancora definito):
		return spinotti.passi(self,alpha)

class cambretta(connettore):
	def Faxk(self,*args):
		return 0,0

###############  Fine sezione dei connettori  #############

class geom_bull:
	pass

class elemento:
	def __init__(self,mat,h,t,fibra=r_[1,0,0]):
		'''elemento ligneo
mat=materiale
h=altezza
t=profondita\'
fibra=vettore direzione fibra'''
		vars(self).update(locals())	

	def fh0k(self,con):
		return con.fh0k(self.mat)
	
	def fhak(self,con,alpha=0):
		return con.fhak(self.mat,alpha)

class piastra: #per ora la scrivo, di fatto Ã¨ meglio se importo la piastra da qualcosa di simile fatto per l'acciaio...
	def __init__(self,mat,t,offset=1):
		vars(self).update(locals())

	def sottile(self,con):
		sot=False
		sot=self.offset>.1*con.d
		if not sot:
			sot=self.t<=.5*con.d
		return sot

	def spessa(self,con):
		sp=False 
		if not self.sottile(con):
			sp=self.t>=con.d
		return sp

	def perc_spes(self,con):
		#calcolo in che percentuale può essere considerata spesa la piastra
		return self.t/con.d

#####################  FINE SEZIONE ELEMENTI   ###########################		

class giunto:#	gli passo elementi, connettori e geometria conlonatura e lui calcola...
	def __init__(self,elementi,con,geom=None,tipo=None,npt=None):
		'''elementi=lista con successione elementi utilizzati nel giunto
con=connettore utilizzato
geom=geometria della bullonatura
tipo=successione degli elementi
npt= numero piani di taglio

NB: i calcoli sono condotte considerando elementi omogenei (stessi spessori e stessi materiali per piastre ed elementi)'''

		vars(self).update(locals())
		if not npt: #conto i piani di taglio
			self.npt=len(elementi)-1


	def Fvrk(self,alpha=0):
		F=[]
		for p in range(self.npt): #per ogni piano di taglio
			#isolo elementi piano di taglio dx e sx...
			elem=self.isola_piano(p)
			#calcolo entrambe le possibilità
			f=[self.calcola_piano(e,alpha) for e in elem]
			f=[i for i in f if i] #escludo i valori nulli
			F.append(min(f))
		return sum([i[0] for i in F]),F

			#valuto resistenza connessione

	def isola_piano(self,n):
		if n==0:
			elem_sx=None
			elem_dx=self.elementi[n:n+3]
		elif n==self.npt-1:
			elem_sx=self.elementi[-3:]
			elem_dx=None
		else:
			elem_sx=[self.elementi[n-1:n+2]]
			elem_dx=[self.elementi[n:n+3]]
		return elem_sx,elem_dx

	def calcola_piano(self,elem,alpha=0):	
		if elem is None: return None 
		seq=[i.__class__.__name__ for i in elem]
		if seq==['elemento','elemento']:#per ora i giunti legno legno non li ho ancora implementati
			pass
		elif seq==['elemento','elemento','elemento']:#per ora i giunti legno legno non li ho ancora implementati
			pass
		#da qui iniziano i collegamenti legno acciaio 	
		elif seq==['elemento','piastra'] or seq== ['piastra','elemento']	:
			el=[i for i in elem if isinstance(i,elemento)][0]
			p=[i for i in elem if isinstance(i,piastra)][0]
			if p.spessa(self.con):
				return self.lasp(el,alpha)
			elif p.sottile(self.con):
				return self.laso(el,alpha)
			else: #interpolo linearmente
				so=self.laso(el,alpha)
				sp=self.lasp(el,alpha)
				m=so[0]+(sp[0]-so[0])*p.perc_spes(self.con)
				return m,[so[1],sp[1]]

		elif seq==['elemento','piastra','elemento']:
		#considero gli elementi di spessore uguale
			el=[i for i in elem if isinstance(i,elemento)][0]
			p=[i for i in elem if isinstance(i,piastra)][0]
			return self.lal(el,alpha)

		elif seq==['piastra','elemento','piastra']:
			el=[i for i in elem if isinstance(i,elemento)][0]
			p=[i for i in elem if isinstance(i,piastra)][0]
			if p.spessa(self.con):
				return self.alasp(el,alpha)
			elif p.sottile(self.con):
				return self.alaso(el,alpha)
			else: #interpolo linearmente
				so=self.alaso(el,alpha)
				sp=self.alasp(el,alpha)
				m=so[0]+(sp[0]-so[0])*p.perc_spes(self.con)
				return m,[so[1],sp[1]]
		else:
			raise IOError('sequenza di elementi sconosciuta')

		
	#####################  MECCANISMI DI COLLASSO   ####################### 
	def laso(self,elem,alpha=0):
		'''resistenza un piano di taglio lamiera sottile'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha)
		F=[[.4*fhak*t*self.con.d,'a'],
		   [1.15*(2*self.con.Myrk*fhak*self.con.d)**.5 ,'b']]
		#calcolo massimi effetti tiranti   
		T=[self.con.Faxrk(elem.mat,alpha)[0]*self.con.Faxrk(elem.mat,alpha)[1] for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i
		return min(F)

	def lasp(self,elem,alpha=0): #nota bene... ora fhak Ã¨ una proprietÃ  del connettore visto che la sua formulazione varia con esso,per logica (resistenza a rifollamento del legno) posso aggiungere un metodo simile all'elemento ligneo che richiama quella del connettore...)
		'''resistenza un piano di taglio lamiera spessa'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha)
		F=[[fhak*t*self.con.d,'c'],
		   [fhak*t*self.con.d*((2+4*self.con.Myrk/(fhak*self.con.d*t**2))**.5-1),'d'],
		   [2.3*sqrt(self.con.Myrk*fhak*self.con.d),'e']]
		#calcolo massimi effetti tiranti   
		T=[self.con.Faxrk(elem.mat,alpha)[0]*self.con.Faxrk(elem.mat,alpha)[1] for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i

		return min(F)

	def lal(self,elem,alpha=0):
		'''resistenza 2 piani di taglio lamiera centrata qualsiasi spessore'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha)
		F=[[fhak*elem.t*self.con.d,'f'],
		   [fhak*elem.t*self.con.d*((2+4*self.con.Myrk/(fhak*self.con.d*elem.t**2))**.5-1),'g'],
		   [2.3*sqrt(self.con.Myrk*fhak*self.con.d),'h']]
		#calcolo massimi effetti tiranti   
		T=[self.con.Faxrk(elem.mat,alpha)[0]*self.con.Faxrk(elem.mat,alpha)[1] for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i
		return min(F)

	def alaso(self,elem,alpha=0):
		'''resistenza lamiere esterne sottili'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha)
		F=[[.5*fhak*t*self.con.d,'j'],
		   [1.15*sqrt(self.con.Myrk*fhak*self.con.d),'k']]
		#calcolo massimi effetti tiranti   
		
		T=[self.con.Faxrk(elem.mat,alpha)[0]*self.con.Faxrk(elem.mat,alpha)[1] for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i
		return min(F)

	def alasp(self,elem,alpha=0):
		'''resistenza lamiere esterne spesse'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha)
		F=[[.5*fhak*t*self.con.d,'l'],
		   [2.3*sqrt(self.con.Myrk*fhak*self.con.d),'m']]
		#calcolo massimi effetti tiranti   
		T=[self.con.Faxrk(elem.mat,alpha)[0]*self.con.Faxrk(elem.mat,alpha)[1] for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i
		return min(F)


