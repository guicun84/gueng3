#-*-coding:utf-8-*-

from scipy import *

class LegnoAcciaio:
	'''calcola resistenza giunto acciaio-legno con bulloni e spinotti se rond<0'''
	def __init__(self,le,t1,h,bul,t,tipo,rond,alpha=0,offset=1.,n=0,m=0,he=0,hm=0,lr=0,l1=0,a1=0,):
		'''le=legno
t1=spessore piano legno
h=altazza sezione
bul= bullone usato
t=spessore lamiera
tipo= successione di l,a per indicare legno e acciaio:
	per esempio piastre esterne: 'ala'
rond= diametro esterno rondella: se negativo considera come spinotto
offset= tolleranza foro acciaio bullone
n=righe contate T fibre
m=colonne contate // fibre
he=distanza max bordo solloecitato bullone
hm=altezza bullonatura misurata T alle fibre
lr=larghezza bullonatura misurata // fibra
l1=distanza serie collegamenti // fibra
a1=interasse bulloni // fibra

le dimensioni delle bullonature sono riferite agli assi 
[possono essere quindi 0 con una sola fila di bulloni]
	'''
		vars(self).update(locals())
		self.sottile=offset>.1*bul.d
		if not self.sottile:
			self.sottile=t<=.5*bul.d
			self.spessa=t>=bul.d
		else:
			self.spessa=False
		#coefficiente massimo effetto tirante: 
		self.max_tir=.25 #questo e' il valore per un bullone...

	def geom_bull(self,n,m,he,hm,lr,l1,a1):
		'''serve per caricare in una seconda fase le dimensioni della bullonatura
n=righe contate T fibre
m=colonne contate // fibre
he=distanza max bordo solloecitato bullone
hm=altezza bullonatura misurata T alle fibre
lr=larghezza bullonatura misurata // fibra
l1=distanza serie collegamenti // fibra
a1=interasse bulloni // fibra'''
		vars(self).update(locals())	

	@property
	def fh0k(self):
		'''resistenza caratteristica a rifollamento per alpha=0'''
		return .082*(1-.01*self.bul.d)*self.le.rok

	@property
	def fhak(self):
		'''resistenza caratteristica a rifollamento per alpha!=0'''
		return self.fh0k/(self.k90*sin(self.alpha*pi/180)**2+cos(self.alpha*pi/180)**2)

	@property
	def k90(self):
		'''coefficiente per calcolo fh0k'''
		es=self.le.essenza
		if 'c' in es or 'gl' in es:
			return 1.35+.015*self.bul.d
		elif 'd' in es:
			.9+.015*self.bul.d
		else:
			return 1.30+.015*self.bul.d

	@property
	def Myrk(self):
		''' momento ultimo resistente del bullone'''
		zb=1.8*self.bul.d**-.4
		return zb*self.bul.ft*self.bul.d**3/6

	@property
	def Faxrk(self):
		#res a traz bullone
                if self.rond<0:
                        return [0,'0']
		bul=self.bul
		if self.rond>0:
			A_rond=pi*(self.rond-bul.d-self.offset)**2/4
		elif self.rond==0:
			rond=min([12*self.t,4*bul.d])
			A_rond=pi*(rond-bul.d-self.offset)**2/4  
		r=[[bul.An*bul.fy,'a'],#res bullone
		[3*self.le.fc90k*A_rond,'b']] ###resistenza legno
		return min(r)
			

	def laso(self):
		'''resistenza un piano di taglio lamiera sottile'''
		F=[[.4*self.fhak*self.t1*self.bul.d,'a'],
		   [1.15*(2*self.Myrk*self.fhak*self.bul.d)**.5 ,'b']]
		#calcolo massimi effetti tiranti   
		T=[min([self.max_tir*i[0], self.Faxrk[0]/4]) for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i
		return min(F)

	def lasp(self):
		'''resistenza un piano di taglio lamiera spessa'''
		F=[[self.fhak*self.t1*self.bul.d,'c'],
		   [self.fhak*self.t1*self.bul.d*((2+4*self.Myrk/(self.fhak*self.bul.d*self.t1**2))**.5-1),'d'],
		   [2.3*sqrt(self.Myrk*self.fhak*self.bul.d),'e']]
		#calcolo massimi effetti tiranti   
		T=[min([self.max_tir*i[0], self.Faxrk[0]/4]) for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i

		return min(F)

	def lal(self):
		'''resistenza 2 piani di taglio lamiera centrata qualsiasi spessore'''
		F=[[self.fhak*self.t1*self.bul.d,'f'],
		   [self.fhak*self.t1*self.bul.d*((2+4*self.Myrk/(self.fhak*self.bul.d*self.t1**2))**.5-1),'g'],
		   [2.3*sqrt(self.Myrk*self.fhak*self.bul.d),'h']]
		#calcolo massimi effetti tiranti   
		T=[min([self.max_tir*i[0], self.Faxrk[0]/4]) for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i
		return min(F)

	def alaso(self):
		'''resistenza lamiere esterne sottili'''
		F=[[.5*self.fhak*self.t1*self.bul.d,'j'],
		   [1.15*sqrt(self.Myrk*self.fhak*self.bul.d),'k']]
		#calcolo massimi effetti tiranti   
		T=[min([self.max_tir*i[0], self.Faxrk[0]/4]) for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i
		return min(F)

	def alasp(self):
		'''resistenza lamiere esterne spesse'''
		F=[[.5*self.fhak*self.t1*self.bul.d,'l'],
		   [2.3*sqrt(self.Myrk*self.fhak*self.bul.d),'m']]
		#calcolo massimi effetti tiranti   
		T=[min([self.max_tir*i[0], self.Faxrk[0]/4]) for i in F]
		#escludo alcuni effetti tirante
		T[0]=0
		for n,i in enumerate(T):
			F[n][0]+=i
		return min(F)


	def Fvrk_(self):
		'''calcolo della resistenza // della giunzione in base alla tipologia'''
		if self.tipo in ['al','la']:
		   Fso=self.laso()
		   Fsp=self.lasp()

		elif self.tipo =='lal':
		   Fso=self.lal()
		   Fsp=Fso

		elif self.tipo=='ala':
		   Fso=self.alaso()
		   Fsp=self.alasp()

		else:
		   return None

		if self.spessa:
		   return Fsp

		if not self.spessa and not self.sottile:
                        '''eseguo interpolazione tra sottile e spesso'''
			F =Fso[0]+(Fsp[0]-Fso[0])/self.bul.d*self.t
			t=[Fso[1],Fsp[1]]
			return F,t

		if self.sottile:
		   return Fso

	@property
	def Fvrk(self):
		return self.Fvrk_()[0]*self.nef*self.n

	def passi(self):
                if self.rond>=0:
                        return self.passi_bul()
                else:
                        return self.passi_spin()

	def passi_bul(self):
                '''calcola i passi minimi per bullonature'''
		a1=(4+3*abs(cos(self.alpha*pi/180)))*self.bul.d
		a2=4*self.bul.d
                a3f=max([7*self.bul.d,80.])
                if 270< self.alpha or self.alpha<90:
                        a3c=0.
		elif 90<=self.alpha<=150:
			a3c=max([(1+6*sin(self.alpha*pi/180))*self.bul.d , 4*self.bul.d])
		elif 150<=self.alpha<=210:
			a3c=4*self.bul.d
		else:
			a3c=max([(1+6*abs(sin(self.alpha*pi/180)))*self.bul.d , 4*self.bul.d])

		if 0<self.alpha<180:
			a4f=max([(2+2*sin(self.alpha*pi/180))*self.bul.d, 3*self.bul.d])
		else:
                        a4f=max([(2-2*sin(self.alpha*pi/180))*self.bul.d, 3*self.bul.d])
		a4c=3*self.bul.d
		out=locals()
		del out['self']
		return out

	def passi_spin(self):
                '''calcola le spaziature minime per gli spinotti'''
                a1=(3+2*abs(cos(self.alpha*pi/180)))*self.bul.d
		a2=3*self.bul.d
                a3f=max([7*self.bul.d,80.])
                if 270< self.alpha or self.alpha<90:
                        a3c=0.
		elif 90<=self.alpha<=150:
			a3c=max([(a3f*sin(self.alpha*pi/180)), 3*self.bul.d])
		elif 150<=self.alpha<=210:
			a3c=3*self.bul.d
		else:
			a3c=max([(a3f*abs(sin(self.alpha*pi/180))), 3*self.bul.d])

		if 0<self.alpha<180:
			a4f=max([(2+2*sin(self.alpha*pi/180))*self.bul.d, 3*self.bul.d])
		else:
                        a4f=max([(2-2*sin(self.alpha*pi/180))*self.bul.d, 3*self.bul.d])
		a4c=3*self.bul.d
		out=locals()
		del out['self']
		return out

	def check_passi(self):
		p_min=self.passi()
		check={}
		for i in list(p_min.keys()):
			if hasattr(self,i):
				print(i)
				v=vars(self)[i]
				check[i]=[v>p_min[i],v,p_min[i]]
		return check

	@property
	def nef(self):
                if self.a1:
                        return min([self.m,self.m**.9*(self.a1/13/self.bul.d)**.25])
                else:
                        return self.m

	def F90rk_(self):
		'''resistenza a spacco della giunzione
h: altezza sezione
he: distanza max bordo caricato-bullone T fibre
n: righe di bullonatura (contate T fibre)
lr: larghezza bullonatura (// fibre)
l1: distanza tra serie di giunti
N.B
la verifica e' soddisfatta se Fvd<=0.5 F90rd'''
		fw=min([1+.75*(self.lr+self.l1)/self.h , 2])
		fr=1+1.75*(self.n*self.hm/1000)/(1+self.n*self.hm/1000) #ho considerato solo i bulloni per ora
		#N.B: nonostante la formula cominci con 2* questa è relativa ad un elemento di spessore t non 2*t!!!!
		F=2*self.t*9*(self.he/(1-(self.he/self.h)**3))**.5*fw*fr 
		out=locals()
		del out['self']
		return F,out

	@property
        def F90rk(self):
                if self.tipo =='lal':
                        n=2
                else:
                        n=1
                return self.F90rk_()[0]*n
	

class Vite:
	outodt=False
	def __init__(self,mat,d_g,d_ext,d_int,li,lt,le,alpha=0):
		'''mat=materiale
d_ext=diametro esterno
d_int=diametro interno
d_g=diametro gambo
li: lunghezza infissione
lt: lunghezza totale
le= legno in cui è infissa
alpha=angolo infissione rispetto fibre deg'''
		vars(self).update(locals())
		d=max([self.d_g,self.d_int])
		self.lef=self.li-d #tolgo un diametro alla lunghezza efficace
		if not self.d_int:
			self.d_int=self.d_ext/1.4 #valore medio dei passi
		self.d_eff=min([self.d_int*1.1,self.d_g])

	@property
	def d(self):
		return self.d_eff

	@property
	def ft(self):
		return self.mat.ftk
	
	@property
	def fy(self):
		return self.mat.fyk



	@property
	def An(self):
		return self.d_int**2*pi/4
	@property
	def faxk(self):
		return 3.6e-3*self.le.rok**1.5
	@property
	def faxak(self):
		alpha=deg2rad(self.alpha)
		return self.faxk/(sin(alpha)**2+1.5*cos(alpha)**2)
	@property
	def Faxak(self):
		return (pi*self.d_ext*self.lef)**.8*self.faxak
	@property
	def Faxad(self):	
		return self.Faxak/self.le.gamma_m

	@property	
	def N_rk(self):
		return self.mat.fyk*pi*self.d_int**2/4
	
	@property
	def N_rd(self):
		return self.N_rk/self.mat.gamma
		
	def passi(self):
		a1=4*self.d
		a2=a1
		a3f=2.5*d
		a3c=a3f
		a4f=a1
		a4c=a1
		out=locals()
		del out['self']
		return out

	def __str__(self):
		st='''Diametro gambo <#d_g#>={0.d_g:.4g} mm
Diametro esterno <#d_ext#>={0.d_ext:.4g} mm
Diametro interno <#d_int#>={0.d_int:.4g} mm
Diametro efficace <#d_eff#>={0.d_eff:.4g} mm
Lunghezza filettatura <#l_i#>= {0.li:.4g} mm
Lunghezza totale <#l_tot#>= {0.lt:.4g} mm
Lunghezza efficace <#l_eff#>= {0.lef:.4g} mm
Angolo rispetto alle fibre <#%alpha#>= {0.alpha:.4g} deg
Resistenza all'estrazione <#F_axak#> {0.Faxak:.4g} N --> <#F_axad#>= {0.Faxad:.4g} N'''.format(self)
		if not self.outodt:
			st=st.replace('<#','')
			st=st.replace('#>','')
		return st
			  



