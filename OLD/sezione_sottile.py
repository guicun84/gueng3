#!/usr/bin/env python
#-*-coding:utf-8-*-

from  pylab import *
from scipy import *
from copy import deepcopy as copy
from scipy.linalg import norm,solve
from scipy.optimize import leastsq
ion()


class linea:
	Mrot=c_[[0.,-1],[1.,0]].T
	def __init__(self,i,j):
		vars(self).update(locals())
		self.orienta()

	def orienta(self):
		self.e1=self.j-self.i
		self.l=norm(self.e1)
		self.e1/=norm(self.l)
		self.e2=dot(self.Mrot,self.e1)

	def inverti(self):
		p=self.i
		self.i=self.j
		self.j=p
		self.orienta()
	
	def asse(self,x):
		return self.i+self.e1*x


	def interseca(self,l):
		'''l1.i+l1.e1*x - (l2.i+l2.e2*y)={0,0}
		[[e1][e2]] * [x,y] +(l1i-l2.i)=[0,0]'''
		A=c_[self.e1,l.e1]
		b=self.i-l.i
		try:
			v=solve(A,-b)
		except LinAlgError:
			return None
		p=self.i+self.e1*v[0]
		return p
	@property
	def Sx(self):
		#calcola momento statico dell'area definita dal segmento rispetto ad asse x
		v=self.j-self.i
		s1=v[0]*self.i[1]*self.i[1]*.5
		s2=v[0]*v[1]*.5* (self.i[1]+1./3*v[1])
		return -s1-s2

	@property
        def Ix(self):
                v=self.j-self.i
		s1=1./12*v[0]*self.i[1]**3 + v[0]*self.i[1]*(self.i[1]*.5)**2
		s2=1./36*v[0]*v[1]**3+ .5*v[0]*v[1]*(self.i[1]+1./3*v[1])**2
		return -s1-s2

	@property
	def A(self):
		v=self.j-self.i
		a1=v[0]*self.i[1]
		a2=.5*v[0]*v[1]
		return -a1-a2                                
	
	def plot(self,form=None):
		x=[self.i[0],self.j[0]]
		y=[self.i[1],self.j[1]]
		if form:
			plot(x,y,form)
		else:
			plot(x,y)
		axis('equal')
	def plot_verso(self,form='r'):
                p1=self.asse(self.l/2)
                p2=p1+self.l/10*(-self.e1+self.e2)
                x=[i[0] for i in [p1,p2]]
                y=[i[1] for i in [p1,p2]]
                plot(x,y,form)
                axis('equal')
			

class sezione:
        _version_='1.8'
	def __init__(self,lati,centra=False):
		#centra serve per ricentrare le coordinate sul baricentro
		self.lati=lati
		self.centra=centra
		if self.centra:
			self.sposta_punti()

	def centra(self):
                c=self.sposta_punti(self.G)
                vars(self).update(vars(c))

                

	def sposta_punti(self,G):
                sez=copy(self)
		for l in sez.lati:
			l.i-=G
			l.j-=G
			l.orienta()
		return sez	

	def ruota(self,ang):
                mc=copy(self)
                m=array([[cos(ang),sin(ang)],[-sin(ang),cos(ang)]]).T
                for l in mc.lati:
                        l.i=dot(m,l.i)
                        l.j=dot(m,l.j)
                        l.orienta()
                return mc
        
        def b(self,y=0):
                if not self.limiti[2]<y<self.limiti[3]:
                        return 0
                '''restituisce lo spessre totale delle varie sezioni ad una data quota'''
                lim=self.limiti
                l=linea(r_[lim[0]-1,y],r_[lim[1]+1,y])
                intersec=[]
                for i in self.lati:
                        p=l.interseca(i)
                        if p is not None:
                                v=dot((p-i.i),i.e1)
#                                print v,i.l
                                if 0<=v<=i.l:
#                                        plot(p[0],p[1],'xr')
                                        intersec.append(p[0])                                        
                #trovati i punti di intersezione:
                intersec=[i for i in set(intersec)]
                intersec.sort()
#                print intersec
                i=None
                s=0
                for p in intersec:
                        if i is None:
                                i=p
                        else:
                                s+=(p-i)
                                i=None
                return s

        def Ibs(self,y='min'):
                '''calcola il valore di (I s)/Sx* per la valutazione delle tensioni di taglio'''
                if y =='min':
                        ymin=leastsq(self.Ibs,0.)[0][0]
                        return self.Ibs(ymin)
                else:
                        return -self.Ix*self.b(y)/self.Sx_(y)
        
	@property
	def Sx(self):
		s=0
		for l in self.lati:
			s+=l.Sx
		return s

	def Sx_(self,y,parte=1):
                l=self.limiti
                if l[2]<y<l[3]:
                        l=linea(r_[0.,y],r_[1.,y])
                        ts=sezione(self.taglia(l,parte))
                        return ts.Sx
                else:
                        return 0.
        @property   
        def Ix(self):
                I=0.
                for i in self.lati:
                        I+=i.Ix
                return I

	@property
	def Sy(self):
		sez=self.ruota(pi/2)
		return sez.Sx
	
        @property
	def Iy(self):
                sez=self.ruota(pi/2)
                return sez.Ix

        @property
        def Wx(self):
                #elastico
                l=self.limiti
                y=r_[l[2],l[3]]
                return min(abs(self.Ix/y))
        
        @property
        def Wy(self):
                sez=self.ruota(-pi/2)
                return sez.Wx

        def Wxp(self,y=0.):
                lim=self.limiti
                if lim[2]<y<lim[3]:
                        l=linea(r_[0.,y],r_[1.,y])
                        s1=sezione(self.taglia(l,-1))
                        s2=sezione(self.taglia(l,1))
                        return s1.Sx-s2.Sx
                else:
                        return 0.
        
        def Wyp(self,x=0.):
                sez=self.ruota(-pi/2)
                return sez.Wxp(x)

        def Apl(self,y=0):
                lim=self.limiti
                if y<=lim[2]: return self.A
                elif y>=lim[3]: return -self.A
                l=linea(r_[0.,y],r_[1.,y])
                s1=sezione(self.taglia(l,-1))
                s2=sezione(self.taglia(l,1))
                return s1.A-s2.A

        def y0p(self,Apl=0):
                def minimizzami(yp,Apl):
                        return self.Apl(yp)-Apl
                yop=leastsq(minimizzami,0,args=(Apl,))[0][0]
                return yop
                
                
	@property
	def A(self):
		a=0
		for l in self.lati:
			a+=l.A
		return a

	@property
	def G(self):
		return r_[self.Sy,self.Sx]/self.A

	@property
	def limiti(self):
                x=[]
                y=[]
                for l in self.lati:
                    x.extend([l.i[0],l.j[0]])
                    y.extend([l.i[1],l.j[1]])
                xm=min(x)
                xM=max(x)
                ym=min(y)
                yM=max(y)
                return xm,xM,ym,yM

        @property
        def ptp(self):
                l=self.limiti
                return l[1]-l[0],l[3]-l[2]

	def taglia(self,asse,parte=1):
        #parte= verso positivo o negativo dell'asse dato su cui considerare la sezione tagliata
                lati_out=[]
                last_int=None
#                count=0
                for lato in self.lati:
#                        count+=1
#                       print '\n',count
                        p=asse.interseca(lato)
                        if p is not None: #se ho punto di intersezione
##                                plot(p[0],p[1],'xr')
                                #controllo se taglio il lato
                                v=p-lato.i
                                l=dot(v,lato.e1)
#                                print 'v,l,p',v,l,p
#                                print 'lato.l',lato.l
				if 0<=l<lato.l: #taglio il lato:
#                                       print 'taglio lato'
                                        c=dot(v,asse.e2)
#                                       print 'test',c
                                        if c*parte>0:
#                                                print 'inizio-taglio'
                                                p1=lato.i.copy()
                                                p2=p
                                        else:
#                                                print 'taglio-fine'
                                                p1=p
                                                p2=lato.j.copy()
                                        if last_int is not None and  all(p1==p):
                                                lati_out.append(linea(last_int,p1))
                                                last_int=None
                                        else:
                                                last_int=p
                                        lati_out.append(linea(p1,p2))
                                        continue
                        #controllo se il lato è dalla parte corretta                                    
                        v=lato.i-asse.i
                        c=dot(v,asse.e2)*parte
                        if c<0:
#                               print 'lato compreso nel contorno'
                                p1=lato.i.copy()
                                p2=lato.j.copy()
                                last_int=None
                        else:
#                                print 'lato escluso dal contorno'
                                continue
                        lati_out.append(linea(p1,p2))
                #controllo chiusura lati:
                ini=lati_out[0].i
                fin=lati_out[-1].j
                if not all(ini==fin):
                        l=linea(fin,ini)
                        lati_out.append(l)
                return lati_out
   
	def plot(self,form='-b'):
		for i in self.lati:
			i.plot(form)
        def plot_versi(self,form='-r'):
                for i in self.lati:
                        i.plot_verso(form)
	
		
class sezione_sottile(sezione):
	def __init__(self,s,asse,close=False):
                self.s=s
                self.close=close
		self.asse=[]
		for i in range(len(asse)-1):
			self.asse.append(linea(asse[i].copy(),asse[i+1].copy()))
		if close:
			self.asse.append(linea(asse[-1].copy(),asse[0].copy()))
		self.solidifica()

	def sposta_punti(self,G):
                sez=copy(self)
                for p in sez.asse:
                        p.i-=G
                        p.j-=G
                sez2=sezione.sposta_punti(self,G)
                sez2.asse=sez.asse
                return sez2

	def offset(self,of):	
		lati=[]
		l1=None
		for a2 in self.asse:
			p1=a2.i+a2.e2*of
			p2=a2.j+a2.e2*of
			l2=linea(p1.copy(),p2.copy())
			if l1 is None:
				l1=l2
				continue
			p=l1.interseca(l2)
			l1.j=p.copy()
			l2.i=p.copy()
			lati.append(l1)
			l1=l2
		lati.append(l1)	
		return lati

	def solidifica(self):
		self.lati=[]
		l1=self.offset(-.5*self.s)
		l2=self.offset(+.5*self.s)
		l2=l2[-1::-1]
		for i in l2:
			i.inverti()
		self.lati=l1
		p1=self.lati[-1].j.copy()
		p2=l2[0].i.copy()
		l=linea(p1,p2)
		self.lati.append(l)
		self.lati.extend(l2)
		p1=self.lati[-1].j.copy()
		p2=self.lati[0].i.copy()
		l=linea(p1,p2)
		self.lati.append(l)
		for l in self.lati:
                        l.orienta()

	def plot_asse(self,form='-.b'):
		for i in self.asse:
			i.plot(form)
			
if __name__=='__main__':
        p=[r_[0,0.],r_[0,1.],r_[1.,1],r_[1,0]]
        #p=[r_[0,0.],r_[1,1.],r_[2,0.]]
        #p=[r_[0,0.],r_[0,1.]]
        s=.2

        se=sezione_sottile(s,p)
        se.centra()
        l=linea(r_[0,0.],r_[1.,0])
        se.plot()
        axis('equal')
        grid()
        c=sezione(se.taglia(l,-1))
        c.plot('x-r')
        print(len(c.lati))

        l=se.limiti
        y=linspace(l[2],l[3],100)
        S=[se.Sx_(i) for i in y]
        figure()
        plot(S,y)
        grid()
