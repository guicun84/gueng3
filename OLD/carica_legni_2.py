import pandas as pa
head='descrizione grandezza castagno quercia pioppo latifoglie'.split()
#pioppo e ontano sono insieme

st='''Flessione (5-percentile), MPa	fmk	28	42	26	27
Trazione parallela alla fibratura (5-percentile), MPa	ft0k	17	25	16	16
Trazione perpendicolare alla fibratura (5-percentile), MPa	ft90k	0.5	0.8	0.4	0.5
Compressione parallela alla fibratura (5-percentile), MPa	fc0k	22	27	22	22
Compressione perpendi-colare alla fibratura (5-percentile), MPa	fc90k	3.8	5.7	3.2	3.9
Taglio (5-percentile), MPa	fvk	2.0	4.0	2.7	2.0
Modulo di elasticità parallelo alla fibratura (medio), MPa (x 103)	E0m	11	12	8	11.5
Modulo di elasticità parallelo alla fibratura (5-percentile), MPa (x 103)	E005	8	10.1	6.7	8.4
Modulo di elasticità perpen dicolare alla fibratura -(medio), MPa (x 102)	E90m	7.3	800	5.3	7.7
Modulo di taglio (medio), MPa (x 102)	Gm	9.5	750	5	7.2
Massa volumica (5-percentile), kg/m3	rok	465	760	420	515
Massa volumica (media), kg/m3	rom	550	825	460	560'''
from io import StringIO
st_=StringIO()
st_.write(st)
st_.seek(0)
dat=pa.read_csv(st_,sep='\t',names=head)
print(dat)
dat_=dat.copy()
del dat_['descrizione']
dat_=dat.T
dat_=dat_.drop('descrizione')
dat_=dat_.rename_axis(dict(list(zip(list(range(12)),dat_.iloc[0]))),1)
dat_=dat_.drop('grandezza')
dat_=dat_.reset_index()
dat_=dat_.rename_axis({'index':'tipo'},1)
print(dat_)
#copio il pioppo come ontano
pi=dat_[dat_.tipo=='pioppo'].iloc[0]
on=pi.copy()
on.tipo='ontano'
dat_=dat_.append(on)
dat_.insert(1,'classe','S')
#correggo i moduli elastici
for i in 'E0m E005 E90m Gm'.split():
	dat_[i]=dat_[i]*1e3

import os
db=os.path.join('..','legni.sqlite')
import sqlite3
db=sqlite3.connect(db)
q=db.execute('select * from legni')
print(q.description)
print(q.fetchall())
#dat_.to_sql('legni',db,if_exists='append',index=False)

dat__= pa.read_sql('select * from legni',db)
print(dat__)
