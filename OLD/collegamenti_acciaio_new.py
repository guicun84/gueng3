﻿
from scipy import *

class bulloni:
	An={12:84.,
		14:115.,
		16:157.,
		18:192.,
		20:245.,
		22:303.,
		24:353.,
		27:459.,
		30:561.}


class acciai:
	fy={4.6:240.,
		5.5:300.,
		6.6:360.,
		8.8:640.,
		10.9:900.}
	
	ft={4.6:400.,
		5.6:500.,
		6.6:600.,
		8.8:800.,
		10.9:1000.}

class bullone:
	def __init__(self,cl,d):
		self.classe=cl
		self.d=d
		self.fy=acciai.fy[self.classe]
		self.ft=acciai.ft[self.classe]
		self.An=bulloni.An[self.d]

	@property
	def F_vrd(self):
		if self.classe<=8.8:
			#gammam=1.25
			return .6*self.fy*self.An/1.25
		else:
			return .5*self.fy*self.An/1.25 #controllare questo passaggio: gamma dovrebbe essere maggiore....

class lamiera:
	__version__='1.1'
	'''secondo DM 14/1/2008'''
	gamma_m2=1.25
	def __init__(self,mat,t,e1=None,e2=None,p1=None,p2=None):
		'''in input
fyk=res a rottura lamiera
t= spessore
e1---p2 passi delle bullonature'''
		vars(self).update(locals())

	def update_passi(self,e1,e2,p1=None,p2=None):
		vars(self).update(locals())

	def Fb_rd(self,bul,bordo=[True,True],offset=1.,full_out=True):
		'''calcolo della resistenza a rifollamento della lamieraì
offset= diametro bullone - diametro foro
bordo: 'considera la posizione del bullone rispetto ai bordi
		in direzione parallela alla forza e perpendicolare alla forza'''
		d0=bul.d+offset
		if bordo[0]:
			alpha=min([self.e1/(3*d0) , bul.ft/self.self.mat.ftk,1.])
		else:
			alpha=min([self.p1/(3*d0)-.025 , bul.fy/self.self.mat.ftk , 1.])
		if bordo[1]:
			k=min([2.8*self.e2/d0-1.7 ,2.5])			
		else:
			k=min([1.4*self.p2/d0-1.7 , 2.5])

		F_rd= k*alpha*self.self.mat.ftk*bul.d*self.t/self.gamma_m2
		if full_out:
			d={}
			for i in ['alpha','k','d0']:
				d[i]=locals()[i]
			return F_rd,d
		else:
			return F_rd

	def Bp_rd(self,bul):
		return .6*pi*bul.d*self.t*self.self.mat.ftk/self.gamma_m2

	def passi(self,bul,exp=None):
		'''calcola i passi minimi delle bullonatura:
ritorna un dizionario con i valori minimi e i massimi per condizioni:
1- unioni esposte a fenomeni corrosivi ambientali
2- unioni non esposte a fenomeni corrosivi o ambientali
3- unioni di elementi in acciaio resistente alla corrosione EN10025-25'''
		d=bul.d
		t=self.t
		#calcolo i valori minimi:
		e1_min=1.2*d
		e2_min=1.2*d
		p1_min=2.2*d
		p10_min=2.2*d
		p1i_min=2.2*d
		p2_min=2.4*d

		e1_1=4*t+40
		e2_1=4*t+40
		p1_1=min([14*t,200])
		p10_1=p1_1
		p1i_1=min([20*t,400])
		p2_1=min([14*t,200])

		e1_2=4*t+40
		e2_2=4*t+40
		p1_2=min([14*t,200])
		p10_2=p1_1
		p1i_2=min([20*t,400])
		p2_2=min([14*t,200])

		e1_3=max([8*t,125])
		e2_3=max([8*t,125])
		p1_3=min([14*t,175])
		p10_3=p1_1
		p1i_3=min([20*t,400])
		p2_3=min([14*t,175])
		del d,t
		if exp is None:
			print('exp=none')
			return locals()
		else:
			outs='e1_min,e1_%d,e2_min,e2_%d,p1_min,p1_%d,p10_min,p10_%d,p1i_min,p1i_%d,p2_min,p2_%d'%tuple([exp]*6)
			outs=outs.split(',')
			dats=[locals()[i] for i in outs]
			return dict(list(zip(outs,dats)))


class saldatura:
	pass
class piastra:
	pass
class bullone:
	pass

#collegamenti tra trave e colonna
class g_tcsal: #trave-colonna saldato su ala
	pass
class g_tcp1al:#trave-colonna piastra saldata a trave su ala
	pass
class g_tcp2al:#trave-colonna piastra saldata a colonna su ala
	pass
class g_tcp3al:#trave-colonna doppio L bullonato su ala
	pass
class g_tcsan: #trave-colonna saldato su anima
	pass
class g_tcp1an:#trave-colonna piastra saldata a trave su anima
	pass
class g_tcp2an:#trave-colonna piastra saldata a colonna su anima
	pass
class g_tcp3an:#trave-colonna doppio L bullonato su anima
	pass
#collegamenti tra trave e trave
class g_ttp1: #trave-trave con piastra saldata anima portata
	pass
class g_ttp2:#trave-trave con piastra saldata anima portante
	pass
class g_ttp2:#trave-trave con con doppio L bullonato	
	pass
#collegamenti tra colonne e colonne/fondazioni
#collegamenti diagonali travi
	
