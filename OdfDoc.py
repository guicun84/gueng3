#provo a sparare tutto su un file odt per relazione:

from odf.opendocument import OpenDocumentText
from odf.style import Style, TextProperties,TableColumnProperties,TableCellProperties,GraphicProperties,ParagraphProperties, TableCellProperties
from odf.text import H, P, Span
from odf.table import TableColumn,TableRow,TableCell,TableHeaderRows
from odf.table import Table as Table_
from odf.draw import Object,Frame,Image as ImageOdf
import re
try:
	from Image import open as openimage
	def getImgAspect(fi):
		sz=openimage(fi)
		sz=[float(i) for i in sz.size]
		return sz[1]/sz[0]
	pass
except:
	from pylab import imread
	def getImgAspect(fi):
		sz=imread(fi)
		sz=[float(i) for i in sz.shape]
		return sz[0]/sz[1]
#finally:
#	def getImgAspect(fi):
#		print '''nessun modulo per aprire le immagini
#installare pylab o PIL'''
#		return 1
#
class Odt:
	def __init__(self,model=None):
		self.doc=OpenDocumentText()
		self.text=self.doc.text
		self.styles=Styles(model)		
		#registro gli stili nel documento
		for i in list(self.styles.paragraph.values()):
			self.doc.styles.addElement(i)
		for i in list(self.styles.column.values()):
			self.doc.styles.addElement(i)
		for i in list(self.styles.frame.values()):
			self.doc.automaticstyles.addElement(i)
		for i in list(self.styles.cell.values()):
			self.doc.automaticstyles.addElement(i)


	def h1(self,text):
		p=Paragraphs(text,self.styles.paragraph['H1'],self)
		for i in p.obj: self.doc.text.addElement(i)
	def h2(self,text):
		p=Paragraphs(text,self.styles.paragraph['H2'],self)
		for i in p.obj: self.doc.text.addElement(i)
	def h3(self,text):
		p=Paragraphs(text,self.styles.paragraph['H3'],self)
		for i in p.obj: self.doc.text.addElement(i)
	def p(self,text):
		p=Paragraphs(text,self.styles.paragraph['text_body'],parent=self)
		for i in p.obj: self.doc.text.addElement(i)
	
	def table(self,value,heading=None,skip=0,formats='9.4g'):
		#aggiungo una tabella
		table=Table(value,heading,parent=self,skip=skip,formats=formats)
		self.doc.text.addElement(table.obj)
		
	def image_odl(self,finame,w='15cm'):
		im=Image(self,finame,w).obj
		p=P()
		p.addElement(im)
		self.doc.text.addElement(p)

	def image(self,finame,w='15cm'):
		im=Image(self,finame,w).obj
		p=P()
		p.addElement(im)
		tab=Table_()
		r=TableRow()
		c=TableCell()
		c.addElement(p)
		r.addElement(c)
		tab.addElement(r)
		self.doc.text.addElement(tab)

	def save(self,fi):
		self.doc.save(fi)


class Styles:
	def __init__(self,fi=None):
		if fi is not None:
			#carico gli stili da file
			self.load_sltyle(fi)
		else:
			self.std_style()

	def std_style(self):
			#stili di testo
			self.paragraph={}
			self.paragraph['text_body']=Style(name='Text body',family='paragraph')
			self.paragraph['H1']=Style(name='Heading 1',family='paragraph')
			self.paragraph['H1'].addElement(TextProperties(attributes={'fontsize':'20pt', 'fontweight':'bold'}))
			self.paragraph['H2']=Style(name='Heading 2',family='paragraph')
			self.paragraph['H2'].addElement(TextProperties(attributes={'fontsize':'14pt', 'fontweight':'bold'}))
			self.paragraph['H3']=Style(name='Heading 3',family='paragraph')
			self.paragraph['H3'].addElement(TextProperties(attributes={'fontsize':'11pt', 'fontweight':'bold'}))
			self.paragraph['tc']=Style(name="Table Contents_", family="paragraph")
			self.paragraph['tc'].addElement(TextProperties(attributes={'fontsize':8}))
			self.paragraph['tc'].addElement(ParagraphProperties(attributes={'textalign':'right'}))
			self.paragraph['th']=Style(name="Table Heading_", family='paragraph')
			self.paragraph['th'].addElement(TextProperties(attributes={'fontsize':8,'fontweight':'bold'}))
			self.paragraph['th'].addElement(ParagraphProperties(attributes={'textalign':'center'}))
			#stili colonna:
			self.column={}
			self.column['std']= Style(name="Column", family="table-column")
			self.column['std'].addElement(TableColumnProperties(columnwidth="auto"))

			#stili frame:
			self.frame={}
			self.frame['formula']=Style(name='Formula',family='graphic')
			self.frame['formula'].addElement(GraphicProperties(
				anchortype='as-char',
				verticalrel='pharagrap',
				y='1cm',
				oledrawaspect=1))

			self.frame['Figure']=Style(name='Figure',family='graphic',parentstylename='Graphics')
			self.frame['Figure'].addElement(GraphicProperties(verticalpos='from-top',
				horizontalrel='pharagrap',
				horizontalpos='center',
				wrap='none',))

			#stili cella
			self.cell={}
			self.cell['th']=Style(name='Table Heading_0',family='table-cell')
			self.cell['th'].addElement(TableCellProperties(attributes={
				'borderleft':'0.05mm solid #000000',
				'bordertop':'0.05mm solid #000000',
				'borderbottom':'0.05mm solid #000000',
				'borderright':'0.05mm solid #000000',
				'backgroundcolor':'#c0c0c0'}))
#			self.cell['th1']=Style(name='Table Headingr_1',family='table-cell')
#			self.cell['th1'].addElement(TableCellProperties(attributes={
#				'borderleft':'0.1mm solid #000000',
#				'bordertop':'0.1mm solid #000000',
#				'borderbottom':'none',
#				'borderright':'0.1mm solid #0000',
#				'backgroundcolor':'#c0c0c0'}))
#
			self.cell['tc']=Style(name='Table Content_0',family='table-cell')
			self.cell['tc'].addElement(TableCellProperties(attributes={
				'paddingright':'1mm',
				'borderleft':'0.05mm solid #000000',
				'bordertop':'0.05mm solid #000000',
				'borderbottom':'0.05mm solid #000000',
				'borderright':'0.05mm solid #000000'}))
#			self.cell['tc1']=Style(name='Table Content_1',family='table-cell')
#			self.cell['tc1'].addElement(TableCellProperties(attributes={
#				'paddingright':'0.2mm',
#				'borderleft':'0.2mm solid #000000',
#				'bordertop':'0.2mm solid #000000',
#				'borderbottom':'none',
#				'borderright':'0.2mm solid #0000'}))
#			self.cell['tc2']=Style(name='Table Content_2',family='table-cell')
#			self.cell['tc2'].addElement(TableCellProperties(attributes={
#				'paddingright':'0.2mm',
#				'borderleft':'0.2mm solid #000000',
#				'bordertop':'0.2mm solid #000000',
#				'borderbottom':'0.2mm solid #0000',
#				'borderright':'none'}))
#
#			self.cell['tc3']=Style(name='Table Content_3',family='table-cell')
#			self.cell['tc3'].addElement(TableCellProperties(attributes={
#				'paddingright':'0.2mm',
#				'borderleft':'0.2mm solid #000000',
#				'bordertop':'0.2mm solid #000000',
#				'borderbottom':'0.2mm solid #0000',
#				'borderright':'0.2mm solid #0000'}))
#


class Paragraphs:
	def __init__(self,text,style=None,parent=None):
		self.style=style
		self.parent=parent
		text=text.replace('\n','\r')
		text=text.split('\r')
		self.obj=[]
		for i in text:
			p=self.createParagraph(i,style)
			self.obj.append(p)

	def createParagraph(self,text,style):
		if not style:
			p=P(stylename=style)
		else:
			name=style.getAttribute('displayname')
			if re.match('^Heading',name):
				lev=int(re.search('\d+',name).group())
			else:
				lev=0
			if not lev:
				p=P(stylename=style)
			else:
				p=H(stylename=style,outlinelevel=lev)
		text=self.searchFormula(text)
		for t in text:
			if t[:2]=='<#':
				t=t[2:-2]
				self.addFormula(t,p)
			else:
				p.addText(t)
		return p			


	def addFormula(self,text,p):
		if self.style:#ricerco l'altezza di testo corretta
			fontheight=None
			bold=None
			if self.style.childNodes:
				k=list(self.style.childNodes[0].attributes.keys())
				for i in k:
					if i[1]=='font-size':
						fontheight=re.search('\d+',self.style.childNodes[0].attributes[i]).group()
					if i[1]=='font-weight':
						bold=True
			if fontheight is not None:
				text= "size %s {%s}"%(fontheight,text)
			if bold is not None:
				text="bold{ %s }"%text
		ma=Formula(text)
		ma_ref=self.parent.doc.addObject(ma)
		#ma.addText('<semantics><annotation encoding="StarMath 5.0">y=sqrt(x)</annotation></semantics>')
		#eq=Equation(formula='y=sqrt(x)')

		#ma_i=doc.addObject(ma)
		#eq_i=doc.addObject(eq)
		formula_style=self.parent.styles.frame['formula']
		obj=Object(href=ma_ref,type="simple",show="embed" ,actuate="onLoad")
		fr=Frame(stylename=formula_style,relwidth='scalemin',relheight='scalemin')
		fr.addElement(obj)
		p.addElement(fr)

	def searchFormula(self,text):
		formule=re.findall('<#.*?#>',text)
		if formule:
			text=text.replace('<#','\n<#')
			text=text.replace('#>','#>\n')
		return text.split('\n')
			

class Table:
	def __init__(self,data,heading=None,parent=None,skip=0,formats='9.4g'):
		self.parent=parent
		styles=parent.styles
		self.obj=Table_()
		if type(data)==str:
			data=data.split('\n')
			for n,i in enumerate(data):
				data[n]=i.split('\t')
		data=data[skip:]
		for i in range(len(data[0])):
			self.obj.addElement(TableColumn(stylename=styles.column['std']))
		if heading is not None:
			if type(heading) ==str:
				heading=heading.split(',')
			head=TableHeaderRows()
			ri_=TableRow()
			head.addElement(ri_)
			self.obj.addElement(head)
			for ce in heading :
				ce_=TableCell(stylename=styles.cell['th'])
#				if ce!=heading[-1]:
#					ce_=TableCell(stylename=styles.cell['th'])
#				else:
#					ce_=TableCell(stylename=styles.cell['th1'])
				ri_.addElement(ce_)
				obj=Paragraphs(ce,styles.paragraph['th'],parent=self.parent).obj
				for i in obj:
					ce_.addElement(i)
		for ri in data:
			ri_=TableRow()
			self.obj.addElement(ri_)
			for ce in ri:
				ce=self.parse(ce,formats)
				ce_=TableCell(stylename=styles.cell['tc'])
#				if ce!=ri[-1] and ri!=data[-1]:
#					ce_=TableCell(stylename=styles.cell['tc'])
#				elif ce==ri[-1] and ri!=data[-1]:
#					ce_=TableCell(stylename=styles.cell['tc1'])
#				elif ce!=ri[-1] and ri==data[-1]:
#					ce_=TableCell(stylename=styles.cell['tc2'])
#				elif ce==ri[-1] and ri==data[-1]:
#					ce_=TableCell(stylename=styles.cell['tc3'])
				ri_.addElement(ce_)
				obj=Paragraphs(ce,styles.paragraph['tc'],parent=self.parent).obj
				for i in obj:
					ce_.addElement(i)

	def parse(self,value,formats='9.4g'): #utile per tabelle...
		if type(value )in [str,str]:
			return value
		else: return ('{{:{}}}'.format(formats)).format(value)

	
class Formula:
	def __init__(self,testo):
		testo=testo.replace('<','&lt;')
		testo=testo.replace('>','&gt;')
		self.testo=testo
		#devo pulire il testo da < e >
		self.childobjects=[]
		self.Pictures={}
		self.mimetype='application/vnd.oasis.opendocument.formula'
		d=OpenDocumentText()
		self.settings=d.settings
	def contentxml(self):
		return '<math xmlns="http://www.w3.org/1998/Math/MathML"><semantics><annotation encoding="StarMath 5.0">' + self.testo + '</annotation></semantics></math>'
	def stylesxml(self):
		return ''

	def hasChildNodes():
		return False

class Image:
	def __init__ (self,parent,fi,w='15cm'):
		img_ref=parent.doc.addPicture(fi)
		img=ImageOdf(href=img_ref)
		aspect=getImgAspect(fi)
		w_=re.search('(\d+)(.*)',w).groups()
		h_=float(w_[0])*aspect
		h='%f%s'%(h_,w_[1])
		self.obj=Frame(stylename=parent.styles.frame['Figure'],width=w,height=h)
		self.obj.addElement(img)

def formulize(st,var=False):
	#ritorna una stringa con il markup per le formule
	st.split('\n')
	st='\n'.join(['<##%s##>'%i for i in st])
	char='alpha beta gamma delta epsilon zita eta theta iota kappa lambda mi nu ni xi omicron pi rho sigma tau upsilon phi chi psi omega'.split()
	char_='%alpha %beta %gamma %delta %epsilon %zita %eta %theta %iota %kappa %lambda %mi %nu %ni %xi %omicron %pi %rho %sigma %tau %upsilon %phi %chi %psi %omega'
	char_v='%alpha %beta %gamma %delta %epsilon %zita %eta %theta %iota %kappa %lambda %mi %nu %ni %xi %omicron %pi %rho %sigma %tau %upsilon %phi %chi %psi %omega'
	
	return st
