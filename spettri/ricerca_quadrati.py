import sqlite3
from scipy.spatial import Delaunay
import pyproj
import pickle,zlib
import pandas as pa
from scipy import *
from numpy import *

db=sqlite3.connect('all2.sqlite')
print('fine caricamento db')
punti=r_[db.execute('select id,lon,lat from punti').fetchall()]
punti=pa.read_sql('select id,lon,lat from punti',db)
punti['index']=arange(punti.shape[0])
punti['id2']=arange(len(punti))
for n,i in  punti.iterrows():
	db.execute('update punti set id2=? where id=?',(i.id2,i.id))
db.commit()

print('eseguo tiangolazine di Delaunay')
de=Delaunay(punti['lon lat'.split()].values)
tri=de.simplices
#triid=[[ide[i],ide[j],ide[k]] for i,j,k in de.simplices]
#db.executescript('''
#	drop table if exists triangoli;
#	create table triangoli (
#	i integer,
#	j integer,
#	k integer);
#	''')
#db.executemany('insert into triangoli values(Null,?,?,?)',triid)
db.commit()

tri_=pa.DataFrame(arange(tri.shape[0]),columns=['row'])
tri_['simp']=tri_.row
tri_['i']=tri[:,0]
tri_['j']=tri[:,1]
tri_['k']=tri[:,2]
#tri_['ids']=triid


############################################################
print('calcolo lunghezze lati')
#n=len('{}'.format(punti[:,0].max()))
#keypath='{{:{}d}}-'.format(n)*2
geod=pyproj.Geod(ellps='intl')
l=[]

if False:
	def lij(i,j):
		pt=punti.iloc[[i,j]]
		return geod.inv(
			pt.iloc[0].lon,
			pt.iloc[0].lat,
			pt.iloc[1].lon,
			pt.iloc[1].lat)[-1]
	for i in tri:
		l.append([lij(i[0],i[1]), lij(i[1],i[2]), lij(i[2],i[0])])
	tri_['l']=l
	print(tri_.iloc[0].l)

	############################################################
	print('ricerco altro triangolo con lato lugno in comune')
	class Myprint:
		run=False
		def __call__(self,*args,**kargs):
			if self.run:
				print(*args,**kargs)
	pr=Myprint()

	def altro(tri):
		#cerco lato con l max
		n,tri=tri
		lindex=tri.l.index(max(tri.l))
		pr(lindex)
		#trovo punti del lato massimo
		pt=[[tri.i,tri.j],[tri.j,tri.k],[tri.k,tri.i]][lindex]
		pts=[tri.i,tri.j,tri.k]
		#cerco triangolo con punti precedenti
		t=tri_[
			(tri_.i.isin(pt) & tri_.j.isin(pt) & ~(tri_.k.isin(pts))) |\
			(tri_.j.isin(pt) & tri_.k.isin(pt) & ~(tri_.i.isin(pts))) |\
			(tri_.k.isin(pt) & tri_.i.isin(pt) & ~(tri_.j.isin(pts))) 
		]
		if len(t)==0:return None
		t=t.iloc[0]
		altro=[i for i in [t.i,t.j,t.k] if i not in pts][0]
		return tri.simp,altro
		#estraggo quarto punto dal triangolo

	import multiprocessing as mp
	pool=mp.Pool(mp.cpu_count())
	ris=pool.map(altro,tri_.iterrows())
	ris2=pa.DataFrame(ris,columns='simp h'.split())
	tri_=pa.merge(tri_,ris2,on='simp'.split())
	tri_.to_csv('quadrati.csv')
else:
	tri_=pa.read_csv('quadrati.csv')

print(tri_[:10])

#tri_['h']=tri_.apply(altro,1)

db.executescript('''
drop table if exists quadri;
create table quadri (ide integer primary key autoincrement,
i integer,
j integer,
k integer,
h integer);
drop view if exists triangoli;
create view if not exists triangoli as select ide,i,j,k from quadri;
''')

#devo inserire gli id dei punti del reticolo
tri_=tri_.merge(punti,left_on='i',right_on='index',suffixes=('','_i'))
tri_=tri_.merge(punti,left_on='j',right_on='index',suffixes=('','_j'))
tri_=tri_.merge(punti,left_on='k',right_on='index',suffixes=('','_k'))
tri_=tri_.merge(punti,left_on='h',right_on='index',suffixes=('','_h'))
print(tri_.keys())


temp=tri_['simp id id_j id_k id_h'.split()].values
for i in temp:
	j=[int(k) for k in i]
	db.execute('insert into quadri values(?,?,?,?,?)',j)
db.commit()

	

#temp=pa.read_sql('''
#select 
#	i.x as xi,
#	i.y as yi,
#	j.x as xj,
#	j.y as yj,
#	k.x as xk,
#	k.y as yk,
#	h.x as xh,
#	h.y as yh
#
#from quadri q inner join punti i on 
