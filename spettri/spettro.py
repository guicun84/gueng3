
from scipy import *

class Spettro:
	def __init__(self,Tb,Tc,Td,ag,S,F0,eta=1,q=1,TE=None):
		'''Tb Tc Td= periodi dello spettro
		ag= accelerazione al suolo
		S= coefficiente di topografia
		F0= parametro spettrale
		eta=1...
		q= fattore di struttura
		TE= periodo di rifierimento spettro di spostamento
		'''
		vars(self).update(locals())
		self.const=ag*S*eta*F0/q
		self.num=100 #numero di punti
		self.start=0
		self.end=4.
		self.q=q
	
	def calc(self,t):
		if 0<=t<self.Tb:
			#return self.const* (t/self.Tb + 1/(self.eta*self.F0)*(1-t/self.Tb))
			return self.ag+(self.const-self.ag)*(t/self.Tb)
			#return self.ag
		elif self.Tb<=t< self.Tc:
			return self.const
		elif self.Tc<= t < self.Td:
			return self.const*self.Tc/t
		else:
			return self.const *  (self.Tc*self.Td)/t**2
	calc_v=vectorize(calc)
	def __call__(self,t):
		return self.calc_v(self,t)

	def __spettro_d(self,t):
		TF=10 #dm2018 pag 49 ta 3.2.VII
		if t<self.TE:
			return self.calc(t)*(t/2/pi)**2
		elif t<TF:
			return .025*self.ag*self.S*self.Tc*self.Td*\
				(self.F0*self.eta+(1-self.F0*self.eta)*(t-self.TE)/(TF-self.TE))
		else:
			return .025*self.ag*self.S*self.Tc*self.Td
	
	__vec_spettro_d=vectorize(__spettro_d)
	def spettro_d(self,T):
		print('spettro di spostamento')
		return self.__vec_spettro_d(self,T)
			


	def to_file(self,fi,t=None):
		if t is None:
			t=linspace(self.start,self.end,self.num)
		v=self(t)
		fi=open(fi,'w')
		for i,j in zip (t,v):
			fi.write('%.5f,%.5f\n'%(i,j))
		fi.close()

	def __str__(self):
		return '''Parametri spettrali:
ag={ag} m/s^-1
F0={F0}
Tb={Tb} s inizio plateau
Tc={Tc} s fine plateau
Td={Td} s
S={S}
eta={eta}
q={q}'''.format(**vars(self))

class Struttura:
	Vn={1: 10,2:50,3:100}
	Cu={1:.7,2:1,3:1.5,4:2}
	def __init__(self,tipo=2,classe=2,q=1.5):
		'''tipo=1: opere povvisionali
				2: opere ordinarie
				3: grandi opere strategiche
			classe 1: presenza occasionale di persone
				   2: normali affollamenti senza elementi pericolosi
				   3: grandi affollamenti o elementi pericolosi
				   4: funzioni pubbliche o strategiche
			q=coefficiente di struttura'''
		vars(self).update(locals())
		self.Vn=Struttura.Vn[tipo]
		self.Cu=Struttura.Cu[classe]
		self.Vr=max((35,self.Vn*self.Cu))
	
#	def Trold(self,stato='slv'):
#		'''stato=slo sld slv slc'''
#		if stato not in 'slo sld slv slc'.split():
#			raise TypeError , 'stato %s non conosciuto'%stato
#		if stato=='slo':
#			return max([30,.6*self.Vn*self.Cu])
#		elif stato=='sld':
#			return self.Vn*self.Cu
#		elif stato=='slv':
#			return self.Vn*self.Cu*9.5
#		else:

#			return min([19.5*self.Vn*self.Cu,2475])
	def Tr(self,stato='slv'):
		'''stato=slo sld slv slc'''
		if stato not in 'slo sld slv slc'.split():
			raise TypeError('stato %s non conosciuto'%stato)
		p=dict(list(zip('slo sld slv slc'.split(),(.81,.63,.1,.05))))[stato]
		t=-self.Vr/log(1-p)
		return max((30,round(t,0)))
		
		

if __name__=='__main__':
	sp_SLO=Spettro(.07,.21,1.731,.033,1,2.5)
	sp_SLD=Spettro(.077,.23,1.762,.041,1,2.54)
	sp_SLV=Spettro(.097,.29,1.993,.098,1,2.51)
	sp_SLC=Spettro(.097,.290,.2117,.129,1,2.47)
	spv_SLO=Spettro(.07,.21,1.731,.033,1,.61)
	spv_SLD=Spettro(.077,.23,1.762,.041,1,.69)
	spv_SLV=Spettro(.097,.29,.1993,.098,1,1.062)
	spv_SLC=Spettro(.097,.290,.2117,.129,1,1.199)

	sp_SLO.to_file('sp_SLO.txt')
	sp_SLD.to_file('sp_SLD.txt')
	sp_SLV.to_file('sp_SLV.txt')
	sp_SLC.to_file('sp_SLC.txt')
	spv_SLO.to_file('spv_SLO.txt')
	spv_SLD.to_file('spv_SLD.txt')
	spv_SLV.to_file('spv_SLV.txt')
	spv_SLC.to_file('spv_SLC.txt')


