#-*- coding:utf-8-*-

from scipy import *
from scipy.linalg import norm
from scipy.spatial import Delaunay
from .spettro import Spettro
import sqlite3,os,re,zlib,pickle
from pylab import *
from scipy.constants import g as G
import scipy as sp
import pandas as pa

class Reticolo:
	db=os.path.dirname(__file__)
	db=os.path.join(db,'reticolo.sqlite')
	print(db)
	db=sqlite3.connect(db)
	print(('scipy version',sp.__version__))
	if sp.__version__=='0.15':
		de=pickle.loads(zlib.decompress(cur.execute('select val from variabili where name="delaunay"').fetchone()[0]))
	else:
		simplices=pa.read_sql('select i,j,k from triangoli2',db)
		pt=pa.read_sql('select lon,lat from punti',db)
		de=Delaunay(pt.values)
		print(type(de.simplices))
		print(de.simplices.dtype)
		de.simplices=simplices.values.astype(np.int32)

	def cerca(self,lon,lat,rif='wgs84'):
		#cerca i 4 punti più vicini del reticolo e calcola i pesi per ognuno nell'interpolazione
#		id1=self.db.execute('select id,lon,lat,lon-:0 as dlon,lat-:1 as dlat from punti where lon<=:0 and lat<=:1 order by dlon*dlon+dlat*dlat limit 1',(lon,lat)).fetchone()
#		id2=self.db.execute('select id,lon,lat,lon-:0 as dlon,lat-:1 as dlat from punti where lon<=:0 and lat>=:1 order by dlon*dlon+dlat*dlat limit 1',(lon,lat)).fetchone()
#		id3=self.db.execute('select id,lon,lat,lon-:0 as dlon,lat-:1 as dlat from punti where lon>=:0 and lat>=:1 order by dlon*dlon+dlat*dlat limit 1',(lon,lat)).fetchone()
#		id4=self.db.execute('select id,lon,lat,lon-:0 as dlon,lat-:1 as dlat from punti where lon>=:0 and lat<=:1 order by dlon*dlon+dlat*dlat limit 1',(lon,lat)).fetchone()
#		punti=[id1,id2,id3,id4]
#		ids=r_[[i[0] for i in punti]] #indici dei punti da utilizzare
#		punti=r_[[[i[1],i[2]] for i in punti]]
#		punti=r_[punti,r_[[[lon,lat]]]]
		ids,punti,posED50=self.cerca_punti(lon,lat,rif)
#		g=GaussBoaga()
#		co=g.convert(punti)
#		dist=sqrt((co[:-1,0]-co[-1,0])**2+(co[:-1,1]-co[-1,1])**2)
		dist=self.distanze(punti)
		p=(1/dist)/(1/dist).sum() #pesi dei punti per interpolazione
		#print('{},{}'.format(*posED50))
		return ids,p,dist,posED50

	def cerca_punti(self,lon,lat,rif='wgs84'):
		'''lon,lat posizione punto 
		rif=sistema di riferimento wgs84 default
			wgs84
			ed50
		'''
		try :
			from pyproj import Geod,Proj,transform
		except:
			print('installare pyproj')
			raise ImportError
		#definisco sistema di riferimento del punto dato
		stinit=dict(wgs84='+init=EPSG:4326',
					ed50='+init=EPSG:4230')[rif]
		ref=Proj(stinit)
		#sistema di riferimento dei punti del reticolo
		ed50=Proj('+init=EPSG:4230')
		#converto punto dato in punto reticolo
		lon,lat=transform(ref,ed50,lon,lat)
		
		#interrogo la triangolazione di delaunay
		simp=self.de.find_simplex(r_[lon,lat])
		pts=self.de.simplices[simp] #punti del triangolo


		#revisione 2021
		#ricerco gli identificativi dei 4 punti del reticolo
		temp=list(pts)*3
		temp=[int(i) for i in temp]
		print(temp)
		ide=pa.read_sql('''
		select q.i,q.j,q.k,q.h from quadri q 
		inner join quadri2 q2 on q.ide=q2.ide
		where q2.i in (?,?,?)
		and q2.j in (?,?,?)
		and q2.k in (?,?,?)''',self.db,params=temp).iloc[0]
		ide=[int(i) for i in ide.values]
		#ricerco le coordinate dei 4 punti
		co=pa.read_sql('select lon,lat from punti where id in (?,?,?,?)',self.db,params=ide).values

############# si puo togliere dopo rev 2021###########
#		co=self.de.points[pts]
		#interrogo il database
#		ide=[cur.execute('select id from punti where lon=? and lat=?',(i[0],i[1])).fetchone()[0] for i in co]
#		tri=cur.execute('''select id from triangoli where
#		i in (?,?,?) 
#		and j in (?,?,?) 
#		and k in (?,?,?)'''
#					  ,ide*3).fetchone()[0]
#		ptagg=cur.execute('select punto from puntoAggiuntivo where triangolo=?',(int(tri),)).fetchone()[0]
#		ide.append(ptagg)
#		punti0=array(cur.execute('select lon,lat from punti where id in (?,?,?,?)',ide).fetchall())

#		tri=self.de.find_simplex(r_[lon,lat])+1
#		cur=self.db.cursor()
#		punti0=array(cur.execute('''select punti.id,lon,lat from punti inner join (
#		select i as pt from quadri where id=:0 union
#		select j as pt from quadri where id=:0 union
#		select h as pt from quadri where id=:0 union
#		select k as pt from quadri where id=:0 ) pts on punti.id=pts.pt''',(int(tri),)).fetchall())


#		punti0=cur.execute('select * from v2 order by dist').fetchall()
#		cur.close()
#		ids=punti0[:,0].astype(int)
#		punti=punti0[:,1:]
#		punti=r_[punti,r_[[[lon,lat]]]]
#################fino a qui################

		co=vstack((co,r_[lon,lat]))

		return r_[ide],co,r_[lon,lat]

	def distanze(self,punti):
		try:
			from pyproj import Geod
		except:
			print('bisogna installare pyproj')
			raise ImportError
		ge=Geod(ellps='intl')
		dist=r_[[ge.inv(i[0],i[1],punti[-1,0],punti[-1,1])[-1] for i in punti[:-1]]]
		return dist
	
class GaussBoaga:
	def __init__(self,lmean=None):
		self.lon_mean=lmean

	def calcola_(self,i):
		if i.ndim==1:
			i=r_[[i]]
		lamp=i[:,0]-self.lon_mean
		ep2=.006768170197
		v1=sqrt(1+ep2*cos(i[:,1])**2)
		xi=arctan(tan(i[:,1])/cos(v1*lamp))
		v=sqrt(1+ep2*cos(xi)**2)
		c=6399936.608
		Y=c*arcsinh(cos(xi)*tan(lamp)/v)
		X=637654.40006*xi - 16107.03468*sin(2*xi) + 16.97621*sin(4*xi) - 0.02227*sin(6*xi)
		return c_[X,Y]

	def calcola(self,p):
		if p.ndim==1:
			p=r_[[p]]
		B=p[:,0]-self.lon_mean
		w=sqrt(1+0.0067681702 * cos(p[:,1])**2)
		A=arctan(p[:,1])/cos(w*B)
		v=sqrt(1+.0067681702*cos(A)**2)
		Y=6397376.633*arcsinh(cos(A)*tan(B)/v)
		N=111092.08210*A - 16100.59187*sin(2*A) + 16.96942*sin(4*A) - .02226*sin(6*A)
		N/=10
		Y/=1e3
		return c_[Y,N]
	
	def convert(self,punti):
		punti=deg2rad(punti)
		if self.lon_mean is None:
			self.lon_mean=punti[:,0].mean()
		return self.calcola(punti)

class Punto:
	r=Reticolo()
	def __init__(self,lon,lat,cat_suolo,topo,struttura=None,unit='m/s**2',rif='wgs84'):
		'''lon,lat=posizione in wgs84
		cat_suolo categoria sulo A B C...
		topo categoria topografica T1,T2...
		struttura= istanza di Struttura per i tempi non necessaria se non per __str__ esteso
		unit=unità utilizzate m/s**2 | g'''
		self.lat=lat
		self.lon=lon
		self.terreno=Terreno(cat_suolo)
		self.topografia=topo
		self.punti,self.pesi,self.dist,self.posED50=self.r.cerca(self.lon,self.lat,rif)
		self.struttura=struttura
		self.unit=unit
		self.rif=rif

	@property
	def multag(self):
		'''moltiplicatore per avere le accelerazioni in g on in m/s**2, sul db ci sono i valori della tabella di normativa espressi in g/10 che sono circa m/s**2,'''
		return {'m/s**2':10/G,
				'g':1./10}[self.unit]
	
	@property
	def St(self):
		dat=self.topografia.split('.')
		val= {'T1':1 ,'T2':1.2 , 'T3':1.2,'T4':1.4}[dat[0]]
		if len(dat)==1: return val
		p=1-float('0.'+dat[1])
		return interp(p,(0,1.),(1.,val))
	
	def valori(self,T='slv'):
		if type(T)==str:
			T=self.struttura.Tr(T)
		'''T= tempo di ritorno
		ritorna i valori di (Tb,Tc,Td,ag,S,f0),locals() per il dato tempo di ritorno'''
		#recupero i valori dei tempi di ritorno presenti nel db:
		trs_s=self.r.db.execute('select name from sqlite_master where type=="table" and name like "TR!_%" escape "!"').fetchall()
		trs_s=r_[[i[0] for i in trs_s]]
		trs=zeros(len(trs_s))
		for n,i in enumerate(trs_s):
			trs[n]=float(re.sub('(tr_|TR_)','',i))
		trs.sort()
		dim=len(self.r.db.execute('select * from ' + trs_s[0] + ' limit 1').fetchone())
		if T in trs:
			dati=zeros((dim,self.punti.size))
			tab='tr_{:.0f}'.format(T)
			for n,i in enumerate(self.punti):
				dati[n,:]=r_[self.r.db.execute('select * from ' + tab + ' where id=?',(int(i),)).fetchall()]
			dati=dati[:,1:]

		else:
			#devo interpolare
			T1=trs[where(trs<T)[0][-1]]
			T2=trs[where(trs>=T)[0][0]]
			dati1=zeros((dim,self.punti.size))
			tab1='tr_{:.0f}'.format(T1)
			for n,i in enumerate(self.punti):
				dati1[n,:]=r_[self.r.db.execute('select * from ' + tab1 + ' where id=?',(int(i),)).fetchall()]
			dati1=dati1[:,1:]
			dati2=zeros((dim,self.punti.size))
			tab2='tr_{:.0f}'.format(T2)
			for n,i in enumerate(self.punti):
				dati2[n,:]=r_[self.r.db.execute('select * from ' + tab2 + ' where id=?',(int(i),)).fetchall()]
			dati2=dati2[:,1:]
			dati=zeros_like(dati1)
			for n in range(dati.shape[0]):
				for m in range(dati.shape[1]):
					v1=dati1[n,m]
					v2=dati2[n,m]
					dati[n,m]=exp(log(v1)+log(v2/v1)*log(T/T1)/log(T2/T1))
		#ora ho i dati al tempo di ritorno corretto,
		#calcolo i valo pesati tra i punti:
		out=zeros(dati.shape[1])
		for n,i in enumerate(out):
			out[n]=(dati[:,n]*self.pesi).sum()
		ag,f0,Tcs= out
		Ss=self.terreno.Ss(f0,ag)
		St=self.St
		S=Ss*St
		Cc=self.terreno.Cc(Tcs)
		Tc=Tcs*Cc
		Tb=Tc/3
		Td=4*ag/9.81+1.6
		return (Tb,Tc,Td,ag,S,f0),locals()

	def spettro(self,T,t=1j,q=True):
		'''T= tempo di ritorno o stringa "slo sld slv slc"
		t=array su cui valutare il tempo,	se non restituisce l'istanza dello spettro.
		q=fattore di struttura:None=1,True=usa quello della struttura,float:specificato di volta in volta'''
		t=atleast_1d(t)
		if iscomplex(t[0]):
			t=linspace(0,4,200)
		if q is None:
			q=1
		elif q is True:
			if self.struttura is not None:
				q=self.struttura.q
			else:
				q=1
		if type(T)==str:
			T=self.struttura.Tr(T)
		val=self.valori(T)[0]
		s=Spettro(*val,q=q,TE=self.terreno.TE)
		if t is None:
			return s*self.multag
		else:
			return c_[t,s(t)*self.multag]

	def spettro_d(self,T,t=1j,q=True):
		'''T= tempo di ritorno o stringa "slo sld slv slc"
		t=array su cui valutare il tempo,	se non restituisce l'istanza dello spettro.
		q=fattore di struttura:None=1,True=usa quello della struttura,float:specificato di volta in volta'''
		t=atleast_1d(t)
		if iscomplex(t[0]):
			t=linspace(0,4,200)
		if q is None:
			q=1
		elif q is True:
			if self.struttura is not None:
				q=self.struttura.q
			else:
				q=1
		if type(T)==str:
			T=self.struttura.Tr(T)
		val=self.valori(T)[0]
		s=Spettro(*val,q=q,TE=self.terreno.TE)
		return c_[t,s.spettro_d(t)]

	def plot(self,coste=0,fiumi=0):
		'''coste: 1 per plot anche delle coste
		fiumi: 1 per plot dei corsi d'acqua princiapli'''
		from pylab import figure,clf,show,draw,text,plot
		from mpl_toolkits import basemap as bm
		#figure(1);show(0)
		clf()
		lllo,llla,urlo,urla=self.r.db.execute('select min(lon),min(lat),max(lon),max(lat) from punti').fetchone()
		limiti=r_[llla,urla,lllo,urlo]
		lllo,urlo=r_[lllo,urlo]+r_[-1,3]*.1*(urlo-lllo)
		llla,urla=r_[llla,urla]+r_[-1,1]*.1*(urla-llla)
		self.mp=bm.Basemap(lllo,llla,urlo,urla,resolution='l',projection='tmerc',lat_0=mean(limiti[:2]), lon_0=mean(limiti[2:]))
#		self.mp=bm.Basemap(lllo,llla,urlo,urla,resolution='l',projection='tmerc',lat_0=self.lat, lon_0=self.lon)
		self.mp.drawparallels(arange(int(llla)//5*5,int(urla),5),labels=[True ,False,False,False])
		self.mp.drawmeridians(arange(int(lllo)//5*5,int(urlo),5),labels=[False,False,False,True])
		self.mp.drawparallels(arange(int(llla),int(urla),1),color='grey')
		self.mp.drawmeridians(arange(int(lllo),int(urlo),1),color='grey')
		if coste:
			self.mp.drawcoastlines(antialiased=True)
			print('fine coste')
		dati=r_[self.r.db.execute('select lon,lat from punti').fetchall()]
		self.mp.plot(dati[:,0],dati[:,1],'.b',alpha=.2,latlon=True)
		print('fine griglia punti')
		self.mp.plot(self.posED50[0],self.posED50[1],'xr',markeredgewidth=2,latlon=True);
		dati=r_[self.r.db.execute('select lon,lat,id from punti where id in (' + ','.join(['?']*len(self.punti)) + ')',self.punti).fetchall()]
		self.mp.plot(dati[:,0],dati[:,1],'.r',alpha=1,latlon=True)
		dati_=self.mp(dati[:,0],dati[:,1])
		dati[:,0]=dati_[0]
		dati[:,1]=dati_[1]
		x,y=self.mp(self.posED50[0],self.posED50[1])
		pt=r_[x,y]
		for i in dati:
			text(i[0],i[1],'{:.0f}'.format(i[2]))
			v=i[:2]-pt
			vm=pt+v/2
			a=arctan(v[1]/v[0])
			plot([pt[0],i[0]],[pt[1],i[1]],'--',color='grey')
			text(vm[0],vm[1],'{:.0f}'.format(norm(v)),rotation=rad2deg(a),ha='center',va='center')
		print('fine punti interesse')
#		self.mp.resolution='f'
		if fiumi:
			self.mp.drawrivers(color='b')
			self.mp.resolution='l'
		draw()

	def __str__(self):
		punt=','.join(['{}'.format(i) for i in self.punti])
		topografia_=self.topografia.split('.')[0]
		vals=self.valori()[1]
		st='''Latitudine= {lat:.6g}
Longitudine= {lon:.6g} [{rif}]
{terreno}
	Cc={Cc:.3f} , Ss={Ss:.3f}
Categoria topografica={topografia_}
	St={st:.4g}

Punti di appoggio del reticolo sismico {punt}'''.format(st=self.St,punt=punt,topografia_=topografia_,Ss=vals['Ss'],Cc=vals['Cc'],**vars(self))
		if self.struttura:
			st+='''
tipo di struttura:{}, classe d'uso:{}			
coefficiente di struttura:{}
Parametri spettrali [{}]:
SL	Vr	Tb	Tc	Td	Tc*	ag	S	F0'''.format(self.struttura.tipo,self.struttura.classe,self.struttura.q,self.unit)

			template='\n{}	{:6.2f}	{:6.4f}	{:6.4f}	{:6.4f}	{Tcs:6.4f}	{:6.4f}	{:6.4f}	{:6.4f}'
			for sl in 'slo sld slv slc'.split():
				tr=self.struttura.Tr(sl)
				dat=self.valori(tr)
				dat_=array(dat[0])
				dat_[3]*=self.multag
				st+=template.format(sl.upper(),tr,*dat_,Tcs=dat[1]['Tcs'])
		return st

	def plotSpettri(self,tr=None,tipo='pr'):
		'''tr= tempo di ritorno o stato limite da plottare [slo sld slv slc],
			se none plotta tutto [serve però self.struttura]
			tipo= [all el pr] spetro da plottare elastico, di progetto , tutto'''
		sl=None
		if tr is None and self.struttura is not None:
			sl='slo sld slv slc'.split()
			tr=[self.struttura.Tr(i) for i in sl]
		elif type(tr)==str and self.struttura is not None:  
			sl=[tr]
			tr=[self.struttura.Tr(tr)]
		elif is_numlike(tr):
			tr=[tr]
		elif type(tr) not in (list,tuple) or type(tr[0])==str:
			raise IOError('serve definire self.struttura')
		colors='b g r c'.split()
		if sl is None:
			sl=['Tr:{:.4g}'.format(i) for i in tr]
		else:
			sl=[i.upper() for i in sl]
		for t,s,c in zip(tr,sl,colors):
			if tipo in('all','el'):
				dat=self.spettro(t,q=1)
				plot(*dat.T,label='{} elastico'.format(s),color=c,linestyle='--')
			if tipo in('all','pr'):
				dat=self.spettro(t,q=True)
				plot(*dat.T,label='{} progetto'.format(s),color=c)
		legend(loc='best')
		xlabel('periodo [s]')
		ylabel('a [{}]'.format(self.unit))
		grid(1)


			







class Terreno:
	def __init__(self,cat):
		self.cat=cat
		self.TE=dict(A=4.5,B=5,C=6,d=6,E=6)[cat]
	def Ss(self,f0,ag):
		if self.cat=='A':
			return 1
		elif self.cat=='B':
			val=1.4-.4*f0*ag/9.81
			if val<1: return 1
			elif val>1.2: return 1.2
			else:return val
		elif self.cat=='C':
			val=1.7-.6*f0*ag/9.81
			if val<1: return 1
			elif val>1.5 :return 1.5
			else: return val
		elif self.cat=='D':
			val =2.4-1.5*f0*ag/9.81
			if val<.9: return .9
			elif val<1.8: return 1.8
			else: return val
		elif self.cat=='E':
			val =2-1.1*f0*ag/9.81
			if val<1:return 1
			if val>1.6: return 1.6
			else: return val

	def Cc(self,tc):
		if self.cat=='A': return 1
		elif self.cat=='B': return 1.1*tc**(-.2)
		elif self.cat=='C': return 1.05*tc**(-.33)
		elif self.cat=='D': return 1.25*tc**(-.5)
		elif self.cat=='E': return 1.15*tc**(-.4)

	def __str__(self):
		return '''categoria suolo ={}'''.format(self.cat)
		
