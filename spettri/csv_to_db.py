from scipy import *
import sqlite3,re,sys
import pandas as pa
#recupero tempi di ritorno
fname='spettri2008.csv'
fi=open(fname,'rb')
st=fi.readline()
fi.close()
tempi=[int(i) for i in re.findall(r'\d+',st)]
print(tempi)

dati=pa.read_csv(fname,skiprows=1)
print(dati.shape , dati.iloc[0])

db=sqlite3.connect('all2.sqlite')
dat_=dati['ID LON LAT'.split()].copy()
dat_.columns='id lon lat'.split()
dat_.to_sql('punti',db,if_exists='replace',index=False,index_label='id')
for t in tempi:
	dat_=dati['ID T{0}ag T{0}F0 T{0}Tc'.format(t).split()].copy()
	dat_.columns='id ag F0 Tc'.split()
	dat_.to_sql('tr_{}'.format(t),db,if_exists='replace',index=False)
