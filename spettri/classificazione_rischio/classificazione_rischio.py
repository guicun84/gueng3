from numpy import *
from plotly import offline as pl

class Classifica():
    classi='A+ A B C D E F G'.split()
    soglie=[8,7,6,5,4,3,2,1]
    strpath=''
    def test(self,v):
        pass
    def __init__(self,val):
        'val=punteggio in val assoluto (no %)'
        self.val=val
        stop=False
        for n,i in enumerate(self.soglie):
            if self.test(i):
                stop=True
                break
        if stop:
            self.classe=self.classi[n]
        else:
            self.classe=self.classi[n+1]
    def __lt__ (self,other):
        return self.classe<other.classe
        
    def __eq__ (self,other):
        return self.classe==other.classe
    def __str__(self):
        return self.strpath.format(**vars(self))
    def __repr__(self):
        return self.__str__()
    


# # Classificazione PAM

class ClassificaPAM(Classifica):
    soglie=r_[.5,1,1.5,2.5,3.5,4.5,7.5]/100
    strpath='Classe PAM: {classe}, PAM={val:.3%}'
    def test(self,v):
        return self.val<=v
    
    def __init__(self,punto,pgao=None,pgad=None,pgav=None,pgac=None):
        if pgao is None and pgad is None :raise IOError('definire uno trao pgao e pgad')
        if pgav is None and pgac is None :raise IOError('definire uno trao pgav e pgac')
        vars(self).update(locals())
        self.lamo=self.lam('slo')
        self.lamd=self.lam('sld')
        self.lamv=self.lam('slv')
        self.lamc=self.lam('slc')
        #ottengo in modo derivato i parametri mancanti:
        if self.lamo is None:
            self.lamo=1.67*self.lamd
        if self.lamd is None:
            self.lamd=self.lamo/1.67
        if self.lamv is None:
            self.lamv=self.lamc/.49
        if self.lamc is None:
            self.lamc=.49*self.lamv
        self.To=1/self.lamo
        self.Td=1/self.lamd
        self.Tv=1/self.lamv
        self.Tc=1/self.lamc
        self.lami,self.Ti=.1,10 #inizio di danno convenzionale
        
        lam=r_[0,self.lamc,self.lamc,self.lamv,self.lamd,self.lamo,self.lami]
        cr=r_[100,100,80,50,15,7,0]/100
        self.curva=vstack((lam,cr)).T
        self.PAM=trapz(self.curva[:,1],self.curva[:,0])
        super().__init__(self.PAM)
        

    def T(self,sl):
        pga=dict(slo=self.pgao,sld=self.pgad,slv=self.pgav,slc=self.pgac)[sl]
        if pga is not None:
            return self.punto.struttura.Tr(sl)*(pga/self.punto.spettro(sl,0)[0,1])**(1/0.41)

    def lam(self,sl):
        T=self.T(sl)
        if T: return 1/T

    def plot(self):
        pl.iplot(dict(
            data=[
                dict(x=self.curva[:,0],y=self.curva[:,1],
                     text=['','','slc','slv','sld','slo','slid'],
                    mode='markers+lines+text',textposition='top right'),
             ],
            layout=dict(xaxis=dict(type='log',title='lambda (frequenza media di superamento annua)'),
                       yaxis=dict(title='costo di ricostruzione percentuale'))))

    def __str__(self):
            return f'''{self.punto}
            
# Accelerazioni agli stati limite:
SLO: {self.pgao:} g
SLD: {self.pgad:} g
SLV: {self.pgav:} g
SLC: {self.pgac:} g

# periodi di ritorno
SLO: T={self.To:.3f} anni , lambda={self.lamo:.3g} , cr {self.curva[-2,1]:.1%}
SLD: T={self.Td:.3f} anni , lambda={self.lamd:.3g} , cr {self.curva[-3,1]:.1%}
SLV: T={self.Tv:.3f} anni , lambda={self.lamv:.3g} , cr {self.curva[-4,1]:.1%}
SLC: T={self.Tc:.3f} anni , lambda={self.lamc:.3g} , cr {self.curva[-5,1]:.1%}

# PAM
PAM= {self.PAM:.3%}
{Classifica.__str__(self)}
'''

class ClassificaISV(Classifica):
    soglie=r_[100,80,60,45,30,15.]/100
    strpath='Classe IS-V: {classe}, IS-V={val:.3%}'
    def test(self,v):
        return self.val>=v
    
