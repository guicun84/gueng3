import sqlite3,re,sys
from xml.etree import ElementTree as ET
fidb='prezzario_2021_00.sqlite'
tree=ET.parse('Prezzario.xml')
root=tree.getroot()
#ricerca namespace
ns={'t':re.search('({.*})',root.tag).group(1)}

contenuto=root.find('{t}Contenuto'.format(**ns))
articoli=contenuto.findall('{t}Articolo'.format(**ns))

if 'db' in list(locals().keys()):
	db.close()
	import os
	os.remove(fidb)


def cerca_inserisci(view,up,code,descrizione):
	#controllo esistenza:
	if up is None: q2='up is Null'
	else: q2='up=:up'

	query='select id from {view} where code =:code and {q2}'.format(**locals())
	ide=db.execute(query,locals()).fetchone()
	if ide:return ide[0]
	else:
		db.execute('insert into heap values (Null,:up,:code,:descrizione)',locals())
	ide=db.execute(query,locals()).fetchone()
	if ide:return ide[0]
	else:
		raise IOError('qualcosa non ha funzionato \n{}'.format(locals()))



with sqlite3.connect(fidb) as db:
	db.executescript('''
	drop table if exists heap;
	drop table if exists dettagli;
	drop table if exists dbinfo;
	drop view if exists tipi;
	drop view if exists capitoli;
	drop view if exists voci;
	drop view if exists articolih;
	drop view if exists articoli;


	create table heap (id integer primary key autoincrement,
		up integer, --nodo precedente
		code text,
		descrizione text);

	create index heap_id_up on heap(id,up);
	create index heap_up_id on heap(up,id);
	create index heap_code on heap(code);

	create table dettagli (id integer primary key autoincrement,
		descrizione_ text,
		um text,
		prezzo real,
		manodopera real,
		sicurezza real,
		foreign key (id) references heap(id));

	create index dettagli_id on dettagli(id);
	create index dettagli_desc on dettagli(descrizione_);

	create table dbinfo(var text primary key ,val );
	
	create view tipi as select *,code as fullcode from heap where up is NULL;

	create view if not exists capitoli as select heap.*
		, tipi.fullcode ||'.'|| heap.code as fullcode
		from heap
		inner join tipi on heap.up=tipi.id;

	create view voci as select heap.*
		, capitoli.fullcode ||'.'|| heap.code as fullcode
		from heap
		inner join capitoli on heap.up=capitoli.id;

	create view articolih as select heap.* 
		, voci.fullcode ||'.'|| heap.code as fullcode
		from heap
		inner join voci on heap.up=voci.id;

	create view articoli as 
		select h.*,d.descrizione_,d.um,d.prezzo,d.manodopera,d.sicurezza,dl.descrizione_long as descrizione_long
		from articolih h inner join dettagli d on h.id=d.id
		inner join 
		(
			select
				a.id
				,v.descrizione || ", " || a.descrizione as descrizione_long 
			from
			voci v inner join articolih a on a.up=v.id
		) dl on dl.id=h.id;
		
		
	''')
	db.commit()
	print(db.execute('select code from voci').fetchall())

	#recupero dati del database:
	intestazione=root.find('{t}intestazione'.format(**ns))
	dettagli=intestazione.find('{t}dettaglio'.format(**ns))
	regione=re.search('Regione (.*)',dettagli.get('area')).group(1)
	code=('{}'+''.join([i for i in regione if i.lower() not in list('aeiou')][:2])).upper()
	anno=dettagli.get('anno')
	code=code.format(anno[-2:])
	anno=int(anno)
	print(code,regione,anno)
	db.execute('insert into dbinfo values (?,?)',('code',code))
	db.execute('insert into dbinfo values (?,?)',('regione',regione))
	db.execute('insert into dbinfo values (?,?)',('anno',anno))



	for n,a in enumerate(articoli):
		#recupero dati da xml
		tc,cc,vc,ac=a.get('codice').split('.')
		tipo=a.find('{t}tipo'.format(**ns)).text
		capitolo=a.find('{t}capitolo'.format(**ns)).text
		voce=a.find('{t}voce'.format(**ns)).text
		articolo=a.find('{t}articolo'.format(**ns)).text
		descrizione_=a.find('{t}descrizionebreve'.format(**ns)).text
		um=a.find('{t}um'.format(**ns)).text
		prezzo=a.find('{t}prezzo'.format(**ns)).get('valore')
		manodopera=a.find('{t}mo'.format(**ns)).text
		sicurezza=a.find('{t}sicurezza'.format(**ns)).text

		#controllo esistenza voci superiori ,inserisco se necessario e recupero indici database
		it=cerca_inserisci('tipi',None,tc,tipo)
		ic=cerca_inserisci('capitoli',it,cc,capitolo)
		iv=cerca_inserisci('voci',ic,vc,voce)
		#aggiungo articolo alla lista
		ia=cerca_inserisci('articolih',iv,ac,articolo)
		db.execute('insert into dettagli values (:ia,:descrizione_,:um,:prezzo,:manodopera,:sicurezza)',locals())
		if not n%100:
			sys.stdout.write('\r{}'.format(n))
			sys.stdout.flush()
sys.stdout.write('\r{}'.format(n))
sys.stdout.flush()
		
db.commit()
