#! /usr/bin/python3
#-*-coding:latin-*-
import tkinter as tk
from tkinter import ttk
import os,re
import sqlite3
from copy import deepcopy as cp

dr=os.path.dirname(__file__)
#import gueng
#dr0=os.path.dirname(gueng.__file__)
#print(dr,dr0)
#dr=os.path.join(dr0,'prezzari','2021')

class Prezzario(tk.Tk):
	dbname=os.path.join(dr,'prezzario_2021_00.sqlite')
	print('dbname',dbname)
	db=sqlite3.connect(dbname)
	print(dbname)
	
	def __init__(self):
		tk.Tk.__init__(self)
		self.wm_title(os.path.basename(self.dbname))
		self.geometry('{}x{}'.format(800,700))
		#frame superiore dell'element tree
		liframe=tk.Frame()
		scroll_et=tk.Scrollbar(liframe)
		scroll_et.pack(side='right',fill='y')
		self.et=ttk.Treeview(liframe,yscrollcommand=scroll_et.set)
		scroll_et.config(command=self.et.yview)
		self.et.pack(expand=True,fill='both')
		self.et['columns']=['code','id']
		self.et.column('#0',width=550)
		self.et.column('code',anchor='e',width=150)
		self.et.column('id',anchor='e',width=50)
		liframe.pack(side='top',fill='both',expand=True)
		#frame inferiore dei dati
		dataframe=tk.Frame(self,height=10)
		#frame inferiore sinistro dei dati brevi
		df2=tk.Frame(dataframe)
		tk.Label(df2,text='Codice').pack()
		self.code=tk.StringVar()
		tk.Entry(df2,textvariable=self.code).pack()
		tk.Label(df2,text='unit�').pack()
		self.unit=tk.StringVar()
		tk.Entry(df2,textvariable=self.unit).pack()
		tk.Label(df2,text='prezzo').pack()
		self.prezzo=tk.StringVar()
		tk.Entry(df2,textvariable=self.prezzo).pack()
		tk.Label(df2,text='manodopera').pack()
		self.mo=tk.StringVar()
		tk.Entry(df2,textvariable=self.mo).pack()
		tk.Label(df2,text='sicurezza').pack()
		self.sic=tk.StringVar()
		tk.Entry(df2,textvariable=self.sic).pack()
		df2.pack(side='left')
		#chiudo pacchetto inf-sinistro dei dati
		self.desc=tk.Text(dataframe,height=15,width=65)
		self.desc.pack(side='left',fill='x',expand=True)
		#frame di ricerca:
		frr=tk.Frame(dataframe,width=150)
		tk.Label(frr,text='Ricerca',width=50).pack(fill='x',expand=True)
		self.ricerca=tk.Entry(frr)
		self.ricerca.pack(fill='x',expand=True)
		self.ricercaBtn=tk.Button(frr,text='Ricerca')
		self.ricercaBtn.pack(fill='x',expand=True)
		frr.pack(side='left',expand=True,fill='x')
		#fine frame ricerca
		dataframe.pack(side='bottom')
		#chiudo pacchetto dei dati inferiore

		self.et.bind('<Double-Button-1>',self.onClickEt)
		self.ricercaBtn.bind('<Button-1>',self.onBtnRicerca)
		self.ricerca.bind('<Return>',self.onBtnRicerca)
		self.riempiEt()

		#ricerco eventuale codice identificativo del prezzario:
		if self.db.execute('select 1 from sqlite_master where type="table" and name="dbinfo"').fetchone():
			code=self.db.execute('select val from dbinfo where var="code"').fetchone()
			if code:self.dbcode=code[0]+'.'
			else: self.dbcode=''

		
		
	def riempiEt(self,st=''):
		if st =='':
			if hasattr(self,'struttura'):
				for n,i in enumerate(self.struttura):
					self.et.move(i[0],i[1],n)
				return

			tipo='select * from tipi'
			capitolo='select * from capitoli where up=?'
			voce='select * from voci where up=?'
			articolo='select * from articoli where up=?'
			op=False
		elif type(st)==str:
			self.db.executescript('''
				drop view if exists tipi_;
				drop view if exists capitoli_;
				drop view if exists voci_;
				drop table if exists articoli_;''')
			self.db.execute(''' create temporary table articoli_ as select a.* from articoli a
				inner join voci v on v.id=a.up 
				where v.descrizione||','||a.descrizione like ?''',('%{}%'.format(st),))
			self.db.executescript('''
				create temporary view voci_ as select * from voci where id in(select distinct up from articoli_);
				create temporary view capitoli_ as select * from capitoli where id in(select distinct up from voci_);
				create temporary view tipi_ as select * from tipi where id in(select distinct up from capitoli_);''')
			tipo='select * from tipi_'
			capitolo='select * from capitoli_ where up=?'
			voce='select * from voci_ where up=?'
			articolo='select * from articoli_ where up=?'
			op=True
		elif type(st)==int:
			self.db.executescript('''
				drop view if exists tipi_;
				drop view if exists capitoli_;
				drop view if exists voci_;
				drop table if exists articoli_;''')
			self.db.execute(' create temporary table articoli_ as select * from articoli where id= ?',(st,))
			self.db.executescript('''
				create temporary view voci_ as select * from voci where id in(select distinct up from articoli_);
				create temporary view capitoli_ as select * from capitoli where id in(select distinct up from voci_);
				create temporary view tipi_ as select * from tipi where id in(select distinct up from capitoli_);''')
			tipo='select * from tipi_'
			capitolo='select * from capitoli_ where up=?'
			voce='select * from voci_ where up=?'
			articolo='select * from articoli_ where up=?'
			op=True

		n=0
		for t in self.db.execute(tipo):
			tipo_=self.et.insert('',n,text=t[3],values=(t[2],''),open=op)
			n+=1
			for c in self.db.execute(capitolo,(t[0],)):
				capitolo_=self.et.insert(tipo_,n,text=c[3],values=('.'.join((t[2],c[2])),''),open=op)
				n+=1
				for v in self.db.execute(voce,(c[0],)):
					voce_=self.et.insert(capitolo_,n,text=v[3],values=('.'.join((t[2],c[2],v[2])),''),open=op)
					n+=1
					for a in self.db.execute(articolo,(v[0],)):
						co='.'.join((t[2],c[2],v[2],a[2]))
						ide=a[0]
						articolo_=self.et.insert(voce_,n,text=a[3],values=(co,ide),open=op)
						n+=1

		if not hasattr(self,'struttura'):
			self.struttura=[]
			for t in self.et.get_children():
				self.struttura.append((t,''))
				for c in self.et.get_children(t):
					self.struttura.append((c,t))
					for v in self.et.get_children(c):
						self.struttura.append((v,c))
						for a in self.et.get_children(v):
							self.struttura.append((a,v))
			self.nodibase=[i[0] for i in self.struttura]
			
	def onBtnRicerca(self,ev):
		ch= self.et.get_children()
#		print ch
#		print self.et.get_children(ch[0])
		if type(ev)==str:st=ev
		else:st=self.ricerca.get()
		if st=='--ripristina':
			for t in self.et.get_children():
				for c in self.et.get_children(t):
					for v in self.et.get_children(c):
						for a in self.et.get_children(v):
							self.et.delete(a)
						self.et.delete(v)
					self.et.delete(c)
				self.et.delete(t)
			del self.struttura
			self.riempiEt('')
			return
			
		if re.match('.{2}\..{3}\..{3}\..{3}',st):
			print ('inizio ricerca codice')
			n=''
			st=st.upper()
			for t in self.et.get_children():
				for c in self.et.get_children(t):
					for v in self.et.get_children(c):
						for a in self.et.get_children(v):
							art=self.et.item(a)['values']
							if st==art[0]:
								n=int(art[1])
								break
			st=n
		for t in self.et.get_children():
			for c in self.et.get_children(t):
				for v in self.et.get_children(c):
					for a in self.et.get_children(v):
						if a in self.nodibase:self.et.detach(a)
						else: self.et.delete(a)
					if v in self.nodibase:self.et.detach(v)
					else: self.et.delete(v)
				if c in self.nodibase:self.et.detach(c)
				else: self.et.delete(c)
			if t in self.nodibase:self.et.detach(t)
			else: self.et.delete(t)
		self.riempiEt(st)

	def onClickEt(self,elem):
		try:
			curitem=self.et.item(self.et.focus())
			if not curitem['values'][1]:
	#			print curitem['text'].encode('latin')
				self.code.set(self.dbcode+curitem['values'][0])
				self.desc.delete('1.0','end')
				s=(curitem['text']).replace('\n',' ')
				self.desc.insert('1.0',s)
				self.unit.set('')
				self.prezzo.set('')
				self.mo.set('')
				self.sic.set('')
			else:
#				dati=self.db.execute('select * from articoli where id=?',(curitem['values'][1],)).fetchone()
#				d2=self.db.execute('select * from voci where id=?',(dati[1],)).fetchone()
#				self.code.set(self.dbcode+curitem['values'][0])
#				self.desc.delete('1.0','end')
#				if d2[3][-1] in ', ': k=d2[3][:-1]
#				else: k=d2[3]
#				s=u','.join((k,dati[3])).replace('\n',' ')
				dati=self.db.execute('select fullcode,um,prezzo,manodopera,sicurezza,descrizione_long from articoli where id=?',(curitem['values'][1],)).fetchone()
				print(dati)
				self.code.set(dati[0])
				self.unit.set(dati[1])
				self.prezzo.set(dati[2])
				self.mo.set(dati[3])
				self.sic.set(dati[4])
				self.desc.delete('1.0','end')
				self.desc.insert('1.0',dati[5])
		except Exception as e:
			print(e)
			self.desc.delete('1.0','end')
			self.desc.insert('1.0','{}'.format(e))
			
if __name__=='__main__':
	ap=Prezzario()
	ap.mainloop()
