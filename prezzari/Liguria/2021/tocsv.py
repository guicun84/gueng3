import pandas as pa
from sqlite3 import connect

db=connect('prezzario_2021_00.sqlite')
dat=pa.read_sql('''
select fullcode as codice,
descrizione_long as descrizione,
um,prezzo,manodopera,sicurezza from articoli''',db)

import re
def um(st):	
	k=re.search('\((.*)\)',st)
	if k:
		return k.group(1)
	return ''

dat['um_']=dat.apply(lambda x:um(x.um),1)
dat.to_csv('prezzario_20201.csv')

