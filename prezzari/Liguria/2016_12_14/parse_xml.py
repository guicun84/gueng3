import sqlite3,re
from xml.etree import ElementTree as ET

tree=ET.parse('PrezzarioXML.xml')
root=tree.getroot()
#ricerca namespace
ns={'t':re.search('({.*})',root.tag).group(1)}

contenuto=root.find('{t}Contenuto'.format(**ns))
articoli=contenuto.findall('{t}Articolo'.format(**ns))

def fun(view,up,code,descrizione):
	#controllo esistenza:
	ide=db.execute('select id from {view} where code =:code'.format(**locals()),locals()).fetchone()
	if ide:return ide[0]
	else:
		db.execute('insert into heap(Null,:up,:code,:descrizione')
	ide=db.execute('select id from {view} where code =:code'.format(**locals()),locals()).fetchone()
	if ide:return ide[0]
	else:
		raise IOError('qualcosa non ha funzionato')



with sqlite3.connect('prezzario_2016_bis.sqlite') as db:
	db.executescript('''
	drop table if exists  voci;
	drop table if exists  articoli;

	create table heap (id integer primary key autoincrement,
	up integer, --nodo precedente
	code text,
	descrizione text);

	create table dettagli (id integer primary key autoincrement,
	descrizione_ text,
	um text,
	prezzo real,
	manodopera real,
	sicurezza real,
	foreign key (id) references heap(id));
	
	create view if not exists tipo as select * from heap if up=Null;
	create view if not exists capitolo as select * from heap if up in (select id from tipo);
	create view if not exists voce as select * from heap if up in (select id from capitolo);
	create view if not exists articolo as select h.*,d.descrizione_,d.um,d.prezzo,d.manodopera,d.sicurezza from heap h inner join dettagli d on h.id=d.id where h.up in select * from voce;
	''')


	for a in articoli:
		#recupero dati da xml
		tc,cc,vc,ac=a.get('codice').split('.')
		tipo=a.find('{t}tipo'.format(**ns)).text
		capitolo=a.find('{t}capitolo'.format(**ns)).text
		voce=a.find('{t}voce'.format(**ns)).text
		articolo=a.find('{t}articolo'.format(**ns)).text
		descrizione_=a.find('{t}descrizionebreve'.format(**ns)).text
		um=a.find('{t}um'.format(**ns)).text
		prezzo=a.find('{t}prezzo'.format(**ns)).get('valore')
		manodopera=a.find('{t}mo'.format(**ns)).text
		sicurezza=a.find('{t}sicurezza'.format(**ns)).text

		#controllo esistenza voci superiori ,inserisco se necessario e recupero indici database
		it=fun('tipi',None,tc,tipo)
		ic=fun('capitoli',it,cc,capitolo)
		iv=fun('voci',ic,vc,voce)
		#aggiungo articolo alla lista
		ia=fun('articoli',iv,ac,articolo)
		db.execute('insert into dettagli values (Null,:ia,:descrizione_,:um,:prezzo,:manodopera,:sicurezza)',locals())

db.commit()
