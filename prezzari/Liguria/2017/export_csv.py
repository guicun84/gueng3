import sqlite3,pandas as pa,re,os


dbn='prezzario_2017_00.sqlite'
db=sqlite3.connect(dbn)

def cercaum(st):
	try:
		return re.search('\((.*?)\)',st).group(1)
	except:
		print(st)
		return st
db.create_function('cercaum',1,cercaum)


db.execute('''create temporary table art as select 
	db.val||'.'||t.code||'.'||c.code||'.'||v.code||'.'||a.code as codice,
	v.descrizione||', '||a.descrizione as descrizione,
	a.um as um,
	cercaum(a.um) as um_,
	a.prezzo as prezzo,
	a.manodopera as manodopera,
	a.sicurezza as sicurezza

from articoli a 
inner join voci v on a.up=v.id 
inner join capitoli c on v.up=c.id 
inner join tipi t on c.up=t.id
inner join (select val from dbinfo where var="code") as db on 1=1''')

print(db.execute('select codice from art limit 1').fetchone())

pa.read_sql('select * from art order by codice',db).to_csv(os.path.splitext(dbn)[0] + '.csv',encoding='utf-8')
