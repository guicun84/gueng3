import sqlite3,re,sys
from xml.etree import ElementTree as ET

tree=ET.parse('Prezzario_2018.xml')
root=tree.getroot()
#ricerca namespace
ns={'t':re.search('({.*})',root.tag).group(1)}

contenuto=root.find('{t}Contenuto'.format(**ns))
articoli=contenuto.findall('{t}Articolo'.format(**ns))

if 'db' in list(locals().keys()):
	db.close()
	import os
	os.remove('prezzario_2018_00.sqlite')


def fun(view,up,code,descrizione):
	#controllo esistenza:
	if up is None: q2='up is Null'
	else: q2='up=:up'

	query='select id from {view} where code =:code and {q2}'.format(**locals())
	ide=db.execute(query,locals()).fetchone()
	if ide:return ide[0]
	else:
		db.execute('insert into heap values (Null,:up,:code,:descrizione)',locals())
	ide=db.execute(query,locals()).fetchone()
	if ide:return ide[0]
	else:
		raise IOError('qualcosa non ha funzionato \n{}'.format(locals()))



with sqlite3.connect('prezzario_2018_00.sqlite') as db:
	db.executescript('''
	drop table if exists heap;
	drop table if exists dettagli;
	drop table if exists dbinfo;

	create table heap (id integer primary key autoincrement,
		up integer, --nodo precedente
		code text,
		descrizione text);

	create index heap_id_up on heap(id,up);
	create index heap_up_id on heap(up,id);
	create index heap_code on heap(code);

	create table dettagli (id integer primary key autoincrement,
		descrizione_ text,
		um text,
		prezzo real,
		manodopera real,
		sicurezza real,
		foreign key (id) references heap(id));

	create index dettagli_id on dettagli(id);
	create index dettagli_desc on dettagli(descrizione_);

	create table dbinfo(var text primary key ,val );
	
	create view if not exists tipi as select * from heap where up is NULL;
	create view if not exists capitoli as select * from heap where up in (select id from tipi);
	create view if not exists voci as select * from heap where up in (select id from capitoli);
	create view if not exists articolih as select * from heap where up in (select id from voci);
	create view if not exists articoli as 
		select h.*,d.descrizione_,d.um,d.prezzo,d.manodopera,d.sicurezza
		from articolih h inner join dettagli d on h.id=d.id;
	''')

	#recupero dati del database:
	intestazione=root.find('{t}intestazione'.format(**ns))
	dettagli=intestazione.find('{t}dettaglio'.format(**ns))
	regione=re.search('Regione (.*)',dettagli.get('area')).group(1)
	code=('{}'+''.join([i for i in regione if i.lower() not in list('aeiou')][:2])).upper()
	anno=dettagli.get('anno')
	code=code.format(anno[-2:])
	anno=int(anno)
	print(code,regione,anno)
	db.execute('insert into dbinfo values (?,?)',('code',code))
	db.execute('insert into dbinfo values (?,?)',('regione',regione))
	db.execute('insert into dbinfo values (?,?)',('anno',anno))



	for n,a in enumerate(articoli):
		#recupero dati da xml
		tc,cc,vc,ac=a.get('codice').split('.')
		tipo=a.find('{t}tipo'.format(**ns)).text
		capitolo=a.find('{t}capitolo'.format(**ns)).text
		voce=a.find('{t}voce'.format(**ns)).text
		articolo=a.find('{t}articolo'.format(**ns)).text
		descrizione_=a.find('{t}descrizionebreve'.format(**ns)).text
		um=a.find('{t}um'.format(**ns)).text
		prezzo=a.find('{t}prezzo'.format(**ns)).get('valore')
		manodopera=a.find('{t}mo'.format(**ns)).text
		sicurezza=a.find('{t}sicurezza'.format(**ns)).text

		#controllo esistenza voci superiori ,inserisco se necessario e recupero indici database
		it=fun('tipi',None,tc,tipo)
		ic=fun('capitoli',it,cc,capitolo)
		iv=fun('voci',ic,vc,voce)
		#aggiungo articolo alla lista
		ia=fun('articolih',iv,ac,articolo)
		db.execute('insert into dettagli values (:ia,:descrizione_,:um,:prezzo,:manodopera,:sicurezza)',locals())
		if not n%100:
			sys.stdout.write('\r{}'.format(n))
			sys.stdout.flush()
sys.stdout.write('\r{}'.format(n))
sys.stdout.flush()
		
db.commit()
