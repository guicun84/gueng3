import sqlite3,re
from gueng.utils import floatPath as fp

with sqlite3.connect('prezzario_Liguria_2016.sqlite') as db:

	if True:
		if db.execute('select 1 from sqlite_master where type="table" and name="famiglia"').fetchone():
			db.executescript('''
				drop table if exists famiglia;
				drop table if exists capitolo;
				drop table if exists voce;
				drop table if exists descrizione;
				drop view if exists elenco;''')

	db.executescript('''
		create table famiglia (code text primary key,
			descrizione text);
		create table capitolo (id integer primary key autoincrement,
			famiglia code text,
			code text,
			descrizione text);
		create table voce (id integer primary key autoincrement,
			capitolo integer,
			code text,
			descrizione text);
		create table descrizione (id integer primary key autoincrement,
			voce integer,
			code text, 
			descrizione text, 
			um text,
			prezzo real, 
			mo real ,
			sic real);
		
		create view elenco as select f.code||"."||c.code||"."||v.code||"."||d.code as code,
			c.famiglia as f,
			v.capitolo as c,
			d.voce as v,
			d.id as d,
			v.descrizione||","||d.descrizione as descrizione,
			d.prezzo,
			d.mo,
			d.sic
			from descrizione d 
			inner join voce v on d.voce=v.id 
			inner join capitolo c on v.capitolo=c.id 
			inner join famiglia f on c.famiglia=f.code'''
	)

	import pandas as pa
	dat=pa.read_csv('Prezzario.csv')
#	def fun(x):
#		if type(x) is not str: return x
#		x=x.replace('.','')
#		x=x.replace(',','.')
#		return x
#
#	dat.PREZZO=dat.apply(lambda x:fun(x.PREZZO),1)
#	dat.SICUREZZA=dat.apply(lambda x:fun(x.SICUREZZA),1)

	vpath=re.compile('(.*) \[([^\]]*)\]?',re.M)

	for i in dat.iterrows():
		j=i[1]
		if type(j.FAMIGLIA) ==str:
			cf,f=vpath.match(j.FAMIGLIA).groups()
			f=f.decode('latin')
			db.execute('insert into famiglia values(?,?)',(cf,f))
			continue
		
		if type(j.CAPITOLO) ==str:
			cc,c=vpath.match(j.CAPITOLO).groups()
			c=c.decode('latin')
			n=db.execute('insert into capitolo values(Null,?,?,?)',(cf,cc,c))
			cc=n.lastrowid
			continue

		if type(j.VOCE) ==str:
			cv,v=vpath.match(j.VOCE).groups()
			v=v.decode('latin')
			n=db.execute('insert into voce values(Null,?,?,?)',(cc,cv,v))
			cv=n.lastrowid
			continue

		if type(j.DESCRIZIONE) ==str:
			cd=j.CODICE.split('.')[-1]
			d=j.DESCRIZIONE.decode('latin')
			p=re.search('({})'.format(fp),j.PREZZO).group(1)
			mo=re.search('({})'.format(fp),j.MO).group(1)
			sic=re.search('({})'.format(fp),j.SIC).group(1)
			um=j.UM.decode('latin')
			db.execute('insert into descrizione values(Null,?,?,?,?,?,?,?)',(cv,cd,d,um,p,mo,sic))
			continue

	db.commit()
