import sqlite3,re
from gueng.utils import floatPath as fp

fi=open('liguria2016.txt','r')
codepath=re.compile('(.{2}\..{3}\..{6})')
valuepath=re.compile('.* ({fp}) \x80 ({fp}) ({fp}) \x80'.format(fp=fp))

with sqlite3.connect('prezzario_pdf.sqlite') as db:
	db.executescript('''
		drop table if exists descrizione;
		create table descrizione (ide integer primary key autoincrement,
			code text,
			prezzo real,
			mo real,
			sic real)
		''')
	for r in fi:
#		r=r.decode('latin')
		t=codepath.match(r)
		if t:
			co=t.groups()[0]
			co=co.split('.')
			co=[co[0],co[1],co[2][:3],co[2][3:]]
			co='.'.join(co)
		
		t=valuepath.match(r)
		if t:
			p,mo,si=t.groups()
			db.execute('insert into descrizione values(null,?,?,?,?)',(co,p,mo,si))
	db.commit()


