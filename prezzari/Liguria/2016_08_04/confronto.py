import sqlite3
db=sqlite3.connect('confronto.sqlite')

db.executescript('''
attach database 'prezzario_pdf.sqlite' as pdf;
attach database 'prezzario_Liguria_2016.sqlite' as csv;
create  table if not exists confronto as select * from csv.elenco d1 inner join pdf.descrizione d2 on d1.code=d2.code''')


print('prezzi differenti',db.execute('select count(*) from confronto where prezzo!=[prezzo:1]').fetchone())
#
print('mano dopera differente',db.execute('select count(*) from confronto where mo!=[mo:1]').fetchone())
#
print('sicurezza differente',db.execute('select count(*) from confronto where sic!=[sic:1]').fetchone())
#
print('differente',db.execute('select count(*) from confronto where sic!=[sic:1] or prezzo!=[prezzo:1] or mo!=[mo:1]').fetchone())
