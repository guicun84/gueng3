from lxml import etree as et
from io import StringIO as SIO

fi='Listino_2017_integrazioni.XML'
tmp=open(fi)
dat=SIO()
dat.write(tmp.read())
tmp.close()
dat.seek(0)
doc=et.parse(dat)
r=doc.getroot()

els=[i for i in set([i.tag for i in r.iter()])]
els.sort()
print(('tag diversi: {}'.format(len(els))))

tip='DGSuperCapitoliItem'
cap='DGCapitoliItem'
voce='DGSubCapitoliItem'
art='EPItem'
tipi=r.xpath('.//{}'.format(tip))
capitoli=r.xpath('.//{}'.format(cap))
voci=r.xpath('.//{}'.format(voce))
articoli=r.xpath('.//{}'.format(art))
print('''
tipi          : {}
capitoli      : {}
voci          : {}
articoli      : {}
'''.format(len(tipi),len(capitoli),len(voci),len(articoli)))

el= doc.xpath('.//{}[text()="{}"]'.format('Tariffa','1C.04.700.0100.a'))[0].getparent()
print(et.tostring(el,pretty_print=1))

