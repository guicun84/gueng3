#-*-coding:utf-8-*-

from pylab import *
from scipy import *
from gueng.profilario import prof
from collections import namedtuple
from numpy.random import get_state,set_state
import gueng.muratura as ver

class Maschio:
	chi=1.2 # il fattore di taglio per sezione rettangolare è 1.5 ma sul libro è indicato 1.2
	def __init__(self,t,l0,h,mat,vincolo='inc-inc',N0=0,mu_=None,raiseError=False):
		'''t=spessore
l0=lunghezza del setto al lordo dei telai laterali
h=altezza
mat=materiale, se muraturaDM ricrea in automatico il materiale da utilizzare
vincolo= inc-inc oppure inc-cer (incastri e cerneiere)
N0=sforzo normale in sommità al maschio
mu_=None coefficiente mu forzato, se none lo calcola in automatico'''
		self.mat_ver=None #materiale per le verifiche
		if not isinstance(mat,Muratura):
			self.mat_ver=mat
			mat=Muratura(mat.fm, mat.tau0, mat.E, mat.G, mat.rho/1e8, mat.FC, gamma_m=mat.gamma)
		vars(self).update(locals())
		self.lq=self.l0 #lunghezza per il cario superiore
		self.offset=[0,0] #serve per tenere traccia dei telai

	@property
	def l(self):
		return self.l0-sum(self.offset)
	
	@property
	def A(self):
		return self.t*self.l

	@property
	def J(self):
		return 1/12*self.t*self.l**3

	def K1(self):
		'rigidezza per doppio incastro'
		return self.mat.G*self.A*self.mat.E*self.l**2 / (self.h**3*self.mat.G+ self.chi*self.h*self.mat.E*self.l**2)
	
	def K2(self):
		'''rigidezza per incastro cerniera'''
		1/( self.h**3/(3*self.mat.E*self.J) + h*self.chi/(self.mat.G*self.A))
	
	@property
	def K(self):
		return {'inc-inc':self.K1,
	   		    'inc-cer':self.K2}[self.vincolo]()
	@property
	def N(self):
		'''aggiungo il peso del maschio a metà altezza'''
		return self.N0 + self.mat.rho *self.t*self.h/2*self.l
	
	@property
	def sigma0(self):
		'''tensione di compressione media a metà altezza del maschio'''
		return self.N/self.A

	@property
	def b(self):
		r=self.h/self.l
		return max([1,min([1.5,r])])

	@property
	def Vt(self): #resistenza a taglio
		return self.l*self.t*self.mat.ftd/self.b*sqrt(1+ self.sigma0/self.mat.ftd)
	
	@property 
	def Vpf(self): #resistenza per pressoflessione
		if self.sigma0>.85*self.mat.fd and self.raiseError:
			raise ValueError('{}>.85*{} non verificato a compressione semplice'.format(self.sigma0,self.mat.fd))
		elif self.sigma0>.85*self.mat.fd:
			if not hasattr(self,'VpfWarning'):
				print('{}>.85*{} non verificato a compressione semplice'.format(self.sigma0,self.mat.fd))
				self.VpfWarning=True
		Mu=self.sigma0*self.l**2*self.t/2 * (1-self.sigma0/(.85*self.mat.fd))
		return 2*Mu/self.h	

	@property
	def mu(self):
		#funzione per avere mu
		#mu|0=1.5
		#mu|1=2
		#(dmu/dphi)|1=.5
		#mu|oo=5
		r=self.h/self.l
		psi=self.Vt/self.Vpf
		if self.mu_ is not None:
			if psi<1 and not 1.5<=self.mu_<=2:
				print('controllare mu={} non in [1.5,2]'.format(self.mu))
			elif psi>=1 and not 2<=self.mu_<=5:
				print('controllare mu={} non in [2,5]'.format(self.mu))
			return self.mu_
		if psi<=1:
			if r<.5: return 1.5
			else: return 1.5+ .5*min((1,r-.5))
		else: 
			return 2+3*min((r-1,1))
#			return polyval((-1/486,1/162,40/81,365/243),psi)

	@property
	def Vu(self): #resistenza ultima
		return min([i for i in [self.Vpf,self.Vt] if i>0])

	@property
	def de(self): #deformazione elastica
		return self.Vu/self.K

	@property
	def du(self): #deformazione plastica
		return min((self.de*self.mu , self.h*4/1e3))

	@property
	def W(self): #Lavoro massimo dissipato
		x=r_[0,self.de,self.du]
		return trapz(list(map(self.V,x)),x/1e3) #V in N,x deve andare in m per avere Joule

	def __V(self,d):
		'''valuta il taglio dato uno spostamento imposto'''
		if d<self.de:
			return self.K*d
		elif self.de<=d<=self.du:
			return self.Vu
		else:
			return 0
	__V=vectorize(__V)
	def V(self,x):
		return self.__V(self,x)

	def __str__(self):
		return ' '.join(['{:9.4g}'.format(i) for i in [self.l , self.t , self.h ,self.b, self.lq,self.N,self.sigma0 , self.K , self.Vt, self.Vpf,self.Vu , self.mu , self.de , self.du]])

	def plot(self,x0=0,eng='plotly'):
		'''x0= punto di partenza maschio'''
		self.plotPlotly(x0)

	def plotPlotly(self,x0):
		color='rgb({},{},{})'.format(*randint(0,200,3))
		return dict(type='rect',x0=x0,x1=x0+self.l,y0=0,y1=self.h,fillcolor=color,line=dict(width=0))

	def plotGrafico(self,*args,**kargs):
		x=r_[0,self.de,self.du]
		plot(x,self.V(x),*args,**kargs)

	def verifica(self,qlin2=r_[0,0],isolato=False,ecp=.25,name=None,):
		'''qlin2=r_[dx,sx] carico lineare dx e sx dei solai soprastanti
		isolato: considera maschio isolato e non connesso in pianta a T
		ecp=[0,.5] default .25 eccentricità percentuale dei carichi del solaio rispetto allo spessore
		name=nome del maschio
		'''
		self.mur_verifica=ver.Maschio(self.l,self.h,self.t,
										self.mat_ver,isolato=isolato,name=name)
		N2=qlin2*self.lq
		self.mur_verifica.N1=self.N0-N2.sum()
		self.mur_verifica.N2=N2
		self.mur_verifica.d2=r_[1,1]*ecp*self.t
		return self.mur_verifica


		



class Parete:
	def __init__(self,h,qsup,*args):
		'''h=altezza compelssiva della parete
qsup=carico superiore uniformemente distribuito
*args elementi componenti la parete

la classe Parete automaticamente modifica i maschi per considerare l'ingombro dei telai'''
		self.h=h
		self.qsup=qsup
		self.objects=list(args)
		self.check=False

	
	def controlla(self,force=False):
		if not self.check or force:
			'''check preeliminari'''
			self.resistenti=[i for i in self.objects if isinstance(i,Maschio) or isinstance(i,Telaio)  or issubclass(i.__class__,Telaio)]
			self.controlla_offset()
			self.carichi_superiori()
			self.check=True
		
	def carichi_superiori(self):
		'aggiunge agli elementi portanti il carico dovuto a qsup considerando le larghezze dei maschi e delle eventuali apertura adiacenti'
		test=[ i.__class__.__name__ in ['Maschio'] for i in self.objects]
		for n,i in enumerate(test):
			if i:
				l=self.objects[n].l0
				if n>0:
					if not test[n-1]: l+=self.objects[n-1].l/2
				if n<len(test)-1:
					if not test[n+1]: l+=self.objects[n+1].l/2
				cur=self.objects[n]
				cur.lq=l
				cur.N0+=(self.qsup + cur.t*(self.h-cur.h)*cur.mat.rho)*l
	
	def controlla_offset(self):
		'assottiglia i maschi andando a togliere lo spessore dei telai adiacenti'
		test=[ i.__class__.__name__ in ['Maschio'] for i in self.objects]
		for n,i in enumerate(test):
			cur=self.objects[n]
			if n>0:
				ad=self.objects[n-1]
#				if ad.__class__.__name__ in ['Telaio']:
				if isinstance(ad,Telaio):
					cur.offset[0]=ad.h_*ad.filo[1]
			if n<len(test)-1:
				ad=self.objects[n+1]
#				if ad.__class__.__name__ in ['Telaio']:
				if isinstance(ad,Telaio):
					cur.offset[1]=ad.h_*ad.filo[0]


	@property
	def K(self):
		self.controlla()
		return sum([i.K for i  in self.resistenti])
	@property
	def de(self):
		self.controlla()
		return min([i.de for i in self.resistenti])
	@property
	def du(self):
		self.controlla()
		d= min([i.du for i in self.resistenti])
		d= min([d,self.h*.04 ])
		return d
	@property
	def Vu(self):
		self.controlla()
		d=self.du
		return sum([i.V(d) for i in self.resistenti])
	@property
	def Ve(self):
		d=self.de
		return sum([i.V(d) for i in self.resistenti])
	
	def __add__(self,val):
		self.objects.append(val)
		self.check=False
	
	def __sub__(self,val):
		if val in self.objects:
			del self.objects[self.objects.index(val)]			
		if val in self.resistenti:
			del self.resistenti[self.objects.index(val)]			
		self.check=False
	
	def __V(self,x):
		return sum([i.V(x) for i in self.resistenti])
	__V=vectorize(__V)
	def V(self,x):
		return self.__V(self,x)

	@property
	def W(self):
		de=[i.de for i in self.resistenti]
		du=self.du
		de=[i for i in de if i<=du]
		de.sort()
		x=r_[0.,de,du]
#		x=linspace(0,self.du,1000)
		return trapz(list(map(self.V,x)),x/1e3)

	@property
	def l(self):
		l=0
		for i in self.objects:
			if isinstance(i,Maschio):
				l+=i.l+sum(i.offset)
			else:
				l+=i.l
		return l

	def __str__(self):
		if not self.check: self.controlla()
		st=''
		if any([isinstance(i,Maschio) for i in self.objects]):
			st+='MASCHI\n' + ' '.join(i.rjust(9) for i in 'l t h b lq N sig0 K Vt Vpf Vu mu de du'.split()) +'\n'
			for i in self.objects:
				if isinstance(i,Maschio):
					st+='{}\n'.format(i)

		if any([isinstance(i,Telaio) for i in self.objects]):
			st+='TELAI\n' + ' '.join(i.rjust(9) for i in 'l h prof n vin. K Vu de du'.split()) +'\n'
			for i in self.objects:
				if isinstance(i,Telaio):
					st+='{}\n'.format(i)

		return st+'''
TOTALE PARETE
h= %.4g mm
K= %.6g Nmm
de= %.6g mm
Ve= %.6g N
d_u=%.6g mm
Vu=%.6g N'''%(self.h,self.K,self.de,self.Ve,self.du,self.Vu)
	
	def confronto(self,altro):
		'''altro=stato iniziale
		self=stato finale'''
		st=[' '.join([' '*12] + [i.rjust(15) for i in ['Iniziale','Finale','Differenza','Percentuale']])]
		st.append('Rigidezza  : {:10.4g} N/mm {:10.4g} N/mm {:10.4g} N/mm {:10.4g}%'.format(altro.K,self.K,self.K-altro.K,(self.K-altro.K)/altro.K*100))
		st.append('Resistenza : {:13.4g} N {:13.4g} N {:13.4g} N {:10.4g}%'.format(altro.Vu,self.Vu,self.Vu-altro.Vu,(self.Vu-altro.Vu)/altro.Vu*100))
		st.append('Spostamento: {:12.4g} mm {:12.4g} mm {:12.4g} mm {:10.4g}%'.format(altro.du,self.du,self.du-altro.du,(self.du-altro.du)/altro.du*100))
		st.append('Lavoro     : {:12.4g} J  {:12.4g} J  {:12.4g} J  {:10.4g}%'.format(altro.W,self.W,self.W-altro.W,(self.W-altro.W)/altro.W*100))
		st='\n'.join(st)
		return st

	def plotGrafico(self,plotAll=False,*args,**kargs):

		if plotAll==True:
			for i in self.resistenti:
				i.plotGrafico(*args)
		de=[i.de for i in self.resistenti]
		du=self.du
		de=[i for i in de if i<=du]
		de.sort()
		x=r_[0.,de,du]
#		x=linspace(0,self.du,100)
		plot(x,self.V(x),*args,**kargs)
		xlabel('spostamento [mm]')
		ylabel('Forza orizzontale [N]')
		grid()

	def plotPlotly(self):
		rndstate=get_state()
		seed(0)
		shapes=[]
		x=0
		for i in self.objects:
			if hasattr(i,'plotPlotly'):
				shapes.append(i.plotPlotly(x))
			x+=i.l
		fg=dict(data=[dict(type='scatter',x=[0,],y=[0,],mode='lines'),],layout=dict(shapes=shapes,yaxis=dict(scaleanchor='x')))
		set_state(rndstate)
		return fg

	def plot_confronto(self,ob):
		k=ob.K/self.K
		v=ob.Vu/self.Vu
		d=ob.du/self.du
		s=.4
		pos=r_[0,.4,1,1.4,2,2.4]
		val=[1,1/k,1,1/v,1,1/d]
		bar(pos,val,s)
		axhspan(*(1+r_[1,-1]*.15),color='g',alpha=.3)
		ticks='K_{init},K_{fin},F_{init},F_{fin},d_{init},d_{fin}'.split(',')
		ticks=['${}$'.format(i) for i in ticks]
		xticks(pos+s/2,ticks)
		grid()


class Telaio(object):
	def __init__(self,l,h,prof_,n,mat,filo=1,asse=False,vincolo='incinc',parent=None):
		'''l= larghezza interna del telaio
h=altezza del telaio
prof_= profilo utilizzato (classe con almeno h,tw,tf,Jy,Wy) 
n=numero complessivo di pofili utilizzati (somma di tutte e 2 le colonne)
mat=materiale utilizzato (se acciaio basta fyk, atrimenti classe con E ed fyd da usare nei calcoli)
filo= n | [n,n] da 1 a 0: indica quanto spessore del telaio va sottratto al maschio adiacente se lista indica il valore per lato dx e sx
asse=considera per il calcolo dell'altezza del telaio la linea d'asse del traverso oppure no [defalut False]
vinc= tipo di vincoli incastro incastro (incinc) o incastro cerniera(inccer)
parent=Parete di appartenenza'''
		self.l=l
		self._n=n
		self.h=h
		self.h_=0
		self.vincolo=vincolo
		self.parent=parent
		if not hasattr(filo,'len'):
			self.filo=[filo,filo]
		else:
			self.filo=filo
		self.asse=asse
		if type(mat) in [float,int]:
			self.mat=namedtuple('Acciaio_','E fyd')(E=210e3,fyd=mat)
		else:
			self.mat=mat
		if type(prof_)==str:
			self._prof=prof[prof_]
			self.J=self._prof.Jy
			self.h_=self._prof.h #altezza del profilo da sottrarre al maschio adiacente
		elif type(prof_) in [int,float]:
			self._prof=None
			self.J=_prof
		else:
			self._prof=prof_
			self.J=self._prof.Jy
			self.h_=self._prof.h
		self.J*=self._n
	
	@property
	def prof(self):
		return self._prof
	@prof.setter
	def prof(self,val):
		self._prof=val
		self.aggiorna()

	@property
	def n(self):
		return self._n
	@n.setter
	def n(self,n):
		self._n=n
		self.aggiorna()
	
	def aggiorna(self):
		if self.prof.__class__.__name__=='Profilo':
			self.J=self.prof.Jy*self._n
			self.h_=self.prof.h
			if self.parent is not None:
				self.parent.check=False

	@property
	def K(self):
		if self.prof is not None and self.asse:
			h=self.h+self.prof.h/2
		else:
			h=self.h
		return {'incinc':12*self.J*self.mat.E/(h)**3,
				'inccer':3*self.J*self.mat.E/(h)**3}[self.vincolo]


	@property
	def Vt(self): #resistenza a taglio del profilo
		return self.mat.fyd*self.prof.tw*(self.prof.h-2*self.prof.tf)/1.5*self._n

	@property
	def Vpf(self): #resistenza a flessione
		Mu=self.prof.Wy*self.mat.fyd
		if self.vincolo=='incinc':
			return Mu*2/(self.h+self.prof.h/2)*self._n
		else:
			return Mu/(self.h+self.prof.h/2)*self._n
	@property
	def Vu(self):
		return min(self.Vt,self.Vpf)
	@property
	def de(self):
		return self.Vu/self.K
	@property
	def du(self):
		return 3*self.de #il 3 è un valore arbitrario
	
	def __V(self,d):
		'''valuta il taglio dato uno spostamento imposto'''
		if d<self.de:
			return self.K*d
		elif self.de<=d<=self.du:
			return self.Vu
		else:
			return 0
	__V=vectorize(__V)
	def V(self,x):
		return self.__V(self,x)

	@property
	def W(self):
		x=r_[0,self.de,self.du]
		return trapz(list(map(self.V,x)),x/1e3)

	def plotGrafico(self,*args,**kargs):
		x=r_[0,self.de,self.du]
		plot(x,self.V(x),'--r',*args)

	def __str__(self):
		try:
			name=self.prof.name
		except:
			name=None
		if name==None:
			try: name=self.prof['name']
			except:pass
		if name==None: name=''
		return ' '.join(['{:9.4g}'.format(i) for i in (self.l , self.h)] +[name.rjust(9)] + ['{:9d}'.format(self.n)] + [self.vincolo.rjust(9)] + ['{:9.4g}'.format(i) for i in (self.K  ,  self.Vu  ,   self.de  ,  self.du)])


class TelaioNl(Telaio):
	'''questo telaio prevede un momento di incastro ad un estremo per simulare meglio la condizione in cui l'incastro alla base non sia sufficiente per trasmettere l'intero momento resistente del telaio'''
	def __init__(self,l,h,prof_,n,mat,M='incinc',filo=1,asse=False,parent=None):
		'''l= larghezza interna del telaio
		h=altezza del telaio
		prof_= profilo utilizzato 
		n=numero di profili utilizzati nei montanti del telaio
		mat=materiale utilizzato (se acciaio basta fyk)
		filo=n | [n,n] da 1 a 0: indica quanto spessore del telaio va sottratto al maschio adiacente se lista valore destro e sinistro
		asse=considera per il calcolo dell'altezza del telaio la linea d'asse del traverso oppure no [defalut False]
		M=momento totale di incastro massimo alla base di un piedritto Nmm
		parent=Parete di appartenenza'''
		Telaio.__init__(self,l,h,prof_,n,mat,filo,asse,'incinc',parent)
		self.M=M
		self.Mu=self.prof.Wy*self.mat.fyd*self.n/2/1.15
		if self.M>self.Mu: 
			raise ValueError('M incastro> Mu: usare telaio incastrato!!') #prossimamente lo tramuto in automatico ;)
		
		#salvo entrambe le rigidezze incastro incastro e cerniera incastro
		if self.prof is not None and self.asse:
			h=self.h+self.prof.h/2
		else:
			h=self.h
		self.K_={'incinc':12*self.J*self.mat.E/(h)**3,
				'inccer':3*self.J*self.mat.E/(h)**3}
		#calcolo deformazione massima per cui ottengo M:
		self.dsner=self.M*2/l/self.K_['incinc']*2# l'ultima moltiplicazione serve xche devo considerare metà rigidezza (il momento è su ogni piedritto)
		#calcolo deformazione elastica ultima
		self.Vsner=self.K_['incinc']*self.dsner
		if self.Vsner<self.Vu:
			dv=self.Vu-self.Vsner
			d2=dv/self.K_['inccer']
			self.__de=d2+self.dsner
		else:
			self.__de=self.Vu/self.K
	@property
	def de(self):
		return self.__de

	def __V(self,d):
		if d<=self.dsner: return self.K_['incinc']*d
		elif self.dsner<d<self.de:
			return self.Vsner+(d-self.dsner)*self.K_['inccer']
		else: return self.Vu
	__V=vectorize(__V)
	def V(self,x):
		return self.__V(self,x)

import gueng.CA as ca

class TelaioCl(Telaio):
	def __init__(self,l,h,d1,d2,As,Asw,s,n,mat,Ktype='nl',filo=1,asse=False,vincolo='incinc',parent=None):
		'''l= larghezza interna del telaio
h=altezza del telaio
d1,d2=dimensioni elemento in direzione parallela e perpendicolare al telaio
As=Area di acciaio teso 
Asw=area staffa
s=passo staffe
n=numero complessivo di elementi utilizzati (somma di tutte e 2 le colonne)
mat=materiale utilizzato cls
Ktype='nl'|'el' tipo di calcolo per k (non lineare o elastico)
filo= n | [n,n] da 1 a 0: indica quanto spessore del telaio va sottratto al maschio adiacente se lista valore dx e sx
asse=considera per il calcolo dell'altezza del telaio la linea d'asse del traverso oppure no [defalut False]
vinc= tipo di vincoli incastro incastro (incinc) o incastro cerniera(inccer)
parent=Parete di appartenenza'''
		self.l=l
		self._n=n
		self.h=h
		self.h_=d1
		self.d1,self.d2=d1,d2
		self.As=As
		self.Asw=Asw
		self.s=s
		self.Ktype=Ktype
		self.vincolo=vincolo
		self.parent=parent
		self.filo=filo
		self.asse=asse
		self.mat_=mat
		self.mat=namedtuple('Calcestruzzo_','E fyd')(mat.Ecm,mat.fcd)
		self.J=1/12*d2*d1**3
		self.J*=self._n
		self._prof=None
		self.prof=namedtuple('sezione','tf tw h Wy')(0,d2,d1,1/6*d2*d1**2)
		cl=ca.Sezione('rec',d2,d1,mat=mat)
		arm=ca.Armatura([ca.Barra(r_[0,-30.],As=As),ca.Barra(r_[0,-d1+30],As=As) ])
		self.sz=ca.SezioneCA((cl,arm))
	
	@property
	def Vpf(self):
		'taglio ultimo dovuto al momento flettente'
		if not hasattr(self,'_Vpf'):
			if not hasattr(self,'Mrd'):
				self.Mrd=self.sz.Mxrd()
				self.chiu=self.sz.chi
			if self.vincolo=="incinc":
				self._Vpf= self.Mrd*2/self.h*self._n
			else:
				self._Vpf= self.Mrd/self.h*self._n
		return self._Vpf

	@property
	def Vt(self):
		'resistenza a taglio della sezione'
		if not hasattr(self,'_Vt'):
			try:
				self._Vt=ca.Vrd(self.d2,self.d1,self.mat_.fcd,self.Asw,self.s).Vrd*self._n
			except:
				self._Vt=0
		return self._Vt

	def resetK(self):
		del self._K

	def Knl(self):
		if not hasattr(self,'_K'):
			if not hasattr(self,'Mrd'):
				self.Mrd=self.sz.Mxrd()
				self.chiu=self.sz.chi
			self.mat_.calcolaFcd('sle')
			self.sz.SLE(self.Mrd*.4)
			chi=self.sz.chi
			m1fes=self.sz.M1fes()
			rel=.4 #rapporto tra momento ultimo ed elastico
			if m1fes<self.Mrd*rel:
				chi1=self.sz.chi
				chi=m1fes/(self.Mrd*.4)*chi1 + (1-m1fes/(.4*self.Mrd))*chi
			self.mat_.calcolaFcd('slu')
			if self.vincolo=='incinc':
				chi=polyfit((0,self.h),r_[chi,-chi],1)
				phi=polyint(chi,1)
	#			phi[-1]=-polyval(phi,self.h)/self.h
				v=polyint(phi,1)
			else:
				chi=polyfit((0,self.h),r_[chi,0],1)
				self.mat_.calcolaFcd('slu')
				v=polyint(chi,2)
#			print(locals())
#			x=linspace(0,self.h)
#			plot(x,polyval(v,x))
			d=abs(polyval(v,self.h))
			self._K= self.Vpf*rel/d		
			self._du=d/chi.max()*self.chiu
		return self._K

	def Kel(self):
		if not hasattr(self,'_K'):
			if self.vincolo=='incinc':
				self._K= 12*self.mat_.Ecm/2*self.J/self.l**3*self._n
			else:
				self._K= 3*self.mat_.Ecm/2*self.J/self.l**3*self_n
			self._du=1.5*self.de
		return self._K

	@property
	def K(self):
		if self.Ktype=='el':
			return self.Kel()
		else:
			return self.Knl()

	@property
	def du(self):
		if not hasattr(self,'_du'):
			self.K
		return self._du
	
	@property
	def mu(self):
		#funzione per avere mu
		#mu|0=1.5
		#mu|1=2
		#(dmu/dphi)|1=.5
		#mu|oo=5
		r=self.h/self.l
		psi=self.Vt/self.Vpf
		self.mu_=None
		if self.mu_ is not None:
			if psi<1 and not 1.5<=self.mu_<=2:
				print('controllare mu={} non in [1.5,2]'.format(self.mu))
			elif psi>=1 and not 2<=self.mu_<=5:
				print('controllare mu={} non in [2,5]'.format(self.mu))
			return self.mu_
		if psi<=1:
			if r<.5: return 1.5
			else: return 1.5+ .5*min((1,r-.5))
		else: 
			return 2+3*min((r-1,1))
#			return polyval((-1/486,1/162,40/81,365/243),psi)



	

		
				



############################################################

class Apertura:
	def __init__(self,l):
		self.l=l
	
class Muratura:
	def __init__(self,fcm,t0,E,G,rho,FC=1,mu=1.5,gamma_m=1):
		'''fcm: resistenza media a compressione
t0= resistenza media taglio
E= modulo elastico medio
G= modulo elastico tangenziale medio
rho= peso specifico medio in N/mm^3
mu= fattore di duttilita 1.5 vecchia muratura, 2 muratura nuova o consolidata
FC= fattore di confidenza'''
		#nota gamma_m preimpostato a 1 perchè sul libro usa valori caratteristici e non di progetto.
		vars(self).update(locals())
		self.calcola()
	def calcola(self):
		self.fd=self.fcm/self.gamma_m/self.FC
		self.t0d=self.t0/self.gamma_m/self.FC
		self.fvk0=self.t0       #è una ripetizione forse pericolosa... ma mette un pò tutti d'accordo
		self.fvd0=self.fvk0/self.gamma_m/self.FC
		self.ftd=1.5*self.t0d
#		self.E/=self.FC
#		self.G/=self.FC

		def fvk(self,sigma):
			return self.fvk0+0.4*sigma

		def fvd(self,sigma):
			return self.fvk(sigma)/self.gamma_m/self.FC
		
		

if __name__=='__main__':
	mat=Muratura(4.16,.0845,1131,377,21/1e6)
	h=2200
	ht=3000
	s=400
	q=42.84
	p1=Parete(ht,q)
	p1+Apertura(940)
	p1+Maschio(s,5990,h,mat)

	p2=Parete(ht,q)
	p2+Maschio(s,3560,h,mat)
	p2+Apertura(1670)
	p2+Maschio(s,1700,h,mat)

	from copy import deepcopy
	p3=deepcopy(p2)
	p3.objects[1]=Telaio(1670,h,prof.ipe240,4,275)
	p3.check=False
	
	p4=Parete(ht,q)
	p4+Maschio(s,3560-240,h,mat)
	p4+Telaio(1670+240*2,h,prof.ipe240,4,275)
	p4.objects[-1].h_=0
	p4+Maschio(s,1700-240,h,mat)
	
	p5=Parete(ht,q)
	p5+Maschio(s,3560,h,mat)
	p5+Telaio(1670,h,prof.ipe240,4,275)
	p5.objects[-1].h_=0
	p5+Maschio(s,1700,h,mat)

	for n,i in enumerate(( p1,p2,p3,p4,p5)):
		print('\nparete',n+1)
		print(i)  

	print(p2.confronto(p1))
	print(p3.confronto(p1))
	print(p5.confronto(p1))
	print(p3.confronto(p5))


	tnl=TelaioNl(1500,2400,prof.upn240,4,275,M=prof.upn240.Wy*275/1.15,filo=1,asse=False,parent=None)
#	from gueng.utils import toString
#	print toString(vars(tnl))
	tnl.plotGrafico()

