import urllib.request, urllib.error, urllib.parse,yaml,os,json
configfile=os.path.join(os.path.dirname(__file__),"config.yaml")

def sendMessage(st):
	with open(configfile,'rb') as fi:
		dati=yaml.load(fi.read())
	resp=urllib.request.urlopen('https://api.telegram.org/bot{}/SendMessage?chat_id={}&text={}'.format(dati['token'],dati['chatid'],st)).read()
	resp=json.loads(resp)
	if not resp['ok'] :
		print(resp)

