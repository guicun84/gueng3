from scipy import *
from sympy import *

x,l,E,Jz,Jy,Jt,A=symbols('x l E Jz Jy Jt A')


a1,b1,c1,d1=symbols('a1 b1 c1 d1')
a2,b2,c2,d2=symbols('a2 b2 c2 d2')


vy=a1*x**3+b1*x**2+c1*x+d1
phiz=diff(vy,x)
Mz=diff(phiz,x)*E*Jz
Ty=diff(Mz,x)

vz=a2*x**3+b2*x**2+c2*x+d2
phiy=diff(vz,x)
My=diff(phiy,x)*E*Jy
Tz=diff(My,x)

e,f,g,h=symbols('e f g h')
vx=e*x+f
N=E*A*diff(vx,x)
phix=g*x+h
Mx=E*Jt*diff(phix,x)


M=zeros(6,6)
	
eq=r_[[vx.subs(x,0),
	vy.subs(x,0),
	vz.subs(x,0),
	phix.subs(x,0),
	phiy.subs(x,0),
	phiz.subs(x,0),
	vx.subs(x,l),
	vy.subs(x,l),
	vz.subs(x,l),
	phix.subs(x,l),
	phiy.subs(x,l),
	phiz.subs(x,l)]]


ze=r_[[0]*12]

print('deformazioni')
ris=[] 
for i in range(12):
	ze_=ze.copy()
	ze_[i]=1
	eq_=eq.copy()
	eq_-=ze_
	ris.append(solve(eq_,('a1,b1,c1,d1,a2,b2,c2,d2,e,f,g,h'.split(','))))

	print(ris[-1])

F_=[N,Ty,Tz,Mx,My,Mz]
F=[]
for i in F_:
	F.append(i.subs(x,0))
for i in F_:
	F.append(-i.subs(x,l))

print('rigidezze')
K=[]
for n in range(12): 
	K.append([ j.subs(ris[n]) for j in F])
K=Matrix(K)
K.print_nonzero()
