#-*-coding.latin-*-
class Counter:
	__instance=[]
	def __init__(self):
		self.n=0
		self.assegnati=[]
		Counter.__instance.append(self)
	
	def __next__(self):
		out=self.n
		if self.assegnati:
			while True:
				self.n+=1
				if self.n not in self.assegnati: break
		else:
			self.n+=1
		return out
	
	def assegna(self,n):
		if n not in self.assegnati:
			self.assegnati.append(n)
			return True
		else:
			raise ValueError('{} gia esistente'.format(n))
	
	def reset(self):
		self.n=0
		self.assegnati=[]

	@staticmethod
	def resetall():
		for i in Counter.__instance:
			i.reset()

	

