#-*-coding:latin-*-

from scipy import *
from .misc import Counter
from .nodo import Nodo
from .trave import Trave

from scipy.linalg import norm
from scipy.optimize import minimize,show_options,leastsq
from copy import deepcopy as cp

class TraveMaster:
	counter=Counter()
	def __init__(self,ni,nj,A=0,Jx=0,Jy=0,Jz=0,E=0,rot=0,ide=None,parent=None,part=7,**kargs):
		'''ni,nj nodo iniziale e finale, se ni=[ni...nj] e nj=None ni � gia la lista dei nodi;)
		A...E=caratteristiche sezione
		rot=rotazione su asse
		ide=numero trave
		parent=elemento genitore
		part=numero elementi in cui dividere la trave
	'''
#		if part<5: part=5
		if ide is None: self.ide=next(TraveMaster.counter)
		else:
			if  TraveMaster.counter.assegna(ide): self.ide=ide
		self.parent=parent
		if nj is None:
			self.i=ni[0]
			self.j=ni[-1]
		else:
			self.i=ni
			self.j=nj
		self.A=A
		self.Jx=Jx
		self.Jy=Jy
		self.Jz=Jz
		self.E=E
		self.rot=rot
		self.l=norm(self.j.co-self.i.co)
		if nj is None:
			self.pt=vstack((i.co for i in ni))
			self.nodi=ni
		else: 
			self.pt=c_[linspace(self.i.co[0],self.j.co[0],part)[1:-1],
				  linspace(self.i.co[1],self.j.co[1],part)[1:-1],
				  linspace(self.i.co[2],self.j.co[2],part)[1:-1]]
			self.nodi=[ni]
			self.nodi.extend([Nodo(*i) for i in self.pt])
			self.nodi.append(nj)

		self.parti=[Trave(i,j,A,Jx,Jy,Jz,E,rot,parent=self,**kargs) for i,j in zip(self.nodi[:-1],self.nodi[1:])]
		################ pi� lento################# :(
#		t0=Trave(self.nodi[0],self.nodi[1],A,Jx,Jy,Jz,E,rot,parent=self,**kargs)
#		self.parti=[cp(t0) for i in self.nodi[:-1]]
#		for p,i,j in zip(self.parti,self.nodi[:-1],self.nodi[1:]):
#			p.i=i
#			p.j=j
#			p.gdl=r_[	[i.gdl[k] for k in ('vx','vy','vz','rx','ry','rz')],
#									[j.gdl[k] for k in ('vx','vy','vz','rx','ry','rz')]]
#			p.parent=self
		self.pt=r_[[self.i.co],self.pt,[self.j.co]]
		self.xcur=apply_along_axis(norm,1,self.pt)
		self.kargs=kargs

	def aggiungi_carico(self,gdl,q):
		'q=carico distribuito'
		pt=self.pt-self.pt[0]
		xc=apply_along_axis(norm,1,pt)
		dl=diff(xc)
		dl=(r_[0,dl]+r_[dl,0])/2
		f=dl*q
		for n,i in enumerate(self.nodi):
			i.gdl[gdl].f+=f[n]

	def __d(self):
		'''riporto lo spostamento nel sistema locale'''
		mrot=self.parti[0].R[:3,:3]
		return r_[[dot(mrot,[i.gdl['vx'].val,i.gdl['vy'].val,i.gdl['vz'].val]) for i in self.nodi]]
	
	def __r(self):
		'''riporto le rotazioni nel sistema locale'''
		mrot=self.parti[0].R[:3,:3]
		return -r_[[dot(mrot,[i.gdl['rx'].val,i.gdl['ry'].val,i.gdl['rz'].val]) for i in self.nodi]]


	def d(self,direz,x=None):
		'''direz= vx vy vz direzione richiesta
		x= posizione del valore : 
			x
			-1 per nodo i 
			-2 per nodo j
			min, max per ricerca
		valuta spostamenti nel sistema locale'''
		ndir=['vx','vy','vz'].index(direz)
		if x is None: return self.__d()[:,ndir]
		if all(x==-1): return self.i.gdl[dire].val
		if all(x==-2): return self.j.gdl[dire].val
		#calcolo ascissa curvilinea
		pt=self.pt-self.pt[0]
		xc=apply_along_axis(norm,1,pt)
		pn=polyfit(xc,self.__d()[:,ndir],4)
		#interpolo lo spostamento considerando anche i valori della rotazioni agli estremi, cos� mi bastano 3 punti invcece di 5
		#rototraslo le rotazioni per averle nel sistema locale
		ndirrot={'vx':1, 'vy':2, 'vz':1}[direz]
		rot=self.__r()[:,ndirrot]
		d=self.__d()[:,ndir]
		x0=polyfit(xc,d,2)
#		print x0,d,rot
		if ndirrot==1:

			pn=polyfit(xc,self.__d()[:,ndir],1) #le deformazioni assiali possono essere solo lineari.
		else:
			def fun(pol,pt,targ):
				d=polyval(pol,pt)
				polr=polyder(pol)
				r=polyval(polr,pt)
				return r_[d,r]-targ
			pn=leastsq(fun,r_[0,0,x0],args=(xc,r_[d,rot]))[0]
#			print 'pn',pn
		if all(x=='min'): fun=lambda v:polyval(pn,v)
		elif all(x=='max'):fun=lambda v: -polyval(pn,v)
		else:return polyval(pn,x)
		out=minimize( fun,r_[xc.mean()],method='tnc',bounds=[[xc.min(),xc.max()]])
		if x=='max': out.fun*=-1
		return out

	def __M(self):
		return r_[[[i.Mxi,i.Myi,i.Mzi] for i in self.parti],c_[self.parti[-1].Mxj,self.parti[-1].Myj,self.parti[-1].Mzj]]
	
	def M(self,direz=None,x=None):
		if direz is None and x is None: return self.__M()
		ndir=['x','y','z'].index(direz)
		if x is None: return self.__M()[:,ndir]
		if all(x==-1): 
			e=self.parti[0]
			return [e.Mxi,e.Myi,e.Mzi][ndir]
		if all(x==-2): 
			e=self.parti[-1]
			return [e.Mxj,e.Myj,e.Mzj][ndir]
		#calcolo ascissa curvilinea
		pt=self.pt-self.pt[0]
		xc=apply_along_axis(norm,1,pt)
		pn=polyfit(xc,self.__M()[:,ndir],2)
		if all(x=='min'): fun=lambda v:polyval(pn,v)
		elif all(x=='max'):fun=lambda v: -polyval(pn,v)
		else:return polyval(pn,x)
		out=minimize( fun,r_[xc.mean()],method='tnc',bounds=[[xc.min(),xc.max()]])
		if x=='max': out.fun*=-1
		return out

	def __F(self,smoot=False):
		'''smooth=True per eseguire grafico taglio da derivata momento'''
		if smoot:
			M=self.__M()[:,[2,1]]
			pt=self.pt-self.pt[0]
			xc=apply_along_axis(norm,1,pt)
			Fy=polyfit(xc,M[:,0],2)
			Fy=polyder(Fy,1)
	#		Fy=gradient(M[:,0],xc[1])
	#		Fy=polyfit(xc[1:-1],Fy[1:-1],1)
			Fy=polyval(Fy,xc)
	#		Fz=gradient(M[:,1],xc[1])
	#		Fz=polyfit(xc[1:-1],Fz[1:-1],1)
			Fz=polyfit(xc,M[:,1],2)
			Fz=polyder(Fz,1)
			Fz=polyval(Fz,xc)
		else:
			Fy=r_[[i.Tyi for i in self.parti],self.parti[-1].Tyj]
			Fz=r_[[i.Tzi for i in self.parti],self.parti[-1].Tzj]
		Fx=r_[[i.Ni for i in self.parti],self.parti[-1].Nj]
		return c_[Fx,Fy,Fz]

	def F(self,direz=None,x=None,smooth=False):
		'''direz=asse su cui calcolare forza
		x=posizione di calcolo
		smooth=True per calcolo tagli da derivate momento'''
		if direz is None and x is None: return self.__F()
		ndir=['x','y','z'].index(direz)
		if x is None: return self.__F()[:,ndir]
		if all(x==-1): 
			return self.__F()[0,ndir]
		if all(x==-2): 
			return self.__F()[-1,ndir]
		#calcolo ascissa curvilinea
		pt=self.pt-self.pt[0]
		xc=apply_along_axis(norm,1,pt)
		pn=polyfit(xc,self.__F()[:,ndir],1)
		if all(x=='min'): fun=lambda v:polyval(pn,v)
		elif all(x=='max'):fun=lambda v: -polyval(pn,v)
		else:return polyval(pn,x)
		out=minimize( fun,r_[xc.mean()],method='tnc',bounds=[[xc.min(),xc.max()]],options={'maxiter':10000})
		if x=='max': out.fun*=-1
		return out

	def plot(self,key=None,**kargs):
		return vstack([i.plot(key,**kargs) for i in self.parti])

################################################################################

if __name__=='__main__':
	import unittest
	from gueng.FEM import *
	from gueng.profilario import prof
	l=2100
	q=3.
	pr=prof.ipe180
	dattr={'A':pr.A,'Jz':pr.Jy,'E':210e3}
	npart=5

	class test_Trave_master(unittest.TestCase):

		def test_carico_distribuito_appoggio_appoggio(self):
			Nodo.initType='d2'
			ni=Nodo(0,0)
			nj=Nodo(l,0)
			for i in 'vx vy'.split(): ni.gdl[i].d=0
			nj.gdl['vy'].d=0
			self.tr=TraveMaster(ni,nj,part=npart,**dattr)
			self.st=Struttura(self.tr.parti)
			self.tr.aggiungi_carico('vy',-q)
			self.st.risolviSparse()

			d=self.tr.d('vy','min')
			dref=5/384*l**4*q/dattr['E']/dattr['Jz']
			print()
			print('''valore calcolato:{}
valore di riferimento:{}
rapporto{}'''.format(d.fun,dref, abs(d.fun/dref)))
			print(d.x)
			figure(1)
			clf()
			subplot(3,1,1)
			pt=self.tr.pt.copy()
			pt-=pt[0]
			pt=apply_along_axis(norm,1,pt)
			d_=self.tr.d('vy')
			scatter(pt,d_,marker='x')
			x=linspace(0,l,1000)
			plot(x,self.tr.d('vy',x),'--r')
			grid('on')
			axhline(0,color='k')
			axhline( sign(d.fun)*dref)
#			self.assertAlmostEqual(abs(d.fun/dref),1,3)

			#controllo il momento
			print('momenti flettenti')
			print(self.tr.M('x'))
			print(self.tr.M('y'))
			print(self.tr.M('z'))
			Mref=q*l**2/8
#			M=abs(self.tr.M('z')).max()
			M=self.tr.M('z','max')
			print('''Mref ={}
M={}
rapporto={}'''.format(Mref,M.fun,Mref/M.fun))
			subplot(3,1,2)
			scatter(pt,self.tr.M('z'),marker='x')
			plot(x,self.tr.M('z',x),'--r')
			grid('on')
			ylim(r_[ylim()][-1::-1])
			axhline(0,color='k')
			axhline( sign(M.fun)*Mref)
			self.assertAlmostEqual(M.fun/Mref,1,3)

			#controllo il taglio
			print('taglio')
			print(self.tr.T('y'))
			print(self.tr.T('z'))
			Tref=q*l/2
			T=self.tr.T('y','max')
			print('''Tref ={}
T={}
rapporto={}'''.format(Tref,T.fun,Tref/T.fun))
			print(T)
			subplot(3,1,3)
			scatter(pt,self.tr.T('y'),marker='x')
			plot(x,self.tr.T('y',x),'--r')
			grid('on')
			axhline(0,color='k')
			axhline( sign(T.fun)*Tref)
			self.assertAlmostEqual(T.fun/Tref,1,3)

			#controllo le reazioni vincolari
			Ti=self.tr.i.gdl['vy'].equilibrio()
			Tj=self.tr.j.gdl['vy'].equilibrio()
			Tref=q*l/2
			print('''Ti={} , Tj={}
Tref= {}'''.format(Ti,Tj,Tref))			
			print(self.tr.T('y'))
			print(self.tr.T('z'))

		def test_carico_distribuito_incastro(self):
			Nodo.initType='d2'
			ni=Nodo(0,0)
			nj=Nodo(l,0)
			for i in 'vx vy rz'.split(): ni.gdl[i].d=0
			self.tr=TraveMaster(ni,nj,part=npart,**dattr)
			self.st=Struttura(self.tr.parti)
			self.tr.aggiungi_carico('vy',-q)
			self.st.risolviSparse()

			d=self.tr.d('vy','min')
			dref=1/8*l**4*q/dattr['E']/dattr['Jz']
			print()
			print('''valore calcolato:{}
valore di riferimento:{}
rapporto{}'''.format(d.fun,dref, abs(d.fun/dref)))
			print(d.x)
			figure(2)
			clf()
			subplot(3,1,1)
			pt=self.tr.pt.copy()
			pt-=pt[0]
			pt=apply_along_axis(norm,1,pt)
			d_=self.tr.d('vy')
			scatter(pt,d_,marker='x')
			x=linspace(0,l,1000)
			plot(x,self.tr.d('vy',x),'--r')
			grid('on')
			axhline(0,color='k')
			axhline( sign(d.fun)*dref)
#			self.assertAlmostEqual(abs(d.fun/dref),1,3)

			#controllo il momento
			print('momenti flettenti')
			print(self.tr.M('x'))
			print(self.tr.M('y'))
			print(self.tr.M('z'))
			Mref=q*l**2/2
#			M=abs(self.tr.M('z')).max()
			M=self.tr.M('z','min')
			print('''Mref ={}
M={}
rapporto={}'''.format(Mref,M.fun,Mref/M.fun))
			subplot(3,1,2)
			scatter(pt,self.tr.M('z'),marker='x')
			plot(x,self.tr.M('z',x),'--r')
			grid('on')
			ylim(r_[ylim()][-1::-1])
			axhline(0,color='k')
			axhline( sign(M.fun)*Mref)
			self.assertAlmostEqual(abs(M.fun/Mref),1,3)

			#controllo il taglio
			print('taglio')
			print(self.tr.T('y'))
			print(self.tr.T('z'))
			Tref=q*l
			T=self.tr.T('y','max')
			print('''Tref ={}
T={}
rapporto={}'''.format(Tref,T.fun,Tref/T.fun))
			print(T)
			subplot(3,1,3)
			scatter(pt,self.tr.T('y'),marker='x')
			plot(x,self.tr.T('y',x),'--r')
			grid('on')
			axhline(0,color='k')
			axhline( sign(T.fun)*Tref)
			self.assertAlmostEqual(T.fun/Tref,1,3)

			#controllo le reazioni vincolari
			Ti=self.tr.i.gdl['vy'].equilibrio()
			Tj=self.tr.j.gdl['vy'].equilibrio()
			Tref=q*l/2
			print('''Ti={} , Tj={}
Tref= {}'''.format(Ti,Tj,Tref))			
			print(self.tr.T('y'))
			print(self.tr.T('z'))

		def test_carico_concentrato_incastro(self):
			Nodo.initType='d2'
			ni=Nodo(0,0)
			nj=Nodo(l,0)
			for i in 'vx vy rz'.split(): ni.gdl[i].d=0
			self.tr=TraveMaster(ni,nj,part=npart,**dattr)
			self.st=Struttura(self.tr.parti)
			self.tr.j.gdl['vy'].f=-q
			self.st.risolviSparse()

			d=self.tr.d('vy','min')
			dref=1/3*l**3*q/dattr['E']/dattr['Jz']
			print()
			print('''valore calcolato:{}
valore di riferimento:{}
rapporto{}'''.format(d.fun,dref, abs(d.fun/dref)))
			print(d.x)
			figure(3)
			clf()
			subplot(3,1,1)
			pt=self.tr.pt.copy()
			pt-=pt[0]
			pt=apply_along_axis(norm,1,pt)
			d_=self.tr.d('vy')
			scatter(pt,d_,marker='x')
			x=linspace(0,l,1000)
			plot(x,self.tr.d('vy',x),'--r')
			grid('on')
			axhline(0,color='k')
			axhline( sign(d.fun)*dref)
#			self.assertAlmostEqual(abs(d.fun/dref),1,3)

			#controllo il momento
			print('momenti flettenti')
			print(self.tr.M('x'))
			print(self.tr.M('y'))
			print(self.tr.M('z'))
			Mref=q*l
#			M=abs(self.tr.M('z')).max()
			M=self.tr.M('z','min')
			print('''Mref ={}
M={}
rapporto={}'''.format(Mref,M.fun,Mref/M.fun))
			subplot(3,1,2)
			scatter(pt,self.tr.M('z'),marker='x')
			plot(x,self.tr.M('z',x),'--r')
			grid('on')
			ylim(r_[ylim()][-1::-1])
			axhline(0,color='k')
			axhline( sign(M.fun)*Mref)
			self.assertAlmostEqual(abs(M.fun/Mref),1,3)

			#controllo il taglio
			print('taglio')
			print(self.tr.T('y'))
			print(self.tr.T('z'))
			Tref=q
			T=self.tr.T('y','max')
			print('''Tref ={}
T={}
rapporto={}'''.format(Tref,T.fun,Tref/T.fun))
			print(T)
			subplot(3,1,3)
			scatter(pt,self.tr.T('y'),marker='x')
			plot(x,self.tr.T('y',x),'--r')
			grid('on')
			axhline(0,color='k')
			axhline( sign(T.fun)*Tref)
			self.assertAlmostEqual(T.fun/Tref,1,3)

			#controllo le reazioni vincolari
			Ti=self.tr.i.gdl['vy'].equilibrio()
			Tj=self.tr.j.gdl['vy'].equilibrio()
			Tref=q*l/2
			print('''Ti={} , Tj={}
Tref= {}'''.format(Ti,Tj,Tref))			
			print(self.tr.T('y'))
			print(self.tr.T('z'))


	unittest.main(verbosity=2)
