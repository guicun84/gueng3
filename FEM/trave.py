#-*-coding:latin-*-

from scipy import *
from scipy.linalg import norm,block_diag
from pylab import plot
from .misc import Counter
#si portrebbe migliorare passando una sezione e un materiale ....
#da definire gli svincoli... modifcando Kl e Kt....
class Trave:
	counter=Counter()
	def __init__(self,ni,nj,A=0,Jx=0,Jy=0,Jz=0,E=0,rot=0,lc=1,ide=None,parent=None,**kargs):
		'''per travi:
			x,y,z in modo che x=asse trave
			dot(y,Zglob)=0 #y su piano orizzontale [asse forte]
		per colonne:
			x parallelo asse Zglob
			y parallelo asse Yglob  
			z  parallelo -Xglob
		rot=rotazione in gradi
		**kargs:
			lc=luce di carico per carichi distribuiti'''

		if ide is None: self.ide=next(Trave.counter)
		else:
			if  Trave.counter.assegna(ide): self.ide=ide
		self.parent=parent
#		self.nodi=[ni,nj]
		self.i=ni
		self.j=nj
		self.A=A
		self.Jx=Jx
		self.Jy=Jy
		self.Jz=Jz
		self.E=E
		self.rot=rot
		if not 'G' in list(kargs.keys()):
			self.G=E/(2*(1-.2)) #ipotizzo ni pari a .2 per default
		else:
			self.G=kargs['G']
		self.gdl=[]
		for i in ni,nj:
			for j in 'vx','vy','vz','rx','ry','rz':
				self.gdl.append(i.gdl[j])
		self.azioni=[] #lista con carichi espressi come dict(fi=real,fy=real,gdl=[vx,vy,vz,rx,ry,rz],ref=[glob,loc]))
		self.aggiorna()
		self.kargs=kargs

	@property
	def nodi(self):
		return [self.i,self.j]

	def aggiorna(self):
		self.l=norm(self.j.co-self.i.co)
		self.__K()
		self.orienta()
		self.Kg=dot(self.R.T,dot(self.Kl,self.R)) 

	def orienta(self):
		e1=(self.j.co-self.i.co)/self.l
		e2=cross(r_[0,0,1.],e1)
		if norm(e2)==0: e2=cross(e1,(1,0,0))
		e2/=norm(e2)
		e3=cross(e1,e2)
		e3/=norm(e3)
		R=r_[[e1,e2,e3]] #da globale a locale
		r=deg2rad(self.rot)
		Rrot=r_[[[1.,0,0],[0,cos(r),sin(r)],[0,-sin(r),cos(r)]]]
		R=dot(Rrot,R)
		self.R=block_diag(R,R,R,R)

	@property
	def R_(self):
		return self.R[:3,:3]


	def __K(self):
		A=self.A
		Jt=self.Jx
		Jy=self.Jy
		Jz=self.Jz
		E=self.E
		G=self.G
		l=self.l
		#v=(vx                , vy            , vz            , rx      , ry           , rz)          , [vi    , vy]
		self.Kl=array([[A*E/l , 0             , 0             , 0       , 0            , 0            , -A*E/l , 0             , 0             , 0       , 0            , 0]            ,
					[0        , 12*E*Jz/l**3  , 0             , 0       , 0            , 6*E*Jz/l**2  , 0      , -12*E*Jz/l**3 , 0             , 0       , 0            , 6*E*Jz/l**2]  ,
					[0        , 0             , 12*E*Jy/l**3  , 0       , 6*E*Jy/l**2  , 0            , 0      , 0             , -12*E*Jy/l**3 , 0       , 6*E*Jy/l**2  , 0]            ,
					[0        , 0             , 0             , G*Jt/l , 0            , 0            , 0      , 0             , 0             , -G*Jt/l  , 0            , 0]            ,
					[0        , 0             , 6*E*Jy/l**2   , 0       , 4*E*Jy/l     , 0            , 0      , 0             , -6*E*Jy/l**2  , 0       , 2*E*Jy/l     , 0]            ,
					[0        , 6*E*Jz/l**2   , 0             , 0       , 0            , 4*E*Jz/l     , 0      , -6*E*Jz/l**2  , 0             , 0       , 0            , 2*E*Jz/l]     ,
					[-A*E/l   , 0             , 0             , 0       , 0            , 0            , A*E/l  , 0             , 0             , 0       , 0            , 0]            ,
					[0        , -12*E*Jz/l**3 , 0             , 0       , 0            , -6*E*Jz/l**2 , 0      , 12*E*Jz/l**3  , 0             , 0       , 0            , -6*E*Jz/l**2] ,
					[0        , 0             , -12*E*Jy/l**3 , 0       , -6*E*Jy/l**2 , 0            , 0      , 0             , 12*E*Jy/l**3  , 0       , -6*E*Jy/l**2 , 0]            ,
					[0        , 0             , 0             , -G*Jt/l  , 0            , 0            , 0      , 0             , 0             , G*Jt/l , 0            , 0]            ,
					[0        , 0             , 6*E*Jy/l**2   , 0       , 2*E*Jy/l     , 0            , 0      , 0             , -6*E*Jy/l**2  , 0       , 4*E*Jy/l     , 0]            ,
					[0        , 6*E*Jz/l**2   , 0             , 0       , 0            , 2*E*Jz/l     , 0      , -6*E*Jz/l**2  , 0             , 0       , 0            , 4*E*Jz/l]]    , float64)
#
#da jasp ma nn funziona...
#		self.Kl=array([[A*E/l , 0             , 0             , 0       , 0            , 0            , -A*E/l , 0             , 0             , 0       , 0            , 0]            ,
#					[0        , 12*E*Jz/l**3  , 0             , 0       , 0            , -6*E*Jz/l**2  , 0      , -12*E*Jz/l**3 , 0             , 0       , 0            , 6*E*Jz/l**2]  ,
#					[0        , 0             , 12*E*Jy/l**3  , 0       , -6*E*Jy/l**2  , 0            , 0      , 0             , -12*E*Jy/l**3 , 0       , -6*E*Jy/l**2  , 0]            ,
#					[0        , 0             , 0             , G*Jt/l , 0            , 0            , 0      , 0             , 0             , -G*Jt/l  , 0            , 0]            ,
#					[0        , 0             , -6*E*Jy/l**2   , 0       , 4*E*Jy/l     , 0            , 0      , 0             , 6*E*Jy/l**2  , 0       , 2*E*Jy/l     , 0]            ,
#					[0        , -6*E*Jz/l**2   , 0             , 0       , 0            , 4*E*Jz/l     , 0      , -6*E*Jz/l**2  , 0             , 0       , 0            , 2*E*Jz/l]     ,
#					[-A*E/l   , 0             , 0             , 0       , 0            , 0            , A*E/l  , 0             , 0             , 0       , 0            , 0]            ,
#					[0        , -12*E*Jz/l**3 , 0             , 0       , 0            , -6*E*Jz/l**2 , 0      , 12*E*Jz/l**3  , 0             , 0       , 0            , 6*E*Jz/l**2] ,
#					[0        , 0             , -12*E*Jy/l**3 , 0       , 6*E*Jy/l**2 , 0            , 0      , 0             , 12*E*Jy/l**3  , 0       , 6*E*Jy/l**2 , 0]            ,
#					[0        , 0             , 0             , -G*Jt/l  , 0            , 0            , 0      , 0             , 0             , G*Jt/l , 0            , 0]            ,
#					[0        , 0             , -6*E*Jy/l**2   , 0       , 2*E*Jy/l     , 0            , 0      , 0             , 6*E*Jy/l**2  , 0       , 4*E*Jy/l     , 0]            ,
#					[0        , 6*E*Jz/l**2   , 0             , 0       , 0            , 2*E*Jz/l     , 0      , 6*E*Jz/l**2  , 0             , 0       , 0            , 4*E*Jz/l]]    , float64)
	def dg(self):
		'restituisce spostamenti nodali in sistema globale'
		vi=array([i.val for i in self.gdl[:6]],float64)
		vj=array([i.val for i in self.gdl[6:]],float64)
		return r_[vi,vj]

	def dl(self):
		'restituisce gli spostamenti nodali sul sistema locale'
		return dot(self.R,self.dg())

	def solLoc(self,v=None):
		'''restituisce le sollecitazioni nodali nel sistema locale
		   v=vettore spostamento, se None cerca gli spostamenti da solo'''
		'''NOTA: la direzione del taglio positivo � verso l'alto per ora, motivo per cui i tagli vengono di segno opposto de san venant aveva z lungo asse trave y verso basso e x uscente dal foglio...!!!'''
		if v is None:v=self.dl()

		vdir=array([-1,  -1, 1, -1, -1, -1,  1, 1,  -1,  1,  1,  1])
#		vdir=array([-1.,-1,-1,-1,-1,-1,1,1,1,1,1,1]) #per jasp ma non funziona
		return (dot(self.Kl,v) - self.sommaForzeLoc())*vdir

	def solGlob(self,v=None):
		'''restituisce le sollecitazioni nodali nel sistema globale
		   v=vettore spostamento, se None cerca gli spostamenti da solo'''
		if v is None:v=self.dg()
		return dot(self.Kg,v)

#	con correzione di segno
#	@property
#	def Ni(self): return -self.solLoc()[0]
#	@property
#	def Nj(self): return self.solLoc()[6]
#	@property
#	def Tyi(self): return self.solLoc()[1]
#	@property
#	def Tyj(self): return -self.solLoc()[7]
#	@property
#	def Tzi(self): return -self.solLoc()[2]
#	@property
#	def Tzj(self): return self.solLoc()[8]
#	@property
#	def Mxi(self): return -self.solLoc()[3]
#	@property
#	def Mxj(self): return self.solLoc()[9]
#	@property
#	def Myi(self): return -self.solLoc()[4]
#	@property
#	def Myj(self): return self.solLoc()[10]
#	@property
#	def Mzi(self): return -self.solLoc()[5]
#	@property
#	def Mzj(self): return self.solLoc()[11]

	#segni corretti a monte
	@property
	def Ni(self): return self.solLoc()[0]
	@property
	def Nj(self): return self.solLoc()[6]
	@property
	def Tyi(self): return self.solLoc()[1]
	@property
	def Tyj(self): return self.solLoc()[7]
	@property
	def Tzi(self): return self.solLoc()[2]
	@property
	def Tzj(self): return self.solLoc()[8]
	@property
	def Mxi(self): return self.solLoc()[3]
	@property
	def Mxj(self): return self.solLoc()[9]
	@property
	def Myi(self): return self.solLoc()[4]
	@property
	def Myj(self): return self.solLoc()[10]
	@property
	def Mzi(self): return self.solLoc()[5]
	@property
	def Mzj(self): return self.solLoc()[11]

	def plot(self,key=None,**kargs):
		
		if key is None:
			i=self.i.co
			j=self.j.co
			return r_[[i[:2],j[:2],r_[nan,nan]]]
#			else:plot([i[0],j[0]],[i[1],j[1]],*args)

		if key=='Mz':
			val=-r_[self.Mzi,self.Mzj]
		if key=='N':
			val=r_[self.Ni,self.Nj]
		if key=='Ty':
			val=r_[self.Tyi,self.Tyj]
		if key=='dy':
			val=self.dl()[[1,7]] 		#da completare la la serie quando avr� alle mani qualcosa per il 3D...
		if key=='d':
			val=self.dl()[[0,1,6,7]]
		#ricerco la scala:
		scala=1.
		if 'scala' in list(kargs.keys()): scala=kargs['scala']

		val*=scala
		if key not in ['dy','d']:
			val=r_[[[0,0],[0,val[0]],[self.l,val[1]],[self.l,0]]]
		elif key=='dy':
			val=r_[[[0,val[0]],[self.l,val[1]]]]
		elif key=='d':
			val=r_[[val[:2],r_[self.l,0]+val[2:]]]
		for n in range(len(val)):
			val[n]=dot(self.R[:2,:2].T,val[n])
		val+=self.i.co[:2]
		return r_[val,c_[nan,nan]]
#		else:plot(val[:,0],val[:,1],*args)

	def sommaForze(self):
		if hasattr(self,'lc'):
			lc=self.lc
		else:
			lc=1
		for az in self.azioni:
			if 'fj' not in list(az.keys()):
				az['fj']=az['fi']
			if 'rif' not in list(az.keys()):
				az['rif']='glob'
			if az['fi']*az['fj']>=0:
				
				a=polyfit((0,self.l),(az['fi'],az['fj']),1)
				Ftot=polyval(polyint(a),self.l)
				S=polyval(polyint(r_[a,0]),self.l)
				xg=S/Ftot
				fi=Ftot/self.l*(self.l-xg)
				fj=Ftot/self.l*xg
#				print '{Ftot:5.3f} {xg:5.3f} {fi:5.3f} {fj:5.3f}'.format(**locals())

			else:
				x0=self.l*az['fi']/(az['fi']-az['fj'])
				Ftot0=0.5*x0*az['fi']
				xg0=x0/3
				fi0=Ftot0/self.l*(self.l-xg0)
				fj0=Ftot0/self.l*xg0
#				print 'p1 {Ftot0:5.3f} {x0:5.3f} {xg0:5.3f} {fi0:5.3f} {fj0:5.3f}'.format(**locals())

				Ftot1=0.5*(self.l-x0)*az['fj']
				xg1=x0+2/3*(self.l-x0)
				fi1=Ftot1/self.l*(self.l-xg1)
				fj1=Ftot1/self.l*xg1
#				print 'p2 {Ftot1:5.3f} {x0:5.3f} {xg1:5.3f} {fi1:5.3f} {fj1:5.3f}'.format(**locals())
				Ftot=Ftot0+Ftot1
				xg=x0
				fi=fi0+fi1
				fj=fj0+fj1
#				print '{Ftot:5.3f} {xg:5.3f} {fi:5.3f} {fj:5.3f}'.format(**locals())
			v=dict( x=r_[1,0,0],
					y=r_[0,1,0],
					z=r_[0,0,1],
					)[az['gdl'][1]]
			if az['rif']=='loc':
				v=dot(self.R_.T,v)
			fi*=v
			fj*=v
			for n,i in enumerate(['x','y','z']):
				self.i.gdl[az['gdl'][0]+i].f+=fi[n]*lc
				self.j.gdl[az['gdl'][0]+i].f+=fj[n]*lc

	def sommaForzeLoc(self):
		'''somma le forze sui gradi di libert�'''
		return zeros(12)
#		if self.azioni:
#			fli={'vx':0,'vy':0,'vz':0,'rx':0,'ry':0,'rz':0}
#			flj={'vx':0,'vy':0,'vz':0,'rx':0,'ry':0,'rz':0}
#			v_={'x':r_[1,0,0],'y':r_[0,1,0],'z':r_[0,0,1]}
#			for p in [i for i in self.azioni if isinstance(i,AzioneTrave)]:
#				f=p.concentra()*self.l
#				for f_,n in zip(f,[fli,flj]):
#					f_=dot(self.R[:3,:3].T,v_[p.faccia[1]]*f_)
#					for n_,d in enumerate(('x','y','z')):
#						n[p.faccia[0]+d]+=f_[n_]
#			fli=r_[[fli[i] for i in ('vx','vy','vz','rx','ry','rz')]]
#			flj=r_[[flj[i] for i in ('vx','vy','vz','rx','ry','rz')]]
#			return r_[fli,flj]
#		else:
#			return zeros(12)
#
