from scipy import *
from scipy.linalg import norm
class trave:
	gld='dx dy rz'.split()
	def __init__(self,i,j,E,J):
		self.l=norm(j.co-i.co)
		#calcolo matrice di rotazione:
		e1=(j.co-i.co)/self.l
		e2=cross(r_[0,0,1],self.e1)
		if norm(e2)==0: e2=cross(r_[0,0,1],e1)
		e2/=norm(e2)
		e3=cross(e1,e2)
		e3/=norm(e3)
		self.R=r_[[e1,e2,e3]]
		self.K=	Matrix([
		[-6*E*J/l**2,  12*E*J/l**3, -6*E*J/l**2, -12*E*J/l**3],
		[   -4*E*J/l,   6*E*J/l**2,    -2*E*J/l,  -6*E*J/l**2],
		[ 6*E*J/l**2, -12*E*J/l**3,  6*E*J/l**2,  12*E*J/l**3],
		[ 6*E*J/l**2, -12*E*J/l**3,  6*E*J/l**2,  12*E*J/l**3]])
