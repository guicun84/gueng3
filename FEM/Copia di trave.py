#-*-coding:latin-*-

from scipy import *
from scipy.linalg import norm,block_diag
from pylab import plot
from .misc import Counter
#si portrebbe migliorare passando una sezione e un materiale ....
#da definire gli svincoli... modifcando Kl e Kt....
class Trave:
	counter=Counter()
	def __init__(self,ni,nj,A=0,Jx=0,Jy=0,Jz=0,E=0,ide=None,parent=None):
		if ide is None: self.ide=next(Trave.counter)
		else:
			if  Trave.counter.assegna(ide): self.ide=ide
		self.parent=parent
		self.nodi=[ni,nj]
		self.i=ni
		self.j=nj
		self.A=A
		self.Jx=Jx
		self.Jy=Jy
		self.Jz=Jz
		self.E=E
		self.l=norm(nj.co-ni.co)
		self.__K()
		self.orienta()
		self.Kg=dot(self.R.T,dot(self.Kl,self.R)) 
		self.gdl=[]
		for i in ni,nj:
			for j in 'vx','vy','vz','rx','ry','rz':
				self.gdl.append(i.gdl[j])
		self.azioni=[]

	def orienta(self):
		e1=(self.j.co-self.i.co)/self.l
		e2=cross(r_[0,0,1],e1)
		if norm(e2)==0: e2=cross(r_[0,0,1],e1)
		e2/=norm(e2)
		e3=cross(e1,e2)
		e3/=norm(e3)
		R=r_[[e1,e2,e3]] #da globale a locale
		self.R=block_diag(R,R,R,R)

	@property
	def R_(self):
		return self.R[:3,:3]


	def __K(self):
		A=self.A
		Jt=self.Jx
		Jy=self.Jy
		Jz=self.Jz
		E=self.E
		l=self.l
		#v=(vx,vy,vz,rx,ry,rz),[vi,vy]
		self.Kl=array([[-A*E/l , 0 , 0 , 0 , 0 , 0 , A*E/l , 0 , 0 , 0 , 0 , 0] , 
					[0 , 12*E*Jz/l**3 , 0 , 0 , 0 , -6*E*Jz/l**2 , 0 , -12*E*Jz/l**3 , 0 , 0 , 0 , -6*E*Jz/l**2] , 
					[0 , 0 , 12*E*Jy/l**3 , 0 , -6*E*Jy/l**2 , 0 , 0 , 0 , -12*E*Jy/l**3 , 0 , -6*E*Jy/l**2 , 0] , 
					[0 , 0 , 0 , -E*Jt/l , 0 , 0 , 0 , 0 , 0 , E*Jt/l , 0 , 0] , 
					[0 , 0 , 6*E*Jy/l**2 , 0 , -4*E*Jy/l , 0 , 0 , 0 , -6*E*Jy/l**2 , 0 , -2*E*Jy/l , 0] , 
					[0 , 6*E*Jz/l**2 , 0 , 0 , 0 , -4*E*Jz/l , 0 , -6*E*Jz/l**2 , 0 , 0 , 0 , -2*E*Jz/l] , 
					[A*E/l , 0 , 0 , 0 , 0 , 0 , -A*E/l , 0 , 0 , 0 , 0 , 0] , 
					[0 , -12*E*Jz/l**3 , 0 , 0 , 0 , 6*E*Jz/l**2 , 0 , 12*E*Jz/l**3 , 0 , 0 , 0 , 6*E*Jz/l**2] , 
					[0 , 0 , -12*E*Jy/l**3 , 0 , 6*E*Jy/l**2 , 0 , 0 , 0 , 12*E*Jy/l**3 , 0 , 6*E*Jy/l**2 , 0] , 
					[0 , 0 , 0 , E*Jt/l , 0 , 0 , 0 , 0 , 0 , -E*Jt/l , 0 , 0] , 
					[0 , 0 , 6*E*Jy/l**2 , 0 , -2*E*Jy/l , 0 , 0 , 0 , -6*E*Jy/l**2 , 0 , -4*E*Jy/l , 0] , 
					[0 , 6*E*Jz/l**2 , 0 , 0 , 0 , -2*E*Jz/l , 0 , -6*E*Jz/l**2 , 0 , 0 , 0 , -4*E*Jz/l]],float64)

	def dg(self):
		'restituisce spostamenti nodali in sistema globale'
		vi=array([self.i.gdl[i].val for i in ('vx','vy','vz','rx','ry','rz')],float64)
		vj=array([self.j.gdl[i].val for i in ('vx','vy','vz','rx','ry','rz')],float64)
		return r_[vi,vj]

	def dl(self):
		'restituisce gli spostamenti nodali sul sistema locale'
		return dot(self.R,self.dg())

	def solLoc(self,v=None):
		'''restituisce le sollecitazioni nodali nel sistema locale
		   v=vettore spostamento, se None cerca gli spostamenti da solo'''
		'''NOTA: la direzione del taglio positivo � verso l'alto per ora, motivo per cui i tagli vengono di segno opposto de san venant aveva z lungo asse trave y verso basso e x uscente dal foglio...!!!'''
		if v is None:v=self.dl()
		return dot(self.Kl,v) - self.sommaForzeLoc()

	def solGlob(self,v=None):
		'''restituisce le sollecitazioni nodali nel sistema globale
		   v=vettore spostamento, se None cerca gli spostamenti da solo'''
		if v is None:v=self.dg()
		return dot(self.Kg,v)

	@property
	def Ni(self): return self.solLoc()[0]
	@property
	def Nj(self): return -self.solLoc()[6]
	@property
	def Tyi(self): return -self.solLoc()[1]
	@property
	def Tyj(self): return self.solLoc()[7]
	@property
	def Tzi(self): return -self.solLoc()[2]
	@property
	def Tzj(self): return self.solLoc()[8]
	@property
	def Mxi(self): return -self.solLoc()[3]
	@property
	def Mxj(self): return self.solLoc()[9]
	@property
	def Myi(self): return -self.solLoc()[4]
	@property
	def Myj(self): return self.solLoc()[10]
	@property
	def Mzi(self): return -self.solLoc()[5]
	@property
	def Mzj(self): return self.solLoc()[11]

	def plot(self,key=None,**kargs):
		
		if key is None:
			i=self.i.co
			j=self.j.co
			return r_[[i[:2],j[:2],r_[nan,nan]]]
#			else:plot([i[0],j[0]],[i[1],j[1]],*args)

		if key=='Mz':
			val=-r_[self.Mzi,self.Mzj]
		if key=='N':
			val=r_[self.Ni,self.Nj]
		if key=='Ty':
			val=r_[self.Tyi,self.Tyj]
		if key=='dy':
			val=self.dl()[[1,7]] 		#da completare la la serie quando avr� alle mani qualcosa per il 3D...
		if key=='d':
			val=self.dl()[[0,1,6,7]]
		#ricerco la scala:
		scala=1
		if 'scala' in list(kargs.keys()): scala=kargs['scala']

		val*=scala
		if key not in ['dy','d']:
			val=r_[[[0,0],[0,val[0]],[self.l,val[1]],[self.l,0]]]
		elif key=='dy':
			val=r_[[[0,val[0]],[self.l,val[1]]]]
		elif key=='d':
			val=r_[[val[:2],r_[self.l,0]+val[2:]]]
		for n in range(len(val)):
			val[n]=dot(self.R[:2,:2].T,val[n])
		val+=self.i.co[:2]
		return r_[val,c_[nan,nan]]
#		else:plot(val[:,0],val[:,1],*args)

	def sommaForze(self):
		'''somma le forze sui gradi di libert�'''
		v_={'x':r_[1,0,0],'y':r_[0,1,0],'z':r_[0,0,1]}
		for p in [i for i in self.azioni if isinstance(i,AzioneTrave)]:
			f=p.concentra()*self.l
			for f_,n in zip(f,self.nodi):
				f_=dot(self.R[:3,:3].T,v_[p.faccia[1]]*f_)
				for n_,d in enumerate(('x','y','z')):
					n.gdl[p.faccia[0]+d].f+=f_[n_]
	
	def sommaForzeLoc(self):
		'''somma le forze sui gradi di libert�'''
		fli={'vx':0,'vy':0,'vz':0,'rx':0,'ry':0,'rz':0}
		flj={'vx':0,'vy':0,'vz':0,'rx':0,'ry':0,'rz':0}
		v_={'x':r_[1,0,0],'y':r_[0,1,0],'z':r_[0,0,1]}
		for p in [i for i in self.azioni if isinstance(i,AzioneTrave)]:
			f=p.concentra()*self.l
			for f_,n in zip(f,[fli,flj]):
				f_=dot(self.R[:3,:3].T,v_[p.faccia[1]]*f_)
				for n_,d in enumerate(('x','y','z')):
					n[p.faccia[0]+d]+=f_[n_]
		fli=r_[[fli[i] for i in ('vx','vy','vz','rx','ry','rz')]]
		flj=r_[[flj[i] for i in ('vx','vy','vz','rx','ry','rz')]]
		return r_[fli,flj]




				
class AzioneTrave:
	def __init__(self,fi,fj=None,faccia='vy',gamma=1,psi=1,ref='loc'):
		'''fi= pressione nodo i
		   fj=pressione nodo j, se fj==None: fj=fi
		   faccia=['z','y'] direzione su cui � applicata la pressone riferita al sistema ref
		   gamma=coefficietne gamma
		   psi=coefficiente psi
		   ref={'loc': riferimento locale, 'glob': riferimento globale}'''
		vars(self).update(locals())
		if self.fj==None:self.fj=fi
		#calcolo quoteparti sui nodi di estremo		
		if self.fi*self.fj>=0: #senza cambio di segno:
			A=(self.fi+self.fj)/2
			S=self.fi/2+(self.fj-self.fi)/3
			G=S/A
			self.p=r_[1-G,G] #percentuale di peso su i e J
			print('{A}| {S}| {G}| {self.p}'.format(**locals()))
		else: #ho il cambio di segno:
			x0=-self.fi/(self.fj-self.fi)
			G=r_[1/3*x0,x0+2/3*(1-x0)]
			A=r_[self.fi*x0,self.fj*(1-x0)]/2
			print(A)
			A/=abs(A).sum()
			self.p=r_[(1-G[0])*A[0]+(1-G[1])*A[1],G[0]*A[0]+G[1]*A[1]]
			print('{x0}|{G}|{A}|{self.p}'.format(**locals()))

	def A(self):
		if self.fi*self.fj>=0:
			A=(self.fi+self.fj)/2
		else:
			x0=-self.fi/(self.fj-self.fi)
			A=abs(r_[self.fi*x0,self.fj*(1-x0)]/2).sum()
		return A*self.gamma*self.psi

	def concentra(self):
		'''restitiusce i carichi da applicare sui nodi DA MOLTIPLICARE PER LA LUNGHEZZA DELLA TRAVE!!!'''
		return self.p*self.A()
