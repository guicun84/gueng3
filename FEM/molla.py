#-*- coding:latin -*-

from scipy import *
from .misc import Counter

class Molla:
	counter=Counter()	

class MollaV(Molla):
	def __init__(self,nodo,K,R=eye(3),mult=1,soglia=None,ide=None,parent=None,**kargs):
		'''nodo=nodo dove � applicata la molla
		   K= costante molla
		   R=matrice di rotazine da globale a locale
		   parent=struttura cui appartiene
		   mult=moltiplicatore per scalare K
		   soglia=valore massimo prima di plasticizzare'''

		if ide is None: self.ide=next(Molla.counter)
		else:
			if  Molla.counter.assegna(ide): self.ide=ide
		self.parent=parent
		self.K=K
		self.Kl_=array([[K,0,0],[0,0,0],[0,0,0]])
		self.R=R
		self.Kg_=dot(dot(R.T,self.Kl_),R)
		self.i=nodo
		self.nodi=array([nodo])
		self.cercaGdl()
		self.mult=mult
		self.soglia=soglia
		self.kargs=kargs

	@property
	def Kl(self):
		return self.Kl_*self.mult
	
	@property
	def Kg(self):
		return self.Kg_*self.mult
		
	def cercaGdl(self):
		self.gdl=[self.nodi[0].gdl[i] for i in ('vx','vy','vz')]

	def dg(self):
		'''resitutisce vettore spostamenti globali'''
		return array([self.nodi[0].gdl[i].val for i in ('vx','vy','vz')],float64)

	def dl(self):
		'''restituisce vettore spostamenti locali'''
		return dot(self.R,self.dg())[0]

	def Fl(self):
		'''restituisce forze locali'''
		return dot(self.Kl,self.dl())[0]
	
	def solGlob(self):
		M=self.R.T
		f=self.Fl()
		ff=r_[1,0,0.]*f
		out=dot(M,ff)
		return out
		

	def testStato(self,sig,m1=1,m2=0,step=.2,cut=.01):
		'''controlla il segno della forza e modifica il moltiplicatore interno.
		sig= segno della forza: 1>0 -1<0
		m1=moltiplicatore se segno concorde
		m2=moltiplicatore se segno discorde'''
		F=dot(self.dl(),self.Kl_)[0]
		if sign(F)==0: 
			return True
		if sign(F)!=sig:
			if self.mult==m2:
				return True
			else:
				self.mult*=step
				if self.mult<=m2+cut:
					self.mult=m2
				return False
		else:
			if self.mult==m1:
				return True
			else:
				self.mult/=step
				if self.mult>=(m1-cut):
					self.mult=m1
				return False

	def testPlastico(self,soglia_er=1e-3,inc=1.3,fullout=False):
		'''controlla che il valore della forza interna alla molla non superi la soglia impostata nella molla
		soglia_er=errore minimo relativo di correzione sotto il non si considera modificata la molla
		inc= fattore di incremento per modifica moltiplicatore interno della molla
		fullout= True false per avere tutti i valori di calcolo o solo il test'''
		F=dot(self.Kl_,self.dl())[0,0]
		if abs(F)<= self.soglia:
			test=True
			er=0
			m=1
		else:
			m=self.soglia/abs(F)
			er1=(self.mult-m)
			er=er1/m
			if abs(er)<soglia_er:
				test= True
			else: 
				self.mult=self.mult+(m-self.mult)*inc
				test=False
		if not fullout: return test
		else: return locals()
		
		
		

class MollaR(MollaV,Molla):
	def __init__(self,nodo,K,R=eye(3),mult=1,soglia=None,ide=None,parent=None,**kargs):
		MollaV.__init__(self,nodo,K,R,mult,soglia,ide,parent,**kargs)
	def cercaGdl(self):
		self.gdl=[self.nodi[0].gdl[i] for i in ('rx','ry','rz')]
	def dg(self):
		return array([self.nodi[0].gdl[i].val for i in ('rx','ry','rz')])

	
class MollaVx(MollaV,Molla):
	def __init__(self,nodo,K,mult=1,soglia=None,ide=None,parent=None,**kargs):
		self.K=K
		if ide is None: self.ide=next(Molla.counter)
		else:
			if  Molla.counter.assegna(ide): self.ide=ide
		self.parent=parent
		self.Kl_=array([[K]])
		self.R=eye(3)
		self.Kg_=self.Kl_
		self.mult=mult
		self.soglia=soglia
		self.nodi=array([nodo])
		self.i=nodo
		self.kargs=kargs
		self.cercaGdl()
		self.kargs=kargs
	def cercaGdl(self):
		self.gdl=[self.nodi[0].gdl['vx']]
#	def dg(self):
#		return r_[self.nodi[0].gdl['vx'].val]
	def solGlob(self):
		return r_[self.Fl()]

class MollaVy(MollaVx,Molla):
	def __init__(self,nodo,K,mult=1,soglia=None,ide=None,parent=None,**kargs):
		MollaVx.__init__(self,nodo,K,mult,soglia,ide,parent,**kargs)
		self.R=array([[0,1,0],[-1,0,0],[0,0,1]],float64)
	def cercaGdl(self):
		self.gdl=[self.nodi[0].gdl['vy']]
#	def dg(self):
#		return r_[self.nodi[0].gdl['vy'].val]
	def solGlob(self):
		return r_[self.Fl()]

class MollaVz(MollaVx,Molla):
	def __init__(self,nodo,K,mult=1,soglia=None,ide=None,parent=None,**kargs):
		MollaVx.__init__(self,nodo,K,mult,soglia,ide,parent,**kargs)
		self.R=array([[0,0,1],[0,1,0],[-1,0,0]],float64)
	def cercaGdl(self):
		self.gdl=[self.nodi[0].gdl['vz']]
#	def dg(self):
#		return r_[self.nodi[0].gdl['vz'].val]
	def solGlob(self):
		return r_[self.Fl()]

class MollaRx(MollaR,Molla):
	def __init__(self,nodo,K,mult=1,soglia=None,ide=None,parent=None,**kargs):
		self.Kl_=array([[K]])
		if ide is None: self.ide=next(Molla.counter)
		else:
			if  Molla.counter.assegna(ide): self.ide=ide
		self.R=eye(3)
		self.Kg_=self.Kl_
		self.nodi=array([nodo])
		self.mult=mult
		self.soglia=soglia
		self.kargs=kargs
		self.cercaGdl()
		self.kargs=kargs
	def cercaGdl(self):
		self.gdl=[self.nodi[0].gdl['rx']]
#	def dg(self):
#		return r_[self.nodi[0].gdl['rx'].val]
	def solGlob(self):
		return r_[self.Fl()]


class MollaRy(MollaRx,Molla):
	def __init__(self,nodo,K,mult=1,soglia=None,ide=None,parent=None,**kargs):
		MollaRx.__init__(self,nodo,K,mult,soglia,ide,parent,**kargs)
		self.R=array([[0,1,0],[-1,0,0],[0,0,1]],float64)
	def cercaGdl(self):
		self.gdl=[self.nodi[0].gdl['ry']]
#	def dg(self):
#		return r_[self.nodi[0].gdl['ry'].val]
	def solGlob(self):
		return r_[self.Fl()]

class MollaRz(MollaRx,Molla):
	def __init__(self,nodo,K,mult=1,soglia=None,ide=None,parent=None,**kargs):
		MollaRx.__init__(self,nodo,K,mult,soglia,ide,parent,**kargs)
		self.R=array([[0,0,1],[0,1,0],[-1,0,0]],float64)
	def cercaGdl(self):
		self.gdl=[self.nodi[0].gdl['rz']]
#	def dg(self):
#		return r_[self.nodi[0].gdl['rz'].val]
	def solGlob(self):
		return r_[self.Fl()]
