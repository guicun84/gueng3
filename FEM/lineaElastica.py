#-*-coding:latin-*-

from pylab import plot,subplot,grid,ylim,ylabel
from scipy import *
from scipy.linalg import solve
from scipy.integrate import quad
from scipy.optimize import minimize
from copy import deepcopy
from hashlib import sha256
import pickle as Pickle

class LineaElastica:
	def __init__(self,l,EJ,v0=None,p0=None,vl=None,pl=None):
		'''l=lunghezza linea elastica
		EJ=prodotto E*J'''
		vars(self).update(**locals())

	@property
	def sha(self):
			return sha256(Pickle.dumps([self.l,self.EJ,self.v0,self.p0,self.vl,self.pl])).digest()
	
	@staticmethod
	def row(p,l,n=6):
		'''calcola una riga per la matrice risolutiva:
			p=polinomio dei coefficienti da usare
			l=punto dove calcolare la matrice
			n=6 lunghezza totale della riga'''
		pp=p*(l**(arange(p.size,dtype=float)[-1::-1]))
		return r_[pp,zeros(n-pp.size)]

	def calcola(self,v0=None,p0=None,vl=None,pl=None,q0=None,ql=None,M0=None,Ml=None):
		'''calcola i parametri della linea elastica:
		v=spostamento
		p=rotazione phi
		q=carico distribuito
		M=momento flettente
		le desinenze 0 o l indicano che il valore � iniziale o finale'''
		if v0 is None:v0=self.v0
		if p0 is None:p0=self.p0
		if vl is None:vl=self.vl
		if pl is None:pl=self.pl

		if ql==None:ql=q0
		v=ones(6) #spostamento
		p=polyder(v) #rotazione
		m=polyder(p)*self.EJ #momento
		t=polyder(m) #taglio
		q=polyder(t) #carico

		row=LineaElastica.row
		l=self.l

		M=[]
		b=[]

		if v0 is not None:
			M.append(row(v,0))
			b.append(v0)
		else:
			M.append(row(t,0))
			b.append(0)

		if p0 is not None:
			M.append(row(p,0))
			b.append(p0)
		else:
			M.append(row(m,0))
			if M0 is not None: b.append(-M0)
			else: b.append(0)

		if vl is not None:
			M.append(row(v,l))
			b.append(vl)
		else:
			M.append(row(t,l))
			b.append(0)

		if pl is not None:
			M.append(row(p,l))
			b.append(pl)
		else:
			M.append(row(m,l))
			if Ml is not None: b.append(Ml)
			else: b.append(0)

		if q0 is not None:
			M.append(row(q,0))
			b.append(q0)

		if ql is not None:
			M.append(row(q,l))
			b.append(ql)

		if M0 is not None and p0 is not None:
			M.append(row(m,0))
			b.append(-M0)

		if Ml is not None and pl is not None:
			M.append(row(m,l))
			b.append(Ml)
			
		M=vstack(M).astype(float)
		b=array(b,dtype=float)
		self.lel=solve(M,b)
		return self.lel

	def __add__(self,other):
		if not isinstance(other,LineaElastica):
			raise TypeError('{} non � istanza di LineaElastica'.format(repr(oter)))
		if not self.sha==other.sha:
			raise ValueError('Linea elastica con paramentri non compatibili')
		out=deepcopy(self)
		out.lel+=other.lel
		return out

	def __iadd__(self,other):
		self=self+other
	
	def __mul__(self,m):
		out=deepcopy(self)
		out.lel*=m
		return out

	def __imul__(self,m):
		self=self*m

	def copy(self):
		return deepcopy(self)

	def valuta(self,c,x=None):
		'''calcolo di una determinata linea elastica
		c=grado di derivazione della linea a partire da v
		x=punto di calcolo, oppure max per massimo, min per minimo'''
		c='v phi M T q'.split().index(c)
		p=polyder(self.lel,c)
		if c>=2: p*=self.EJ
		if x is None:x=linspace(0,self.l)
		if x=='l':x=self.l
		if x =='max':
			out= minimize(lambda j:-polyval(p,j),r_[self.l/2.1],bounds=((0,self.l),))
			out.fun*=-1
			return out	
		elif x =='min':
			return minimize(lambda j:polyval(p,j),r_[self.l/2.1],bounds=((0,self.l),))
		else:
			return polyval(p,x)

	def v(self,x=None):
		'''calcolo spostamento 
		x=punti oppure max, min'''
		return self.valuta('v',x)
	def phi(self,x=None):
		'''calcolo rotazione 
		x=punti oppure max, min'''
		return self.valuta('phi',x)
	def M(self,x=None):
		'''calcolo Momento 
		x=punti oppure max, min Mm per momento medio assoluto, Meq momento equivalente per CNR1011'''
		if x=='Mm':
			M=lambda x:abs(self.M(x))
			return quad(M,0,self.l)[0]/self.l

		elif x=='Meq':
			MM=self.M('max').fun
			Mm=self.M('min').fun
			MM=max(abs(r_[MM,Mm]))
			return r_[MM,r_[.75*MM,self.M('Mm')].max()].min()
		else:
			return self.valuta('M',x)
	def T(self,x=None):
		'''calcolo Taglio 
		x=punti oppure max, min'''
		return self.valuta('T',x)
	def q(self,x=None):
		'''calcolo carico distribuito 
		x=punti oppure max, min'''
		return self.valuta('q',x)
	
	#utli per CNR
	def Meq(self):
		MM=self.M('max').fun
		Mm=self.M('min').fun
		MM=max(abs(r_[MM,Mm]))
		return r_[MM,r_[.75*MM,self.Mm].max()].min()
	def plt(self,d='v phi M T q',**kargs):
		d=d.split()
		p=self.lel
		x=linspace(0,self.l)
		for i,v in enumerate(d):
			subplot(len(d),1,i+1)
			plot(x,self.valuta(v),**kargs)
			if v in ('M','q'):ylim(sort(ylim())[-1::-1])
				
			ylabel(d[i])
			grid(1)


class Carichi:
	def __init__(self,carichi=[],parent=None):
		'''carichi= lista con elementi formati da_
			[x,F] posizione carico concentrato
			[xi,xf,qi,qf=None] carico distribuito
			x espresse come numero complesso= % lunghezza di parent
			parent=oggetto parene da cui cogliere la luce totale'''
		self.carichi=carichi
		self.parent=parent
	def elabora(self):
		cc=[i for i in self.carichi if len(i)==2]
		cd=[i for i in self.carichi if len(i)>2]
		pass

