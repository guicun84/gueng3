#-*- coding:latin-*-

from scipy import *
from scipy.linalg import block_diag,solve,eig,norm
import scipy.sparse as spr
from gueng.utils import TicToc

class Struttura:
	def __init__(self,elementi,nodi=None):
		self.elementi=elementi
		if type(self.elementi)!=ndarray:
			self.elementi=r_[elementi]
		if nodi is None:
			nodi=[]
			for i in self.elementi: nodi.extend(i.nodi)
			self.nodi=r_[[i for i in frozenset(nodi)]]
		for i in self.nodi: i.parent=self
		self.gdl=None
		self.K=None
		self.f=None
		self.M=None
		self.gdl=None
		self.f=None
		self.reset()
	
	def reset(self,K=False,f=False,gdl=False,M=False,nodi=False,elementi=False):
		'''K,f,gdl=flag: se true viene resettato
		False =non cancella nulla
		True :cancella i dati solo in Struttura
		2 : cancella i dati anche sui gradi di libertà
		K True|False matrice di rigidezza
		f True|False|2 vettore fonrzanti
		gdl True|false|2 gradi di libertà
		M True|False matrice di massa
		'''
		if K:self.K=None
		if f:
			self.f=None
			if self.gdl is not None and f==2:
				for i in self.gdl:
					i.f=0
		if gdl:
			if self.gdl is not None and gdl==2:
				for i in self.gdl: i.reset()
			self.gdl=None
		if M:
			self.M=None
		if nodi:
			for i in self.nodi:
				i.reset()
		if elementi:
			for i in self.elementi:
				i.aggiorna()
		
	def cercaGdl(self):
		ngdl=[len(i.gdl) for i in self.elementi]
		ngdltot=sum(ngdl)
		n=0
		gdl=zeros(ngdltot, object)
		for m,i in enumerate(ngdl):
			gdl[n:n+i]=self.elementi[m].gdl
			n+=i
#		gdl=r_[[i.gdl for i in self.elementi]].flatten()
		self.gdl=array([i for i in frozenset(gdl)])
		self.gdlUnlock=isnan(array([i.d for i in self.gdl],float))
		#dovrei ora scrivere la matrice per eseguire le somme di rigidezza sugli stessi gdl dovuti a differenti elementi
		n=len(self.gdl)
		m=len(gdl)
		self.sumMat=zeros((n,m),float64)
		idgdl=array([id(i) for i in gdl],int)
		sidgdl=array([id(i) for i in self.gdl],int)
		for n,i in enumerate(sidgdl):
			self.sumMat[n,:]=idgdl==i

	def assemblaForze(self):
		#forze dovute a spostamenti imposti:
		d=array([i.d for i in self.gdl],float64)
		d[isnan(d)]=0
		fd=dot(self.Ktot,d)
		#forze applicate ai nodi:
		f=r_[[i.f for i in self.gdl]]
		self.ftot=f-fd
		self.f=self.ftot[self.gdlUnlock]
		
	def assemblaRigidezza(self):
		K=block_diag(*[i.Kg for i in self.elementi])
		self.Ktot=dot(dot(self.sumMat,K),self.sumMat.T)
#		self.M=self.Mtot.copy()
		self.K=self.Ktot[self.gdlUnlock].T[self.gdlUnlock].T

	def assemblaMasse(self,gdl0,a=1,mmult=1e-6):
		'''gdl0=grado di libertà da utilizzare per considerare la massa
		a=accelerazione da applicare per passare da forza a massa
		'''
		gdl='vx vy vz'.split()
		gdl=[i for i in gdl if i!=gdl0]
		for n in self.nodi:
			for g in gdl:
				n.gdl[g].f=n.gdl[gdl0].f
		self.reset(K=False,gdl=False,M=True,f=True)
		self.assemblaForze()
		self.M=abs(block_diag(*self.f/a))
		temp=r_[[not(any((i!=0))) for i in self.M]].reshape(-1,1).astype(float)
		temp/=temp.sum()
		temp*=abs(self.f[self.f!=0]).min()*mmult/a
		temp=block_diag(*temp)
		self.M+=temp

	def risolvi(self):
		if self.gdl is None: self.cercaGdl()
		if self.K is None: self.assemblaRigidezza()
		if self.f is None: self.assemblaForze()
		self.v=solve(self.K,self.f)
		self.riportaSoluzione()

	def analisiModale(self,gdl0,a=1,scala=1):
		'''
		gdl0=grado di libertà da utilizzare per considerare la massa
		a=accelerazione da applicare per passare da forza a massa
		scala=1 da modificare in base alle unità adottate nel modello
			se SI scala=1
			se N kg mm scala=1e3
			'''
		if self.gdl is None: self.cercaGdl()
		if self.K is None: self.assemblaRigidezza()
		if self.M is None: self.assemblaMasse(gdl0,a)
		lam,psi=eig(self.K,self.M)
		ordine=argsort(lam)
		lam=lam[ordine]
		psi=psi[:,ordine]
		omega=(lam*scala)**.5  #da capire xche il pi
		frq=omega/(2*pi) #per non dimenticare 2 giro completo=2*pi
		T=1/frq
		return dict(omega=omega,frq=frq,T=T,psi=psi)

	def riportaSoluzione(self,ris=None):
		if ris is None:
			for n,i in enumerate(self.v):
				self.gdl[self.gdlUnlock][n].val=i
			for i in self.gdl[~self.gdlUnlock]:
				i.val=i.d
			self.vtot=r_[[i.val for i in self.gdl]]
		else:
			for n,(i,j) in enumerate(ris):
				self.gdl[n].val=i
				self.gdl[n].f=j
			self.vtot=ris[:,0].copy()
			self.fot=ris[:,1].copy()
			self.v=self.vtot[self.gdlUnlock].copy()
			self.f=self.ftot[self.gdlUnlock].copy()

	def riportaModo(self,psi):
		p=zeros((len(self.gdl),2))
		n=0
		for nn,i in enumerate(self.gdlUnlock):
			if i:
				p[nn,0]=psi[n]
				n+=1
		self.riportaSoluzione(p)

		

	@property
	def stato(self):
		try:
			return c_[self.vtot,array(self.ftot.todense()).flatten()].copy()
		except:
			return c_[self.vtot,self.ftot].copy()
			


	############################################################	
	#ridifinizione per uso matrici sparse

	def cercaGdlSparse(self):
		ngdl=[len(i.gdl) for i in self.elementi]
		ngdltot=int(sum(ngdl))
		n=0
		gdl=zeros(ngdltot, object)
		for m,i in enumerate(ngdl):
			gdl[n:n+i]=self.elementi[m].gdl
			n+=i
#		gdl=r_[[i.gdl for i in self.elementi]].flatten()
		self.gdl=array([i for i in frozenset(gdl)])
		self.gdlUnlock=isnan(array([i.d for i in self.gdl],float)).nonzero()[0]
		#dovrei ora scrivere la matrice per eseguire le somme di rigidezza sugli stessi gdl dovuti a differenti elementi
		n=len(self.gdl)
		m=len(gdl)
#		self.sumMat=spr.csr_matrix((n,m),dtype=float64)
		self.sumMat=spr.lil_matrix((n,m),dtype=float64)
		idgdl=array([id(i) for i in gdl],int64)
		sidgdl=array([id(i) for i in self.gdl],int64)
		for n,i in enumerate(sidgdl):
			test=idgdl==i
			for j in test.nonzero()[0]:
				self.sumMat[n,j]=1
			
	def assemblaRigidezzaSparse(self):
#		M=spr.block_diag([i.Kg for i in self.elementi],'csr')
		K=spr.block_diag([i.Kg for i in self.elementi],'dia')
		self.Ktot=self.sumMat.dot(K.dot(self.sumMat.T))
#		self.M=self.Mtot.copy()
		self.K=self.Ktot[self.gdlUnlock].T[self.gdlUnlock].T

	def assemblaForzeSparse(self):
		#forze dovute a spostamenti imposti:
		d=array([i.d for i in self.gdl],float64)
		d[isnan(d)]=0
		d=spr.csr_matrix(d)
		fd=self.Ktot.dot(d.T)
		#forze applicate ai nodi:
		f=spr.csr_matrix(array([i.f for i in self.gdl],float64)).T
		self.ftot=f-fd
		self.f=self.ftot[self.gdlUnlock]

	def assemblaMasseSparse(self,gdl0,a=1,mmult=1e-6):
		'''gdl0=grado di libertà da utilizzare per considerare la massa
		a=accelerazione da applicare per passare da forza a massa
		'''
		gdl='vx vy vz'.split()
		gdl=[i for i in gdl if i!=gdl0]
		for n in self.nodi:
			for g in gdl:
				n.gdl[g].f=n.gdl[gdl0].f
		self.reset(K=False,gdl=False,M=True,f=True)
		self.assemblaForzeSparse()
		self.M=spr.block_diag(abs(self.f.todense().flatten()/a).reshape(-1,1),'dia')
		#tolgo parti nulle con escamotage....
		temp=r_[[not(any((i!=0).todense())) for i in self.M.tolil()]].reshape(-1,1).astype(float)
		temp/=temp.sum()
		temp*=abs(self.f[self.f!=0]).min()*mmult/a
		temp=spr.block_diag(temp,'dia')
		self.M+=temp

	
	def risolviSparse(self,permc_spec='NATURAL',use_umfpack=False):
		'''perm_spec e use_umf sono parametri della funzione spsolve, quelli di default nei test non mi chiudono la soluzione'''
		if self.gdl is None: self.cercaGdlSparse()
		if self.K is None: self.assemblaRigidezzaSparse()
		if self.f is None: self.assemblaForzeSparse()
		self.v=spr.linalg.spsolve(self.K,self.f,permc_spec,use_umfpack)
		self.riportaSoluzioneSparse()
		self.vtot=r_[[i.val for i in self.gdl]]

#
#	def riportaSoluzioneSparse(self):
#		for i in self.gdl: i.val=i.d
#		for n,i in enumerate(self.v):
#			self.gdl[self.gdlUnlock][n].val=i

	def analisiModaleSparse(self,gdl0,a=1,scala=1,n=None,which='LM',sigma=0,tol=1e-3,ncv=None):
		'''
		gdl0=grado di libertà da utilizzare per considerare la massa
		a=accelerazione da applicare per passare da forza a massa
		scala=1 da modificare in base alle unità adottate nel modello
			se SI scala=1
			se N kg mm scala=1e3
		n=numero di gdl da trovare se None li cerca tutti
			'''
		if self.gdl is None: self.cercaGdlSparse()
		if self.K is None: self.assemblaRigidezzaSparse()
		if self.M is None: self.assemblaMasseSparse(gdl0,a)
		if n is None:n=self.K.shape[0]-2
		print('dimensioni',self.M.shape)
		print('modi da cercare',n)

		lam,psi=spr.linalg.eigs(self.K,n,self.M,which=which,sigma=sigma,tol=tol,ncv=ncv)
		for i in range(psi.shape[1]):
			psi[:,i]/=norm(psi[:,i])
		ordine=argsort(lam)
		lam=lam[ordine]
		psi=psi[:,ordine]
		omega=(lam*scala)**.5/pi #da capire xche il pi
		frq=omega/(2*pi) #1giro=2pi
		T=1/frq
		frq=frq[ordine]
		return dict(omega=omega,frq=frq,T=T,psi=psi)

	def riportaSoluzioneSparse(self,ris=None):
		if ris is None:
			for i in self.gdl: i.val=i.d
			for n,i in enumerate(self.v):
				self.gdl[self.gdlUnlock][n].val=i
			self.vtot=r_[[i.val for i in self.gdl]]
		else:
			for i in self.gdl: i.val=i.d
			for n,(i,j) in enumerate(ris):
				self.gdl[n].val=i
				self.gdl[n].f=j
			self.vtot=ris[:,0].copy()
			self.ftot=ris[:,1].copy()
			self.v=self.vtot[self.gdlUnlock].copy()
			self.f=self.ftot[self.gdlUnlock].copy()

	def riportaModoSparse(self,psi):
		old_gdl=self.gdl
		self.gdl=None
		self.cercaGdl()
		p=zeros((len(self.gdl),2))
		n=0
		for nn,i in enumerate(self.gdlUnlock):
			if i:
				p[nn,0]=psi[n]
				n+=1
		self.riportaSoluzione(p)
		self.gdl=old_gdl
