#-*-coding:latin-*-

from scipy import *
from .misc import Counter
#da definire i vincoli tra gdl

class Gdl:
	'''gradi di libert� del problema'''
	counter=Counter()
	def __init__(self,d=None,f=0,ide=None,parent=None):
		'''d= spostamento impostato
		f= forza esterna applicata al nodo
		parent= nodo cui appartiene'''
		if ide is None: self.ide=next(Gdl.counter)
		else:
			if  Gdl.counter.assegna(ide): self.ide=ide
		self.d=d #valore impostato di spostamento
		self.f=f #forza applicata al nodo
		self.parent=parent
		self.val=None #spostamento del gdl da soluzione

	def forzeElementi(self):
		'''calcola le forze applicate derivanti dagli elementi cui fa capo il gdl'''
		#forze esterne applicate al nodo:
#		fe=self.f
		#recupero reazioni elementi
		#cerco gli elementi:
		e=self.parent.cerca_elementi()
		f=0
		for i in e:
			if self in i.gdl:
				f+=i.solGlob()[i.gdl.index(self)]
#		return f-fe
		return f
	
	def reset(self):
		self.val=None
		self.f=0

	def equilibrio(self):
		return self.forzeElementi()-self.f

	@property
	def label(self):
		for i,j in list(self.parent.gdl.items()):
			if j==self: return i
	
	@property
	def co(self):
		return self.parent.co
		

class Nodo:
	initType=None
	counter=Counter()
	def __init__(self,x,y,z=0,ide=None,parent=None):
		'''x,y,z=coordinate del nodo
		   parent=struttura [non elemento] in cui sono presenti'''
		if ide is None: self.ide=next(Nodo.counter)
		else:
			if  Nodo.counter.assegna(ide): self.ide=ide
		self.parent=parent
		self.co=r_[x,y,z].astype(float)
		self.co0=self.co.copy()
		self.gdl={} #preparo i gradi di libert�
		self.azioni=[]
		if Nodo.initType=='d2': self.initd2()
		if Nodo.initType=='d3': self.initd3()

	 
	def sommaForze():
		for f in [i for i in self.azioni if isinstance(i,AzioneNodo)]:
			self.gdl[f.direz].f+=f.val
	def initd3(self):
		 #preprara i gradi di libert� per sistema 3d
		for i in 'vx','vy','vz','rx','ry','rz':
			self.gdl[i]=Gdl(parent=self)

	def initd2(self):
		 #preprara i gradi di libert� per sistema 2d imponendo quelli fuori piano = 0
		for i in 'vx','vy','rz':
			self.gdl[i]=Gdl(parent=self)
		for i in 'vz','rx','ry':
			self.gdl[i]=Gdl(0,parent=self)

	def cerca_elementi(self):
		return [i for i in self.parent.elementi if self in i.nodi]

	def addGdl(self,label):
		self.gdl[label]=Gdl(parent=self)
		return self.gdl[label]

	def reset(self):
		self.co=self.co0.copy()


	#############################################################
	def deform(self,scala=1):
		d=r_[self.gdl['vx'].val,self.gdl['vy'].val,self.gdl['vz'].val].astype(float)
		d[isnan(d)]=0
		d*=scala
		return self.co+d


class AzioneNodo:
	def __init__(self,val,direz,gamma=1,psi=1):
		'''val= valore della forza
		   direz= [vx vy vz rx ry rz] grado di libert� che interessa'''
		self.val=val
		self.direz=direz

	def f(self):
		return self.val*self.gamma*self.psi

