import plotly
import plotly.graph_objs as go
import plotly.offline as pl
from .trave import Trave
from scipy import vstack,array,dot,apply_along_axis,ndarray,r_,where,isnan
from scipy.linalg import norm

def plot3d(val=None,scala=1,*strutture,**kargs):
	'''val= N,Vy,Vz,Mx,My,Mz o valori da plottare sugli elementi dati array(n,2)
	opzioni:
	returnFig True false per ritornare la figura senza plottare
	direz=array(3x1) direzione su cui forzare plot None sceglie da solo'''
	if 'direz' not in list(kargs.keys()): kargs['direz']=None
	data=[]
	layout=go.Layout(
					margin=dict(l=0,r=0,t=20,b=0),
					scene=dict(
						aspectmode="data",
						xaxis=dict(ticks='',showticklabels=True,showgrid=True,showbackground=False),
						yaxis=dict(ticks='',showticklabels=True,showgrid=True,showbackground=False),
						zaxis=dict(ticks='',showticklabels=True,showgrid=True,showbackground=False),
					),
					paper_bgcolor='silver',
	)
	if 'camera' in list(kargs.keys()):
		layout['scene']['camera']=kargs['camera']
	#variabili per tener traccia dei punti di max e min
	plim=[None,None]
	vlim=[None,None]

	def checklim(val,pts,fun):
		'''Funzione per controllare max e min
		val=valori da controllare
		pts=punti in cui sono registrati
		fun= max |min '''
		v=fun(val)
		p=pts[where(val==fun(val))[0][0]]
		if (fun==max and vlim[0] is None):
			vlim[0]=v
			plim[0]=p
		elif (fun==max and vlim[0]<v):
			vlim[0]=v
			plim[0]=p
		elif (fun==min and vlim[1] is None):
			vlim[1]=v
			plim[1]=p
		elif (fun==min and vlim[1]>= v):
			vlim[1]=v
			plim[1]=p

	def checklim2(val,pts):
		checklim(val,pts,max)
		checklim(val,pts,min)


	for nst,els in enumerate(strutture):
		if nst==0:sst=""
		else:sst=' %d'%nst
		trs=[i for i in els if isinstance(i,Trave)]
		x,y,z=[],[],[]
		x1,y1,z1,col=[],[],[],[]
		xn,yn,zn=[],[],[]
		text=[]

		def nz(x):
			r=x[x!=0]
			if r :return r
			return 0

		for ntr,t in enumerate(trs):
			pt=vstack((t.i.co,t.j.co))
			x.extend([pt[0,0],pt[1,0],None])
			y.extend([pt[0,1],pt[1,1],None])
			z.extend([pt[0,2],pt[1,2],None])
			if any([t.i.gdl[j].d is not None  for j in ['vx','vy','vz','rx','ry','rz']]):
				xn.append(t.i.co[0])
				yn.append(t.i.co[1])
				zn.append(t.i.co[2])
			if any([t.j.gdl[j].d is not None for j in ['vx','vy','vz','rx','ry','rz']]):
				xn.append(t.j.co[0])
				yn.append(t.j.co[1])
				zn.append(t.j.co[2])

			if type(val) ==str:
				if 'd' in val:
#					da=array([[n.gdl[gd].val for gd in ['vx','vy','vz']] for n in [t.i,t.j]]).reshape(2,3)
					da=array([t.gdl[i].val for i in [0,1,2,6,7,8]]).reshape(2,3)
					c=[]
					if 'x' in val:c=[1,2]
					if 'y' in val:c=[0,2]
					if 'z' in val:c=[0,1]
					for i in c:
						da[:,i]=0
					dat=da.copy().astype(float)
					if len(val)==1:
						da=apply_along_axis(norm,1,da)
					else:
						da=(apply_along_axis(nz,1,da)).flatten()
					text.extend(['%.3f'%i for i in da]+[''])
					dat*=scala
					dat=pt+dat
#					checklim(da,dat,max)
#					checklim(da,dat,min)
					checklim2(da,dat)

				elif val=='N':
					da=array([0,0,t.Ni,0,0,t.Nj]).reshape(2,3).astype(float)
					c=2
					if kargs['direz'] is not None:
						direz=kargs['direz']
						if direz[0]!=0: 
							da=da[:,[2,0,1]]
							c=0
						elif direz[1]!=0: 
							da=da[:,[0,2,1]]
							c=1
					dat=da.copy()
#					da=apply_along_axis(norm,1,da)/1e3
					da=dat[:,c]/1e3
					text.extend(['%.3f'%i for i in dat[:,c]/1e3]+[''])
					dat[0]=dot(t.R_.T,dat[0])
					dat[1]=dot(t.R_.T,dat[1])
					dat=pt+dat*scala
#					checklim(da,dat,max)
#					checklim(da,dat,min)
					checklim2(da,dat)


				if 'M' in val:
					if 'x' in val:
						da=array([0,0,t.Mxi,
								   0,0,t.Mxj]).reshape(2,3).astype(float)
						text.extend(['%.3f'%i for i in da[:,2]/1e6]+[''])
					elif 'y' in val:
						da=array([0,0,t.Myi,
								   0,0,t.Myj]).reshape(2,3).astype(float)
						text.extend(['%.3f'%i for i in da[:,2]/1e6]+[''])
					elif 'z' in val:
						da=array([0,t.Mzi,0,
								   0,t.Mzj,0]).reshape(2,3).astype(float)
						text.extend(['%.3f'%i for i in da[:,1]/1e6]+[''])
					dat=da.copy()
					da=(apply_along_axis( nz,1,da)/1e6).flatten()
					dat[0]=dot(t.R_.T,dat[0])
					dat[1]=dot(t.R_.T,dat[1])
					dat=pt-dat*scala
#					checklim(da,dat,max)
#					checklim(da,dat,min)
					checklim2(da,dat)

				if 'V' in val :
					if 'y' in val:
						da=array([0,t.Tyi,
								   0,0,t.Tyj,0]).reshape(2,3).astype(float)
						text.extend(['%.3f'%i for i in da[:,1]/1e3]+[''])
					elif 'z' in val:
						da=array([0,0,t.Tzi,
								   0,0,t.Tzj]).reshape(2,3).astype(float)
						text.extend(['%.3f'%i for i in da[:,2]/1e3]+[''])

					dat=da.copy()
#					da=apply_along_axis(norm,1,da)/1e3
					da=(apply_along_axis(nz,1,da)/1e3).flatten()
					dat[0]=dot(t.R_.T,dat[0])
					dat[1]=dot(t.R_.T,dat[1])
					dat=pt+dat*scala
#					checklim(da,dat,max)
#					checklim(da,dat,min)
					checklim2(da,dat)

				x1.extend([dat[0,0],dat[1,0],None])
				y1.extend([dat[0,1],dat[1,1],None])
				z1.extend([dat[0,2],dat[1,2],None])
				col.extend([da[0],da[1],None])

#		printmd('''len(x){},len(x1){},len(text){}''',len(x),len(x1),len(text))
			elif type(val) ==ndarray:
				da=r_[0,0,val[ntr,0],0,0,val[ntr,1]].reshape((2,3)).astype(float)
				c=2
				if kargs['direz'] is not None:
					direz=kargs['direz']
					if direz[0]!=0: 
						da=da[:,[2,0,1]]
						c=0
					elif direz[1]!=0: 
						da=da[:,[0,2,1]]
						c=1
				dat=da.copy()
				da=da[:,c].flatten()
				dat[0]=dot(t.R_.T,dat[0])
				dat[1]=dot(t.R_.T,dat[1])
				dat=pt+dat*scala
#				checklim(da,dat,max)
#				checklim(da,dat,min)
				checklim2(da,dat)
				text.extend(['%.3f'%i for i in val[ntr]]+[''])
				x1.extend([dat[0,0],dat[1,0],None])
				y1.extend([dat[0,1],dat[1,1],None])
				z1.extend([dat[0,2],dat[1,2],None])
				col.extend([da[0],da[1],None])
				


		dat0=go.Scatter3d(x=x,y=y,z=z,
						  mode='lines',
						  line=dict(color="black",),
						  name="unifilare"+sst,
#						   hoverinfo='none',
						  opacity=0.3,
						   )
		data.append(dat0)

		def elabora(x):
			x3d=array(x).copy().flatten().astype(float)
			x3d=x3d[~isnan(x3d)]
			return x3d.mean(),x3d.std()

		stds=vstack([elabora(x),elabora(y),elabora(z)])
		if any(stds[:,1]==0):
			st=min([i for i in stds[:,1] if i!=0])
			dat3d=go.Scatter3d(x=[stds[0,0]+st],y=[stds[1,0]+st],z=[stds[2,0]+st],
							  mode='markers',
							  name="3D",
							  marker=dict(size=0),
							  hoverinfo='none',
							  opacity=0.,
							   )
			data.append(dat3d)

		
		nodi=go.Scatter3d(x=xn,y=yn,z=zn,
						  mode="markers",
						  marker=dict(size=2,color="green",),
						  name="vincoli"+sst,
						  hoverinfo='none',
						  opacity=0.2,
						   )
		data.append(nodi)
		if val is not None:
			col=array(col,dtype=float)
#			col-=col[~isnan(col)].min()
#			col/=col[~isnan(col)].max()
			if type(val)==str:
				namepl=val+sst
			else:
				namepl=sst
#			if nst==0:line=dict(color='red')
#			else:line=dict()
			dat1=go.Scatter3d(x=x1,y=y1,z=z1,
							  text=text,
							  hoverinfo="text",
							  mode='lines',
							  line=dict(color=col,
#							  	showscale=True,
								),
							  name=namepl,
							   )
			data.append(dat1)
			data.append(go.Scatter3d(x=(plim[0][0],plim[1][0]),
									 y=(plim[0][1],plim[1][1]),	
									 z=(plim[0][2],plim[1][2]),	
									 mode='markers+text',
									 marker=dict(size=3,color=('red','blue')),
									 name='limiti',
									 text=['{:.3f}'.format(i) for i in vlim],
									 ))

	fig=go.Figure(data=data,layout=layout)
	if 'returnFig' in list(kargs.keys()):
		if kargs['returnFig']:
			return fig
	pl.iplot(fig)
