
from gueng.FEM import *
#eseguo controllo di test per molle a sola compressione o trazione

Nodo.initType='d2'

l=2e3
nodi=[Nodo(i,0) for i in [-l,0,l]]
tr=[TraveMaster(i,j,A=400*400,Jz=1/12*400*400**3,E=25e3,part=111) for i,j in zip(nodi[:-1],nodi[1:])]

nodis=[]
els=[]
for i in tr:
	i.aggiungi_carico('vy',-i.A/1e6*2500/1000)
	nodis.extend(i.nodi[:-1])
	els.extend(i.parti)
nodis.append(i.nodi[-1])
co=r_[[i.co[0] for i in nodis]]
dl=co[1]-co[0]

mo=[MollaVy(i,10e3*dl) for i in nodis]
els.extend(mo)
f=10e3
nodi[0].gdl['vy'].f=-f
nodi[1].gdl['vy'].f=-f
nodi[2].gdl['vy'].f=-f
st=Struttura(els)
st.risolviSparse()
figure(1)
clf()
show(0)
grid(1)
axhline(0,color='k')
num=0
while True:
	d=r_[[i.gdl['vy'].val for i in nodis]]
	plot(co,d,label='{}'.format(num))
	num+=1
	draw()
	if num>=1e3:break
	test=[i.testStato(-1) for i in mo]
	if all(test):break
	st.M=None
	st.risolviSparse()
	if num>=1e3:break
legend(loc='best')
draw()

#controllo forze:
Fs=-r_[[dot(i.dl(),i.Kl)[0] for i in mo]]
print(Fs.sum())

Frec=3*f + 2500*tr[0].A/1e6*l*2/1e3
print(Frec)
er=(Fs.sum()-Frec)/Frec
print(er)


