
import gueng.FEM
import gueng.FEM.trave
import imp
imp.reload(gueng.FEM.trave)
from gueng.FEM import *
from gueng.FEM.trave import *
from pylab import *


Nodo.initType='d2'

n1=Nodo(0,0)
n2=Nodo(1000,0)
n3=Nodo(500,0)
for i in n1,n2:
	i.gdl['vx'].d=0
	i.gdl['vy'].d=0

pr=prof.upn80
dattr={'A':pr.A,'Jz':pr.Jy, 'E':210e3,'part':11}

t=Trave(n1,n3,**dattr)
t2=Trave(n3,n2,**dattr)
q=AzioneTrave((0,-1.,0,0,0,0))

t.aggiungi_carico(q)
t2.aggiungi_carico(q)
st=Struttura([t,t2])
st.risolviSparse()

figure(1)
clf()
show(0)
uc=False
d=t.d('vy',usaCarichi=uc)
plot(d[0],d[1])
grid(1)
draw()

vat=5/384*1*1e3**4/210e3/pr.Jy
print('''v atteso {vat:.4g} 
calcolato={d:.4g}
rapporto calc/att={r:.4g}'''.format(r=-d[1].min()/vat,vat=vat,d=-d[1].min()))

le,dat=t.lineaElastica(usaCarichi=uc,fullout=True)
print(dat)

Mz=polyder(le[5],2)*t.E*t.Jz
figure(2)
clf()
show(0)
plot(d[0],Mz(d[0]))
draw()
