
from gueng.FEM import *
from gueng.profilario import prof
from scipy import *
from gueng.utils import toDict
pr=prof.ipe200
A=pr.A
Jx=0
Jy=pr.Jz
Jz=pr.Jy
E=210e3
dati_sez=toDict('A E Jz')
l=2000
n=41
nm=int(floor(n/2))
x=linspace(0,l,n)
Nodo.initType='d2'
nodi=[Nodo(i,0) for i in x]
#considero trave in semplice appoggio
nodi[0].gdl['vx'].d=0
nodi[0].gdl['vy'].d=0
nodi[-1].gdl['vy'].d=0

nodi[nm].gdl['vy'].f=-1e3
els=[Trave(j,i,**dati_sez) for i,j in zip(nodi[:-1],nodi[1:])]
s=Struttura(els)
s.risolviSparse()
print('semplice appoggio')
vref=-1/(3*16)*1e3*2000**3/(E*pr.Jy) #semplice appoggio
print('controllo spostamento')
print('vref',vref)
print('calcolato',nodi[nm].gdl['vy'].val)
print('controllo momento')
print('Mref',1/4*1e3*2e3)
print('calcolato',els[nm].Mzj)
print('controllo Taglio')
print('Tref',1/2*1e3)
print('calcolato',els[0].Tyi,els[0].Tyj)

import sys


#Considero incastro-incastro
print()
s.reset()
nodi[0].gdl['rz'].d=0
nodi[-1].gdl['vx'].d=0
nodi[-1].gdl['rz'].d=0
nodi[nm].gdl['vy'].f=-1e3
s.risolviSparse()
vref=1/(E*pr.Jy*3*8*8)*2000**3*1e3 #incastro incastro
print('incastro incastro')
print('vref',vref)
print('calcolato',nodi[nm].gdl['vy'].val)
import copy
s1=copy.deepcopy(s)
figure(2)
d=[i.gdl['vy'].val for i in nodi]
plot(d)


############################################################
#pilastro
x=linspace(0,l,n)
Nodo.initType='d2'
nodi=[Nodo(0,i) for i in x]
#considero trave in semplice appoggio
print()
nodi[0].gdl['vx'].d=0
nodi[0].gdl['vy'].d=0
nodi[-1].gdl['vx'].d=0

nodi[nm].gdl['vx'].f=-1e3
els=[Trave(i,j,**dati_sez) for i,j in zip(nodi[:-1],nodi[1:])]
s=Struttura(els)
s.risolvi()
print('pilastro semplice appoggio')
vref=1/(3*16)*1e3*2000**3/(E*pr.Jy) #semplice appoggio
print('vref',vref)
print('calcolato',nodi[nm].gdl['vx'].val)

#Considero incastro-incastro
print()
s.reset()
nodi[0].gdl['rz'].d=0
nodi[-1].gdl['vx'].d=0
nodi[-1].gdl['rz'].d=0
nodi[nm].gdl['vx'].f=-1e3
s.risolvi()
vref=1/(E*pr.Jy*3*8*8)*2000**3*1e3 #incastro incastro
print('pilastro incastro incastro')
print('vref',vref)
print('calcolato',nodi[nm].gdl['vx'].val)

############################################################
#inclinata45°
print()
x=linspace(0,l,n)/sqrt(2)
Nodo.initType='d2'
nodi=[Nodo(i,i) for i in x]
#considero trave in semplice appoggio
print()
nodi[0].gdl['vx'].d=0
nodi[0].gdl['vy'].d=0
nodi[-1].gdl['vx'].d=0
nodi[-1].gdl['vy'].d=0

nodi[nm].gdl['vx'].f=-1e3/sqrt(2)
nodi[nm].gdl['vy'].f=1e3/sqrt(2)
els=[Trave(j,i,**dati_sez) for i,j in zip(nodi[:-1],nodi[1:])]
s=Struttura(els)
s.risolvi()
print('45 semplice appoggio')
vref=1/(3*16)*1e3*2000**3/(E*pr.Jy) #semplice appoggio
print('vref',vref)
vx=nodi[nm].gdl['vx'].val
vy=nodi[nm].gdl['vy'].val
print('calcolato',sqrt(vx**2+vy**2))

#Considero incastro-incastro
print()
s.reset()
nodi[0].gdl['rz'].d=0
nodi[-1].gdl['vx'].d=0
nodi[-1].gdl['rz'].d=0
nodi[nm].gdl['vx'].f=-1e3/sqrt(2)
nodi[nm].gdl['vy'].f=1e3/sqrt(2)
s.risolvi()
vref=1/(E*pr.Jy*3*8*8)*2000**3*1e3 #incastro incastro
print('45 incastro incastro')
print('vref',vref)
vx=nodi[nm].gdl['vx'].val
vy=nodi[nm].gdl['vy'].val
print('calcolato',sqrt(vx**2+vy**2))


#Eseguo test sulle pressioni distribuite:
print()
p=prof.HEA200
p={'A':p.A,'Jz':p.Jy,'E':210e3}

n=[Nodo(i,0) for i in arange(3)*2000]
e=[Trave(i,j,**p) for i,j in zip(n[0:-1],n[1:])]
q=AzioneTrave(-10,faccia='vy')
for i in e: 
	i.azioni.append(q)
	i.sommaForze()
n[0].gdl['vx'].d=0
n[0].gdl['vy'].d=0
n[-1].gdl['vy'].d=0

st=Struttura(e)
st.risolvi()
pe=vstack([i.plot() for i in e])
pM0=vstack([i.plot('Mz') for i in e])
scala=.1*pe[-isnan(pe)].ptp()/pM0[-isnan(pM0)].ptp()
pM=vstack([i.plot('Mz',scala=scala) for i in e])
figure(1)
subplot(1,2,1)
plot(pe[:,0],pe[:,1],'--b')
plot(pM[:,0],pM[:,1],'r')

pV0=vstack([i.plot('Ty') for i in e])
scala=.1*pe[-isnan(pe)].ptp()/pV0[-isnan(pV0)].ptp()
pV=vstack([i.plot('Ty',scala=scala) for i in e])
subplot(1,2,2)
plot(pe[:,0],pe[:,1],'--b')
plot(pV[:,0],pV[:,1],'r')
axis('equal')
for i in n:
	print(i.gdl['vy'].f)

for i in n[0],n[2]:
	print(i.gdl['vy'].equilibrio())


for i in e:
	print(i.Tyi,i.Tyj)
#raw_input()
