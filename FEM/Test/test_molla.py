
from scipy import *
from gueng.FEM import *
#molla semplice
Nodo.initType='d2'
n=Nodo(0,0)
m=MollaVx(n,50)
n.gdl['vx'].f=100

s=Struttura([m])
s.risolvi()
print(n.gdl['vx'].val)
print(n.gdl['vy'].val)

#trave su letto di molle alla winkler
x=linspace(0,15000,61)
nodi=[Nodo(i,0) for i in x]
nm=int(floor(len(nodi)/3*2))
nodi[nm].gdl['vy'].f=-1e4
nodi[0].gdl['vx'].d=0
b,h=600,400
E=25e3
J=1/12*b*h**3
A=b*h
e=[Trave(i,j,A=A,E=E,Jz=J) for i,j in zip(nodi[:-1],nodi[1:])]
K=b*(x[1]-x[0])*.1
m=[MollaVy(i,K) for i in nodi]
els=[]
els.extend(e)
els.extend(m)
st=Struttura(els)
st.risolvi()

clf()
subplot(2,2,1)
plot(x,zeros_like(x),'--b')
print('deformazione massima', max(abs(r_[[i.deform()[1] for i in nodi]])))
scala=1
plot([i.deform(scala)[0] for i in nodi],[i.deform(scala)[1] for i in nodi],'r')
#axis('equal')
grid()

#trave su letto di molle alla winkler in verticale
x=linspace(0,15000,61)
nodi=[Nodo(0,i) for i in x]
nm=int(floor(len(nodi)/3*2))
nodi[nm].gdl['vx'].f=-1e4
nodi[0].gdl['vy'].d=0
b,h=600,400
E=25e3
J=1/12*b*h**3
A=b*h
e=[Trave(i,j,A=A,E=E,Jz=J) for i,j in zip(nodi[:-1],nodi[1:])]
K=b*(x[1]-x[0])*.1
m=[MollaVx(i,K) for i in nodi]
els=[]
els.extend(e)
els.extend(m)
st=Struttura(els)
st.risolvi()

subplot(2,2,3)
plot(zeros_like(x),x,'--b')
print('deformazione massima', max(abs(r_[[i.deform()[0] for i in nodi]])))
scala=1
plot([i.deform(scala)[0] for i in nodi],x,'r')
#axis('equal')
grid()

#tipo paratia
x=linspace(0,15000,201)
nodi=[Nodo(0,i) for i in x]
nm=int(floor(len(nodi)/3*2))
nodi[-1].gdl['vx'].f=-1e4
nodi[-1].gdl['rz'].f=1e4*1000
#nodi[0].gdl['vy'].d=0
b,h=600,400
E=25e3
J=1/12*b*h**3
A=b*h
e=[Trave(i,j,A=A,E=E,Jz=J) for i,j in zip(nodi[:-1],nodi[1:])]
K=b*(x[1]-x[0])*.1
m=[MollaVx(i,K) for i in nodi]
els=[]
els.extend(e)
els.extend(m)
st=Struttura(els)
st.risolviSparse()

subplot(1,1,1)
plot(zeros_like(x),x,'--b')
print('deformazione massima', max(abs(r_[[i.deform()[0] for i in nodi]])))
scala=1
plot([i.deform(scala)[0] for i in nodi],x,'r')
#axis('equal')
grid()

figure()
def plot_():
	subplot(1,4,1)
	d=vstack([i.plot() for i in e])
	plot(d[:,0],d[:,1],'--b')
	d=vstack([i.plot('dy') for i in e])
	plot(d[:,0],d[:,1],'r')

	subplot(1,4,2)
	d=vstack([i.plot(None) for i in e])
	plot(d[:,0],d[:,1],'--b')
	d=vstack([i.plot('N') for i in e])
	plot(d[:,0],d[:,1],'r')

	subplot(1,4,3)
	d=vstack([i.plot(None) for i in e])
	plot(d[:,0],d[:,1],'--b')
	d=vstack([i.plot('Ty') for i in e])
	plot(d[:,0],d[:,1],'r')

	subplot(1,4,4)
	d=vstack([i.plot(None) for i in e])
	plot(d[:,0],d[:,1],'--b')
	d=vstack([i.plot('Mz') for i in e])
	plot(d[:,0],d[:,1],'r')

plot_()

input()
