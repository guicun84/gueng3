
from gueng.FEM import *
import unittest
from pylab import *
from scipy import *

if __name__=='__main__':
	class TestFrame(unittest.TestCase):
		def setUp(self):
			Nodo.initType='d2'
			self.nodi=[ Nodo(0,0), #appogio basso
						Nodo(0,1), #appoggio alto
						Nodo(1,0), #nodo centrale trave
						Nodo(1,0), #nodo centrale tirante
						Nodo(2,0)] #nodo sbalzo

			for i in 'vx vy'.split():self.nodi[-2].gdl[i]=self.nodi[-3].gdl[i] #impongo la cerniera
			for i in [0,1]:
				self.nodi[i].gdl['vx'].d=0
				self.nodi[i].gdl['vy'].d=0
			dattr={'A':10, 'Jz':1, 'E':1}
			def trave(i,j,n=5):
				x=linspace(i.co[0],j.co[0],n)[1:-1]
				no=[i]
				no.extend([Nodo(i,0) for i in x])
				no.append(j)
				return [Trave(i,j,**dattr) for i,j in zip(no[:-1],no[1:])]
			self.tirante=Trave(self.nodi[1],self.nodi[3],**dattr)
			self.t1=trave(self.nodi[0],self.nodi[2])
			self.t2=trave(self.nodi[2],self.nodi[4])
			self.els=[self.tirante]
			for i in self.t1,self.t2: self.els.extend(i)
			self.st=Struttura(self.els)

		def test_carico_nodo_controvento(self):
			print(all([i.gdl['vy'].f==0 for i in self.nodi]))
			self.nodi[2].gdl['vy'].f=-1
			self.st.risolviSparse()
			print(self.els[0].Ni)
			self.plot(1,[1e-1,1,1,5e-1])
			for n,i in enumerate (self.els):
				print(n,i.Ni , i.Nj)
			self.assertAlmostEqual(self.tirante.Ni,sqrt(2),3)
			self.assertAlmostEqual(self.t1[0].Ni,-1,3)

		def test_carico_nodo_estremo(self):
			print(all([i.gdl['vy'].f==0 for i in self.nodi]))
			self.nodi[4].gdl['vy'].f=-1
			self.st.risolviSparse()
			print(self.els[0].Ni)
			self.plot(2,[1e-1,1,1,5e-1])
			for n,i in enumerate (self.els):
				print(n,i.Ni , i.Nj)
			self.assertAlmostEqual(self.tirante.Ni,2*sqrt(2),3)
			#controllo segno della rotazione:
			self.assertEqual(sign(self.nodi[-1].gdl['rz'].val),-1)

		def plot(self,f,s=[1,1,1,1]):
			d0=vstack([i.plot() for i in self.els])
			d=vstack([i.plot('d',scala=s[0]) for i in self.els])
			M=vstack([i.plot('Mz',scala=s[1]) for i in self.els])
			T=vstack([i.plot('Ty',scala=s[2]) for i in self.els])
			N=vstack([i.plot('N',scala=s[3]) for i in self.els])
			titoli='deformata momento taglio normale'.split()
			figure(f)
			clf()
			for n,i in enumerate([d,M,T,N]):
				subplot(2,2,n+1)
				plot(d0[:,0],d0[:,1])
				plot(i[:,0],i[:,1])
				axis('equal')
				grid('on')
				title(titoli[n])

	unittest.main(verbosity=2)


