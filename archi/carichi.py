#-*-coding:latin 

from plotly import offline as pl , graph_objs as go
from scipy import *
from scipy.linalg import norm
from gueng.geotecnica import Coulomb


class CaricoConcentrato:
	def __init__(self,F,x0,off=0,l0=0,alpha=30,checkAllOn=True,estradosso=True):
		'''F=forza totale da applicare
		x0= posizione orizzontale forza
		off= quota oltre estradosso cui è applicato il carico
		l0= impronta carico
		alpha= angolo distribuzine carico
		checkAllOn: se true se il carico agli estremi deborda dall'arco
		estradosso True se false applica carico su intersezione con asse arco '''
		vars(self).update(locals())
		
	def valutaLimiti(self,arco):
		#estraggo i nodi dagli elementi
		h0=arco.r+arco.s+arco.h0+self.off
		R={True:arco.r+arco.s,
			False:arco.r+arco.s/2}[self.estradosso]
		#valuto le linee di ridistribuzione
		p=tan(deg2rad(90-self.alpha)) #pendenza linea
		l1=polyfit( (self.x0-self.l0/2,self.x0-self.l0/2+1),(h0,h0+p),1)
		l2=polyfit( (self.x0+self.l0/2,self.x0+self.l0/2+1),(h0,h0-p),1)
		yy=r_[-1,0,R**2] #eq circonferenza...
		x1=(roots(polyadd(yy,-1*polymul(l1,l1))))
		x1=[real(i) for i in x1 if not imag(i)]
#		x1=polyval([1-l1[1]],ymin)/l1[0]
		x2=(roots(polyadd(yy,-1*polymul(l2,l2))))
		x2=[real(i) for i in x2 if not imag(i)]
		if x1 or x2:
			if x1: x1=max(x1)
			else:x1=min(arco.asse[:,0])
			if x2:x2=min(x2)
			else:x2=max(arco.asse[:,0])
		else:
			x1=1
			x2=1
#		x2=polyval([1-l2[1]],ymin)/l2[0]
		return h0,l1,l2,x1,x2
		
	def cercaNodi(self,arco,h0=None,l1=None,l2=None,x1=None,x2=None):
		if h0 is None:
			h0,l1,l2,x1,x2=self.valutaLimiti(arco)
		#controllo se il carico finisce su arco:
		nodi=arco.nodi
		nodiq=[i for i in nodi if x1<=i.co[0]<=x2]
		if not nodiq:
			nodiq=[]
			nds=[i for i in nodi if i.co[0]<=self.x0-self.l0/2]
			if nds: nodiq.append(nds[-1])
			nds=[i for i in nodi if i.co[0]>=self.x0+self.l0/2]
			if nds: nodiq.append(nds[0])
		return nodiq
	
	def applica(self,arco,superficie=0):
		'''arco=Arco su cui applicare i carichi
		sperficie:0 per estradosso 1 per asse arco'''
		h0,l1,l2,x1,x2=self.valutaLimiti(arco)
		ymin=min(arco.asse[:,1])
		p1=polyval(r_[1,-l1[1]]/l1[0],ymin)
		p2=polyval(r_[1,-l2[1]]/l2[0],ymin)
		ltot=p2-p1
		p1=max((p1,arco.asse[:,0].min()))
		p2=min((p2,arco.asse[:,0].max()))
		l=p2-p1
		if l<0:return 
		#cerco nodi interessati
		nodiq=self.cercaNodi(arco,h0,l1,l2,x1,x2)
		if not nodiq: return 
		if self.checkAllOn:
			r=l/ltot
		else:
			r=1
		w=r_[[1/norm(r_[self.x0,h0]-i.co[:2]) for i in nodiq]]**2
		if len(w)>1: w*=gradient([i.co[0] for i in nodiq])
		W=w.sum()
		for m,n in enumerate(nodiq):
			n.gdl['vy'].f-=self.F*r/W*w[m]
		return 
	
	def plotInfluenza(self,arco,superficie=0):
		'''arco Arco di riferimento
		superficie=0 estradosso 1 asse arco'''
		h0,l1,l2,x1,x2=self.valutaLimiti(arco)
		nodiq=self.cercaNodi(arco,h0,l1,l2,x1,x2)
		x=arco.asse[:,0]
		l1=polyval(l1,x)
		l2=polyval(l2,x)
		plt=[go.Scatter(x=x,y=l1,name='linea 1'),
			 go.Scatter(x=x,y=l2,name='linea 2'),
			 go.Scatter(x=[i.co[0] for i in nodiq],y=[i.co[1] for i in nodiq],name='area caricata',line=dict(color='red', width=3))
		]
		return plt

class CaricoLitostaticoOrizzontale:
	def __init__(self,gamma,phi,c=0,q0=0):
		vars(self).update(locals())
		self.terreno=Coulomb(gamma,phi,c)

	def calcolaPressioni(self,arco):
		z=arco.r+arco.h0-arco.asse[:,1] #profondità 
		xmax=arco.asse[arco.asse[:,1]==arco.asse[:,1].max()][0,0]
		press=zeros(arco.asse.shape[0])
		for n,(nodo,p) in enumerate(zip(arco.nodi,z)):
			if nodo.co[0]<=xmax:dire=1
			else: dire=-1
			press[n]=dire*self.terreno.K0*(self.terreno._gamma*p + self.q0)
		return press

	def applica (self,arco):
		dy=abs(gradient(arco.asse[:,1])) #lunghezze di influenza in verticale
		press=self.calcolaPressioni(arco)
		for n,l,p in zip(arco.nodi,dy,press):
			n.gdl['vx'].f+=p*l*arco.b

	def plotInfluenza(self,arco,title=''):
		press=self.calcolaPressioni(arco)
		return go.Scatter(x=arco.asse[:,0],
						  y=press,
						  name=title)





		

		
