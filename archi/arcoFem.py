# coding: utf-8

from gueng.FEM import *
Nodo.initType='d2'
from plotly import offline as pl , graph_objs as go
from scipy import *
from scipy.linalg import norm


class Arco:
	def __init__(self,r,s,h0,qarco,qriempimento,g,q,a0=0,af=180,b=1000,n=100,incastri=[1,1],E=2e5,fd=None):
		'''r=raggio intradosso
		s=spessore arco
		h0= spessore minimo copertura
		qarco= carico arco [F/l^3]
		qriempimento=carico riempimento [F/l^3]
		g= carico proprio distribuito [F/l^2]
		q=carico accidentale distribuito [F/l^2]
		a0 af angoli iniziali e finali
		b=base considerata
		n=numero conci
		incastri= flag per incastri iniziale e finale
		E=modulo elastico'''
		vars(self).update(locals())
		self.a=deg2rad(linspace(a0,af,n+1))
		self.asse=c_[cos(self.a),sin(self.a)]*(self.r+self.s/2)
		
	def genera(self):
		self.spostamenti=zeros(self.asse.shape[0])
		self.nodi=[Nodo(*i) for i in self.asse]
		dat=dict(A=self.s*self.b,Jz=1/12*self.s**3*self.b,E=self.E,s=self.s,Wz=1/6*self.b*self.s**2,fd=self.fd)
		self.els=[Trave(i,j,**dat) for i,j in zip(self.nodi[:-1],self.nodi[1:])]
		self.nodi[0].gdl['vx'].d=0
		self.nodi[0].gdl['vy'].d=0
		self.nodi[-1].gdl['vx'].d=0
		self.nodi[-1].gdl['vy'].d=0
		if self.incastri[0]:
			self.nodi[0].gdl['rz'].d=0
		if self.incastri[1]:
			self.nodi[-1].gdl['rz'].d=0
		self.st=Struttura(self.els)
		
	def carichiPropri(self):
		for e in self.els:
			xg=mean((e.i.co[0],e.j.co[0]))
			l0=e.i.co[0]-e.j.co[0]
			ysup=sqrt((self.r+self.s)**2-xg**2)
			yinf=sqrt((self.r)**2-xg**2)
			if imag(yinf):
				yinf=min(self.asse[:,1])
			qa=self.qarco*(ysup-yinf)*l0
			qr=self.qriempimento*(self.r+self.s+self.h0-ysup)*l0
			g=self.g*l0
			q=self.q*l0
			Q=(qa+qr+g+q)*self.b
			e.i.gdl['vy'].f-=Q/2
			e.j.gdl['vy'].f-=Q/2
			
	@property		
	def ecc(self):
		'calcolo eccentricità'
		sol=vstack([i.solLoc() for i in self.els])
		e=sol[:,[5,11]]/sol[:,[0,6]]+c_[self.spostamenti[:-1],self.spostamenti[1:]]
		return e
	
	@property
	def pmin(self):
		 sol=vstack([i.solLoc() for i in self.els])
		 A=r_[[i.A for i in self.els]]
		 W=r_[[i.kargs['Wz'] for i in self.els]]
		 return -c_[abs(sol[:,0]/A) + abs(sol[:,5]/W) , 
			 abs(sol[:,6]/A) + abs(sol[:,11]/W)]
	
	@property
	def pmax(self):
		 sol=vstack([i.solLoc() for i in self.els])
		 A=r_[[i.A for i in self.els]]
		 W=r_[[i.kargs['Wz'] for i in self.els]]
		 return -c_[abs(sol[:,0]/A) - abs(sol[:,5]/W) , 
			 abs(sol[:,6]/A) - abs(sol[:,11]/W)]
		
	@property
	def mu(self):
		'coefficiente di attrito'
		sol=vstack([i.solLoc() for i in self.els])
		mu=abs(sol[:,[1,7]]/sol[:,[0,6]])
		return mu
	
	def carichi(self):
		return r_[[i.gdl['vy'].f for i in self.nodi]]/gradient(self.asse[:,0])
		
	
	@staticmethod
	def mean(x):
		'''x=array nx2: su ogni riga il valore al nodo i e j di un elemento;
		ritorna la media su un nodo facendo la media tra il valore i e j di due elementi contigui
		'''
		return (r_[x[:,0],x[-1,1]] + r_[x[0,0],x[:,1]])/2
	
	def checkNlTrazione(self,eps=1e-3,weight=1):
		'''esegue il check sull'eccentricità dei carichi e modifica spessori e linea d'asse
		eps= differenza percentuale tra spostamento e spessore
		weight=1 percentuale di eccentricità da considerare ad ogni iterazione'''
		#analizzo eccentricità dell'arco
		spostamenti=zeros(self.n+1)
		spessori=zeros(self.n+1)
		for cnt,n in enumerate(self.nodi):
			#pesco gli elementi
			if cnt==0:
				ei=self.els[cnt]
				ej=None
			elif cnt==len(self.nodi)-1:
				ej=self.els[cnt-1]
				ei=None
			else:
				ei=self.els[cnt]
				ej=self.els[cnt-1]
			#valuto eccentricità media:
			if ei is None:
				e=ej.Mzj/ej.Nj
				s=ej.kargs['s']
			elif ej is None:
				e=ei.Mzi/ei.Ni
				s=ei.kargs['s']
			else:
				pass
				e=mean((ej.Mzj/ej.Nj, ei.Mzi/ei.Ni))
				s=mean((ei.kargs['s'],ej.kargs['s']))
			#se serve sposto il nodo e modifico gli spessori degli elementi
			if abs(e)>s/6:
				spessori[cnt]=(s/2-abs(e))*3
				spostamenti[cnt]=(s-spessori[cnt])/2*sign(e)
				spessori[cnt]*=(2-weight)
				spostamenti[cnt]*=(2-weight)   
			else:
				spessori[cnt]=s
				spostamenti[cnt]=0
		#se non ho da eseguire spostamenti è finita
		test= all(abs(spostamenti)/spessori<=eps)
		if not test:
			self.spostamenti += spostamenti
			for cnt,sp in enumerate(spostamenti):
				if sp!=0:
					self.nodi[cnt].co += r_[cos(self.a[cnt]),sin(self.a[cnt]),0]*sp
					if cnt==0:
						el=[self.els[cnt]]
					elif cnt==len(self.nodi)-1:
						el=[self.els[cnt-1]]
					else:
						el=[self.els[cnt],self.els[cnt-1]]
					s=spessori[cnt]
					for e in el:
						if e.kargs['s']>s:
							e.kargs['s']=s
							e.kargs['Wz']=1/6*self.b*s**2
							e.A=self.b*s
							e.Jz=1/12*self.b*s**3
		return test
						
	def checkNlPlastica(self,eps):
		'''esegue controllo su plasticità materiale (bisogna definire fd dell'arco)
		eps=errore percentuale tra tensione calcolata e tensione di progetto'''
		test=True
		if self.fd is not None:
			for e in self.els:
				pi=abs(e.Ni/e.A) + abs(e.Mzi/e.kargs['Wz'])
				pj=abs(e.Nj/e.A)+ abs(e.Mzj/e.kargs['Wz'])
				p=max((pi,pj))
				if abs(p/e.kargs['fd'])-1>eps:
					test=False
					e0=e.E
					e.E*=e.kargs['fd']/p
					print(pi,pj,p,e.kargs['fd'],e0,e.E)
					
		return test
	
	def risolviNl(self,epstr=1e-3,maxiter=100,weight=1,epspl=1e-3):
		'''eps=spostamento minimo
		maxiter=massimo iterazioni
		w= moltiplicatore per lo spostamento da adottare'''
		iter=0
		while True:
			self.st.reset(f=0,gdl=0)
			self.st.risolviSparse()
			if iter==maxiter: 
				print ('esecuzione interrotta')
				break
			testnl=[
				self.checkNlTrazione(epstr,weight),
				self.checkNlPlastica(epspl),
				   ]
			if all(testnl):break
			iter+=1
			for e in self.els:
				e.aggiorna()
		
	def plotArco(self):
		'''ritorna una lista con go.Scatter rappresentanti l'arco'''
		pinf=c_[cos(self.a),sin(self.a)]*self.r
		psup=pinf/self.r*(self.r+self.s)
		ti=pinf/self.r*(self.r+self.s/3)
		ts=pinf/self.r*(self.r+self.s/3*2)
		grp=[go.Scatter(x=psup[:,0],y=psup[:,1],
						mode='lines',name='profilo superiore',
						line=dict(color='black',shape='spline')),
			 go.Scatter(x=pinf[:,0],y=pinf[:,1],
						mode='lines',name='profilo inferiore',
						line=dict(color='black',shape='spline')),
			 go.Scatter(x=ts[:,0],y=ts[:,1],
						mode='lines',name='terzo medio',
						line=dict(color='black',width=1,shape='spline')),
			 go.Scatter(x=ti[:,0],y=ti[:,1],
						mode='lines',name='terzo medio',
						line=dict(color='black',width=1,shape='spline')),
			 go.Scatter(x=self.asse[:,0],y=ones(self.asse.shape[0])*(self.r+self.s+self.h0),
						mode='lines',name='estradosso',
						line=dict(color='black',shape='spline')),
			]
		return grp
	
	def plotLineaCarico(self,text=''):
		'''ritorna goScatter rappresentante linea di carico'''
		e=self.mean(self.ecc)
		carico=self.asse/(self.r+self.s/2)
		carico[:,0]*=(self.r+self.s/2+e)
		carico[:,1]*=(self.r+self.s/2+e)
		grp=go.Scatter(x=carico[:,0],y=carico[:,1],
				   mode='lines',name='carichi {}'.format(text),
				   line=dict(shape='spline'))
		return [grp]
	
	def plotElementi(self):
		pt=vstack(i.co for i in self.nodi)
		grs=[]
		grs.append(go.Scatter(x=pt[:,0],y=pt[:,1],name='asse elementi',line=dict(dash='dash',width=1)))
		pt=empty((0,3))
		for e in self.els:
			new=vstack((e.i.co + dot(e.R_.T,r_[0,e.kargs['s']/2,0]),
					   e.j.co + dot(e.R_.T,r_[0,e.kargs['s']/2,0]),
					   r_[nan,nan,nan] ,
					   e.i.co + dot(e.R_.T,r_[0,-e.kargs['s']/2,0]),
					   e.j.co + dot(e.R_.T,r_[0,-e.kargs['s']/2,0]),
					   r_[nan,nan,nan] ,
					 ))
			pt=vstack((pt,new))
		grs.append(go.Scatter(x=pt[:,0],y=pt[:,1],name='sezione resistente'))
		return grs
	
	def plotValNodi(self,val,title=''):
		return go.Scatter(x=[i.co[0] for i in self.nodi],
						  y=val,
						  name=title)
	
	def __str__(self):
		return '''r={r}, s={s}, h0={h0}'''.format(**vars(self))
