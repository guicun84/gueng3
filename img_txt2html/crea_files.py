#-*-coding:utf-8-*-
from scipy import *
from pylab import *
ion()
#from PIL import Image
import re,os
#creo le immagini
def crea_immagini():
	n=10
	for i in range(10):
		im='c%03d.png'%i
		frq=rand()*10
		t=linspace(0,1,1000)
		y=sin(frq*pi*t)
		clf()
		plot(t,y)
		xlabel(r'$t [s]$')
		ylabel(r'$y$')
		title(r'$sin(%.3f \pi t)$'%frq)
		savefig(im)

def crea_txt_base():
	files=[i for i in os.listdir('.') if os.path.splitext(i)[1].lower()=='.png']
	for fi in files:
		fi=re.sub('\.png$','.txt',fi)
		fi=open(fi,'w')
		fi.close()

def riempi_txt_a_muzzo():
	files=[i for i in os.listdir('.') if os.path.splitext(i)[1].lower()=='.txt']
	for fi in files:
		txt=randint(0,128,200)
		txt=''.join([chr(i) for i in txt])
		fi=open(fi,'w')
		fi.write(txt)
		fi.close()
	


def componi_html():
	st=''
	st='''<html><head>
<style type="text/css">
img.grp{height:7cm;
	display:block;
	margin:auto;
	}
table.grp {width:17cm;
	border-spacing:0px;
	}

td.img{width:17cm;
	border:1px solid black;
	margin:0px;	
	}


td.txt{width:8.5cm;
	border: 1px solid black;
	margin:0px;
	font-size:8;
	}
</style>
</head>'''
	
	form='''
<h4>caso %d</h4>
<table class="grp">
<tr> <td class="img" colspan=2> <img class="grp" src="%s"></td></tr>
<tr><td class="txt">%s</td>
	<td class="txt">%s</td>
</tr>
</table>'''


#quanto prima se i css funzionassero
	scale=96/2.5
	w1=int(17*scale)
	w2=int(w1/2)
	h1=int(7*scale)
	ftz=11
	st='''
<html>
<head></head>
<body>
'''
	form='''
<h4>caso %(n)d</h4>
<table>
<tr> <td colspan=2 width=%(w1)d style="border: 1px solid black; text-align:center;"> <img height=%(h1)d src="%(fi)s" style="display:block; margin:auto"></td></tr>
<tr><td width=%(w2)d style="border: 1px solid black; font-size:%(ftz)dpx">%(tx1)s</td>
	<td width=%(w2)d style="border: 1px solid black; font-size:%(ftz)dpx">%(tx2)s</td>
</tr>
</table>'''



	files=[i for i in os.listdir('.') if os.path.splitext(i)[1].lower()=='.png']
	files.sort()
	for fi in files:
		n=int(re.search('\d+',fi).group())
		txt=fi.replace('.png','.txt')
		txt=open(txt,'r')
		tx=txt.read()
		txt.close()
		tx=tx.replace('<','&lt')
		tx=tx.replace('>','&gt')
		tx1=tx[:len(tx)/2]
		tx1=tx1.replace('\n','<br>\n')
		tx2=tx[len(tx)/2:]
		tx2=tx2.replace('\n','<br>\n')
		st+=form%locals()
	st+='</body></html>'
	out=open('prova.html','w')
	out.write(st)
	out.close()




