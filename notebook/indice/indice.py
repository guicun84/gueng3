from IPython.display import display,HTML,Javascript
import os
DIRE=os.path.dirname(__file__)

class DivDiIndice:
	js=open(os.path.join(DIRE,'indice.js')).read()
	css=open(os.path.join(DIRE,'indice.css')).read()
	def __init__(self):
		display(HTML(''' <style>{css}</style>
		<script>{js}</script>
		<div id="DivDiIndice__init__"> </div>
		'''.format(**vars(DivDiIndice))))
		display(Javascript('DivDiIndice__init__()'));
	
