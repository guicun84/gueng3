function DivDiIndice__init__(){
	var div=document.getElementById('DivDiIndice');
	if (div==null){
		var div=document.getElementById('DivDiIndice__init__');
		div.setAttribute('id','DivDiIndice');
		var tmp=document.createElement('p');
		tmp.innerText='INDICE';
		tmp.style.color="darkred";
		tmp.onclick=DivDiIndice_scriviIndice;
		tmp.style.cursor='pointer';
		tmp.onmouseover=function(){this.style.color='red';};
		tmp.onmouseout=function(){this.style.color='darkred';};
		div.appendChild(tmp);
	}
}

function DivDiIndice_scriviIndice(){
	//elimito l'indice precedente se esiste
	var div=document.getElementById('DivDiIndice');
	div.innerHTML="";
	var tmp=document.createElement('p');
	tmp.innerText='INDICE';
	tmp.style.color="darkred";
	tmp.onclick=DivDiIndice_scriviIndice;
	tmp.style.cursor='pointer';
	tmp.onmouseover=function(){this.style.color='red';};
	tmp.onmouseout=function(){this.style.color='darkred';};
	div.appendChild(tmp);

    var tutto=document.getElementsByTagName('*');
    var o=document.createElement('ul');
    o.id="lista_indice";
    for (var i in tutto){
        if (/^H\d$/.test(tutto[i].tagName)){
			if (!(tutto[i].id)){
				tutto[i].setAttribute('id','titolo_'+tutto[i].innerHTML);
			}
            var a=document.createElement('a');
            a.setAttribute('href','#'+tutto[i].id);
            var n=parseInt(/^H(\d+)$/.exec(tutto[i].tagName)[1]);
            var l=document.createElement('li');
            a.innerHTML=tutto[i].innerHTML;
			l.setAttribute('class','gueng_indice_'+n);
            l.appendChild(a);
            o.appendChild(l);
        }
    }
    div.appendChild(o);
	}
