from IPython.display import Javascript as Js,HTML as Ht ,display as dsp
from gueng.utils import Verifiche
import re,os
DIRE=os.path.dirname(__file__)
class DivDiSintesi():
	js=open(os.path.join(DIRE,'sintesi.js')).read()
	css=open(os.path.join(DIRE,'sintesi.css')).read()
	def __init__(self):
		dsp(Ht('''<div id=DivDiSintesi>
			<p class="DivDiSintesiHead"> Sintesi dei risultati</p>
			</div>
			<script>{js} </script>
			<style>{css} </style>'''.format(**vars(DivDiSintesi))))
	
	def __call__(self,name,obj):
		'''name=descrizione sintesi
		obj=oggetto di test'''
		test=bool(obj)
		test_={True:'Verificato',False:'NON verificato'}[test]
		name_=re.sub('\s+','_',name)

		st='<span id="DivDiSintesi_{0}">  </span>'.format(name_)
		dsp(Ht(st))
		st='DivDiSintesi__call__("{}","{}","{}")'.format(name_,name,test)
		dsp(Js(st))
		
class DivDiVerifica(Verifiche,DivDiSintesi):
	def __init__ (self,*args,**kargs):
		Verifiche.__init__(self,*args,**kargs)
		DivDiSintesi.__init__(self)
	
	def __call__(self,*args,**kargs):
		Verifiche.__call__(self,*args,**kargs)
		if self.red:
			DivDiSintesi.__call__(self,self.uncolorize(self.st[-1]),self.ris[-1])
			return 
		else:
			self.red=True
			DivDiSintesi.__call__(self,self.uncolorize(self.st[-1]),self.ris[-1])
			self.red=False

	def __add__(self,ot,inplace=False):
		out=Verifiche.__add__(self,ot,inplace)
		bkred=self.red
		self.red=True
		for n in range(len(ot)):
			DivDiSintesi.__call__(self,ot.uncolorize(ot.st[n]),ot.ris[n])
		self.red=bkred
		return out
			
		
