import pylab as pl
from io import StringIO as SIO
from binascii import b2a_base64 
import PIL
import numpy as np
import re

ascii_ord=['@', 'W', 'M', 'N', 'B', 'Q', '%', 'D', 'O', '&', '#', 'm', 'R', '8', 'G', 'w', 'H', '6', '9', 'K', 'Z', 'E', 'A', 'U', '0', 'b', 'd', 'g', 'S', '$', 'p', 'q', 'P', 'X', '5', '4', 'V', '3', 'h', 'C', '2', 'a', 'e', 'k', 'o', 'F', 'n', 'u', 'T', '1', 'y', 'Y', 'x', 's', 'z', '7', '=', '>', '<', '{', '}', 'L', 'v', '+', '[', ']', 'f', 't', 'c', 'J', '?', ')', '(', '|', 'j', 'I', 'r', 'l', '/', '\\', '*', '!', 'i', '^', '~', '"', ';', ':', "'", ',', '-', '`', '.', '_', ' ']

class Figure2dat:
	def __init__(self,n=None):
		if n==None:
			self.figure=pl.gcf()
		else:
			self.figure=pl.figure(n)

	def base64(self):
		fi=SIO()
		self.figure.savefig(fi,format='png')
		self.data='data:image/png;base64, {}'.format(b2a_base64(fi.getvalue()))

	def __str__(self):
		if not hasattr(self,'data'): self.base64()
		return self.data

	def ascciart(self,maxw=100,maxh=50):
		fi=SIO()
		self.figure.savefig(fi,format='png')
		im=PIL.Image.open(fi)
		im.convert('L')
		w,h=im.size
		h=int(h/2) #in ascci raddoppia la scala in altezza
		r=w/h
		w,h=maxw,int(maxw/r)
		im=im.resize((w,h),PIL.Image.ANTIALIAS)
		dat=np.array(im.getdata(),dtype=float).mean(1)
		dat=(dat/255*(len(ascii_ord)-1)).astype(int)
		dat=[ascii_ord[i] for i in dat.flat]
		dat=np.array(dat).reshape(h,w)
		st='\n'.join([''.join(j) for j in dat])
		return st

	def __format__(self,val):
		'''b o '': return base64 data
		aa: return asciiart;'''
		if val in ('b' , ''):
			return self.__str__()
		elif 'aa' in val:
			return self.asciiart()
		else: return "".format(val)







	
