# coding: utf-8

import selenium
from selenium.webdriver import Firefox,FirefoxOptions,PhantomJS,Chrome,ChromeOptions
# from selenium.webdriver.common.action_chains import ActionChains
import os,time,tempfile
try:
    import Image
except:
    from PIL import Image
from binascii import b2a_base64
from io import BytesIO
import pypandoc
from scipy import r_

def html2odt(finame,to=10,speed=1,width=950,purge=True):
	'''finame= file html da convertire 
	to=timeout di attesa per caricamento pagina
	speed=moltiplicatore per velocità
	'''
	tdir=tempfile.mkdtemp(dir=os.path.abspath(os.path.dirname(finame)))
	filename=os.path.basename(finame)
	print(f'tdir={tdir}')

	op=FirefoxOptions()
#	op.add_argument('-headless')
	wd=Firefox(options=op)
	wd.set_window_size(width,int(width/3*2))

	url='file://'+os.path.abspath(finame)
	print(f'url={url}')
	wd.get(url)
	time.sleep(to*speed)
	#recupero gli elementi da convertire
#	lists= wd.find_elements_by_css_selector('.plot-container.plotly')
	lists= wd.find_elements_by_css_selector('.plotly-graph-div')
	print(lists)
	print(f'grafici trovati:{len(lists)}')
	for n,el in enumerate(lists):
		wd.execute_script('arguments[0].scrollIntoView()',el)
		time.sleep(1*speed)
		loc=el.location_once_scrolled_into_view
		print(loc)
		size=el.size
		print(size)
		bs=wd.get_screenshot_as_png()
		im=Image.open(BytesIO(bs))
		pos=[loc['x'],loc['y'],loc['x']+size['width'],loc['y']+size['height']]
		pos=[int(i) for i in pos]
		im=im.crop((loc['x'],loc['y'],loc['x']+size['width'],loc['y']+size['height']))
#		im.save(os.path.join(tdir,f'{n:04d}.png'),format='PNG')
		bs=BytesIO()
		im.save(bs,format='PNG')
		src='data:image/png;base64,{}'.format(b2a_base64(bs.getvalue()).decode('ascii'))
		wd.execute_script('''
			var im=document.createElement('img');
			im.setAttribute('src',arguments[1]);
			arguments[0].parentNode.insertBefore(im,arguments[0]);
			console.log(arguments[0]);
			arguments[0].innerHTML="";
			arguments[0].parentNode.removeChild(arguments[0]);''',el,src)

	#rimuovo i grafici plotly
	lists= wd.find_elements_by_css_selector('script')
	for n,i in enumerate(lists):
		tx=wd.execute_script('return arguments[0].innerHTML',i)
		if 'plotly' in tx:
			wd.execute_script('arguments[0].parentNode.removeChild(arguments[0])',i)

	time.sleep(2)
	tx=wd.execute_script('return document.getElementsByTagName("html")[0].innerHTML')
	tx='<html>'+tx+'</html>'
	with open(os.path.join(tdir,'source.html'),'w') as fi:
		fi.write(tx)

	fidocx=os.path.join(tdir,filename+'.docx')
	pypandoc.convert_text(tx,'docx',format='html',outputfile=fidocx)
	pypandoc.convert_file(fidocx,'odt',format='docx',outputfile=finame+'.odt')

	if purge:
		for i in os.listdir(tdir):
			os.remove(os.path.join(tdir,i))
		os.removedirs(tdir)
		wd.quit()

	print('fine')
