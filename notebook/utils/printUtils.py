#-*-encoding:latin-*-
import re
from IPython.display import display,HTML,Markdown

greco='alpha beta gamma delta epsilon zeta eta theta iota kappa lambda mu nu xi omicron pi rho sigma tau upsilon phi chi psi omega'.split()

def st2ht(st):
    if not type(st)==str:
        st='%s'%st
    #st=st.replace('<','&lt;').replace('>','&gt;')
    st=st.replace('<','&lt;').replace('>','&gt;')
    st=re.sub('\[1;32m(.*?)\[0m','<span class=green>\\1</span>',st)
    st=re.sub('\[42m(.*?)\[0m','<span class=green2>\\1</span>',st)
    st=re.sub('\[1;31m(.*?)\[0m','<span class=red>\\1</span>',st)
    st=re.sub('\[1;41m(.*?)\[0m','<span class=red2>\\1</span>',st)
    st=st.replace('\\','&&')
    st=st.replace('\n','&&newline')
    st=re.sub('&&s1(.*?)&&s0','<h1>\\1</h1>',st,re.DOTALL)
    st=re.sub('&&s2(.*?)&&s0','<h2>\\1</h2>',st,re.DOTALL)
    st=re.sub('&&s3(.*?)&&s0','<h3>\\1</h3>',st,re.DOTALL)
    st=st.replace('&&newline','<br>\n')
    st=st.replace('&&','\\')
    st=str(st,'latin')
    st=st.replace('a\'','�')
    st=st.replace('e\'','�')
    st=st.replace('i\'','�')
    st=st.replace('o\'','�')
    st=st.replace('u\'','�')
    
    return '''<style type="text/css">
    .green{color:forestgreen;}
    .green2{background:green;}
    .red{color:red;}
    .red2{background:red;}
    </style>'''+st

def printht(st):
    display(HTML(st2ht(st)))

class Markdown2(Markdown):
	'''Classe per sfruttare anche il Markdown per testo con apici e pedici'''
	def __init__(self,st,*args,**kargs):
		st=re.sub(r'([^\\])\~([^\~\$]*)([^\\])\~',r'\1<sub>\2\3</sub>',st)
		st=re.sub(r'([^\\])\^([^\^\$]*)([^\\])\^',r'\1<sup>\2\3</sup>',st)
		Markdown.__init__(self,st,*args,**kargs)

def displayMarkdown2(st):
	m=Markdown2(st)
	display(m)

import inspect
def printmd(st,*args,**kargs):
	'''printmdForceBr :true|false default true per forzare la sostituzione di "\\n" con "  \\n"'''
	forcebr=True # se truesostituisce automaticamene '\n' con '  \n'
	if type(st)==dict:
		kargs=st
		st='  \n'.join(['{{{}=}}'.format(i) for i in list(st.keys())])
	if not type(st) in [str, str]:
		if hasattr(st,'to_html'):
			st=st.to_html()
		else:
			st='{}'.format(st)
	ka1={}
	for i in args:
		if type(i)==dict:
			ka1.update(i)
	if not kargs:
		ka={}
		ka.update(inspect.currentframe().f_back.f_locals)
	else:
		if 'printmdForceBr' in list(kargs.keys()):
			forcebr= kargs['printmdForceBr']
		k={}
		k.update(inspect.currentframe().f_back.f_locals)
		k.update(kargs)
		ka={}
		ka.update(k)
	ka.update(ka1)
	k=re.findall('\{[^\}]*\}',st)
	#eseguo escape di caratteri speciali per ricerca:
	kk=k[:]
	for n,i in enumerate(kk):
		kk[n]=re.escape(i)
	k=dict(list(zip(kk,k)))
	scale=dict()
	mults={'G': 1000000000.0,
	 'M': 1000000.0,
	 'T': 1000000000000.0,
	 'k': 1000.0,
	 'm': 0.001,
	 'n': 1e-09,
	 'p': 1e-12,
	 'u': 1e-06}
	for i,v in list(k.items()):
		if '=' in i:
			var,form=re.search('{([^=]*)=:?(.*)}',v).groups()
			if var not in list(scale.keys()):
				t= re.search(r'\*(.*)',form)
				if t:
					form=re.search('([^*]*)',form).groups()[0]
					t=t.group()[1:]
					if t in list(mults.keys()):
						scale[var]=1./mults[t]
					else:
						scale[var]=float(t)
			var2=var
			for let in greco:
				for let_ in (let,let.capitalize()):
					if let_ in var2:
						var2=re.sub(let_,'&{};'.format(let_),var2)
			if '_' in var2: var2=var2.replace('_','~')+'~'
			if '^' in var2: var2=var2+'^'
			k[i]='{}= {{{}:{}}}'.format(var2,var,form).replace('\\','')

	for key,val in list(scale.items()):
		key=re.search('[^[]*',key).group()
		ka[key]=ka[key]*val
	for i in k:
		st=re.sub(i,k[i],st)
	st=st.format(*args,**ka)
	st=re.sub('(\d) +(\d)',r'\1 , \2',st)
	if forcebr:
		#dovrebbe evitare il bug dovuto a stringhe in html, senza lascia gli spazzi vuoti sopra al testo html...
		st=re.sub('([^<>])\n',r'\1  \n',st) 
	displayMarkdown2(st)

