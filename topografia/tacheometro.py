
from pylab import *
from scipy import *
from scipy.optimize import leastsq
from scipy.linalg import norm

def rad2cent(ang):
	'converte radianti in gradi centesimali'
	return ang*200/pi

def cent2rad(ang):
	'converte gradi centesimali in radianti'
	return ang*pi/200

def gms2rad(ang):
	'converte gradi,primi,secondi in radianti'
	g,m,s=ang
	return(g+m/60+s/3600)*pi/180

def rad2gms(ang):
	'converte radianti in gradi,primi,secondi'
	ang=rad2deg(ang)
	g=ang
	m=g%1*60
	s=m%1*60
	return r_[ang//1,m//1,s]

class Punto:
	correggi_lm=True #corregge automaticamente la lettura media
	def __init__(self,theta,phi,ls,li,lm=None,comment='',name=None,parent=None):
		vars(self).update(locals())
		if lm is None:
			self.lm=mean([ls,li])
		elif Punto.correggi_lm:
			self.lm=mean([lm,mean([ls,li])])
		if ls<lm:
			raise ValueError('ls={ls:.4g} < li={li:,4g}'.format(**locals()))

	def calcola(self,staz=None):
		a=self.parent.ang_conv
		if staz is None:
			staz=self.parent
		self._d0=staz.K*(self.ls-self.li)*sin(a(self.phi))**2
		self.x=staz.x+self._d0*(sin(a(self.theta)-a(staz.dtheta)))
		self.y=staz.y+self._d0*(cos(a(self.theta)-a(staz.dtheta)))
		self.z=staz.z+self._d0/tan(a(self.phi))+staz.h-self.lm

	def __cmp__(self,val):
		if all(self.co==val.co): return 0
		else:return 1
		
	
	@property
	def d0(self):
		if not hasattr(self,'_d0'):
			self.calcola()
		return self._d0
	
	@property
	def co(self):
		if not hasattr(self,'x'):
			self.calcola()
		return r_[self.x,self.y,self.z]

	def reset(self):
		var='_d0 x y z'.split()
		for i in var:
			del vars(self)[i]

	def __str__(self):
		return'{name: 5d} {co[0]:10.4f} {co[1]:10.4f} {co[2]:10.4f} {comment:s}'.format(co=self.co,**vars(self))


class Stazione(Punto):
	staz_count=0
	def __init__(self,K,h,x=None,y=None,z=None,dtheta=0,name=None,comment='Stazione',ang_conv=cent2rad,andata=None,ritorno=None):
			vars(self).update(locals())
			if  self.x is None:
				for i in 'x y z'.split():
					del vars(self)[i]
			if name is None:
				Stazione.staz_count+=1
				self.name=100*Stazione.staz_count
			self.punti=[]
			self.iang_conv={deg2rad:rad2deg , cent2rad:rad2cent , gms2rad:rad2gms}[ang_conv]

	def battuta(self,theta,phi,ls,li,lm=None,name=None,comment=''):
		if name==None:
			name=self.name+len(self.punti)+1
		self.punti.append(Punto(theta,phi,ls,li,lm,comment,name,self))

	def reset_punti(self):
		for i in self.punti:
			i.reset()

	def to_staz(self,n=-1,K=None,h=None,dtheta=0,name=None,comment='Stazione',ang_conv=None):
		if ang_conv is None: ang_conv=self.ang_conv
		if K is None: K=self.K
		p=self.punti[n]
		x,y,z=p.co
		s=Stazione(K,h,x,y,z,dtheta,name,comment,ang_conv,andata=p)
		self.punti[n]=s
		return self.punti[n]
	
	def orienta(self,n=-1,h=None):
		if type(n)==int:
			self.ritorno=self.punti[n]
			theta=self.ang_conv(self.ritorno.theta)
			del self.punti[n]
		else :
			theta=self.ang_conv(n)
		atteso=2*pi-self.andata.parent.ang_conv(self.andata.theta)
		self.dtheta=theta-atteso
		self.dtheta=self.iang_conv(self.dtheta)
		if h is not None:
			self.h=h

	def __str__(self):
		st=[]
		st.append('''Stazione {name:d}'''.format(**vars(self)))
		st.append('Posizione: {x:10.4f}{y:10.4f}{z:10.4f}'.format(**vars(self)))
		stazioni=[i for i in self.punti if isinstance(i,Stazione)]
		punti=[i for i in self.punti if i not in stazioni]
		if punti:
			st.append('Punti battuti:')
			for i in punti:
				st.append('%s'%i)
		if stazioni:
			st.append('Stazioni collegate:')
			for i in stazioni:
				st.append(Punto.__str__(i))
		return '\n'.join(st)

class PuntoFiduciale(Stazione):
	def __init__(self,x,y,z,name=None,comment=''):
		vars(self).update(locals())
		self.K=.5

	def battuta(self,theta,dist,name=None,comment=''):
		self.punti.append(Punto(theta,0,dist,-dist,None,comment,name,self))

	def reset(self):
		pass
			

class Poligonale:
	def __init__(self):
		self.stazioni=[]
		self.pf=[]
		
	def snelius(self,p1,p2,p3,a1,a2,a3=None,n=-1):
		'''p1,p2,p3 = punti fiduciali
		a1,a2,[a3]: angoli interni o letture sui punti
		n=-1 stazione da posizionare None se non prevista'''
		if isinstance(p1,PuntoFiduciale): 
			p1_=p1.co
			p2_=p2.co
			p3_=p3.co
		else:
			p1_=p1
			p2_=p2
			p3_=p3
		if a3 is not None:
			_a1=a2.theta-a1.theta
			_a2=a3.theta-a1.theta
		else:
			_a1=a1
			_a2=a2
		p=r_[[p1_,p2_,p3_]].mean(0)
		def angoli(p,out=False):
			e1=p1_-p
			e1/=norm(e1)
			e2=p2_-p
			e2/=norm(e2)
			e3=p3_-p
			e3/=norm(e3)
			a1_=arccos(dot(e1,e2))
			a2_=arccos(dot(e2,e3))
			if out: return locals()
			else: return r_[_a1-a1_,_a2-a2_]
		p=leastsq(angoli,p)
		if n is None: return p
		#devo orientare la poligonale...
		locals().update(angoli(p,True))
		#angolo di direzione inverso p1-p
		ang=arccos(dot(p-p1_,r_[0,1]))
		dist=norm(p1-p2)
		p1.battuta(ang,dist)
		self.stazioni[n].andata=p1.punti[-1]
		self.stazioni[n].orienta(a1)

	def intersezione_avanti(self,p1,p2,a1,a2,n=-1):
		v1=p2-p1
		e0=v1/norm(v1)
		p=r_[[p1,p2]].mean(0)
		def angoli(p):
			e1=p-p1
			e1/=norm(e1)
			e2=p-p2
			e2/=norm(e2)
			a1_=arccos(dot(e0,e1))
			a2_=arccos(dot(-e0,e2))
			return r_[a1-a1_,a2-a2_]
		return leastsq(angoli,p)

	

if __name__=='__main__':
	import unittest
	class test_tachometro(unittest.TestCase):
		def setUp(self):
			s=[]
			s.append(Stazione(100,1.23,0,0,0))
			s[0].battuta(0,100,1.10,.9)
			s[0].battuta(100,100,1.10,.9)
			s[0].battuta(200,100,1.10,.9)
			s[0].battuta(300,100,1.10,.9)

			s.append(s[0].to_staz())
			s[1].battuta(200,100,1.10,.9)
			s[1].orienta(-1,1.23)
			s[1].battuta(0,100,1.10,.9)
			self.s=s

		def test_numero_punti(self):
			n=0
			for s in self.s:
				n+=len([i for i in s.punti if i.__class__.__name__=='Punto'])
			self.assertEqual(n,4)

		def test_coordinate_punto(self):
			co=self.s[1].punti[-1].co
			d=norm(co-r_[-40,0,.23*2])
			self.assertLess(d,1e-6)

	unittest.main()
