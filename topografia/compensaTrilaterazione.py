
from pylab import *
from scipy import *
from scipy.linalg import norm
from scipy.optimize import minimize,leastsq
from tkinter import Tk

'''
considero tutte le cooridnate dei nodi in una matrice nx2
impongo che x[0]=0,0 e x[1]=x1,0 #primo lato orizontale e fisso
in rilievo inserisco tutte le lunghezze rilevate
in lati indico i vertici su cui calcolare le lunghezze nell'ordine di rilievo
in x0 indico le coordinate di primo tentativo da utilizzare
'''

class CompensaTrilaterazione:
	def __init__(self,misure,lati,x0=None):
		'''misure=r_[..]misure dei lati effetuate
		lati=r_[[i,j],[i,j]..] lista degli indici dei vertici collegati 
		x0=r_[[x,y],[x,y]] valore di partenza delle coordinate
		'''
		vars(self).update(locals())
		if self.x0 is  None:
			n=max(lati.flat)+1
			a=linspace(0,2*pi,n+1)[:-1]-pi*.5
			r=max(misure)/2
			self.x0=r_[[r_[r*cos(i),r*sin(i)+r] for i in a]]
			e1=r_[self.x0[1]-self.x0[0],0]
			e1/=norm(e1)
			e2=cross((0,0,1.),e1)
			M=r_[[e1[:2]],[e2[:2]]]
			self.x0=apply_along_axis(lambda x:dot(M,x),1,self.x0)

		self.x0_=self.x0.flatten()[r_[2,r_[4:self.x0.size]]]
		self.compensa()

	def l(self,x):
		'''calcolo le lunghezze in base ai dati:
		x=r_[x1,x2,y2,x3,y3...] vettore delle incognite
		il vettore delle incognite da per buono x0=0,0 e y1=0
		ritorna r_[l1,l2..] lunghezze dei lati'''
		#genero la matrice delle coordinate
		xx=r_[0,0,x[0],0,x[1:]].reshape((x.size+3)//2,2)
		#calcolo le lunghezze
		return r_[[norm(xx[i]-xx[j]) for i,j in self.lati]]

	def compensa(self):
		x_=leastsq(lambda x:(self.l(x)-self.misure)/self.misure,self.x0_) #minimizzo errore relativo
		x__=x_[0]
		#ricreo matrice delle coordintae dei nodi
		self.x=r_[0,0,x__[0],0,x__[1:]].reshape((x__.size+3)//2,2)
		self.ll=self.l(x__)
		return self.x

	@property
	def stcad(self):
		'''ritorna stringa per disegno su cad'''
		st= ''.join(['linea\n{},{}\n{},{}\n\n'.format(self.x[i,0],self.x[i,1],self.x[j,0],self.x[j,1]) for i,j in lati])
		cl=Tk()
		cl.clipboard_clear()
		cl.clipboard_append(st)
		cl.destroy()
		return st

	@property
	def err(self):
		return self.ll-self.misure

	def plot(self):
		axis('equal')
		grid(1)
		scatter(self.x0[:,0],self.x0[:,1],color='r',alpha=.3)
		scatter(self.x[:,0],self.x[:,1],color='b')
		for n,i in enumerate(self.lati):
			plot(self.x[i,0],self.x[i,1],'b')
			pt=self.x[i[0]] + (self.x[i[1]]-self.x[i[0]])/3
			rot=rad2deg(arctan(divide(*(self.x[i[1]]-self.x[i[0]])[-1::-1])))
			text(pt[0],pt[1],'{:.2f}'.format(self.ll[n]),fontsize=8,rotation=rot)
		draw()

		

if __name__=='__main__':

	misure=r_[
		577,
		468,
		547,
		447,
		573,
		841]

	lati=r_[[
		[1,2],
		[2,3],
		[3,4],
		[4,1],
		[1,3],
		[4,2]
		]]-1


	x0=r_[[
		[0,0],
		[6,0],
		[4,4],
		[-2,4]
		]]*100.

	cm=CompensaTrilaterazione(misure,lati,x0)
	cm.compensa()
	figure(1)
	show(0)
	clf()
	cm.plot()

	cm=CompensaTrilaterazione(misure,lati)
	cm.compensa()
	cm.plot()
	print(cm.stcad)
	print(cm.err)


