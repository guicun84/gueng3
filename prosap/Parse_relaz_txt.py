#-*-coding:latin-*-
from io import StringIO
from itertools import cycle
import sqlite3
from scipy import *
from scipy.linalg import norm
import re,os,sys
import time
from gueng.utils import floatPath as fp

'''queste routine sono da applicare alla relazione in formato txt ottenuta direttamente da prosap'''
##questo script riesce partendo da una relazione in txt a dividere le tabelle e le passa su oggetti StringIO utilizzabili come fossero file

def checkFirstWord(ri,ls,path='\s+([^ ]+)'):
	'''ri= riga di cui controllare la prima parola
	ls=lista di parole consentite
	ritorna True se la prima parola � in ls'''
	w=re.search(path,ri)
	if not w:return True
	return w.groups()[0] in ls

class Parse_relaz:
	def __init__(self,fi,mode='r'):
		self.mode=mode
		self.fi=open(fi,mode,encoding='latin-1')

	def isola_mat(self):
		'''isola in un ogetto StringIO tutte le righe di relazione che contengono dati sui materiali'''
		print('\tda rivedere')
		out=StringIO()
		self.fi.seek(0)
		fine_doc=False
		n=0
		while True:
			n+=1
			ri=self.fi.readline()
			if ri=='':
				fine_doc=True
				break
			if re.match(' =* *MODELLAZIONE DEI MATERIALI *=*',ri):
				break
		if not fine_doc:
			#comincio a ricercare i dati dei materiali:
			mat=None
			mats=[]
			for i in range(7):
				self.fi.readline()
			while True:
				ri=self.fi.readline()
				if ri=='':break
				if re.match('^ =+$',ri):
					mats.append(mat)
					break
				if re.match(' Codice\s+=',ri):
					if mat is not None:
						mats.append(mat)
					mat=[]
					mat.append(re.search('=\s*(.*)$',ri).groups(1)[0])
				else:
					test=re.search('.*?=\s*(.*)$',ri)
					if test:
						mat.append(test.groups(1)[0])
			for i in mats:
				out.write('\t'.join(i) +'\n')
			out.seek(0)
		return out
						
	############################################################	
	def isola_sez(self):
		out=StringIO()
		self.fi.seek(0)
		dats={}
		#prima fase recupero le descrizioni delle sezioni
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if  check:
				g=re.match('\s+(\d+)',ri)
				if g:
					dats[g.groups()[0]]=[ri]
				elif not checkFirstWord(ri,('pag.')): break
				continue
			if 'Codice   Sigla identificativa' in ri:
				check=True

		#seconda fase recupero dati sezione
		check=True
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if check:
				g=re.search('\s+(\d+)\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})'.format(fp=fp),ri)
				if g:
					dats[g.groups()[0]].append(ri)
				elif not checkFirstWord(ri, ('pag.','Codice')):break
				continue
			if 'Codice	 Area	 Area_V2	Area_V3	 Jt	  J2-2	 J3-3' in ri:
				check=True
				self.fi.readline()

		#terza fase, riassemblo i dati e li passo in output
		for d in list(dats.values()):
			ri=''.join(d)
			desc=re.search('\s+\d+\s+(.*)',d[0]).groups()[0]
			ri='"{}" {}'.format(desc,d[1])
			ri=ri.replace('\n','')
			ri=ri.replace('\r','')
			out.write(ri+'\n')
		out.seek(0)
		return out

	def isola_nodi(self):
		out=StringIO()
		self.fi.seek(0) #riporto puntatore file a inizio
		check=False
		#ricerco inizio tabella nodi
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match('\s+Nodo\s+pos. X\s+pos. Y\s+pos. Z\s+cod.vincoli  Fondazione',ri): 
				self.fi.readline()
				check=True
				break
		#inizio a leggere i nodi
		nodi=[]
		if check:
			while True:
				ri=self.fi.readline()
				if re.match('\s+=+$',ri): 
					break
				g=re.search('^\s+(\d+)\s*(-?\d+\.\d+(?:e-?\d+)?)\s*(-?\d+\.\d+(?:e-?\d+)?)\s*(-?\d+\.\d+(?:e-?\d+)?)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)',ri)
				if g:
					g=g.groups()
					nodi.append('\t'.join(g))
				elif not checkFirstWord(ri, ('Nodo','rig.','[','pag.','+')):break 
				if '======' in ri: break	
		out.write('\n'.join(nodi))
		out.seek(0)
		return out

	def isola_d2(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match('\s+Elem.\s+Cod.\s+NodoI\s+NodoJ\s+Mat.\s+Sez.\s+Rot.\s+Svincoli\s+Wink V\s+Wink O',ri): 
				self.fi.readline()
				check=True
				break
		elementi=[]
		if check:
			while True:
				ri=self.fi.readline()
				if ri=='':break
				if re.match('\s+=+$',ri): 
					break
				g=re.search('\s+(\d+)\s+([A-Z])'+'\s+([0-9.e+\-]+)'*9,ri)
				if g:
					g=g.groups()
					elementi.append('\t'.join(g))
				elif not checkFirstWord(ri, ('Elem.','pag.')):break
		out.write('\n'.join(elementi))
		out.seek(0)
		return out

	def isola_spostamenti(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match('  Nodo   Cmb   trasl. X   trasl. Y   trasl. Z   rotaz. X   rotaz. Y   rotaz. Z',ri): 
				self.fi.readline()
				check=True
				break
		spostamenti=[]
		if check:
			while True:
				ri=self.fi.readline()
				g=re.search('\s+([0-9.e+\-]+)'*8,ri)
				if g:
					g=g.groups()
					spostamenti.append('\t'.join(g))
				elif not checkFirstWord(ri,('Nodo','[','pag.')):break
				if 'Nodo' in ri:
					if not 'trasl.' in ri: break
		out.write('\n'.join(spostamenti))
		out.seek(0)
		return out

	def isola_azioni(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		azioni=[]
		while True:
			ri=self.fi.readline()
			if ri=='':break
#			if re.match(' LEGENDA RISULTATI NODALI',ri): 
			if  'Nodo   Cmb   azione X  azione  Y  azione  Z  azione RX  azione RY  azione RZ' in ri:
				self.fi.readline()
				check=True
				break
		if check:
			while True:
				ri=self.fi.readline()
				if ri=='':break
				g=re.search('\s+({})'.format(fp)*8,ri)
				if g:
					g=g.groups()
					azioni.append('\t'.join(g))
				elif not checkFirstWord(ri,  ('Nodo','[','pag.')): break
				if 'Nodo' in ri:
					if not 'azione' in ri: break

		out.write('\n'.join(azioni))
		out.seek(0)
		return out

	def isola_sol_d2(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
#		stout='============================================================'
		for ri in self.fi.readlines():
			if not check:
				if re.match('\s+(?:Trave|Pil\.|Trv f\.)\s+Cmb\s+Sforzo\s+N\s+Taglio\s+V2\s+Taglio\s+V3\s+Momento\s+T\s+Momento\s+M2\s+Momento\s+M3',ri):
					check=True
			else:
				if re.match('\s+\d+\s+\d+\s+$',ri) or re.match( '\s+{fp}\s+{fp}\s+{fp}\s+{fp}\s+{fp}\s+{fp}\s+{fp}'.format(fp=fp), ri):
					out.write(ri)
					continue
				elif re.match('\s+{}'.format(fp)*5,ri):continue
				elif re.match('\s+{}'.format(fp)*4,ri):continue
				elif not checkFirstWord(ri,('Trave','Pil.','Trv','[','pag.','deform.','[daN' ,'[cm')):
					break
		out.seek(0)
		return out
	
#se va bene nn dovrebbe pi� servire xche incluso in isola_sol_d2
#	def isola_sol_travi_fondazioni(self):
#		print da rivedere
#		out=StringIO()
#		self.fi.seek(0)
#		check=False
#		while True:
#			ri=self.fi.readline()
#			if ri=='':break
#			if re.match(' Trv f. Cmb   Sforzo N  Taglio V2  Taglio V3  Momento T Momento M2 Momento M3',ri): 
#				self.fi.readline()
#				check=True
#				break
#		sol=[]
#		c=cycle([1,2,3,4])
#		cur=c.next()
#		if check:
#			while True:
#				ri=self.fi.readline()
#				if re.match(' =+$',ri) or ri=='' : 
#					break
#				if cur==1:
#					g=re.search('\s+([0-9.e+\-]+)'*2,ri)
#				elif cur in (2,3):
#					g=re.search('\s+([0-9.e+\-]+)'*7,ri)
#				else:
#					g=re.search('\s+([0-9.e+\-]+)'*4,ri)
#				if g:
#					g=g.groups()
#					if cur==1:
#						row=g
#					else:
#						row+=g
#					if cur==4:
#						sol+=['\t'.join(row)]
#					cur=c.next()
#		out.write('\n'.join(sol))
#		return out

	def isola_pressioni_fondazioni_d3(self):
		self.fi.seek(0)
		test=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match('Pressioni indotte da gusci su suolo elastico' ,ri):
				test=True
				break
		#genero una lista con per primo valore il nodo e a seguire tutti i valori di pressione nelle varie cmb di carico
		cur_nod=None
		cur_dat=None
		dat=[]
		if test:
			while True:
				ri=self.fi.readline()
				if ri=='' :break
				#controllo se ho a che fare con dati
				test1=re.match('\s+-?\d+',ri)
				if test1:
					#controllo se iniziano i dati di un nuovo nodo
					te=re.match('\s+[0-9]+',ri[:6])
					if te: #nuovo nodo
						if cur_nod is not None:
							dat.append('{}\t{}'.format(cur_nod,'\t'.join(cur_dat)))
						g=re.finditer('[0-9.e+\-]+',ri)
						cur_nod=int(te.group())
						cur_dat=[i.group() for i in g][1:]
					else: #continuo da nodo precedente
						g=re.finditer('[0-9.e+\-]+',ri)
						cur_dat.extend([i.group() for i in g])
					continue
				if not checkFirstWord(ri,('pag.','Pressioni','Nodo','[','\n'),path='\s*([^\s]+)'):
					break
				if 'Pressioni' in ri:
					if not re.match('Pressioni indotte da gusci su suolo elastico' ,ri): break

			dat.append('{}\t{}'.format(cur_nod,'\t'.join(cur_dat)))
		out=StringIO()
		out.write('\n'.join(dat))
		out.seek(0)
		return out

	def isola_pressioni_fondazioni_d2(self):
		print('\tda controllare')
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			st=self.fi.readline()
			if st=='':break
			if check:
				if re.match('\s+\d+',st): out.write(st)
				else:check=False
			if st[:-1]=='Pressioni indotte da travi su suolo elastico':
				for i in range(3):self.fi.readline()
				check=True
		out.seek(0)
		return out

	def isola_d3(self):
		out=StringIO()
		self.fi.seek(0)
		check1=False
		for st in self.fi:
			if check1:
				if re.match('\s*\d+\s+[SGF]\s+\d+',st):
					out.write(st)
					continue
				elif not checkFirstWord(st,  ('Elem.','pag.')): break
				continue
			if  re.match('\s+Elem.\s+Cod.\s+NodoI\s+NodoJ\s+NodoK\s+NodoL\s+Mat.\s+Spess.\s+Wink V\s+Wink O',st):
				check1=True
				continue
		out.seek(0)
		return out

	def isola_sollecitazioni_d3(self):
		out=StringIO()
		self.fi.seek(0)
		check1=False
		for st in self.fi:
			if check1:
				if re.match('\s+-?\d',st):
					out.write(st)
					continue
				elif not checkFirstWord(st,('Elem.','Sf.N','[','pag.')):
					break
				continue
			if re.match('\s+Elem.\s+Cmb\s+n.\s+nodo\s+Von Mises\s+Sf.N\s+mx\s+Sf.N\s+mn\s+Sf.M\s+mx\s+Sf.M\s+mn' ,st):
				check1=True
				continue
		return out
	
	def isola_combinazioni(self):
		out=StringIO()
		self.fi.seek(0)
		for st in self.fi.readlines():
			if re.match('\s*CMB n\.\s+\d+\s+tipo = .*\s+sigla = .*',st):
				out.write(st)
			else:
				if re.match('\s*CMB n\.\s+\d+\s+(?!tipo=)',st):
					break
		out.seek(0)
		return out

class Loader:
	def __init__(self,fi,rel=None,mode='r'):
		'''fi=file per database [file.sqlite]
		rel='file txt di relazione se none modifica fi in 'file.txt' '''
		if rel==None:
			name=os.path.splitext(fi)
			if name[1].lower()=='.txt':
				rel=fi
				fi=name[0]+'.sqlite'
			else:
				rel=name[0]+'.txt'
		self.db=sqlite3.connect(fi)
		self.relaz=Parse_relaz(rel,mode=mode)

	def crea(self):
		self.db.executescript('''
create table dbinfo (
	name text,
	value);

insert into dbinfo values ("unit","kg-cm");

create  table if not exists materiali (
	id integer primary key,
	tipo text,
	name text,
	E real  ,
	nu real ,
	G real,
	gamma real ,
	alpha real ,
	var text);

create table if not exists sezioni(
	id integer primary key ,
	name text,
	A real,
	AV2 real,
	AV3 real,
	Jt real,
	J22 real ,
	J33 real);

create table  if not exists nodi (
	id integer primary key ,
	x real,
	y real,
	z real,
	vx integer,
	vy integer,
	vz integer,
	rx integer,
	ry integer,
	rz integer,
	fondazione integer);

create table  if not exists d2 (
	id integer primary key,
	tipo text,
	i integer,
	j integer,
	materiale integer ,
	sezione integer,
	rotazione real ,
	svincoli_i text ,
	svincoli_j text,
	winkler_V real ,
	winkler_O real);

create table if not exists sollecitazioni_d2_interne (
	id integer primary key autoincrement,
	elem integer ,
	cmb integer,
	x real,
	N real ,
	V2 real ,
	V3 real,
	M1 real,
	M2 real ,
	M3 real
	);

create table if not exists spostamenti(
	id integer primary key autoincrement,
	nodo integer ,
	cmb integer ,
	vx real,
	vy real,
	vz real,
	rx real ,
	ry real,
	rz real);

create table if not exists azioni(
	id integer primary key autoincrement,
	nodo integer ,
	cmb integer ,
	fx real,
	fy real,
	fz real,
	mx real ,
	my real,
	mz real);

create table if not exists pressioni_fondazione_d3 (
	id integer primary key autoincrement,
	nodo integer,
	cmb integer,
	p real);

create table if not exists pressioni_fondazione_d2 (
	id integer primary key autoincrement,
	elem integer,
	cmb integer,
	Pi real,
	Pf real,
	Pmax real);

create table if not exists sollecitazioni_d3 (
	id integer primary key autoincrement ,
	elem integer ,
	cmb iteger ,
	nodo integer ,
	vonMises real ,
	Nmax real ,
	Nmin real ,
	Mmax real ,
	Mmin real ,
	N11 real ,
	N22 real ,
	N12 real ,
	M11 real ,
	M22 real ,
	M12 real);

create table if not exists d3(
	id integer primary key,
	codice text,
	i integer,
	j integer,
	k integer,
	l integer,
	materiale integer,
	spessore real,
	winkV real,
	winkO real);
	
create table if not exists combinazioni(
	id integer primary key,
	tipo text,
	descrizione text);
	''')

	def crea_indici(self):
		self.db.executescript('''
	create index if not exists nodi_id on nodi(id);
	create index if not exists nodi_xyz on nodi(x,y,z);
	create index if not exists nodi_yz on nodi(y,z);
	create index if not exists nodi_z on nodi(z);

	create index if not exists d2_id on d2(id);
	create index if not exists d2_ij on d2(i,j);
	create index if not exists d2_j on d2(j);

	create index if not exists sold2_elemcmbx on sollecitazioni_d2_interne(elem,cmb,x);
	create index if not exists sold2_cmbx on sollecitazioni_d2_interne(cmb,x);
	create index if not exists sold2_x on sollecitazioni_d2_interne(x);
	create index if not exists sold2_elemx on sollecitazioni_d2_interne(elem,x);
	
	create index if not exists sold3_elemcmb on sollecitazioni_d3(elem,cmb);
	create index if not exists sold3_elemnodocmb on sollecitazioni_d3(elem,nodo,cmb);
	create index if not exists sold3_nodocmb on sollecitazioni_d3(nodo,cmb);
	create index if not exists sold3_cmb on sollecitazioni_d3(cmb);

	create index if not exists d3_idijkl on d3(id,i,j,k,l);
	create index if not exists d3_ijkl on d3(i,j,k,l);
	create index if not exists d3_jkl on d3(j,k,l);
	create index if not exists d3_kl on d3(k,l);
	create index if not exists d3_l on d3(l);

	create index if not exists sp_idncmb on spostamenti(id,nodo,cmb);
	create index if not exists sp_ncmb on spostamenti(nodo,cmb);

	create index if not exists cmb_id on combinazioni(id);
	create index if not exists cmb_id_tipo on combinazioni(id,tipo);
	create index if not exists cmb_tipo on combinazioni(tipo);

	''')
		self.db.commit()

	def drop_all(self):
		self.db.executescript('''drop table if exists dbinfo;
		drop table if exists materiali;
		drop table if exists sezioni;
		drop table if exists nodi;
		drop table if exists d2;
		drop table if exists sollecitazioni_d2_interne;
		drop table if exists spostamenti;
		drop table if exists azioni;
		drop table if exists pressioni_fondazione_d3;
		drop table if exists pressioni_fondazione_d2;
		drop table if exists sollecitazioni_d3;
		drop table if exists d3;
		drop table if exists combinazioni;
		drop table if exists infod2;
		drop table if exists infod3;

		drop index if exists nodi_id;
		drop index if exists nodi_xyz;
		drop index if exists nodi_yz;
		drop index if exists nodi_z;
		drop index if exists d2_id;
		drop index if exists d2_ij;
		drop index if exists d2_i;
		drop index if exists sold2_elemcmbx;
		drop index if exists sold2_cmbx;
		drop index if exists sold2_x;
		drop index if exists sold2_elemx;
		drop index if exists sold3_elemcmb;
		drop index if exists sold3_elemnodocmb;
		drop index if exists sold3_nodocmb;
		drop index if exists sold3_cmb;
		drop index if exists d3_idijkl;
		drop index if exists d3_ijkl;
		drop index if exists d3_jkl;
		drop index if exists d3_kl;
		drop index if exists d3_l;
		drop index if exists cmb_id;
		drop index if exists cmb_id_tipo;
		drop index if exists cmb_tipo;
		drop index if exists infod2id;
		drop index if exists infod3id;
		''')

		if self.db.execute('select 1 from sqlite_master where type="table" and name="sollecitazioni_d2"').fetchone():
			self.db.execute('drop table sollecitazioni_d2')
		self.db.commit()


	def load_mat(self,com=True):
		mat=self.relaz.isola_mat().getvalue().split('\n')
		k=0
		for n in range(len(mat)):
			if mat[n]=='':continue
			m=mat[n-k]
			dat=m.split('\t')
			if True: #len(dat)>=9: #non capisco pi� questa opzione....
				dat=[int(dat[0])]+[dat[i] for i in [1,2]]+[float(dat[i]) for i in range(3,len(dat))]+[' ,'.join(dat[8:])]
				mat[n]=dat
			else:
				del mat[n-k]
				k+=1
		if len(mat)==0: return
		for i in mat:
			if i=='':continue
			kk=min(9,len(i))
			self.db.execute('insert into materiali values(' + ','.join(['?']*kk) + ')',i[:kk])
		if com:self.db.commit()

	def load_sez(self,com=True):
		sez=self.relaz.isola_sez().getvalue()
		sez=sez.split('\n')
		for n,r in enumerate(sez):
			gr=re.search('"([^"]+)"\s+(.*)',r)
			if not gr:continue
			dat=gr.groups()[1].split()
			if len(dat)!=7:
				continue
			else:
				dat=[int(dat[0]),gr.groups()[0]]+[float(i) for i in dat[1:]]
				self.db.execute('insert into sezioni values(?,?,?,?,?,?,?,?)',dat)
		if len(sez)==0: return
#		self.db.executemany('insert into sezioni values(?,"",?,?,?,?,?,?)',sez)
		if com:	self.db.commit()

	def load_nodi(self,com=True):
		nodi=self.relaz.isola_nodi().getvalue().split('\n')
		for n,r in enumerate(nodi):
			dat=r.split('\t')
			if len(dat)!=11:
				del nodi[n]
			else:
				nodi[n]=[int(dat[0])]+[float(dat[i]) for i in range(1,4)] + [int(dat[i]) for i in range(4,11)]
		if len(nodi)==0:return 
		self.db.executemany('insert into nodi values(?,?,?,?,?,?,?,?,?,?,?)',nodi)
		if com:self.db.commit()

	def load_d2(self,com=True):
		d2=self.relaz.isola_d2().getvalue().split('\n')
		for n,d in enumerate(d2):
			dat=d.split('\t')
			if len(dat)!=11:
				del d2[n]
			else:
				d2[n]=[int(dat[0]),dat[1]]+[int(dat[i]) for i in range(2,6)] + [float(dat[6]),dat[7],dat[8]] +[float(dat[i]) for i in range(9,11)]
		if len(d2)==0:return 
		self.db.executemany('insert into d2 values(?,?,?,?,?,?,?,?,?,?,?)',d2)
		if com: self.db.commit()

	def load_sol_d2(self,com=True):
		dati=self.relaz.isola_sol_d2().getvalue().split('\n')	
		dat=[]
		for ri in dati:
			test= re.search('^\s+(\d+)\s+(\d+)\s*$',ri)
			if test:
				el,cmb=[int(i) for i in test.groups()]
				continue
			test= re.search('({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s+({fp})\s*$'.format(fp=fp),ri)
			if test:
				temp=[el,cmb]
				temp.extend([float(i) for i in test.groups()])
					
				dat.append(temp)
				continue
		self.db.executemany('insert into sollecitazioni_d2_interne(elem,cmb,x,N,V2,V3,M1,M2,M3) values (?,?,?,?,?,?,?,?,?)',dat)
		if com: self.db.commit()



#	def load_sol_d2(self,com=True):
#		pil=self.relaz.isola_sol_pilastri().getvalue().split('\n')
#		tr=self.relaz.isola_sol_travi().getvalue().split('\n')
#		for n,p in enumerate(pil):
#			dat=p.split('\t')
#			if len(dat)!=16:
#				del pil[n]
#			else:
#				pil[n]=[int(dat[i]) for i in xrange(2)]+[float(dat[i]) for i in xrange(2,16)]
##				self.db.execute('insert into sollecitazioni_d2 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',pil[n])
#		if len(pil)!=0: 
#			self.db.executemany('insert into sollecitazioni_d2 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',pil)
#		# 'finito con i pilastri'
#		for n,t in enumerate(tr):
#			dat=t.split('\t')
#			if len(dat)!=21:
#				del tr[n]
#			else:
#				tr[n]=[int(dat[i]) for i in xrange(2)]+[float(dat[i]) for i in xrange(2,16)]
##				self.db.execute('insert into sollecitazioni_d2 values(?,?,?,?,?,?,?,?)',tr[n])
#		if len(tr)!=0: 
#			self.db.executemany('insert into sollecitazioni_d2 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',tr)
##		self.db.executemany('insert into sollecitazioni_d2 values(?,?,?,?,?,?,?,?)',tr)
#		trf=self.relaz.isola_sol_travi_fondazioni().getvalue().split('\n')
#		for n,t in enumerate(trf):
#			dat=t.split('\t')
#			if len(dat)!=20:
#				del trf[n]
#			else:
#				trf[n]=[int(dat[i]) for i in xrange(2)]+[float(dat[i]) for i in xrange(2,16)]
##				self.db.execute('insert into sollecitazioni_d2 values(?,?,?,?,?,?,?,?)',tr[n])
#		if len(trf)!=0:
#			self.db.executemany('insert into sollecitazioni_d2 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',trf)
##		self.db.executemany('insert into sollecitazioni_d2 values(?,?,?,?,?,?,?,?)',tr)
#		if com:self.db.commit()
	
	def load_spostamenti(self,com=True):
		sp=self.relaz.isola_spostamenti().getvalue().split('\n')
		for n,s in enumerate(sp):
			dat=s.split('\t')
			if len(dat)!=8:
				del sp[n]
			else:
				sp[n]=[int(dat[i]) for i in range(2)]+[float(dat[i]) for i in range(2,8)]
		if len(sp)==0:return 
		self.db.executemany('insert into spostamenti values(Null,?,?,?,?,?,?,?,?)',sp)
		if com: self.db.commit()

	def load_azioni(self,com=True):
		az=self.relaz.isola_azioni().getvalue().split('\n')
		for n,s in enumerate(az):
			dat=s.split('\t')
			if len(dat)!=8:
				del az[n]
			else:
				az[n]=[int(dat[i]) for i in range(2)]+[float(dat[i]) for i in range(2,8)]

		if len(az)==0:return 
		self.db.executemany('insert into azioni values(Null,?,?,?,?,?,?,?,?)',az)
		if com: self.db.commit()
	
	def load_pressioni_fondazione_d3(self,com=True):
		p3=self.relaz.isola_pressioni_fondazioni_d3().getvalue().split('\n')
		for n,i in enumerate(p3):
			dat=i.split()
			for cm,val in enumerate(dat[1:]): 
				self.db.execute('insert into pressioni_fondazione_d3 values(Null,?,?,?)',(int(dat[0]),cm+1,float(val)))
		if com:
			self.db.commit()
	
	def load_pressioni_fondazione_d2(self,com=True):
		'''com=True: esegue automaticamente il commit del db'''
		p2=self.relaz.isola_pressioni_fondazioni_d2().getvalue().split('\n')[:-1]
		if not p2:return
		for n,i in enumerate(p2):
			i=i.split()
			p2[n]=[int(i[0]),int(i[1]),float(i[2]),float(i[3]),float(i[4])]
		self.db.executemany('insert into pressioni_fondazione_d2 values(Null,?,?,?,?,?)',p2)
		if com: self.db.commit()

	def load_sollecitazioni_d3(self,com=True):
		datas=self.relaz.isola_sollecitazioni_d3().getvalue().split('\n')
#		floatpath='(-?\d+\.?\d*(?:e-?\d+)?)'
#		startpath=re.compile('\s+(d+)\s+(d+)\s+(d+)\s+'+'\s+'.join([floatpath]*5))
#		datapath=re.compile('\s+'+'\s+'.join([floatpath]*6))
		dati=[]
		n=0
		el=None
		for i in datas:
			if i=='':continue
			datp=[float(j) for j in i.split()]
			if len(datp)==8:
				n=0
			if n==0: 
				da=datp
				da[:3]=[int(j) for j in da[:3]]
				el,cmb=da[:2]
			elif n%2: #riga dispari completo la precedente
				da.extend(datp)
				dati.append(da)
			else: #riga pari, nuovo nodo con el e cmb precedente
				da=[el,cmb] + datp
				da[2]=int(da[2])
			n+=1
			if len(dati)>100000:
				self.db.executemany('insert into sollecitazioni_d3 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',dati)
				del dati
				dati=[]

		self.db.executemany('insert into sollecitazioni_d3 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',dati)
		if com:self.db.commit()

	def load_d3(self,com=False):
		datas=self.relaz.isola_d3().getvalue().split('\n')
		dati=[]
		for i in datas:
			if i=='':continue
			i=i.split()
			if len(i)==9:
				tmp=i[:5]
				tmp.append(i[4])
				tmp.extend(i[5:])
				i=tmp
			for j in 0,2,3,4,5,6: i[j]=int(i[j])
			for j in 7,8,9:i[j]=float(i[j])
			dati.append(i)
		self.db.executemany('insert into d3 values(?,?,?,?,?,?,?,?,?,?)',dati)
		if com:self.db.commit()

	def load_combinazioni(self,com=False):
		datas=self.relaz.isola_combinazioni().getvalue().split('\n')
		dati=[]
		datpath=re.compile('CMB n\.\s+(\d+)\s+tipo = (.*)\s+sigla = (.*)')
		for i in datas:
			if i=='':continue
			tm=list(datpath.search(i).groups())
			tm[0]=int(tm[0])
			dati.append(tm)
		self.db.executemany('insert into combinazioni values(?,?,?)',dati)
		if com:self.db.commit()

	@staticmethod
	def elaboraD2(xi,yi,zi,xj,yj,zj,rot,n):
		i=r_[xi,yi,zi].astype(float)
		j=r_[xj,yj,zj].astype(float)
		if n==9:
			return norm(j-i)
		e1=j-i
		l=norm(e1)
		e1/=l
		e3=cross(e1,(0,0,1.))
		if norm(e3)==0:
			e3=cross(e1,(-1.,0,0))
		e3/=norm(e3)
		e2=cross(e3,e1)
		if rot!=0:
			rot=deg2rad(rot)
			Mx=r_[[[1,0,0],[0,cos(rot),sin(rot)],[0,-sin(rot),cos(rot)]]]
			e1,e2,e3=dot(Mx,hstack((e1,e2,e3)).reshape(3,3))
		if n<3: return e1[n]
		elif n<6: return e2[n-3]
		elif n<9: return e3[n-6]
		if n==9: return  l

	@staticmethod
	def elaboraD3(xi,yi,zi,xj,yj,zj,xk,yk,zk,xl,yl,zl,n):
		i=r_[xi,yi,zi]
		j=r_[xj,yj,zj]
		k=r_[xk,yk,zk]
		l=r_[xl,yl,zl]
		if norm(k-l)<1e-2:
			l=i
		if n==9: #calcolo area
			return (norm(cross(j-i,k-j)) + norm(cross(i-l,k-l)))/2
		l1=j-i
		l3=k-l
		e1=l1+l3
		e1/=norm(e1)
		l2=k-j
		e3=cross(e1,l2)
		e3/=norm(e3)
		e2=cross(e3,e1)
		if n<3: return e1[n]
		elif n<6: return e2[n-3]
		elif n<9: return e3[n-6]


	def elabora_elementi(self,force=False,com=False):
		count=self.db.execute('select count(*) from d2').fetchone()[0]
		if count==0:
			print ('\tdb vuoto, eseguo commit per poter procedere')
			self.db.commit()
			count=self.db.execute('select count(*) from d2').fetchone()[0]
		print(('\ttrovati {} elementi'.format(count)))
		test=self.ex('''
			select sum(n) from (
				select 1 as n from sqlite_master where name="infod2" and type="table" 
				union all select 1 as n from sqlite_master where name="infod3" and type="table"
				)''').fetchone()[0]
		if test!=2 or force:
			self.db.create_function('elaboraD2',8,self.elaboraD2)
			self.db.create_function('elaboraD3',13,self.elaboraD3)
			q='''
			drop table if exists infod2;
			drop index if exists ifnod2id;

			create table infod2 as select
				d2.id as id,
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 0) as r11 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 1) as r12 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 2) as r13 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 3) as r21 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 4) as r22 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 5) as r23 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 6) as r31 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 7) as r32 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 8) as r33 , 
				elaboraD2(i.x , i.y , i.z , j.x , j.y , j.z , rotazione, 9) as l  
				from d2 
				inner join nodi i on d2.i=i.id 
				inner join nodi j on d2.j=j.id;
			
			create index  ifnod2id on infod2(id);
			
			drop table if exists infod3;
			drop index if exists ifnod3id;

			create table infod3 as select
				d3.id as id,
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 0) as r11 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 1) as r12 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 2) as r13 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 3) as r21 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 4) as r22 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 5) as r23 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 6) as r31 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 7) as r32 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 8) as r33 , 
				elaboraD3(i.x , i.y , i.z , j.x , j.y , j.z , k.x , k.y , k.z , l.x , l.y , l.z , 9) as A,
				Null as macro
				from d3
				inner join nodi i on d3.i=i.id 
				inner join nodi j on d3.j=j.id
				inner join nodi k on d3.k=k.id
				inner join nodi l on d3.l=l.id ;
				
			create index  infod3id on infod3(id);

				'''
#			for i in q.split(';'):
#				try:
#					self.ex(i)
#				except Exception as e:
#					print i
#					print e
#					break
			self.db.executescript(q)
			count=self.db.execute('select "infod2" as type, count(*) as n from infod2 union all select "infod3" as type, count(*) as n from infod3').fetchall()
			print (count)
			if self.db.execute('select 1 from sqlite_master where name="sollecitazioni_d2_interne" and type="table"').fetchone():
				q='''
				create temp view maxl as
					select elem,max(x) as l from (
						select * from  sollecitazioni_d2_interne
						where cmb=(select min(cmb) from sollecitazioni_d2_interne)
					) group by elem;
				
				update  infod2
				set l=(select l from maxl where elem= infod2.id)
				where exists (select l from maxl where elem=infod2.id) ;

				drop view maxl;

			create view if not exists sollecitazioni_d2 as select
				s1.id,
				s1.elem,
				s1.cmb,
				s1.x as x1,
				s1.N as N1,
				s1.V2 as V21,
				s1.V3 as V31,
				s1.M1 as M11,
				s1.M2 as M21,
				s1.M3 as M31,
				s2.x as x2,
				s2.N as N2,
				s2.V2 as V22,
				s2.V3 as V32,
				s2.M1 as M12,
				s2.M2 as M22,
				s2.M3 as M32 from 
				(select * from sollecitazioni_d2_interne where x=0 ) s1 inner join
				(select * from sollecitazioni_d2_interne s inner join infod2 i on s.elem=i.id and s.x=i.l) s2
				on s1.elem=s2.elem and s1.cmb=s2.cmb;
				'''
			else:
				q='''update  infod2
				set l=(select x2 from sollecitazioni_d2 where elem= infod2.id)
				where exists (select x2 from sollecitazioni_d2 where elem=infod2.id)
				;'''
			self.db.executescript(q)
#			for i in q.split(';'):
#				try:
#					self.ex(i)
#				except Exception as e:
#					print i
#					print e
#					break

			#isolo i maschi murari e le piastre:
			if self.db.execute('select 1 from d3 limit 1').fetchone(): #se ci sono d3 da elaborare
				def testplane(r1,r2,r3,rr1,rr2,rr3):
					return abs(abs(r1*rr1+r2*rr2+r3*rr3)-1)

				self.db.create_function('testplane',6,testplane)
				ermax=1e-2
				n=-1
				query='''
				drop view if exists used;
				create temp view used as select id from infod3 where macro ={n};

				drop view if exists usednodi;
				create temp view usednodi as
					select i as nodo from d3 where id in (select * from used)
					union all
					select j as nodo from d3 where id in (select * from used)
					union all
					select k as nodo from d3 where id in (select * from used)
					union all
					select l as nodo from d3 where id in (select * from used)
					;

				drop view if exists new;
				create temp view new as 
					select d3.id from d3
					inner join infod3 id3 on d3.id=id3.id
					where 
					macro is Null and
					testplane({r1},{r2},{r3},r31,r32,r33)<={er} and
					( 
						i in (select * from usednodi) or 
						j in (select * from usednodi) or 
						k in (select * from usednodi) or 
						l in (select * from usednodi)
					)
					;
				update infod3 set macro={n} where id in (select * from new);
				'''
				while True:
					n+=1
					start=self.db.execute('select id from infod3 where macro is Null limit 1').fetchone()[0]
					r1,r2,r3=self.db.execute('select r31,r32,r33 from infod3 where id=?',(start,)).fetchone()
					self.db.execute('update infod3 set macro=? where id=?',(n,start))
					used=1
					while True:
						self.db.executescript(query.format(n=n,r1=r1,r2=r2,r3=r3,er=ermax))
						nu= self.db.execute('select count(*) from infod3 where macro=?',(n,)).fetchone()[0]
						if nu==used: break
						else: used=nu
					if not self.db.execute('select count(*) from infod3 where macro is Null limit 1').fetchone()[0]:
						break

			if com:self.db.commit()


	def load(self):
		ts0=time.time()
		ts=time.time()
		print('carico combinazioni')
		self.load_combinazioni(0)
		print('carico materiali')
		self.load_mat(0)
		print('carico sezioni')
		self.load_sez(0)
		print('carico nodi')
		self.load_nodi(0)
		print('carico d2')
		self.load_d2(0)
		print('carico d3')
		self.load_d3(0)
		print('carico spostamenti')
		self.load_spostamenti(0)
		print('carico azioni')
		self.load_azioni(0)
		print('carico sollecitazioni d2')
		self.load_sol_d2(0)
		print('carico pressioni_fondazione_d2')
		self.load_pressioni_fondazione_d2(0)
		print('carico sollecitazioni d3')
		self.load_sollecitazioni_d3(0)
		print('carico pressioni_fondazione_d3')
		self.load_pressioni_fondazione_d3(0)
		print('durata carcicamento db {}'.format(time.time()-ts))
		ts=time.time()
		print('elaborazione elementi')
		self.elabora_elementi()
		print('creo indici')
		self.crea_indici()
		print('salvataggio')
		self.db.commit()
		print('durata elaborazione db {}'.format(time.time()-ts))
		print('durata totale db {}'.format(time.time()-ts0))
		print('\nDimensione db\n{}'.format(self.dimensioniDb()))

	def ex(self,st,args=[]):
		return self.db.execute(st,args)
	
	def ex_(self,st,args=[]):
		return self.ex(st,args).fetchall()

	def scala_valori(self,E,gamma,A,J,x,F,M,p):
		'''moltiplica i valori delle tabelle per i corrispondenti moltiplicatori
		sono necessari i seguenti moltiplicatori:
		E modulo elastico
		gamma peso proprio
		A Area
		J momenti inerzia
		x coordinate
		F Forze
		M Momenti
		p pressioni'''

		print('ancora da fare per le sollecitazioni d3')
		q='''
			update materiali set E=E*{E},G={E},gamma=gamma*{gamma};
			update sezioni set A=A*{A} , AV2=AV2*{A} , AV3=AV3*{A} , Jt=Jt*{J} , J22=J22*{J} , J33=J33*{J};
			update nodi set x=x*{x},y=y*{x},z=z*{x};
			update sollecitazioni_d2_interne set
				x=x*{x},
				N=N*{F},
				V2=V2*{F},
				V3=V3*{F},
				M1=M1*{M},
				M2=M2*{M},
				M3=M3*{M};
			update spostamenti set vx=vx*{x},vy=vy*{x},vz=vz*{x};
			update azioni set fx=fx*{F},fy=fy*{F}, fz=fz*{F} , mx=mx*{M},my=my*{M},mz=mz*{M};
			update pressioni_fondazione_d3 set p=p*{p};
			update pressioni_fondazione_d2 set Pi=Pi*{p} , Pf=Pf*{p}, Pmax=Pmax*{p};
			update infod2 set l=l*{x};
			update infod3 set A=A*{A};
			/*
			*/
		'''.format(**locals())

		self.db.executescript(q)
		self.db.commit()

	def scala_kgcm_Nmm(self):
		#test di verifica unit�_
		unit=self.ex_('select value from dbinfo where name=="unit"')[0][0]
		if not unit in ('Kg-cm','kg-cm'):
			print('errore unit�: unit� in uso {}'.format(unit))
			return
		self.db.execute('update dbinfo set value="N-mm" where name="unit"')
		self.scala_valori(
			E=.1,
			gamma=.01,
			A=100,
			J=1e4,
			x=10,
			F=10,
			M=100,
			p=.1,
			)


	def scala_Nmm_kgcm(self):
		unit=self.ex_('select value from dbinfo where name=="unit"')[0][0]
		if not unit=='N-mm':
			print('errore unit�:unit� in uso {}'.format(unit))
			return
		self.db.execute('update dbinfo set value="kg/cm" where name="unit"')
		self.scala_valori(
			E=10,
			gamma=100,
			A=.01,
			J=1e-4,
			x=.1,
			F=.1,
			M=.01,
			p=10,
			)

	def dimensioniDb(self):
#		tabs='dbinfo; materiali; sezioni; nodi; d2; sollecitazioni_d2_interne; spostamenti; azioni; pressioni_fondazione_d3; pressioni_fondazione_d2; sollecitazioni_d3; d3; combinazioni; infod2; infod3'.split(';')
		tabs=self.db.execute('select name from sqlite_master where type="table" and name not like "sqlite_%" ').fetchall()
		q='create view if not exists dimensioniDB as ' +' union all\n'.join(['select "{t}" as tab, count(*) as num from {t}'.format(t=t[0]) for t in tabs])
		q+='\nunion all\nselect \'macro\' as tab, max(macro)+1 as num from infod3'
		self.db.execute(q)
		ris=list(self.db.execute('select * from dimensioniDB').fetchall())
		for n,i in enumerate(ris):
			if i[1] is None:
				ris[n]=(i[0],0)
		return '\n'.join(['{:>30} : {:>6}'.format(*i) for i in ris])

 	
if __name__=='__main__':
#	p=parse_relaz('relazione__.txt')
#	mat=p.isola_mat()
#	print mat.getvalue()
#	sez=p.isola_sez()
#	print sez.getvalue()
#	nodi=p.isola_nodi()
#	print nodi.getvalue()
#	travi=p.isola_travi()
#	print travi.getvalue()
#	spost=p.isola_spostamenti()
#	n=len(spost.getvalue().split('\n'))
#	print n
#	solp=p.isola_sol_pilastri()
#	sp=solp.getvalue()
#
#	sp=sp.split('\n')
#	print len(sp)
#
#	solt=p.isola_sol_travi()
#	st=solt.getvalue()
#	st=st.split('\n')
#	print len(st)
#	print st[0]
#	print st[-1]

#	import os
	db=Loader('prova.sqlite','relazione__.txt')
	db.db.execute('drop table materiali')
	db.db.execute('drop table sezioni')
	db.db.execute('drop table nodi')
	db.db.execute('drop table d2')
	db.db.execute('drop table sollecitazioni_d2')
	db.db.execute('drop table spostamenti')
	db.db.commit()
	db.crea()
	db.load()
	db.scala_Kgcm_Nmm()
	print(db.ex_('select count(*) from materiali'))
	print(db.ex_('select count(*) from sezioni'))
	print(db.ex_('select count(*) from nodi'))
	print(db.ex_('select count(*) from d2'))
	print(db.ex_('select count(*) from sollecitazioni_d2'))
	print(db.ex_('select count(*) from sollecitazioni_d2 where elem=22'))
	print(db.ex_('select count(*) from sollecitazioni_d2 where elem=149'))
	print(db.ex_('select count(*) from spostamenti'))

