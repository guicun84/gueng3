#-*-coding:utf-8-*-

import sqlite3
from Parse_relaz_txt_from_doc import *
class loader:
	def __init__(self,fi):
		self.db=sqlite3.connect(fi)
	def drop_all(self):
		self.db.executescript('''drop table if exists materiali;
		drop table if exists sezioni;
		drop table if exists nodi;
		drop table if exists travi;
		drop table if exists sollecitazioni;
		''')
	def load_materiali(self,fi):
		if type(fi)==str:
				fi=open(fi)
		self.db.execute('create table  if not exists materiali  (id integer prmary key, note text, E real,ni real,G real,ro real,alpha real,other blob)')
		mat=None
		def carica_materiale(dati,var):
			dati.append(';'.join(var))
			self.db.execute('insert into materiali values(?,?,?,?,?,?,?,?)',tuple(dati))
		while True:
			st=fi.readline()
			if st=='':break
			st=st.replace('\n','')
			if st=='':continue
			if st[0].isdigit():
				if mat is not None:
					carica_materiale(dati,var)
				st=st.split('\t')
				dati=st[:2]+st[3:]
				mat=st[0]
				var=[]
			else:
				if mat is not None:
					st=st.split('\t')
					var.append('='.join(st[1:3]))
		carica_materiale(dati,var)			
		self.db.commit()
		fi.close()

	def load_sezioni(self,fi):
		#carico le sezioni degli elementi
		if type(fi)==str:
			fi=open(fi)
		self.db.execute('create table if not exists sezioni (id integer primary key, tipo text, A real, AV2 real ,AV3 real, Jt real , J22 real, J33 real, W22 real, W33 real, Wp22 real, W33p real)')
		while True:
			st=fi.readline()
			if st=='':break
			if st[0].isdigit():
				st=st.replace('\n','')
				st=st.split('\t')
				self.db.execute('insert into sezioni values (?,?,?,?,?,?,?,?,?,?,?,?)',tuple(st))
		self.db.commit()	


	#caricamento dei nodi
	def load_nodi(self,fi):
		if type(fi)==str:
			fi=open(fi)
		self.db.execute('create table if not exists nodi (id integer primary key, X real, Y real, Z real, Note, Rig_TX real, Rig_TY real, Rig_TZ real, Rig_RX real, Rig_RY real, Rig_RZ real)')
		self.db.execute('create table if not exists nodi (id integer primary key,x real , y real, z real)')
		while True:
			st=fi.readline()
			if st=='':break
			if st[0].isdigit():
				st=st.replace('\n','')
				st=st.split('\t')
				p=[st[i*4:(i+1)*4] for i in range(3)]
				for i in p:
					if i[0]:
						self.db.execute('insert into nodi (id,X,Y,Z) values(?,?,?,?)',tuple(i))
		self.db.commit()				
		fi.close()

	def load_nodi_vincolati(self,fi):
		if type(fi)==str:
			fi=open(fi,'r')

		self.db.execute('create table if not exists nodi (id integer primary key, X real, Y real, Z real, Note, Rig_TX real, Rig_TY real, Rig_TZ real, Rig_RX real, Rig_RY real, Rig_RZ real)')

	def load_travi(self,fi):
		if type(fi)==str:
			fi=open(fi)
		self.db.execute('create table if not exists travi (id integer primary key, note text, i integer,j integer,mat integer , sez integer, rot real, svi text, svj text ,winkv real, winco real)')
		while True:
			st=fi.readline()
			if st=='':break
			if st[0].isdigit():
				st=st.replace('\n','')
				st=st.split('\t')
				self.db.execute('insert into travi values (?,?,?,?,?,?,?,?,?,?,?)',tuple(st))
		self.db.commit()		
	
	def load_soll_travi(self,fi):
		if type(fi)==str:
			fi=open(fi)
		self.db.execute('''create table if not exists sollecitazioni ( id integer primary key autoincrement, elem integer, cmb integer, M3max real, M3min real, M2max real, M2min real, D2 real, D3 real, Q2 real, Q3 real, pos_1  real, N_1 real, V2_1 real, V3_1 real, T_1 real, M2_1 real, M3_1 real, pos_2  real, N_2 real, V2_2 real, V3_2 real, T_2 real, M2_2 real, M3_2 real)''')
		el=None
		cmb=None
		def carica_sollecitazioni(el,cmb,dat1,dat2):
			dat=[el,cmb]
			for i in zip(dat1[:4],dat2[:4]):
				dat.extend(i[:])
			dat.extend(dat1[4:])
			dat.extend(dat2[4:])
			self.db.execute('insert into sollecitazioni values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',tuple(dat))
		while True:
			st=fi.readline()
			if st=='':break
			if st[0].isdigit():
				if el:
					#carico i risultati
					carica_sollecitazioni(el,cmb,dat1,dat2)
				st=st.replace('\n','')
				st=st.split('\t')
				el=st[0]
				cmb=st[1]
				dat1=st[2:]

			else:
				if el:
					st=st.replace('\n','')
					st=st.split('\t')
					dat2=st[2:]
		self.db.commit()
		fi.close()
		
	def scala_Nmm(self):
		#uso questa funzione per scalare in MPa Newton  mm e Nmm tutto
		#matereriali
		self.db.execute('update materiali set E=E*.1')
		self.db.execute('update materiali set G=G*.1')
		#sezioni
		self.db.execute('update sezioni set A=A*100')
		self.db.execute('update sezioni set AV2=AV2*100')
		self.db.execute('update sezioni set AV3=AV3*100')
		self.db.execute('update sezioni set Jt=Jt*10000 ')
		self.db.execute('update sezioni set J22=J22*10000 ')
		self.db.execute('update sezioni set J33=J33*10000')
		self.db.execute('update sezioni set W22=W22*1000 ')
		self.db.execute('update sezioni set W33=W33*1000 ')
		self.db.execute('update sezioni set Wp22=Wp22*1000 ')
		self.db.execute('update sezioni set W33p=W33p*1000')
		#nodi
		self.db.execute('update  nodi set x=x*10')
		self.db.execute('update  nodi set y=y*10')
		self.db.execute('update  nodi set z=z*10')
		#sollecitazioni

		self.db.execute('update sollecitazioni set M3max=M3max*100  ')
		self.db.execute('update sollecitazioni set M3min=M3min*100  ')
		self.db.execute('update sollecitazioni set M2max=M2max*100  ')
		self.db.execute('update sollecitazioni set M2min=M2min*100  ')
		self.db.execute('update sollecitazioni set D2=D2*10  ')
		self.db.execute('update sollecitazioni set D3=D3*10  ')
		self.db.execute('update sollecitazioni set Q2=Q2*10  ')
		self.db.execute('update sollecitazioni set Q3=Q3*10  ')
		self.db.execute('update sollecitazioni set pos_1 =pos_1*10  ')
		self.db.execute('update sollecitazioni set N_1=N_1*10  ')
		self.db.execute('update sollecitazioni set V2_1=V2_1*10  ')
		self.db.execute('update sollecitazioni set V3_1=V3_1*10  ')
		self.db.execute('update sollecitazioni set T_1=T_1*100  ')
		self.db.execute('update sollecitazioni set M2_1=M2_1*100  ')
		self.db.execute('update sollecitazioni set M3_1=M3_1*100  ')
		self.db.execute('update sollecitazioni set pos_2 =pos_2 *10  ')
		self.db.execute('update sollecitazioni set N_2=N_2*10  ')
		self.db.execute('update sollecitazioni set V2_2=V2_2*10  ')
		self.db.execute('update sollecitazioni set V3_2=V3_2*10  ')
		self.db.execute('update sollecitazioni set T_2=T_2*100  ')
		self.db.execute('update sollecitazioni set M2_2=M2_2*100  ')
		self.db.execute('update sollecitazioni set M3_2=M3_2*100  ')
		self.db.commit()	

	def scala_Kgcm(self):
		#uso questa funzione per sacalre in Kg/cm^2 Kg e cm kgcm...
		#matereriali
		self.db.execute('update materiali set E=E/.1')
		self.db.execute('update materiali set G=G/.1')
		#sezioni
		self.db.execute('update sezioni set A=A/100')
		self.db.execute('update sezioni set AV2=AV2/100')
		self.db.execute('update sezioni set AV3=AV3/100')
		self.db.execute('update sezioni set Jt=Jt/10000 ')
		self.db.execute('update sezioni set J22=J22/10000 ')
		self.db.execute('update sezioni set J33=J33/10000')
		self.db.execute('update sezioni set W22=W22/1000 ')
		self.db.execute('update sezioni set W33=W33/1000 ')
		self.db.execute('update sezioni set Wp22=Wp22/1000 ')
		self.db.execute('update sezioni set W33p/1000')
		#nodi
		self.db.execute('update table nodi set x=x/10')
		self.db.execute('update table nodi set y=y/10')
		self.db.execute('update table nodi set z=z/10')
		#sollecitazioni

		self.db.execute('update sollecitazioni set M3max=M3max/100  ')
		self.db.execute('update sollecitazioni set M3min=M3min/100  ')
		self.db.execute('update sollecitazioni set M2max=M2max/100  ')
		self.db.execute('update sollecitazioni set M2min=M2min/100  ')
		self.db.execute('update sollecitazioni set D2=D2/10  ')
		self.db.execute('update sollecitazioni set D3=D3/10  ')
		self.db.execute('update sollecitazioni set Q2=Q2/10  ')
		self.db.execute('update sollecitazioni set Q3=Q3/10  ')
		self.db.execute('update sollecitazioni set pos_1 =pos_1/10  ')
		self.db.execute('update sollecitazioni set N_1=N_1/10  ')
		self.db.execute('update sollecitazioni set V2_1=V2_1/10  ')
		self.db.execute('update sollecitazioni set V3_1=V3_1/10  ')
		self.db.execute('update sollecitazioni set T_1=T_1/100  ')
		self.db.execute('update sollecitazioni set M2_1=M2_1/100  ')
		self.db.execute('update sollecitazioni set M3_1=M3_1/100  ')
		self.db.execute('update sollecitazioni set pos_2 =pos_2 /10  ')
		self.db.execute('update sollecitazioni set N_2=N_2/10  ')
		self.db.execute('update sollecitazioni set V2_2=V2_2/10  ')
		self.db.execute('update sollecitazioni set V3_2=V3_2/10  ')
		self.db.execute('update sollecitazioni set T_2=T_2/100  ')
		self.db.execute('update sollecitazioni set M2_2=M2_2/100  ')
		self.db.execute('update sollecitazioni set M3_2=M3_2/100  ')
		self.db.commit()	

	def load_relaz(self,fi):
		par=parse_relaz(fi)
		self.load_materiali(par.isola_mat())
		self.load_sezioni(par.isola_sez())
		self.load_nodi(par.isola_nodi())
		self.load_nodi_vincolati(par.isola_nodi_vinc())
		self.load_travi(par.isola_travi())
		self.load_soll_travi(par.isola_soll_travi())
		self.load_soll_travi(par.isola_soll_pilastri())


if __name__=='__main__':
	c=input('provare a caricare in automatico? [s,n]: ')
	if c=='s':
		l=loader('struttura.sqlite')
		l.drop_all()
		l.load_materiali('materiali.txt')
		l.load_sezioni('sezioni.txt')
		l.load_nodi('nodi.txt')
		l.load_nodi_vincolati('nodi_vincolati.txt')
		l.load_travi('travi.txt')
		l.load_soll_travi('sollecitazioni-travi.txt')
		l.load_soll_travi('sollecitazioni-pilastri.txt')
		l.scala_Nmm()

