from io import StringIO
from scipy import *
import re
##questo script riesce partendo da una relazione in txt a dividere le tabelle e le passa su oggetti StringIO utilizzabili come fossero file
class parse_relaz:
	def __init__(self,fi):
		self.fi=open(fi,'r')
		self.tab_mat='''Id
Tipo / Note

Young
Poisson
G
Gamma
Alfa


daN/cm2
daN/cm2

daN/cm2
daN/cm3

'''.replace('\n','\r')
		self.tab_sez='''Id
Tipo
Area
A V2
A V3
Jt
J 2-2
J 3-3
W 2-2
W 3-3
Wp 2-2
Wp 3-3


 cm2
 cm2
 cm2
 cm4
 cm4
 cm4
 cm3
 cm3
 cm3
 cm3'''.replace('\n','\r')
		self.tab_nodi='''	Nodo	X	Y	Z	Nodo	X	Y	Z	Nodo	X	Y	Z'''
		self.tab_nodi_vinc='''	Nodo	X	Y	Z	Note	Rig. TX	Rig. TY	Rig. TZ	Rig. RX	Rig. RY	Rig. RZ'''
		self.tab_elem='''	Elem.	Note	Nodo I	Nodo J	Mat. 	Sez.	Rotaz. 	Svincolo I	Svincolo J	Wink V	Wink O'''
		self.sol_pilastri='''	Pilas.	Cmb	M3 mx/mn	M2 mx/mn	D 2 / D 3	Q 2 / Q 3	Pos.	N	V 2	V 3	T	M 2	M 3'''
		self.sol_travi='''	Trave	Cmb	M3 mx/mn	M2 mx/mn	D 2 / D 3	Q 2 / Q 3	Pos.	N	V 2	V 3	T	M 2	M 3'''

	def isola_mat(self):
		out=StringIO()
		self.fi.seek(0)
		for i in self.fi:
			if re.search(self.tab_mat,i):
				break
		i=i.split('\r')
		nrighe=int(len(i)/8)
		righe=[]
		for n in range(nrighe):
			righe.append('\t'.join(i[n*8:(n+1)*8]))
		out.write('\n'.join(righe))
		out.seek(0)
		return out
		
	def isola_sez(self):
		out=StringIO()
		self.fi.seek(0)
		for i in self.fi:
			if re.search(self.tab_sez,i):
				break
		i=i.split('\r')
		nrighe=int(len(i)/12)
		righe=[]
		for n in range(nrighe):
			righe.append('\t'.join(i[n*12:(n+1)*12]))
		out.write('\n'.join(righe))
		out.seek(0)
		return out
	
	def isola_nodi(self):
		return self.isola_gen_path(self.tab_nodi)

	def isola_nodi_vinc(self):
		return self.isola_gen_path(self.tab_nodi_vinc)

	def isola_travi(self):
		return self.isola_gen_path(self.tab_elem)

	def isola_soll_pilastri(self):
		return self.isola_gen_path(self.sol_pilastri)

	def isola_soll_travi(self):
		return self.isola_gen_path(self.sol_travi)

	def isola_gen_path(self,path):
		out=StringIO()
		self.fi.seek(0)
		while True:
			i=self.fi.readline()
			if i=='':break
			if re.search(path,i):
				break
		righe=[re.sub('^\t','',path)+'\n']
		while True:
			i=self.fi.readline()
			if re.match('\t*$',i):
				break
			else:
				righe.append(re.sub('^\t','',i))
		out.write(''.join(righe))
		out.seek(0)
		return out

if __name__=='__main__':
	p=parse_relaz('relazione_struttura_per_db_.txt')
	mat=p.isola_mat()
	print(mat.getvalue())
	sez=p.isola_sez()
	print(sez.getvalue())
	nodi=p.isola_gen_path(p.tab_nodi)
	print(nodi.getvalue())
	nodi_vinc=p.isola_gen_path(p.tab_nodi_vinc)
	print(nodi_vinc.getvalue())
	sol_pil=p.isola_gen_path(p.sol_pilastri)
	for i in range(10):
		print(sol_pil.readline())
	sol_trav=p.isola_gen_path(p.sol_travi)
	for i in range(10):
		print(sol_trav.readline())
