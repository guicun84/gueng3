import plotly
import plotly.graph_objs as go
import plotly.offline as pl
from scipy import vstack,array,dot,apply_along_axis,ndarray,r_,where,isnan,zeros,nan,mean
from scipy.linalg import norm
import pandas as pa

def plot3d(val,scala,db,*args,**kargs):
	'''val=array 2d con elemento,vi,vj
	opzioni:
	returnFig True false per ritornare la figura senza plottare
	direz=array(3x1) direzione su cui forzare plot None sceglie da solo'''

	if 'direz' not in list(kargs.keys()): kargs['direz']=None
	data=[]
	layout=go.Layout(
					margin=dict(l=0,r=0,t=20,b=0),
					scene=dict(aspectmode="data",),
#					paper_bgcolor='silver',
					xaxis=dict(ticks='',showticklabels=False),
					yaxis=dict(ticks='',showticklabels=False),
					)
	if 'camera' in list(kargs.keys()):
		layout['scene']['camera']=kargs['camera']
	#variabili per tener traccia dei punti di max e min
	plim=[None,None]
	vlim=[None,None]

	def checklim(val,pts,fun):
		'''Funzione per controllare max e min
		val=valori da controllare
		pts=punti in cui sono registrati
		fun= max |min '''
		v=fun(val)
		p=pts[where(val==fun(val))[0][0]]
		if (fun==max and vlim[0] is None):
			vlim[0]=v
			plim[0]=p
		elif (fun==max and vlim[0]<v):
			vlim[0]=v
			plim[0]=p
		elif (fun==min and vlim[1] is None):
			vlim[1]=v
			plim[1]=p
		elif (fun==min and vlim[1]>= v):
			vlim[1]=v
			plim[1]=p

	def checklim2(val,pts):
		checklim(val,pts,max)
		checklim(val,pts,min)

	#recupero coordinate
	if type(val) in (list,tuple):
		val=r_[val]
	els=val[:,0].astype(int).tolist()
	query='''
select d2.id as id,
pi.x as xi,
pi.y as yi,
pi.z as zi,
pj.x as xj,
pj.y as yj,
pj.z as zj,
r11,r12,r13,r21,r22,r23,r31,r32,r33 from d2 
inner join nodi pi on d2.i=pi.id 
inner join nodi pj on d2.j=pj.id
inner join infod2 on infod2.id=d2.id
where d2.id in ('''+','.join(['?']*len(els))+')'
	
	dati1=pa.DataFrame(val,columns=('el','vi','vj')).set_index('el')
	dati2=pa.read_sql(query,db,params=els).set_index('id')
	dati=dati1.join(dati2)
	x,y,z,x1,y1,z1,col=[zeros(3*len(els)),
		zeros(3*len(els)),
		zeros(3*len(els)),
		zeros(3*len(els)),
		zeros(3*len(els)),
		zeros(3*len(els)),
		zeros(3*len(els)),
		]

	sst=""
	def nz(x):
		r=x[x!=0]
		if r :return r
		return 0
	
	for n,(num,v) in enumerate(dati.iterrows()):
		x[n*3:(n+1)*3]=r_[v.xi,v.xj,nan]
		y[n*3:(n+1)*3]=r_[v.yi,v.yj,nan]
		z[n*3:(n+1)*3]=r_[v.zi,v.zj,nan]

		da=r_[0,0,v.vi,0,0,v.vj].reshape((2,3)).astype(float)
		c=2
		if kargs['direz'] is not None:
			direz=kargs['direz']
			if direz[0]!=0: 
				da=da[:,[2,0,1]]
				c=0
			elif direz[1]!=0: 
				da=da[:,[0,2,1]]
				c=1
		dat=da.copy()
		da=da[:,c].flatten()
		R=r_[v.r11, v.r12, v.r13, v.r21, v.r22, v.r23, v.r31, v.r32, v.r33].reshape(3,3)
		dat[0]=dot(R.T,dat[0])
		dat[1]=dot(R.T,dat[1])
		pt=r_[v.xi, v.yi, v.zi, v.xj, v.yj, v.zj].reshape(2,3)
		dat=pt+dat*scala
#				checklim(da,dat,max)
#				checklim(da,dat,min)
		checklim2(da,dat)
#		text.extend(['%.3f'%i for i in val[ntr]]+[''])
		x1[n*3:(n+1)*3]=[dat[0,0],dat[1,0],None]
		y1[n*3:(n+1)*3]=[dat[0,1],dat[1,1],None]
		z1[n*3:(n+1)*3]=[dat[0,2],dat[1,2],None]
		col[n*3:(n+1)*3]=[da[0],da[1],None]
	
	text=['{:.3f}'.format(i) for i in col]
			

	dat0=go.Scatter3d(x=x,y=y,z=z,
					  mode='lines',
					  line=dict(color="black",),
					  name="unifilare"+sst,
#						   hoverinfo='none',
					  opacity=0.3,
					   )
	data.append(dat0)
	if val is not None:
		col=array(col,dtype=float)
		if type(val)==str:
			namepl=val+sst
		else:
			namepl=sst
		dat1=go.Scatter3d(x=x1,y=y1,z=z1,
						  text=text,
						  hoverinfo="text",
						  mode='lines',
						  line=dict(color=col,
#							  	showscale=True,
							),
						  name=namepl,
						   )
		data.append(dat1)
		data.append(go.Scatter3d(x=(plim[0][0],plim[1][0]),
								 y=(plim[0][1],plim[1][1]),	
								 z=(plim[0][2],plim[1][2]),	
								 mode='markers+text',
								 marker=dict(size=3,color=('red','blue')),
								 name='limiti',
								 text=['{:.3f}'.format(i) for i in vlim],
								 ))

	fig=go.Figure(data=data,layout=layout)
	if 'returnFig' in list(kargs.keys()):
		if kargs['returnFig']:
			return fig
	pl.iplot(fig)


def plotd3(val,db,fun=mean):
	'''val=DataFrame con elem,nodo,val
	db=database modello'''
	n=','.join('%d'%i for i in val.nodo)
	nodi=pa.read_sql('select * from nodi where id in ({})'.format(n),db)
	inte=val.groupby('nodo').apply(lambda x:fun(x.val))
	nodi.reset_index()
	nodi['idx']=nodi.index.copy()
	nodimap=dict(zip(nodi.id,nodi.idx))
	el=','.join('%d'%i for i in val.elem)
	tris=pa.read_sql('select * from d3 where id in ({})'.format(el),db)
	i=r_[tris.i,tris.k]
	j=r_[tris.j,tris.l]
	k=r_[tris.k,tris.i]
	i=[nodimap[ii] for ii in i]
	j=[nodimap[ii] for ii in j]
	k=[nodimap[ii] for ii in k]
	mesh=dict(type='mesh3d',x=nodi.x,y=nodi.y,z=nodi.z,i=i,j=j,k=k,intensity=inte,text=['%f'%i for i in inte],hoverinfo='text')
	lay=dict(scene=dict(aspectmode='data'))
	return dict(data=[mesh],layout=lay)


