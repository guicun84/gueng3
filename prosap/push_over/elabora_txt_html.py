from lxml import html,etree
from lxml.builder import E
from lxml.cssselect import CSSSelector
import pandas as pa
from PIL import Image
import cssutils
from .elabora_txt import FileParser
import os,re

class PushTxt2Html(FileParser):
	def __init__(self,finame,dire='.',fiout='pushover.html',style=False):
		FileParser.__init__(self,finame)
		self.tohtml(dire,fiout,style)

	def tohtml(self,dire='.',fiout='pushover.html',style=True):
		dat=pa.read_sql('select * from risultati_push_over',self.db)	
		fis=[i for i in os.listdir(dire) if os.path.splitext(i)[1].lower() in ['.jpg','.jpeg','.png']]
		repath=re.compile('(\d+)')
		rc=[repath.search(i) for i in fis]
		fis=pa.DataFrame([[int(i.group(1)),j] for i,j in zip(rc,fis)],columns=['combinazione','imgname'])
		dat=pa.read_sql('select * from risultati_push_over',self.db)
		dat=pa.merge(dat,fis,on='combinazione')
		doc=E.html(E.head(E.link(type='text/css',rel='stylesheet',href=f'{fiout}.css')),E.body())
		body=doc.xpath('//body')[0]
		ks=[i for i in dat.keys() if i not in ['combinazione','imgname','data','hash','id','time']]
		nr=(len(ks)//2)
		def mm2px(mm):
			return mm/25.4*96
		for n,d in dat.iterrows():
			#calcolo dimensioni in px delle immagini
			impath=os.path.join(dire,d.imgname)
			im=Image.open(impath)
			w,h=im.size
			r=w/h
			wpx=mm2px(160)
			hpx=int(wpx/r)
			wpx=int(wpx)
			t=E.table(
				E.tr(E.td(E.img(src=os.path.join(dire,d.imgname),width=f'{wpx}',height=f'{hpx}'),{'class':'img'},colspan='2')),
				)
			#genero le righe sottostanti all'immagine
			rows=[]
			for m,k in enumerate(ks):
				k_=k.replace('s','*')
				if m<nr:
					if type(d[k]) in [str, int]:
						rows.append(E.tr(E.td(f'{k_}= {d[k]}')))
					else:
						rows.append(E.tr(E.td(f'{k_}= {d[k]:.4g}')))
					t.append(rows[-1])
				else:
					row=rows[m-nr]
					if type(d[k]) in [str, int]:
						row.append(E.td(f'{k_}= {d[k]}'))
					else:
						row.append(E.td(f'{k_}= {d[k]:.4g}'))

			t=E.div(E.h1(f'Combinazione {d.combinazione}'),t)
			body.append(t)
		#copio i css su html, lo faccio xche libreoffice non li importa da css linkato ma li applica se sono presenti inline
		csssource=os.path.join(os.path.dirname(__file__),'stili.css')
		css=cssutils.parseFile(csssource)
		css_=[[i.selectorText,i.style] for i in css.cssRules]
		css=[]
		css.extend([i for i in css_ if '#' not in i[0] and '.' not in i[0]])
		css.extend([i for i in css_ if '.'  in i[0]])
		css.extend([i for i in css_ if '#'  in i[0]])
		for sel,st in css:
			xpath=CSSSelector(sel).path
			els=doc.xpath(xpath)
			for el in els:
				if not 'style' in el.attrib.keys():
					el.attrib['style']=st.cssText.replace('\n','')
				else:
					el.attrib['style']+='; '+st.cssText.replace('\n','')
				for pr in ['width','height']:
					val=st[pr]
					if val:
						if 'mm' in val: val=f'{int(mm2px(float(val[:-2])))}'
						elif 'cm' in val: val=f'{int(mm2px(float(val[:-2]))/10)}'
						for el in els:
							el.attrib[pr]=val


		with open(fiout,'w') as fi:
			fi.write(html.tostring(doc,pretty_print=True).decode('utf8'))

		if style:
			with open(f'{fiout}.css','w') as fi:
				fi.write(open(csssource).read())

			
