import sqlite3,re,datetime,os,hashlib
from gueng.utils import floatPath as fp

#db=sqlite3.connect('risultati-push-over.sqlite')
#db.executescript('''
#create table if not exists risultati (
#id integer primary key autoincrement,
#test integer,
#data data,
#combinazione integer,
#arresto text,
#areaE real,
#d07Fbu real,
#ag real,
#Dd real,
#Cd real)''')
#
class TxtParser:
	path=dict(
		combinazione=('Analisi combinazione n. (\d+)',int),
		interruzione=('Analisi terminata per: (.*)',str),
		FBmax_Fb1=(f'Rapporto  Fb max/Fb 1 :\s*({fp})',float),
		AreaEs=(f'Area sottesa dal diagramma E\* :\s*({fp})',float),
		ds07Fbu=(f'Spostamento d\* allo 0,7 di Fbu:\s*({fp})',float),
		Ks=(f'Rigidezza K\*  :\s+({fp})',float),
		dys=(f'Spostamento dy\*  :\s*({fp})',float),
		Fys=(f'Taglio Fy\*:\s*({fp})',float),
		ms=(f'Massa m\* :\s*({fp})',float),
		Ts=(f'Periodo T\*:\s*({fp})',float),
		SeTs=(f'Accelerazione Se\(T\*\) :\s*({fp})',float),
		ds=(f'Spostamento d\* e,max  \(da Sd\(T\*\)\)  :\s*({fp})',float),
		qs=(f'Fattore q\*:\s*({fp})',float),
		Ddsmax=(f'Spostamento d\* max:\s*({fp})',float),
		DFs=(f'Taglio F\*:\s*({fp})',float),
		Ddu=(f'Spostamento Domanda SL:\s*({fp})',float),
		CdDanno=(f'Spostamento dc Danno:\s*({fp})',float),
		CdMax=(f'Spostamento dc Fb max:\s*({fp})',float),
		Cdu=(f'Spostamento dc Ultimo:\s*({fp})',float),
		CTDanno=(f'Taglio Fb Danno:\s*({fp})',float),
		CTmax=(f'Taglio Fb max:\s*({fp})',float),
		CTu=(f'Taglio Fb Ultimo:\s*({fp})',float),
		CPGADanno=(f'PGA dc Danno:\s*({fp})',float),
		CPGAu=(f'PGA dc Ultimo:\s*({fp})',float),
		alphae=(f'Indicatore di rischio alfae:\s*({fp})',float),
		alphau=(f'Indicatore di rischio alfau:\s*({fp})',float),
	)
	calcolati=dict(
		gamma=float,
	)
	keys=['combinazione','interruzione','FBmax_Fb1','AreaEs','ds07Fbu','Ks','dys','Fys','ms','Ts','SeTs','ds','qs','Ddsmax','DFs','Ddu','CdDanno','CdMax','Cdu','CTDanno','CTmax','CTu','CPGADanno','CPGAu','alphae','alphau','gamma',]
	reg=dict((i,re.compile(j[0])) for i,j in path.items())
	
	@property
	def hash(self):
		st=''.join([f'{self.ris[i]}' for i in self.keys])
		return hashlib.md5(st.encode()).hexdigest()

	def __init__(self,righe):
		self.righe=righe
		self.parse()
		self.elabora()
		
	def parse(self):
		self.ris=dict()
		righe=self.righe[:]
		for i,j in self.reg.items():
			for r in righe:
				t=j.search(r)
				if t:
					self.ris[i]=self.path[i][1](t.group(1))
					n=righe.index(r)
					del righe[n]
	
	def elabora(self):
		AEs=self.ris['AreaEs']
		Fys=self.ris['Fys']
		dys=self.ris['dys']
		Cdus=(AEs-(Fys*dys/2))/Fys+dys
		self.ris['Cdus']=Cdus
		self.ris['gamma']=abs(self.ris['Ddu']/self.ris['Ddsmax'])

	def controlladb(self,db):
		return bool(db.execute('select 1 from sqlite_master where type="table" and name="risultati_push_over"').fetchone())
	
	def creatabella(self,db):
		query='''create table risultati_push_over(
id integer primary key autoincrement,
'''
		choiches={str:'text',float:'real',int:'integer'}
		for i in self.path.items():
			query+=f'{i[0]} {choiches[i[1][1]]},\n'
		for i in self.calcolati.items():
			query+=f'{i[0]} {choiches[i[1]]},\n'
		query+='time data,hash text)'
		db.execute(query)

	def todb(self,db,time=None):
		if not db.execute('select 1 from risultati_push_over where hash=?',[self.hash]).fetchone():
			if time is None:
				time=datetime.datetime.now()
			keys=self.keys[:]
			val=[self.ris[i] for i in keys]
			keys.append('time')
			val.append(f'{time}')
			keys.append('hash')
			val.append(self.hash)
			k=','.join(keys)
			qm=','.join(['?']*len(val))
			q=f'insert into risultati_push_over ({k}) values ({qm})'
			db.execute(q,val)

class FileParser:
	def __init__(self,finame):
		self.fi=open(finame)
		self.split()
		self.txt=[TxtParser(i) for i in self.parti]
		dbname=f'{os.path.splitext(finame)[0]}.sqlite'
		self.db=sqlite3.connect(dbname)
		if not self.txt[0].controlladb(self.db):
			self.txt[0].creatabella(self.db)
		time=datetime.datetime.now()
		for i in self.txt:
			i.todb(self.db,time)
		self.db.commit()

	def split(self):
		self.parti=[]
		parte=[]
		test=False
		for r in self.fi.readlines():
			if 'Analisi combinazione n' in r:
				test=True
				if parte:
					self.parti.append(parte)
					parte=[r]
				else:
					parte.append(r)
				continue
			if test:
				parte.append(r)
				continue
		self.parti.append(parte)

