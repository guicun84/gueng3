import os,re,sys
from lxml.html import etree as et
import pypandoc


class Assembler:
	def __init__(self,path_img='.*\.png',path_txt='.*\.txt',fiout='pushover.odt'):
		'''path_img=regex per nomi immagini
		path_txt=regex per nomi file txt
		NOTA: per sicurezza tutti i nomi file devono inizire con il numero di combinazione'''
		vars(self).update(locals())
		self.cerca_file()
		self.assembla()

	def cerca_file(self):
		self.imgs=[i for i in os.listdir('.') if re.match(self.path_img,i)]
		self.txts=[i for i in os.listdir('.') if re.match(self.path_txt,i)]
		if len(self.txts)!=len(self.imgs):
			print('controllo i file')	
			self.elaboraTxt()
			if False:
				raise ValueError('trovati %d .txt e %d.img'%(len(self.txts),len(self.imgs)))
		self.imgs.sort()
		self.txts.sort()

	def elaboraTxt(self):
		path='Analisi combinazione n. (\d+)'
		self.string=dict()
		for fi in self.txts:
			print(fi)
			f=open(fi,'r')
			st=None
			num=None
			for r in f:
				ricerca=re.search(path,r)
				if ricerca:
					if num:
						self.string[num]=st
					st=r
					num=int(ricerca.group(1))
					print(num)
				else:
					st+=r
			f.close()
			self.string[num]=st
		print(list(self.string.keys()))
	
	def assembla(self):
		self.html=et.Element('html')
		head=et.Element('head')
		meta=et.Element('meta')
		meta.set('charset','IT')
		head.append(meta)
		self.html.append(head)
		self.body=et.Element('body')
		self.html.append(self.body)
		h=et.Element('h2')
		h.text='Risultati grafici push over'
		self.body.append(h)
		for ind,i in enumerate(self.imgs):
			if True:
				n=int(re.search('\d+',i).group(0))
				print(n)
			else: 
				n='__'
			h=et.Element('h3')
			h.text='Combinazione %s'%n
			self.body.append(h)
			#inserisco l'immagine
			im=et.Element('img')
			im.set('width','590')
			im.set('src',i)
			self.body.append(im)
			T=et.Element('table')
			T.set('style','border-collapse: collapse;')
			self.body.append(T)
			#recupero testo:
			if not hasattr(self,'string'):
				fi=open(self.txts[ind],'r')
				t=fi.read()
				fi.close()
				t=t.split('\n')
			else:
				t=self.string[n].split('\n')
			print(len(t))
			#divido in due il testo e lo inserisco
			for n,i in enumerate(t):
				t[n]=re.sub('<?=*>?','',i)

			nri=0
			for ri in t:
				#if 'INFORMAZIONI AGGIUNTIVE' in ri: break
				if 'CALCOLO DOMANDA DI SPOSTAMENTO' in ri: break
				else: nri+=1
			t1=t[:nri]
			t2=t[nri:]
			r=et.Element('tr')
			for te in t1,t2:
				c=et.Element('td')
				c.set('style','border:1px solid black;')
				for t in te:
					p=et.Element('p')
					p.text=t.decode('utf-8')
					c.append(p)
				r.append(c)
			T.append(r)
			print('fatto')
		pypandoc.convert_text(et.tostring(self.html,pretty_print=True).decode('latin'),to='odt',format ='html',outputfile='pushover.odt')

