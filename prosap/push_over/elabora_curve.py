import numpy as np
import os 
from PIL import Image

class CurveParser:
	rosso=(232,96,96)
	verde=(96,232,96)
	nero=(0,0,0)

	def __init__(self,finame):
		im=Image.open(finame)
		self.dat=np.array(im)
		print(self.dat.shape)

	def isola_colore(self,col):
		t=self.dat[:,:,0]==col[0]
		t=t & (self.dat[:,:,1]==col[1])
		t=t & (self.dat[:,:,2]==col[2])
		return t

	def elabora_colore(self,colore,num=-1,fun=np.mean):
		mask=self.isola_colore(colore)
		att=self.cerca_attraversamenti(mask)
		return self.elabora_attraversamenti(att,num,fun)

	@property
	def shape(self):
		return self.dat.shape

	@staticmethod
	def cerca_attraversamenti(mask):
		ris=[]
		h=mask.shape[1]
		for col in range(mask.shape[1]):
			val=[]
			temp=[]
			for n,v in  enumerate(mask[:,col]):
				if v and len(temp)==0:
					temp.append(h-n)
				elif not v and len(temp)==1:
					temp.append(h-n)
					val.append(temp[:])
					temp=[]
			if len(temp)==1:temp.append(h-n)
			ris.append(val[:])
		return ris

	@staticmethod
	def elabora_attraversamenti(att,num=-1,fun=np.mean):
		out=[None]*len(att)
		for n,i in enumerate(att):
			if not i: continue
			if len(i)<num+1:continue
			out[n]=fun(i[num])
		return np.array(out)

	@staticmethod
	def scala(ar,ofx,ofy,valx,valy,lx=1,ly=1):
		val=np.vstack([np.arange(ar.size),ar]).T.astype(float)
		val-=np.r_[ofx,ofy]
		val*=np.r_[valx/lx,valy/ly]
		return val






		
