#!/usr/bin/python


from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

class Beam:
	def __init__(self,i,j):
		vars(self).update(locals())

	def drawWire(self):
		glBegin(GL_LINES)
		glColor(255,255,255)
		glVertex3d(*self.i)
		glVertex3d(*self.j)
		glEnd()

class Camera():
	def __init__(self,window,x=0,y=0,z=0,vx=-1,vy=-1,vz=-1., alpha=60):
		'''x,y,z=posozione camera
		vx vy vz punto di mira
		alpha=angolo di vista
		ratio=x/y della finestra'''
		vars(self).update(locals())

	def draw(self):
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(self.alpha,self.window.ratio,10,100e3)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()
		gluLookAt(self.x , self.y , self.z , self.vx , self.vy , self.vz , 0 , 0 , 1)


class World:
	def __init__(self,objects=[]):
		vars(self).update(locals())

		
class Window:
	def onReshape(self,width,height):
		if height==0: height=1
		vars(self).update(locals())
		self.ratio=ratio=self.width/self.height
		glViewport(0,0,width,height)

	def __init__(self):
		glutInit()
		glutInitWindowSize(600,400)
		self.win=glutCreateWindow("test")
		glutReshapeFunc(self.onReshape)

		glClearColor(0,0,0,0)
		glClearDepth(1)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		glShadeModel(GL_SMOOTH)
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH)
		self.ratio=600/400	


	def draw(self):
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		#altro codice per il disegno


if __name__=='__main__':

	win=Window()
	c=Camera(win,10,10,10,0,0,0)
	lines=[Beam((2,0,i),(0,2,i)) for i in range(6)]
	def draw():
		win.draw()
		c.draw()
		glBegin(GL_LINES)
		glColor(255,0,0)
		glVertex(0,0,0)
		glVertex(10,0,0)
		glColor(0,255,0)
		glVertex(0,0,0)
		glVertex(0,10,0)
		glColor(0,0,255)
		glVertex(0,0,0)
		glVertex(0,0,10)
		glEnd()
		for i in lines: i.drawWire()
		glutSwapBuffers()
	glutDisplayFunc(draw)
#	glutIdleFunc(draw)
	glutMainLoop()
