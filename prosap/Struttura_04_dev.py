#-*-coding:utf-8-*-

from sqlite3 import connect
import sqlite3
from gueng.utils import qm_like
import re,sys
from scipy import *
from scipy.linalg import norm

'''questa versione funziona con i db realizzati partendo dal txt fatto da prosap
questa versione funziona con il tipo di db multisezione crato con parse_ralaz_02'''
print('messa a posto solo get_sol')
class Struttura:
	def __init__(self,fi):
		self.fi=fi
		self.db=connect(fi)
	
	def ex(self,query,dat=None):
		if dat is not None:
			return self.db.execute(query,dat)
		else:
			return self.db.execute(query)
	
	def ex_(self,query,dat=None):
		return self.ex(query,dat).fetchall()

	def cerca_part(self,elem,nodo):
		'cerca se nodo è il nodo i o j di un elemento'
		i=self.ex_('select i from d2 where id=?',(elem,))[0][0]
		if i==nodo :
			return 1
		j=self.ex_('select j from d2 where id=?',(elem,))[0][0]
		if j==nodo: 
			return 2
		else:
			raise NodeError('l\'elemento %(elem)d non converge sul nodo %(nodo)d'%locals())
	
	def get_sol(self,elem,part=None,cmb=None):
		'''elem=elemento'
		   part=parte da considerare: 1 o 2; oppure <0 per indicare il nodo, float per lunghezza'
		   cmb=combinazione numero o lista'''
		if part:
			if part<0:
				part=self.cerca_part(elem,-part)
		
		if type(part)== int:
			if part==1:x=0
			else: x=self.db.execute('select max(x) from sollecitazioni_d2 where elem=?',(elem,)).fetchone()[0]

			query='select N,V2,M3,cmb,elem,x from sollecitazioni_d2 where elem=? and x=?'
			if cmb is not None:
				if hasattr(cmb,'__len__'):
					query+=' and cmb in (' +','.join(['?']*len(cmb)) +')'
				else:
					query+=' and cmb=?'
			print(query)
			print(query.count('?'))
			if query.count('?')==2: dat=[elem,x]
			elif query.count('?')==3: dat=[elem,x,cmb]
			else: 
				dat=[elem,x]
				dat.extend(cmb)
			print(dat)
			return r_[self.db.execute(query,dat).fetchall()]
		else:
			x=part
			if cmb  is None:
				cmb=self.ex_('select cmb from sollecitazioni_d2 where elem=?',(elem,))
			if not hasattr(cmb,'__len__'):
				cmb=[cmb]
			xx=r_[self.ex_('select distinct(x) from sollecitazioni_d2 where elem=?',(elem,))].flatten()
			out=zeros((len(cmb),6))
			for n,c in enumerate(cmb):
				query='select N,V2,M3,x from sollecitazioni_d2 where elem=? and cmb=? order by x'
				dat=r_[self.db.execute(query,(elem,c)).fetchall()]
				out[n,0]=interp(x,xx,dat[:,0])
				out[n,1]=interp(x,xx,dat[:,1])
				out[n,2]=interp(x,xx,dat[:,2])
				out[n,3]=c
				out[n,4]=elem
				out[n,5]=x
			return out



	def get_sol_tot(self,elem,part=None,cmb=None):
		'''elem=elemento o lista elementi'
		   part=parte da considerare: 1 o 2; oppure <0 per indicare il nodo'
		   cmb=combinazione o lista combinazioni'''
		if part:
			if part<0:
				part=self.cerca_part(elem,-part)
		if part==1:
			query='select N1,V21,V31,M11,M21,M31,cmb,elem from sollecitazioni_d2 where elem '
		elif part==2:
			query='select N2,V22,V32,M12,M22,M32,cmb,elem from sollecitazioni_d2 where elem '
		else:
			query='select N1,V21,V31,M11,M21,M31,N2,V22,V32,M12,M22,M32,cmb,elem from sollecitazioni_d2 where elem '	
		
		dat=[]
		if type(elem) in (list,tuple):
			query += 'in '+ qm_like(elem)
			dat.extend(elem)
		else:
			query+= '=?'
			dat.append(elem)
		if cmb:
			if type(cmb) in [list,tuple]:
				query += ' and cmb in ' +qm_like(cmb)
				dat.extend(cmb)
			else:
				query += ' and cmb =? '
				dat.append(cmb)
		return r_[self.db.execute(query,dat).fetchall()]


	def get_dir(self,elem,f=0):
		'''considero assi 1,2,3 come prosap:
		e1= asse elemento:
		e2= asse verticale o -y a seconda che siano travi o pilastri a meno di rotazioni
		e3= cross(e1,e2)'''
		nodi=r_[self.ex_('select nodi.id,x,y,z from nodi inner join d2 on d2.i=nodi.id or d2.j=nodi.id where d2.id=:elem',locals())]
		i=r_[self.ex_('select i from d2 where id=:elem',locals())][0]
		ni=where(nodi[:,0]==i)[0][0]
		nj=where(nodi[:,0]!=i)[0][0]
		i=nodi[ni,1:]
		j=nodi[nj,1:]
		e1=j-i
		e1/=norm(e1)
		e3=cross(e1,r_[0,0,1.])
		if norm(e3)==0:
			e3=cross(e1,r_[-1.,0,0])
		e3/=norm(e3)
		e2=cross(e3,e1)
		e2/=norm(e2)
		M=c_[[e1,e2,e3]]
		rot=deg2rad(self.ex_('select rotazione from d2 where id=:elem',locals())[0])
		if rot!=0:
			Mx=matrix([[1,0,0],[0,cos(rot),sin(rot)],[0,-sin(rot),cos(rot)]])
			M=dot(Mx,M)
		if f and rot!=0:
			return M.T,Mx
		else:
			return M.T

	def get_sol_glob(self,elem,part,cmb=None):
		'''elem=elemento o lista elementi
		   part=parte da considerare: 1 o 2; oppure <0 per indicare il nodo
		   cmb=combinazione o lista combinazioni'''
		if part:
			if part<0:
				part=self.cerca_part(elem,-part)
		M_rot=self.get_dir(elem)
		def cerca(elem,part,cmb):
			sol=self.get_sol_tot(elem,part,cmb)
			if part==1: sol[:,:6]*=-1
			for n,i in enumerate(sol):
				sol[n,:3]=dot(M_rot,i[:3]*r_[-1,1,1])
				sol[n,3:6]=dot(M_rot,i[3:6]*r_[-1,1,-1])
			return sol
		if hasattr(elem,'__len__'):
			if not hasattr(cmb,'__len__'): cmb=[cmb]*len(elem)
			return r_[[cerca(elem[n],part[n],cmb[n]) for n,t in enumerate(elem)]]
		else:
			return cerca(elem,part,cmb)

	def cerca_elementi(self,nodo):
		return [int(i[0]) for i in self.ex_('select id from d2 where i==:nodo or j==:nodo',locals())]

	def get_sol_sum(self,elem,part,cmb=None):
		'''restituisce la somma delle azioni di più elementi in un nodo:
		   elem=elemento o lista elementi'
		   part=lista di parti da considerare 1 o 2; oppure unico valore <0 per indicare il nodo'
		   cmb=combinazione o lista combinazioni'''
		if not hasattr(elem,'__len__'): elem=[elem]
		if not hasattr(part,'__len__'): part=[part]*len(elem)
		S=self.get_sol_glob(elem[0],part[0],cmb)
		for e,p in zip(elem[1:],part[1:]):
			S[:,:6]+=self.get_sol_glob(e,p,cmb)[:,:6]
		return S

	def copy_on_memory(self):
		new_db=sqlite3.connect(':memory:')
		new_db.executescript(''.join(self.db.iterdump()))
		self.db.close()
		self.db=new_db

	def test_equilbrio_nodi(self,cmb=None):
		'''calcola l'errore di chiusura delle forze sui nodi non vincolati'''
		if cmb is None:
			cmb=[int(i[0]) for i in self.ex_('select distinct cmb from sollecitazioni_d2')]
		#ricerca dei nodi non vincolati
		nodi=[int(i[0]) for i in self.ex_('select id from nodi where vx=0 and vy=0 and vz=0 and rx=0 and ry=0 and rz=0')]
		#per ogni nodo calcolo l'errore massimo di chiusura delle forze
		sys.stdout.write( ('\rnodo %d  --%.2f%%'%(nodi[0],0)).ljust(40))
		sys.stdout.flush()
		for nn,n in enumerate(nodi):
			e=self.cerca_elementi(n)
			eq=self.get_sol_sum(e,-n,cmb)
			sys.stdout.write(('\rnodo %d  --%.2f%%'%(n,100*(nn+1)/len(nodi))).ljust(40))
			sys.stdout.flush()
		return eq[:,:6].max(0)		






#	de get_sez(self,elem):
#		query='select sezioni.tipo,travi.id from sezioni inner join travi on travi.sez=sezioni.id where travi.id '
#		dati=[]
#		if type(elem)in (list,tuple):
#			query+= 'in '+qm_like(elem)
#			dati.extend(elem)
#		else:
#			query+='=? '
#			dati.append(elem)
#		return self.db.execute(query,dati).fetchall()
#
#	def get_vec(self,elem):
#		'''ritorna il vettore orientato dell'elemento non normalizzato'''
#		i=r_[self.ex_('select x,y,z from nodi inner join travi on travi.i = nodi.id where travi.id=:elem',locals())][0]
#		j=r_[self.ex_('select x,y,z from nodi inner join travi on travi.j = nodi.id where travi.id=:elem',locals())][0]
#		return j-i

class NodeError(Exception):
	pass
