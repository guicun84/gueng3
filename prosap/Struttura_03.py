#-*-coding:utf-8-*-

from sqlite3 import connect
import sqlite3
from gueng.utils import qm_like
import re,sys
from scipy import *
from scipy.linalg import norm
from pylab import clf,suptitle,subplot,plot,legend,grid,draw,show,ylabel
try:
	from pylab import tight_layout
except:
	def tight_layout():
		pass

'''questa versione funziona con i db realizzati partendo dal txt fatto da prosap'''

for i in int8,int16,int32,int64,uint8,uint16,uint32,uint64: sqlite3.register_adapter(i,int)
#for i in float16,float32,float64,float96: sqlite3.register_adapter(i,float)
for i in float16,float32,float64: sqlite3.register_adapter(i,float)

class Struttura:
	def __init__(self,fi):
		self.fi=fi
		self.db=connect(fi)

	def ex(self,query,dat=None):
		if dat is not None:
			return self.db.execute(query,dat)
		else:
			return self.db.execute(query)
	
	def ex_(self,query,dat=None):
		return self.ex(query,dat).fetchall()

	def cerca_part(self,elem,nodo):
		i=self.ex_('select i from d2 where id=?',(elem,))[0][0]
		if i==nodo :
			return 1
		j=self.ex_('select j from d2 where id=?',(elem,))[0][0]
		if j==nodo: 
			return 2
		else:
			raise NodeError('l\'elemento %(elem)d non converge sul nodo %(nodo)d'%locals())
	
	def get_sol(self,elem,part=None,cmb=None):
		'''elem=elemento o lista elementi'
		   part=parte da considerare: 1 o 2; oppure <0 per indicare il nodo'
		   cmb=combinazione o lista combinazioni'''
		if part:
			if part<0:
				part=self.cerca_part(elem,-part)
		if part==1:
			query='select N1,V21,M31,cmb,elem from sollecitazioni_d2 where elem '
		elif part==2:
			query='select N2,V22,M32,cmb,elem from sollecitazioni_d2 where elem '
		else:
			query='select N1,V21,M31,N2,V22,M32,cmb,elem from sollecitazioni_d2 where elem '	
		
		dat=[]
#		if type(elem) in (list,tuple):
		if hasattr(elem,'__len__'):
			query += 'in '+ qm_like(elem)
			dat.extend(elem)
		else:
			query+= '=?'
			dat.append(elem)
		if cmb:
#			if type(cmb) in [list,tuple]:
			if hasattr(cmb,'__len__'):
				query += 'and cmb in ' +qm_like(cmb)
				dat.extend(cmb)
			else:
				query += 'and cmb =? '
				dat.append(cmb)
		return r_[self.db.execute(query,dat).fetchall()]


	def get_sol_tot(self,elem,part=None,cmb=None):
		'''elem=elemento o lista elementi'
		   part=parte da considerare: 1 o 2; oppure <0 per indicare il nodo'
		   cmb=combinazione o lista combinazioni'''
		if part:
			if part<0:
				part=self.cerca_part(elem,-part)
		if part==1:
			query='select N1,V21,V31,M11,M21,M31,cmb,elem from sollecitazioni_d2 where elem '
		elif part==2:
			query='select N2,V22,V32,M12,M22,M32,cmb,elem from sollecitazioni_d2 where elem '
		else:
			query='select N1,V21,V31,M11,M21,M31,N2,V22,V32,M12,M22,M32,cmb,elem from sollecitazioni_d2 where elem '	
		
		dat=[]
#		if type(elem) in (list,tuple):
		if hasattr(elem,'__len__'):
			query += 'in '+ qm_like(elem)
			dat.extend(elem)
		else:
			query+= '=?'
			dat.append(elem)
		if cmb:
#			if type(cmb) in [list,tuple]:
			if hasattr(cmb,'__len__'):
				query += ' and cmb in ' +qm_like(cmb)
				dat.extend(cmb)
			else:
				query += ' and cmb =? '
				dat.append(cmb)
		return r_[self.db.execute(query,dat).fetchall()]

	def get_dir(self,elem,f=0):
		return r_[self.ex('select r11,r12,r13,r21,r22,r23,r31,r32,r33 from infod2 where id=?',(elem,)).fetchone()].reshape(3,3).T
		
	def get_dir_old(self,elem,f=0):
		'''considero assi 1,2,3 come prosap:
		e1= asse elemento:
		e2= asse verticale o -y a seconda che siano travi o pilastri a meno di rotazioni
		e3= cross(e1,e2)'''
		nodi=r_[self.ex_('select nodi.id,x,y,z from nodi inner join d2 on d2.i=nodi.id or d2.j=nodi.id where d2.id=:elem',locals())] #recupero nodi dell'elemento
		i=r_[self.ex_('select i from d2 where id=:elem',locals())][0]
		assert i in nodi[:,0] ,'nodo {i} not in {nodi}'.format(**locals())
		ni=where(nodi[:,0]==i)[0][0]
		nj=where(nodi[:,0]!=i)[0][0]
		i=nodi[ni,1:]
		j=nodi[nj,1:]
		e1=j-i
		e1/=norm(e1)
		e3=cross(e1,r_[0,0,1.])
		if norm(e3)==0:
			e3=cross(e1,r_[-1.,0,0])
		e3/=norm(e3)
		e2=cross(e3,e1)
		e2/=norm(e2)
		M=c_[[e1,e2,e3]]
		rot=deg2rad(self.ex_('select rotazione from d2 where id=:elem',locals())[0])
		if rot!=0:
			Mx=matrix([[1,0,0],[0,cos(rot),sin(rot)],[0,-sin(rot),cos(rot)]])
			M=dot(Mx,M)
		if f and rot!=0:
			return M.T,Mx
		else:
			return M.T

	def get_sol_glob(self,elem,part,cmb=None):
		'''elem=elemento o lista elementi
		   part=parte da considerare: 1 o 2; oppure <0 per indicare il nodo
		   cmb=combinazione o lista combinazioni'''
		if part:
			if part<0:
				part=self.cerca_part(elem,-part)
		M_rot=self.get_dir(elem)
		def cerca(elem,part,cmb):
			sol=self.get_sol_tot(elem,part,cmb)
			if part==1: sol[:,:6]*=-1
			for n,i in enumerate(sol):
				sol[n,:3]=dot(M_rot,i[:3]*r_[-1,1,1])
				sol[n,3:6]=dot(M_rot,i[3:6]*r_[-1,1,-1])
			return sol
		if hasattr(elem,'__len__'):
			if not hasattr(cmb,'__len__'): cmb=[cmb]*len(elem)
			return r_[[cerca(elem[n],part[n],cmb[n]) for n,t in enumerate(elem)]]
		else:
			return cerca(elem,part,cmb)

	@staticmethod
	def ruota_sol(sol,Mr):
		'''metodo statico per ruotare le sollecitazioni secondo la matrice di rigidezza
		sol=Sollecitazioni da ruotare fx fy fz mx my mz cmb el
		Mr matrice di rotazione '''
		solr=zeros_like(sol)
		for n,i in enumerate(sol):
			solr[n,:3]=dot(Mr,i[:3])
			solr[n,3:6]=dot(Mr,i[3:6])
		solr[:,6:]=sol[:,6:]
		return solr

	def get_sol_rot(self,elem,part,cmb=None,Mr=eye(3)):
		'''elem=elemento o lista elementi
		   part=parte da considerare: 1 o 2; oppure <0 per indicare il nodo
		   cmb=combinazione o lista combinazioni
		   Mr= matrice di rotazione da globale a locale'''
		sol=self.get_sol_glob(elem,part,cmb)
		return self.ruota_sol(sol,Mr)
		

	def cerca_elementi(self,nodo):
		return [int(i[0]) for i in self.ex_('select id from d2 where i==:nodo or j==:nodo',locals())]

	def get_sol_sum(self,elem,part,cmb=None):
		'''restituisce la somma delle azioni di più elementi in un nodo:
		   elem=elemento o lista elementi'
		   part=lista di parti da considerare 1 o 2; oppure unico valore <0 per indicare il nodo'
		   cmb=combinazione o lista combinazioni'''
		if not hasattr(elem,'__len__'): elem=[elem]
		if not hasattr(part,'__len__'): part=[part]*len(elem)
		S=self.get_sol_glob(elem[0],part[0],cmb)
		for e,p in zip(elem[1:],part[1:]):
			S[:,:6]+=self.get_sol_glob(e,p,cmb)[:,:6]
		return S
	
	def get_sol_sum_rot(self,elem,part,cmb=None,Mr=eye(3)):
		'''restituisce la somma delle azioni in un nodo ruotata su un sistema di riferimento voluto:
		   elem=elemento o lista elementi'
		   part=lista di parti da considerare 1 o 2; oppure unico valore <0 per indicare il nodo'
		   cmb=combinazione o lista combinazioni
		   Mr: matrice di rotazione da globale a locale'''
		sol=self.get_sol_sum(elem,part,cmb)
		return self.ruota_sol(sol,Mr)

	def copy_on_memory(self):
		new_db=sqlite3.connect(':memory:')
		new_db.executescript(''.join(self.db.iterdump()))
		self.db.close()
		self.db=new_db

	def test_equilbrio_nodi(self,cmb=None):
		'''calcola l'errore di chiusura delle forze sui nodi non vincolati'''
		if cmb is None:
			cmb=[int(i[0]) for i in self.ex_('select distinct cmb from sollecitazioni_d2')]
		#ricerca dei nodi non vincolati
		nodi=[int(i[0]) for i in self.ex_('select id from nodi where vx=0 and vy=0 and vz=0 and rx=0 and ry=0 and rz=0')]
		#per ogni nodo calcolo l'errore massimo di chiusura delle forze
		sys.stdout.write( ('\rnodo %d  --%.2f%%'%(nodi[0],0)).ljust(40))
		sys.stdout.flush()
		eq=empty((0,8),float)
		for nn,n in enumerate(nodi):
			e=self.cerca_elementi(n)
			eq=append(eq,self.get_sol_sum(e,-n,cmb),0)
			sys.stdout.write(('\rnodo %d  --%.2f%%'%(n,100*(nn+1)/len(nodi))).ljust(40))
			sys.stdout.flush()
		print('')
		return abs(eq[:,:6]).max(0)		
		
	@staticmethod
	def plt_(sol,tit='',unit=['','']):
		'''plotta un grafico delle sollecitazioni
		sol=sollecitazioni fx fy fz mx my mz cmb elem
		tit=titolo per il grafico
		unit=unità di misura: '''
		clf()
		nsb={True:3,False:2}[sol.shape[1]>6]
		subplot(nsb,1,1)
		plot(sol[:,:3],'.')
		ylabel('Forze{}'.format({True:' [{}]'.format(unit[0]) ,
								False:''}[bool(unit[0])]
								))
		legend('fx fy fz'.split())
		grid(1)
		subplot(nsb,1,2)
		plot(sol[:,3:6],'.')
		ylabel('Momenti{}'.format({True:' [{}]'.format(unit[1]),False:''}[bool(unit[1])]))
		legend('mx my mz'.split())
		grid(1)
		if nsb==3:
			subplot(3,1,3)
			plot(sol[:,-1],'.')
			ylabel('elementi')
			grid(1)
		suptitle(tit)
		tight_layout()
		draw()
		show(0)

	def plt(self,sol,tit='',unit=None):
		'''plotta un grafico delle sollecitazioni
		sol=sollecitazioni fx fy fz mx my mz cmb elem
		tit=titolo per il grafico
		unit=unità di misura: se None prende quelle salvate nel database altrimenti si puo passare lista con unità per forze e unità per momenti'''
		if unit is None: unit=self.unit
		if unit=='N-mm': unit=['N' , 'Nmm']
		elif unit=='Kg-cm': unit=['Kg','Kgcm']
		plt_(sol,tit,unit)
		

	@property
	def unit(self):
		return self.ex_('select value from dbinfo where name="unit"')[0][0]
	
	def cerca_nodo(self,elementi):
		self.ex('create temp view if not exists elementiNodi as select id,i as n from d2 union select id,j as n from d2')
		nodi=self.ex_('select count(*),n from (select * from elementiNodi where id in (' + ('?,'*len(elementi))[:-1] + ')) group by n',elementi)
		return max(nodi)[1]






#	de get_sez(self,elem):
#		query='select sezioni.tipo,travi.id from sezioni inner join travi on travi.sez=sezioni.id where travi.id '
#		dati=[]
#		if type(elem)in (list,tuple):
#			query+= 'in '+qm_like(elem)
#			dati.extend(elem)
#		else:
#			query+='=? '
#			dati.append(elem)
#		return self.db.execute(query,dati).fetchall()
#
#	def get_vec(self,elem):
#		'''ritorna il vettore orientato dell'elemento non normalizzato'''
#		i=r_[self.ex_('select x,y,z from nodi inner join travi on travi.i = nodi.id where travi.id=:elem',locals())][0]
#		j=r_[self.ex_('select x,y,z from nodi inner join travi on travi.j = nodi.id where travi.id=:elem',locals())][0]
#		return j-i

class NodeError(Exception):
	pass
