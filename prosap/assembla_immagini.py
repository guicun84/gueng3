﻿#-*-coding:utf-8-*-
import os
from lxml.html import etree
import re
import pypandoc
try:
	from PIL import Image
except:
	pass
import tempfile


class AssemblaImmagini:
	def __init__(self,dire='.',columns=1,path='',hmax=120,wmax=170,maxdpi=150):
		'''dire= directori dove operare in cui sono le immagini
		columns=colonne di output 
		path=regex per trovare nomi dei file
		hmax=altezza massima immagine
		
		l'ordine dei file in output è dato dall'ordine alfabetico dei file,
		se sono presenti dei [\d -_]* all'inizio del nome file vengono rimossi per le immagini dei carichi viene rimossa la stringa iniziale ".*_CARICHI "'''
		self.dire=dire
		self.columns=columns
		self.path=path
		self.wmax=wmax
		self.hmax=hmax
		self.maxdpi=maxdpi
		self.assembla()

	def assembla(self):
		tempdir=tempfile.mkdtemp(prefix='resized_',dir='.')
		print(tempdir)
		'''genera l'elemento html formattato'''
		fi=[i for i in os.listdir(self.dire) if os.path.splitext(i)[1].lower()=='.png']
		fi=[i for  i in fi if re.match(self.path,i)]
		fi.sort()
		self.html=etree.Element('html')
		self.html.append(etree.Element('head'))
		b=etree.Element('body')
		self.html.append(b)
		w='{:d}'.format(int(91.*self.wmax/25.4/self.columns))
		tb=etree.Element('table')
		b.append(tb)
		from itertools import cycle
		for f,n in zip(fi,cycle(list(range(self.columns)))):
			if n==0:
				tr=etree.Element('tr')
				tb.append(tr)
			td=etree.Element('td')
			tr.append(td)
			p=etree.Element('p')
			td.append(p)
			i=etree.Element('img')
			p.append(i)
			i.attrib['src']=os.path.join(tempdir,f)
			#valuto rapporto d'aspetto
			img=Image.open(f)
			size=img.size
			ratio=float(size[0])/size[1]
			#ridimensiono
			wpmax=int(self.wmax/25.4*self.maxdpi)
			hpmax=int(self.hmax/25.4*self.maxdpi)
			if wpmax/ratio<hpmax:
				img=img.resize((wpmax,int(wpmax/ratio)))
			else:
				img=img.resize((int(wpmax/ratio),hpmax))
			img.save(os.path.join(tempdir,f))

			print(f,ratio,self.wmax/ratio,self.hmax)	
			if self.wmax/ratio<=self.hmax:
				i.attrib['width']=w
			else:
				i.attrib['width']='{:d}'.format(int(91*(self.wmax/ratio)/25.4/self.columns))
			p=etree.Element('p')
			td.append(p)
			f=re.sub('(?:[\d\-_ ]*)(?:.*CARICHI )?(.*?)_?\.(?:png|PNG)','\\1',f)
			p.text=f
	def salvaHtml(self,finame='immagini'):
		'''salva in html il risultato'''
		st= etree.tostring(self.html,pretty_print=True)
		fi=open(os.path.join(self.dire,finame+'.html'),'w')
		fi.write(st)
		fi.close()
	def salvaOdt(self,finame='immagini'):
		'''salva in odt il risultato'''
		st= etree.tostring(self.html,pretty_print=True)
		pypandoc.convert_text(st,format='html',to='odt',outputfile=os.path.join(self.dire,finame+'.odt'))
