#-*-coding:utf-8-*-
from sqlite3 import connect
from gueng.utils import qm_like
import re
from scipy import *
from scipy.linalg import norm

class struttura:
	def __init__(self,fi):
		self.fi=fi
		self.db=connect(fi)
	
	def ex(self,query,dat=None):
		if dat is not None:
			return self.db.execute(query,dat)
		else:
			return self.db.execute(query)
	
	def ex_(self,query,dat=None):
		return self.ex(query,dat).fetchall()
	
	def get_sol(self,elem,part=None,cmb=None):
		if part==1:
			query='select N_1,V2_1,M3_1,cmb,elem from sollecitazioni where elem '
		elif part==2:
			query='select N_2,V2_2,M3_2,cmb,elem from sollecitazioni where elem '
		else:
			query='select N_1,V2_1,M3_1,N_2,V2_2,M3_2,cmb,elem from sollecitazioni where elem '	
		
		dat=[]
		if type(elem) in (list,tuple):
			query += 'in '+ qm_like(elem)
			dat.extend(elem)
		else:
			query+= '=?'
			dat.append(elem)
		if cmb:
			if type(cmb) in [list,tuple]:
				query += 'and cmb in ' +qm_like(cmb)
				dat.extend(cmb)
			else:
				query += 'and cmb =? '
				dat.append(cmb)
		return r_[self.db.execute(query,dat).fetchall()]

	def get_sol_max(self,elem,cmb=None):
		query='select M3max,M3min,M2max,M2min,cmb,elem from sollecitazioni where elem '	
		
		dat=[]
		if type(elem) in (list,tuple):
			query += 'in '+ qm_like(elem)
			dat.extend(elem)
		else:
			query+= '=?'
			dat.append(elem)
		if cmb:
			if type(cmb) in [list,tuple]:
				query += 'and cmb in ' +qm_like(cmb)
				dat.extend(cmb)
			else:
				query += 'and cmb =? '
				dat.append(cmb)
		return r_[self.db.execute(query,dat).fetchall()]

	def get_sol_tot(self,elem,part=None,cmb=None):
		if part==1:
			query='select N_1,V2_1,V3_1,T_1,M2_1,M3_1,cmb,elem from sollecitazioni where elem '
		elif part==2:
			query='select N_2,V2_2,V3_2,T_2,M2_2,M3_2,cmb,elem from sollecitazioni where elem '
		else:
			query='select N_1,V2_1,V3_1,T_1,M2_1,M3_1,N_2,V2_2,V3_2,T_2,M2_2,M3_2,cmb,elem from sollecitazioni where elem '	
		
		dat=[]
		if type(elem) in (list,tuple):
			query += 'in '+ qm_like(elem)
			dat.extend(elem)
		else:
			query+= '=?'
			dat.append(elem)
		if cmb:
			if type(cmb) in [list,tuple]:
				query += 'and cmb in ' +qm_like(cmb)
				dat.extend(cmb)
			else:
				query += 'and cmb =? '
				dat.append(cmb)
		return r_[self.db.execute(query,dat).fetchall()]


	def get_dir(self,elem):
		nodi=r_[self.ex_('select nodi.id,x,y,z from nodi inner join travi on travi.i=nodi.id or travi.j=nodi.id where travi.id=:elem',locals())]
		i=r_[self.ex_('select i from travi where id=:elem',locals())][0]
		ni=where(nodi[:,0]==i)[0][0]
		nj=where(nodi[:,0]!=i)[0][0]
		i=nodi[ni,1:]
		j=nodi[nj,1:]
		e1=j-i
		e1/=norm(e1)
		e3=cross(e1,r_[0,0,1.])
		if norm(e3)==0:
			e3=cross(e1,r_[-1.,0,0])
		e3/=norm(e3)
		e2=cross(e3,e1)
		e2/=norm(e2)
		M=c_[[e1,e2,e3]].T
		return M

	def get_sol_glob(self,elem,part,cmb=None):
		sol=self.get_sol_tot(elem,part,cmb)
		if part==2: sol[:,:6]*=-1
		M_rot=self.get_dir(elem)
		for n,i in enumerate( sol):
			sol[n,:3]=dot(M_rot,i[:3]*r_[1,-1,-1])
			sol[n,3:6]=dot(M_rot,i[3:6])
		return sol



	def get_sez(self,elem):
		query='select sezioni.tipo,travi.id from sezioni inner join travi on travi.sez=sezioni.id where travi.id '
		dati=[]
		if type(elem)in (list,tuple):
			query+= 'in '+qm_like(elem)
			dati.extend(elem)
		else:
			query+='=? '
			dati.append(elem)
		return self.db.execute(query,dati).fetchall()

	def get_vec(self,elem):
		'''ritorna il vettore orientato dell'elemento non normalizzato'''
		i=r_[self.ex_('select x,y,z from nodi inner join travi on travi.i = nodi.id where travi.id=:elem',locals())][0]
		j=r_[self.ex_('select x,y,z from nodi inner join travi on travi.j = nodi.id where travi.id=:elem',locals())][0]
		return j-i
