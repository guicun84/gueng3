#-*-coding:latin-*-
import subprocess,time,re,sys,datetime as dt,pandas as pa
from gueng.telegram import sendMessage
from io import StringIO
import threading

class Processo:
	def __init__(self,name,dmin=2,mexmin=20):
		vars(self).update(locals())
		self.lastCheck=time.time()
		self.startRun=time.time()-dmin*3
		self.checkRun=time.time()-dmin*3
		self.stopRun=time.time()-dmin*1.1
		self.checkStop=time.time()-dmin*1.1
		print('creato nuovo processo {}'.format(name))

	def checkEsecuzione(self,p):
		self.lastCheck=time.time()
		if p==0 and self.run: #stop process
			if self.lastCheck-self.checkRun>self.dmin:
				self.stopRun=self.lastCheck-self.dmin
				self.checkStop=self.checkRun
		elif p==0 and not self.run: #continue stop process
			self.checkStop=self.lastCheck
		elif p!=0 and not self.run: #start process
			if self.lastCheck-self.checkStop>self.dmin:
				self.checkRun=self.lastCheck
				self.startRun=self.lastCheck-self.dmin
		elif p!=0 and self.run: #continue start porcess
			self.checkRun=self.lastCheck
		self.message()

	def closeProcess(self):
		self.lastCheck=time.time()
		self.stopRun=self.lastCheck
		self.checkStop=self.checkRun
		print('chiuso processo {}'.format(self.name))
		self.message()	

	@property 
	def run(self):
		return self.startRun>self.stopRun

	@property
	def durata(self):
		if self.run:
			return time.time()-self.startRun
		return self.stopRun-self.startRun

	@property
	def stdurata(self):
		t=dt.datetime.combine(dt.date.today(),dt.time(0,0,0))+dt.timedelta(seconds=self.durata)
		return dt.datetime.strftime(t,'%H:%M:%S')

	@property
	def switch(self):
		t=time.time()
		k=2
		return  (t-self.startRun) <self.dmin*k or (t-self.stopRun) <self.dmin*k

	def __str__(self):
		out= 'processo {}'.format(self.name)
		if self.run: out+=' stato in ESECUZIONE'
		else: out+=' stato in sospensione'
		out+=' '+self.stdurata +' '
		return out

	def message(self):
		if self.run:
			sys.stdout.write('\r%s'%self)
			sys.stdout.flush()
		if self.switch and not self.run:
			print() 
			print('%s'%self)
			if self.durata>self.mexmin:
				sendMessage('{}'.format(self))
				time.sleep(2*self.dmin)

			
	
class Pswinmonitor2:
	def __init__(self,continuo=True,dmin=2,mexmin=60,autorun=True):
		vars(self).update(locals())
		self.com2='wmic path win32_perfformatteddata_perfproc_process get Name, Caption, PercentProcessorTime /format:list'
		self.processi=[]
		self.runMonitor=False
		if autorun:
			self.cicloControllaProcessi()
	
	def getWmic(self):
		proc=[]
		out=subprocess.check_output(self.com2)
		pswin=None
		for i in out.split('\n'):
			for name in ['pswin','e_Sap']:
				test= re.match('Name=({}[^\r]*)'.format(name),i)
				if test: break
			if test:
				pswin=test.group(1)
				continue
			test= re.match('PercentProcessorTime=(\d+)',i) 
			if test and pswin:
				proc.append([pswin,int(test.group(1))])
				pswin=None
				continue
		return proc
	
	def controllaProcessi(self):
		proc=self.getWmic()
		#controllo processi esistenti o da creare
		for p in proc:
			test=[i for i in self.processi if i.name==p[0]]
			if not test:
				prs=Processo(p[0],dmin=self.dmin,mexmin=self.mexmin)
				self.processi.append(prs)
			else:
				if len(test)!=1: raise IOError('presenti più processi con stesso nome').with_traceback(test)
				prs=test[0]
			prs.checkEsecuzione(p[1])
		#elimino processi chiusi:
		procn=[i[0] for i in proc]
		for i in [j for j in self.processi if j.name not in procn]:
			i.closeProcess()
		self.processi=[i for i in self.processi if i.name in procn]
		
	def cicloControllaProcessi(self):
		self.runMonitor=True
		def threadfun():
			while True and self.runMonitor:
				ts=time.time()
				self.controllaProcessi()
				dt=( 1-(time.time()-ts))
				if dt<0:dt=0
				time.sleep(dt)
			print('fine thread controllo')
		tr=threading.Thread(target=threadfun)
		tr.start()

	def stopMonitor(self):
		self.runMonitor=False



		

		

#	
#
#class Pswinmonitor:
#	def __init__(self,continuo=False,dmin=60,autorun=True):
#		vars(self).update(locals())
#		self.com='wmic path win32_perfformatteddata_perfproc_process where Name="pswin" get Name, Caption, PercentProcessorTime /format:list'
#		self.com2='wmic path win32_perfformatteddata_perfproc_process get Name, Caption, PercentProcessorTime /format:list'
#		self.proc=[]
#		if autorun:
#			self.controlla_avvio()
#	
#	def controlla_esecuzione(self):
#		n=0
#		terminato=False
#		ts=time.time()
#		while True:
#			time.sleep(1)
#			durata=time.time()-ts
#			d=dt.datetime.combine(dt.date.today(),dt.time(0,0,0))+dt.timedelta(seconds=durata)
#			st=dt.datetime.strftime(d,'%H:%M:%S')
#			sys.stdout.write('\r'+st)
#			sys.stdout.flush()
#			out=subprocess.check_output(self.com)
#			for i in out.split('\n'):
#				test=re.search('PercentProcessorTime=(\d+)',i)
#				if test:
#					if test.group(1)=='0':
#						n+=1
#						if n>2:
#							terminato=True
#							break
#					else: n=0
#			if terminato:break
#		st='pswin.exe terminato in {}'.format(st)
#		print
#		print(st)
#		if durata>=self.dmin:
#			sendMessage(st)
#		if self.continuo:
#			self.controlla_avvio()
#
#	def controlla_avvio(self):
#		n=0
#		avviato=False
#		while True:
#			time.sleep(1)
#			out=subprocess.check_output(self.com)
#			for i in out.split('\n'):
#				test=re.search('PercentProcessorTime=(\d+)',i)
#				if test:
#					if test.group(1)!='0':
#						n+=1
#						if n>2:
#							avviato=True
#							break
#					else: n=0
#			if avviato:break
#		self.controlla_esecuzione()
#
#	def __call__(self):
#		self.controlla_avvio()
#
#	def controlla_bis(self):
#		keys='Caption', 'CommandLine', 'CreationClassName', 'CreationDate', 'CSCreationClassName', 'CSName', 'Description', 'ExecutablePath', 'ExecutionState', 'Handle', 'HandleCount', 'InstallDate', 'KernelModeTime', 'MaximumWorkingSetSize', 'MinimumWorkingSetSize', 'Name', 'OSCreationClassName', 'OSName', 'OtherOperationCount', 'OtherTransferCount', 'PageFaults', 'PageFileUsage', 'ParentProcessId', 'PeakPageFileUsage', 'PeakVirtualSize', 'PeakWorkingSetSize', 'Priority', 'PrivatePageCount', 'ProcessId', 'QuotaNonPagedPoolUsage', 'QuotaPagedPoolUsage', 'QuotaPeakNonPagedPoolUsage', 'QuotaPeakPagedPoolUsage', 'ReadOperationCount', 'ReadTransferCount', 'SessionId', 'Status', 'TerminationDate', 'ThreadCount', 'UserModeTime', 'VirtualSize', 'WindowsVersion', 'WorkingSetSize', 'WriteOperationCount', 'WriteTransferCount'
#		ts=time.time()
#		com='wmic process'# | findstr /b "pswin.exe"'
#		while True:
#			st=subprocess.check_output(com)
#			dat=pa.read_csv(StringIO(st),sep='\s{2,}')
#			dat=dat[dat.Caption=='pswin.exe']
#			print dat['Caption ProcessId UserModeTime ReadOperationCount WriteOperationCount'.split()]
#			print(time.time()-ts)
#			
#
#
#
