#-*-coding:utf-8-*-
'''questa versione funziona con i db realizzati partendo dal txt fatto da prosap'''
from sqlite3 import connect
from gueng.utils import qm_like
import re
from scipy import *
from scipy.linalg import norm

class Struttura:
	def __init__(self,fi):
		self.fi=fi
		self.db=connect(fi)
	
	def ex(self,query,dat=None):
		if dat is not None:
			return self.db.execute(query,dat)
		else:
			return self.db.execute(query)
	
	def ex_(self,query,dat=None):
		return self.ex(query,dat).fetchall()
	
	def get_sol(self,elem,part=None,cmb=None):
		if part==1:
			query='select N1,V21,M31,cmb,elem from sollecitazioni_d2 where elem '
		elif part==2:
			query='select N2,V22,M32,cmb,elem from sollecitazioni_d2 where elem '
		else:
			query='select N1,V21,M31,N2,V22,M32,cmb,elem from sollecitazioni_d2 where elem '	
		
		dat=[]
		if type(elem) in (list,tuple):
			query += 'in '+ qm_like(elem)
			dat.extend(elem)
		else:
			query+= '=?'
			dat.append(elem)
		if cmb:
			if type(cmb) in [list,tuple]:
				query += 'and cmb in ' +qm_like(cmb)
				dat.extend(cmb)
			else:
				query += 'and cmb =? '
				dat.append(cmb)
		return r_[self.db.execute(query,dat).fetchall()]


	def get_sol_tot(self,elem,part=None,cmb=None):
		if part==1:
			query='select N1,V21,V31,M11,M21,M31,cmb,elem from sollecitazioni_d2 where elem '
		elif part==2:
			query='select N2,V22,V32,M12,M22,M32,cmb,elem from sollecitazioni_d2 where elem '
		else:
			query='select N1,V21,V31,M11,M21,M31,N2,V22,V32,M12,M22,M32,cmb,elem from sollecitazioni_d2 where elem '	
		
		dat=[]
		if type(elem) in (list,tuple):
			query += 'in '+ qm_like(elem)
			dat.extend(elem)
		else:
			query+= '=?'
			dat.append(elem)
		if cmb:
			if type(cmb) in [list,tuple]:
				query += ' and cmb in ' +qm_like(cmb)
				dat.extend(cmb)
			else:
				query += ' and cmb =? '
				dat.append(cmb)
		return r_[self.db.execute(query,dat).fetchall()]


	def get_dir(self,elem,f=0):
		nodi=r_[self.ex_('select nodi.id,x,y,z from nodi inner join d2 on d2.i=nodi.id or d2.j=nodi.id where d2.id=:elem',locals())]
		i=r_[self.ex_('select i from d2 where id=:elem',locals())][0]
		ni=where(nodi[:,0]==i)[0][0]
		nj=where(nodi[:,0]!=i)[0][0]
		i=nodi[ni,1:]
		j=nodi[nj,1:]
		e1=j-i
		e1/=norm(e1)
		e3=cross(e1,r_[0,0,1.])
		if norm(e3)==0:
			e3=cross(e1,r_[-1.,0,0])
		e3/=norm(e3)
		e2=cross(e3,e1)
		e2/=norm(e2)
		M=c_[[e1,e2,e3]]
		rot=deg2rad(self.ex_('select rotazione from d2 where id=:elem',locals())[0])
		if rot!=0:
			Mx=matrix([[1,0,0],[0,cos(rot),-sin(rot)],[0,sin(rot),cos(rot)]])
			M=dot(Mx.T,M)
		if f and rot!=0:
			return M.T,Mx
		else:
			return M.T

	def get_sol_glob(self,elem,part,cmb=None):
		def run(elem,part,cmb):
			sol=self.get_sol_tot(elem,part,cmb)
			if part==1: sol[:,:6]*=-1
			M_rot=self.get_dir(elem)
			for n,i in enumerate(sol):
				sol[n,:3]=dot(M_rot,i[:3]*r_[-1,1,1])
				sol[n,3:6]=dot(M_rot,i[3:6])
			return sol
		if hasattr(elem,'__len__'):
			if not hasattr(cmb,'__len__'): cmb=[cmb]*len(elem)
			return r_[[run(elem[n],part[n],cmb[n]) for n,t in enumerate(elem)]]
		else:
			return run(elem,part,cmb)



#	def get_sez(self,elem):
#		query='select sezioni.tipo,travi.id from sezioni inner join travi on travi.sez=sezioni.id where travi.id '
#		dati=[]
#		if type(elem)in (list,tuple):
#			query+= 'in '+qm_like(elem)
#			dati.extend(elem)
#		else:
#			query+='=? '
#			dati.append(elem)
#		return self.db.execute(query,dati).fetchall()
#
#	def get_vec(self,elem):
#		'''ritorna il vettore orientato dell'elemento non normalizzato'''
#		i=r_[self.ex_('select x,y,z from nodi inner join travi on travi.i = nodi.id where travi.id=:elem',locals())][0]
#		j=r_[self.ex_('select x,y,z from nodi inner join travi on travi.j = nodi.id where travi.id=:elem',locals())][0]
#		return j-i
