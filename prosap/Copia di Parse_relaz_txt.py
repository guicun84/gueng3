from io import StringIO
from itertools import cycle
import sqlite3
from scipy import *
from scipy.linalg import norm
import re,os
'''queste routine sono da applicare alla relazione in formato txt ottenuta direttamente da prosap'''
##questo script riesce partendo da una relazione in txt a dividere le tabelle e le passa su oggetti StringIO utilizzabili come fossero file
class Parse_relaz:
	def __init__(self,fi):
		self.fi=open(fi,'r')

	def isola_mat(self):
		out=StringIO()
		self.fi.seek(0)
		fine_doc=False
		n=0
		while True:
			n+=1
			ri=self.fi.readline()
			if ri=='':
				fine_doc=True
				break
			if re.match(' =* *MODELLAZIONE DEI MATERIALI *=*',ri):
				break
		if not fine_doc:
			#comincio a ricercare i dati dei materiali:
			mat=None
			mats=[]
			for i in range(7):
				self.fi.readline()
			while True:
				ri=self.fi.readline()
				if re.match('^ =+$',ri):
					mats.append(mat)
					break
				if re.match(' Codice\s+=',ri):
					if mat is not None:
						mats.append(mat)
					mat=[]
					mat.append(re.search('=\s*(.*)$',ri).groups(1)[0])
				else:
					test=re.search('.*?=\s*(.*)$',ri)
					if test:
						mat.append(test.groups(1)[0])
			for i in mats:
				out.write('\t'.join(i) +'\n')
			out.seek(0)
		return out
						
	############################################################	
	def isola_sez(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			i=self.fi.readline()
			if i=='':break
			if re.search(' =*MODELLAZIONE DELLE SEZIONI *=*',i):
				check=True
				break
		if check:
			for i in range(7):
				self.fi.readline()
			nomi={}
			f1=False
			while True:
				ri=self.fi.readline()
				if re.match(' Codice\s+',ri):
					if f1: break
					else:f1=True
				g=re.search('^\s+(\d+)\s+(.*)',ri)
				if g:
					g=g.groups()
					nomi[g[0]]=g[1]
			dati={}
			while True:
				ri=self.fi.readline()
				if re.match(' =+$',ri): break
#				g=re.search('\s(\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)\s+(\d+\.\d+)',ri)
				g=re.search('\s+(\d+)\s+([0-9e+.]+)\s+([0-9e\-+.]+)\s+([0-9e+.]+)\s+([0-9e+.]+)\s+([0-9e+.]+)\s+([0-9e+.]+)',ri)
				if g:
					g=g.groups()
					dati[g[0]]=g[1:]
			st=[]
			for i in list(nomi.keys()):
				r=[i,nomi[i]]+list(dati[i])
				st.append('\t'.join(r))
			out.write('\n'.join(st))
			out.seek(0)
		return out

	def isola_nodi(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match(' LEGENDA TABELLA DATI NODI',ri): 
				self.fi.readline()
				check=True
				break
		nodi=[]
		if check:
			while True:
				ri=self.fi.readline()
				if re.match('\s+=+$',ri): 
					break
				g=re.search('\s+([0-9.e+\-]+)'*11,ri)
				if g:
					g=g.groups()
					nodi.append('\t'.join(g))
		out.write('\n'.join(nodi))
		out.seek(0)
		return out

	def isola_d2(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match(' LEGENDA TABELLA DATI TRAVI',ri): 
				self.fi.readline()
				check=True
				break
		elementi=[]
		if check:
			while True:
				ri=self.fi.readline()
				if re.match('\s+=+$',ri): 
					break
				g=re.search('\s+(\d+)\s+([A-Z])'+'\s+([0-9.e+\-]+)'*9,ri)
				if g:
					g=g.groups()
					elementi.append('\t'.join(g))
		out.write('\n'.join(elementi))
		return out

	def isola_spostamenti(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match(' LEGENDA RISULTATI NODALI',ri): 
				self.fi.readline()
				check=True
				break
		spostamenti=[]
		if check:
			while True:
				ri=self.fi.readline()
				if re.match('\s+=+$',ri) or 'Nodo   Cmb   azione X  azione  Y  azione  Z  azione RX  azione RY  azione RZ' in ri:
					break
				g=re.search('\s+([0-9.e+\-]+)'*8,ri)
				if g:
					g=g.groups()
					spostamenti.append('\t'.join(g))
		out.write('\n'.join(spostamenti))
		return out

	def isola_azioni(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match(' LEGENDA RISULTATI NODALI',ri): 
				self.fi.readline()
				check=True
				break
		azioni=[]
		if check:
			check=False
			while True:
				ri=self.fi.readline()
				if  'Nodo   Cmb   azione X  azione  Y  azione  Z  azione RX  azione RY  azione RZ' in ri:
					check=True
					break
			if check:
				while True:
					ri=self.fi.readline()
					if re.match('\s+=+$',ri):
						break
					g=re.search('\s+([0-9.e+\-]+)'*8,ri)
					if g:
						g=g.groups()
						azioni.append('\t'.join(g))
		out.write('\n'.join(azioni))
		return out

	def isola_sol_pilastri(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match(' LEGENDA RISULTATI ELEMENTI TIPO TRAVE',ri): 
				self.fi.readline()
				check=True
				break
		sol=[]
		c=cycle([1,2,3])
		cur=next(c)
		if check:
			while True:
				ri=self.fi.readline()
				if re.match(' Trave Cmb   Sforzo N  Taglio V2  Taglio V3  Momento T Momento M2 Momento M3',ri) or ri=='': 
					break
				if cur==1:
					g=re.search('\s+([0-9.e+\-]+)'*2,ri)
				else:
					g=re.search('\s+([0-9.e+\-]+)'*7,ri)
				if g:
					g=g.groups()
					if cur==1:
						row=g
					else:
						row+=g
					if cur==3:
						sol+=['\t'.join(row)]
					cur=next(c)
		out.write('\n'.join(sol))
		return out
	def isola_sol_travi(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match(' Trave Cmb   Sforzo N  Taglio V2  Taglio V3  Momento T Momento M2 Momento M3',ri): 
				self.fi.readline()
				check=True
				break
		sol=[]
		c=cycle([1,2,3,4])
		cur=next(c)
		if check:
			while True:
				ri=self.fi.readline()
				if re.match(' =+$',ri) or ri=='' or re.match(' Trv f. Cmb',ri): 
					break
				if cur==1:
					g=re.search('\s+([0-9.e+\-]+)'*2,ri)
				elif cur in (2,3):
					g=re.search('\s+([0-9.e+\-]+)'*7,ri)
				else:
					g=re.search('\s+([0-9.e+\-]+)'*5,ri)
				if g:
					g=g.groups()
					if cur==1:
						row=g
					else:
						row+=g
					if cur==4:
						sol+=['\t'.join(row)]
					cur=next(c)
		out.write('\n'.join(sol))
		return out
	
	def isola_sol_travi_fondazioni(self):
		out=StringIO()
		self.fi.seek(0)
		check=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match(' Trv f. Cmb   Sforzo N  Taglio V2  Taglio V3  Momento T Momento M2 Momento M3',ri): 
				self.fi.readline()
				check=True
				break
		sol=[]
		c=cycle([1,2,3,4])
		cur=next(c)
		if check:
			while True:
				ri=self.fi.readline()
				if re.match(' =+$',ri) or ri=='' : 
					break
				if cur==1:
					g=re.search('\s+([0-9.e+\-]+)'*2,ri)
				elif cur in (2,3):
					g=re.search('\s+([0-9.e+\-]+)'*7,ri)
				else:
					g=re.search('\s+([0-9.e+\-]+)'*4,ri)
				if g:
					g=g.groups()
					if cur==1:
						row=g
					else:
						row+=g
					if cur==4:
						sol+=['\t'.join(row)]
					cur=next(c)
		out.write('\n'.join(sol))
		return out

	def isola_pressioni_fondazioni_d3(self):
		self.fi.seek(0)
		test=False
		while True:
			ri=self.fi.readline()
			if ri=='':break
			if re.match('Pressioni indotte da gusci su suolo elastico' ,ri):
				test=True
				break

		cur_nod=None
		cur_dat=[]
		dat=[]
		if test:
			while True:
				ri=self.fi.readline()
				if ri=='' :break
				if 'Pressioni' in ri:
					if not re.match('Pressioni indotte da gusci su suolo elastico' ,ri): break
				#controllo se iniziano i dati di un nuovo nodo
				sl=ri[:6]
				te=re.search('[0-9]+',sl)
				if te: 
					if cur_nod is not None:
						for n,i in enumerate(cur_dat):
							dat.append('\t'.join([cur_nod,'%d'%(n+1),i]))
					g=re.finditer('[0-9.e+\-]+',ri)
					cur_nod=te.group()
					cur_dat=[i.group() for i in g]
					cur_dat=cur_dat[1:]
				elif re.match('^\s+$',sl):
					g=re.finditer('[0-9.e+\-]+',ri)
					if g:
						cur_dat+=[i.group() for i in g]

		if cur_nod is not None:
			for n,i in enumerate(cur_dat):
				dat.append('\t'.join([cur_nod,'%d'%(n+1),i]))
		out=StringIO()
		out.write('\n'.join(dat))
		return out



class Loader:
	def __init__(self,fi,rel=None):
		if rel==None:
			name=os.path.splitext(fi)
			if name[1].lower()=='.txt':
				rel=fi
				fi=name[0]+'.sqlite'
			else:
				rel=name[0]+'.txt'
		self.db=sqlite3.connect(fi)
		self.relaz=Parse_relaz(rel)

	def crea(self):
		self.db.execute('create  table if not exists materiali (id integer primary key,tipo text, name text,E real  ,nu real ,G real,gamma real ,alpha real ,var text)')
		self.db.execute('create table if not exists sezioni( id integer primary key ,name text, A real, AV2 real,AV3 real,Jt real, J22 real ,J33 real)')
		self.db.execute('create table  if not exists nodi ( id integer primary key , x real,y real,z real,vx integer,vy integer,vz integer, rx integer, ry integer, rz integer,fondazione integer)')
		self.db.execute('create table  if not exists d2 (id integer prmary key, tipo text,i integer,j integer, materiale integer ,sezione integer, rotazione real ,svincoli_i text ,svincoli_j text,winkler_V real ,winkler_O real)')
		self.db.execute('create table if not exists sollecitazioni_d2 (id integer primary key autoincrement, elem integer , cmb integer,x1 real, N1 real , V21 real ,V31 real, M11 real,M21 real ,M31 real,x2 real,N2 real , V22 real ,V32 real, M12 real,M22 real ,M32 real)')
		self.db.execute('create table if not exists spostamenti(id integer primary key autoincrement, nodo integer ,cmb integer ,vx real,vy real,vz real,rx real ,ry real,rz real)')
		self.db.execute('create table if not exists azioni(id integer primary key autoincrement, nodo integer ,cmb integer ,fx real,fy real,fz real,mx real ,my real,mz real)')
		self.db.execute('create table pressioni_fondazione_d3 (id integer primary key autoincrement, nodo integer, cmb integer, p real)')

	def drop_all(self):
		self.db.executescript('''drop table materiali;
		drop table sezioni;
		drop table nodi;
		drop table d2;
		drop table sollecitazioni_d2;
		drop table spostamenti;
		drop table azioni;
		drop table pressioni_fondazione_d3''')
		self.db.commit()


	def load_mat(self,com=True):
		mat=self.relaz.isola_mat().getvalue().split('\n')
		k=0
		for n in range(len(mat)):
			m=mat[n-k]
			dat=m.split('\t')
			if len(dat)>=9:
				dat=[int(dat[0])]+[dat[i] for i in [1,2]]+[float(dat[i]) for i in range(3,8)]+[' ,'.join(dat[8:])]
				mat[n]=dat
			else:
				del mat[n-k]
				k+=1
		if len(mat)==0: return
		self.db.executemany('insert into materiali values(?,?,?,?,?,?,?,?,?)',mat)
		if com:self.db.commit()

	def load_sez(self,com=True):
		sez=self.relaz.isola_sez().getvalue()
		sez=sez.split('\n')
		for n,r in enumerate(sez):
			dat=r.split('\t')
			if len(dat)!=8:
				del sez[n]
			else:
				sez[n]=[int(dat[0]),dat[1]]+[float(dat[i]) for i in range(2,8)]
		if len(sez)==0: return
		self.db.executemany('insert into sezioni values(?,?,?,?,?,?,?,?)',sez)
		if com:	self.db.commit()

	def load_nodi(self,com=True):
		nodi=self.relaz.isola_nodi().getvalue().split('\n')
		for n,r in enumerate(nodi):
			dat=r.split('\t')
			if len(dat)!=11:
				del nodi[n]
			else:
				nodi[n]=[int(dat[0])]+[float(dat[i]) for i in range(1,4)] + [int(dat[i]) for i in range(4,11)]
		if len(nodi)==0:return 
		self.db.executemany('insert into nodi values(?,?,?,?,?,?,?,?,?,?,?)',nodi)
		if com:self.db.commit()

	def load_d2(self,com=True):
		d2=self.relaz.isola_d2().getvalue().split('\n')
		for n,d in enumerate(d2):
			dat=d.split('\t')
			if len(dat)!=11:
				del d2[n]
			else:
				d2[n]=[int(dat[0]),dat[1]]+[int(dat[i]) for i in range(2,6)] + [float(dat[6]),dat[7],dat[8]] +[float(dat[i]) for i in range(9,11)]
		if len(d2)==0:return 
		self.db.executemany('insert into d2 values(?,?,?,?,?,?,?,?,?,?,?)',d2)
		if com: self.db.commit()

	def load_sol_d2(self,com=True):
		pil=self.relaz.isola_sol_pilastri().getvalue().split('\n')
		tr=self.relaz.isola_sol_travi().getvalue().split('\n')
		for n,p in enumerate(pil):
			dat=p.split('\t')
			if len(dat)!=16:
				del pil[n]
			else:
				pil[n]=[int(dat[i]) for i in range(2)]+[float(dat[i]) for i in range(2,16)]
#				self.db.execute('insert into sollecitazioni_d2 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',pil[n])
		if len(pil)!=0: 
			self.db.executemany('insert into sollecitazioni_d2 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',pil)
		# 'finito con i pilastri'
		for n,t in enumerate(tr):
			dat=t.split('\t')
			if len(dat)!=21:
				del tr[n]
			else:
				tr[n]=[int(dat[i]) for i in range(2)]+[float(dat[i]) for i in range(2,16)]
#				self.db.execute('insert into sollecitazioni_d2 values(?,?,?,?,?,?,?,?)',tr[n])
		if len(tr)!=0: 
			self.db.executemany('insert into sollecitazioni_d2 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',tr)
#		self.db.executemany('insert into sollecitazioni_d2 values(?,?,?,?,?,?,?,?)',tr)
		trf=self.relaz.isola_sol_travi_fondazioni().getvalue().split('\n')
		for n,t in enumerate(trf):
			dat=t.split('\t')
			if len(dat)!=20:
				del trf[n]
			else:
				trf[n]=[int(dat[i]) for i in range(2)]+[float(dat[i]) for i in range(2,16)]
#				self.db.execute('insert into sollecitazioni_d2 values(?,?,?,?,?,?,?,?)',tr[n])
		if len(trf)!=0:
			self.db.executemany('insert into sollecitazioni_d2 values(Null,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',trf)
#		self.db.executemany('insert into sollecitazioni_d2 values(?,?,?,?,?,?,?,?)',tr)
		if com:self.db.commit()
	
	def load_spostamenti(self,com=True):
		sp=self.relaz.isola_spostamenti().getvalue().split('\n')
		for n,s in enumerate(sp):
			dat=s.split('\t')
			if len(dat)!=8:
				del sp[n]
			else:
				sp[n]=[int(dat[i]) for i in range(2)]+[float(dat[i]) for i in range(2,8)]
		if len(sp)==0:return 
		self.db.executemany('insert into spostamenti values(Null,?,?,?,?,?,?,?,?)',sp)
		if com: self.db.commit()

	def load_azioni(self,com=True):
		az=self.relaz.isola_azioni().getvalue().split('\n')
		for n,s in enumerate(az):
			dat=s.split('\t')
			if len(dat)!=8:
				del az[n]
			else:
				az[n]=[int(dat[i]) for i in range(2)]+[float(dat[i]) for i in range(2,8)]

		if len(az)==0:return 
		self.db.executemany('insert into azioni values(Null,?,?,?,?,?,?,?,?)',az)
		if com: self.db.commit()
	
	def load_pressioni_fondazione_d3(self,com=True):
		p3=self.relaz.isola_pressioni_fondazioni_d3().getvalue().split('\n')
		for n,i in enumerate(p3):
			dat=i.split('\t')
			if len(dat)!=3:
				del p3[n]
			else:
				p3[n]=[int(dat[0]),int(dat[1]),float(dat[2])]
		if len(p3)==0:return 
		self.db.executemany('insert into pressioni_fondazione_d3 values(Null,?,?,?)',p3)
		if com:
			self.db.commit()
	
	def load(self):
		self.load_mat(0)
		self.load_sez(0)
		self.load_nodi(0)
		self.load_d2(0)
		self.load_sol_d2(0)
		self.load_spostamenti(0)
		self.load_azioni(0)
		self.load_pressioni_fondazione_d3(0)
		self.db.commit()
	def ex(self,st,args=[]):
		return self.db.execute(st,args)
	
	def ex_(self,st,args=[]):
		return self.ex(st,args).fetchall()

	def scala_Kgcm_Nmm(self):
		self.ex('update materiali set E=E*.10,G=G*.1,gamma=gamma*1000000')
		self.ex('update sezioni set A=A*100,AV2=AV2*100,AV3=AV3*100,Jt=Jt*10000,J22=J22*10000,J33=J33*10000')
		self.ex('update nodi set x=x*10,y=y*10,z=z*10')
		self.ex('update sollecitazioni_d2 set x1=x1*10,N1=N1*10,V21=V21*10,V31=V31*10,M11=M11*100,M21=M21*100,M31=M31*100,x2=x2*1,N2=N2*10,V22=V22*10,V32=V32*10,M12=M12*100,M22=M22*100,M32=M32*100')
		self.ex('update spostamenti set vx=vx*10,vy=vy*10,vz=vz*10')
		self.ex('update azioni set fx=fx*10,fy=fy*10, fz=fz*10 , mx=mx*100,my=my*100,mz=mz*100')
		self.ex('update pressioni_fondazione_d3 set p=p*.1')
		self.db.commit()
		
	def scala_Nmm_Kgcm(self):
		self.ex('update materiali set E=E/.10,G=G/.1,gamma=gamma/.1000000')
		self.ex('update sezioni set A=A/100,AV2=AV2/100,AV3=AV3/100,Jt=Jt/10000,J22=J22/10000,J33=J33/10000')
		self.ex('update nodi set x=x/10,y=y/10,z=z/10')
		self.ex('update sollecitazioni_d2 set x1=x1/10,N1=N1/10,V21=V21/10,V31=V31/10,M11=M11/100,M21=M21/100,M31=M31/100,x2=x2/1,N2=N2/10,V22=V22/10,V32=V32/10,M12=M12/100,M22=M22/100,M32=M32/100')
		self.ex('update spostamenti set vx=vx/10,vy=vy/10,vz=vz/10')
		self.ex('update azioni set fx=fx/10,fy=fy/10, fz=fz/10 , mx=mx/100,my=my/100,mz=mz/100')
		self.ex('update pressioni_fondazione_d3 set p=p/.1')
		self.db.commit()
	
	

#class Struttura:
#	def __init__(self,fi):
#		self._db=sqlite3.connect(fi)
#
#	def ex(self-,st,args):
#		return self._db.execute(st,args)
#	
#	def ex_(self,st,args):
#		return self.ex(st,args).fetchall()
#	
#	def get_sol(self,elem,cmb=None):
#		if cmb is None:
#			dat=self.ex_('select * from sollecitazioni_d2 where elem=?',(elem,))
#		elif hasattr(cmb,'__len__'):
#			dat=self.ex_('select * from sollecitazioni_d2 where elem=? and cmb in (' + ','.join(['?']*len(cmb)) +')',[elem]+list(cmb))
#		else:
#			dat=self.ex_('select * from sollecitazioni_d2 where elem=? and cmb =?',(elem,cmb))
#		return dat
#	def sol(self,elem,cmb=None):
#		dat=self.get_sol(elem,cmb)
#		if len(dat)==1:
#			return D2(dat[0],self)
#		else:
#			return [D2(i,self) for i in dat]
#
#	def sol_glob(self,elem,cmb=None):
#		dat=r_[self.get_sol(elem,cmb)]
#		#creo matrice di rotazione elemento:
#		xi=r_[self.ex_('select x,y,z from nodi inner join d2 on nodi.id==d2.i where d2.id=?',(elem,))[0]]
#		xj=r_[self.ex_('select x,y,z from nodi inner join d2 on nodi.id==d2.j where d2.id=?',(elem,))[0]]
#		rot=self.ex_('select rotazione from d2 where id=?',(elem,))[0][0]
#		v=xj-xi
#		e1=v/norm(v)
#		e2=cross(r_[0,0,1,],e1)
#		if norm(e2)==0:
#			e3=cross(e1,r_[0,-1,0])
#			e3=e3/norm(e3)
#			e2=cross(e3,e1)
#		else:
#			e2=e2/norm(e2)
#			e3=cross(e1,e2)
#		M=c_[e1,e2,e3]
#		mr=r_[[[1,0,0],[0,cos(rot),-sin(rot)],[0,sin(rot),cos(rot)]]]
#		M=dot(mr,M)
#		M=matrix(M).T
#		for n,d in enumerate(dat):
#			dat[n,4:7]=dot(M,dat[n,[4,6,5]]*r_[1,-1,-1])
#			dat[n,7:10]=dot(M,dat[n,[7,9,8]])
#			dat[n,11:14]=dot(M,dat[n,[11,13,12]]*r_[1,-1,-1])
#			dat[n,14:17]=dot(M,dat[n,[14,16,15]])
#		if dat.shape[0]==1:
#			return D2(dat[0],self)
#		else:
#			return [D2(i,self) for i in dat]
	


#class D2:
#	def __init__(self,dati,parent=None,mult=-1):
#		self.parent=parent
#		keys='ide id cmb x1 N1 V21 V31 M11 M21 M31 x2 N2 V22 V32 M12 M22 M32'.split()
#		dat=dict(zip(keys,dati))
#		vars(self).update(dat)
#		self.mult=mult
#
#	@property
#	def v1(self):
#		return r_[self.N1,self.V21,self.V31]*self.mult
#	@property
#	def v2(self):
#		return r_[self.N2,self.V22,self.V32]
#
#		
#	@property
#	def m1(self):
#		return r_[self.M11,self.M21,self.M31]
#	@property
#	def m2(self):
#		return -r_[self.M12,self.M22,self.M32]
#
#	def v(self,p):
#		if p==1:return self.v1
#		else: return self.v2
#	def m(self,p):
#		if p==1: return self.m1
#		else: return self.m2


		


if __name__=='__main__':
#	p=parse_relaz('relazione__.txt')
#	mat=p.isola_mat()
#	print mat.getvalue()
#	sez=p.isola_sez()
#	print sez.getvalue()
#	nodi=p.isola_nodi()
#	print nodi.getvalue()
#	travi=p.isola_travi()
#	print travi.getvalue()
#	spost=p.isola_spostamenti()
#	n=len(spost.getvalue().split('\n'))
#	print n
#	solp=p.isola_sol_pilastri()
#	sp=solp.getvalue()
#
#	sp=sp.split('\n')
#	print len(sp)
#
#	solt=p.isola_sol_travi()
#	st=solt.getvalue()
#	st=st.split('\n')
#	print len(st)
#	print st[0]
#	print st[-1]

#	import os
	db=Loader('prova.sqlite','relazione__.txt')
	db.db.execute('drop table materiali')
	db.db.execute('drop table sezioni')
	db.db.execute('drop table nodi')
	db.db.execute('drop table d2')
	db.db.execute('drop table sollecitazioni_d2')
	db.db.execute('drop table spostamenti')
	db.db.commit()
	db.crea()
	db.load()
	db.scala_Kgcm_Nmm()
	print(db.ex_('select count(*) from materiali'))
	print(db.ex_('select count(*) from sezioni'))
	print(db.ex_('select count(*) from nodi'))
	print(db.ex_('select count(*) from d2'))
	print(db.ex_('select count(*) from sollecitazioni_d2'))
	print(db.ex_('select count(*) from sollecitazioni_d2 where elem=22'))
	print(db.ex_('select count(*) from sollecitazioni_d2 where elem=149'))
	print(db.ex_('select count(*) from spostamenti'))

