
from copy import copy

class Schema:
	'''classe base per la generazione degli schemi di armatura per travi e pilastri da utilizzare negli edifici esistenti'''
	st_tipo=''
	st_elemento=''
	def __init__(self,name,els,t1,t2=None,t3=None):
		'''els=lista elementi cui applicare armature
		t1,t2,t3= armature dei tratti '''
		vars(self).update(locals())
		if self.t2 is None:
			self.t2=copy(self.t1)
			self.t2[0]=0
		if self.t3 is None:
			self.t3=copy(self.t1)
	
	def __str__(self):
		name=self.name
		t1=self.st_tipo.format(*self.t1)
		t2=self.st_tipo.format(*self.t2)
		t3=self.st_tipo.format(*self.t3)
		els=[['{}'.format(j) for j in self.els[i*9:(i+1)*+9]] for i in range(1+len(self.els)//9)]
		els='\n'.join([' '.join(i) for i in els])
		st='''gruppo {name} "{0}"
{t1}
{t2}
{t3}
{els}'''.format(self.st_elemento,**locals())
		return st

	

class SchemaTrave(Schema):
	st_tipo='{0:.1f} {1} {0:.1f} {2} {0:.1f} {3} {4}'
	st_elemento='trave'

	def __init__(self,name,els,t1,t2=None,t3=None):
		'''els=lista elementi cui applicare armature
		t1,t2,t3= armature dei tratti (l asup ainf d_staffe p_staffe)  se T2 e t3 =None: t2 e T3 sono uguali a T1'''
		Schema.__init__(self,name,els,t1,t2,t3)

class SchemaPilastro(Schema):
	st_tipo='{0:.1f} {1} {2} {3} {4} {0:.1f} {5} {6}'
	st_elemento='pilastro'

	def __init__(self,name,els,t1,t2=None,t3=None):
		'''name = nome gruppo
		els=lista elementi
		t1 t2 t3=armautre:[lunghezza d_angolo, d_parete, ferri_lato_1, ferri_lato_2, d_staffe, passo staffe] se t3 e t3=None vengono presi =t1'''
		Schema.__init__(self,name,els,t1,t2,t3)

