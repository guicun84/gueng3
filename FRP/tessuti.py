#-*-coding:latin

from scipy import *
from gueng.CA import Calcestruzzo 

class Filo:
	def __init__(self,Tx,rho):
		'''Tx= [Tex = g/km] titolo del filo= peso di un km di filato
		rho= g/cm^3densitÓ del filato'''
		vars(self).update(locals())
		self.A=Tx/rho/1e3 #area

class Tessuto:
	def __init__(self,trama,Nfx,alphaff,alphafE,ordito=None,Nfy=None):
		'''trama filo usato per la trama
		Nfx=numero fili per trama
		alphafE,alphaff,coefficiente riduzione elasticitÓ\resistenza dovuto alla tessitura
		ordito: tipo di filo per ordito (defalut uguale a trama)
		tutte le stesse proprietÓ dell'ordito sono considerate uguali a quelle della trama a meno che non vengano specificate con [proprietÓ]y
		'''
		vars(self).update(locals())
		if ordito is None: self.ordito=trama #li imposto uguali
		if Nfy is None: self.Nfy=trama #li imposto uguali

		Artx=trama.Tx*nfx/10/trama.rho #area resistente
		Arty=trama.Tx*nfy/10/trama.rho
		teqx=Artx/1e3 #spessore equivalente
		teqy=Arty/1e3


