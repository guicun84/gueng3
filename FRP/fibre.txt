Tabella 2-1 cnr 200
Modulo di elasticitΰ normale E
Resistenza a trazione sr
Deformazione a rottura er
Coefficiente di dilatazione termica a
Densitΰ

tipo                                 , [GPa]     , [MPa]       , [%]        , [10-6 °C-1] , [g/cm3]
Fibre di vetro E                     , 70  80   , 2000  3500 , 3.5  4.5  , 5  5.4     , 2.5  2.6
Fibre di vetro S                     , 85  90   , 3500  4800 , 4.5  5.5  , 1.6  2.9   , 2.46  2.49
Fibre di carbonio (alto modulo)      , 390  760 , 2400 3400  , 0.5  0.8  , -1.45       , 1.85  1.9
Fibre di carbonio (alta resistenzaa) , 240  280 , 4100  5100 , 1.6  1.73 , -0.6  -0.9 , 1.75
Fibre arammidiche                    , 62  180  , 3600  3800 , 1.9  5.5  , -2          , 1.44  1.47
Matrice polimerica                   , 2.7  3.6 , 40  82     , 1.4  5.2  , 30  54     , 1.10  1.25
Acciaio da costruzione(snervamento)  , 206       , 250  400   , 20  30    , 10.4        , 7.8
Acciaio da costruzione(rottura)      , 206       , 350  600   , 20  30    , 10.4        , 7.8

