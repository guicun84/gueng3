import sqlite3,os,re

savedbflag=True
fi='codici_comuni.sqlite'
db=sqlite3.connect(':memory:')
db.execute('create table comuni(id text primary key,nome text,provincia text)')

codepath='[A-Z]\d{3}$'
prpath='[A-Z]{2}$'
compath='([A-Z \'\-\.]{2,})(.*?)\(([A-Z]{2})\)'

fis=open('codici_comuni.txt','r')
n=0
codici=[]
comuni=[]
prov=[]
for r in fis:
	if re.match(codepath,r):
		#print 'codice',r[:-1]
		codici.append(r[:-1])
	elif re.match(compath,r):
		reg=re.match(compath,r)
		com=reg.groups()[0][:-1]
		#print 'comune' ,com
		comuni.append(com)
		prov.append(reg.groups()[-1])

	n+=1

print(len(codici),len(comuni),len(prov))
db.executemany('insert into comuni values (? , ? ,?) ',list(zip(codici,comuni,prov)))
db.executescript('''create index comuni_nome on comuni(nome);
	create index comuni_id on comuni(id);''')




if savedbflag:
	if 'db2' in list(locals().keys()):
		db2.close()
	if os.path.exists(fi):
		os.remove(fi)
	db2=sqlite3.connect(fi)
	db2.executescript(''.join(db.iterdump()))
	db2.commit()

print(db.execute('select * from comuni where nome like "NE"').fetchall())
