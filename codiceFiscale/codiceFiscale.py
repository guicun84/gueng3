import datetime as dt
import sqlite3,os
from collections import OrderedDict as odict
dire=os.path.abspath(os.path.dirname(__file__))

class ComuneError(Exception): pass
class SessoError(Exception):pass


class CodiceFiscale:
	vocali='A E I O U'.split()
	codiceMese='A B C D E H L M P R S T'.split()
	db=sqlite3.connect(os.path.join(dire,'codici_comuni.sqlite'))
	caratteri='0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split()
	tabpari=[1,0,5,7,9,13,15,17,19,21,1,0,5,7,9,13,15,17,19,21,2,4,18,20,11,3,6,8,12,14,16,10,22,25,24,23]
	tabpari=odict(list(zip(caratteri,tabpari)))
	tabdispari=[0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
	tabdispari=odict(list(zip(caratteri,tabdispari)))
	codiceControllo='A B C D E F G H I J K L M N O P Q R S T U V W X Y Z'.split()
	del caratteri

	def __init__(self,nome,cognome,sesso,comuneNascita,dataNascita):
		try:
			self.data=dt.datetime.strptime(dataNascita,'%d/%m/%Y')
		except:
			self.data=dt.datetime.strptime(dataNascita,'%d/%m/%y')
		self.comune=comuneNascita.lower().capitalize()
		self.nome=nome.lower().capitalize()
		self.cognome=cognome.lower().capitalize()
		self.sesso=sesso

	def elaboraCognome(self):
		n=self.cognome.upper()
		n=n.replace(' ','')
		serie=[i for i in n if i not in self.vocali]
		serie.extend([i for i in n if i in self.vocali])
		if len(serie)<3:
			serie.extend(['X']*(3-len(serie)))
		return(serie[:3])

	def elaboraNome(self):
		n=self.nome.upper()
		n=n.replace(' ','')
		serie=[i for i in n if i not in self.vocali]
		if len(serie)>3:
			return serie[0],serie[2],serie[3]
		serie.extend([i for i in n if i in self.vocali])
		if len(serie)<3:
			serie.extend(['X']*(3-len(serie)))
		return(serie[:3])

	def elaboraData(self):
		return [('{}'.format(self.data.year))[-2:],
			self.codiceMese[self.data.month-1],
			'{:02d}'.format(self.data.day+{'m':0,'f':40}[self.sesso.lower()])]

	def elaboraComune(self):
		ide= self.db.execute('select id from comuni where nome=?',(self.comune.upper(),)).fetchone()
		if not ide: raise ComuneError('Comune {} non trovato'.format(self.comune.upper()))
		return [ide[0]]

	@property
	def CF(self):
		dati=[]
		for i in (self.elaboraCognome(),self.elaboraNome(),self.elaboraData(),self.elaboraComune()):
			dati.extend(i)
		cf=''.join(dati)
		cf+=self.elaboraControllo(cf)
		return cf

	def elaboraControllo(self,cfp):
		s=0
		for n,i in enumerate(cfp):
			if n%2:s+=self.tabdispari[i]
			else:s+=self.tabpari[i]
		r=s%26
		return self.codiceControllo[r]

	def __str__(self):
		return '''Nome: {}
Cognome: {} sesso: {}
nato a: {} il: {}
CF: {}'''.format(self.nome,
				self.cognome,
				self.sesso,
				self.comune,
				dt.datetime.strftime(self.data,'%d/%m/%Y'),
				self.CF)


class BackCF(CodiceFiscale):
	def __init__(self,cf):
		if len(cf)!=16:
			raise IOError(f'codice fiscale di lunghezza {len(cf)}!=16')
		self.cf=cf.upper()
	
	def data(self):
		aa=self.cf[6:8]
		mm=self.cf[8]
		mm=CodiceFiscale.codiceMese.index(mm)+1
		gg=self.cf[9:11]
		return f'{gg}/{mm:02d}/{aa}'
		

	def comune(self):
		code=self.cf[11:15]
		nome= CodiceFiscale.db.execute('select nome from comuni where id=?',(code,)).fetchone()[0].title()
		return nome

	def __str__(self):
		return f'''nato a {self.comune()}
il {self.data()}'''



