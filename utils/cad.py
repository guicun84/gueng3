
import re
from .varie import floatPath
from numpy import empty,append

cadpath=re.compile('X ?=\s*({fp}).*Y ?=\s*({fp}).*Z ?=\s*({fp})'.format(fp=floatPath))
def fromCadSt(st,isfile=False):
	'''estrae i dati dalle stringhe cad dei comandi id o lista riconoscendo le linee con x,y,z
	st=Stringa copiata da CAD'''
	if isfile:
		with open(st) as fi:
			st=fi.read()
	dat= cadpath.findall(st)
	r=empty(0,float)
	for i in dat:
		for j in i:
			r=append(r,float(j))
	r=r.reshape(-1,3)
	return r

from  tkinter import Tk
def toPolyLine(dat,close=False):
	st='plinea\n'+'\n'.join(['{},{}'.format(*i) for i in dat]) + {True:'\nch\n',False:'\n'}[close]+'\n'
	r=Tk()
	r.withdraw()
	r.clipboard_append(st)
	r.update()
	r.destroy()
	return st
	



