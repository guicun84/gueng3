#-*-coding:latin-*-

from scipy import cross,dot,vstack,array,real,imag,sign
from scipy.linalg import norm

def v2m(*args):
	'''calcola la matrice di orientamento dati due vettori casuali la matrice cos� calcolata trasforma da sistema globale a locale vec_loc=dot(M,vec_glob)
	e1 vettore parallelo COINCIDE con E1
	e2 vettore verso il quale PUNTA E2 nel piano E1-E2:
	e3 � perpendicolare ad entrambi
	args puo essere:
		[e1,e2] i due vettori gia divisi
		[e11,e12,e13,e21,e22,e23] le 6 componenti dei due vettori
	se si usa il metodo (e1,e2) i vettori // assi globali si p� abbreviare indicandoli con numeri immaginari ('-?\dj') dove \d= 1,2,3 per indicare x,y,z,il - se � verso negativo e j per indicarlo come numero immaginario.
	'''
	#controllo dati in input
	if len(args)==6: #6 componenti dei 2 vettori
		e1=array(args[:3],dtype=float)
		e2=array(args[3:],dtype=float)
	if len(args)==2: #2 dati in input (o i vettori o le loro forme contratte)
		e1,e2=args
	elif len(args)==4: #1 vettore completo e uno solo in forma contratta)
		if imag(args[0])!=0:
			e1=args[0]
			e2=args[1:]
		else:
			e1=args[:3]
			e2=args[-1]
	#esplico le forme contratte dei vettori
	choose={1:array([1,0,0.]),2:array([0,1,0.]),3:array([0,0,1.])}
	if not hasattr(e1,'__len__'):
		e1=choose[abs(imag(e1))]*sign(imag(e1))
	if not hasattr(e2,'__len__'):
		e2=choose[abs(imag(e2))]*sign(imag(e2))
	#se avessi delle liste le passo ad array
	if not type(e1)=='numpy.ndarray': e1=array(e1,dtype=float)
	if not type(e2)=='numpy.ndarray': e2=array(e2,dtype=float)
	#calcolo dei vettori
	e1/=norm(e1)
	e2/=norm(e2)
	e3=cross(e1,e2)
	e3/=norm(e3)
	e2=cross(e3,e1)
	e2/=norm(e2)
	return vstack([e1,e2,e3])


if __name__=='__main__':
	from scipy import r_,eye
	import unittest
	class MyTests(unittest.TestCase):
		def setUp(self):
			self.v1=r_[1,0,0.]
			self.v2=r_[0,1,0.]
			self.v3=r_[0,0,1.]
		def test_1(self):
			'passo 2 vettori'
			d=v2m(self.v1,self.v2)
			self.assertTrue(all(d==eye(3)))
		def test_2(self):
			'passo 2 vettori contratti'
			d=v2m(1j,2j)
			self.assertTrue(all(d==eye(3)))
		def test_3(self):
			'passo 1 vettore contratto e uno no'
			d=v2m(1j,(0,1,0))
			self.assertTrue(all(d==eye(3)))
		def test_4(self):
			'passo 1 vettore contratto e uno per componenti'
			d=v2m(1j,0,1,0)
			self.assertTrue(all(d==eye(3)))
		def test_5(self):
			'passo 1 vettore e uno contratto'
			d=v2m((1,0,0),2j)
			self.assertTrue(all(d==eye(3)))
		def test_6(self):
			'passo 1 vettore per componenti e uno contratto'
			d=v2m(1,0,0,2j)
			self.assertTrue(all(d==eye(3)))
		def test_7(self):
			'passo 2 vettore per componenti'
			d=v2m(1,0,0,0,1,0)
			self.assertTrue(all(d==eye(3)))
		def test_8(self):
			'passo 2 vettori con direzioni diverse'
			d=v2m(-2j,3j)
			m=matrix(r_[[[0,-1,0],[0,0,1],[-1,0,0]]])
			self.assertTrue(all(d==m))

	unittest.main(verbosity=2)
