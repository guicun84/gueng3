
import os,subprocess,tempfile
from scipy import ndimage,where
from matplotlib.colors import rgb_to_hsv
import re,PyPDF2 as pyPdf,time
class CercaPagColor:
	def __init__(self,file,soglias=.25,sogliav=.5,sogliapg=1e-3,imdpi=10,size=None,dire=None,fr=True):
		'''file= nome del file pdf da analizzare
		soglias=soglia per la saturazione del pixel
		sogliav= soglia per valore del pixel
		sogliapg= soglia per valore relativo colore/bn
		imdpi= dpi immagini ottenute dal pdf per analisi
		size= se impostata dimensione delle immagini
		dire= nome della directory dove salvare le immagini
		fr=[true|false] per output che consideri fronte-retro o no'''
		vars(self).update(locals())
		
	def crea_immagini(self):
		#self.dr='tmpimg'
		#os.mkdir(self.dr)
		ts=time.time()
		if not hasattr(self,'dr'):
			if self.dire is None:
				self.dr=tempfile.mkdtemp('CercaPagColor')
			else:
				self.dr=self.dire
				if not os.path.exists(self.dr):
					os.mkdir(self.dr)

			print((self.dr))
			st='convert -colorspace rgb'
			if self.imdpi:
				st+=' -density {}'.format(self.imdpi)
#			st+=' "{}[{{npag}}]"'.format(self.file)
			st+=' "{}"'.format(self.file)
			if self.size:
				st+=' -resize {sz[0]}x{sz[1]}'.format( sz=self.size)
			if self.imdpi and not self.size:
				st+=' -resample {}'.format( self.imdpi)
#			st+=' "{}"'.format(os.path.join(self.dr,'im{npag:06d}.png'))
			st+=' "{}"'.format(os.path.join(self.dr,'im%06d.png'))
			print(st)
			pdfdoc=pyPdf.PdfFileReader(open(self.file,'rb'))
			self.pages=pdfdoc.getNumPages()
			print(('totale pagine:{}'.format(self.pages)))

#			for npag in xrange(pages):
#				pr=subprocess.Popen(st.format(npag=npag),shell=True)
#				pr.communicate()
#				print('{} di {}'.format(npag,pages))
			pr=subprocess.Popen(st,shell=True)
			pr.communicate()
			self.filist=[os.path.join(self.dr,i) for i in os.listdir(self.dr)]
			self.imlist=[]
			for i in self.filist:
				self.imlist.append(ndimage.imread(i,mode='RGBA'))
		print(('tempo per creazione: {}'.format(time.time()-ts)))
		
	def elimina_immagini(self):
		if hasattr(self,'filist'):
			for i in self.filist:
				os.remove(i)
			del self.filist
		if hasattr(self,'dr'):
			os.removedirs(self.dr)
			del self.dr
	 
	def calcola_colore(self,im,soglias=None,sogliav=None,show=False):
		if soglias is None:soglias=self.soglias
		if sogliav is None:sogliav=self.sogliav
		if type(im)==int:
			im=self.imlist[im]
		imh=rgb_to_hsv(im[:,:,:3])
		if show:
			print((imh.shape))
			figure(1)
			clf()
			imshow(im)
			figure(2)
			clf()
			subplot(2,2,2)
			imshow(imh[:,:,1]*imh[:,:,2])
			subplot(2,2,3)
			imshow(imh[:,:,1])
			subplot(2,2,4)
			imshow(imh[:,:,2])
		s=where((imh[:,:,1]>=soglias)&(imh[:,:,2]>sogliav),1,0)
		return (s.sum()/s.size)
	
	def controlla_colorate(self,files=None,soglia=None):
		if soglia is None: soglia=self.sogliapg
		if files is None: files=self.imlist
		colori=[]
		bn=[]
		for n,i in enumerate(files):
			v=self.calcola_colore(i)
			if v>=soglia:	
				colori.append((n,i,v))
			else: bn.append((n,i,v))
		return colori,bn
	
	def __call__(self):
		if not hasattr(self,'imlist'):
			self.crea_immagini()
		self.co,self.bn=self.controlla_colorate()
		if self.dire is None:
			self.elimina_immagini()
		return self.co,self.bn
	
	def __str__(self):
		if not hasattr(self,'co'): self()
		c=[i[0] for i in self.co]
		n=[i[0] for i in self.bn]
		if self.fr:
			cc=[]
			for i in c:
				if not i%2:
					if i not in cc:cc.append(i)
					if i+1 not in cc:cc.append(i+1)
				else:
					if i-1 not in cc:cc.append(i-1)
					if i not in cc:cc.append(i)
		else:
			cc=c
		bb=[]
		for i in n:
			if i not in cc:
				bb.append(i)
		cc=[i+1 for i in cc]
		bb=[i+1 for i in bb]
		def form(lis):
			if lis:
				st=[]
				start=lis[0]
				prew=start
				for cur in lis[1:]:
					if cur-prew!=1:
						if start!=prew:
							st.append('{}-{}'.format(start,prew))
						else:
							st.append('{}'.format(start))
						start=cur
					prew=cur
				if start!=prew:
					st.append('{}-{}'.format(start,prew))
				else:
					st.append('{}'.format(start))
				st=','.join(st)
			else:
				st=''
			return st
		cc=form(cc)
		bb=form(bb)
				
		return 'colori:\n{}\nbianco-nero\n{}'.format(cc,bb)
