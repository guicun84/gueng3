
from scipy import *

class Rettangolo:
	'''classe per calcolare i parametri geometrici di sezioni rettangolari:
	la sezione viene comunque orienteata con asse forte y e debole z'''
	def __init__(self,b,h):
		'''b,h dimensioni della sezione'''
		vars(self).update(locals())
		d=[b,h]
		d.sort()
		self._b,self._a=d
		self._r=self._a/self._b
		self.A=self.b*h
		self.Avy=self.Avz=self.A/1.5
		self.Jy,self.Jz=1/12*r_[self._b,self._a]*r_[self._a,self._b]**3
		self.Wy,self.Wz=1/6*r_[self._b,self._a]*r_[self._a,self._b]**2
		d=r_[d]
		dd=d.copy()[-1::-1]
		self.Wply,self.Wplz=(d*dd/2*dd/4)*2
		self.iy=(self.Jy/self.A)**2
		self.iz=(self.Jz/self.A)**2


	@property
	def beta(self):
		'''calcolo coefficiente per caclolo momento torsionale
		Jt=beta*a*b**3  a>b'''
		if not hasattr(self,'_beta'):
			if self._r<100: self._beta=interp(self._r , (1 , 1.2 , 1.5 , 2 , 2.5 , 3 , 4 , 5 , 10,100) , (0.141 , .166 , .196 , .229 , .249 , .263 , .281 , .291 , .312 , .333))
			else: self._beta= .333
		return self._beta

#	@property
#	def alpha_old(self):
#		'''calcolo coefficiente per modulo di resistenza torsionale
#		tau=alpha*M/(a*b**2)  a>b'''
#		if not hasattr(self,'_alpha'):
#			if self._r<100: self._alpha=interp(self._r , (1 , 1.2 , 1.5 , 2 , 2.5 , 3 , 4 , 5 , 10,100) , (4.808 , 4.566 , 4.329 , 4.065 , 3.876 , 3.745 , 3.546 , 3.436 , 3.205 , 3))
#			else: self._alpha= 3.
#		return self._alpha
	
	@property
	def alpha(self):
		'''calcolo coefficiente per modulo di resistenza torsionale
		tau=alpha*M/(a*b**2)  a>b'''
		if not hasattr(self,'_alpha'):
			self._alpha=3+1.8*self._b/self._a
		return self._alpha


	@property
	def Jt(self):
		if not hasattr(self,'_Jt'):
			self._Jt=self._a*self._b**3*self.beta
		return self._Jt

	@property
	def Wt(self):
		if not hasattr(self,'_Wt'):
			self._Wt=self._a*self._b**2/self.alpha
		return self._Wt

	


	def __str__(self):
		return '''b={b:.4g} mm
h={h:.4g} mm
A={A:.4g} mm^2^
W~y~={Wy:.4g} mm^3^ ; W~z~={Wz:.4g} mm^3^
J~y~={Jy:.4g} mm^4^ ; J~z~={Jz:.4g} mm^4^
&alpha;={a:.4g} , &beta;={be:.4g}
W~t~={Wt:.4g} mm^3^ , J~y~={Jt:.4g} mm^4^ 
W~ply~={Wply:.4g} mm^3^ ; W~plz~={Wplz:.4g} mm^3^ '''.format(a=self.alpha,be=self.beta,Wt=self.Wt,Jt=self.Jt,**vars(self))


class RettangoloCavo():
    def __init__(self,b,h,s):
        vars(self).update(locals())
        self.r1=Rettangolo(b,h)
        self.r2=Rettangolo((b-2*s),(h-2*s))
        self.Wt=2*(b-s)*(h-s)*s
        self.Avy=2*h*s/1.5
        self.Avz=2*b*s/1.5
        
    def __getattr__(self,val):
        return getattr(self.r1,val)-getattr(self.r2,val)
    def __str__(self):
        st=f'sezione {self.b}x{self.h}x{self.s}'
        for i in 'A Wy Wz Wt Jy Jz iy iz Avy Avz Jt'.split():
            st+=f'\n{i} = {getattr(self,i)}'
        return st
    @property
    def dict(self):
        out={}
        out['name']=f'{self.b:.1f}x{self.h:.1f}x{self.s:.1f}'
        out['G']=0
        out.update( {i:getattr(self,i) for i in 'b h s A Wy Wz Jy Jz iy iz Avy Avz Wt'.split()})
        out['G']=out['A']*7850/1e6
        out['IT']=self.Jt
        return out
