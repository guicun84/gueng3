#!/usr/bin/env python
#-*- coding:latin-*-

import sys
import threading,time,itertools

class ProgressBar:
	def __init__(self,end,start=0.,lbar=45,ltot=80,refresh=.1):
		'''end: valore finale da raggiuntere: se l'oggetto passato è un iteratore considera la sua lunghezza
		   start: valore di partenza
		   lbar: numero di caratteri che compongono la barra 
		   ltot: numero caratteri totali della stringa
		   refresh= secondi per ogni refresh'''
		vars(self).update(locals())
		if hasattr(end,'__len__'):
			self.end=len(end)
		self.isrun=False
		self.spinner=itertools.cycle('- \ | /'.split())
		self.ts=None
		self.tf=None
		self.last=None
	
	def __call__(self,val):
		'''val : valore raggiunto: se tipo float non viene modificato, se tipo int aggiunge automaticamente 1 per arrivare alla fine usando n di enumerate su un iteratore'''
		if type(val)==int: self.val=val+1
		else: self.val=val
		if not self.isrun:
			self.isrun=True
			self.tr=threading.Thread(target=self.tr_fun)
			self.tr.start()
		if self.val>=self.end: self.tr.join()

	def format_time(self,s):
		if s>=60:
			m=s//60
			s=s%60
			if m>=60:
				h=m//60
				m=m%60
				tim='{h:.0f}:{m:.0f}:{s:.0f}'.format(**locals())
			else:
				tim='{m:.0f}:{s:.0f}'.format(**locals())
		else:
			tim='{s:.0f}'.format(**locals())
		return tim


	def write (self):
		self.p=min([1,self.val/self.end]) #percentuale relativa
		pp=self.p*100 #percentuale
		t=self.p*self.lbar #caratteri da utilizzare
		ti=int(t) #caretteri interi
		tm=int((t-ti)>=.5) #carattere mezzo
		s=self.stima()
		if s is not None:tim=self.format_time(s)
		else: tim=''

		st=('\r' + next(self.spinner) + '[' + '='*ti + '-'*tm + ' '*(self.lbar-ti-tm) +']{pp:.1f}%|{self.val:.3g}/{self.end:.3g}|{tim} ETA').format(**locals()).ljust(self.ltot)[:self.ltot]
		sys.stdout.write(st)
		sys.stdout.flush()
	
	def stima(self):
		if self.ts is None:
			self.ts=time.time()
			return
		if self.last!=self.val:
			self.last=self.val
			dt=time.time()-self.ts
			self.s=(1-self.p)*dt/self.p
		return self.s

	def tr_fun(self):
		while True:
			self.write()
			if self.val>=self.end:
				break
			time.sleep(self.refresh)
		self.write()
		self.tf=time.time()
		self.durata=self.tf-self.ts
		self.durata_=self.format_time(self.durata)
		print()

if __name__=='__main__':
#	from progress_bar_02 import  *
	x=range(10)
	bar=ProgressBar(x)
	ts=time.time()
	for n,i in enumerate(x):
		time.sleep(.1)
		bar(n)
	
	ts=time.time()
	t=3
	bar=ProgressBar(t)
	while True:
		dt=time.time()-ts
		bar(dt)
		if dt>=t:break

	if False:
		x=range(30000000)
		bar=ProgressBar(x)
		a=0
		for n,i in enumerate(x):
			a=a+i
			bar(n)
		
	if True:
		bar=ProgressBar(10)
		bar(0)
		time.sleep(2)
		bar(1)
		time.sleep(2)
		bar(5)
		time.sleep(2)
		bar(8)
		time.sleep(2)
		bar(9)
