﻿#-*- coding:utf-8-*-

from scipy import *
from time import strftime
from itertools import cycle
from collections import namedtuple
import inspect

def print_out(dic,ordine=None,col=1):
	'''scirve a monitor i risultati:
dic= dizionario con i risultati da scrivere
ordine=ordine con cui scriverli,se non presente li scrive tutti'''
	if not type(dic)==dict: #lo adatto per le named-tuple
		dic=dic._asdict()
	if ordine==None:
		ordine=list(dic.keys())
	elif type(ordine)==str:
		ordine=ordine.split(',')
	r=[]
	out=[]
	cy=cycle(list(range(col)))
	for i in ordine:
		if type(dic[i])==int:
			r.append('%s= % 10d'%(i,dic[i]))
		else:
			try:
				r.append('%s= % 10.4g'%(i,dic[i]))
			except:
				r.append('%s= % 10s'%(i,dic[i]))
		if next(cy)==col-1:
			out.append('\t'.join(r))
			r=[]
	out.append('\t'.join(r))
	print('\n'.join(out))

def print_ver(v,l=1.,s=1.):
    '''esegue il print di una verifica
v=valore da verificare
l=limite della verifica
s=1 se deve essere v>l
  -1 se deve essere v<l'''
    test=v>=l
    test={True:1,False:-1}[test]
    test*=s
    if test==1:
            print('\t\t\tVerificato')
    else:
            print('\t\t\tNON Verificato')

def to_dict(var,*args):
    '''genera un dizionario con le variabili passate in args
args segue la stessa metodologia di print_output'''
    v={}
    if len(args)==1:
        args=args[0].split(',')
    for i in args:
        v[i]=var[i]
    return v

def to_nt(var,*args):
	dic=to_dict(var,*args)
	if len(args)==1:
		args=args[0].split(',')
	nt=namedtuple('collections',args)(**dic)
	return nt

def M_rot(rx=0,ry=0,rz=0,conv=1):
	scale={'d':pi/180 , 'c':pi/200 , 'r':1.}
	if conv in list(scale.keys()):
		conv=scale[conv]
	rx*=conv
	ry*=conv
	rz*=conv
	mx=c_[r_[1,0,0],r_[0,cos(rx),sin(rx)],r_[0,-sin(rx),cos(rx)]].T
	my=c_[r_[cos(ry),0,sin(ry)],r_[0,1,0],r_[-sin(ry),0,cos(ry)]].T
	mz=c_[r_[cos(rz),sin(rz),0],r_[-sin(rz),cos(rz),0],r_[0,0,1]].T
	return dot(dot(mx,my),mz)

def tabify(val,ordine=None,righe=False,formati={int:' 5d',str:' 5s',str:' 5s','other':' 5.4g'}):
	val_=[]
	if type(val[0]) in (list,tuple):
		val=list2listdict(val,ordine,righe)
		for i in val:
			val_.append(i)
	elif type(val[0])!=dict and '_asdict' in dir(val[0]): #ho delle named_tuple
		for i in val:
			val_.append(i._asdict())
	else:
		for i in val:
			val_.append(i)
	#identifico i tipi di dato
	if ordine is None:
		ordine=list(val_[0].keys())
	elif type(ordine)==str:
		ordine=ordine.split(',')
	desc='\t'.join(list(val_[0].keys()))
	for i in val_:
		if desc!= '\t'.join(list(i.keys())): raise IOError('dizionari differenti')	
	desc='\t'.join(['% 5s'%i for i in ordine])
	types=[]
	for i in ordine:
		if type(val_[0][i]) in list(formati.keys()):
			types.append(formati[type(val_[0][i])])
		else:
			types.append(formati['other'])
	out=[]
	for i in val_:
		r=[]
		for t,j in zip(types,ordine):
			r.append(('%'+t)%i[j])
		r='\t'.join(r)
		out.append(r)
	out='\n'.join(out)
	out=desc+'\n'+out

	return out

def str_now():
	return strftime('%Y%m%d%H%M%S')

def qm_like(val):
	'resitituisce "(?,?,...,?)" tanti quanti gli elementi passati'
	return '(' + ','.join(['?' for i in val]) + ')'



def list2listdict(li,ordine,righe=False):
	if type(ordine)==str:
		ordine=ordine.split(',')
	out=[]
	if type(li)==dict:
		l=[]
		for i in ordine:
			l.append(li[i])
		li=l
	if righe:
		r=len(li)
		c=len(li[0])
		l=[]
		for co in range(c):
			ou=[]
			for ri in range(r):
				ou.append(li[ri][co])
			l.append(ou)
		li=l
	for n,i in enumerate(li[0]):
		out.append(dict(list(zip(ordine,[v[n] for v in li]))))
	return out

def get_out(fi,fi_out=None,loc=locals(),glob=globals()):
	from io import StringIO
	import sys
	__old__out__=sys.stdout
	__new__out__=StringIO()
	try:
#	if True:
		sys.stdout=__new__out__
		exec(compile(open(fi, "rb").read(), fi, 'exec'),glob,loc)
		sys.stdout=__old__out__
		if fi_out is None:
			return __new__out__.getvalue()
		else:
			fi_out=open(fi_out,'w')
			fi_out.write(__new__out__.getvalue())
			fi_out.close()
	except :
#	else:
		sys.stdout=__old__out__
		return __new__out__.getvalue()

class Verifiche:
	'''classe utile per controllare le verifiche e tenerne traccia'''
	full_out=True #se false in scrittura considera solo quelle non verificate
	def __init__(self,title=None):
		self.title=title
		self.val=[]
		self.lim=[]
		self.conf=[]
		self.ris=[]
		self.desc=[]
	
	def __call__(self,val,conf,lim,desc=None,verbose=False):
		'''esegue la verifica: 
		val= valore 
		conf= l,le,e,ge,g o equivalente numero identificativo per le verifiche (iniziali inglesi)
		lim= limite per la verifica
		desc= descrizione opzionale da salvare'''
		if not conf in 'l,le,e,ge,g'.split(','):
			if  type (conf)==int and conf<=4:
				conf='l,le,e,ge,g'.split(',')[conf]
			else:	
				return None
		try:val=val.flatten()[0]
		except:pass
		try:
			val=val[0]
		except: pass
		self.val.append(val)
		self.lim.append(lim)
		self.conf.append(conf)
		self.desc.append(desc)
		if conf=='ge':
			self.ris.append(val>=lim)
		elif conf=='ge':
			self.ris.append(val>lim)
		elif conf=='le':
			self.ris.append(val<=lim)
		elif conf=='l':
			self.ris.append(val<lim)
		elif conf=='e':
			self.ris.append(val==lim)
		if verbose:
			print(self[-1])
		return self.ris[-1]

	def all(self):
		return all(self.ris)
	
	def __getitem__(self,n):
		if n==-1:
			n=len(self.ris)-1
		t=dict(list(zip('g,ge,l,le,e'.split(','),'>,>=,<,<=,='.split(','))))
		tx=t[self.conf[n]]
		st='% 4d) %.4g %s %.4g : %s'%(n,self.val[n],tx,self.lim[n],{True:'Ok',False:'No'}[self.ris[n]])
		if self.desc[n]:
			st+=' , %s'%self.desc[n]
		return st
		
	def last(self):
		return self[-1]

	def __str__(self):
		if Verifiche.full_out:
			st=[self[i] for i in range(len(self.ris))]
			st.append('Tutte le verifiche soddisfatte: %s'%{True:'Ok',False:'No'}[self.all()])
			if not self.all():
				st.append('controllare ' + ' , '.join(['%s'%i for i in arange(len(self.ris))[-r_[self.ris]]]))
		else:
			st=[self[i] for i in range(len(self.ris)) if not self.ris[i]]
		if not st:
			st.append('\t OK: Tutte le verifiche superate')
		st='\n'.join(st)
		if self.title is not None:
			st='\n%s\n'%self.title + st
		return st

	def last_old(self):
		t=dict(list(zip('g,ge,l,le,e'.split(','),'>,>=,<,<=,='.split(','))))
		tx=t[self.conf[-1]]
		st='% 4d) %.4g %s %.4g : %s'%(len(self.val)-1,self.val[-1],tx,self.lim[-1],self.ris[-1])
		if self.desc[-1]:
			st+=' , %s'%self.desc[-1]
		return st

	def __str___old(self):
		st=[]
		t=dict(list(zip('g,ge,l,le,e'.split(','),'>,>=,<,<=,='.split(','))))
		for i in range(len(self.ris)):
			tx=t[self.conf[i]]
			s='% 4d) %.4g %s %.4g : %s'%(i,self.val[i],tx,self.lim[i],self.ris[i])
			if self.desc[i]:
				s+=' , %s'%self.desc[i]
			st.append(s)
		st.append('\nTutte le verifiche soddisfatte: %s'%self.all())
		if not self.all():
			st.append('controllare ' + ', '.join(['%s'%i for i in arange(len(self.ris))[-r_[self.ris]]]))
		return '\n'.join(st)
