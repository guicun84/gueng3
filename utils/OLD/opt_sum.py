from itertools import combinations
class OptSum:
	def __init__(self,vals,tg):
		self.vals=vals
		self.tg=tg
		self.stato=1
		self.combinazioni=None
		self.controlloIniziale()
		self.combina()

	def controlloIniziale(self):
		if min(self.vals)>=self.tg:
			self.stato=False
		if sum(self.vals)<=self.tg:
			self.soluzione=self.tg-sum(self.vals),list(range(len(self.vals)))
			self.stato=2

	def combina(self):
		'''ricerca la combinazione migliore per ridurre al minimo il resto'''
		if self.combinazioni is None and self.stato:
			if self.stato==2:return self.soluzione
			#cerco di utilizzare il metodo esaustivo
			self.combinazioni=[]
			for l in range(len(self.vals)):
				cs=combinations(list(range(len(self.vals))),l+1)
				for c in cs:
					self.combinazioni.append([self.tg-sum([self.vals[i] for i in c]),c])
			self.combinazioni=[i for i in self.combinazioni if i[0]>=0]
			self.combinazioni.sort()

	
	def ottimizzaResto(self):
		self.combina()
		mi=min([i[0] for i in self.combinazioni])
		cmb=[[len(i[1]),i] for i in self.combinazioni if i[0]==mi]
		cmb.sort()
		return cmb[-1][1]


		return self.combinazioni[0]
	
	def ottimizzaNumero(self):
		'''ricerca la combinazione migliore per massimizzare il numero di richieste'''
		self.combina()
		num=[len(i[1]) for i in self.combinazioni]
		num=max(num)
		cmb=[i for i in self.combinazioni if len(i[1])==num]
		return cmb[0]


if __name__=='__main__':
	import unittest
#	class myTest(unittest.TestCase):
#		def setUp(self):
#			vals=(1,2,3,9,8,7,5,4,6)
#			tg=9
#			self.opt=OptSum(val,tg)
#		def test_ottimizza_resto(self):
#			r=opt.ottimizzaResto()
#			print r
#			v=r[0]
#			self.assertEqual(v,0)
#		def test_ottimizza_numero(self):
#			r=opt.ottimizzaNumero()
#			print r
#			v=len(r[1])
#			self.assertEqual(v,3)
#
#	unittest.main(verbosity=2)

	vals=(1,2,3,9,8,7,5,4,6)
	tg=15
	opt=OptSum(vals,tg)
	print(opt.ottimizzaResto())
	print(opt.ottimizzaNumero())
