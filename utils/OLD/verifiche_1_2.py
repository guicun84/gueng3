#-*-coding:utf-8-*-
from scipy import *
from copy import deepcopy
class Verifiche:
	__version__=1.2
	'''classe utile per controllare le verifiche e tenerne traccia'''
	full_out=True #se false in scrittura considera solo quelle non verificate
	istanze=[]
	def __init__(self,title=None,append=False):
		self.title=title
		self.val=[] #valori ottenuti
		self.lim=[] #valore limite
		self.conf=[] #tipo di confronto
		self.ris=[] #risultato del confronto 
		self.desc=[] #descrizione della verifica
		self.append=append
		if append:
			Verifiche.istanze.append(self)
	
	def __call__(self,val,conf,lim,desc=None,verbose=False):
		'''esegue la verifica: 
		val= valore 
		conf= l,le,e,ge,g o equivalente numero identificativo per le verifiche (iniziali inglesi)
		lim= limite per la verifica
		desc= descrizione opzionale da salvare'''
		if not conf in 'l,le,e,ge,g'.split(','):
			if  type (conf)==int and conf<=4:
				conf='l,le,e,ge,g'.split(',')[conf]
			else:	
				return None
		try:val=val.flatten()[0]
		except:pass
		try:
			val=val[0]
		except: pass
		self.val.append(val)
		self.lim.append(lim)
		self.conf.append(conf)
		self.desc.append(desc)
		if conf=='ge':
			self.ris.append(val>=lim)
		elif conf=='ge':
			self.ris.append(val>lim)
		elif conf=='le':
			self.ris.append(val<=lim)
		elif conf=='l':
			self.ris.append(val<lim)
		elif conf=='e':
			self.ris.append(val==lim)
		if verbose:
			print(self[-1])
		return self.ris[-1]

	def all(self):
		return all(self.ris)

	def __bool__(self):
		return int(self.all())

	def __getitem__(self,n):
		if n==-1:
			n=len(self.ris)-1
		t=dict(list(zip('g,ge,l,le,e'.split(','),'>,>=,<,<=,='.split(','))))
		tx=t[self.conf[n]]
		st='%.4g %s %.4g : %s'%(self.val[n],tx,self.lim[n],{True:'Verificato',False:'NON verificato'}[self.ris[n]])
		if self.desc[n]:
			st='%s : %s'%(self.desc[n],st)
#			st+=' , %s'%self.desc[n]
		return st

	def __add__(self,other):
		if not isinstance(other,Verifiche):
			raise TypeError('somma consentita solo tra istanze di Verifiche')
		new=deepcopy(self)
		new.val.extend(other.val)
		new.lim.extend(other.lim)
		new.conf.extend(other.conf)
		new.ris.extend(other.ris)
#		if other.title!='':
#			new.desc.extend(['{}-{}'.format(other.title,j) for j in other.desc])
#		else:
		new.desc.extend(other.desc)
		if self.append:
			Verifiche.istanze.append(new_desc)
		return new

	
	@property
	def last(self):
		return self[-1]

	def delete(self,n):
		if not hasattr(n,'__len__'):
			n=[n]
		else:
			n=[i-j for j,i in enumerate(n)]
		for i in n:
			del self.val[i]
			del self.lim[i]
			del self.conf[i]
			del self.ris[i]
			del self.desc[i]

	def __str__old(self):
		'''se full_out==True o 1 scrive tutto
			se full_out==2: scrive solo non verificate
			se full_out=0 scrive solo come è andata'''
		if self.full_out:
			if self.full_out in [True,1]:
				st=['%d) '%i+self[i] for i in range(len(self.ris))]
				if len (self.ris)==1:
					st.append('Verifica soddisfatta: %s'%({True:'Ok',False:'No'}[self.all()]))
				else:
					st.append('Tutte le %d verifiche soddisfatte: %s'%(len(self.ris),{True:'Ok',False:'No'}[self.all()]))
				if not self.all():
					st.append('controllare \n ' + ' , '.join(['%s'%i for i in arange(len(self.ris))[-r_[self.ris]]]))
			else:
				st=['%d) '%i +self[i] for i in range(len(self.ris)) if not self.ris[i]]
			if not st:
				if len(self.ris)==1:
					st.append('\t OK: Verifica soddisfatta'%len(self.ris))
				else:
					st.append('\t OK: Tutte le %d verifiche superate'%len(self.ris))
			else:
				st.append('Verifiche condotte: %d, non superate %d'%(len(self.ris),len([i for i in self.ris if not i])))
		else:
			st=['%d) '%i +self[i] for i in range(len(self.ris)) if not self.ris[i]]
			st=['Verifiche condotte: %d, non superate %d'%(len(self.ris),len(st))]
		if self.full_out:
			st.append('Peggiore')
			st.append('%d) %s'%self.max())
			st.append('Migliore')
			st.append('%d) %s'%self.min())
		st='\n'.join(st)
		if self.title is not None:
			st='\n%s\n'%self.title + st
		return st

	def str_all(self):
		'restituisce tutte le verifiche condotte'
		st=['%d) '%i+self[i] for i in range(len(self.ris))]
		if len(self.ris)==1:
			st.append('Verifica soddisfata: %s'%({True:'Ok',False:'No'}[self.all()]))
		else:
			st.append('Tutte le %d verifiche soddisfatte: %s'%(len(self.ris),{True:'Ok',False:'No'}[self.all()]))
		if not self.all():
			st.append('controllare \n ' + ' , '.join(['%s'%i for i in arange(len(self.ris))[-r_[self.ris]]]))
		st.append('')
		st= '\n'.join(st)
		if self.title is not None:
			st='\n%s\n'%self.title + st
		return st

	def str_red(self):
		'''resitiuisce solo l'esito dele verifiche'''
		if self.title:
			st=[self.title]
		else:
			st=[]
		st.append('Verifiche condotte: %d, non superate %d'%(len(self.ris),len([i for i in self.ris if not i])))
		st.append('')
		return '\n'.join(st)
	
	def str_min_max(self):
		if self.title:
			st=[self.title]
		else:
			st=[]
		st.append('Verifiche condotte: %d, non superate %d'%(len(self.ris),len([i for i in self.ris if not i])))
		st.append('Peggiore')
		st.append('%d) %s'%self.max())
		st.append('Migliore')
		st.append('%d) %s'%self.min())
		st.append('')
		return '\n'.join(st)

	def __str__(self):
		if self.full_out in [1,True]:
			return self.str_all()
		elif self.full_out==2:
			return self.str_min_max()
		elif self.full_out==0:
			return self.str_red()

	def __len__(self):
		return len(self.ris)

	def notVerif(self):
		return [n for n,i in enumerate(self.ris) if not i]

	def max(self):
		mask=r_[self.conf]
		mask=logical_or(mask=='le',mask=='l')
		test=where(mask,r_[self.val]/r_[self.lim],r_[self.lim]/r_[self.val])
		n=where(test==test.max())[0][0]
		return n,self[n]
	
	def min(self):
		mask=r_[self.conf]
		mask=logical_or(mask=='le',mask=='l')
		test=where(mask,r_[self.val]/r_[self.lim],r_[self.lim]/r_[self.val])
		n=where(test==test.min())[0][0]
		return n,self[n]

	@staticmethod
	def check(istanze=None):
		'''restituiscei true se tutte le tabelle di verifiche istanziate sono vere'''
		if istanze==None:istanze=Verifiche.istanze
		ck=all(r_[istanze])
		if ck: return True
		else: return False
#	@staticmethod
#	def da_controllare():
#		k=[i for i in Verifiche.istanze if not(i)]
#		for i in k: 
#			print i
#			print
	@staticmethod
	def reset():
		'''resetta tutte le istanze di verifica'''
		Verifiche.istanze=[]

	@staticmethod
	def resoconto(out_type='err',istanze=None):
		'''out_type= tipo di output:
						err= rendiconto solo delle verifiche on superate
						all= rendiconto di tutte le verifiche
						ptp= rendiconto di tutte le verifiche sol max e min'''
		if istanze is None: istanze=Verifiche.istanze
		if isinstance(istanze,Verifiche): istanze=[istanze]
		out=[]
		n=int(sum([len(i) for i in istanze]))
		if out_type=='err': #rendiconto solo quelle non superate
			if Verifiche.check(istanze):
				if n>1:
					out.append( 'Tutte le {n:d} verifiche soddisfatte'.format(n=n))
				else:
					out.append( 'Verifica soddisfatta')
			else:
				n=int(sum([sum([1 for j in i.ris if not j]) for i in istanze]))
				out.append( '{n:d} VERIFIC{d[0]} NON SODDISFATT{d[1]}:\n'.format(n=n,d={True:['HE','E'],False:['A','A']}[n>1] ))
				k=[i for i in istanze if not(i)]
				for i in k:
					if i.title: out.append( i.title)
					for n,j in enumerate(i.ris):
						if not j:
							out.append( '{:d}) {:s}'.format(n,i[n]))
					out.append('')
			return '\n'.join(out)
		if out_type=='all':
			for i in istanze:
				out.append('{:s}'.format( i))
		if out_type=='ptp':
			for i in istanze:
				out.append(i.title)
				out.append('\tMAX: {}){:s}'.format(*i.max()))
				out.append('\tmin: {}){:s}'.format(*i.min()))
		if out_type=='min':
			for i in istanze:
				out.append(i.title)
				out.append('\tmin: {}){:s}'.format(*i.min()))
		if out_type=='max':
			for i in istanze:
				out.append(i.title)
				out.append('\tMAX: {}){:s}'.format(*i.max()))

		if Verifiche.check(istanze):
			if n>1:
				out.append( 'Tutte le {n:d} verifiche soddisfatte'.format(n=n))
			else:
				out.append( 'Verifica soddisfatta')
		else:
			if n>1:
				out.append('NON tutte le verifiche soddisfatte')
			else:
				out.append('Verifica NON soddisfatta')
		return '\n'.join(out)
				


if __name__=='__main__':
	ver=Verifiche('1')
	ver(1,1,1,'1')
	ver(1,1,1,'2')
	ver=Verifiche('2')
	ver(1,1,1,'1')
	ver(2,1,1,'2')
	ver=Verifiche('3')
	ver(1,1,1,'1')
	ver(1,1,1,'2')
	print(Verifiche.resoconto())
	print(Verifiche.resoconto('all'))
	Verifiche.reset()
	ver=Verifiche('1')
	ver(1,1,1,'1')
	ver(1,1,1,'2')
	ver=Verifiche('2')
	ver(1,1,1,'1')
	ver(1,1,1,'2')
	ver=Verifiche('3')
	ver(1,1,1,'1')
	ver(1,1,1,'2')
	print(Verifiche.resoconto())
	print(Verifiche.resoconto('all'))

