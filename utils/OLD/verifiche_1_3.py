#-*-coding:latin-*-
from scipy import *
from copy import deepcopy
class Verifiche:
	__version__=1.3
	'''classe utile per controllare le verifiche e tenerne traccia'''
	outType='all' #all tutte, max-min solo massimo o minimo, ptp massimo e minimo
	showError=True
	#outType pu� essere all, max min ptp err none
	istanze=[]
	def __init__(self,title=None,append=False):
		self.title=title
		self.val=[] #valori ottenuti
		self.lim=[] #valore limite
		self.conf=[] #tipo di confronto
		self.ris=[] #risultato del confronto 
		self.desc=[] #descrizione della verifica
		if append:
			Verifiche.istanze.append(self)
	
	def __call__(self,val,conf,lim,desc=None,verbose=False):
		'''esegue la verifica: 
		val= valore 
		conf= l,le,e,ge,g o equivalente numero identificativo per le verifiche (iniziali inglesi)
		lim= limite per la verifica
		desc= descrizione opzionale da salvare'''
		if not conf in 'l,le,e,ge,g'.split(','):
			if  type (conf)==int and conf<=4:
				conf='l,le,e,ge,g'.split(',')[conf]
			else:	
				return None
		try:val=val.flatten()[0]
		except:pass
		try:
			val=val[0]
		except: pass
		self.val.append(val)
		self.lim.append(lim)
		self.conf.append(conf)
		self.desc.append(desc)
		if conf=='ge':
			self.ris.append(val>=lim)
		elif conf=='g':
			self.ris.append(val>lim)
		elif conf=='le':
			self.ris.append(val<=lim)
		elif conf=='l':
			self.ris.append(val<lim)
		elif conf=='e':
			self.ris.append(val==lim)
		if verbose:
			print(self.strf(-1))
		return self.ris[-1]

	def all(self):
		return all(self.ris)

	def __bool__(self):
		return int(self.all())

	def __getitem__(self,n):
		return self.val[n], self.lim[n], self.conf[n], self.ris[n] ,self.desc[n]

	def __add__(self,ot):
		new=deepcopy(self)
		new.val.extend(ot.val)
		new.lim.extend(ot.lim)
		new.conf.extend(ot.conf)
		new.ris.extend(ot.ris)
		new.desc.extend(ot.desc)
		return new

	def __iadd__(self,ot):
		return self+ot
		

	def strf(self,vals):		
		try:
			vals=self[vals]
		except: pass
#		if type(vals)==int:vals=self[vals]
		val,lim,conf,ris,desc=vals
		conf=dict(list(zip('l le e ge g'.split(),'< <= = >= >'.split())))[conf]
		ris={True:'Verificato', False:'NON verificato'}[ris]
		return '{desc} : {val:.3g}{conf}{lim:.3g}\t{ris}'.format(**locals())

	
	@property
	def last(self):
		return self[-1]

	def delete(self,n):
		if not hasattr(n,'__len__'):
			n=[n]
		else:
			n=[i-j for j,i in enumerate(n)]
		for i in n:
			del self.val[i]
			del self.lim[i]
			del self.conf[i]
			del self.ris[i]
			del self.desc[i]

	def __str__(self):
		st=['']
		if self.title: st.append(self.title)
		er=self.notVerif()
		if self.outType=='all':
			for n,i in enumerate(self.ris):
				st.append('{}) {}'.format(n,self.strf(n)))
		elif self.outType=='max':
			tmp=self.max()
			st.append('MAX {}) {}'.format(tmp[0],self.strf(tmp[2])))
		elif self.outType=='min':
			tmp=self.min()
			st.append('min {}) {}'.format(tmp[0],self.strf(tmp[2])))
		elif self.outType=='ptp':
			tmp=self.max()
			st.append('MAX {}) {}'.format(tmp[0],self.strf(tmp[2])))
			tmp=self.min()
			st.append('min {}) {}'.format(tmp[0],self.strf(tmp[2])))
		elif self.outType=='err':
			for n in er:
				st.append('{}) {}'.format(n,self.strf(n)))
		elif self.outType=='none':
			pass
		#sintesi degli errori:
		st.append('Verifiche condotte:{} , critiche:{} {}'.format(len(self.ris),len(er),{True:'OK' , False:'NON Verificato'}[bool(self)]))
		if er and self.showError: st.append('Controllare ' + ', '.join(['{}'.format(i) for i in er]))
		return '\n'.join(st)

	@property
	def st(self):
		if not hasattr(self,'_st'):
			self._st= [self.strf(i) for i in range(len(self))]
		elif len(self._st) !=len(self):
			self._st= [self.strf(i) for i in range(len(self))]
		return self._st

	def __len__(self):
		return len(self.ris)

	def notVerif(self):
		return [n for n,i in enumerate(self.ris) if not i]

	def max(self):
		mask=r_[self.conf]
		mask=logical_or(mask=='le',mask=='l')
		test=where(mask,r_[self.val]/r_[self.lim],r_[self.lim]/r_[self.val])
		n=where(test==test.max())[0][0]
		return n,test[n],self[n]
	
	def min(self):
		mask=r_[self.conf]
		mask=logical_or(mask=='le',mask=='l')
		test=where(mask,r_[self.val]/r_[self.lim],r_[self.lim]/r_[self.val])
		n=where(test==test.min())[0][0]
		return n,test[n],self[n]

	@staticmethod
	def check():
		'''restituiscei true se tutte le tabelle di verifiche istanziate sono vere'''
		return bool(all(r_[Verifiche.istanze]))

	@staticmethod
	def reset():
		'''resetta tutte le istanze di verifica'''
		Verifiche.istanze=[]

	@staticmethod
	def resoconto(istanze=None,outType='max',showError=False):
		'''out_type= tipo di output:
						err= rendiconto solo delle verifiche on superate
						all= rendiconto di tutte le verifiche
						max solo massimo
						min solo minimo
						ptp massimo e minimo'''
						
		out=[]
		if istanze is None:istanze=Verifiche.istanze
		nv,ne=0,0
		for v in istanze:
			#modifico i parametri per __str__ con quelli generici
			prevOutType=v.outType
			prevShowError=v.showError
			v.outType=outType
			v.showError=showError
			out.append('{}'.format(v))
			nv+=len(v)
			ne+=len(v.notVerif())
			#risetto i precedenti parametri per __str__
			v.outType=prevOutType
			v.showError=prevShowError
		
		out.append('\nRESOCONTO VERIFICHE\nVerifiche totali:{} ,Critiche {} '.format(nv,ne) + {True:'OK',False:'NON Verificato'}[ne==0])
		return '\n'.join(out)
				


if __name__=='__main__':
	import unittest

	class test(unittest.TestCase):

		def setUp(self):

			Verifiche.reset()
			self.vrt=Verifiche('tutto ok',1)
			self.vrt(.4,0,1,'minore')
			self.vrt(1,1,1,'minore uguale')
			self.vrt(1,2,1,'uguale')
			self.vrt(1.5,3,1,'maggiore uguale')
			self.vrt(1.6,4,1,'maggiore')

			self.vrf1=Verifiche('falso 1',1)
			self.vrf1(5,0,1,'minore')
			self.vrf1(.5,1,1,'minore uguale')
			self.vrf1(1,2,1,'uguale')
			self.vrf1(1.5,3,1,'maggiore uguale')
			self.vrf1(1.5,4,1,'maggiore')

			self.vrf2=Verifiche('falso 2',1)
			self.vrf2(.5,0,1,'minore')
			self.vrf2(5,1,1,'minore uguale')
			self.vrf2(1,2,1,'uguale')
			self.vrf2(1.5,3,1,'maggiore uguale')
			self.vrf2(1.5,4,1,'maggiore')

			self.vrf3=Verifiche('falso 3',1)
			self.vrf3(.5,0,1,'minore')
			self.vrf3(.5,1,1,'minore uguale')
			self.vrf3(3,2,1,'uguale')
			self.vrf3(1.5,3,1,'maggiore uguale')
			self.vrf3(1.5,4,1,'maggiore')

			self.vrf4=Verifiche('falso 4',1)
			self.vrf4(.5,0,1,'minore')
			self.vrf4(.5,1,1,'minore uguale')
			self.vrf4(1,2,1,'uguale')
			self.vrf4(.5,3,1,'maggiore uguale')
			self.vrf4(1.5,4,1,'maggiore')

			self.vrf5=Verifiche('falso 5',1)
			self.vrf5(.5,0,1,'minore')
			self.vrf5(.5,1,1,'minore uguale')
			self.vrf5(1,2,1,'uguale')
			self.vrf5(1,3,1,'maggiore uguale')
			self.vrf5(1,4,1,'maggiore')

		def test_boleano(self):
			self.assertTrue(bool(self.vrt))
			self.assertFalse(bool(self.vrf1))
			self.assertFalse(bool(self.vrf2))
			self.assertFalse(bool(self.vrf3))
			self.assertFalse(bool(self.vrf4))
			self.assertFalse(bool(self.vrf5))

		def test_format_string(self):
			st=self.vrt.strf(0)
			stref='minore : 0.4<1\tVerificato'
			self.assertTrue(st==stref)
		
		def test_max(self):
			M=self.vrf1.max()
			self.assertEqual(M[0],0)

		def test_min(self):
			m=self.vrf1.min()
			self.assertEqual(m[0],1)

		def test_str(self):
			print()
			print('test_str'.center(60,'#'))
			print(self.vrt)
			print(self.vrf1)
			self.assertTrue(True)

		def test_resoconto(self):
			print()
			print('test_resoconto'.center(60,'#'))
			print(Verifiche.resoconto())
			self.assertTrue(True)

		def test_add(self):
			n=self.vrt+self.vrf1
			self.assertEqual(len(n),10)
			self.vrf2+=self.vrf3
			self.assertEqual(len(self.vrf2),10)
		

	unittest.main(verbosity=2)
