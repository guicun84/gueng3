import inspect
class Search:
	'''questo oggetto ripercorre i namespace all'indietro alla ricerca di una variabile
	uso:
	var_prova=10
	def fun():
		s=Search()
		print s['var_prova']
	fun()
	out= 10'''
	
	def __init__(self,n=0):
		self.n=n
		pass
	
	def __getitem__(self,val):
		self.curfr=inspect.currentframe().f_back
		return self.cerca(val)

	def cerca(self,val):
		try:
			return self.curfr.f_locals[val]
		except:
			if '__name__' in list(self.curfr.f_locals.keys()):
				if self.curfr.f_locals['__name__']=='__main__':
					raise KeyError('impossibile trovare variabile %s'%val)
			self.curfr=self.curfr.f_back
			return self.cerca(val)



