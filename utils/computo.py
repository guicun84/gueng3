#-*-coding:latin-#-

from copy import deepcopy as cp
from numpy import array,r_,prod,imag,real,atleast_1d
import re
from lxml import etree
#calcolo dei volumi per spese istruttoria:

class Computo:
	def __init__(self,name='',vals=None,unit=''):
		'''name nome da dare al computo
		vals=lista di valori di partenza
		unit=unit� di misura del risultato'''
		if vals is None:
			vals=[]
		elif not hasattr(vals[0][1],'__len__'):
			if type(vals)==tuple:
				vals=list(vals)
			vals=[[i[0],i[1:]] for i in vals]
		self.name=name
		self.vals=vals
		self.unit=unit
		if self.vals:
			self.sum()

	def __call__(self,nome,*args):
		'''aggiunge valori alla lista:
		nome=nome del valore da aggiungere
		args=valori da moltiplicare tra loro, se complessi usa il inverso (divide)'''
		self.vals.append([nome,list(args)])

	def __add__(self,val):
		'''esegue la somma tra due Computi estendendo il primo con i dati del secondo'''
		if not isinstance(val,Computo): raise TypeError
		if self.unit!=val.unit: raise TypeError('controllare le unit�')
		out=cp(self.vals)
		out.extend(val.vals)
		if self.name and val.name: name='{} + {}'.format(self.name,val.name)
		elif self.name:name=self.name
		else: name=val.name
		return Computo(name,out,self.unit)

	def __sub__(self,val):
		'''esegue la differenza tra due Computi estendendo il primo con i dati del secondo'''
		assert isinstance(val,Computo)
		val=cp(val)
		val*=-1
		return self+val
		
	def __imul__(self,val):
		'''moltiplica i valori presenti in vals[1]'''
		for n in range(len(self.vals)):
			self.vals[n][1][0]*=val
		return self

	def __mul__(self,val):
		'''aggiunge un coefficiente moltiplicativo esplicito a tutti i campi del computo'''
		out=Computo(self.name,cp(self.vals),self.unit)
		if hasattr(val,'__len__'):
			out.unit=val[1]
			val=val[0]
		for n in range(len(out.vals)):
			out.vals[n][1]=r_[out.vals[n][1],val].flatten()
		return out

	def __floordiv__(self,val):
		return self.__mul__(1./val)

	def __div__(self,val):
		return self.__mul__(1./val)

	def __idiv__(self,val):
		return self.__imul__(1./val)
	
	@property
	def tot(self):
		'''ritorna il valore totale del computo'''
		s=0
		for i in self.vals:
			k=cp(i[1])
			for n,j in enumerate(k):
				if imag(j): k[n]=1./imag(j)
				else: k[n]=real(k[n])
			s+=prod(k)
		self.totale= float(atleast_1d(real(s))[0])
		return self.totale

	@property
	def T(self):
		'''ritorna un computo con il totale del computo'''
		val=self.tot
		out=Computo(self.name,unit=self.unit)
		out(self.name,val)
		return out

	def sum(self):
		''' altro metodo per definire il valore della somma'''
		return self.tot

	@staticmethod
	def calcola(v):
		'''elabora i valori v che rappresentano i membri del prodotto
		restituisce i segni del calcolo e i valori per il prodotto con gli inversi se necessari'''
		temp=[[],[]]
		for i in v:
			if imag(i): 
				temp[0].append('/') 
				temp[1].append(1./imag(i))
			else:
				temp[0].append('*')
				temp[1].append(float(i))
		return temp
		

	def __str__(self):
		st=[]
		if self.name:
			st.append(self.name)
		u=self.unit
		ts=[]	
		for v in self.vals:
			temp=self.calcola(v[1])
			t=[' '.join(['{} {:.4g}'.format(i,abs(j)) for i,j in zip(temp[0],v[1])])[2:]]
			t.append(' = {:>8.2f} {} {}'.format(prod(temp[1]),u,v[0]))
			ts.append(t)
		l=max([len(i[0]) for i in ts])
		for i in ts:
			st.append(i[0].ljust(l,' ')+i[1])
		st.append('Totale'.rjust(l,' ')+' = {:>8.2f} {}'.format(self.tot,u))
		return '\n'.join(st)

	def _repr_html_(self):
		if self.unit:
			st='{} - unit� {} <br>'.format(self.name,self.unit)
		else:
			st='{}<br>'.format(self.name)
		k=max((len(i[1]) for i in self.vals))
		st+='<table><tr><th>descrizione</th>'
		for n in range(k):
			st+='<th>dim {}</th>'.format(n+1)
		st+='<th>Tot</th></tr>'
		for i in self.vals:
			st2=''
			for j in i[1]:
				if imag(j):
					j=1/imag(j)
				st2+='<td>{:5g}</td>'.format(j)
			st2+='<td>{:5g}</td>'.format(prod(self.calcola(i[1])[1]))

			st2+='</tr>'
			st2='<td></td>'*(k-len(i[1])) + st2
			st2='<tr><td>{}</td>'.format(i[0])+st2
			st+=st2
		st+='<tr><td><b>Totale</b></td>'
		st+='<td></td>'*k
		st+='<td><b>{:5g}</b></td>'.format(self.sum())
			
		st+='</table>'
		return(st)

	def to_html(self):
		return self._repr_html_()

	def __repr__(self): 
#<gueng.utils.computo.Computo instance at 0x05C11030>
		return '''<{} instance at 0x{:08x}>
{}'''.format(self.__class__,id(self),self)



	def index (self,*val):
		'''val=indici del valore da prendere
			 	stringhe da ricercare
				(val,regex[bool|int]) lista con stringa e flag per usare regex: true false oppure 2 per usare come patterne ".*{val}.*" '''
		if type(val[0])==int: return val
		out=[]
		for n,i in enumerate(val): 
			test=re.compile('.*{}.*'.format(i))
			mask=[bool(test.match(i)) for i in [j[0] for j in self.vals]]
			ind=[]
			if not sum(mask):raise KeyError('{} non trovato'.format(val))

			for i,j in enumerate(mask):
				if j:ind.append(i)
			out.extend(ind)
		out=[i for i in set(out)]	
		return out

		
	def __getitem__(self,val):
		'''val=indice del valore da prendere
			stringa da ricercare
			regex pat compilato 
			(val,regex[bool]) lista con stringa e flag per usare regex'''
		if type(val) in (int,str): val=[val]
		out=[cp(self.vals[j]) for j in self.index(*val)]
		return Computo(vals=out,unit=self.unit)

	def __setitem__(self,index,value):
		if type(index) not in (list,tuple): index=(index,) 
		ind=self.index(*index)
		if not ind: raise KeyError('{} non trovato'.format(index))
		if len(ind)>1: raise KeyError('{} ambiguo '.format(index))
		ind=ind[0]
		self.vals[ind]=[value[0],r_[value[1:]]]

	def tocsv(self,fi=None):
		datlen=max([len(i[1]) for i in self.vals])
		k=','*datlen
		st=['"{}","{}"{}Tot'.format(self.name,self.unit,k)]

		for i in self.vals:
			temp=self.calcola(i[1])
			s=','.join('{}'.format(abs(j)) for j in temp[1])
			s+=','+','*(datlen-len(i[1])) + '{}'.format(prod(temp[1]))
			st.append('"{}",{}'.format(i[0],s))
		st.append('"TOTALE",{}{}'.format(','*datlen,self.sum()))
		if fi is None:
			return '\n'.join(st)
		fi=open(fi,'w')
		fi.write('\n'.join(st))
		fi.close()

	@staticmethod
	def fromcsv(fi):
		if type(fi)==str:fi=open(fi)
		name,unit=re.match('"(.*?)","(.*?)"',fi.readline()).groups()
		vals=[]
		for i in fi.readlines():
			n,dat=re.match('"(.*?)",(.*)',i).groups()
			if n=='TOTALE': continue
			dat=r_[[float(i) for i in [j for j  in dat.split(',')[:-1] if j!='']]]
			vals.append([n,dat])
		return Computo(name=name,vals=vals,unit=unit)

	def simplify(self):
		key=set([i[0] for i in self.vals])
		v=[]
		for k in key:
			print(k)
			dat= self[[k]]
			if dat.sum()==0:continue
			v.extend(dat.vals)
		return Computo(self.name,v,self.unit)
	
	def copy(self):
		return cp(self)
		
	@staticmethod
	def loadXml(el):
		dati=dict()
		u=el.xpath('dati/unita')
		if len(u)==1: dati['unit']=u[0].text
		desc=el.xpath('descrizione')
		if len(desc)==1: dati['name']=desc[0].text
		carichi=el.xpath('dati/carico')
		vals=[]
		for c in carichi:
			desc=c.xpath('descrizione')
			v=[desc[0].text.strip()]
			for dat in c.xpath('dati/dato'):
				if any([i in  dat.text for i in ['i','j']]):
					v.append(complex(dat.text))
				else:
					v.append(float(dat.text))
			vals.append(v)
		dati['vals']=vals 
		return Computo(**dati)
	

	def toXml(self,nome='computo',ide=None):
		E=etree.Element
		root=E(nome)
		if ide is not None:
			root.attrib['id']=ide
		desc=E('descrizione')
		desc.text=self.name
		root.append(desc)
		carichi=E('dati')
		root.append(carichi)
		if self.unit is not None:
			u=E('unita')
			u.text=self.unit
			carichi.append(u)
		for i in self.vals:
			carico=E('carico')
			desc=E('descrizione')
			desc.text=i[0]
			carico.append(desc)
			dati=E('dati')
			carico.append(dati)
			for j in i[1]:
				dt=E('dato')
				dt.text='{}'.format(j)
				dati.append(dt)
			carichi.append(carico)
		return root
