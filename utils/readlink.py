import os
def readlink(path):
    st=open(path,'rb').read()
    for n,i in enumerate(st):
        if st[n:n+2]==b'\\\\': 
            break
    for m,i in enumerate(st[n:]):
        if st[n+m:n+m+2]==b'\x00\x18':
            break
    m+=n
    st=st[n:m].split(b'\x00')
    st=[i.decode('utf-8') for i in st]
    for n,i in enumerate(st):
        if':' in i:
            st[n]=i.replace(':',':\\\\')
    st=os.path.join(*st[-2:])
    return st
