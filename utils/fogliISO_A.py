
from scipy import sqrt ,r_ ,prod

class FoglioA:
	m=1/2**.25*1e3
	r=2**.5
	def __init__(self,num):
		self.num=num
		self.dim=r_[self.m*self.r,self.m]
		for i in range(num):
			k=self.dim[0]/2
			self.dim[0]=self.dim[1]
			self.dim[1]=k
		self.A=prod(seld.dim)

	def __repr__(self):
		return 'Foglio A{:d}, {:.1f}x{:.1f} mm = {:.4f} m^2'.format(self.num,self.dim[0],self.dim[1],self.A/1e6)
