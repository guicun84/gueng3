import inspect
from copy import copy
from collections import OrderedDict
class SearchVar:
	'''questo oggetto ripercorre i namespace all'indietro alla ricerca di una variabile
	uso:
	var_prova=10
	def fun():
		s=Search()
		print s['var_prova']
	fun()
	out= 10'''
	__version__=1.2
	
	def __init__(self,n=1):
		self.n=n
		pass
	
	def __getitem__(self,val):
		val=copy(val)
		if type(val)==str and not ' ' in val:
			self.__curfr=inspect.currentframe()
			for i in range(self.n):
				self.__curfr=self.__curfr.f_back
			out=self.__cerca(val)
			del self.__curfr
			return out
		elif type(val)==str:
			val=val.split()
		elif type(val)==tuple:
			val=list(val)
		self.__last=OrderedDict(list(zip(val,[None]*len(val))))
		self.__curfr=inspect.currentframe()
		for i in range(self.n):
			self.__curfr=self.__curfr.f_back
		self.__cerca_molti(val)
		out=self.__last
		del self.__last
		del self.__curfr
		return out


	def __cerca(self,val):
		try:
			return self.__curfr.f_locals[val]
		except:
			if '__name__' in list(self.__curfr.f_locals.keys()):
				if self.__curfr.f_locals['__name__']=='__main__':
					raise KeyError('impossibile trovare variabile %s'%val)
			self.__curfr=self.__curfr.f_back
			return self.__cerca(val)

	def __cerca_molti(self,val):
		trovati=[]
		for i in val:
			if i in list(self.__curfr.f_locals.keys()):
				self.__last[i]=self.__curfr.f_locals[i]
				trovati.append(i)
		for i in trovati: val.remove(i)
		if len(val)!=0:
			if '__name__' in list(self.__curfr.f_locals.keys()):
				if self.__curfr.f_locals['__name__']=='__main__':
					raise KeyError('impossibile trovare variabile %s'%val)
			self.__curfr=self.__curfr.f_back
			self.__cerca_molti(val)

if __name__=='__main__':
	import unittest
	class test(unittest.TestCase):
		def test_singolo(self):
			a=1
			s=SearchVar()
			self.assertEqual(s['a'],a)

		def test_lista(self):
			a,b,c=1,2,3
			s=SearchVar()
			r=s['a','b','c']
			self.assertEqual(len(r),3)
			self.assertEqual(r['a'],a)
			self.assertEqual(r['b'],b)
			self.assertEqual(r['c'],c)
		
		def test_stringa(self):
			a,b,c=1,2,3
			s=SearchVar()
			r=s['a b c']
			self.assertEqual(len(r),3)
			self.assertEqual(r['a'],a)
			self.assertEqual(r['b'],b)
			self.assertEqual(r['c'],c)


	unittest.main(verbosity=2)

