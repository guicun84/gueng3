
from pylab import *
from scipy import *

class Graph: 
	'''classe per visualizzare un grafico e poterne ottenere dei dati puntuali'''
	def __init__(self,finame):
		'finame= nome file da usare'
		self.finame=finame
		self.im=imread('alpha_T_stub.png')

	def inizializza(self,ruota=True):
		'''ottiene centro assi e scala sulle due dimensioni'''
		self.show()
		print('selezionare origine asssi,fine asse x, fine asse y')
		O,dx,dy=array(ginput(3))
		lx=float(input('lungezza asse x'))
		ly=float(input('lungezza asse y'))
		self.carica(O,dx,dy,lx,ly,ruota)
	
	def carica(self,O,dx,dy,lx,ly,ruota=True):
		vars(self).update(locals())
		if not ruota:
			self.R=r_[1,0,0,-1].reshape(2,2)
		else:
			e1=dx-self.O
			e1/=norm(e1)
			e2=dy-self.O
			e2/=norm(e2)
			self.R=c_[e1,e2]
		self.sx=lx/dot(self.R.T,(dx-self.O))[0]
		self.sy=ly/dot(self.R.T,(dy-self.O))[1]
		

	def show(self):
		'''plotta immagine del grafico'''
		clf()
		show(0)
		imshow(self.im)
		draw()
		self.xl=xlim()
		self.yl=ylim()

	def relim(self):
		xlim(self.xl)
		ylim(self.yl)

	def getPunti(self,soglia=.03):
		'''ritorna punti sul grafico'''
		pts=empty((0,2),float)
		xl=r_[xlim()]
		yl=r_[ylim()]
		dref=norm(r_[xl.ptp(),yl.ptp()])/100
		l1=plot(None,None,'-xr')[0]
		xlim(xl)
		ylim(yl)
		while True:
			pt=array((ginput())[0],float)
			if len(pts)<2:
				pts=vstack((pts,pt))
			else:
				if norm(pt-pts[-1])/dref<soglia:break
				else:
					pts=vstack((pts,pt))
			l1.set_xdata(r_[l1.get_xdata(),pt[0]])
			l1.set_ydata(r_[l1.get_ydata(),pt[1]])
			draw()
		pts=pts-self.O
		pts=array([dot(self.R.T,i) for i in pts]).reshape(pts.shape)
		return pts*r_[self.sx,self.sy]

	def toImage(self,pts):
		out= pts/r_[self.sx,self.sy]
		out=array([dot(self.R,i) for i in out]).reshape(out.shape)
		return out+self.O
