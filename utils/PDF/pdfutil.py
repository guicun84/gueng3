import PyPDF4 as pdf
import os,sys,yaml,platform,subprocess,tempfile


BASEDIR=os.path.dirname(os.path.abspath(__file__))
class PDFUtil:
	def __init__(self):
		pl=platform.system()
		if pl=='Linux':
			self.gsbase='gs'
		elif pl=='Windows':
			opt=yaml.load(open(os.path.join(BASEDIR,'config.yaml')))
			print(opt)
			for i in opt['gspath']:
				if os.path.exists(i):
					self.gsbase=i
					print(self.gsbase)
					break
			if not hasattr(self,'gsbase'):
				print('posizione di ghostscript non trovata: aggiungerla al file config.yaml e riavviare')

		

	@staticmethod
	def merge(files,out):
		'''files= lista di file da unire, oppure
			[file, pag inizio ,  pag fine] per definire le pagine
			oppure directory con i file
			oppure file contenente la lista dei file (e pagine se necessario)
		out=nome file salvataggio'''
		wr=pdf.PdfFileWriter()
		if type(files)==str:
			if os.path.isdir(files):
				files=os.listdir('.')
				files=[i for i in files if os.path.splitext(i)[1].lower()=='.pdf']
			elif os.path.isfile(files):
				files1=open(files,'r').read()
				files1=files1.replace('\r','')
				files1=files1.split('\n')
				files=[]
				for i in files1:
					if i=='':
						continue
					if ';' in i:
						files.append(i.split(';'))
					else:
						files.append(i)
		for fi in files:
			if fi=='':continue
			if type(fi)==list:
				re=pdf.PdfFileReader(fi[0])
				if fi[1]:
					fi[1]=int(fi[1])
				else:
					fi[1]=0
				if fi[2]:
					fi[2]=int(fi[2])
				else:
					fi[2]=re.getNumPages()
				ra=range(fi[1],fi[2])
			else:
				re=pdf.PdfFileReader(fi)
				ra=range(re.getNumPages())
			for n in ra:
				wr.addPage(re.getPage(n))
		with open(out,'wb') as fo:
			wr.write(fo)

	def pdf2pdfa(self,fiin,fiout=None):
		test=fiout is None
		if test:
			fiout=tempfile.mktemp()
		fiin=os.path.abspath(fiin)
		fiout=os.path.abspath(fiout)
		com=[ 
			self.gsbase,
			'-dPDFA',
			'-dBATCH',
			'-dNOPAUSE',
			'-dNOOUTERSAVE',
			'-sColorConversionStrategy=UseDeviceIndependentColor',
			'-sDEVICE=pdfwrite',
			f'-sOutputFile={fiout}',
			f'{fiin}',
			]
		c=com
		print(' '.join(c))
		subprocess.Popen(c)
		if test:
			os.remove(fiin)
			os.rename(fiout,fiin)
			os.remove(fiout)


