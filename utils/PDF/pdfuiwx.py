import os,re
from pdfutil import PDFUtil
import wx
from wx.lib.mixins.listctrl  import TextEditMixin

class Main(wx.Frame,PDFUtil):
	def __init__(self,parent=None):
		wx.Frame.__init__(self,parent,title='PDF-merge',size=(600,300))
		PDFUtil.__init__(self)
		buttons={}
		buttons['sfoglia']=wx.Button(self,label="Sfoglia files")
		self.Bind(wx.EVT_BUTTON,self.onSfogliaFiles, buttons['sfoglia'])
		buttons['sfogli dir']=wx.Button(self,label="Sfoglia directory")
		self.Bind(wx.EVT_BUTTON,self.onSfogliaDirectory,buttons['sfogli dir'])
		buttons['merge']= wx.Button(self,label='Merge')
		self.Bind(wx.EVT_BUTTON,self.onMerge,buttons['merge'])
		buttons['save']= wx.Button(self,label='Save')
		self.Bind(wx.EVT_BUTTON,self.onSave,buttons['save'])
		buttons['open']= wx.Button(self,label='Open')
		self.Bind(wx.EVT_BUTTON,self.onOpen,buttons['open'])
		btsize=55
		btup= wx.Button(self,label='up [J]',size=(btsize,-1))
		self.Bind(wx.EVT_BUTTON,self.moveup,btup)
		btdown= wx.Button(self,label='down [k]',size=(btsize,-1))
		self.Bind(wx.EVT_BUTTON,self.movedown,btdown)

		self.list=EditableList(self,style=wx.LC_REPORT|wx.LC_HRULES|wx.LC_VRULES,size=(800,300))
		self.list.AppendColumn('file',width=700)
		self.list.AppendColumn('start',width=50)
		self.list.AppendColumn('end',width=50)
#		self.Bind(wx.EVT_LIST_DELETE_ITEM,self.onDeleItem,self.list)
		self.Bind(wx.EVT_LIST_KEY_DOWN,self.onKeyDown,self.list)

		self.statusbar=self.CreateStatusBar(1)
	
		s1=wx.BoxSizer(wx.VERTICAL)
		s2=wx.BoxSizer(wx.HORIZONTAL)
		s2.Add(s1,0,wx.ALIGN_TOP|wx.EXPAND,border=1)
		s2.Add(self.list,1,wx.EXPAND)
		for i in buttons.values():
			s1.Add(i)
		sdo=wx.BoxSizer(wx.HORIZONTAL)
		sdo.Add(btup,1)
		sdo.Add(btdown,1)
		s1.Add(sdo,1)
#		s3=wx.BoxSizer(wx.VERTICAL)
#		s3.Add(s2,1,wx.EXPAND)
#		s3.Add(self.statusbar,0,wx.EXPAND)
		self.SetSizerAndFit(s2)

		path = os.path.abspath("./icons/icon.png")
		icon = wx.Icon(path, wx.BITMAP_TYPE_PNG)
		self.SetIcon(icon)
		self.Show()

	def onSfogliaFiles(self,evt):
		fis=wx.FileDialog(self,style=wx.FD_OPEN|wx.FD_MULTIPLE)
		if fis.ShowModal() != wx.ID_CANCEL:
			fis=fis.GetPaths()
			for f in fis:
				if f:
					self.list.Append((f,'',''))
		self.statusbar.SetStatusText('aggiunti file')
			


	def onSfogliaDirectory(self,evt):
		dire=wx.DirDialog(self,style=wx.FD_OPEN)
		if dire.ShowModal()!=wx.ID_CANCEL:
			dire=dire.GetPath()
			fis=os.listdir(dire)
			fis=[os.path.join(dire,i) for i in fis if os.path.splitext(i)[1].lower()=='.pdf']
			for f in fis:
				if f:
					self.list.Append((f,'',''))
		self.statusbar.SetStatusText('aggiunti file della directory')

	def onMerge(self,evt):
		self.statusbar.SetStatusText('inizio merge dei file')
		fiout=wx.FileDialog(self,style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
		if not fiout.ShowModal()==wx.ID_CANCEL:
			fiout=fiout.GetPath()
			fis=self.getList()
			print(fis)
			self.merge(fis,fiout)
		self.statusbar.SetStatusText('fine merge dei file')
	
	def getList(self):
		out=[]
		for r in range(self.list.GetItemCount()):
			rr=[]
			for c in range(self.list.GetColumnCount()):
				i=self.list.GetItem(r,c)
				rr.append(i.GetText())
			out.append(rr)
		return out

	def onSave(self,evt):
		fiout=wx.FileDialog(self,style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
		if not fiout.ShowModal()==wx.ID_CANCEL:
			fiout=fiout.GetPath()
			li=self.getList()
			li='\n'.join([';'.join(i) for i in li])
			with open(fiout,'w') as fi:
				fi.write(li)
		self.statusbar.SetStatusText('lista file salvata')
	
	def onOpen(self,evt):
		fis=wx.FileDialog(self,style=wx.FD_OPEN)
		if not fis.ShowModal()==wx.ID_CANCEL:
			fis=fis.GetPath()
			data=(open(fis,'r').read())
			data=data.replace('\r','')
			data=data.split('\n')
			for n,r in enumerate(data):
				if ';' in r:
					data[n]=r.split(';')
				else:
					data[n]=[r,'','']
				self.list.Append(data[n])
		self.statusbar.SetStatusText('lista file caricata')

	def onKeyDown(self,evt):
		key=evt.GetKeyCode()
		opt=dict()
		opt[wx.WXK_DELETE]=self.cancella
		opt[ord('J')]=self.moveup
		opt[ord('K')]=self.movedown
		if key in opt.keys():
			opt[key]()

	def cancella(self):
		item=self.list.GetFirstSelected()
		self.list.DeleteItem(item)

	def move(self,d):
		item=self.list.GetFirstSelected()
		if item>=0:
			newindex=item+d
			if newindex<0 : return 
			vals=[self.list.GetItem(item,i).GetText() for i in range(3)]
			self.list.DeleteItem(item)
			new=self.list.InsertStringItem(newindex,'')
			for n,i in enumerate(vals):
				it= self.list.SetStringItem(new,n,i)
		self.list.Select(new)

	def moveup(self,*args):
		self.move(-1)
	
	def movedown(self,*args):
		self.move(+1)



class DropFile(wx.FileDropTarget):
	def __init__(self,obj):
		wx.FileDropTarget.__init__(self)
		self.obj=obj
		obj.SetDropTarget(self)

	def OnDropFiles(self,x,y,finame):
		print(finame)
		return True

class EditableList(wx.ListCtrl,TextEditMixin):
	def __init__(self,*args,**kargs):
		wx.ListCtrl.__init__(self,*args,**kargs)
		TextEditMixin.__init__(self)



app=wx.App(False)
m=Main()
app.MainLoop()
