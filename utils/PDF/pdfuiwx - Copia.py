import os,re
from merge import merge
import wx

class Main(wx.Frame):
	def __init__(self,parent=None):
		wx.Frame.__init__(self,parent,title='PDF-merge',size=(600,300))
		buttons={}
		buttons['sfoglia']=wx.Button(self,label="Sfoglia files")
		self.Bind(wx.EVT_BUTTON,self.onSfogliaFiles, buttons['sfoglia'])
		buttons['sfogli dir']=wx.Button(self,label="Sfoglia directory")
		self.Bind(wx.EVT_BUTTON,self.onSfogliaDirectory,buttons['sfogli dir'])
		buttons['merge']= wx.Button(self,label='Merge')
		self.Bind(wx.EVT_BUTTON,self.onMerge,buttons['merge'])
		buttons['save']= wx.Button(self,label='Save')
		self.Bind(wx.EVT_BUTTON,self.onSave,buttons['save'])
		buttons['open']= wx.Button(self,label='Open')
		self.Bind(wx.EVT_BUTTON,self.onOpen,buttons['open'])
		self.text=wx.TextCtrl(self,style=wx.TE_MULTILINE,size=(600,300))
		self.dropfile=DropFile(self.text)
		s1=wx.BoxSizer(wx.VERTICAL)
		s2=wx.BoxSizer(wx.HORIZONTAL)
		s2.Add(s1,0,wx.ALIGN_TOP,border=1)
		s2.Add(self.text,1,wx.EXPAND)
		for i in buttons.values():
			s1.Add(i)
		self.SetSizerAndFit(s2)
		self.Show()

	def onSfogliaFiles(self,evt):
		fis=wx.FileDialog(self,style=wx.FD_OPEN|wx.FD_MULTIPLE)
		if fis.ShowModal() != wx.ID_CANCEL:
			fis=fis.GetPaths()
			print(fis)
			fis='\n'.join(fis)
			self.text.SetValue(fis)
			


	def onSfogliaDirectory(self,evt):
		dire=wx.DirDialog(self,style=wx.FD_OPEN)
		if dire.ShowModal()!=wx.ID_CANCEL:
			dire=dire.GetPath()
			fis=os.listdir(dire)
			fis=[os.path.join(dire,i) for i in fis if os.path.splitext(i)[1].lower()=='.pdf']
			print(fis)
			if fis:
				self.text.SetValues('\n'.join(fis))

	def onMerge(self,evt):
		fiout=wx.FileDialog(self,style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
		if not fiout.ShowModal()==wx.ID_CANCEL:
			fiout=fiout.GetPath()
			fis=self.text.GetValue().split('\n')
			merge(fis,fiout)

	def onSave(self,evt):
		fiout=wx.FileDialog(self,style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT)
		if not fiout.ShowModal()==wx.ID_CANCEL:
			fiout=fiout.GetPath()
			st=self.text.GetValue()
			with open(fiout,'w') as fi:
				fi.write(st)
	
	def onOpen(self,evt):
		fis=wx.FileDialog(self,style=wx.FD_OPEN)
		if not fis.ShowModal()==wx.ID_CANCEL:
			fis=fis.GetPath()
			self.text.SetValue(open(fis,'r').read())

class DropFile(wx.FileDropTarget):
	def __init__(self,obj):
		wx.FileDropTarget.__init__(self)
		self.obj=obj
		obj.SetDropTarget(self)

	def OnDropFiles(self,x,y,finame):
		print(finame)
		return True


app=wx.App(False)
m=Main()
app.MainLoop()
