import tkinter as tk
from  tkinter import ttk
from tkinter import filedialog
import os,re
from merge import merge

class Main(tk.Tk):
	def __init__(self):
		tk.Tk.__init__(self)
		tk.Button(text="Sfoglia files",command=self.onSfogliaFiles).grid(column=0,row=0,sticky='W')
		tk.Button(text="Sfoglia directory",command=self.onSfogliaDirectory).grid(column=0,row=1,sticky='W')
		self.text=tk.Text(width=120,height=30)
		self.text.grid(row=0,column=1,rows=5,sticky='WE',padx=10,pady=10)
		tk.Button(text='Merge',command=self.onMerge).grid(row=2,column=0,sticky='W')
		tk.Button(text='Save',command=self.onSave).grid(row=3,column=0,sticky='W')
		tk.Button(text='Open',command=self.onOpen).grid(row=4,column=0,sticky='W')

	def onSfogliaFiles(self):
		fis=filedialog.askopenfilenames()
		if fis:
			fis='\n'.join(fis)
			self.text.insert(tk.END,fis)


	def onSfogliaDirectory(self):
		dire=filedialog.askdirectory()
		fis=os.listdir(dire)
		fis=[os.path.join(dire,i) for i in fis if os.path.splitext(i)[1].lower()=='.pdf']
		print(fis)
		if fis:
			self.text.insert(tk.END,'\n'.join(fis))

	def onMerge(self):
		fiout=filedialog.asksaveasfilename()
		fis=self.text.get('1.0',tk.END).split('\n')
		merge(fis,fiout)

	def onSave(self):
		st=self.text.get('1.0',tk.END)
		print(st)
		fiout=filedialog.asksaveasfilename()
		with open(fiout,'w') as fi:
			fi.write(st)
	
	def onOpen(self):
		fis=filedialog.askopenfilename()
		if fis:
			self.text.insert(tk.END,open(fis,'r').read())

if __name__=='__main__':
	m=Main()
	m.mainloop()
