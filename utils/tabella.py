#-*- coding:utf-8-*-
import inspect
from numpy import array,ndarray,float64
import re
from gueng.utils.cercaVar import SearchVar
class Tabella:
	def __init__(self,values,title=None):
		self.ordine=values
		if ' ' in values:
			self.keys=values.split()
		else:
			self.keys=values.split(',')
		self.unit=['']*len(self.keys)
		for n,i in enumerate(self.keys):
			te=re.search('\[(.*)\]',i)
			if te:
				self.unit[n]=te.group(1)
				self.keys[n]=re.sub('\[.*\]','',i)
		self.title=title
		self.values=[]
		self.forms={str:'s',
			str:'s',
			int:'d',
			float:'4.3e',
			float64:'4.3e'}

	def __call__(self):
		fr=SearchVar()
		self.values.append(dict(list(zip(self.keys,[fr[i] for i in self.keys]))))
	def __str__(self,s=None,e=None):
		if self.title:
			st=[self.title.upper()]
		else:
			st=[]
		st.append('\t'.join(['% 6s'%i for i in self.keys])) #intestazione colonne ok
		if any([i!='' for i in self.unit]):
			st.append('\t'.join(['% 6s'%i for i in self.unit])) #unità di misura colonne ok
		#realizzo la stringa di output in base ai dati
		form=self.format(self.values[0])
		if s is None and e is None:
			index=range(len(self.values))
		elif type(s)==int:
			if e is None:
				e=s+1
			index=range(s,e)
		else:
			index=s
		for j in index:
			st.append(form%self.values[j])
		return '\n'.join(st)
	
	def format(self,val):#realizza la stringa di formattazione delle righe
		form=[]
		for i in self.keys:
			t=type(val[i])
			if t in [ndarray,array]:
				t=t.dtype
			try:
				form.append('%%(%s)%s'%(i,self.forms[t]))
			except:
				form.append('%%(%s)%s'%(i,self.forms[str]))
		return '\t'.join(form)

	def heading(self):
		st=['\n'.join(['%s\n%s'%i for i in zip(self.keys,self.unit)])]


	def get(self,s,e=None):
		if type(s)==int:
			if e==None:
				e=s+1
		return self.__str__(s,e)

	def toFile(self,fi):
		open(fi,'w').write('%s'%self)

	

