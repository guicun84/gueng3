from collections import OrderedDict as ODi
import re

def toAlias(**kargs):
	'''funzione per creare alias delle variabili da poter concatenare in python nelle query e passare i valori nel dizionario creato per la sostituzione in sqlite3
toAlias(d=[1,2,3],c=4,e={'a':4,'b':'casa'})
Out: 
{'c': OrderedDict([('c_1', 4)]),
 'd': OrderedDict([('d_0', 1), ('d_1', 2), ('d_2', 3)]),
 'e': OrderedDict([('e_a', 4), ('e_b', 'casa')])}
	'''
	out={}
	for name,val in list(kargs.items()):
		if isinstance(val,ODi) or  type(val) == dict:
			var=ODi()
			for na,va in list(val.items()):
				if re.match('[ ;\'"]',na):
					raise ValueError('{} contiene caratteri potenzialmente pericolosi [ ;\'"]'.format(na))
				var['{}_{}'.format(name,na)]=va
			out[name]=var
			continue
		elif type(val) is str or val is None:
			out[name]={'{}_1'.format(name):val}
			continue
		var=ODi()#contiene le associazioni alias-valore per eseguire le query
		if hasattr(val,'__len__'):
			for n,v in enumerate(val):
				var['{}_{}'.format(name,n)]=v
		else:
			var['{}_{}'.format(name,1)]=val
		out[name]=var
	return out

def toOneDict(di):
	'''compatta tutti i valori dei dizionari presenti in di in uno solo'''
	test=True
	if type(di) is dict or isinstance(di,ODi):
		tests=[type(i) == dict or isinstance(i,ODi) for i in list(di.values())]
		print(tests)
		if all(tests):
			out=ODi()
			for i in list(di.values()):
				out.update(i)
			return out
		else :
			print('secondo caso')
			test=False
	else:
		print('primo caso')
		test=False
	if not test: raise ValueError('di deve essere dizionario di dizionari')
