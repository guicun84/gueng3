#-*-coding:latin-*-

from scipy import *
import numpy as np
import re
from copy import deepcopy
#try:
#	import unum
#	unumimport=True
#except:
#	unumimport =False
#try:
#	import pint
#	pintimport=True
#except:
#	pintimport =False

np.set_printoptions( formatter={'float_kind':lambda x:'{:.4g}'.format(x)})

class Verifiche:
	__version__=1.5
	'''classe utile per controllare le verifiche e tenerne traccia'''
	outType='all' #all tutte, max-min solo massimo o minimo, ptp massimo e minimo
	showError=True
	#outType pu essere all, max min ptp err none
	istanze=[]
	def __init__(self,title=None,append=False,instOut=False,outType=None,red=False,stformat='md'):
		'''title : titolo delle verifiche
		   append :  aggiunge set di verifiche alla lista delle istanze di verifica
		   instOut :  scrive immediatamente l'output della verifica appena chiamato metodo call senza specificarlo ogni volta
		   outType :  metodo di output da usare quando si scrive tutta il set di verifica [max min ptp all err]
		   red=False|True se True quando viene passato un array di valori estrae il peggiore
		   format='termcolor' per colori a terminale
		   		'md' per grassetto in markdown
		   '''
		self.title=title
		self.red=red
		self.val=[] #valori ottenuti
		self.lim=[] #valore limite
		self.conf=[] #tipo di confronto
		self.ris=[] #risultato del confronto 
		self.desc=[] #descrizione della verifica
		self.cu=[] #coefficiente d'uso
		self.rel=[] #valori per calcolo relativo
		self.index=[] #indice di massima verifica
		if append:
			Verifiche.istanze.append(self)
		self.instOut=instOut
		if outType is not None:
			self.outType=outType
		self.stformat=stformat
	
	def __call__(self,val,conf,lim,desc=None,verbose=None,rel=1):
		'''esegue la verifica: 
		val= valore 
		conf= l,le,e,ge,g o equivalente numero identificativo per le verifiche (iniziali inglesi)
		lim= limite per la verifica
		desc= descrizione opzionale da salvare
		verbose: se vero scrive la verifica appena chiamato il metodo call, altrimenti la scrive solo se self.instOut  impostato a True
		rel=valore di riferimento per output con valore relativo'''
		if not conf in 'l,le,e,ge,g'.split(','):
			if  type (conf)==int and conf<=4:
				conf='l,le,e,ge,g'.split(',')[conf]
			else:
				return None
#		try:val=val.flatten()[0]
#		except:pass
#		try:
#			val=val[0]
#		except: pass
#		if pintimport:
#			if isinstance(lim,pint.quantity._Quantity):
#				lim=lim.to_compact()
#				pintunit=lim.u
#				try:
#					val=val.to(lim.u) 
#				except:
#					val=[i.to(lim.u) for i in val]
		try:
			val=atleast_1d(val).astype(float)
		except:
			val=atleast_1d(val)
		try:
			lim=atleast_1d(lim).astype(float)
		except:
			lim=atleast_1d(lim)
		self.val.append(val)
		self.lim.append(atleast_1d(lim)[0])
		self.conf.append(conf)
		self.desc.append(desc)
		if conf=='ge':
			self.ris.append(all(val>=lim))
			self.cu.append((lim/val))
		elif conf=='g':
			self.ris.append(all(val>lim))
			self.cu.append((lim/val))
		elif conf=='le':
			self.ris.append(all(val<=lim))
			self.cu.append((val/lim))
		elif conf=='l':
			self.ris.append(all(val<lim))
			self.cu.append((val/lim))
		elif conf=='e':
			self.ris.append(all(val==lim))
			self.cu.append((val/lim))

#		if pintimport:
#			if isinstance(rel,pint.quantity._Quantity):
#				rel=rel.to(pintunit).m
#			if isinstance(self.cu[-1][0],pint.quantity._Quantity):
#				self.cu[-1]=r_[[i.to_base_unit() for i in self.cu[-1]]]
		self.rel.append(rel)
#		if self.red:
#			i=where(self.cu[-1]==self.cu[-1].max())[0][0]
#			self.cu[-1]=self.cu[-1][i]
#			self.val[-1]=atleast_1d(self.val[-1][i])
#		else:
#			self.cu[-1]=max(self.cu[-1])
		self.index.append(where(self.cu[-1]==self.cu[-1].max())[0][0])
#		if isinstance(self.cu[-1][0],unum.Unum):
#			self.cu[-1]=r_[[i.asNumber() for i in self.cu[-1]]]

		if verbose or  verbose is None and self.instOut:
			print(self.strf(-1))
		return self.ris[-1]

	def all(self):
		return all(self.ris)

	def __bool__(self):
		if self.all(): return True
		return False

	def __getitem__(self,n):
		return self.val[n], self.lim[n], self.conf[n], self.ris[n] ,self.desc[n],self.rel[n],self.index[n],self.cu[n]

	def __add__(self,ot,inplace=False):
		'''estende le verifiche con un altro oggetto verifica'''
		if not inplace: new=deepcopy(self)
		else: new=self
		new.val.extend(ot.val)
		new.lim.extend(ot.lim)
		new.conf.extend(ot.conf)
		new.ris.extend(ot.ris)
		new.desc.extend(ot.desc)
		new.cu.extend(ot.cu)
		new.rel.extend(ot.rel)
		new.index.extend(ot.index)
		return new

	def __iadd__(self,ot):
		return self.__add__(ot,True)		

	def stformatcode(self,ris):
		if self.stformat=='termcolor':
			colorcode={True:'\33[1;32m' , False:'\33[1;31m'}[ris]
			ris={True:'\33[42mVerificato\33[0m', False:'\33[1;41mNON verificato\33[0m'}[ris]
			closecode='\33[0m'
		elif self.stformat=='md':
			colorcode=''
			ris={True:'**Verificato**', False:'**NON** *verificato*'}[ris]
			closecode=''
		return colorcode,ris,closecode

		

	def strf(self,vals):		
		'''genera la stringa di output per una singola verifica'''
		try:
			vals=self[vals]
		except: pass
#		if type(vals)==int:vals=self[vals]
		val,lim,conf,ris,desc,rel,ind,cu_=vals
		cu=cu_[ind]
		if self.red and hasattr(val,'__len__'):
			if len(val)>1:val=val[ind]
		conf=dict(list(zip('l le e ge g'.split(),'< <= = >= >'.split())))[conf]
		colorcode,ris,closecode=self.stformatcode(ris)
#		return '{desc} : {val:.3g}{conf}{lim:.3g} (cu:{cu:.3g})\t{ris}'.format(**locals())
		if hasattr(val,'__len__'): #and not isinstance(val,unum.Unum):
			if len(val)>1 : #or isinstance(val[0],unum.Unum) :
				out='{desc} : {val}{conf}{lim} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
				out=re.sub('(\d)\s+(\d)',r'\1 , \2',out)
			else:
				out= '{desc} : {val[0]:.3g}{conf}{lim:.3g} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
		else:
#			if isinstance(val,unum.Unum):
#				out='{desc} : {val}{conf}{lim} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
#			else:
				out='{desc} : {val:.3g}{conf}{lim:.3g} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
#		out=out.replace('[','')
#		out=out.replace(']','')
		return out

	def strfrel(self,vals):		
		'''genera la stringa di output per una singola verifica con valori relativi'''
		try:
			vals=self[vals]
		except: pass
#		if type(vals)==int:vals=self[vals]
		val,lim,conf,ris,desc,rel,ind,cu_=deepcopy(vals)
		cu=cu_[ind]
		if self.red:val=val[ind]
		val/=rel
		lim/=rel
		conf=dict(list(zip('l le e ge g'.split(),'< <= = >= >'.split())))[conf]
		colorcode,ris,closecode=self.stformatcode(ris)
#		return '{desc} : {val:.3g}{conf}{lim:.3g} (cu:{cu:.3g})\t{ris}'.format(**locals())
		if hasattr(val,'__len__') :#and not isinstance(val,unum.Unum):
			if len(val)>1 :#or isinstance(val[0],unum.Unum):
				out='{desc} (relativo): {val}{conf}{lim} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
				out=re.sub('(\d)\s+(\d)',r'\1 , \2',out)
			else:
				out= '{desc} (relativo): {val[0]:.3g}{conf}{lim[0]:.3g} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
		else:
#			if isintance(val,unum.Unum):
#				out='{desc} (relativo): {val}{conf}{lim} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
#			else:
				out='{desc} (relativo): {val:.3g}{conf}{lim:.3g} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
#		out=out.replace('[','')
#		out=out.replace(']','')
		return out

	def strfreli(self,vals):		
		'''genera la stringa di output per una singola verifica con valori relativi'''
		try:
			vals=self[vals]
		except: pass
#		if type(vals)==int:vals=self[vals]
		val,lim,conf,ris,desc,rel,ind,cu_=deepcopy(vals)
		cu=cu_[ind]
		if self.red:val=val[ind]
		val=1/val*rel
		lim=1/lim*rel
		conf=dict(list(zip('l le e ge g'.split(),'< <= = >= >'.split())))[conf]
		colorcode,ris,closecode=self.stformatcode(ris)
#		return '{desc} : {val:.3g}{conf}{lim:.3g} (cu:{cu:.3g})\t{ris}'.format(**locals())
		if hasattr(val,'__len__') :# and not isinstance(val,unum.Unum):
			if len(val)>1:# or isinstance(val[0],unum.Unum):
				out='{desc} (relativo): 1/({val}){conf}1/({lim}) {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
				out=re.sub('(\d)\s+(\d)',r'\1 , \2',out)
			else:
				out= '{desc} (relativo): 1/{val[0]:.3g}{conf}1/{lim:.3g} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
		else:
#			if isinstance (val,unum.Unum):
#				out='{desc} (relativo): 1/({val}){conf}1/({lim}) {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
#			else:
				out='{desc} (relativo): 1/{val:.3g}{conf}1/{lim:.3g} {colorcode}(cu:{cu:.4g}){closecode}\t{ris}'.format(**locals())
#		out=out.replace('[','')
#		out=out.replace(']','')
		return out

	@staticmethod
	def uncolorize(st):
		'''rimuove i caratteri ansi per i colori dalla stringa'''
		return re.sub('\x1b[^m]+?m','',st)

	@property
	def last(self):
		'''ritorna l'ultima verifica'''
		return self[-1]

	def delete(self,n):
		'''cancella una verifica '''
		if not hasattr(n,'__len__'):
			n=[n]
		else:
			n=[i-j for j,i in enumerate(n)]
		for i in n:
			del self.val[i]
			del self.lim[i]
			del self.conf[i]
			del self.ris[i]
			del self.desc[i]

	def __str__(self):
		st=['']
		if self.title: st.append(self.title)
		er=self.notVerif()
		if self.outType=='all':
			for n,i in enumerate(self.ris):
				st.append('{}) {}'.format(n,self.strf(n)))
		elif self.outType=='max':
			tmp=self.max()
			st.append('MAX {}) {}'.format(tmp[0],self.strf(tmp[2])))
		elif self.outType=='min':
			tmp=self.min()
			st.append('min {}) {}'.format(tmp[0],self.strf(tmp[2])))
		elif self.outType=='ptp':
			tmp=self.max()
			st.append('MAX {}) {}'.format(tmp[0],self.strf(tmp[2])))
			tmp=self.min()
			st.append('min {}) {}'.format(tmp[0],self.strf(tmp[2])))
		elif self.outType=='err':
			for n in er:
				st.append('{}) {}'.format(n,self.strf(n)))
		elif self.outType=='none':
			pass
		#sintesi degli errori:
		if self.stformat=='md':
			colorcode={True:('' , '' , ''),False:('**','**','**')}[bool(self)]
		else:
			colorcode={True: ('\33[1;32m' , '\33[42m' , '\33[0m') , False:('\33[31;1m' , '\33[1;41m','\33[0m')}[bool(self)]
		ck='{colorcode[0]}Verifiche condotte:{} , critiche:{}{colorcode[2]} {colorcode[1]}{}{colorcode[2]}\n'.format(len(self.ris),len(er),{True:'OK' , False:'NON Verificato'}[bool(self)],colorcode=colorcode)
		st.append(ck)
		if er and self.showError: st.append('{colorcode[0]}Controllare '.format(colorcode=colorcode) + ', '.join(['{}'.format(i) for i in er]) + '\33[0m')
		return '  \n'.join(st)

	@property
	def st(self):
		if not hasattr(self,'__st'):#genero le stringhe di output
			self.__st= [self.strf(i) for i in range(len(self))]
		elif len(self.__st) !=len(self): #controllo che non siano state aggiunte altre
			self.__st= [self.strf(i) for i in range(len(self))]
		return self.__st
	@property
	def strel(self):
		if not hasattr(self,'__strel'):#genero le stringhe di output
			self.__strel= [self.strfrel(i) for i in range(len(self))]
		elif len(self.__strel) !=len(self): #controllo che non siano state aggiunte altre
			self.__strel= [self.strfrel(i) for i in range(len(self))]
		return self.__strel
	@property
	def streli(self):
		if not hasattr(self,'__streli'):#genero le stringhe di output
			self.__streli= [self.strfreli(i) for i in range(len(self))]
		elif len(self.__streli) !=len(self): #controllo che non siano state aggiunte altre
			self.__streli= [self.strfreli(i) for i in range(len(self))]
		return self.__streli

	def __len__(self):
		return len(self.ris)

	def notVerif(self):
		'''ritorna solo le verifiche non superate'''
		return [n for n,i in enumerate(self.ris) if not i]

	def max(self):
#		mask=r_[self.conf]
#		mask=logical_or(mask=='le',mask=='l')
#		test=where(mask,r_[self.val]/r_[self.lim],r_[self.lim]/r_[self.val])
		if len(self.cu)==0:
			return 0,0,None
		temp=[]
		for i in self.cu:
			if hasattr(i,'__len__'):
				temp.append(max(i))
			else:
				temp.append(i)
		n=where(array(temp)==max(temp))[0][0]
		return n,self.cu[n],self[n]

	@property
	def cumax(self):
		cu=self.max()[1]
		if hasattr(cu,'__len__'):cu=cu.max()
		return cu
	
	def min(self):
#		mask=r_[self.conf]
#		mask=logical_or(mask=='le',mask=='l')
#		test=where(mask,r_[self.val]/r_[self.lim],r_[self.lim]/r_[self.val])
		if len(self.cu)==0:
			return 0,0,None
		n=where(array(self.cu)==min(self.cu))[0][0]
		return n,self.cu[n],self[n]

	@staticmethod
	def check():
		'''restituiscei true se tutte le tabelle di verifiche istanziate sono vere'''
		return bool(all(r_[Verifiche.istanze]))

	@staticmethod
	def reset():
		'''resetta tutte le istanze di verifica'''
		Verifiche.istanze=[]

	@staticmethod
	def resoconto(istanze=None,outType='max',showError=False):
		'''out_type= tipo di output:
						err= rendiconto solo delle verifiche on superate
						all= rendiconto di tutte le verifiche
						max solo massimo
						min solo minimo
						ptp massimo e minimo
						err2 rendiconto non verificati pi sintetico
						max2 rendiconto massimi non verificati'''
						
		out=['#'*80,'RESOCONTO'.center(80),'',]
		if istanze is None:istanze=Verifiche.istanze
		if isinstance(istanze, Verifiche):istanze=[istanze]
		nv,ne=0,0
		testerr2=outType=='err2'
		if testerr2:outtype='err'
		testmax2=outType=='max2'
		if testmax2: outType='max'
		for v in istanze:
			#modifico i parametri per __str__ con quelli generici
			prevOutType=v.outType
			prevShowError=v.showError
			v.outType=outType
			v.showError=showError
			if not any([testerr2,testmax2]):
				out.append('{}'.format(v))
			else:
				if not bool(v):
					out.append('{}'.format(v))
			nv+=len(v)
			ne+=len(v.notVerif())
			#risetto i precedenti parametri per __str__
			v.outType=prevOutType
			v.showError=prevShowError

		test=ne==0
		
		out.append('\n{}RESOCONTO VERIFICHE\nVerifiche totali:{} ,Critiche {} \33[0m'.format({True:'\33[32;1m',False:'\33[31;1m'}[test] ,nv,ne,{True:'OK',False:'NON Verificato'}[test]))
		return '\n'.join(out)
				


if __name__=='__main__':
	import unittest

	class Mytest(unittest.TestCase):

		def setUp(self):
			Verifiche.reset()
			self.vrt=Verifiche('tutto ok',1)
			self.vrt(.4,0,1,'minore')
			self.vrt(1,1,1,'minore uguale')
			self.vrt(1,2,1,'uguale')
			self.vrt(1.5,3,1,'maggiore uguale')
			self.vrt(1.6,4,1,'maggiore')

			self.vrf1=Verifiche('falso 1',1)
			self.vrf1(5,0,1,'minore')
			self.vrf1(.5,1,1,'minore uguale')
			self.vrf1(1,2,1,'uguale')
			self.vrf1(1.5,3,1,'maggiore uguale')
			self.vrf1(1.5,4,1,'maggiore')

			self.vrf2=Verifiche('falso 2',1)
			self.vrf2(.5,0,1,'minore')
			self.vrf2(5,1,1,'minore uguale')
			self.vrf2(1,2,1,'uguale')
			self.vrf2(1.5,3,1,'maggiore uguale')
			self.vrf2(1.5,4,1,'maggiore')

			self.vrf3=Verifiche('falso 3',1)
			self.vrf3(.5,0,1,'minore')
			self.vrf3(.5,1,1,'minore uguale')
			self.vrf3(3,2,1,'uguale')
			self.vrf3(1.5,3,1,'maggiore uguale')
			self.vrf3(1.5,4,1,'maggiore')

			self.vrf4=Verifiche('falso 4',1)
			self.vrf4(.5,0,1,'minore')
			self.vrf4(.5,1,1,'minore uguale')
			self.vrf4(1,2,1,'uguale')
			self.vrf4(.5,3,1,'maggiore uguale')
			self.vrf4(1.5,4,1,'maggiore')

			self.vrf5=Verifiche('falso 5',1)
			self.vrf5(.5,0,1,'minore')
			self.vrf5(.5,1,1,'minore uguale')
			self.vrf5(1,2,1,'uguale')
			self.vrf5(1,3,1,'maggiore uguale')
			self.vrf5(1,4,1,'maggiore')

		def test_boleano(self):
			self.assertTrue(bool(self.vrt))
			self.assertFalse(bool(self.vrf1))
			self.assertFalse(bool(self.vrf2))
			self.assertFalse(bool(self.vrf3))
			self.assertFalse(bool(self.vrf4))
			self.assertFalse(bool(self.vrf5))

#		def test_format_string(self):
#			st=self.vrt.strf(0)
#			stref='minore : 0.4<1\tVerificato'
#			self.assertTrue(True) #da ricontrollare
		
		def test_max(self):
			M=self.vrf1.max()
			self.assertEqual(M[0],0)

		def test_min(self):
			m=self.vrf1.min()
			self.assertEqual(m[0],1)

		def test_str(self):
			print()
			print('test_str'.center(60,'#'))
			print(self.vrt)
			print(self.vrf1)
			self.assertTrue(True)

		def test_resoconto(self):
			print()
			print('test_resoconto'.center(60,'#'))
			print(Verifiche.resoconto())
			self.assertTrue(True)

		def test_add(self):
			n=self.vrt+self.vrf1
			self.assertEqual(len(n),10)
			self.vrf2+=self.vrf3
			self.assertEqual(len(self.vrf2),10)
		

	unittest.main(verbosity=2)
