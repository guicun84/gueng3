#-*-coding:utf-8-*-
from hashlib import sha256
import os
from tempfile import TemporaryFile
import re
from pandas import read_csv

class FileSha:
	def __init__(self,filist=None,dire='.',out=None,rec=True):
		'''filist: lista di file o nome file (anche espressione regolare)
		dire: directory dove cercare
		rec=True ricorsivo nelle sottocartelle'''
		# genero il nuovo file su cui scrivre
		self.dire=dire
		#elaboro filist:
		if type(filist)==str: 
			#se filist * una stringa la considero come espressione regolare del nome dei file
			if not rec:
				self.filist=[os.path.join(self.dire,i) for i in os.listdir(dire) if re.match(filist,i)]
			else:
				self.filist=[]
				for d,drd,fs in os.walk(self.dire):
					self.filist.extend([os.path.join(d,i) for i in fs if re.match(filist,i)])
			self.filist=[i for i in self.filist if os.path.isfile(i)]
		else:
			#altrimenti è gia una lista di nomi file da trattare
			self.filist=filist
		self.outFi=out

	def digest(self,verbose=False):
		'''elaboro tutti i fle in filist
		verbose=True|False stampa a monitor i risultati'''
		if self.outFi is None:
			self.fiOut=TemporaryFile(prefix='sha256_',suffix='.txt',delete=False,dir=self.dire)
		else:
			if os.path.exists(self.outFi):
				raise ValueError('il file "{}" esiste già'.format(self.outFi)) 
			self.fiOut=open(self.outFi,'w')
		for i in self.filist:
			with open(os.path.join(self.dire,i),'rb') as fi:
				sha=sha256()
				while True:
					st=fi.read(2**16) #leggo un mega
					if st=='':break
					sha.update(st)
				self.fiOut.write('{}\t{}\n'.format(sha.hexdigest(),i))
				del sha
		self.fiOut.close()
		if verbose:
			print(open(self.fiOut.name,'r').read())

	def check(self,shafile,filist=None):
		'''shafile =file di origine degli sha da controllare con quelli attuali o istanza FileSha
		filist'''
		if isinstance(shafile,FileSha):
			shafile=shafile.fiOut.name
		dt=read_csv(shafile,sep='\t',names='sha file'.split())
		if filist==None:
			filist=self.filist
		if not filist==None:
			dt=dt[[i in filist for i in dt.file]]

		def fun(fi):
			with open(fi,'rb') as f:
				sh=sha256()
				while True:
					st=f.read(2**16)
					if st=='':break
					sh.update(st)
			return sh.hexdigest()

		dt['sha_check']=dt.apply( lambda x:fun(x.file) , 1)
		dt['test']=dt.sha==dt.sha_check
		nfi=dt.shape[0]
		neq=dt.test.astype(int).sum()
		ndif=nfi-neq
		print('''file controllati {nfi}
file uguali= {neq}
file diversi= {ndif}'''.format(**locals()))
		if ndif:
			print('FILE CAMBIATI:')
			for i in dt[[not i for i in dt.test]].file:
				print(i)



	def __str__(self):
		if not hasattr(self,'fiOut'):
			self.digest()
		dt=read_csv(self.fiOut.name,sep='\t',names='sha file'.split())
		gr=dt.groupby('sha')
		assert len(self.filist)==dt.shape[0]
		nfi=len(self.filist)
		ngr=len(gr)
		nrep=nfi-ngr
		return '''file processati:{nfi}
file differenti={ngr}
file uguali={nrep}'''.format(**locals())

