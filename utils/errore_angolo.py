
from scipy import cos,sin,r_,deg2rad,rad2deg,vstack,sqrt
from scipy.optimize import root

class ErroreAngolo:
	'''questa classe serve per calcolare gli errori di approssimazione dovuti agli angoli'''
	def __init__(self,alpha,l,er=.5/sqrt(2),deg='true'):
		'''alpha=angolo in esame
		l=lungezza su cui calcolare errori
		er=massimo errore di posizionamento tollerabile
		deg=True se angoli in gradi'''
		vars(self).update(locals())
		if deg:
			self.__alpha=deg2rad(alpha)
		else:
			self.__alpha=alpha
		self.__co=None

	@property
	def co(self):
		'''coordinate del punto'''
		if self.__co is None:
			self.__co=r_[cos(self.__alpha),sin(self.__alpha)]*self.l
		return self.__co

	
	def coord_err(self,er):
		'''calcola le coordinate del punto supponento un errore di angolo pari ad er
		er=errore ipotizzato'''
		if self.deg: er=deg2rad(er)
		a=self.__alpha+er
		c1=r_[cos(a),sin(a)]*self.l
		a=self.__alpha-er
		c2=r_[cos(a),sin(a)]*self.l
		return r_[c1,c2]

	def error(self,er):
		'''calcolo della distanza tra punto teorico e punto con errore
		er=errore'''
		return r_[self.co,self.co]-self.coord_err(er)

	def deltaMax(self,er=None):
		'''calcolo angolo massimo '''
		if er ==None: er=self.er
		self.x=root(lambda x: max(abs(self.error(x)))-er,.1)
		if not self.x.success:
			print(self.x)
		return self.x.x

	def plt(self,er=None):
		from pylab import clf,show,draw,plot,legend,grid,axhspan,figure,xlabel,ylabel
		from scipy import linspace
		if er==None:
			if hasattr(self,'x'):
				er=linspace(0,5*self.x.x,100)
			else:
				er=linspace(0,1,100)
		ers=vstack(list(map(self.error,er)))
		figure(1)
		clf()
		show(0)
		plot(er,ers)
		legend('x+ y+ x- y-'.split())
		grid(1)
		axhspan(-self.er,self.er,color='g',alpha=.2)
		xlabel('errore angolo')
		ylabel('errore posizionamento')
		draw()

		
