#!/usr/bin/env python
#-*- coding:latin-*-

import sys
import threading,time,itertools
from numpy import *

class ProgressBar:
	def __init__(self,end_val,start_val=0,lbar=38,ltot=80,refresh=.1,autostart=True):
		'''end_val: valore finale da raggiuntere: se l'oggetto passato è un iteratore considera la sua lunghezza
		   start: valore di partenza
		   lbar: numero di caratteri che compongono la barra 
		   ltot: numero caratteri totali della stringa
		   refresh= secondi per ogni refresh'''
		vars(self).update(locals())
		if hasattr(end_val,'__len__'):
			self.end_val=len(end_val)
		self.val=start_val
		self.p=0
		self.isRun=False
		self.spinner=itertools.cycle('- \ | /'.split())
		self.stimatore=Stimatore()
		self.forceStop=False
		if self.autostart:self.start()

	def start(self,tr=True):
		if self.isRun: return
		self.stimatore.inizializza()
		if tr:
			self.tr=threading.Thread(target=self.tr_fun)
			self.tr.start()
			self.isRun=True

	def stop(self):
		self.forceStop=True
	
	def end(self):
		self.val=self.end_val
	
	def __call__(self,val=None):
		'''val : valore raggiunto: se tipo float non viene modificato, se tipo int aggiunge automaticamente 1 per arrivare alla fine usando n di enumerate su un iteratore'''
		if val is None:
			self.val+=1
		elif type(val)==int: self.val=val+1
		else: self.val=val
		self.p=min([1,(self.val-self.start_val)/(self.end_val-self.start_val)]) #percentuale relativa
		if self.val>=self.end_val: self.tr.join()

	def format_time(self,s):
		if s>=60:
			m=s//60
			s=s%60
			if m>=60:
				h=m//60
				m=m%60
				tim='{h:.0f}:{m:.0f}:{s:.0f}'.format(**locals())
			else:
				tim='{m:.0f}:{s:.0f}'.format(**locals())
		else:
			tim='{s:.0f}'.format(**locals())
		return tim


	def write (self):
		self.stimatore.aggiorna(self.p)
		p_=self.stimatore.stima_perc()
		if p_ is None: p_=self.p
		p_=min([p_,1])
		pp=p_*100 #percentuale
		t=p_*self.lbar #caratteri da utilizzare
		ti=int(t) #caretteri interi
		tm=int((t-ti)>=.5) #carattere mezzo
		s=self.stimatore.stima_tempo()
		if s is not None:tim=self.format_time(s)
		else: tim='--'
		dur=self.format_time(self.stimatore.durata(time.time()))
		st=('\r' + next(self.spinner) + '[' + '='*ti + '-'*tm + ' '*(self.lbar-ti-tm) +']{pp:.1f}%|{self.val:.3g}/{self.end_val:.3g}|{dur} ETA {tim}').format(**locals())
		if self.ltot:st=st.ljust(self.ltot)[:self.ltot]
		sys.stdout.write(st)
		sys.stdout.flush()
	

	def tr_fun(self):
		'''funzione eseguita in thread separato'''
		while True:
			self.write()
			if self.val>=self.end_val or self.forceStop:
				break
			time.sleep(self.refresh)
		if self.val>self.end_val: self.val=self.end_val
		self.isRun=False
		self.p=1
		self.write()
		print()

	def durata_(self):
		return self.stimatore.durata()
	
	def durata(self):
		return slef.format_time(self.durata())


class Stimatore:
	def __init__(self,n=100):
		self.start=None
		self.times=None
		self.perc=None
		self.v=None
		self.n=n
	
	def inizializza(self):
		self.start=time.time()
		self.times=r_[self.start]
		self.perc=r_[0]
	
	def aggiorna(self,p):
		if p==self.perc[-1]:return
		self.times=r_[self.times,time.time()][-self.n:]
		self.perc=r_[self.perc,p][-self.n:]
		if self.times[-1]==self.times[-2]:
			self.times=self.times[:-1]
			self.perc=self.perc[:-1]
		else:
#			self.v=(diff(self.perc)/diff(self.times)).mean()
			self.v=self.perc[-1]/(self.times[-1]-self.start)

		
	def stima_tempo(self):
		if self.v is None:return
		return (1-self.perc[-1])/self.v - (time.time()-self.times[-1])

	def stima_perc(self):
		if self.v is None:return
		return self.perc[-1] + (time.time()-self.times[-1])*self.v

	def durata(self,t=None):
		if t is None: t=self.times[-1]
		return t-self.start
		
############################################################6
if __name__=='__main__':
#	from progress_bar_02 import  *
	x=range(10)
	bar=ProgressBar(x)
	ts=time.time()
	bar.start()
	for n,i in enumerate(x):
		time.sleep(.1)
		bar(n)
#------------------------------	
	ts=time.time()
	t=3
	bar=ProgressBar(t)
	while True:
		dt=time.time()-ts
		bar(dt)
		if dt>=t:break

#------------------------------	
	if True:
		x=range(3000000)
		bar=ProgressBar(x)
		a=0
		bar.start()
		for n,i in enumerate(x):
			a=a+i
			bar()
		
#------------------------------	
	if True:
		n=10
		bar=ProgressBar(n)
		bar.start()
		for i in range(n):
			time.sleep(1)
			bar()
