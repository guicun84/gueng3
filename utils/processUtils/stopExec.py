import argparse
from sys import exit

class StopExec:
	def __init__(self,parent=None):
		self.num=0
		if not hasattr(parent,'__len__'):
			if parent is None: parent=[]
			else: parent=[parent]

		parser=argparse.ArgumentParser(parents=parent)
		parser.add_argument('-s','--stop',action='store',type=int,default=0,help='imposta punto di interruzione')
		args=parser.parse_args()
		self.stop=args.stop

	def __call__(self,n=None):
		self.num+=1
		if n is None:
			n=self.num
		if n==self.stop:
			print('Esecuzione Interrotta al punto {}'.format(n))
			exit()
