
from time import time

class TicToc:
	def __init__(self):
		self.ts=time()
		self.flagts=True
	
	def tic(self):
		self.ts=time()
		self.flagts=True
	
	def toc(self):
		if self.flagts:
			self.tf=time()
			dt= self.tf-self.ts
			self.flagts=False
		else:
			t=time()
			self.ts=self.tf
			self.tf=t
			dt= self.tf-self.ts
		return dt

	def __call__(self):
		if self.flagts: return self.toc()
		else: return self.tic()


if __name__=='__main__':
	import unittest
	from time import sleep
	class Test(unittest.TestCase):
		def test_1(self):
			tt=TicToc()
			dtr=1.5
			sleep(dtr)
			dt=tt.toc()
			self.assertAlmostEqual(dt/dtr,1,2)
		def test_2(self):
			tt=TicToc()
			dtr=1
			tt.tic()
			sleep(dtr)
			dt=tt.toc()
			self.assertAlmostEqual(dt/dtr,1,2)
		def test_3(self):
			tt=TicToc()
			dtr=1
			tt.tic()
			sleep(dtr)
			dt=tt.toc()
			sleep(dtr)
			dt=tt.toc()
			self.assertAlmostEqual(dt/dtr,1,2)
		def test_4(self):
			tt=TicToc()
			dt=1
			sleep(dt)
			dtm=tt()
			self.assertAlmostEqual(dt/dtm,1,2)
	unittest.main(verbosity=2)
