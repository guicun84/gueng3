#-*-coding:latin
import os,sqlite3,re,sys,time,base64
from hashlib import sha256 
from gueng.utils.dbUtils import toAlias,toOneDict
from collections import OrderedDict as ODi
toBin=sqlite3.Binary

class FileInspect(object):
	def __init__(self,fi,calcSHA=True):
		'''classe pre ispezione dei file:
		   fi=file del database, se il file non esiste considera come basedir e crea database
		   '''
		self.__calcSHA=calcSHA
		if os.path.exists(fi):
			if os.path.isfile(fi):
				self.__caricaDb(fi)
			else:
				self.__creaDb(fi)
		else:
			self.__creaDb(fi)

	@property
	def calcSHA(self):
		return self.__calcSHA
	@calcSHA.setter
	def calcSHA(self,values):
		if values not in [True,False]: raise ValueError
		self.db.execute('update dbinfo set value=? where name="calcSHA"',(values,))
		self.db.commit()
		self.__calcSHA=values

	def __creaDb(self,fi):
		if not os.path.exists(fi):
			raise ValueError('cartella inesistente')
		#creazione nome file
		self.basedir=os.path.abspath(fi).decode('latin')
		fis=[i for i in os.listdir('.') if re.match('file_inspect',i)]
		if fis:
			n=([int(re.match('file_inspect_(\d+)',i).group(1)) for i in fis])
			print(n)
			n=max(n)
			dbname='file_inspect_{:03d}.sqlite'.format(n+1)
		else:dbname='file_inspect_000.sqlite'
		if os.path.exists(dbname): raise IOError('file {} gia esistente'.format(dbname))
		#creazione database
		self.db=sqlite3.connect(dbname)
		self.db.create_function('fullPath',1,self.__fullPath)
		query='''
create table if not exists dbinfo (
	id integer primary key autoincrement,
	name text,
	value blob);

create table if not exists fileList (
	ide integer primary key autoincrement,
	updir integer,--indice della cartella soprastante
	finame text,--nome del file completo
	type integer,--0 directory,1 file 
	ctime integer, --data creazione
	deleted integer, --1 se file cancellato o non ritrovato
	foreign key (updir) references files(id));

create table if not exists skipList(
	ide integer primary key autoincrement,
	finame text,
	type integer);

create table if not exists fileVersion( 
	ide integer primary key autoincrement,
	file integer,-- indice del file in filesList
	--lnk integer, --nodo linkato (solo linux)
	--ino integer, --inode (solo linux)
	mtime date, --data ultima modifica
	atime date, --data ultimo accesso su win poco utile visto che non si puo' ripristinare access time dopo calcolo SHA256 se non si modifica registro sistema con perdite prestazioni...

	size int, --dimensione
	sha256 blob, --sha file
	foreign key (file) references filesList(ide));
	'''
		self.db.executescript(query)
		self.db.execute('insert into dbinfo(name,value) values("basedir",?)',(self.basedir,))
		self.db.execute('insert into dbinfo(name,value) values("calcSHA",?)',(self.calcSHA,))
		s=os.stat(fi)
		dat=ODi((	('ide',1),
					('updir',1),
					('finame',os.path.basename(os.path.abspath(fi)).decode('latin')),
					('type',0),
#					('lnk',0),
#					('ino',0),
					('atime',s.st_atime),
					('ctime',s.st_ctime),
					('mtime',s.st_mtime),
					('atime',s.st_atime),
					('sha256',None)))
				
		self.db.execute('insert into fileList values (@ide,@updir,@finame,@type,@ctime,0)',dat)
		self.db.commit()
		self.__caricaDb()

	def __fullPath(self,fIndex):
		'''ricrea il nome del file ripercorrendo il database
		fIndex= indice nel database del file'''
		if fIndex==1: return self.basedir
		drs=[]
		ind=fIndex
		while True:
			dat=self.db.execute('select updir,finame from fileList where ide=?',(ind,)).fetchone()
			drs.append(os.path.basename(dat[1]))
			if dat[0]==1:break
			ind=dat[0]
		drs.append(self.basedir)
		return os.path.join(*drs[-1::-1])
	

	def __caricaDb(self,fi=None):
		if not hasattr(self,'db'):
			self.db=sqlite3.connect(fi)
			self.basedir=self.db.execute('select value from dbinfo where name="basedir"').fetchone()[0]
			self.__calcSHA=self.db.execute('select value from dbinfo where name="calcSHA"').fetchone()[0]
			self.db.create_function('fullPath',1,self.__fullPath)
		self.db.executescript('''
			--drop table if exists fileListFull;
			create view if not exists fileListFull as select ide,fullPath(ide) as finame from fileList;
			--create index if not exists fileListFull_finame on fileListFull(finame);
			create index if not exists fileList_ide on fileList(ide);
			create index if not exists fileList_type on fileList(type)''')
		self.db.commit()

	def __updateFile(self,fullpath,ide=None,typ=None):
		'''aggiorna file gia presenti nel database:
		fullpat=nome completo file
		ide=indice del file [se non presente lo cerca]
		typ=tipo di file, [se non presnte lo cerca]'''
		#recupero dati mancanti
		if ide is None or typ is None:
			if ide is None:
				query='select ide,type from fileListFull where finame=@finame'
			else:
				query='select ide,type from fileList where ide=@ide'
			ide,typ=self.db.execute(query,{'finame':fullpath,'ide':ide}).fetchone()
		if not os.path.exists(fullpath): #controllo che il file non sia stato cancellato
			self.db.execute('update fileList set deleted=1 where ide=?',(ide,))
			return
		if typ==1:
			if self.calcSHA:
				with open(fullpaht,'rb') as fi:
					sha=sha256.new()
					while True:
						s=fi.read(10000000)
						if s=='':break
						sha.update(s)
					sha=sha.digest
			else: sha=None
		else:
			sha=None
		s=os.stat(fullpath)
		dat=ODi((	
			('ide',ide),
			('type',ty),
#				('lnk',0),
			('mtime',s.st_mtime),
			('atime',s.st_atime),
			('size',s.st_size),
			('sha256',sha)))
		self.db.execute('insert into fileVersion values (Null, @ide ,@mtime,@atime,@size,@sha256)',dat)

	def __addNewFile(self,updir,fullpath,ty):
		'''aggiunte un file al database
		updir: indice directory appartenenza
		fullpath: nome di completo del file
		ty=0,1,2 se directory,file,link o altro'''
		if ty==0:
			sha=None
		elif ty==1:
			if self.calcSHA:
				sha=sha256()
				with open(fullpath,'rb') as ff:
					while True:
						t=ff.read(10000000)
						if t=='':break
						sha.update(t)
				sha=toBin(sha.digest())
			else: sha=None
		else:
			sha=None
		s=os.stat(fullpath)
		dat=ODi((	('updir',updir),
					('finame',os.path.basename(fullpath)),
					('type',ty),
#					('lnk',0),
					('ctime',s.st_ctime),
					('mtime',s.st_mtime),
					('atime',s.st_atime),
					('size',s.st_size),
					('sha256',sha)))
		self.db.execute('insert into fileList values (Null,@updir,@finame,@type,@ctime,0)',dat)
		ide=self.db.execute('select max(ide) from fileList').fetchone()[0]
		try: #nel caso ci sia la tabella con i nomi file completi la aggiungo
			self.db.execute('insert into FileListFulltb values (?,?)',(ide,fullpath))
		except:pass
		if ty==1:
			dat['finame']=fullpath
			self.db.execute('insert into fileVersion values (Null, (select max(ide) from fileList) ,@mtime,@atime,@size,@sha256)',dat)

	
	def aggiornaFile(self,mode='mtime',numToCommit=1000):
		'''controlla i file presenti su hd e li confronta con i dati in database, in base ai casi:
			aggiunge nuovi file
			aggiorna file presenti
			mode: [mtime,sha] valore di test per definire se il file � stato modificato
			mumToCommit= numero di inserimenti prima di un commit'''
		n=[0,0]
		self.db.executescript('''create temporary table fileListFulltb as select * from fileListFull;
		create index fileListFulltb_finame on fileListFulltb(finame);''')
		for cd,ds,fs in os.walk(self.basedir):
			if type(cd) != str:cd=cd.decode('latin')
			updir=self.db.execute('select ide from fileListFulltb where  finame=? limit 1',(cd,)).fetchone()[0] 
			for d in ds:
				if type(d) != str:d=d.decode('latin')
				pa=os.path.join(cd,d)
				if not self.db.execute('select 1 from fileListFulltb where finame=?',(pa,)).fetchone():
					self.__addNewFile(updir,pa,0)
					n[0]+=1
				else:
					n[1]+=1
				if not n[0]%numToCommit:
					self.db.commit()
				if not sum(n)%numToCommit:
					sys.stdout.write('\r{: 9}-{:60}'.format(sum(n),pa)[:71])
					sys.stdout.flush()

			for f in fs:
				if type(f) != str:f=f.decode('latin')
				pa=os.path.join(cd,f)
				if not self.db.execute('select 1 from fileListFulltb where finame=?',(pa,)).fetchone():
					self.__addNewFile(updir,pa,1)
					n[0]+=1
				else:
					n[1]+=1
				if not n[0]%numToCommit:
					self.db.commit()
				if not sum(n)%numToCommit:
					sys.stdout.write('\r{: 9}-{:60}'.format(sum(n),pa)[:71])
					sys.stdout.flush()
					
		self.db.executescript('''create index if not exists fileList_finame on fileList(finame);
		drop table fileListFulltb''')

		self.db.commit()
		self.__caricaDb()
		return n

	def cancellaFile(self):
		self.db.executescript('''delete from fileList where ide!=1 ;
		delete from fileVersion;
		drop index if exists FileList_finame;''')
		self.db.commit()
		self.__caricaDb()

	@staticmethod
	def __testCheckName(check):
		'''funzione di test per controllare che check sia valido'''
		test=check in('sha256 mtime atime size'.split())
		if not test:raise ValueError('check={} not in [sha256 mtime atime size]')

	
	def __checkFile(self,check,files=[]):
		'''controlla che la propriet� check non sia cambiata tra db e fs'''
		self.__testCheckName(check)
		queryDat={}
		query='''
select fileListFull.finame,{check} from fileList inner join fileListFull on fileList.ide=fileListFull.ide inner join (
	select f1.file,f1.{check} from fileversion f1 inner join (
		select f2.file,max(f2.mtime) as mm from fileVersion f2 group by file) ma on
		f1.file=ma.file and f1.mtime=ma.mm) shl on fileList.ide=shl.file'''.format(check=check)
		if files:
			queryDat.update(toAlias(file=files))
			query += ' where finame in (@'+ ',@'.join(list(queryDat['file'].keys())) +')'
		dat=self.db.execute(query,toOneDict(queryDat))
		lst=[]
		n=0
		for i in dat:
			n+=1
			if os.path.exists(i[0]):
				if check =='sha256':
					sha=sha256()
					with open(i[0],'rb') as f:
						while True:
							r=f.read(10000000)
							if r=='':break
							sha.update(r)
					sha=sha.digest()
					if sha!=str(i[1]):
						lst.append(i[0])
				else:
					s=os.stat(i[0])
					ref={'mtime':s.st_mtime,'atime':s.st_atime,'size':s.st_size}[check]
					if ref != i[1]:
						lst.append(i[0])
			else:
				lst.append(i[0])
		print('controllati {} files'.format(n))
		return lst

	def checkFileSha256(self,files=[]):
		return self.__checkFile('sha256',files)
	def checkFileMtime(self,files=[]):
		return self.__checkFile('mtime',files)
	def checkFileAtime(self,files=[]):
		return self.__checkFile('atime',files)
	def checkFileSize(self,files=[]):
		return self.__checkFile('size',files)

	def cercaNuoviFile(self):
		w=os.walk(self.basedir)
		newDirs=[]
		newFiles=[]
		for cd,ds,fs in w:
			for d in ds:
				pa=os.path.join(cd,d)
				if not self.db.execute('select 1 from fileListFull where finame=?',(pa,)).fetchone():
					newDirs.append(pa)
			for f in fs:
				pa=os.path.join(cd,f)
				if not self.db.execute('select 1 from fileListFull where finame=?',(pa,)).fetchone():
					newFiles.append(pa)
		return newDirs,newFiles

	def __str__(self):
		out=[]
		for i in self.db.execute('''
			select flf.finame,atime,mtime,sha256,num from fileListFull flf 
			inner join fileList fl on flf.ide=fl.ide 
			inner join (
				select *,max(mtime) as maxmtime,count(*) as num from fileversion group by file) fv
			on flf.ide=fv.file'''):
			fn=i[0].replace(self.basedir,'')
			at=time.localtime(i[1])
			at=time.strftime('%Y-%m-%dT%H:%M:%S',at)
			mt=time.localtime(i[2])
			mt=time.strftime('%Y-%m-%dT%H:%M:%S',mt)
			sha=base64.binascii.hexlify(i[3])
			num='{}'.format(i[4])
			out.append(' | '.join([fn,at,mt,sha,num]))
		return '\n'.join(out)


