
class Int2St:
	'''classe per trascrivere in lettere un numero intero'''
	cifre='zero uno due tre quattro cinque sei sette otto nove dieci undici dodici tredici quattordici quindici sedici diciassette diciotto diciannove'.split()
	decine=['']+'dieci venti trenta quaranta cinquanta sessanta settanta ottanta novanta'.split()
	centinaia='cento'

	def __init__(self,num,addSpace=False):
		'''num=numero da trascrivere, se float considera int(num)
		addSpace=False, se True aggiunge uno spazio tra Miliardi Milioni...'''
		if type(num) is not int: num=int(num)
		vars(self).update(locals())
		self.__st=None

	@property
	def st(self):
		if self.__st is None:
			self.__st=self.parse()
		return self.__st
	
	def parseCento(self,num):
		'''calcolo la stringa per le centinaia'''
		ce=num//100
		de=(num-ce*100)//10
		u=num -de*10 -ce*100

		if de<2:
			u=u+10*de
			de=0
		if ce==0 and de==0 and u==0: return 'zero'
		u=Int2St.cifre[u]
		de=Int2St.decine[de]
		if ce==0: ce=''
		elif ce==1: ce='cento'
		else: ce=Int2St.cifre[ce]+'cento'
		if u=='zero':u=''
		if u in ('uno','otto') and de!='': de=de[:-1]
		return ce+de+u	

	def parse(self):
		'''individuo miliardi,milioni,migliaia e applico ad essi il parse delle centinaia aggiungendo i suffissi dovuti'''
		G=self.num//1000000000 #miliardi
		M=(self.num-G*1000000000)//1000000 #milioni
		K=(self.num-G*1000000000 - M*1000000)//1000 #migliaia
		c=(self.num-G*1000000000 - M*1000000-K*1000) #centinaia

		if G>0: 
			G=self.parseCento(G)
			if G=='uno': G='unmiliardo'
			else: G+='miliardi'
		else: G=''

		if M>0: 
			M=self.parseCento(M)
			if M=='uno': M='unmilione'
			else: M+='milioni'
		else: M=''

		if K>0: 
			K=self.parseCento(K)
			if K=='uno': K='mille'
			else: K+='mila'
		else: K=''

		c=self.parseCento(c)
		
		if (G or M or K) and c=='zero': c=''
		if self.addSpace:
			c1={True:' ' ,False:''}[bool(G)]
			c2={True:' ' ,False:''}[bool(M)]
			c3={True:' ' ,False:''}[bool(K)]
			return G+c1+M+c2+K+c3+c
		else:
			return G+M+K+c

	def __str__(self):
		return self.st

class Float2St:
	'''classe per trascrivere in lettere un numero float'''
	def __init__(self,num,dig=2,sep=',' ,intsuf='', decsuf=''):
		'''num=numero
		dig=numero di decimali a cui ARROTONDA il numero
		sep= separatore tra parte intera e decimale
		intsuf suffisso per lap arte intera
		decsuf=suffisso per la parte decimale'''
		vars(self).update(locals())
		self.__st=None

	@property
	def st(self):
		if self.__st is None: self.parse()
		return self.__st

	def parse(self):
		i=int(self.num)
		self.num=round(self.num,self.dig)
		d=int(round((self.num-i)*(10**self.dig),0))
		self.__st='{} {} {} {} {}'.format(Int2St(i),self.intsuf,self.sep,Int2St(d),self.decsuf)
		self.__st=self.st.replace('  ',' ')
		if self.__st[-1]==' ': self.__st=self.__st[:-1]


	def __str__(self):
		return self.st


class Float2StE(Float2St):
	def __init__(self,num):
		Float2St.__init__(self,num,2,'e','Euro','centesimi')


import re
class St2Int(Int2St):
	def __init__(self,st):
		self.st=st
		self.parse()

	def parse(self):
#		path='(unmiliardo|.*miliardi)?(unmilione|.*milioni)?(mille|.*mila)?(.?)'
#		dat=re.match(path,self.st)
		
		path='(?:(.*)miliard[oi])?(?:(.*)milion[ei])?(?:(.*m)(?:il(?:le|a)))?(.*)?'
		dat=list(re.match(path,self.st).groups())
		for n,i in enumerate(dat):
			if i =='un':dat[n]='uno'
		if dat[2]:
			if dat[2]=='m': dat[2]='uno'
			else: dat[2]=dat[2][:-1]
		for n,i in enumerate(dat):
			if i is None: dat[n]='zero'
		for n,i in enumerate(dat):
			dat[n]=self.parseCento(i)
		self.num=int(dat[0]*1e9 + dat[1]*1e6 + dat[2]*1e3 + dat[3])


	def parseCento(self,st):
		path='(?:(.*c)ento)?(.*)?'
		dat=list(re.match(path,st).groups())
		if dat[0]:
			if dat[0]=='c':dat[0]='uno'
			else: dat[0]=dat[0][:-1]
		for n,i in enumerate(dat):
			if i is None: dat[n]='zero'
		num=Int2St.cifre.index(dat[0])*100
		#devo ottenere le decine:
		if dat[1] in Int2St.cifre:
			num+=Int2St.cifre.index(dat[1])
		else:
#			path='dd(?:(.*)('+'|'.join(Int2St.cifre) +')|(.*))'
			dec=[i[:-1] for i in Int2St.decine[1:]]
			path='(' + '|'.join(dec) +')?.?(' + '|'.join(Int2St.cifre) +')?'
			dat=list(re.match(path,dat[1]).groups())
			if dat[1] is None: dat[1]='zero'
			num+=10*(dec.index(dat[0])+1)
			num+=Int2St.cifre.index(dat[1])
		return num
		
	def __str__(self):
		return '{}'.format(self.num)

if __name__=='__main__':
	n=int(122e9+15e6+18e3+16)
	st=Int2St(n).st
	n2=St2Int(st)
	print(n)
	print(n2)
	print(st)
