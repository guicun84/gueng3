from numpy import *
#questa funzione server per poter ottenere la versione in scala di grigio delle mappe di colore di matplotlib (interessante e copper e cool)
def grayify_cmap(cmap):
    """Return a grayscale version of the colormap"""
    cmap = cm.get_cmap(cmap)
    colors = cmap(arange(cmap.N))
    
    # convert RGBA to perceived greyscale luminance
    # cf. http://alienryderflex.com/hsp.html
    RGB_weight = [0.299, 0.587, 0.114]
    luminance = sqrt(dot(colors[:, :3] ** 2, RGB_weight))
    colors[:, :3] = luminance[:, newaxis]
	
    return cmap.from_list(cmap.name + "_grayscale", colors, cmap.N)

