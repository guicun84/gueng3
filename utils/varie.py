﻿#-*- coding:utf-8-*-

from scipy import *
from time import strftime
from itertools import cycle
from collections import namedtuple,OrderedDict
import inspect ,re
from gueng.utils.cercaVar import SearchVar
import sys,os
from io import StringIO

floatPath='(?:(?:-?\d+(?:\.\d+)?)|(?:-?\.\d+))(?:[eE][+-]?\d+)?' #stringa generica per individuare qualunghe numero

class Err:
	def __init__(self,target,value):
		er=value-target
		erp=er/target
		vars(self).update(locals())
	def __str__(self):
		return f'target={self.target:.5g},value={self.value:.5g},errore={self.er:.5g} --> {self.erp:.2%}'
	

def err(v1,v2):#funzioneper calcolo errore percentuale
	'''v1= riferimento
	   v2= valore da controllare'''
	er=(v2-v1)/v1
	return er

def print2(st):
	'''funzione per scirvere e sostituire immediatamente i valori di una stringa con formattazione semplificata: recuperando i valori da locals()
	print2('prima sostituzione {valore}
	{#val2} secondo valore')
	restituirebbe:
	'prima sostituzione 13212321
	val2= 32131.321321 secondo valore'
	controlla automaticamente i tipi float e li rappresenta con 4 cifre decimali in notazione esponenziale
	'''
	loc=inspect.currentframe().f_back.f_locals
	st=re.sub('{#(([^:]+?):?.*?)}','\\2= {\\1}',st)
	k=re.findall('{[^:]+?}',st)
	for i in k:
		if isinstance(loc[i[1:-1]],float):
			st=st.replace(i,'{{{}:g}}'.format(i[1:-1]))
	st=st.format(**loc)
	k=re.findall('\[[^\]]+\]',st)
	for i in k:
		st=st.replace(i,i.replace(' ',' , '))
	print(st)

def print_(st):
	'''funzione per scrivere a monitor e su eventuale doc solo se in esecuzione come '__main__' altrimenti nulla'''
	loc=inspect.currentframe().f_back.f_locals
	if loc['__name__'] != '__main__': return
	print (st)
	if 'doc' in list(loc.keys()):
		if hasattr(loc['doc'],'write'):
			print(st, file=loc['doc'])

try: 
	import pint
	pint.UnitRegistry()
	testImportPint=True
except:
	testImportPint=False

def toString(*args,**kargs):
	'''scirve a monitor i risultati:
*args= dic,ordine 
*args= ordine
*args= dic'''
	std={'col':1,'libo':0}
	if 'col' in list(kargs.keys()):
		col=kargs['col']
	else:
		col=std['col']

	if 'libo' in list(kargs.keys()):
		libo=kargs['libo']
	else:
		libo=std['libo']

	if type(args[0]) not in [str,list,tuple]:
		dic=args[0]
		if type(dic)!=dict:
			dic=dic._asdict()
		if len(args)>=2:
			ordine=args[1]
		else:
			ordine=list(dic.keys())
		if len(args)==3:
			col=args[2]
	else:
		dic= SearchVar(2)
#		dic=inspect.currentframe().f_back.f_locals
		ordine=args[0]
	if type(ordine)==str:
		if '\n' in ordine:
			ordine=ordine.split('\n')
		elif ',' in ordine:
			ordine=ordine.split(',')
		else:
			ordine=ordine.split()
	#cerco eventuali unità di misura
	ordine=[i for i in ordine if i!='']
	unit={}
	for n,i in enumerate(ordine):
		#rimuovo eventuali spaziature iniziali:
		ordine[n]=re.sub('^\s*','',i)
		i=ordine[n]
		if '[' in i and i[0]!='#':
			u=re.search('\[(.*)\]',i)
			ordine[n]=re.sub('\[.*\]','',i)
			unit[ordine[n]]=u.groups(1)[0]
		else:
			unit[i]=('')
	r=[] #riga
	out=[] #riga_formattata
	cy=cycle(list(range(col)))
	for i in ordine:
		varname=re.search('[\$\s]*([^\$]*)\$*',i).group(1)
		if i[0]=='#':
			r.append(i[1:])

		elif type(dic[varname])==int:
#			r.append('%s= %d %s'%(i,dic[varname],unit[i]))
			r.append('{}= {} {}'.format(i,dic[varname],unit[i]))
		else:
			tiflag=False
			if testImportPint:
				if isinstance(dic[varname],pint.quantity._Quantity):
					r.append('{}= {:.3f~} {}'.format(i,dic[varname],unit[i]))
					tiflag=True
			if not tiflag:
				try:
	#				r.append('%s= %.4g %s'%(i,dic[varname],unit[i]))
					r.append('{}= {:.4g} {}'.format(i,dic[varname],unit[i]))
				except:
	#				r.append('%s= %s %s'%(i,dic[varname],unit[i]))
					r.append('{}= {} {}'.format(i,dic[varname],unit[i]))
		if libo:
			r[-1]=re.sub('^([^=]*)',r'<#\1#>',r[-1])
			if unit[i]:
				r[-1]=re.sub(' ([^\d].*)',r' <#\1#>',r[-1])

		if next(cy)==col-1:
			out.append('\t'.join(r))
			r=[]
	out.append('\t'.join(r))
	return '\n'.join(out[:-1])

def print2s(*args,**kargs):
	'''print diretto a monitor della stringa ottenuta da toString con stessa sintassi'''
	print((toString(*args,**kargs)))

def to_dict(*args):
	'''genera un dizionario con le variabili passate in args
	args segue la stessa metodologia di toString'''
	v={}
	if type(args[0])==dict:
		var=args[0]
		args=args[1:]
	else:
#		var=inspect.currentframe().f_back.f_locals
		var=SearchVar(2)
	if len(args)==1:
		if ',' in args:
			args=args[0].split(',')
		else:
			args=args[0].split()
			
	for i in args:
		v[i]=var[i]
	return v

toDict=to_dict

def to_nt(*args):
#	var=inspect.currentframe().f_back.f_locals
	var=SearchVar(2)
	dic=to_dict(var,*args)
	if len(args)==1:
		args=args[0].split(',')
	nt=namedtuple('collections',args)(**dic)
	return nt

def M_rot(rx=0,ry=0,rz=0,conv=1):
	scale={'d':pi/180 , 'c':pi/200 , 'r':1.}
	if conv in list(scale.keys()):
		conv=scale[conv]
	rx*=conv
	ry*=conv
	rz*=conv
	mx=c_[r_[1,0,0],r_[0,cos(rx),sin(rx)],r_[0,-sin(rx),cos(rx)]].T
	my=c_[r_[cos(ry),0,sin(ry)],r_[0,1,0],r_[-sin(ry),0,cos(ry)]].T
	mz=c_[r_[cos(rz),sin(rz),0],r_[-sin(rz),cos(rz),0],r_[0,0,1]].T
	return dot(dot(mx,my),mz)

def tabify(val,ordine=None,righe=False,formati={int:' 5d',str:' 5s',str:' 5s','other':' 5.4g'}):
	val_=[]
	if type(val[0]) in (list,tuple):
		val=list2listdict(val,ordine,righe)
		for i in val:
			val_.append(i)
	elif type(val[0])!=dict and '_asdict' in dir(val[0]): #ho delle named_tuple
		for i in val:
			val_.append(i._asdict())
	else:
		for i in val:
			val_.append(i)
	#identifico i tipi di dato
	if ordine is None:
		ordine=list(val_[0].keys())
	elif type(ordine)==str:
		ordine=ordine.split(',')
	desc='\t'.join(list(val_[0].keys()))
	for i in val_:
		if desc!= '\t'.join(list(i.keys())): raise IOError('dizionari differenti')	
	desc='\t'.join(['% 5s'%i for i in ordine])
	types=[]
	for i in ordine:
		if type(val_[0][i]) in list(formati.keys()):
			types.append(formati[type(val_[0][i])])
		else:
			types.append(formati['other'])
	out=[]
	for i in val_:
		r=[]
		for t,j in zip(types,ordine):
			r.append(('%'+t)%i[j])
		r='\t'.join(r)
		out.append(r)
	out='\n'.join(out)
	out=desc+'\n'+out

	return out

def str_now():
	return strftime('%Y%m%d%H%M%S')

def qm_like(val):
	'resitituisce "(?,?,...,?)" tanti quanti gli elementi passati'
	return '(' + ','.join(['?' for i in val]) + ')'



def list2listdict(li,ordine,righe=False):
	if type(ordine)==str:
		ordine=ordine.split(',')
	out=[]
	if type(li)==dict:
		l=[]
		for i in ordine:
			l.append(li[i])
		li=l
	if righe:
		r=len(li)
		c=len(li[0])
		l=[]
		for co in range(c):
			ou=[]
			for ri in range(r):
				ou.append(li[ri][co])
			l.append(ou)
		li=l
	for n,i in enumerate(li[0]):
		out.append(dict(list(zip(ordine,[v[n] for v in li]))))
	return out

def get_out(fi,fi_out=None,loc=None,glob=None):
	'''fi= file da eseguire
	fi_out= file dove scrivere: None resitiuisce loutput come stringa, 1 scrive su file con nome= fi ed estensione txt,
	loc=local se none usa il frame precedente
	glob=globale se None usa il frame precedente'''
	if loc is None:
		loc=inspect.currentframe().f_back.f_locals
	if glob==None:
		glob=inspect.currentframe().f_back.f_globals
	__old__out__=sys.stdout
	__new__out__=StringIO()
#	try:
	if True:
		sys.stdout=__new__out__
		exec(compile(open(fi, "rb").read(), fi, 'exec'),glob,loc)
		sys.stdout=__old__out__
		if fi_out is None:
			return __new__out__.getvalue()
		else:
			if not type(fi_out)==str:
				fi_out=os.path.splitext(fi)[0]+'.txt'
			fi_out=open(fi_out,'wb')
#			fi_out.write(__new__out__.getvalue().encode('latin'))
			fi_out.write(__new__out__.getvalue())
			fi_out.close()
#	except :
	else:
		sys.stdout=__old__out__
		return __new__out__.getvalue()

class NewVar:
	'''tiene traccia delle nuove variabili... poco valida'''
	def __init__(self):
		self.start=dict([[i,j] for i,j in list(inspect.currentframe().f_back.f_locals.items())])
		self.new=[]
	def __call__(self):
		if self.new: 
			pass
		else:
			cur=inspect.currentframe().f_back.f_locals
			for i,j in list(cur.items()):
				if i in ['nan', 'NaN', 'NAN'] :continue
				if i not in list(self.start.keys()) :
					self.new.append(i)
				else:
					if any(j!=self.start[i]):
						self.new.append(i)

	
def inte2p(vi,vf,dif,dip):
	'''vi=valore iniziale
	vf=valore finale
	dif=distanza totale tra punti
	dip distanza tra punto iniziale e punto in esame'''
	return interp(dip,(0,dif),(vi,vf))

def rotsol(R,sol,copy=True):
	if copy:
		out=sol.copy()
	else:
		out=sol
	for n,i in enumerate(out):
		out[n,:3]=R@i[:3]
		out[n,3:6]=R@i[3:6]
	return out
	

