#-*-coding:utf-8-*-
from lxml import etree
import time
import os

class Time:
	def __init__(self):
		pass
	
	def get(self):
		return time.strftime('%Y/%m/%d-%H:%M:%S')

	def s2f(self,st):
		return time.strptime('%Y/%m/%d-%H:%M:%S',st)

class Document:
	tags=['disegno','aggiunta','modifica','rimozione','file','fork','backup','nota','info']
	timer=Time()
	def __init__(self,fi):
		self.fi=fi
		if os.path.exists(fi):
			self.open_file()
		else:
			self.crea_file()

		
	def crea_file(self):
		self.doc=etree.Element('disegno',time=self.timer.get())
		self.note_count=0
		self.titoli_count=0
		self.note={}
		self.note_all=[]
		self.titoli={}
	
	def open_file(self):
		parser = etree.XMLParser(remove_blank_text=True)
		self.doc=etree.parse(self.fi,parser).getroot()
		#ricerco i "riquadri" presenti nel disegno
		self.titoli={}
		for e in self.doc.iter('aggiunta'):
				self.titoli[int(e.attrib['ide'])]=e.attrib['titolo']
		self.titoli_count=max(self.titoli.keys())
		#ricerco e conto le note:
		self.note_all=[i for i in self.doc.iter('nota')]
		if self.note_all:
			self.note_count=max([int(i.attrib['ide']) for i in self.note_all])
		else: self.note_count=0
		#ricerco le note non cancellate
		self.note={}
		for i in self.note_all:
			if i.attrib['delete']=='False':
				self.note[int(i.attrib['ide'])]=i

	def file(self,st):
		'''aggiunge un file di disegno al log'''
		#controllo esistenza file
		if not os.path.exists(st):
			print('il file indicato non esiste')
			return 
		else:
			self.doc.append(etree.Element('file',time=self.timer.get(),url=st))

	def add_last_file(self,tag,titolo=None,st=None):
		'''aggiunge all'ultimo file un elemento con "tag"'''
		if tag not in self.tags: return 
		fi=self.doc.findall('file')
		if not len(fi): return 
		last=fi[-1]
		new=etree.Element(tag,time=self.timer.get())
		if titolo:
			if type(titolo)==int:
				parent=titolo
				titolo=self.titoli[titolo]
			else:
				parent=list(self.titoli.keys())[self.titoli.values.index(titolo)]
			new.attrib['titolo']=str(titolo,'utf8')
			new.attrib['parent']='{}'.format(parent)
		if st:
			st=str(st,'utf8')
			new.attrib['value']=st
		last.append(new)
			
	def aggiunta(self,titolo,st=None,parent=None,completo=False):
		'''aggiunte all'ultimo file un titolo'''
		titolo=str(titolo)

		if titolo not in list(self.titoli.values()):
			last=self.doc.findall('file')
			if last:
				el=etree.Element('aggiunta',time=self.timer.get(),titolo=titolo)
				if st: el.attrib['value']=str(st,'utf8')
				if parent:
					if parent not in list(self.titoli.keys()):
						if parent in list(self.titoli.values()):
							parent=list(self.titoli.keys())[list(self.titoli.values()).index(parent)]
						else: 
							print('paren non trovato')
							return 
					el.attrib['parent']='{}'.format(parent)
				if completo==False: el.attrib['completo']='False'
				elif completo==True: el.attrib['completo']=self.timer.get()
				elif type(completo)==str:
					try :
						dat=self.timer.s2f(completo)
						el.attrib['completo']=completo
					except:
						print('data di completamento mal formattata')
						return 
				self.titoli_count+=1
				el.attrib['ide']='{}'.format(self.titoli_count)
				self.titoli[self.titoli_count]=titolo
				last[-1].append(el)
			else: print('nessun file presente')
		else:
			print(titolo, 'gia presente, inserire nuovo nome')
			return

	def modifica(self,titolo,st):
		'''aggiunge all'ultimo file una modifica'''
		if titolo not in list(self.titoli.values()) and titolo not in list(self.titoli.keys()): 
			print(titolo, ' non presente nei disegi')
			return 
		self.add_last_file('modifica',titolo,st)

	def rimozione(self,titolo,st=None):
		'''aggiunge all'utlimo file un elemento per indicare la rimozione di un titolo'''
		if titolo not in list(self.titoli.values()) and titolo not in list(self.titoli.keys()):
			print(titolo, ' non presente nei files')
			return 
		self.add_last_file('rimozione',titolo,st)

	def fork(self,st):
		'''aggiunge un elemento per indicare che si è cominciato un nuovo file che verrà portato avannti per altre finalità'''
		self.add_last_file('fork',st)
	
	def info(self,st):
		self.add_last_file('info',st=st)

	def backup(self,st):
		'''aggiunge un elemento per indicare che si è lasciato un file di backup e si continua su un altro file'''
		if not os.path.exists(st):
			print('il file indicato non esiste')
			return 
		else:
			fi=etree.Element('file',time=self.timer.get(),url=st)
		self.add_last_file('backup',st)
		self.doc.append(fi)

	def nota(self,titolo=None,st=None,fi=True):
		'''aggiunge una nota per indicare criticità nei disegni da risolvere'''
		if not titolo and not st: return 
		no=etree.Element('nota',delete='False',time=self.timer.get())
		if titolo and  not titolo  in list(self.titoli.keys()):
			if titolo not in list(self.titoli.values()):
				print(titolo, 'non presente')
			else:
				parent=list(self.titoli.keys())[list(self.titoli.values()).index(titolo)]
		else:
			parent=titolo
			titolo=self.titoli[titolo]
		if titolo: 
			no.attrib['titolo']=titolo
			no.attrib['parent']='{}'.format(parent)
		if st: no.attrib['value']=str(st,'utf8')
		if fi:
			last=self.doc.findall('file')
			if not last: 
				print('nessun file trovato in cui aggiungere nota')
				return 
			last=last[-1]
		else:
			last=self.doc
		self.note_count+=1
		no.attrib['ide']='{}'.format(self.note_count)
		last.append(no)
		self.note[self.note_count]=no
		self.note_all.append(no)

	def delnota(self,ide):
		'''imposta una nota come cancellata'''
		if not ide in list(self.note.keys()): print('nota {} non trovata'.format(ide))
		else:
			self.note[ide].attrib['delete']=self.timer.get()
			del self.note[ide]

	def completa(self,ide,st=None):
		'''imposta un titolo come completato'''
		if not ide in list(self.titoli.keys()):
			if ide in list(self.titoli.values()):
				ide=list(self.titoli.keys())[list(self.titoli.values()).index(ide)]
			else: 
				print(ide, 'non presente nei titoli')
				return 
		ide='{}'.format(ide)
		ag=[i for i in self.doc.iter('aggiunta') if i.attrib['ide']==ide][0]
		if ag.attrib['completo']!="False":
			print(ide, ' risulta gia completato ',ag.attrib['completo'])
			return 
		if st:
			try:
				self.timer.s2f(st)
				ag.attrib['completo']=st
			except:
				print(st, 'mal formattato')
		else:
			ag.attrib['completo']=self.timer.get()

	def save(self,fi=None):
		'''scrive il file'''
		if fi is None: fi=self.fi
		fi=open(self.fi,'w')
		fi.write('{}'.format(self))
		fi.close()

	def get(self,tag,ide=None,parent=None,fun=None,el=1):
		li=[i for i in self.doc.iter() if i.tag==tag]
		if ide: li=[i for i in li if i.attrib['ide']=='{}'.format(ide)]
		if parent: li=[i for i in li if i.attrib['parent']=='{}'.format(parent)]
		if fun:li=[i for i in li if fun(i)]
		if el: return ElementList(li)
		else: return li

	@property
	def daFinire(self):
		'''restituisce i titoli dei box ancora da completare'''
		df=[i for i in self.doc.iter('aggiunta') if i.attrib['completo']=='False']
		return '\n'.join(['{})\t{}'.format(i.attrib['ide'],i.attrib['titolo']) for i in df])

	@property
	def daFare(self):
		'''restituisce le note ancora attive'''
		no=[i for i in self.doc.iter('nota') if i.attrib['delete']=='False']
		ti=[i.attrib['parent'] for i in no if 'parent' in list(i.attrib.keys())]
		ti=[int(i) for i in ti if i!='None']
		ti.sort()
		st=[]
		for t in ti:
			st.append('{}'.format(self.titoli[t]))
			for n in no:
				if 'parent' in list(n.attrib.keys()):
					if n.attrib['parent']!='None':
						if int(n.attrib['parent'])==t:
							st.append('\t{}\t{}'.format(n.attrib['ide'],n.attrib['value']))
		st.append('')
		for n in no:
			if 'parent' not in list(n.attrib.keys()):
				st.append('{}\t{}'.format(n.attrib['ide'],n.attrib['value']))
			elif n.attrib['parent']=='None':
				st.append('{}\t{}'.format(n.attrib['ide'],n.attrib['value']))
		return '\n'.join(st)



	
	
	def __str__(self):
		return etree.tostring(self.doc,pretty_print=True)


class ElementList:
	def __init__(self,li):
		self.li=li

	def tostring(self,att=None):
		if att==None:
			for i in self.li:
				print(etree.tostring(i,pretty_print=True))
		else:
			if type(att)==str: att=[att]
			for i in self.li:
				st=';'.join(['{}: {} '.format(j,i.attrib[j]) for j in att])
				print(st)
				

