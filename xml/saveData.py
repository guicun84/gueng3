import os
from lxml import etree
from lxml.builder import E
import re
import datetime

class SaveData:
	def __init__(self,finame):
		self.finame=finame
		if os.path.exists(finame):
			self.loadXml()
		else:
			self.createXml()
			self.loadXml()
	
	def createXml(self):
		with open(self.finame,'w') as fi:
			fi.write('<dati></dati>')
	
	def loadXml(self):
		self.xml=etree.parse(self.finame)

	def save(self):
		with open('new_'+self.finame,'w') as fi:
			data='{}'.format(datetime.datetime.now())
			self.xml.xpath('/dati')[0].attrib['last-save']=data
			fi.write(etree.tostring(self.xml,pretty_print=True))
		if os.path.exists('journal_'+self.finame):
			os.remove('journal_'+self.finame)
		os.rename(self.finame,'journal_'+self.finame)
		os.rename('new_'+self.finame,self.finame)
		os.remove('journal_'+self.finame)
	
	@staticmethod
	def tostring(el):
		return etree.tostring(el,pretty_print=True)

	def __str__(self):
		return etree.tostring(self.xml,pretty_print=True)

	def __getitem__(self,xpath):
		out= self.xml.xpath(xpath)
		if not out:
			out= self.creaElemento(xpath)
		return out

	def xpath(self,xpath):
		return self.xml.xpath(xpath)

	def __setitem__(self,xpath,value):
		el=self[xpath]
		if not el:
			el=self.creaNodo(xpath)
		for e in el:
			e.text='{}'.format(value)

	def append(self,els):
		self.xml.getroot().append(els)
		return els

	def creaElemento(self,xpath):
		path=xpath.split('/')
		last=self.xml.getroot()
		for p in path:
			search=last.xpath(p)
			if search:
				if len(search)==1:
					last=search[0]
				else:
					return []
			else:
				p=re.search('''([^\[]+)(?:\[text\(\)=['"]([^'"]+)["']\])?''',p).groups()
				tag=p[0]
				string=None
				if len(p)==2: string=p[1]
				if string:
					search=E(tag,string)
				else:
					search=E(tag)
				last.append(search)
				last=search
		return last

	@staticmethod
	def wipe(elem):
		for c in elem:
			elem.remove(c)

