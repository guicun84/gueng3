#-*-coding:utf-8-*-

from scipy import *
from gueng.profilario import bul as BUL
from scipy.linalg import norm

'''PROBLEMATICHE CONOSCIUTE

1) nel calcolo del giunto  viene considerata come larghezza quella totale dell'elemento, cosa sempre vera solo se il connettore è passante (bullone, spinotto non sottomesso)

DA FARE 
1) passare o far calcolare l'effettiva profondità del connettore per eliminare la problematica 1 
'''

class Connettore:
	def __init__(self,d,mat):
		vars(self).update(locals())
		self.Ares=self.d**2*pi/4

	def Nrk(self):
		return self.mat.fyk*self.Ares

class Bullone(Connettore):
	def __init__(self,d,mat,rond=None):
		Connettore.__init__(self,d,mat)
		try:
			b=BUL(d)
			if rond is None: rond=b.dro
			self.Ares= b.Ares
		except:
			if rond is None: rond=2*d
			self.Ares=self.d**2*pi/4*.7 #riduco l'area totale
		vars(self).update(locals())

	def passi(self,alpha=0):
		'''calcola i passi minimi per bullonature
alpha in radianti'''
		a1=(4+3*abs(cos(alpha)))*self.d
		a2=4*self.d
		a3f=max([7*self.d,80.])
		if 270< rad2deg(alpha) or rad2deg(alpha)<90:
				a3c=0.
		elif 90<=rad2deg(alpha)<=150:
			a3c=max([(1+6*sin(alpha))*self.d , 4*self.d])
		elif 150<=rad2deg(alpha)<=210:
			a3c=4*self.d
		else:
			a3c=max([(1+6*abs(sin(alpha)))*self.d , 4*self.d])
		if 0<rad2deg(alpha)<180:
			a4f=max([(2+2*sin(alpha))*self.d, 3*self.d])
		else:
						a4f=max([(2-2*sin(alpha))*self.d, 3*self.d])
		a4c=3*self.d
		out=locals()
		del out['self']
		return out

	def fh0k(self,le):
		'''resistenza caratteristica a rifollamento per alpha=0'''
		return .082*(1-.01*self.d)*le.rok

	def fhak(self,le,alpha=0):
		'''resistenza caratteristica a rifollamento per alpha!=0'''
		return self.fh0k(le)/(self.k90(le)*sin(alpha)**2+cos(alpha)**2)

	def k90(self,le):
		'''coefficiente per calcolo fh0k'''
		if 'c' in le.essenza or 'gl' in le.essenza:
			return 1.35+.015*self.d
		elif 'd' in le.essenza:
			.9+.015*self.d
		else:
			return 1.30+.015*self.d

	@property
	def Myrk(self):
		''' momento ultimo resistente del bullone'''
		zb=1.8*self.d**-.4
		return zb*self.mat.fuk*self.d**3/6

	def Kser(self,le):
		return le.rhok**1.5*self.d/20
	
	def nef(self,a1,n=1,alpha=0,*args):
		'angoli in radianti'
		if n==1:return 1
		ne=min([n,n**.9*(a1/13/self.d)**(1./4)])
		return ne+(n-ne)*alpha/(pi/2)

	def Faxrk(self,le,*args):	
		f=[pi*self.d**2/4*self.mat.fyk,
			(self.rond**2-self.d**2)*pi/4*3*le.fc90k]
		return min(f),.25
		
class Spinotto(Bullone):
	def passi(self,alpha=0):
		'''calcola le spaziature minime per gli spinotti
alpha in rad'''
		a1=(3+2*abs(cos(alpha)))*self.d
		a2=3*self.d
		a3f=max([7*self.d,80.])
		if 270< rad2deg(alpha) or rad2deg(alpha)<90:
			a3c=0.
		elif 90<=rad2deg(alpha)<=150:
			a3c=max([(a3f*sin(alpha)), 3*self.d])
		elif 150<=rad2deg(alpha)<=210:
			a3c=3*self.d
		else:
			a3c=max([(a3f*abs(sin(alpha))), 3*self.d])
		if 0<rad2deg(alpha)<180:
			a4f=max([(2+2*sin(alpha))*self.d, 3*self.d])
		else:
						a4f=max([(2-2*sin(alpha))*self.d, 3*self.d])
		a4c=3*self.d
		out=locals()
		del out['self']
		return out

	def Faxrk(self,*args):
		return 0,0


class Vite(Spinotto):#per ora considero solo vidi di diametro gambo > 6 mm
	#in mancanza di dati certi la filettatura interna ha diametro pari a d_ext/1.4
	def __init__(self,mat,d,d_ext,d_int,li,lt,alpha=0):
		'''mat=materiale
d=diametro gambo
d_ext=diametro esterno [con 0 lo setta u d]
d_int=diametro interno [con 0 lo setta a dxet/1.4]
li: lunghezza infissione
lt: lunghezza totale
alpha=angolo infissione rispetto fibre rad'''
		vars(self).update(locals())
		self.lef=self.li-d #tolgo un diametro alla lunghezza efficace
		if not self.d_ext: self.d_ext=self.d
		if not self.d_int:
			self.d_int=self.d_ext/1.4 #valore medio dei passi
		self.d_eff=min([self.d_int*1.1,self.d])

	def faxk(self,le):
		'''resistenza unitaria ad estrazione
le= legno in cui è infissa la vite'''
		return 3.6e-3*le.rok**1.5

	def faxak(self,le):
		'''resistenza unitaria inclinata ad estrazione
le=legno in cui è infissa la vite		'''
		return self.faxk(le)/(sin(self.alpha)**2+1.5*cos(self.alpha)**2)
	
	def Faxak(self,le,*args):
		'''resistenza ad estrazione della vite
le=legno in cui è infissa la vite'''
		return (pi*self.d_ext*self.lef)**.8*self.faxak(le)
	
	def Faxrk(self,le,*args):
		'''per congruenza con gli altri connettori'''
		return self.Faxak(le,*args),1

	def neff_ax(self,n):
		return n**.9

	def Kser(self):
		pass


class Chiodo(Connettore): #da completare
	def __init__(self,mat,d,sez='circ',perforatura=False):
		vars(self).update(locals())

	@property
	def Myrk(self):
		wpl={'circ':self.d**3/6 , 'quad':self.d**3/4}[self.sez]
		xi=1.8*self.d**-.4
		return self.mat.fuk*xi*wpl
	
	def fh0k(self,le):
		if self.d<8:
			if self.preforatura:
				return .082*(1-.01*self.d)*le.rok
			else:
				return .082*le.rok*self.d**-.3
		else:
			return Bullone.fh0k(self,le)
	
	def fhak(self,le,alpha=0): ##### per ora mutuo quella del bullone
		return Bullone.fhak(self,le,alpha)

	def nef(self,a1,*args):
		r=a1/self.d
		if r>=14:
			nef=1
		elif 10<=r<=14:
			nef=.85
		elif 7<=r<10:
			nef=.7
		elif 4<=r<7:
			if self.preforatura:
				nef=.5
			else:	
				nef=0
		else:
			nef=0
		return nef	

	def passi_assali(self):
		#per viti caricate assialmente
		a1=4*self.d
		a2=4*self.d
		a3f=4*self.d
		a3c=2.5*self.d
		a4f=2.5*self.d
		a4c=4*self.d
	
	def passi(self,alpha=0):
		#per i passi delle viti caricate a taglio uso quelli degli spinotti o dei chiodi( che nn ho ancora definito):
		return spinotti.passi(self,alpha)

class Cambretta(Connettore):
	def Faxk(self,*args):
		return 0,0

###############  Fine sezione dei connettori  #############

class Geom_bull:
	def __init__(self,m,n,v1=r_[1,0,0],v2=r_[0,1,0]):
		'''m=elementi lungo l'asse v1
n=elementi lungo l'asse v2
v1=primo asse della bullonatura
v2=secondo asse della bullonatura'''
		vars(self).update(locals())
		self.v1=self.v1/norm(self.v1)
		self.v2=self.v2/norm(self.v2)
		self.e1=0
		self.e2=0
		self.e3c=0
		self.e3f=0
		self.e4c=0
		self.e4f=0
	
	def check(self,elem):
		#valutare rotazione tra bullonatura e fibratura elemento
		alpha=arccos(dot(self.v1,elem.fibra))
		#ottengo i passi minimi degli elementi
		
		#controllo se quelli presenti nella bullonatura sono coeretni



		pass
class Elemento:
	def __init__(self,mat,h,t,fibra=r_[1.,0,0]):
		'''elemento ligneo
mat=materiale
h=altezza
t=profondita\'
fibra=vettore direzione fibra'''
		vars(self).update(locals())	
		self.fibra=self.fibra/norm(self.fibra)

	def fh0k(self,con):
		return con.fh0k(self.mat)
	
	def fhak(self,con,alpha=0):
		return con.fhak(self.mat,alpha)
	
	def F90rk_(self):
		'''resistenza a spacco della giunzione
h: altezza sezione
he: distanza max bordo caricato-bullone T fibre
hm: altezza bullonatura
n: righe di bullonatura (contate T fibre)
lr: larghezza bullonatura (// fibre)
l1: distanza tra serie di giunti
N.B
la verifica e' soddisfatta se Fvd<=0.5 F90rd'''
		fw=min([1+.75*(self.lr+self.l1)/self.h , 2])
		fr=1+1.75*(self.n*self.hm/1000)/(1+self.n*self.hm/1000) #ho considerato solo i bulloni per ora
		F=2*self.t*9*(self.he/(1-(self.he/self.h)**3))**.5*fw*fr
		out=locals()
		del out['self']
		return F,out


class Piastra: #per ora la scrivo, di fatto Ã¨ meglio se importo la piastra da qualcosa di simile fatto per l'acciaio...
	def __init__(self,mat,t,offset=1):
		vars(self).update(locals())

	def sottile(self,con):
		sot=False
		sot=self.offset>.1*con.d
		if not sot:
			sot=self.t<=.5*con.d
		return sot

	def spessa(self,con):
		sp=False 
		if not self.sottile(con):
			sp=self.t>=con.d
		return sp

	def perc_spes(self,con):
		#calcolo in che percentuale può essere considerata spesa la piastra
		return self.t/con.d

#####################  FINE SEZIONE ELEMENTI   ###########################		

class Giunto:#	gli passo elementi, connettori e geometria conlonatura e lui calcola...

	rotture_legno={'a':'Plasticizza sezione a',
			 'b':'Plasticizza sezioen b',
			 'c':'Plasticizzano entrambe le sezioni',
			 'd':'Plasticizza sezione a e connettore nella sezione b',
			 'e':'plasticizza sezione b e connettore nella sezioen a',
			 'f':'plasticizza connettore in entrambe le sezioni',
			 'g':'plasticizzano le sezioni esterne',
			 'h':'plasticizza sezione interna',
			 'j':'plasticizza connettore nella sezione interna',
			 'k':'plasticizza connettore nelle sezioni esterna'}

	rotture_acciaio={'a':'piastra sottile plasticizza solo sezione legno ',
					 'b':'piastra sottile plasticizza connettore',
					 'c':'piastra spessa plasticizza solo legno',
					 'd':'piastra spessa plasticizza sia legno che connettore',
					 'e':'piastra spessa plasticizza solo connettore',
					 'f':'piastra interna plasticizza solo legno',
					 'g':'piastra interna plasticizza solo connettore',
					 'h':'piastra interna plasticizza sia legno e connettore',
					 'j':'piastre esterne plasticizza solo legno',
					 'k':'piastre esterne plasticizza connettore e lengo',
					 'm':'piastre esterne plasticizza solo connettore'}


	def __init__(self,elementi,con,geom=None,tipo=None,npt=None):
		'''elementi=lista con successione elementi lignei utilizzati nel giunto
con=connettore utilizzato
geom=geometria della bullonatura
tipo=successione degli elementi
npt= numero piani di taglio

NB: i calcoli sono condotte considerando elementi omogenei (stessi spessori e stessi materiali per piastre ed elementi)'''

		vars(self).update(locals())
		if not npt: #conto i piani di taglio
			self.npt=len(elementi)-1


	def Fvrk(self,alpha=0):
		'''alpha[rad]è angolo tra fibrature e forza dell' elemento ligneo più esterno'''
		F=[]
		for p in range(self.npt): #per ogni piano di taglio
			#isolo elementi piano di taglio dx e sx...
			elem=self.isola_piano(p)
			#calcolo entrambe le possibilità
			f=[self.calcola_piano(e,alpha) for e in elem]
			f=[i for i in f if i] #escludo i valori nulli
			F.append(min(f))
		return sum([i[0] for i in F]),F

			#valuto resistenza connessione

	def isola_piano(self,n):
		if n==0:
			elem_sx=None
			elem_dx=self.elementi[n:n+3]
		elif n==self.npt-1:
			elem_sx=self.elementi[-3:]
			elem_dx=None
		else:
			elem_sx=[self.elementi[n-1:n+2]]
			elem_dx=[self.elementi[n:n+3]]
		return elem_sx,elem_dx

	def calcola_piano(self,elem,alpha=0):	
		if elem is None: return None 
		seq=[i.__class__.__name__ for i in elem]
		if seq==['Elemento','Elemento']:
			return self.ll(elem,alpha)
		elif seq==['Elemento','Elemento','Elemento']:
			return self.lll(elem,alpha)
		#da qui iniziano i collegamenti legno acciaio 	
		elif seq==['Elemento','Piastra'] or seq== ['Piastra','Elemento']	:
			el=[i for i in elem if isinstance(i,Elemento)][0]
			p=[i for i in elem if isinstance(i,Piastra)][0]
			if p.spessa(self.con):
				return self.lasp(el,alpha)
			elif p.sottile(self.con):
				return self.laso(el,alpha)
			else: #interpolo linearmente
				so=self.laso(el,alpha)
				sp=self.lasp(el,alpha)
				m=so[0]+(sp[0]-so[0])*p.perc_spes(self.con)
				return m,[so[1],sp[1]]

		elif seq==['Elemento','Piastra','Elemento']:
		#considero gli elementi di spessore uguale
			el=[i for i in elem if isinstance(i,Elemento)][0]
			p=[i for i in elem if isinstance(i,Piastra)][0]
			return self.lal(el,alpha)

		elif seq==['Piastra','Elemento','Piastra']:
			el=[i for i in elem if isinstance(i,Elemento)][0]
			p=[i for i in elem if isinstance(i,Piastra)][0]
			if p.spessa(self.con):
				return self.alasp(el,alpha)
			elif p.sottile(self.con):
				return self.alaso(el,alpha)
			else: #interpolo linearmente
				so=self.alaso(el,alpha)
				sp=self.alasp(el,alpha)
				m=so[0]+(sp[0]-so[0])*p.perc_spes(self.con)
				return m,[so[1],sp[1]]
		else:
			raise IOError('sequenza di elementi sconosciuta')
	
	def angolo(self,elem):
		#calcolo l'angolo tra la fibratura del primo elemento ligneo e dell'ultimo
		#recupero il primo element ligneo:
		p=[i for i in self.elementi if isinstance(i,Elemento)][0]
		return arccos(dot(p.fibra,elem.fibra))


		
	#####################  MECCANISMI DI COLLASSO   ####################### 
	#NOTA: la CNR dice esplicitamente che le formule dei collassi valgono per ogni piano di taglio
	def ll(self,elem,alpha=0):
		'''collegamento con 2 elementi lignei'''
		#devo correggere l'angolo aggiungendo quello con il primo elemento
		fh1k=elem[0].fhak(self.con,alpha+self.angolo(elem[0]))
		fh2k=elem[1].fhak(self.con,alpha+self.angolo(elem[1]))
		t1=elem[0].t
		t2=elem[1].t
		d=self.con.d
		beta=fh2k/fh1k
		tir=self.con.Faxrk(elem[0].mat,alpha)
		tir=tir[0]*tir[1]
		Myrk=self.con.Myrk
		F=[[fh1k*t1*d,'a'],
		[fh2k*t2*d,'b'],
		[fh1k*t1*d/(1+beta)*((beta+2*beta**2*(1+t2/t1+(t2/t1)**2)+beta**3*(t2/t1)**2)**.5-beta*(1+t2/t1))+tir,'c'],
		[1.05*fh1k*t1*d/(2+beta)*((2*beta*(1+beta)+(4*beta*(2+beta)*Myrk)/(fh1k*d*t1**2))**.5-beta)+tir/4,'d'],
		[1.05*fh1k*t1*d/(1+2*beta)*((2*beta**2*(1+beta)+(4*beta*(1+2*beta)*Myrk)/(fh1k*d*t2**2))**.5-beta)+tir/4,'e'],
		[1.15*sqrt(2*beta/(1+beta))*sqrt(2*Myrk*fh1k*d)+tir/4 ,'f'] ]
		return min(F)

	def lll(self,elem,alpha=0):
		'''collegamento con 3 elementi lignei'''
		fh1k=elem[0].fhak(self.con,alpha+self.angolo(elem[0]))
		fh2k=elem[1].fhak(self.con,alpha+self.angolo(elem[1]))
		t1=elem[0].t
		t2=elem[1].t
		d=self.con.d
		beta=fh2k/fh1k
		tir=self.con.Faxrk(elem[0].mat,alpha)
		tir=tir[0]*tir[1]
		Myrk=self.con.Myrk
		F=[[fh1k*t1*d,'g'],
		[.5*fh2k*t2*d,'h'],
		[1.05*fh1k*t1*d/(2+beta)*((2*beta*(1+beta)+(4*beta*(2+beta)*Myrk)/(fh1k*d*t1**2))**.5-beta)+tir/4,'j'],
		[1.15*sqrt(2*beta/(1+beta))*sqrt(2*Myrk*fh1k*d)+tir/4 ,'k']]
#		for n,i in enumerate(F): F[n][0]=F[n][0]/2 #divido tutto per 2 xche la formula considera gia 2 piani di taglio, il programma invece calcola tutto per ogni piano di taglio e quindi la calcola 2 volte
		return min(F)

	def laso(self,elem,alpha=0):
		'''resistenza un piano di taglio lamiera sottile'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha+self.angolo(elem))
		tir=self.con.Faxrk(elem.mat,alpha)
		tir=tir[0]*tir[1]
		F=[[.4*fhak*t*self.con.d,'a'],
		   [1.15*(2*self.con.Myrk*fhak*self.con.d)**.5+tir/4,'b']]
		return min(F)

	def lasp(self,elem,alpha=0): #nota bene... ora fhak Ã¨ una proprietÃ  del connettore visto che la sua formulazione varia con esso,per logica (resistenza a rifollamento del legno) posso aggiungere un metodo simile all'elemento ligneo che richiama quella del connettore...)
		'''resistenza un piano di taglio lamiera spessa'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha+self.angolo(elem))
		tir=self.con.Faxrk(elem.mat,alpha)
		tir=tir[0]*tir[1]
		F=[[fhak*t*self.con.d,'c'],
		   [fhak*t*self.con.d*((2+4*self.con.Myrk/(fhak*self.con.d*t**2))**.5-1)+tir/4,'d'],
		   [2.3*sqrt(self.con.Myrk*fhak*self.con.d)+tir/4,'e']]

		return min(F)

	def lal(self,elem,alpha=0):
		'''resistenza 2 piani di taglio lamiera centrata qualsiasi spessore'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha+self.angolo(elem))
		tir=self.con.Faxrk(elem.mat,alpha)
		tir=tir[0]*tir[1]
		F=[[fhak*elem.t*self.con.d,'f'],
		   [fhak*elem.t*self.con.d*((2+4*self.con.Myrk/(fhak*self.con.d*elem.t**2))**.5-1)+tir/4,'g'],
		   [2.3*sqrt(self.con.Myrk*fhak*self.con.d),'h']+tir/4]
#		for n,i in enumerate(F): F[n][0]=F[n][0]/2 #divido tutto per 2 xche la formula considera gia 2 piani di ti
#		for n,i in enumerate(F): F[n][0]=F[n][0]/2 #divido tutto per 2 xche la formula considera gia 2 piani di taaglio, il programma invece calcola tutto per ogni piano di taglio e quindi la calcola 2 volte
		return min(F)

	def alaso(self,elem,alpha=0):
		'''resistenza lamiere esterne sottili'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha+self.angolo(elem))
		tir=self.con.Faxrk(elem.mat,alpha)
		tir=tir[0]*tir[1]
		F=[[.5*fhak*t*self.con.d,'j'],
		   [1.15*sqrt(self.con.Myrk*fhak*self.con.d)+tir/4,'k']]
#		for n,i in enumerate(F): F[n][0]=F[n][0]/2 #divido tutto per 2 xche la formula considera gia 2 piani di taglio, il programma invece calcola tutto per ogni piano di taglio e quindi la calcola 2 volte
		return min(F)

	def alasp(self,elem,alpha=0):
		'''resistenza lamiere esterne spesse'''
		t=elem.t
		fhak=elem.fhak(self.con,alpha+self.angolo(elem))
		tir=self.con.Faxrk(elem.mat,alpha)
		tir=tir[0]*tir[1]
		F=[[.5*fhak*t*self.con.d,'l'],
		   [2.3*sqrt(self.con.Myrk*fhak*self.con.d)+tir/4,'m']]
#		for n,i in enumerate(F): F[n][0]=F[n][0]/2 #divido tutto per 2 xche la formula considera gia 2 piani di taglio, il programma invece calcola tutto per ogni piano di taglio e quindi la calcola 2 volte
		return min(F)


	
