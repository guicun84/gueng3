#! /usr/bin/python
#-*- coding:latin-*-

import os,subprocess
bkdir='../gueng3'

#creo cartella se non esiste
if not os.path.exists(bkdir):
	os.mkdir(bkdir)
# cancello file su gueng3 nuovo
for cur,dirs,files in os.walk(bkdir):
	for f in files:
		os.remove(os.path.join(cur,f))

#controllo tutti i files che siano in utf-8

for curdir,dirs,files in os.walk('.'):
	for f in files:
		if os.path.splitext(f)[1].lower() in ['.py','.pyw']:
			subprocess.call(['vim','+"set nobomb | set fenc=utf8 | wq" ',os.path.join(curdir,f)])
