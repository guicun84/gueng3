#!/usr/bin/python
from . import core
import imp
imp.reload(core)
from .core import Rtf
fi=Rtf('#test.rtf','w')
testo='''\s1 Test di scritura1
\s2 sottotitolo
\s0 testo normale, provo a vedere bene come funzioni.
adesso dovrebbe essere nello stesso paragrafo
ma soprattutto metti le è accentate da unicode?'''

fi.write(testo)
fi.write('\s1 ri-aggiungo il testo')
testo='\n'.join(testo.split('\n')[1:])
fi.write(testo)
fi.close()
print('riapro')
fi=Rtf('#test.rtf','a')
fi.write('\s1 Aggiunta dopo riapertura\n'+testo)
fi.close()


fi=Rtf('#test.rtf','a')
fi.write('\s2 Provo a inserire immagine')
st=Rtf.parseImage('imgtest.png')
fi.write(st)
fi.write('''\s0 spero ci sia l'immagine png qui sopra''')
fi.write(Rtf.parseImage('imgtest.jpg'))
fi.write(''' e qui quella in jpeg''')

st='''test lettera greca a schiaffi \\u963*
test lettere greche per sostituzione di $lettera: sigma $sigma  theta $theta'''
print(Rtf.parseGreek(st))
fi.write(st)


st='ricerca della formula $$a_2^2$$ e lettera greca $alpha e formula con lettera greca: $$ $alpha^2 $$'
fi.write(st)
fi.write('valore caratteristico $$f_yk$$ valore di progetto $$f_yd$$')

fi.write('''\s1 Grafico
\s0 provo ad inserire un grafico di pylab direttamente senza passare da file...''')

x=linspace(0,2*pi)
y=sin(x)
figure(1)
clf()
plot(x,y)
grid(1)
xlabel('x')
ylabel('y')
axhline(0,color='k')
draw()
from io import StringIO
buf=StringIO()
savefig(buf,format='png')
fi.write(fi.parseImage(buf))
fi.close()
