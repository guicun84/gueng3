#-*-coding:utf-8
from io import StringIO as sio
from collections import namedtuple
import binascii
from pylab import Figure,savefig,gcf,figure
try :
	import Image
except: 
	from PIL import Image
import os,re

class Rtf:
	def __init__(self,finame=None,mode='a',template=None):
		assert mode in ['a','w']
		'''finame= nome del file su cui scrivere,none scrive su StringBuffer,
		mode=modo di apertura file se fornito
		template: file rtf di partenza per stili e impostazioni'''
		if mode=='w':
			open(finame,'wb').close()# cancello

		if finame is None:
			self.fi=sio()
		else:
			if mode=='w': self.fi=open(finame,'wb+')#,'utf-8')
			elif mode=='a':self.fi=open(finame,'rb+')#,'utf-8')

		if mode!='a' or finame is None:
			if template is None:
				dire=os.path.dirname(__file__)
				template=os.path.join(dire,'template.rtf')
			self.fi.write(open(template).read()) 

		self.fi.seek(0,2)
#		print 'posizione di partenza',self.fi.tell()
		#opzioni 
		self.opt=namedtuple('Opzioni',['colorSubs'])(False)

	def posiziona(self):
		''' posiziona il puntatore del file in corrispondenza dell'ultima parentesi graffa chiusa'''
		test=False
		n=0
		while not test:
			pos=self.fi.tell()
#			print pos
			self.fi.seek(pos-1)
			st=self.fi.read(1)
			self.fi.seek(pos-1)
#			print pos,repr(st)
			test=st=='}'
			n+=1
			if n==10:raise IOError('qualcosa non torna')

	def write(self,st):
		self.posiziona()
		st=self.parseGraph(st) #ricerco i grafici da inserire
		st=self.parseTable(st) #inserisco le tabelle
		st=self.parseTab(st) #inserisco i tabulatori
		st=self.parseLine(st)
		st=self.parseGreek(st)
		st=self.parseFormula(st)
		st=self.parseEscapeFormats(st,self.opt.colorSubs)
		st=st+'\par }'
		self.fi.write(bytearray(st))

	def close(self):
		self.fi.close()

	def flush(self):
		self.fi.flush()

	@staticmethod
	def parseGraph(st):
		t=re.findall(r'\$\$\$gcf\d*\$\$',st)
		if t:
			t=[i for i in set(t)]
			imgs=[Rtf.parseImage(i) for i in t]
			t=[i.replace('$','\$') for i in t]
			for i,j in zip(t,imgs):
				st=re.sub(i,j,st)
		return st

	@staticmethod
	def parseLine(st):
		'''aggiungo le linee e i paragrafi'''
		st= re.sub('\\n(\\\\s\d+)','\\n\\\\pard\\1',st)
		st= re.sub('\\n','\\line\n',st)
		st= re.sub('\\\\row}\\\\line\\n','\\\\row}\n',st)
		st= re.sub('\\\\line\\n\\\\pard','\\\\par\\n\\\\pard',st)
		st= re.sub('\\\\line\\n\\\\par','\\n\\\\par',st)
		st= re.sub('(\\\\tab .*})\\\\line\\n','\\1\\par\\n',st)
		return st

	@staticmethod
	def parseImage(dat,wg=15,hg=None):
		'''dat può essere il nome di un file png oppure una istanza StringIO su cui è salvata l'immagine '''
		if isinstance(dat,sio):
			dat.seek(0)
			img=Image.open(dat)
			dat.seek(0)
			val=dat.getvalue()
		if isinstance(dat,str): 
			if re.match('\$\$\$gcf\d*\$\$',dat):
				n=re.search('\d+',dat)
				if not n:
					dat=gcf()
				else:
					figure(int(n.group()))
					dat=gcf()
			elif os.path.exists(dat): #nel caso in cui sia il nome di un file caso semplice
				val=open(dat,'rb').read()
				img=Image.open(dat)

		if type(dat)==Figure :		
			s=sio()
			dat.savefig(s)
			s.seek(0)
			img=Image.open(s).convert('RGB')
			s=sio()
			img.save(s,format='png')
			s.seek(0)
			val=s.getvalue()
		w,h=img.size
		r=float(w)/h
		if wg is None:	wg=hg*r
		wg=int(Rtf.cm2twips(wg))
		hg=int(wg/r)
#			data= ''.join([('%s'%(hex(ord(i))[2:])).rjust(2,'0') for i in open(dat,'rb').read()])
		data=binascii.hexlify(val)
		return '{{\\pict\picw{w}\\pich{h}\\picwgoal{wg}\\pichgoal{hg}\n{data}}}'.format(**locals())

	
	greek={ 
'Alpha':913,
'Beta':914,
'Gamma':915,
'Delta':916,
'Epsilon':917,
'Zeta':918,
'Eta':919,
'Theta':920,
'Iota ':921,
'Kappa':922,
'Lambda':923,
'Mi':924,
'Mu':924,
'Ni':925,
'Xi':926,
'Omicron':927,
'Pi':928,
'Rho':929,
'Sigma':931,
'Tau':932,
'Upsilon':933,
'Phi':934,
'Chi':935,
'Psi':936,
'Omega':937,
'alpha':945,
'beta':946,
'gamma':947,
'delta':948,
'epsilon':949,
'zeta':950,
'eta':951,
'theta':952,
'iota ':953,
'kappa':954,
'lambda':955,
'mi':956,
'mu':956,
'ni':957,
'xi':958,
'omicron':959,
'pi':960,
'rho':961,
'sigma':963,
'tau':964,
'upsilon':965,
'phi':966,
'chi':967,
'psi':968,
'omega':969}

	@staticmethod
#	def parseGreek(st):
#		'''in una stringa ricerca le lettere greche nella formula $lettera e sostituisce i caratteri unicode relativi alla corretta visualizzazione'''
#		g=re.findall('\$([^\$ ]+)',st)
#		if not g: return st
#		g=[i for i in g if i in Rtf.greek.keys()]
#		for i in g:
#			st=re.sub('\$'+i, "\u{}*".format(Rtf.greek[i]),st)
#		return st
	def parseGreek(st):
		m=[]
		for i in list(Rtf.greek.keys()):
			w='${}'.format(i)
			m.extend([w]*st.count(w))
		for i in m:
			st=st.replace(i,'\\u{}*'.format(Rtf.greek[i[1:]]))
		return st
			

	@staticmethod
	def parseFormula(st):
		'''data la stringa ricerca le formule (semplici apici e pedici) e el formatta correttamente
		la formula è racchiusa tra "$$ formula $$", ed è possibile formattare correttamente apici, pedici'''
		f=re.findall('\$\$([^\$]+?)\$\$',st)
		if not f: return st
		for i in f:
			s=''
			ingr=False
			for j in i:
				if j=='_':
					if ingr: s+='}'
					s+='{\sub '
					ingr=True
				elif j=='^':
					if ingr: s+='}'
					s+='{\super '
					ingr=True
				elif j==' ':
					if ingr:
						s+='}'
						ingr=False
					else: s+=' '
				else: s+=j
			if ingr:
				s+='}'
#			print f,s
			st=st.replace('$${}$$'.format(i),s)
		return st

	@staticmethod
	def parseEscapeFormats(st,subs=False):
		'''data una stringa elabora se presenti i caratteri speciali di formattazione del testo usati per l'output da terminale
		to do (sostituisce i colori... ;) )'''
		grp=re.findall('(\x1b[^m]+m)',st)
		if not grp: return st
#		print repr(st)
#		print grp
		if subs: #sostituisco colori
			pass
		else:
			for i in grp:
				st=st.replace(i,'')
#		print repr(st)
		return st

	@staticmethod
	def cm2twips(cm):
		'converte da cm a twips'
		return float(cm)/2.54*1440

	@staticmethod
	def parseTable(st,maxWidth=(3,17.5)):
		'''aggiunge comandi per tabella: deve risultare nella forma
		$$table
		riga1
		riga2...'''
		if not '\t' in st: return st
		li=st.split('\n')
		so=''
		testUseTable=False
		testTab=False
		for i in li:
			if '$$table' in i:
				testUseTable=True
				continue
			if '\t' not in i:
				if testTab:
					if i=='':
						pass
#						so+='\par '
					else:
						so+=' '+i+'\n'
					testTab=False
					testUseTable=False
				else:
					so+=i+'\n'

			elif '\t' in i and testUseTable:
				if not testTab:
					so+='\pard '
					testTab=True
				i=i.split('\t')
				l=min([maxWidth[1]/len(i),maxWidth[0]])
				l=int(Rtf.cm2twips(l))
				so+='{\\trowd '
				for n in  range(len(i)): so+='\\cellx{} '.format(l*(n+1))
				for j in i:so+= '{}\intbl\cell '.format(j)
				so+='\\row}\n'
			else: so+=i+'\n'
		return so[:-1] #l'ultimo a capo lo tolgo se no è aggiunto
	
	@staticmethod
	def parseTab(st,maxWidth=(3,17.5)):
		'''aggiunge comandi per tabella'''
		if not '\t' in st: return st
		li=st.split('\n')
		so=''
		testTab=False
		for i in li:
			if '\t' not in i:
				so+=i+'\n'
				if testTab:
#					so+='\par '
					testTab=False

			else:
				if not testTab:
					so+='\pard '
					testTab=True
				so+='{'
				i=i.split('\t')
				l=min([maxWidth[1]/len(i),maxWidth[0]])
				l=int(Rtf.cm2twips(l))
				for n in  range(len(i)): so+='\\tx{} '.format(l*(n))
				so+= '\\tab '.join(i)
				so+='}\n'
		return so[:-1] #tolgo l'ultimo a capo aggiunto


