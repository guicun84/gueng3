from .travi import vectorizemethod,Trave,TraveAppoggiata,TraveIncastrata,TraveIncastroIncastro,TraveIncastroCerniera

from .travi_poly import  Trave as TraveP , TraveAppoggiata as TraveAppoggiataP , TraveIncastrata as TraveIncastrataP , TraveIncastroIncastro as TraveIncastroIncastroP , TraveIncastroCerniera as TraveIncastroCernieraP
