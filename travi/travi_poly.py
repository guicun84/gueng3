from numpy import *
from scipy.linalg import solve
import pylab as pl

class Trave:
	def __init__(self,l,q=r_[0,0],E=1,J=1,lc=1):
		dat={i:j for i,j in locals().items() if i!='self'}
		self.aggiorna(**dat)

	def aggiorna(self,**kargs):
		if 'q' in kargs.keys():
			self.q_=-atleast_1d(kargs['q']).astype(float)*kargs['lc']
		del kargs['q']
		vars(self).update(kargs)
		self.integra()

	def integra(self):
		'''
q=Ax+B
T=A/2*x^2 + B*x + C
M=A/6*x^3 + B/2*x^2 + C*x +D
chi=(A/6*x^3 + B/2*x^2 + C*x +D)/EJ
phi=(A/24*x^4 + B/6*x^3 + C/2*x^2 +D*x+E)/EJ
v=(A/120*x^5 + B/24*x^4 + C/6*x^3 +D/2*x^2 + E*x +F)/EJ
'''
		pass

	@property
	def poly(self):
		return dict(
			q=self.q_,
			t=self.T_,
			m=self.M_,
			c=self.chi_,
			p=self.phi_,
			v=self.v_,
		)

	def sol(self,n=3,multl=1,x=None):
		'''n=numero di punti da considerare
		multl= percentuale di trave su cui posizionare gli n punti (1=tutta la trave, 0,5 mezza...)
		x=posizione dei punti, se fornita, altrimenti vengono calcolati in base a n e multl
		'''
		if x is None:
			x=linspace(0,self.l*multl,n)
			x=r_[x,self.cerca_T()[1],self.cerca_M()[1]]
			x=r_[[i for i in set(x)]]
			x.sort()
		s=zeros((len(x),6))
		s[:,1]=polyval(self.T_,x)
		s[:,5]=polyval(self.M_,x)
		return s




	def __calcola(self,x,poly):
		return polyval(self.poly[poly],x)

	def q(self,x):
		return self.__calcola(x,'q')
	def T(self,x):
		return self.__calcola(x,'t')
	def M(self,x):
		return self.__calcola(x,'m')
	def chi(self,x):
		return self.__calcola(x,'c')
	def phi(self,x):
		return self.__calcola(x,'p')
	def v(self,x):
		return self.__calcola(x,'v')

	def __cerca(self,f,d,fun=None):
		if fun is None:fun=lambda x:max(abs(x))
		x=roots(self.poly[d])
		x=r_[[i for i in x if 0<=i<=self.l],0,self.l]
		val=polyval(self.poly[f],x)
		tg=fun(val)
		x=x[abs(val)==abs(tg)][0]
		return tg,x

	def cerca_T(self,fun=None):
		return self.__cerca('t','q',fun)
	def cerca_M(self,fun=None):
		return self.__cerca('m','t',fun)
	def cerca_chi(self,fun=None):
		return self.__cerca('c','t',fun)
	def cerca_phi(self,fun=None):
		return self.__cerca('p','c',fun)
	def cerca_v(self,fun=None):
		return self.__cerca('v','p',fun)

		

	def plot(self,st='qtmpv'):
		x=linspace(0,self.l)
		titoli=dict(
			q='Carico',
			t='Taglio',
			m='Momento',
			c='Curvatura',
			p='Rotazione',
			v='Spostamento',
		)

			
		for i in st:
			pl.figure()
			pl.title(titoli[i])
			pl.plot(x,polyval(self.poly[i],x))
			pl.axhline(0,color='k')
			pl.grid(1)
			if i=='m':
				pl.ylim(pl.ylim()[-1::-1])

class TraveAppoggiata(Trave):
	def integra(self):
		self.M_=polyint(self.q_,2)
		self.M_[-2]=-polyval(self.M_,self.l)/self.l
		self.T_=polyder(self.M_)
		self.chi_=self.M_/self.E/self.J
		self.v_=polyint(self.chi_,2)
		self.v_[-2]=-polyval(self.v_,self.l)/self.l
		self.phi_=polyder(self.v_)

	
class TraveIncastrata(Trave):
	'''
q=Ax+B
T=A/2*x^2 + B*x + C
M=A/6*x^3 + B/2*x^2 + C*x +D
chi=(A/6*x^3 + B/2*x^2 + C*x +D)/kchi
phi=(A/24*x^4 + B/6*x^3 + C/2*x^2 +D*x+0)/kchi
v=(A/120*x^5 + B/24*x^4 + C/6*x^3 +D/2*x^2 + 0*x +0)/kchi
'''
	def integra(self):
		self.T_=polyint(self.q_)
		self.T_[-1]=-polyval(self.T_,self.l)
		self.M_=polyint(self.T_)
		self.M_[-1]=-polyval(self.M_,self.l)
		self.chi_=self.M_/self.E/self.J
		self.phi_=polyint(self.chi_,)
		self.v_=polyint(self.phi_,)

class TraveIncastroIncastro(Trave):
	'''
q=Ax+B
T=A/2*x^2 + B*x + C
M=A/6*x^3 + B/2*x^2 + C*x +D
chi=(A/6*x^3 + B/2*x^2 + C*x +D)/EJ
phi=(A/24*x^4 + B/6*x^3 + C/2*x^2 +D*x+0)/EJ phi(l)=0
v=(A/120*x^5 + B/24*x^4 + C/6*x^3 +D/2*x^2 + 0*x +0)/EJ v(l)=0
'''
	def integra(self):
		l=self.l
		A=r_[[
			[l**2/2,l],
			[l**3/6,l**2/2]
		]]
		b=-r_[
			self.q_[0]*l**4/24 + self.q_[1]*l**3/6,
			self.q_[0]*l**5/120 + self.q_[1]*l**4/24,
		]
		kchi=self.E*self.J
		self.v_=polyint(self.q_,4)
		self.v_[2:4]=solve(A,b)/r_[6,2]
		self.v_/=kchi
		self.phi_=polyder(self.v_)
		self.chi_=polyder(self.phi_)
		self.M_=self.chi_*kchi
		self.T_=polyder(self.M_)


class TraveIncastroCerniera(Trave):
	'''
q=Ax+B
T=A/2*x^2 + B*x + C
M=A/6*x^3 + B/2*x^2 + C*x +D M(l)=0
chi=(A/6*x^3 + B/2*x^2 + C*x +D)/EJ
phi=(A/24*x^4 + B/6*x^3 + C/2*x^2 +D*x+0)/EJ
v=(A/120*x^5 + B/24*x^4 + C/6*x^3 +D/2*x^2 + 0*x +0)/EJ v(l)=0
'''
	def integra(self):
		l=self.l
		kchi=self.E*self.J
		A=r_[[
			[l,1.],
			r_[l**3./6,l**2/2.]
			]]

		b=-r_[
			self.q_[0]*l**3/6. + self.q_[1]*l**2/2.,
			self.q_[0]*l**5/120. + self.q_[1]*l**4/24.,
			]
		print(A,b)
		self.v_=polyint(self.q_,4)
		self.v_[2:4]=solve(A,b)/r_[6,2.]
		self.v_/=kchi
		self.phi_=polyder(self.v_)
		self.chi_=polyder(self.phi_)
		self.M_=self.chi_*kchi
		self.T_=polyder(self.M_)
