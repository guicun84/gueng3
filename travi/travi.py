from numpy import *
from scipy.integrate import cumtrapz
from scipy.optimize import root
from scipy import interp
import pylab as pl

def vectorizemethod(fun):
	'decoratore per vettorializzare i metodi delle classi'
	ff=vectorize(fun,otypes=[float])
	def wrapper(self,*args):
		return ff(self,*args)
	return wrapper

class Trave:
	def __init__(self,l,n=101,q0=0,E=1,J=1,lc=1):
		vars(self).update(locals())
		self.aggiorna()

	def aggiorna(self):
		self.x=linspace(0,self.l,self.n)
		self.q=self.calcq(self.x)
		self.T=-cumtrapz(self.q,self.x,initial=0)
		self.M=cumtrapz(self.T,self.x,initial=0)
		self.chi=self.M2chi(self.M)
		self.phi=-cumtrapz(self.chi,self.x,initial=0)
		self.v=cumtrapz(self.phi,self.x,initial=0)
		self.boundary()

	@vectorizemethod
	def M2chi(self,M):
		return M/self.E/self.J
		
	@vectorizemethod
	def calcq(self,x):
		return self.q0*self.lc

	def boundary(self):
		pass

	def v_(self,x):
		return interp(x,self.x,self.v)
	def phi_(self,x):
		return interp(x,self.x,self.phi)
	def M_(self,x):
		return interp(x,self.x,self.M)
	def T_(self,x):
		return interp(x,self.x,self.T)
	def q_(self,x):
		return interp(x,self.x,self.q)

	def sol(self,x=None):
		if x is None:x=self.x
		out=zeros((len(x),6))
		out[:,1]=self.T_(x)
		out[:,5]=self.M_(x)
		return out


	def plot(self,st='qtmpv'):
		st=st.lower()
		dat=dict(
				q=[self.q,'Carico lineare'],
				t=[self.T,'Taglio'],
				m=[self.M,'Momento flettente'],
				p=[self.phi,'Rotazione'],
				v=[self.v,'spostamento'],
			)
		for g in st:
			y,tit=dat[g]
			pl.figure()
			pl.grid(1)
			pl.plot(self.x,y)
			pl.title(tit)
			if g in ['m','v']:
				pl.ylim(pl.ylim()[-1::-1])
		pl.show()

class TraveAppoggiata(Trave):
	def boundary(self):
		x0=-r_[self.T[-1]/2,self.M[-1]/2]
		def zerome(x):
			t0,phi0=x0*x
			return r_[
				self.v[-1]-(t0/6*self.l**3)/self.E/self.J+phi0*self.l,
				self.M[-1]+t0*self.l  
				]
		x=root(zerome,r_[1.,1])
		if not x.success:
			print(x)
		t0,phi0=x.x*x0
		self.v+=(-t0/6*self.x**3 )/self.E/self.J+phi0*self.x
		self.phi+=(-t0/2*self.x**2 )/self.E/self.J+phi0
		self.M+=t0*self.x 
		self.T+=t0
	
class TraveIncastrata(Trave):
	def boundary(self):
		x0=-r_[self.T[-1]/2,self.M[-1]/2]
		def zerome(x):
			t0,m0=x*x0
			return r_[
				self.T[-1]+t0,
				self.M[-1]+t0*self.l+m0
				]
		self.x0=root(zerome,r_[1,1.])
		if not self.x0.success:
			print(x)
		t0,m0=self.x0.x*x0
		self.v+=(-t0/6*self.x**3 - m0/2*self.x**2)/self.E/self.J
		self.phi+=(-t0/2*self.x**2 - m0*self.x)/self.E/self.J
		self.M+=t0*self.x + m0
		self.T+=t0
	
class TraveIncastroIncastro(Trave):
	def boundary(self):
		x0=-r_[self.T[-1]/2,self.M[-1]/2]
		def zerome(x):
			t0,m0=x*x0
			return r_[
				self.v[-1]+(-t0/6*self.l**3 - m0/2*self.l**2)/self.E/self.J,
				self.phi[-1]+(-t0/2*self.l**2 - m0*self.l)/self.E/self.J,
				]
		self.x0=root(zerome,r_[1.,1])
		if not self.x0.success:
			print(self.x0)
		t0,m0=self.x0.x*x0
		self.v+=(-t0/6*self.x**3 - m0/2*self.x**2)/self.E/self.J
		self.phi+=(-t0/2*self.x**2 - m0*self.x)/self.E/self.J
		self.M+=t0*self.x + m0
		self.T+=t0


class TraveIncastroCerniera(Trave):
	def boundary(self):
		x0=-r_[self.T[-1]/2,self.M[-1]/2]
		def zerome(x):
			t0,m0=x*x0
			return r_[
				self.v[-1]+(-t0/6*self.l**3 - m0/2*self.l**2)/self.E/self.J,
				self.M[-1] + t0*self.l + m0
				]
		self.x0=root(zerome,r_[1.,1])
		if not self.x0.success:
			print(self.x0)
		t0,m0=self.x0.x*x0
		self.v+=(-t0/6*self.x**3 - m0/2*self.x**2)/self.E/self.J
		self.phi+=(-t0/2*self.x**2 - m0*self.x)/self.E/self.J
		self.M+=t0*self.x + m0
		self.T+=t0


'''
q
T=-iq+t0
M=-iiq +t0*x +M0
phi=iiiq - t0/2*x**2 -M0*x)/E/J +phi0
v=iiiiq -t0/6*x**3 -M0/2*x**2)/E/J + phi0*x + v0
'''
