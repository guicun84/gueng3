#-*-coding:utf-8-*-

from scipy import *
from scipy.optimize import minimize
from .sezione import Sezione
from .armature import A2d
from .materiali import Calcestruzzo
import pandas as pa

def T_rd (sez,phi_st,n_b,s,P,A,t=None,c=30,ac=None,theta=None):
	'''resistenza a torsione della trave DM2008 4.1.[27,28,29]
	sez=oggetto Sezione
	phi_st=diametro staffe
	n_b=numero bracci staffe
	s=passo staffe
	P=perimetro sezione (oppure r_[b,h])
	A=area racchiusa dalla fibra media del profilo periferico (staffa ;) )
	t=None (spessore sezione cava se None lo calcola come da DM per sez piena)
	c=Copriferro
	ac=Acciaio
	theta=angolo inclinazione'''
	As=phi_st**2*pi/4*n_b 
	Al=sum([i.As for i in sez.barre])
	if A is None:
		if hasattr(P,'__len__'):
			A=(r_[P]-c).prod()
		else:
			A=sum([i.A for i in sez.objects if isinstance(i,Sezione)])
	if hasattr(P,'__len__'):
		P=2*sum(P)
	um=P-8*c
	if not theta:
		theta=((Al/um)/(As/s))**.5
		if 1/tan(theta)<1:
			theta=arctan(1)
		if 1/tan(theta)>2.5:
			theta=arctan(1/2.5)
	if t is None: t=A/um
	fcd_=sez.ca[0].mat.fcd/2 #nella norma indicato come f'cd
	Trcd= 2*A*t*fcd_ * 1/tan(theta)*(1+(1/tan(theta))**2)
	Trsd=2*A*As/s*sez.barre[0].mat.fyd/tan(theta)
	Trld=2*A*Al / um*sez.barre[0].mat.fyd*tan(theta)
	return min([Trcd,Trsd,Trld]),locals()

def V_rcd(cls,b,d,Asl,N=0,gamma_c=1.6):
	''''resistenza a taglio sezione non armata a taglio DM2008 4.1.13:
	cls=calcestruzzo utilizzato o fck direttamente
	b=base mm
	d=altezza utile mm
	Asl= area armatura longitudinale mm^2
	N=compressione sezione [N]
	gamma_c : coef. parziale calcestruzzo
	
	Ritorna
	Vrd taglio resistente
	dizionario con valori intermedi e stringa output  [st]'''
	if isinstance (cls,Calcestruzzo): fck=cls.fck/cls.FC
	else: fck=cls
	k=min((1+(200/d)**.5,2))
	vmin=0.035*k**(3/2)*fck**.5
	Ac=b*d
	rol=Asl/Ac
	sigmacp=N/Ac
	Vrd1=(.18*k*(100*rol*fck)**(1/3)/gamma_c+0.15*sigmacp)*Ac
	Vrd2=(vmin+0.15*sigmacp)*Ac
	test=Vrd1>=Vrd2
	st='''b={b:.4g} mm , d={d:.4g} mm, A~sl~={Asl:.4g} mm^2^
f~ck~={fck:.4g} MPa , N~ed~={N:.4g} N , &gamma;~c~={gamma_c:.4g}
k={k:.4g}
v~min~={vmin:.4g} MPa
&rho;~l~={rol:.4g} rapporto geometrico di armatura longitudinale
&sigma;~cp~={sigmacp:.4g} MPa tensione media di compressione
V~rd min~={Vrd2:.4g} N taglio minimo
V~rd~={Vrd1:.4g} N taglio resistente
test taglio minimo: V~rd min~ <= V~rd~ :{tst}'''.format(tst={True:'Verificato' ,False:'NON verificato'}[test],**locals())
	return Vrd1,locals()

class Vrcd:
	def __init__(self,cls,b,d,Asl,N=0,gamma_c=1.6):
		''''resistenza a taglio sezione non armata a taglio DM2008 4.1.13:
		cls=calcestruzzo utilizzato o fck direttamente
		b=base mm
		d=altezza utile mm
		Asl= area armatura longitudinale mm^2
		N=compressione sezione [N]
		gamma_c : coef. parziale calcestruzzo

		proprietà Vrcd = resistenza a taglio [equivale a Vrd1]
		'''
		vars(self).update(locals())
		if isinstance (cls,Calcestruzzo): self.fck=cls.fck/cls.FC
		else: self.fck=cls
		self.calcola()
	
	def calcola(self):
		self.k=min((1+(200/self.d)**.5,2))
		self.vmin=0.035*self.k**(3/2)*self.fck**.5
		self.Ac=self.b*self.d
		self.rol=self.Asl/self.Ac
		self.sigmacp=self.N/self.Ac
		self.Vrd1=(.18*self.k*(100*self.rol*self.fck)**(1/3)/self.gamma_c+0.15*self.sigmacp)*self.Ac
		self.Vrd2=(self.vmin+0.15*self.sigmacp)*self.Ac
		self.test=self.Vrd1>=self.Vrd2

	@property
	def Vrcd(self):
		return self.Vrd1
	
	
	def __str__(self):
		st='''b={b:.4g} mm , d={d:.4g} mm, A~sl~={Asl:.4g} mm^2^
f~ck~={fck:.4g} MPa , N~ed~={N:.4g} N , &gamma;~c~={gamma_c:.4g}
k={k:.4g}
v~min~={vmin:.4g} MPa
&rho;~l~={rol:.4g} rapporto geometrico di armatura longitudinale
&sigma;~cp~={sigmacp:.4g} MPa tensione media di compressione
V~rd min~={Vrd2:.4g} N taglio minimo
V~rd~={Vrd1:.4g} N taglio resistente
test taglio minimo: V~rd min~ <= V~rd~ :{tst}'''.format(tst={True:'Verificato' ,False:'NON verificato'}[self.test],**vars(self))
		return st



def V_rd (sez,phi_st,n_b,s,b,h,c=30,N=0,theta=pi/4,ac=None,alpha=pi/2):
	'''Resistenza a taglio della sezione armata a taglio DM2008 4.1.[18,19,20]
	sez=oggetto Sezione
	phi_st=diametro staffe
	n_b=numero bracci staffe
	s=passo staffe
	b=base sezione
	h=altezza sezione
	c=Copriferro
	ac=Acciaio
	theta=angolo inclinazione staffe rispetto asse'''
	Asw=phi_st**2*pi/4*n_b #area staffe
	d=h-c #altezza utile
	if theta is None:
		cotgte=s/d
		if cotgte>2.5: 
			raise ValueError('Cotan(theta)={}>2.5 puntone troppo orizzontale'.format(cotgte))
		cotgte=max([1,min([2.5,cotgte])])
		theta=arctan(1/cotgte)
	cotgte=1/tan(theta)
	Vrsd=.9*d*Asw/s*sez.barre[0].mat.fyd*(1/tan(alpha)+1/tan(theta))*sin(alpha)
	scp=N/sum([i.A for i in sez.ca]) #tensione del calcestruzzo
	fcd=sez.ca[0].mat.fcd
	if scp<=0: alphac=1
	elif 0<scp<=.25*fcd : alphac=1+scp/fcd
	elif .25*fcd <scp <= .5*fcd : alphac=1.25
	elif .5*fcd< scp <=fcd : alphac=2.5*(1-scp/fcd)
	else: 
		raise ValueError('Sezione troppo compressa')
	Vrcd=.9*d*b*alphac*.5*sez.ca[0].mat.fcd*(1/tan(alpha)+1/tan(theta))/(1+(1/tan(theta))**2)
	Vrd=min([Vrsd,Vrcd])
	del i
	stOut='''Resistenza a Taglio
Diametro staffe: {phi_st} mm
numero bracci: {n_b}
passo staffe:{s} mm
base resistente: {b:.1f} mm
altezza sezione: {h:.1f} mm
copriferro: {c} mm
&theta; :{theta_g:.2f} ° inclinazione puntoni rispetto asse
f~yd~ ={fyd} resistenza acciaio

A~sw~= {Asw} area staffe
d={d} mm altezza utile
V~r sd~= {Vrsd:.1f} N resistenza delle staffe

&sigma;~cp~ ={scp:.4g} MPa tensione per compressione 
f~cd~= {fcd:.4g} MPa reisitenza calcestruzzo
&alpha;~c~ ={alphac:.4g}
V~r cd~= {Vrcd:.4g} N resistenza calcestruzzo
V_rd~={Vrd:.4g} Resistenza a taglio'''.format(theta_g=rad2deg(theta),fyd=sez.barre[0].mat.fyd,**locals())

	return Vrd,locals()


class Vrd:
	'''calcolo della resistenza a taglio di una sezione armata rettangolare'''
	def __init__(self,b,d,fcd,Asw=None,s=None,fyd=450/1.15,alpha=90,sc=0,theta=45,errors=dict()):
		'''fcd resistenza cls
		b=base calcestruzzo
		d=altezza utile sezione
		fyd=resistenza acciaio
		Asw=area totale staffatura
		s=passo staffe
		alpha=inclinazione staffe
		theta=None calcola inclinazione puntone oppure angolo in gradi
		sc=tensione di compressione dovuta a sforzo normale
		errors=dizionario per definire comportamento in caso di incongruenze sui limiti dei valori
			valori raise warn skip
			keys cott'''
		vars(self).update(locals())
		self.errors=dict(cott='raise')
		self.errors.update(errors)
		self.alphar=deg2rad(alpha)
		self.ni=.5
		if theta is None:
			self.optTheta()
	@property
	def thetar(self): 
		'''inclinazione del puntone in rad'''
		if self.theta is  None:
#			self.theta=rad2deg(sqrt(arcsin(self.Asw*self.fyd/(self.b*self.s*self.alphac*self.ni*self.fcd))))
			self.theta=rad2deg(sqrt(self.alphac*self.ni*self.fcd*self.b/(self.Asw/self.s*self.fyd)-1))
		return deg2rad(self.theta)
	@property
	def cott(self):
		'''cotangente angolo di inclinazione puntone'''
		val=1/tan(self.thetar)
		if val>2.5: return 2.5
		if val<1:
			ercode= self.errors['cott']
			if ercode=='raise':
				raise ValueError('cot(theta)={}<1 non ammissibile'.format(val))
			elif ercode=='warn':
				print('cot(theta)={}<1 non ammissibile ritorno 1'.format(val))
				return 1
			else: return 1
		return val
	@property
	def alphac(self):
		'''coefficiente maggiorativo in caso di sforzo di compressione'''
		sc=self.sc
		fcd=self.fcd
		if sc<=0:return 1
		elif 0<sc<0.25*fcd: return 1+sc/fcd
		elif .25*fcd<=sc<.5*fcd: return 1.25
		elif .5*fcd<=sc<fcd: return 2.5*(1-sc/fcd)

	@property
	def Vrsd(self):
		'''resistenza a taglio lato staffe'''
		return 0.9*self.fyd*self.Asw*self.d/self.s*(1/tan(self.alphar) + self.cott)*sin(self.alphar)
	@property	
	def Vrcd(self):
		'''resistenza a taglio lato cls'''
		return 0.9*self.d*self.b*self.alphac*self.ni*self.fcd*(1/tan(self.alphar)+ self.cott)/(1+self.cott**2)

	@property
	def Vrd(self):
		return min((self.Vrsd,self.Vrcd))

	def optTheta(self):
		bkerr=self.errors
		self.errors=dict(cott='skip')
		def fun(theta):
			self.theta=theta
			return -self.Vrd
		x=minimize(fun,33,constraints=[
			dict(type='ineq',fun=lambda x:x[0]-rad2deg(arctan(1/2.5))),
			dict(type='ineq',fun=lambda x:45-x[0]),
			])
#		if not x.success:
#			print('ottimizzazione di theta')
#			print(x)
		self.theta=x.x[0]
		self.errors=bkerr

	def predimensiona(self,T,nb,cumax=.95):
		'''predimensiona armatura
		T=taglio a SLU
		nb=numero bracci
		cumax=massimo coefficiente d'uso:
		ritorna
		risultato di minimizzazione x=Asw,s
		armatura tipo da A2d.cerca'''
		def funmin(xxx_todo_changeme):
			(A,s) = xxx_todo_changeme
			return -s
		def funz(xxx_todo_changeme1,cumax=cumax): #coefficiente d'uso massimo imposto
			(A,s) = xxx_todo_changeme1
			tr=Vrd(self.b,self.d,self.fcd,A,s,self.fyd,self.alpha,self.sc)
			try:
				vrsd=tr.Vrsd
				val=T/vrsd-cumax
			except ValueError:
				vrsd=0
				val=1e4
			return val
		def funz2(xxx_todo_changeme2): #passo minimo 5 cm 
			(A,s) = xxx_todo_changeme2
			return s-50
		def funz3(xxx_todo_changeme3): #passo massimo 0.8*d
			(A,s) = xxx_todo_changeme3
			return 0.8*self.d-s
		def funz4(xxx_todo_changeme4): #diametro minimo %%c8
			(A,s) = xxx_todo_changeme4
			return A/nb-50

		val=minimize(funmin,
					(50,.5*self.d),
					method='slsqp',
					constraints=[{'type':'eq','fun':funz}, #controllo coefficiente sicurezza
							     {'type':'ineq','fun':funz2},#controllo passo minimo	
							     {'type':'ineq','fun':funz3}, #controllo passo massimo
							     #{'type':'ineq','fun':funz4}, 
								]
					)
		a=A2d.cerca(val.x[0],nd=1,n=nb,limit=1)[0]
		return val,a

	def __str__(self):
		return '''b={b:.4g} , d={d:.4g}  
A~sw~={asw:.4g} mm^2^ , s={s:.4g} mm , &alpha;={alpha:.4g} deg  
f~cd~={fcd:.4g} Mpa , f~yd~={fyd:.4g} MPa  
&nu;={ni:.4g} , &alpha;~c~={ac:.4g}  
&theta;={theta:.4g} --> cotg(&theta;)={cotg:.4g}  
V~rsd~={vrsd:.4g} N , V~Rcd~={vrcd:.4g} N  
**V~Rd~={vrd:.4g} N**'''.format(b=self.b,
							d=self.d,
							ni=self.ni,
							theta=self.theta,
							cotg=self.cott,
							ac=self.alphac,
							vrsd=self.Vrsd,
							vrcd=self.Vrcd,
							vrd=self.Vrd,
							asw=self.Asw,
							s=self.s,
							alpha=self.alpha,
							fcd=self.fcd,
							fyd=self.fyd)

fc=12,16,20,25,30,35,40,45,50
trd1=0.18,.22,.26,.30,.34,.37,.41,.44,.48
trd2=0.17,.21,.24,.28,.32,.35,.38,.41,.45
Prospetto_EC2_4_8=pa.DataFrame([[1.5,i,j] for i,j in zip(fc,trd1)],columns='gammac fck taurd'.split())
Prospetto_EC2_4_8=Prospetto_EC2_4_8.append(pa.DataFrame([[1.6,i,j] for i,j in zip(fc,trd2)],columns='gammac fck taurd'.split()))

del fc,trd1,trd2

class Bprd:
	taurd=Prospetto_EC2_4_8
	def __init__ (self,fck,p,d,rho1x,rho1y,beta=1.15,gammac=1.6):
		'''resistenza a punzonamento 
		fck [MPA] resistenza cls
		p= [mm] perimetro della base di punzonamento
		d= [mm] altezza utile piastra
		rho1x,rho1y= rapporto di armatura TESA nelle 2 direzioni
		beta=1.15 #centro piastra, 1.4 bordo piastra, 1.5 spigolo di piastra
		'''
		vars(self).update(locals())
		self.u=p+pi*d
	
	def vrd1(self,fullOut=False):
		'vrd1=resistenza per unità di perimetro'
		tau_rd=self.taurd[self.taurd.gammac==self.gammac]
		tau_rd=interp(self.fck,tau_rd.fck,tau_rd.taurd)
		k=max((1,1.6-self.d/1e3))
		rho_1=min((0.015,sqrt(self.rho1x*self.rho1y)))
		Vrd_1= tau_rd*k*(1.2+40*rho_1)*self.d
		if fullOut: return locals()
		return Vrd_1


class Trd :
	def __init__(self,cl,Asw,s,Al,P,A,t=None,c=30,ac=None,theta=None):
		'''resistenza a torsione della trave DM2008 4.1.[27,28,29]
		cl=calcestruzzo
		Asw= area staffatura
		s=passo staffe
		Al= area ferri longitudinali
		P=perimetro sezione (oppure r_[b,h])
		A=area racchiusa dalla fibra media del profilo periferico (staffa ;) )
		t=None (spessore sezione cava se None lo calcola come da DM per sez piena)
		c=Copriferro
		ac=Acciaio
		theta=angolo inclinazione'''
		if A is None:
			if hasattr(P,'__len__'):
				A=(r_[P]-c).prod()
		if hasattr(P,'__len__'):
			P=2*sum(P)
		um=P-8*c
		if not theta:
			theta=((Al/um)/(Asw/s))**.5
			theta=min([max([.4,theta]),2.5])
		if t is None: t=A/um
		fcd_=cl.fcd/2 #nella norma indicato come f'cd
		Trcd= 2*A*t*fcd_ * 1/tan(theta)*(1+(1/tan(theta))**2)
		Trsd=2*A*Asw/s*ac.fyd/tan(theta)
		Trld=2*A*Al / um*ac.fyd*tan(theta)
		Trd=min((Trcd,Trsd,Trld))
		vars(self).update(locals())
	
if __name__=='__main__':
	pass
	cl=Calcestruzzo(Rck=300)
	sez=Sezione(rec,600,400)
	sez.mat=cl
	sezca=SezioneCA()
	sezca+=sez
	



