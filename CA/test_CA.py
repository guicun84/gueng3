
from .__init__ import *


cl=Calcestruzzo(Rck=30)
sez=Sezione('rec',200,500)
sez.mat=cl
arm=Armatura([Barra(r_[0,-470],12,4)])

sezCA=SezioneCA()
sezCA+sez
sezCA+arm

Mrd=sezCA.Mxrd()

Mref=76.81e6
sigc=-13.29
ss=391.3
ec=2.426
es=10/1e3
d=47*10
yn=9.175*10
r=Mrd/Mref
print('''
MOMENTO
valore calcolato {}
valore VCASLU {}
calcolato/ref {}

ASSE NEUTRO
calcolato {}
riferimento {}
calc./ref. {}'''.format(Mrd,Mref,r,sezCA.yn,yn,sezCA.yn/yn))

print('''
ALTEZZA EFFICACE
calcolato {}
riferimento {}
calc./ref. {}'''.format(sezCA.ptp[1],d,sezCA.ptp[1]/d))

