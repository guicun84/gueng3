#-*- coding:latin-*-
from scipy import pi,sign
from scipy.optimize import root
from scipy.integrate import trapz
from .sezione import Sezione

class Connettore:
	def __init__(self,K):
		'''K=rigidezza del connettore'''
		self.K=K
	def dx(self,N):
		return N/self.K

class ConnettoreCilindrico(Connettore):
	def __init__(self,d,h,incastri=1,E=210e3,nu=.25,G=None):
		'''d=diametro connettore
		h=altezza liera connettore
		incastri=1|2
		E=modulo elastico
		nu=.25 poissont
		G=None automatico'''
		if G is None:
			G=E/(2*(1+nu))
		vars(self).update(locals())
		self.J=pi*d**4/64
		self.Kf=3*E*self.J/h**3
		self.A=pi*d**2/4
		self.Ka=self.A*G/h
		if incastri==2:
			self.Kf*=4
		self.K=1/(1/self.Kf+1/self.Ka)

class Connessione:
	def __init__(self,connettore,l,n):
		'''connettore=Connettore utilizzato
		l=lunghezza della connessione
		n=numero di connettori
		'''
		vars(self).update(locals())
		self.K=connettore.K*n
		
	def eps(self,N):
		return self.connettore.dx(N)/self.n/self.l

	def N(self,eps):
		return eps*self.l*self.K

class ConnessioneConnettoriCilindrici(Connessione):
	def __init__(self,d,h,l,n,incastri=1,E=210e3,nu=.25,G=None):
		connettore=ConnettoreCilindrico(d,h,incastri,E,nu,G)
		Connessione.__init__(self,connettore,l,n)


class SezioneConnessa(Sezione):
	def __init__(self,*args,**kargs):
		'''oltre a quanto previsto in Sezione
		connessione= Connessione
		'''
		self.connessione=kargs['connessione']
		del kargs['connessione']
		Sezione.__init__(self,*args,**kargs)

	def sc(self,ec=None,eprec=None):
		if ec is None:
			ec=self.parent.e(self.sezioni[0,:])
		def fun(esc):
			sc=Sezione.sc(self,ec-esc,)
			N=trapz(sc*self.sezioni[1,:],self.sezioni[0,:])*sign(self.moltiplicatore)
			F=self.connessione.N(esc)
			return N-F
		sc=Sezione.sc(self,ec)
		N=trapz(sc*self.sezioni[1,:],self.sezioni[0,:])*sign(self.moltiplicatore)
		e0=.5*self.connessione.eps(N)
		x=root(fun,e0)
		if x.fun>1e-5:
			print (x)
		return Sezione.sc(self,ec-x.x,)

