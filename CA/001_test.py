from gueng.CA import *
#confronto i risultati di Mxrd ottenuti dalla classe SezioneCirc e da Sezione approssimando con 101 elementi...
#calcolo con la sezione circolare
cr=SezioneCirc(140,di=100)
cls=Calcestruzzo(fck=25)
cr.mat=cls
arm=Armatura([Barra(r_[0,-60],10)])
sez=SezioneCA()
sez+cr
sez+arm
sez.aggiorna()
mxrd1=sez.Mxrd()
sez.plot_tensioni()
sez.plot()


d=140
d2=100
a=linspace(0,2*pi,101)[:-1]
p=c_[cos(a),sin(a)]*d/2
cr_=Sezione('var',p)

p=c_[cos(a),sin(a)]*d2/2
cr__=Sezione('var',p)
cls=Calcestruzzo(fck=25)
cr_.mat=cls
cr__.mat=cls
cr__.moltiplicatore=-1
arm=Armatura([Barra(r_[0,-60],10)])
sez_=SezioneCA()
sez_+cr_
sez_+cr__
sez_+arm
sez_.aggiorna()
mxrd2=sez_.Mxrd()
figure()
sez_.plot_tensioni()
sez_.plot()
print(mxrd1,mxrd2,mxrd1/mxrd2)
