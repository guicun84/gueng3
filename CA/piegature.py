#-*-coding:latin-*-

from scipy import pi

class Barra:
	def __init__ (self,d,ac):
		'''d=diametro, ac=Classe Acciaio'''
		test=False
		if not hasattr(ac,'euk'):
			raise IOError('acciaio deve avere attributo "eu" epsilon ultimo')
		vars(self).update(locals())
		self.A=d**2*.25*pi

	@property
	def phimk(self):
		'diametro del mandrino minimo per non eccedere epsilon ultimo caratteristico'
		return 2*self.d/self.ac.euk

	@property
	def phimd(self):
		'diametro del mandrino minimo per non eccedere epsilon ultimo di progetto'
		return 2*self.d/self.ac.eud
	@property
	def Fbt(self):
		return self.A*self.ac.ftk

	def phimEC(self,cl,a=30):
		'''cl=Calcestruzzo
		a=distanza netta tra barre o copriferro
		'''
		ab=a+self.d/2
		return self.Fbt*(1./ab + 1./2/self.d)/cl.fcd

	def Acld(self,cl):
		'calcola area minima di calcestruzzo'
		return self.Fbt/cl.fcd

		
		

