#-*- coding:latin-*-
from scipy import *
class Trave:
    def __init__(self,As1,As2,b,h,fyd=450/1.15,cop=30):
        '''As1 As2=area armatura longitudinale superiore e inferiore
        h=altezza sezione
        fyd=resistenza
        cop=copriferro
        '''
        vars(self).update(locals())
        self.As=As1+As2
        self.Asmax=max((As1,As2))
        
    def __str__(self):
        return f'''As1={self.As1:.1f} mm^2 
As2={self.As2:.1f} mm^2 
b={self.b:.1f} mm ,h={self.h:.1f} mm, fyd={self.fyd:.1f} Mpa, cop={self.cop:.1f}'''
        
class Pilastro:
    def __init__(self,Vc,Nsup,Ninf,b0,b1,cl,cop=30):
        '''VC=[T1 T1] taglio nelle due direzioni
        Nsup inf compressione pilastro sopra e sotto al nodo
        b0 b1 dimensioni pilastro
        cl=calcestruzzo
        cop=copriferro''' 
        vars(self).update(locals())
        self.nidsup=self.Nsup/(b0*b1*cl.fcd)
        self.nidinf=self.Nsup/(b0*b1*cl.fcd)
        self.Vc=abs(self.Vc)
        
class Nodo:
    def __init__(self,t01,t02,t11,t12,pilastro,
                 gammaRd=1.1,fywd=450/1.15):
        '''t01,t02,t11,t12, travi che arrivano al nodo :
            t11,t12 direzione 1 , t21,t22 direzione 2
            none se non presente
        pilastro=pilastro superiore
        gammard TAb 7.2.1  1.1 per ca in opera CDB
        fyd resistenza barre'''
        vars(self).update(locals())
        self.cl=pilastro.cl
        self.b0,self.b1=self.pilastro.b0,self.pilastro.b1
        self.cop=self.pilastro.cop
        
    def getTravi(self,direz):
        '''direz=0|1 direzione di calcolo'''
        if direz==0:
            travi= [self.t01,self.t02]
        else :
            travi=[self.t11,self.t12]
        return [i for i in travi if i is not None]
    
    def tipo(self,direz):
        tr=self.getTravi(direz)
        if len(tr)==2:return 'interno'
        else:return 'esterno'
        
    def Vjdb(self,direz):
        travi=self.getTravi(direz)
        tipo=self.tipo(direz)
        if tipo=='interno':
            F=max([i.As*i.fyd for i in travi])
        elif tipo=='esterno':
            F=max([i.Asmax*i.fyd for i in travi])
        return self.gammaRd*F-self.pilastro.Vc[direz]
    
    def Vjdb_11e12(self,direz):
        travi=self.getTravi(direz)
        tipo=self.tipo(direz)
        if tipo=='interno':
            F=max([i.As*i.fyd for i in travi])
        elif tipo=='esterno':
            F=max([i.Asmax*i.fyd for i in travi])
        return self.gammaRd*F
    
    def eta(self,direz):
        return self.alphaj(direz)*(1-self.cl.fck/250)
    
    def alphaj(self,direz):
        if self.tipo(direz)=='interno':return 0.6
        return 0.48
    
    def bjhjhjw(self,direz):
        '''determina bh,hj,hjw in base alla direzione'''
        if direz==0:
            hjc=self.b0-2*self.cop
            bj=self.b1
        else:
            hjc=self.b1-2*self.cop
            bj=self.b0
        travi=self.getTravi(direz)
        hjw=min([i.h-2*i.cop for i in travi])
        btr=min([i.b for i in travi])
        bj=min((max((bj,btr)),min((bj+hjc/2+self.cop,btr+hjc/2+self.cop))))
        
        return bj,hjc,hjw
    
    def Rcd(self,direz):
        '''resistenza puntone compresso 7.4.8'''
        bj,hjc,hjw=self.bjhjhjw(direz)
        eta=self.eta(direz)
        return eta*self.cl.fcd*bj*hjc*(1-self.pilastro.nidsup/eta)**.5
    
    def Ash7_4_10(self,direz):
        '''area staffe per evitare fessurazione 7.4.10'''
        bj,hjc,hjw=self.bjhjhjw(direz)
        return ((self.Vjdb(direz)/(bj*hjc))**2/(self.cl.fctd+self.pilastro.nidsup*self.cl.fcd)-self.cl.fctd)/self.fywd*bj*hjw
    
    def Ash7_4_11o12(self,direz):
        '''area staffe resistente 7.4.10'''
        if self.tipo(direz)=='interno':
            return (self.Vjdb_11e12(direz)*(1-0.8*self.pilastro.nidsup))/self.fywd
        else:
            return (self.Vjdb_11e12(direz)*(1-0.8*self.pilastro.nidinf))/self.fywd
        
    def Ash(self,direz):
        return min((self.Ash7_4_10(direz),self.Ash7_4_11o12(direz)))
    
    def dict(self,direz):
        bj,hj,hjw=self.bjhjhjw(direz)
        tipo=self.tipo(direz)
        travi=self.getTravi(direz)
        if tipo=='interno':
            As=max([i.As for i in travi])
        else:
            As=max([i.Asmax for i in travi])
        out= dict(
            tipo=tipo,
            As=As,
            nidsup=self.pilastro.nidsup,
            nidinf=self.pilastro.nidinf,
            Vjdb10=self.Vjdb(direz),
            Rcd=self.Rcd(direz),
            Ash10=self.Ash7_4_10(direz),
            Vjdb11=self.Vjdb_11e12(direz),
            Ash11=self.Ash7_4_11o12(direz),
            bj=bj,hj=hj,hjw=hjw  ,   
        )
        out['Ash']=min((out['Ash10'],out['Ash11']))
        return out
        
