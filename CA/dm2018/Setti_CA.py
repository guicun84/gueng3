from numpy import *
class Setto:
	def __init__(self,lw,bw,hw,hs=3000,np=6,cd='b'):
		'''misure in mm
		lw lunghezza setto
		bw=largezza setto
		hw=altezza setto
		hs=altezza interpiano
		np=numero piani
		cd=classe di duttilità'''
		cd=cd.lower()
		vars(self).update(locals())

	@property
	def hcr(self):
		'altezza critica alla base 7.4.13'
		h=max((self.lw,self.hw/6))
		test=[h<=2*self.lw]
		nn={True:1,False:2}[self.np<=6]
		test.append(h<nn*self.hs)
		if not all(test):
			print('controllare limiti')
			for n,i in enumerate(['2*lw',f'{nn}*hs']):
				print (i,test[i])
		return h

	@property
	def lc(self):
		'lunghezza critica laterale §7.4.4.5.2'
		return max(.2*self.lw,1.5*self.bw)

	@property
	def Aslong(self):
		'armatura longitudinale zone dissipative §7.4.6.2.4'
		Asmin=self.lc*self.bw*.01
		Asmax=Asmin/.01*.04
		intemax=250
		_keys=[i for i in locals().keys()]
		_out=dict()
		for i in _keys:
			if not(i[0]=='_' or i =='self'):
				_out[i]=locals()[i]
		return _out

	def Astras(self,dlon,fcd,muphi,nid,c=30,fyd=450/1.15,esyd=450/1.15/210e3):
		'''armatura longitudinale zone dissipative §7.4.6.2.4
		dlon=diametro armature longitudinali massimo
		fcd resistenza cls
		muphi=domanda in duttilità di curvatura a SLC
		nid=forza assiale adimensionalizzata (Nd/(Ac*fcd))
		c=copriferro
		fyd=resistenza acciaio
		esyd=deformazione di snervamento acciaio
		'''
		intelongmax=dict(a=150,b=200)[self.cd]
		if self.cd=='a':dmin=max((6,.4*dlon))
		else:dmin=6
		smax=[dict(a=1/3,b=1/2)[self.cd]*self.bw]
		smax.append( dict(a=5,b=8)[self.cd]*self.bw*dlon)
		smax.append(dict(a=125,b=175)[self.cd])
		smax=min(smax)
		omwdmin=dict(a=.12,b=.08)[self.cd]
		#7.4.29
		b0,h0=r_[self.bw,self.lc]-(2*c)
		_p=2*(b0+h0) #perimetro staffa
		_nbar=ceil(h0/intelongmax)
		intelong=h0/_nbar
		alphan=1-_nbar*intelong**2/(6*b0*h0)
		alphas=(1-smax/(2*b0))*(1-smax/(2*h0))
		alpha=alphan*alphas

		omwdmin=min((omwdmin,1/alpha*30*muphi*nid*esyd*self.bw/b0-.035))
		#inversa da 7.4.30
		amin=(omwdmin*b0*h0*fcd*smax)/(_p*fyd)
		dmin=max((dmin,(amin/pi*4)**.5))
		_keys=[i for i in locals().keys()]
		_out=dict()
		for i in _keys:
			if not(i[0]=='_' or i =='self'):
				_out[i]=locals()[i]
		return _out

class DomandaDuttilita:
	def __init__(self,q0,T1,Tc):
		'''q0=fattore di comportamento
		T1 periodo primo modo
		Tc perido spettro'''
		vars(self).update(locals())
		#7.4.3
		if T1>=Tc:
			self.muphi=1.2*(q0-1)
		else:
			self.muphi=1.2*(1+2*(q0-1)*(Tc/T1))
