#-*-coding:utf-8-*-

from scipy import array,pi,imag,zeros,arange,sign,r_,ceil
import os,sqlite3,re,sys,pandas as pa
from gueng.utils.varie import print2s
from .materiali import Calcestruzzo

ferpath=re.compile('(\d+)d(\d+)')

dbname='aree_barre.sqlite'
dbname=os.path.join(os.path.dirname(__file__),dbname)
class A2d:
	nmaxbar=20
	ndiam=4
	db_name=dbname
	db=sqlite3.connect(db_name)
	if db.execute('select 1 from sqlite_master where name="armature2"').fetchone():
		db.execute('''create view if not exists armature2 as 
select id,A,nome from (
	select armatura,group_concat(num||"\u00f8"||diam," + ") as nome from barre group by armatura) t1
inner join armature on t1.armatura=armature.id order by A asc''')
		
	@staticmethod
	def db2ram():
		db2=sqlite3.connect(':memory:')
		db2.executescript('\n'.join(A2d.db.iterdump()))
		A2d.db.close()
		A2d.db=db2

	def __init__(self,A):
		'''A=area da ottenere'''
		vars(self).update(locals())
		if not A2d.db.execute('select 1 from sqlite_master where type="table" and name="armature"').fetchone():
			print('genero tabelle')
			A2d.db.isolation_level=None
			A2d.db.executescript('''
pragma foreign_keys=ON;
create table armature(
	id integer primary key autoincrement,
	nbar integer,
	ndiam integer,
	A real);

create table barre( 
	id integer primary key autoincrement,
	armatura integer,
	diam integer,
	num integer,
	foreign key (armatura) references armature(id))
	''')
			d=arange(6,33,2)
			num=Num(len(d),A2d.nmaxbar,A2d.ndiam)
			A=d**2*.25*pi
			A2d.db.execute('begin')
			k=0
			kkk=0
			stout='\r'+' '.join(['{{i[{j}]:02d}}'.format(j=j) for j in range(len(d))]) +' {kkk}'
			for i in num:
				kkk+=1
				if not kkk%int(1e2):
					sys.stdout.write(stout.format(i=i,kkk=kkk))
					sys.stdout.flush()
				if not kkk%int(1e3):
					A2d.db.commit()
					A2d.db.execute('begin')
				n=sum(i)
				if sum(i)==0:continue
				#controllo sui diametri
				nd=sum(sign(i))
				diam=arange(6,33,2)[i!=0]
##				dist=(diam-diam.min()).sum()
#				dist=diam.max()-diam.min()
#				if dist>4:continue #considero solo armature con 3 diametri adiacenti
				k+=1
				a=sum(i*A)
				A2d.db.execute('insert into armature values(?,?,?,?)',(k,n,nd,a))
#				d=6 
				for j,h in zip(diam,i[i!=0]):
					A2d.db.execute('insert into barre values(Null,?,?,?)',(k,j,h))
			sys.stdout.write('\r{} {}'.format(i,kkk))
			sys.stdout.flush()
			print('')
			A2d.db.execute('commit')
			A2d.db.isolation_level=''
			A2d.db.commit()
			print('generate {} armature'.format(A2d.db.execute('select count(*) from armature').fetchone()[0]))
			print('genero gli indici')
			A2d.db.executescript('''
				CREATE INDEX barre_armatura on barre(armatura);
				CREATE INDEX armature_area on armature(A);
				''')
			A2d.db.commit()
			print('indici creati')
	@staticmethod	
	def cerca(A,ndmin=1,ndmax=2,nmin=None,nmax=None,limit=5,soglia=0.2,sort='k',dchar='d',l=None,p=None,pmin=40,pmax=350,nd=None,n=None,distM=4,printquery=False):
		'''Area da cercare in mm^2
		ndmin, ndmax, numero massimo e minimo di diametri da usare, se ndmin=[d1..dn] ricerca solo i diametri specificati
		nmin,nmax: numero massimo e minimo di barre da usare [1,30]
		limit= limite di risultati da riportare
		soglia, errore massimo tollerato in difetto
		sort: parametro di ordinamento [k=abs(dif)*nbar*nd , dif,ndiam,nbar]
		dchar= carattere separatore per diametro (\\u00f8 per simbolo phi)
		[l]=lunghezza da coprire mm
		[pmax pmin] passi massimi e minimi se fornito l
		n,nd,p : se forniti fissano i max e min delle omonime allo stesso valore
		distM = distanza massima tra diametro min e max'''
		if hasattr(A,'__len__'): 
			if len(A)==1:
				A=A[0]
			else:
				raise TypeError('A={:r} deve essere al massimo monodimensionale con 1 elemento'.format(a))

		#controllo settaggi di massimi e minimi uguali
		if p is not None:
			pmax=pmin=p
		if n is not None:
			nmax=nmin=n
		if nd is not None:
			ndmax=ndmin=nd

		#controllo calcolo di n max e min in caso di lunghezza da coprire e passo
#		if l is None and (pmax is not None or pmin is not None):
#			l=1000
		if l is not None:
			if nmin is None:
				nmin=int(l/pmax)+1
			if nmax is None:
				nmax=min((A2d.nmaxbar,int(l/pmin)+1))
		if nmin is None or hasattr(nmin,'__len__'): nmin=1
		elif nmin>A2d.nmaxbar: raise ValueError('nmin>{}'.format(A2d.nmaxbar))
		if nmax is None: nmax=A2d.nmaxbar

		#aree massime e minime
		Amin=A-soglia
		Amax=A*1.5

		query='''
select group_concat(armstr," + "),A,dif,ndiam,nbar from  (
	select *,
	A-:A as dif,
	abs((A-:A)*nbar*ndiam) as k
	from armature where A between :Amin and :Amax and ndiam<=:distM {cond}  ) arm
inner join (
	select  *,
	num||"{dchar}"||diam as armstr 
	from  barre) b
on b.armatura=arm.id 
{query_dist_bar}
{c2}
group by arm.id 
 order by {sort} asc {lim} '''

		cond=[]
		c2=[]
		if hasattr(ndmin,'__len__') or hasattr(ndmax,'__len__'):
			if hasattr(ndmax,'__len__'): ndmin=ndmax
			try:
				ndmin=[int(i) for i in ndmin]
			except:
				raise ValueError
#			if not all([type(i)==int for i in ndmin]):
#				raise ValueError
			stbar=','.join(['{}'.format(i) for i in ndmin])
			q='''
id in (
	select armatura from (
		select armatura,sum(t1) as t2, sum(c1) as c2 from (
			select armatura,
			case when diam in ({stbar}) then 1 else 0 end as t1,
			1 as c1
			from barre)
		group by armatura having t2=c2
	)
)'''.format(stbar=stbar)
			cond.extend(['and',q])

#			c2.append('where diam in (' +','.join(['{}'.format(i) for i in nd]) +')')
			ndmin=len(ndmin)
			c2.extend(['ndiam<=:ndmin'])
		else:
			cond.extend(['and','ndiam<=:ndmax','and','ndiam>=:ndmin'])
		if not hasattr(n,'__len__'):
			cond.extend(['and', 'nbar<=:nmax'])
			cond.extend(['and', 'nbar>=:nmin'])
		else:
			cond.extend(['and','nbar in ({})'.format(','.join(['{}'.format(i) for i in n]))])

		if limit: lim=' limit :limit'
		else: lim=''
#		if c2:
#			lim2=lim
#			lim=''
#		else: lim2=''
		if distM is None:
			query_dist_bar=''
		else:
			query_dist_bar=''' inner join (
	select armatura,max(diam)-min(diam) as ddist from barre group by armatura ) td
on td.armatura=arm.id'''
			c2.extend(['and','ddist<=:distM'])

		if c2:
			if c2[0] in ['and','or']: c2=c2[1:]
			if len(c2)>0: c2.insert(0,'where')
		query=query.format(cond=' '.join(cond), lim=lim,c2=' '.join(c2),sort=sort,dchar=dchar,query_dist_bar=query_dist_bar)
		if printquery:
			print2s('query A Amin Amax distM ndmax ndmin nmax nmin l pmin pmax')
		return A2d.db.execute(query,locals()).fetchall()

	@staticmethod
	def area(*args):
		'''args=sequenza [n1,d1,n2,d2....] numero diametro'''
		if len(args)==1:
			n=array([1.])
			d=array([args[0]],float)
		else:
			d=array(args[1::2],float)
			n=array(args[0::2])
		return (d**2*.25*pi*n).sum()

	@staticmethod
	def parseArm(arm,out='list'):
		'''analizza la stringa di armatura per restituire una lista con numero e diametri armatura
		arm = stringa di armatura tipo 1d12 + 3d16...
		out=[list,grp] tipo di output: list ritorna una lista con tutti i diametri ripetuti, grp ritorna lista con numero e diametro'''
		f=ferpath.findall(arm)
		f=[[int(i[0]),int(i[1])] for i in f]
		if out=='grp':return f
		ff=[]
		for i in f: ff.extend([i[1]]*i[0])
		return ff

	@staticmethod
	def peso(d,l,n=1,p=100):
		'''calcolo del peso di una posizione
		d= diametro mm
		ls= lunghezza ferro cm
		n=moltiplicatore [per esempio su staffe mettere semiperimetro e 2]
		p=per staffe = passo'''
		return n*l/100*d**2*.25*pi/1e6*7850*100/p

class Num:
	'''classe per il calcolo di tutte le possibili combinazioni di barre'''
	def __init__(self,l,m,k=5):
		'''l=lunghezza del vettore
		m= valore massimo della somma
		k=numero di elementi consecutivi da considerare'''
		vars(self).update(locals())
		self.n=zeros(l,int)
		self.s=0 #inizio del set di parametri da valutare
		self.tot=0 #valore della somma delle barre

	def add1(self):
		if self.n[-1]==self.m :raise StopIteration
		end=min(len(self.n),self.s+self.k)
		test=False
		for i in range(self.s,end):
			if self.n[i:end].sum()+1<=self.m:
				self.n[i]+=1
				self.tot=self.n.sum()
				test=True
				break
			else:
				self.n[i]=0
		if not test:
			self.s=min((self.s+1,len(self.n)-2))
			end=min(end,len(self.n)-1)
			self.n[end]=+1
			self.tot=self.n.sum()

	def __iter__(self):	
		return self

	def __next__(self):
		v=self.n.copy()
		self.add1()
		return v

class Ancoraggio:
	@staticmethod
	def ancoraggio(cls,barra,buoneCondizioni=False,fyd=391.,modulo=10):
		'''calcolo dell'ancoraggio considerando DM012008#4.1.2.1.1.4
		cls=calcestruzzo o rck
		barra=diametro barra o istanza di Barra
		fyd= resistenza a trazione acciaio
		buoneCondizioni: flag per considerare buone condizioni di ancoraggio o meno'''
		if not isinstance(cls,Calcestruzzo):
			cls=Calcestruzzo(cls)
		if hasattr(barra,'di'):
			phi=barra.di
			fyd=barra.mat.fyd
		else:
			phi=barra
		eta1={True:1,False:.7}[buoneCondizioni]
		eta2={True:1,False:(132-phi)/100}[phi<=32]

		fbk=2.25*eta1*eta2*cls.fctk
		fbd=fbk/1.5
		l= 	phi**2*pi/4*fyd / (fbd*phi*pi)
		if modulo:
			l=modulo*ceil(l/modulo)
		return l

	@staticmethod
	def sovrapposizione(cls,barra,buoneCondizioni=False,fyd=391.,modulo=10):
		if hasattr(barra,'__len__'):
			l1=Ancoraggio.ancoraggio(cls,barra[0],buoneCondizioni,fyd,modulo=None)	
			l2=Ancoraggio.ancoraggio(cls,barra[1],buoneCondizioni,fyd,modulo=None)	
			l=l1+l2
		else:
			l=2*Ancoraggio.ancoraggio(cls,barra,buoneCondizioni,fyd,modulo=None)
		if modulo:
			l=modulo*(ceil(l/modulo))
		return l


	@staticmethod
	def __ancoraggi_full(d,cls,fyd,modulo=10):
		if not isinstance(cls,Calcestruzzo):
			cls=Calcestruzzo(cls)
		out= dict(
			la=Ancoraggio.ancoraggio(cls,d,True,fyd,modulo),
			ls=Ancoraggio.sovrapposizione(cls,d,True,fyd,modulo),
			lacc=Ancoraggio.ancoraggio(cls,d,False,fyd,modulo),
			lscc=Ancoraggio.sovrapposizione(cls,d,False,fyd,modulo),
		)
		return pa.Series(out)

	@staticmethod
	def tabella(cls,fyd=391.,modulo=10):
		di=r_[arange(6,31,2),25]
		di.sort()
		out=pa.DataFrame(di)
		out.columns=['di']
		if not isinstance(cls,Calcestruzzo):
			cls=Calcestruzzo(cls)
		out=out.join(out.apply(lambda x:Ancoraggio.__ancoraggi_full(x.di,cls,fyd,modulo),1))
		out=out[['di','la','ls','lacc','lscc']]
		return out
		
			

