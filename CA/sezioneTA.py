#-*- coding:latin -*-

from gueng.CA import *
from scipy import * 
from scipy.optimize import root
from gueng.geometrie import linea as Linea,area as Area

class SezioneTA(SezioneCA):
	n=15
	def Somog(self,y):
		S=0
		for a in self.ca:
			b=a.sposta_punti(r_[0,y])
			b=Area(b.taglia(Linea(r_[100.,0],r_[0.,0]),1))
			S+=b.Sx
		for a in self.ac:
			S+=self.n*a._As*a.n*(a.co[1]-y)
		return S
	@property
	def yn(self,rh0=.25):
		'''rh0 altezza relativa da cui iniziare ricerca'''
		if not hasattr(self,'_SezioneTA__yn'):
			psup=None
			pinf=None
			for i in self.ca:
				l=i.limiti[-2:]
				if psup is None:
					psup=l[1]
					pinf=l[0]
				if psup>l[1]:psup=l[1]
				if pinf>l[0]:pinf=l[0]
			h0=psup-rh0*(psup-pinf)
			yn=root(self.Somog,h0)
			if yn.success:
				self.__yn=atleast_1d(yn.x)[0]
			else:
				return yn
		return self.__yn

	@property
	def Jomog(self):
		'''riferito ad asse neutro'''
		J=0
		for a in self.ca:
			b=a.sposta_punti(r_[0,self.yn])
			b=Area(b.taglia(Linea(r_[100.,0],r_[0.,0]),1))
			J+=b.Ix
		for a in self.ac:
			temp=self.n*a._As*a.n*(a.co[1]-self.yn)**2
			J+=temp
		return J

	
	def Ibs(self,y=None):
		if y is None: y=self.yn
		S=0
		base=0
		for a in self.ca:
			base+=a.b(self.yn)
			b=a.sposta_punti(r_[0,self.yn])
			ht=max((0,-self.yn+y)) #altezza a cui eseguire taglio
			b=b.taglia(Linea(r_[100.,ht],r_[0.,ht]),1)
			if b:
				b=Area(b)
				S+=b.Sx
		for a in self.ac:
			if a.co[1]>=y:
				S+=self.n*a._As*a.n*(a.co[1]-self.yn)
		return self.Jomog*base/S

	def sigmac(self,M):
		ysup=max([i.limiti[3] for i in self.ca])
		return M/self.Jomog*abs(ysup-self.yn)

	def sigmas(self,M):
		ymin=min([i.co[1] for i in self.ac])
		return self.n*M/self.Jomog*(self.yn-ymin)

		
