#-*-coding:latin-*-

from scipy import *
from scipy.integrate import cumtrapz

def M2chi(sz,M,N=0.,sz2=None,fullout=False,start=1e-6,startsle=5e-4):
	'''sz=SezioneCA
	M,N=momento sforzo normale 
	sz2=sezione per momento negativo, se None ruota sz di pi'''
	#preparazione dei dati
	if sz2 is None: sz2=sz.ruota(pi)
	if M>=0:
		m=M
		s=sz
	else: 
		m=-M
		s=sz2
	#ricerca prima fessurazione
	m1f=s.M1fes(N,start)
	soglia=1e-2
	if N==0:test=s.N()<=soglia
	else :test=abs((s.N()-N)/s.N())<=soglia
#	if not test:
#		print( f'N assegnato {N}, calcolato {s.N()}')
	chi1f=s.chi.copy()
	#ricerca curvatura fessurata
	if m1f>m:startsle=r_[1,-1.]*start
	s.deforma(0,0)
	s.SLE(m,N,start=startsle)
	msle=s.M().copy()
	if isnan(msle):msle=0.
	if M==0:test2=s.M()<=soglia
	else :test2=abs((msle-m)/msle)<soglia
#	if not test2:
#		print( f'N assegnato {N}, calcolato {s.N()}')
#		print( f'M assegnato {m}, calcolato {s.M()}')
	chis=s.chi.copy()
	#calcolo curvatura media
	xi=m1f/m
#	xi=chi1f/chis
	if xi>1:
		chi=chis
	else:
		chi=xi*chi1f + (1-xi)*chis
	#se su sezione ruotata inverto il segno di chi
	if s is sz2: chi=-chi
	if isnan(chi):chi=0.
	if fullout:return locals()
	return chi


def deformata(sz,x,M,N=None,l=None,poly=True,fullout=False,start=1e-6,startsle=5e-4):
	'''sz=SezioneCA
	x=array con ascissa curvilinea
	M,N=momento sforzo normale sui punti x
	l=luce trave
	se x è scalare viene considerato come luce della trave e MN in mezzeria
	se len(x)==2 viene considerato come trave incastrata'''

	M=atleast_1d(M)
	x=atleast_1d(x)
	
	if N is None: N=zeros_like(M,dtype=float)
	else: N=atleast_1d(N)
	chi=zeros_like(x,dtype=float)
	datchi=[]
	sz2=sz.ruota(pi)
	for i,(m,n) in enumerate(zip(M,N)):
		datchi.append(M2chi(sz,m,n,sz2,fullout=True,start=start,startsle=startsle))
		chi[i]=datchi[-1]['chi']

	if len(x)==1:
		x=r_[0,.5,1]*x[0]
		chi=r_[0,1,0]*chi[0]
		if l is None:l=x[2]
	
	elif len(x)==2:
		x=r_[-x[1],x[0],x[1]]
		chi=r_[-chi[1],chi[0],chi[1]]
		if l is None:l=x[2]

	#test per bontà valutazione
	I2=r_[[i['msle'] for i in datchi]]
	check=sum(abs(abs(M)-I2))/sum(abs(M))
	del I2
	
	if poly:
		if l is None:l=x[-1]
		chip=polyfit(x,chi,2)
		vp=polyint(chip,2)
		vp[-2]=-polyval(vp,l)/l
		if fullout:return locals()
		return vp
	else:
		if l is None:l=x[-1]
		phi=cumtrapz(chi,x,initial=0)
		v=cumtrapz(phi,x,initial=0)
		v-=v[-1]/l*x
		if fullout:return locals()
		return v

