#include <stdio.h>
#include <math.h>

double  sigma(double ep, double fcd, double ec2, double ecu)
{
	if (ep<0)
	{
		return 0;
	}
	else if (ep<ec2)
	{
		return (-1*pow(ep/ec2,2)+ 2*ep/ec2)*fcd;
	}
	else if (ep<=ecu)
	{
		return fcd;
	}
	else
	{
		return 0;
	}
}

sigma_vec(int n ,double *ep, double fcd, double ec2,double ecu, double *out )
{
	int i;
	for (i=0;i<n;i++)
	{
		out[i]=sigma(ep[i],fcd,ec2,ecu);
	}
}
	
