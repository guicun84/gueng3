#-*-coding:utf-8-*-


from scipy import *

def a2f(A,p,f=None):
	'''calcola i ferri necessari per avere l'area A con passo p
	l'idea di base è usare o un solo ferro o un ferro intermedio
	f=None, server per forzare uno dei due diametri da cercare:'''
	n=100/p
	a=A/n
	diametri=r_[6,8,10,12,14,16,18,20,22,24] #da controllare e completare	#aree con ferro singolo:
	A1=diametri**2*pi/4
	#aree con due ferri:
	f1=dot(A1.reshape(A1.size,1),ones((1,A1.size)))
	f2=f1.T
	A2=f1+f2
	#ricerco barra singola
	d1=where(A1>=a)
	if d1[0].size>0:
		d1=d1[0][0]
		d1=diametri[d1]
	#ricerba barra doppia:
	if f is None:
		dif=A2-a
		dif[dif<0]=dif.max()
		d2=where(dif==dif.min())
		if d2[0].size==2:
			d2=d2[0]
		else:
			d2=r_[d2[0],d2[1]]
		d2=diametri[d2[0]],diametri[d2[1]]
	else:
		a-=f**2*pi/4
		A2=diametri**2*pi/4
		d2=where(A2>=a)[0][0]
		d2=f,diametri[d2]
	return d1,d2
