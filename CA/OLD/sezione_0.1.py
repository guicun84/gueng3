#-*-coding:utf-8-*-

from pylab import *
from scipy import *
from scipy.optimize import leastsq , root , brute
import gueng.geometrie
from pdb import set_trace
ion()
'''
DA FARE
Per lo stato limite ultimo
1) valutazione delle resistenze ultime date le deformazioni
2) calcolo della deformazione nella sezione nei vari campi
3) automatizzazione della ricerca dello SLU

Per la sezione in generale
1) ridefinizione iniziale della sezione [tipi standard rettngolare,T ecc..]
2) definizione della rotazione della sezione (sulla sezione gia ereditato da trasportare sulle armature)
3) valutazione domini di rottura e resistenze ultime in asse Y (poca roba avendo definito 2)
'''

class sezione (object,gueng.geometrie.area):
	__version__='1.1'
	def __init__(self,tipo,*args):
		choose={'rec':self.__init_rect , 'T':self.__init_T}
		if tipo in list(choose.keys()):
			choose[tipo](*args)
		else:
			gueng.geometrie.area.__init__(self,*args)
		self.seziona()

	def __init_rect(self,*args):
		b,h=args
		punti=r_[[[-b/2,0.],[-b/2,-h],[b/2,-h],[b/2,0]]]
		gueng.geometrie.area.__init__(self,punti)
	
	def __init_T(self,*args):
		pass

	
	def seziona(self,n=200):
		'''divide la sezione in n striscie  parallele asse x e
		calcola la larghezza di ogni striscia'''
		h=linspace(self.limiti[2],self.limiti[3],n+1)
		h=h[:-1]+(h[1]-h[0])/2
		b=r_[[self.b(i) for i in h]]
		self.sezioni=r_[[h,b]]
		
	
	def sc(self,ecu,ecd):
		'''valuta la tensione all'interno del calcestruzzo
		ecu,ecd= deformazione superiore deformazine inferiore (up-down)'''
		ec=linspace(ecd,ecu,self.sezioni.shape[1]+1)
		ec=ec[:-1]+(ec[1]-ec[0])/2
		sc=self.mat.sigma(ec)
		return sc

	def Nc(self,ecu,ecd):
		'''valuta la componente normale dovuta al calcestruzzo'''
		sc=self.sc(ecu,ecd)
		return (sc*self.sezioni[1,:]*(self.sezioni[0,1]-self.sezioni[0,0])).sum()

	def Mc(self,ecu,ecd):
		'''valuta il momento dovuto al calcestruzzo'''
		sc=self.sc(ecu,ecd)
		return (sc*self.sezioni[1,:]*(self.sezioni[0,1]-self.sezioni[0,0])*(self.sezioni[0,:]-self.G[1])).sum()

	
	@property
	def armatura(self):
		return self._armatura
	@armatura.setter
	def armatura(self,arm):
		self._armatura=arm
		self._armatura.parent=self
		self._armatura.aggiorna()

	def ss(self,ecu,ecd):
		'''valuta le tensioni allinterno delle armature'''
		es=self.armatura.es(ecu,ecd) #lo chiedo ovviamente alle armature...
		ss=self.armatura.sigma(es)
		return ss

	def Ns(self,ecu,ecd):
		'''Valuta la componete normale dovuta alle armature'''
		es=self.armatura.es(ecu,ecd)
		Ns=self.armatura.N(es)
		return Ns.sum()

	def Ms(self,ecu,ecd):
		'''Valuta il momento dovuto alle armature'''
		es=self.armatura.es(ecu,ecd)
		Ms=self.armatura.M(es)
		return Ms.sum()
	
	def N(self,ecu,ecd):
		return self.Nc(ecu,ecd)+self.Ns(ecu,ecd)

	def M(self,ecu,ecd):
		return self.Mc(ecu,ecd)+self.Ms(ecu,ecd)

	def dN(self,ec,N=0):
		return self.N(*ec)-N
	
	def dN2(self,ecs,eci,N):
		return self.N(ecs,eci)-N
	
	def dM(self,ec,M=0.):
		return self.M(*ec)-M
	
	def sle(self,ec,N=0.,M=0):
		return r_[self.dN(ec,N),self.dM(ec,M)*1e-3]
	def cerca_sle(self,N=0,M=0,full_out=False):
		ec=leastsq(self.sle,r_[.0,.0],(N,M))
		if full_out: return ec[0],locals()
		else:return ec[0]
	
	def slu(self,eci,N=0.):
		try: eci=eci[0]
		except:pass
		ecs=root(self.dN2,.003,args=(eci,N)).x
#		if not -.01<ecs<.0035:
#			ecs=0
#		if ecs<-.01: ecs=-.01
#		if ecs>.0035: ecs=.0035
		M=self.M(ecs,eci)
		return -M
	
	def cerca_slu_(self,N=0.,k=27):
		#lo calcolo per pentacotomia..... valuto in 10 punti iterativamente
		#lento e non copre bene tutto l'intervallo... devo scrivere bene tutto con calcma...
		#funziona bene solo per travi, per colonne da problemi
		eci=linspace(-.01,.004,k)
		M=r_[[self.slu(i,N) for i in eci]]
		er=1
		while er>1e-4:
			c=eci[M==M.min()].mean()
			d=eci[1]-eci[0]
			eci=linspace(c-d,c+d,10)
			M=r_[[self.slu(i,N) for i in eci]]
			er=abs(M.ptp()/M.min())
#		print M
#		print er
#		eci=minimize(self.slu,.0035,(N,),method='anneal',bounds=((-.1,.0035),)).x
#		ecs=root(self.dN2,0,(eci,N)).x
		return -M.min()

	def cerca_slu(self,N=0,k=27,full_out=False):
		'''ricerca il momento resistente ultimo '''
		#versione ottimizzata con le funzion di scipy (velocità 3.2X rispetto cerca_slu_
		M=brute(self.slu,([-.01,.0035],),(N,),Ns=k,full_output=True)
#		ecs=root(self.dN2,0.,(eci,N)).x
		if full_out: return -M[1],M
		else:return -M[1]


	def fessura(self,N,M,Es=210000,kt=.4,k2=.5):
		b=self.ptp[0]
		h=self.ptp[1]
		d=abs(self.armatura.b.min()+self.G[1])
		c=h-d
		fck=self.mat.fck
		ec=self.cerca_sle(N,M)
		ss=abs(self.ss(*ec)).max()
		y0=interp(0,[min(ec),max(ec)],[h,0])
		#diametro equivalente
		#considero tutte le barre tese con b<y0-G
		As=sum([i.As for i  in self.armatura.barre if i.co[1]<=-y0])
		phi=As*4/pi/sum([i.di*i.n for i  in self.armatura.barre if i.co[1]<=-y0])

		Aec=min([ 2.5*(h-d) , (h-y0)/3 , h/2])*b #area efficace di calcestruzzo per sezione rettangolare... andrebbe ridefinita... per le sezioni generiche
		reff=As/Aec
		fcm=fck+8
		fctm=.3*fck**(2./3)
		Ecm=22000*(fcm/10)**.3
		ale=Es/Ecm
		esm=max([(ss-kt*fctm/reff*(1-ale*reff))/(Es),
		0.6*ss/Es]) #deformazione unitaria delle barre
		dsmax=1.3*(h-y0)# distanza tra fessure
		wd=esm*dsmax
		#apertura fessura in prossimità della barra.
		k1=.8
		k3=3.4
		k4=.425
		dsmax2=k3*c+k1*k2*k4*phi/reff
		wd2=esm*dsmax2
		print(locals())
		return r_[wd2,wd,5*(c+phi/2)]

	def ruota(self,ang):
		#eseguo la rotazione ereditata:
		ns=gueng.geometrie.area.ruota(self,ang)
		ns.seziona()
		if hasattr(ns,'armatura'):
			ns.armatura.parent=ns
			ns.armatura.ruota(ang)
		return ns	

	def plot(self,*args):
		gueng.geometrie.area.plot(self,*args)
		if hasattr(self,'armatura'):
			self.armatura.plot()

	def __repr__(self):
		return '%g,%g, A=%g\nMateriale: %s\nArmatura\n%s'%(self.ptp[0],self.ptp[1],self.A,self.mat,self.armatura)
	
class calcestruzzo:
	gamma_c=1.6 
	ec=.002
	ecu=.0035
	def __init__(self,Rck,fck=0):
		self.Rck=Rck
		if not fck:
			self.fck=.83*Rck
		self.fcm=fck+8
		if self.Rck<=60:
			self.fctm=0.3*self.fcm**(2./3)
		else:
			self.fctm=2.12*log(1+self.fcm/10)
		self.fctk=.7*self.fctm
		self.fctk_=1.3*self.fctm
		self.Ecm=22000*(self.fcm/10)**.3
		self.G=self.Ecm/(2*(.2+1))

	def __sigma(self,ec,fcd=0,alpha=.85):
		'''calcola la tensione nel calestruzzo in un punto'''
		if not 0< ec < calcestruzzo.ecu:
			return 0.
		if not fcd:
			fcd=self.fck/calcestruzzo.gamma_c
		if 0<ec< calcestruzzo.ec:
			return 1000*ec*alpha*fcd*(-250*ec+1)
		elif calcestruzzo.ec<=ec<=calcestruzzo.ecu:
			return fcd*alpha
	__sigma_=vectorize(__sigma)	#versione vettorizzata della __sigma, in questo modo richiede come input self ed ec obbligatori	
	def sigma(self,ec,fcd=0,alpha=.85): #versione vettorizzata di sigma che si comporta correttamente come __sigma in fase di chiamata
		'''calcola la tensione del calcestruzzo nota la deformazione'''
		return self.__sigma_(self,ec,fcd,alpha)
	
	def __repr__(self):
		f=int(round(self.fck))
		r=int(round(self.Rck))
		return 'Calcestruzzo C%d/%d'%(f,r)



class armatura:
	def __init__(self,barre=[],parent=None):
		self.barre=barre
		self.parent=parent
		
	def aggiorna(self):
		if self.parent:
			self.G=self.parent.G[1]
			self.limiti=self.parent.limiti[2:]
		else:
			self.G=0
			self.limiti=r_[0,0.]
		self.aree=r_[[i.As for i in self.barre]]
		self.eu=r_[[i.mat.eud for i in self.barre]]
		self.ey=r_[[i.mat.eyd for i in self.barre]]
		self.fyd=r_[[i.mat.fyd for i in self.barre]]
		self.b=r_[[i.co[1] for i in self.barre]]-self.G## bracci delle barre di armatura]
		self.E=r_[[i.mat.Es for i in self.barre]]
		
	def es(self,sup,infe):
		b=r_[[i.co[1] for i in self.barre]]
#		return interp(b,[self.limiti[0],self.limiti[1]],[infe,sup])
		return infe+(sup-infe)/(self.limiti[1]-self.limiti[0])*(b-self.limiti[0])

	def sigma(self,es):
		'''calcola la tensione nelle barre'''
		return (abs(es)<self.eu) * ((abs(es)< self.ey) * self.E*es + (abs(es)>=self.ey) * sign(es)*self.fyd)

	def N(self,es):
		'''calcola lo sforzo normale nelle barre'''
		return self.aree* self.sigma(es)
	
	def M(self,es,G=0):
		'''calcola il momento flettente dovuto alle barre'''
		return self.N(es)*(self.b-G)

	def plot(self,*args,**kargs):
		for i in self.barre:
			i.plot(False,*args,**kargs)
		draw()
		axis('equal')
	
	def ruota(self,rot):
		if not hasattr(rot,'shape'):
			rot=r_[[[cos(rot),sin(rot)],[-sin(rot),cos(rot)]]]
		for b in self.barre:
			b.co=dot(b.co,rot)
		self.aggiorna()	

	def __repr__(self):
		st=['%s'%i for i in self.barre]
		return '\n'.join(st)

class acciaio:
	def __init__(self,ftk=540,fyk=450,euk=.0079):
		self.fyk=fyk
		self.ftk=ftk
		self.euk=euk
		self.eud=euk*.9
		self.Es=210e3
		self.eyk=self.fyk/self.Es
		self.fyd=self.fyk/1.15
		self.eyd=self.fyd/self.Es
	def sigma(self,es):
		return (abs(es)<=self.eud) * ((abs(es)<self.eyd) * self.Es*es + (abs(es)>=self.eyd)* sign(es)*self.fyd)
	

class barra(object):
	mat=acciaio()
	def __init__(self,co,di=None,n=1,As=None,mat=None):
		'''co= coordinate barra
		di=diametro
		As=area [opzionale]
		mat=materiale

		As viene calcolata automaticamente in base al diametro
		se di=0 e As!=0 viene calcolato un diametro equivalente
		mat per default Ã¨ b450c'''
		self.co=co
		if di:
			self.di=di
		elif As:
			self.As=As
		if mat:
			self.mat=mat
		self.n=n

	@property
	def As(self):
		return self._As*self.n
	@As.setter
	def As(self,As):
		self._As=As
		self._di=sqrt(As*4/pi)


	@property
	def di(self):
		return self._di
	@di.setter
	def di(self,di):
		self._di=di
		self._As=self.di**2*pi/4

	def plot(self,dr=True,*args,**kargs):
		r=sqrt(self.As/pi)
		cr=Circle(self.co,r)
		gca().add_patch(cr)
		if dr:
			draw()
			axis('equal')

	def __repr__(self):
		return '%d d %d ,As=%4g, co=%4g,%4g'%(self.n,self.di,self.As,self.co[0],self.co[1])
		

if __name__=='__main__':
#	sez=sezione(r_[[[0.,0],[-e00,0],[-500,1000],[0,1000]]])
	close('all')
	sez=sezione('rec',1000.,500.)
	cls=calcestruzzo(30.)
	sez.mat=cls
	barre=[barra(r_[0,-40.],12,5), barra(r_[0,-460.],14,5)]
	arm=armatura(barre)
	sez.armatura=arm
	print(sez.fessura(50000,10000000))
