#-*-coding:utf-8-*-

from pylab import *
from scipy import *
from scipy.optimize import leastsq , fmin , minimize , root , anneal ,brute
import gueng.geometrie
from pdb import set_trace
ion()
'''
DA FARE
Per lo stato limite ultimo
1) valutazione delle resistenze ultime date le deformazioni
2) calcolo della deformazione nella sezione nei vari campi
3) automatizzazione della ricerca dello SLU

Per la sezione in generale
1) ridefinizione iniziale della sezione [tipi standard rettngolare,T ecc..]
2) definizione della rotazione della sezione (sulla sezione gia ereditato da trasportare sulle armature)
3) valutazione domini di rottura e resistenze ultime in asse Y (poca roba avendo definito 2)
'''

class sezione (object,gueng.geometrie.area):
	__version__='1.1'
	def __init__(self,tipo,*args):
		choose={'rec':self.__init_rect , 'T':self.__init_T}
		if tipo in list(choose.keys()):
			choose[tipo](*args)
		else:
			gueng.geometrie.area.__init__(self,*args)
		self.seziona()

	def __init_rect(self,*args):
		b,h=args
		punti=r_[[[-b/2,0.],[-b/2,-h],[b/2,-h],[b/2,0]]]
		gueng.geometrie.area.__init__(self,punti)
	
	def __init_T(self,*args):
		pass

	
	def seziona(self,n=200):
		'''divide la sezione in n striscie  parallele asse x e
		calcola la larghezza di ogni striscia'''
		h=linspace(self.limiti[2],self.limiti[3],n+1)
		h=h[:-1]+(h[1]-h[0])/2
		b=r_[[self.b(i) for i in h]]
		self.sezioni=r_[[h,b]]
		
	
	def sc(self,ecu,ecd):
		'''valuta la tensione all'interno del calcestruzzo
		ecu,ecd= deformazione superiore deformazine inferiore (up-down)'''
		ec=linspace(ecd,ecu,self.sezioni.shape[1]+1)
		ec=ec[:-1]+(ec[1]-ec[0])/2
		sc=self.mat.sigma(ec)
		return sc

	def Nc(self,ecu,ecd):
		'''valuta la componente normale dovuta al calcestruzzo'''
		sc=self.sc(ecu,ecd)
		return (sc*self.sezioni[1,:]*(self.sezioni[0,1]-self.sezioni[0,0])).sum()

	def Mc(self,ecu,ecd):
		'''valuta il momento dovuto al calcestruzzo'''
		sc=self.sc(ecu,ecd)
		return (sc*self.sezioni[1,:]*(self.sezioni[0,1]-self.sezioni[0,0])*(self.sezioni[0,:]-self.G[1])).sum()

	def Myc(self,ecu,ecd):
		'''calcola la componente My del momento per sezioni assimmetriche)'''
		pass
	
	@property
	def armatura(self):
		return self._armatura
	@armatura.setter
	def armatura(self,arm):
		self._armatura=arm
		self._armatura.parent=self
		self._armatura.aggiorna()

	def ss(self,ecu,ecd):
		'''valuta le tensioni allinterno delle armature'''
		es=self.armatura.es(ecu,ecd) #lo chiedo ovviamente alle armature...
		ss=self.armatura.sigma(es)
		return ss

	def Ns(self,ecu,ecd):
		'''Valuta la componete normale dovuta alle armature'''
		es=self.armatura.es(ecu,ecd)
		Ns=self.armatura.N(es)
		return Ns.sum()

	def Ms(self,ecu,ecd):
		'''Valuta il momento dovuto alle armature'''
		es=self.armatura.es(ecu,ecd)
		Ms=self.armatura.M(es)
		return Ms.sum()
	
	def Mys(self,ecu,ecd):
		'''valuta il momento rispetto ad y dovuto alle armature ( per sezioni assimmetriche)'''
		pass

	def N(self,ecu,ecd):
		return self.Nc(ecu,ecd)+self.Ns(ecu,ecd)

	def M(self,ecu,ecd):
		return self.Mc(ecu,ecd)+self.Ms(ecu,ecd)

	def dN(self,ec,N=0):
		return self.N(*ec)-N
	
	def dN2(self,ecs,eci,N):
		return self.N(ecs,eci)-N

	def dM(self,ec,M=0.):
		return self.M(*ec)-M
	
	def sle(self,ec,N=0.,M=0):
		return r_[self.dN(ec,N),self.dM(ec,M)]
	def cerca_sle(self,N=0,M=0):
		ec=leastsq(self.sle,r_[.0,.0],(N,M))
		return ec[0]
	
	def slu(self,eci,N=0.):
		try:eci=eci[0]
		except:pass
		ecs=root(self.dN2,0.,args=(eci,N)).x
		if not -.01<ecs<.0035:
			ecs=0
		M=self.M(ecs,eci)
		return -M
	
	def campo_1(self,N=None):
		'tutta trazione'
		eud=r_[[i.mat.eud for i in self.armatura.barre]].min()
		if N==None:	#restituisco i limiti di N che caratterizzano il campo 1
			ecu=ecd=-eud
			N=[self.N(ecu,ecd),0]
			ecu=0
			N[1]=self.N(ecu,ecd)
			return N
		else:#cerco le deformazioni per cui eguaglio il momento
			ecd=eud
			def zerome(ecu,N):
				return self.N(ecu,ecd)-N
			ecu=root(zerome,eud,args=(N)).x
#			ecu=root(zerome,0,args=(N)).x
			return ecu,ecd
	
	def campo_2(self,N=None):
		eud=[[i.co[1],i.mat.eud] for i in self.armatura.barre]
		eud.sort()
		eud=eud[0][1]
		ecu=self.mat.ecu
		if N is None:
			N=[self.N(0,-eud),0]
			N[1]=self.N(ecu,-eud)
			return N
		else:
			def zerome(ecu,N):
				return self.N(ecu,-eud)-N
			ecu=root(zerome,ecu,args=(N,)).x[0]
			return ecu,-eud
	
	def campo_3_4_5(self,N=None):
		eud=[[i.co[1],i.mat.eud] for i in self.armatura.barre]
		eud.sort()
		eud=eud[0][1]
		eyd=[[i.co[1],i.mat.eyd] for i in self.armatura.barre]
		eyd.sort()
		eyd=eyd[0][1]
		ecu=self.mat.ecu
		if N is None:
			N=[self.N(ecu,-eud) , self.N(ecu,0)]
			return N
		else:
			def zerome(ecd,N):
				return self.N(ecu,ecd)-N
			ecd=root(zerome,0,args=(N,)).x[0]
			return ecu,ecd

	def campo_6(self,N=None):
		ecu=self.mat.ecu
		ec=self.mat.ec
		if N is None:
			N=[self.N(ecu,0),self.N(ec,ec)]
			return N
		else:
			def zerome(p,N):
				esup=ecu-p*(ecu-ec)
				einf=ec*p
				return self.N(esup[0],einf[0])-N
			p=root(zerome,.5,args=(N,)).x[0]
			esup=ecu-p*(ecu-ec)
			einf=ec*p
			return esup,einf
	
	def cerca_slu(self,N=0):
		test=[self.campo_1,self.campo_2,self.campo_3_4_5,self.campo_6]
		check=False
		for i in test:
			val=i()
			if val[0]==val[1]:continue
			if val[0]<=N<=val[1]:
				e=i(N)
				check=True
				break
		if check:
			return self.M(*e)
		else:
			print('sezione rotta')
			return None

	def dominio_MN(self,n=20,doppio=True):
		N=linspace(min(self.campo_1()),max(self.campo_6()),n)
		M=vectorize(self.cerca_slu)
		M=M(N)
		if doppio:
			s2=self.ruota(pi)
			n2,m2=s2.dominio_MN(n,doppio=False)
			N=r_[N,n2[-1::-1]]
			M=r_[M,-m2[-1::-1]]
		return N,M

			


	
	def cerca_slu_(self,N=0):
		M=brute(self.slu,(r_[-.01,.0035],),(N,),full_output=True)[1]
		return -M
	def cerca_slu_2(self,N=0.):
		#lo calcolo per pentacotomia..... valuto in 10 punti iterativamente
		#lento e non copre bene tutto l'intervallo... devo scrivere bene tutto con calcma...
		#funziona bene solo per travi, per colonne da problemi
		eci=linspace(-.01,.004,10)
		M=r_[[self.slu(i,N) for i in eci]]
		er=1
		while er>1e-4:
			c=eci[M==M.min()]
			d=eci[1]-eci[0]
			eci=linspace(c-d,c+d,10)
			M=r_[[self.slu(i,N) for i in eci]]
			er=abs(M.ptp()/M.min())
#		eci=minimize(self.slu,.0035,(N,),method='anneal',bounds=((-.1,.0035),)).x
#		ecs=root(self.dN2,0,(eci,N)).x
		return -M.min()

	def cerca_slu_3(self,N=0):
		#più lento di pentacotomia e brute 
		M=minimize(self.slu,0.,(N,)).fun
		return -M


	def fessura(self,N,M,Es=210000,kt=.4,k2=.5):
		b=self.ptp[0]
		h=self.ptp[1]
		d=abs(self.armatura.b.min()+self.G[1])
		c=h-d
		fck=self.mat.fck
		ec=self.cerca_sle(N,M)
		ss=abs(self.ss(*ec)).max()
		y0=interp(0,[min(ec),max(ec)],[h,0])
		#diametro equivalente
		#considero tutte le barre tese con b<y0-G
		As=sum([i.As for i  in self.armatura.barre if i.co[1]<=-y0])
		phi=As*4/pi/sum([i.di*i.n for i  in self.armatura.barre if i.co[1]<=-y0])

		Aec=min([ 2.5*(h-d) , (h-y0)/3 , h/2])*b #area efficace di calcestruzzo per sezione rettangolare... andrebbe ridefinita... per le sezioni generiche
		reff=As/Aec
		fcm=fck+8
		fctm=.3*fck**(2./3)
		Ecm=22000*(fcm/10)**.3
		ale=Es/Ecm
		esm=max([(ss-kt*fctm/reff*(1-ale*reff))/(Es),
		0.6*ss/Es]) #deformazione unitaria delle barre
		dsmax=1.3*(h-y0)# distanza tra fessure
		wd=esm*dsmax
		#apertura fessura in prossimità della barra.
		k1=.8
		k3=3.4
		k4=.425
		dsmax2=k3*c+k1*k2*k4*phi/reff
		wd2=esm*dsmax2
		print(locals())
		return [wd2,wd,5*(c+phi/2),locals()]

	def ruota(self,ang):
		#eseguo la rotazione ereditata:
		ns=gueng.geometrie.area.ruota(self,ang)
		ns.seziona()
		if hasattr(ns,'armatura'):
			ns.armatura.parent=ns
			ns.armatura.ruota(ang)
		return ns	

	def plot(self,*args):
		gueng.geometrie.area.plot(self,*args)
		if hasattr(self,'armatura'):
			self.armatura.plot()
	
class calcestruzzo:
	gamma_c=1.6 
	ec=.002
	ecu=.0035
	def __init__(self,Rck,fck=0):
		self.Rck=Rck
		if not fck:
			self.fck=.83*Rck
		self.fcm=fck+8
		if self.Rck<=60:
			self.fctm=0.3*self.fcm**(2./3)
		else:
			self.fctm=2.12*log(1+self.fcm/10)
		self.fctk=.7*self.fctm
		self.fctk_=1.3*self.fctm
		self.Ecm=22000*(self.fcm/10)**.3
		self.G=self.Ecm/(2*(.2+1))

	def __sigma(self,ec,fcd=0,alpha=.85):
		'''calcola la tensione nel calestruzzo in un punto'''
		if not 0< ec < calcestruzzo.ecu:
			return 0.
		if not fcd:
			fcd=self.fck/calcestruzzo.gamma_c
		if 0<ec< calcestruzzo.ec:
			return 1000*ec*alpha*fcd*(-250*ec+1)
		elif calcestruzzo.ec<=ec<=calcestruzzo.ecu:
			return fcd*alpha
	__sigma_=vectorize(__sigma)	#versione vettorizzata della __sigma, in questo modo richiede come input self ed ec obbligatori	
	def sigma(self,ec,fcd=0,alpha=.85): #versione vettorizzata di sigma che si comporta correttamente come __sigma in fase di chiamata
		'''calcola la tensione del calcestruzzo nota la deformazione'''
		return self.__sigma_(self,ec,fcd,alpha)



class armatura:
	def __init__(self,barre=[],parent=None):
		self.barre=barre
		self.parent=parent
		
	def aggiorna(self):
		if self.parent:
			self.G=self.parent.G[1]
			self.limiti=self.parent.limiti[2:]
		else:
			self.G=0
			self.limiti=r_[0,0.]
		self.aree=r_[[i.As for i in self.barre]]
		self.eu=r_[[i.mat.eud for i in self.barre]]
		self.ey=r_[[i.mat.eyd for i in self.barre]]
		self.fyd=r_[[i.mat.fyd for i in self.barre]]
		self.b=r_[[i.co[1] for i in self.barre]]-self.G## bracci delle barre di armatura]
		self.E=r_[[i.mat.Es for i in self.barre]]
		
	def es(self,sup,infe):
		b=r_[[i.co[1] for i in self.barre]]
		return interp(b,[self.limiti[0],self.limiti[1]],[infe,sup]).astype(float)

	def sigma(self,es):
		'''calcola la tensione nelle barre'''
		return (abs(es)<=self.eu) * ((abs(es)< self.ey) * self.E*es + (abs(es)>=self.ey) * sign(es)*self.fyd)

	def N(self,es):
		'''calcola lo sforzo normale nelle barre'''
		return self.aree* self.sigma(es)
	
	def M(self,es,G=0):
		'''calcola il momento flettente dovuto alle barre'''
		return self.N(es)*(self.b-G)

	def plot(self,*args,**kargs):
		for i in self.barre:
			i.plot(*args,**kargs)
	
	def ruota(self,rot):
		if not hasattr(rot,'shape'):
			rot=r_[[[cos(rot),sin(rot)],[-sin(rot),cos(rot)]]]
		for b in self.barre:
			b.co=dot(b.co,rot)
		self.aggiorna()	

class acciaio:
	fyk=450.
	ftk=540.
	euk=0.079
	eud=euk*.9
	Es=210e3
	eyk=fyk/Es
	fyd=fyk/1.15
	eyd=fyd/Es
	def __init__(self,*args):
		pass
	def sigma(self,es):
		return (abs(es)<=self.eud) * ((abs(es)<self.eyd) * self.Es*es + (abs(es)>=self.eyd)* ones_like(es)*self.fyd)

class barra(object):
	mat=acciaio('b450c')
	def __init__(self,co,di=None,n=1,As=None,mat=None):
		'''co= coordinate barra
		di=diametro
		As=area [opzionale]
		mat=materiale

		As viene calcolata automaticamente in base al diametro
		se di=0 e As!=0 viene calcolato un diametro equivalente
		mat per default Ã¨ b450c'''
		self.co=co
		if di:
			self.di=di
		elif As:
			self.As=As
		if mat:
			self.mat=mat
		self.n=n

	@property
	def As(self):
		return self._As*self.n
	@As.setter
	def As(self,As):
		self._As=As
		self._di=sqrt(As*4/pi)


	@property
	def di(self):
		return self._di
	@di.setter
	def di(self,di):
		self._di=di
		self._As=self.di**2*pi/4

	def plot(self,*args,**kargs):
		r=sqrt(self.As/pi)
		cr=Circle(self.co,r)
		gca().add_patch(cr)
		draw()
		axis('equal')
		

if __name__=='__main__':
#	sez=sezione(r_[[[0.,0],[-500,0],[-500,1000],[0,1000]]])
	close('all')
	sez=sezione('rec',1000.,500.)
	cls=calcestruzzo(30.)
	sez.mat=cls
	barre=[barra(r_[0,-40.],12,0), barra(r_[0,-460.],14,5)]
	arm=armatura(barre)
	sez.armatura=arm
#	print sez.fessura(50000,10000000)
#	print sez.cerca_slu(0)
