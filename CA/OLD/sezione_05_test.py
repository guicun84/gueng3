#-*-coding:utf-8-*-

from pylab import *
from scipy import *
from scipy.optimize import root,fminbound,leastsq
import gueng.geometrie
from pdb import set_trace
from copy import deepcopy
ion()
'''
DA FARE
Per lo stato limite ultimo

Per la sezione in generale
1) ridefinizione iniziale della sezione [tipi standard rettngolare,T ecc..]
3) valutazione domini di rottura e resistenze ultime in asse Y (poca roba avendo definito 2)

FATTO:
1) le deformazioni inferiori sono riferite alla barra più bassa e non al bordo della sezione
2) implementati i vari campi della sezione (100x piu veloce :-P )

'''

class SezionePiana:
	def __init__(self):
		self.objects=[]
		self.p_sup=None #punto di controllo superiore
		self.p_inf=None	#punto di controllo inferiore
		self.e_sup=None	#deformata punto inferiore
		self.e_inf=None	#deformata punto superiore

	def __add__(self,val):
		self.objects.append(val)
		if hasattr(val,'parent'):
			val.parent=self
			
	def __sub__(self,val):
		for n,i in enumerate(self.objects):
			if i is val:
				del self.objects[n]
				self.aggiorna()
	
	def aggiorna(self):
		for i in self.objects:
			if hasattr(i,'aggiorna'):
				i.aggiorna()
	
	def deforma(self,e_sup,e_inf):
		'''imposta le deformazioni della sezione nei punti di controllo self.p_sup,self.p_inf'''
		self.e_sup=e_sup
		self.e_inf=e_inf
	
	def e(self,y): 
		'''valuta la deformazione in qualsiasi punto della sezine
		la sezione è deformata in modo piano in base alle deformazioni imposte con self.deforma'''
		return self.e_sup+(self.e_inf-self.e_sup)/(self.p_inf-self.p_sup)*(y-self.p_sup)

	def N(self,e_sup=None,e_inf=None):
		if e_sup is not None:
			self.deforma(e_sup,e_inf)
		N=sum([i.N() for i in self.objects])
		return N

	def M(self,e_sup=None,e_inf=None):
		if e_sup is not None:
			self.deforma(e_sup,e_inf)
		M=[i.M() for i in self.objects]
		return sum(M)
	
	def yn_(self): #DA RICONTROLLARE
		self.yn= self.p_sup-(self.p_inf-self.p_sup)/(self.e_inf-self.e_sup)*self.e_sup

	@property
	def chi(self):
		if self.p_sup is not None:
			return (self.e_sup-self.e_inf)/(self.p_sup-self.p_inf)
		else:
			return 0


	def ruota(self,ang):
		ns=deepcopy(self)
		for n,i in enumerate(ns.objects):
			ns.objects[n]=i.ruota(ang)
			ns.objects[n].parent=ns
		ns.aggiorna()
		return ns


	def plot(self,*args):
		for i in self.objects:
			i.plot(*args)
		grid()
		try:
			plot(self.G[0],self.G[1],'xr')
		except:
			pass

	def stato(self):
		'''ritorna una stringa preformattata con le caratteristiche dello stato della sezione'''
		scmax=r_[[i.sc().max() for i in self.ca]].max()
		ssmax=-r_[[i.sigma().min() for i in self.objects if isinstance(i,Armatura)],[i.sc().min() for i in self.ac if isinstance(i,Sezione)]].min()
		return '''%epsilon_"sup"={e_sup:.4g} , %epsilon_inf={e_inf:.4g}
yn= {ync:.2f} mm , %chi= {chi:.4g} mm^-1
%sigma_{{C max}}={scmax:.2f} MPa , %sigma_{{S max}}= {ssmax:.2f} MPa
M={M:.1f} Nm , N={N:.1f} N'''.format(ync=self.p_sup-self.yn,N=self.N(),M=self.M()/1e3,scmax=scmax,ssmax=ssmax,chi=self.chi,**vars(self))


		

	def plot_tensioni(self,*args):
		for i in self.objects:
			i.plot_tensioni()
		grid()
		axvline(0,color='k')
		xlabel('Tensione [MPa]')
		ylabel('Quota [mm]')

class SezioneCA(SezionePiana,object):
	def __init__(self):
		SezionePiana.__init__(self)
		self._aggiornata=False
	def trova_limiti(self):
		if self.objects:
			sezioni=self.ca#[i for i in self.objects if isinstance(i,Sezione)]
			armature=self.ac#[i for i in self.objects if isinstance(i,Armatura)]
			try:
				sup_sez=max([i.limiti[3] for i in sezioni])
				inf_sez=min([i.limiti[2] for i in sezioni])
			except:
				sup_sez=inf_sez=None
			sup_arm=None
			inf_arm=None
			try:
				for arm in armature:
					if isinstance(arm,Armatura):
						sup=max([i.co[1] for i in arm.barre])
						infe=min([i.co[1] for i in arm.barre])
					else:
						sup=arm.co[0]
						infe=arm.co[1]
					if sup_arm is not None:
						sup_arm=max([sup_arm,sup])
						inf_arm=min([inf_arm,infe])
					else:
						sup_arm=sup
						inf_arm=infe
			except:pass
			#a questo punto ho trovato tutti i limiti estremi degli oggetti.
			#visto che armature e barre sono parenti stretti i loro limiti li metto direttamente a confronto per avere il limite massimo
			#finito con le armature e le barre considero i limiti della sezione se c'è
			if sup_sez is not None:
				self.p_sup=sup_sez
				self.p_inf=inf_sez
			if self.p_sup is None and sup_arm is not None:
				self.p_sup=sup_arm
			if inf_arm is not None:
				self.p_inf=inf_arm
			self.ptp=r_[r_[[i.sezioni[:,1] for i in self.ca]].flatten().max() , self.p_sup-self.p_inf]
	def calcola_G(self):
		sezioni=[i for i in self.objects if isinstance(i,Sezione)]
		G=r_[[i.G for i in sezioni]]
		A=r_[[i.A*i.moltiplicatore for i in sezioni]]
		self.G=r_[[i*j for i,j in zip(G,A)]].sum(0)/sum(A)
	
	def aggiorna(self):
		self.trova_limiti()
		self.calcola_G()
		[i.aggiorna() for i in self.objects if isinstance(i,Armatura)]
		self._aggiornata=True

	def aggiorna_tutto(self):
		self.aggiorna()
		SezionePiana.aggiorna(self)

	@property
	def ca(self):
		ca=[i for i in self.objects if hasattr(i,'mat')]
		return [i for i in ca if isinstance(i.mat,Calcestruzzo)]
	
	@property
	def ac(self):
		ac=[i for i in self.objects if hasattr(i,'mat')]
		ac= [i for i in ac if isinstance(i.mat,Acciaio)]
		ac.extend(self.barre)
		return ac

	@property
	def barre(self):
		armature=[i for i in self.objects if isinstance(i,Armatura)]
		barre=[]
		for arm in armature:
			barre.extend(arm.barre)
		return barre

	
	def campo_1(self,N=None):
		'tutta trazione'
		eud=r_[[i.mat.eud for i in self.ac]].min()
		if N==None:	#restituisco i limiti di N che caratterizzano il campo 1
			ecu=ecd=-eud
			N=[self.N(ecu,ecd),0]
			ecu=0
			N[1]=self.N(ecu,ecd)
			return N
		else:#cerco le deformazioni per cui eguaglio il momento
			ecd=eud
			def zerome(ecu,N):
				return (self.N(ecu,ecd)-N)**2
			ecu=fminbound(zerome,-eud,0,args=(N))
#			ecu=root(zerome,0,args=(N)).x
			return ecu,ecd

	def campo_2(self,N=None):
		eud=[[i.co[1],i.mat.eud] for i in self.ac]
		eud.sort()
		eud=eud[0][1]
		ecu=[i.mat.ecu for i in self.ca]
		ecu=min(ecu)
		if N is None:
			N=[self.N(0,-eud),0]
			N[1]=self.N(ecu,-eud)
			return N
		else:
			def zerome(ecu,N):
				return (self.N(ecu,-eud)-N)**2
			ecu=fminbound(zerome,0,ecu,args=(N,))
			return ecu,-eud

	def campo_3_4_5(self,N=None):
		eud=[[i.co[1],i.mat.eud] for i in self.ac]
		eud.sort()
		eud=eud[0][1]
		eyd=[[i.co[1],i.mat.eyd] for i in self.ac]
		eyd.sort()
		eyd=eyd[0][1]
		ecu=min([i.mat.ecu for i in self.ca])
		if N is None:
			N=[self.N(ecu,-eud) , self.N(ecu,0)]
			return N
		else:
			def zerome(ecd,N):
				return (self.N(ecu,ecd)-N)**2
			ecd=fminbound(zerome,-eud,0,args=(N,))
			return ecu,ecd

	def campo_6(self,N=None):
		ecu=min([i.mat.ecu for i in self.ca])
		ec=min([i.mat.ec for i in self.ca])
		if N is None:
			N=[self.N(ecu,0),self.N(ec,ec)]
			return N
		else:
			def zerome(p,N):
				esup=ecu-p*(ecu-ec)
				einf=ec*p
				return (self.N(esup[0],einf[0])-N)**2
			p=root(zerome,0,1,args=(N,))
			esup=ecu-p*(ecu-ec)
			einf=ec*p
			return esup,einf

	def Mxrd(self,N=0):
		if not self._aggiornata:
			self.aggiorna()
		test=[self.campo_2,self.campo_2,self.campo_3_4_5,self.campo_6]
		check=False
		for i in test:
			val=i()
			if val[0]==val[1]:continue
			if val[0]<=N<=val[1]:
				e=i(N)
				check=True
				break
		if check:
			M=self.M(*e)
			self.yn_()
			return M
		else:
			print('sezione rotta')
			return 0

	def dominio_MN(self,n=20,doppio=True):
		'''n numero di punti in cui valutare il dominio 
		doppio: True per eseguire il calcolo anche del momento negativo.'''
		if not self._aggiornata:
			self.aggiorna()
		N=linspace(min(self.campo_1()),max(self.campo_6()),n)
		M=vectorize(self.Mxrd)
		M=M(N)
		if doppio:
			s2=self.ruota(pi)
			n2,m2=s2.dominio_MN(n,doppio=False)
			N=r_[N,n2[-1::-1]]
			M=r_[M,-m2[-1::-1]]
		return N,M

	def fessura(self,M,N=0,c=3,Es=210000,kt=.4,k2=.5):
		'''c=copriferro'''
		if not self._aggiornata:
			self.aggiorna()
		b=self.ptp[0]
		h=self.ptp[1]
#		d=abs(self.armatura.b.min()+self.G[1])
		if c is not None:
			d=h
			h+=c
		else:
			d=h-min([i.co[1] for i in self.barre])
			c=h-d
		fck=min([i.mat.fck for i in self.ca])
		ec=self.SLE(M,N)[0]
		ss=r_[[i.mat.sigma(self.e(i.co[1])) for i in self.barre]]
		ss=(-1*ss[ss<0]).max()
#		ss=abs(self.ss(*ec)).max()
		y0=interp(0,[min(ec),max(ec)],[h,0])
		#diametro equivalente
		#considero tutte le barre tese con b<y0-G
		As=sum([i.As for i  in self.barre if i.co[1]<=(self.p_sup-y0)])
		phi=As*4/pi/sum([i.di*i.n for i  in self.barre if i.co[1]<=(self.p_sup-y0)])

		Aec=min([ 2.5*(h-d) , (h-y0)/3 , h/2])*b #area efficace di calcestruzzo per sezione rettangolare... andrebbe ridefinita... per le sezioni generiche
		reff=As/Aec
		fcm=fck+8
		fctm=.3*fck**(2./3)
		Ecm=22000*(fcm/10)**.3
		ale=Es/Ecm
		esm=max([(ss-kt*fctm/reff*(1-ale*reff))/(Es),
			0.6*ss/Es]) #deformazione unitaria delle barre
		dsmax=1.3*(h-y0)# distanza tra fessure
		wd=esm*dsmax #ampiezza lontano dalle barre
		#apertura fessura in prossimità della barra.
		k1=.8
		k3=3.4
		k4=.425
		dsmax2=k3*c+k1*k2*k4*phi/reff #vicino alle barre
		wd2=esm*dsmax2 #vicino alle barre
#		print locals()
		st_out='''
base={b:.4g} mm
altezza={h:.4g} mm
copriferro={c:.4g} mm
f_ck={fck:.4g} MPa
f_cm={fcm:.4g} MPa
f_ctm={fctm:.4g} MPa
E_cm={Ecm:.4g} MPa
{stato}
y0={y0:.4g}mm
As={As:.4g} mm^2
%phi={phi:.4g}
A_ec={Aec:.4g} mm^2 area efficace di calcestruzzo 
r_eff={reff:.4g} 
%alpha_le={ale:.4g}
%epsilon_sm={esm:.4g} deformazione unitaria delle barre
d_{{s,max}}={dsmax:.4g} mm distanza tra le fessure
w_d={wd:.4g} ampiezza fessure lontano dalle barre
k1={k1:.4g} ; k2={k2:.4g} ; k4={k4:.4g}
d_{{s,max 2}}={dsmax2:.4g} mm apertura fessure vicino alle barre
w_d2={wd2:.4g} mm dimensione trasversale per cui si ha fessura vicino alle barre
'''.format(stato=self.stato(),**locals())
		return [wd,wd2,5*(c+phi/2),locals()]
	
	def SLE(self,M=0,N=0,start=r_[.0001,0.0001]): #da affinare...
		if not self._aggiornata:
			self.aggiorna()
		if N==0:N=1e-9*self.campo_6()[1]
		if M==0:M=1e-9*self.Mxrd()
		def minimizzami(e,M,N):
			self.deforma(*e)
			out= (r_[self.M(),self.N()]/r_[M,N])-r_[1.,1.]
#			out= (r_[self.M(),self.N()]-r_[M,N])
			return out
		if start is None:
			start=r_[self.e_sup,self.e_inf]
			if all(start==0): start=r_[.0001,.0001]
		e=leastsq(minimizzami,r_[.0001,-.0001].astype(float),args=(M,N),xtol=0,ftol=1e-8,maxfev=int(1e6),full_output=True)
#		e=root(minimizzami,r_[.001,-.001].astype(float),args=(M,N))
		self.deforma(*e[0])
		self.yn_()
		return e

class Sezione (object,gueng.geometrie.area):
	__version__='1.1'
	def __init__(self,tipo,*args,**kargs):
		choose={'rec':self.__init_rect , 
			    'T':self.__init_T}
		if tipo in list(choose.keys()):
			choose[tipo](*args,**kargs)
		else:
			gueng.geometrie.area.__init__(self,*args)
		self.num_part=200
		self.parent=None
		vars(self).update(kargs)
		self.seziona()
		self.moltiplicatore=1 #nel calcolo del baricentro viene preso in considerazione questo valore moltiplicato per l'area, per le resistenze solo il modulo

	def __init_rect(self,*args):
		b,h=args
		punti=r_[[[-b/2,0.],[-b/2,-h],[b/2,-h],[b/2,0]]]
		gueng.geometrie.area.__init__(self,punti)

	def __init_T(self,*args):
		pass
	
	def seziona(self,n=None):
		'''divide la sezione in n striscie  parallele asse x e
		calcola la larghezza di ogni striscia'''
		if n == None:
			n=self.num_part
		else:
			self.num_part=n
		h=linspace(self.limiti[2],self.limiti[3],n+1)
		b=r_[[self.b(i) for i in h]]
		self.sezioni=r_[[h,b]]

	def sc(self,ec=None):
		if ec is None:
			ec=self.parent.e(self.sezioni[0,:])
		return self.mat.sigma(ec)
	
	def aggiorna(self):
		self.seziona()
		
	def N(self,ec=None):
		'''valuta la componente normale dovuta al calcestruzzo'''
		sc=self.sc(ec)
		return trapz(sc*self.sezioni[1,:],self.sezioni[0,:])*sign(self.moltiplicatore)

	def M(self,ec=None):
		'''valuta il momento dovuto al calcestruzzo'''
		sc=self.sc(ec)
		if self.parent is not None:
			YG=self.parent.G[1]
		else:
			YG=self.G[1]
		return trapz(sc*self.sezioni[1,:]*(self.sezioni[0,:]-YG),self.sezioni[0,:])*sign(self.moltiplicatore)

	def My(self,ecu,ecd):
		'''calcola la componente My del momento per sezioni assimmetriche)'''
		pass

	@property
	def co(self): #espediente per ordinarli dal basso verso l'alto
		return r_[self.limiti[3],self.limiti[2]]
	

	def ruota(self,ang):
		#eseguo la rotazione ereditata:
		ns=gueng.geometrie.area.ruota(self,ang)
		ns.seziona()
		if hasattr(ns,'armatura'):
			ns.armatura.parent=ns
			ns.armatura.ruota(ang)
		return ns	

	def plot(self,*args):
		gueng.geometrie.area.plot(self,*args)
		if hasattr(self,'armatura'):
			self.armatura.plot()
	
	def plot_tensioni(self,*args):
		y=self.sezioni[0,:]
		x=self.sc()
		plot(x,y,*args)

class SezioneCirc(gueng.geometrie.Cerchio,Sezione):
	def __init__(self,de,di=0,G=r_[0,0.],**kargs):
		vars(self).update(locals())
		gueng.geometrie.Cerchio.__init__(self,de,di,G)
		self.num_part=201
		self.parent=None
		self.moltiplicatore=1
		vars(self).update(kargs)
		self.seziona()
	
	def seziona(self,n=None):
		if n is None:
			n=self.num_part
		l=self.limiti
		y=linspace(l[2],l[3],n)
		b=r_[[self.b(i) for i in y]]
		self.sezioni=r_[[y,b]]

	def G(self):
		return gueng.geometrie.Cerchio.G(self)

	def plot(self):
		gueng.geometrie.Cerchio.plot(self)


class Calcestruzzo(object):
	gamma_c=1.6 
	ec=.002
	ecu=.0035
	def __init__(self,Rck=None,fck=None,FC=1.,Ecm=None):
		'''Rck fck: resistenza del calcestruzzo [uno dei due],
		FC=fattore di confidenza
		Ecm=modulo di Young (per cl alleggeriti)'''
		self.__Rck=Rck
		self.__fck=fck
		self.__FC=FC
		self.__Ecm=Ecm
		if fck is None:
			self.__fck=.83*Rck
		else:
			self.__Rck=fck/.83
		self.calcola()
	
	def calcola(self):
		self.fck=self.__fck/self.FC
		self.Rck=self.__Rck/self.FC
		self.fcm=self.__fck+8 
		if self.Rck<=60:
			self.fctm=0.3*self.fcm**(2./3)
		else:
			self.fctm=2.12*log(1+self.fcm/10)
		self.fctk=.7*self.fctm
		self.fctk_=1.3*self.fctm
		if self.__Ecm is None:
			self.Ecm=22000*(self.fcm/10)**.3
		else:
			self.Ecm=self.__Ecm
			es=(-2+sqrt(2**2-4*-1*-.4))/(2*-1)
			ec=0.4/es/self.Ecm*self.fcm
			if ec<=self.ecu: self.ec=ec
			else:self.ec=self.ecu
		self.G=self.Ecm/(2*(.2+1))	
		self.calcolaFcd()

	def calcolaectu(self,rho):	
		#calcolo della deformazione ultima a trazione del calcestruzzo,
		#modello proposto da Guyon
		rho=min([.01,rho])
		self.ectu=0.0001*(2.5-(100*rho-1)**2)


	def calcolaFcd(self,mode='SLU',mult=1):
		if type(mode)==str:
			mode=mode.upper()
		if mode=='SLU' or mode=='D':
			self.fcd=self.fck/self.gamma_c*.85*mult
		elif mode=='SLE':
			self.fcd=self.fcm*mult
		elif mode=='K':
			self.fcd=self.fck*mult
		elif mode=='M':
			self.fcd=self.fcm*mult

	def _Ecm(self):
		def zerome(x):
			return self.sigma(x)-.4*self.fcd
		x=root(zerome,self.ec/2)
		if x.status:
			return .4*self.fcd/x.x[0],x
		else:
			return x
	
	@property
	def FC(self):
		return self.__FC
	
	@FC.setter
	def FC(self,val):
		self.__FC=val
		self.calcola()
	
	def __sigma(self,ec):
		'''calcola la tensione nel calestruzzo in un punto'''
		if not 0< ec <=self.ecu:
			return 0.
		if 0<ec< self.ec:
			return (-1/self.ec**2*ec**2 + 2/self.ec*ec)*self.fcd
		elif self.ec<=ec<=self.ecu:
			return self.fcd

	__sigma=vectorize(__sigma,otypes=(float,))	#versione vettorizzata della __sigma, in questo modo richiede come input self ed ec obbligatori	
	def sigma(self,ec): #versione vettorizzata di sigma che si comporta correttamente come __sigma in fase di chiamata
		'''calcola la tensione del calcestruzzo nota la deformazione'''
		return self.__sigma(self,ec)

	def __str__(self):
		if self.__FC!=1:
			st='<#FC#> = %.4g\n'%self.__FC
		else:st=''
		st+= '''<#Rck#> = %(Rck).4g MPa
<#f_ck#> = %(fck).4g MPa
<#f_cm#> = %(fcm).4g MPa
<#f_ctm#> = %(fctm).4g MPa
<#f_ctk#> = %(fctk).4g MPa
<#E_cm#> = %(Ecm).4g MPa
<#G#> = %(G).4g MPa'''%vars(self)
		return st

	def plot_diagramma(self,mode='k',mult=1,**kargs):
		opt={'plot_E':bool(self.__Ecm),
			 'plot_ep':True,
			 'plot_info':True}
		opt.update(kargs)
		if not opt['plot_info']:
			for i in list(opt.keys()):
				opt[i]=False
		old_fcd=self.fcd
		self.calcolaFcd(mode,mult)
		ep=linspace(0,self.ecu,100)
		s=self.sigma(ep)
		plot(ep,s)
		if mode=='d':
			text(self.ec,self.fcd,r'$\alpha f_{c%s}$=%.4g MPa'%(mode,self.fcd),horizontalalignment='right')
		else:
			text(self.ec,self.fcd,r'$f_{c%s}$=%.4g MPa'%(mode,self.fcd),horizontalalignment='right')
		#modulo elastico
		if opt['plot_E']:
			E,x=self._Ecm()
			s=self.sigma(x.x)
			plot([0,x.x,x.x],[s,s,0],'--',color='grey')
			plot(x.x,s,'xr')
			plot([0,x.x],[0,s],'r')
			text(.5*x.x,.5*s,r'$E_{cm}$=%(E).4g MPa'%locals())
			text(x.x,s,r'$0.4 f_{c%s}$'%mode)
		#epsilon
		if opt['plot_ep']:
			h=r_[ylim()].ptp()
			plot([self.ec,self.ec],[0,self.fcd],'--',color='grey')
			text(self.ec,.01*h,r'$\epsilon_c$=%.4g'%self.ec,horizontalalignment='right')
			text(self.ecu,.05*h,r'$\epsilon_{cu}$=%.4g'%self.ecu,horizontalalignment='right')
		xlabel(r'$\epsilon$')
		ylabel(r'$\sigma_c [MPa]$')
		grid()
		self.fcd=old_fcd


#se avessi barre con legami differenti in questo momento non posso trattarle...
#la possibilità è pressochè nulla
class Armatura:
	def __init__(self,barre=[],parent=None):
		self.barre=barre
		self.parent=parent
		self.aggiorna()
		
	def aggiorna(self):
		if self.parent:
			self.G=self.parent.G[1]
		else:
			self.G=0
			self.limiti=r_[0,0.]
		self.aree=r_[[i.As for i in self.barre]]
		self.eu=r_[[i.mat.eud for i in self.barre]]
		self.ey=r_[[i.mat.eyd for i in self.barre]]
		self.fyd=r_[[i.mat.fyd for i in self.barre]]
		self.p=r_[[i.co[1] for i in self.barre]]## profondit delle barre di armatura
		self.b=r_[[i.co[1] for i in self.barre]]-self.G## bracci delle barre di armatura]
		self.E=r_[[i.mat.Es for i in self.barre]]

	def sigma(self,es=None):
		'''calcola la tensione nelle barre'''
		if es is None:
			es=self.parent.e(self.p)
		return (abs(es)<=self.eu) * ((abs(es)< self.ey) * self.E*es + (abs(es)>=self.ey) * sign(es)*self.fyd)

	def N_(self,es=None):
		'''calcola lo sforzo normale nelle barre'''
		return self.aree* self.sigma(es)

	def N(self,es=None):
		return sum(self.N_(es))
	
	def M_(self,es=None):
		'''calcola il momento flettente dovuto alle barre'''
		return self.aree * self.sigma(es)* self.b
	def M(self,es=None):
		return sum(self.M_(es))

	def plot(self,*args,**kargs):
		for i in self.barre:
			i.plot(*args,**kargs)
	
	def plot_tensioni(self,*args):
		y=self.p
		x=self.sigma()
		plot(x,y,'x')

	def ruota(self,rot):
		rot=r_[[[cos(rot),sin(rot)],[-sin(rot),cos(rot)]]]
		na=deepcopy(self)
		for b in na.barre:
			b.co=dot(b.co,rot)
		na.aggiorna()	
		return na

	def __str__(self):
		return '\n'.join(['{}'.format(i) for i in self.barre])

class Acciaio:
	def __init__(self,*args,**kargs):
		self.fyk=450.
		self.ftk=540.
		self.euk=0.079
		self.Es=210e3
		self.FC=1
		vars(self).update(**kargs)
		self.calcola()

	def calcola(self):
		self.fyk/=self.FC
		self.ftk/=self.FC
		k=self.ftk/self.fyk
		self.eud=self.euk*.9
		self.eyk=self.fyk/self.Es
		self.fyd=self.fyk/1.15
		self.ftd=self.fyk*k
		self.eyd=self.fyd/self.Es

	def sigma(self,es):
		return (abs(es)<=self.eud) * ((abs(es)<self.eyd) * self.Es*es + (abs(es)>=self.eyd)* sign(es)*self.fyd)

	def __str__(self):
		if self.FC==1:
			st=''
		else:
			st='<#FC#>= %(FC).4g'%vars(seflf)
		st+='''<#f_tk#> = %(ftk).4g MPa
<#f_yk#> = %(fyk).4g MPa
<#f_td#> = %(ftd).4g MPa
<#f_yd#> = %(fyd).4g MPa
<#e_uk#> = %(euk).4g
<#e_ud#> = %(eud).4g
<#e_yk#> = %(eyk).4g
<#e_yd#> = %(eyd).4g
<#E_s#> = %(Es).4g MPa'''%vars(self)
		return st

class AcciaioIncrudente(Acciaio): #acciaio con legame incrudente
	def sigma(self,es):
		return (abs(es)<=self.eud).astype(float) * ((abs(es)<self.eyd).astype(float) * self.Es*es + (abs(es)>=self.eyd).astype(float)* sign(es)*(self.fyd + ((self.ftd-self.fyd)/(self.eud -self.eyd)*(abs(es)-self.eyd) )))

class Barra(object):
	mat=Acciaio('b450c')
	def __init__(self,co,di=None,n=1,As=None,mat=None):
		'''co= coordinate barra
		di=diametro
		As=area [opzionale]
		mat=materiale

		As viene calcolata automaticamente in base al diametro
		se di=0 e As!=0 viene calcolato un diametro equivalente
		mat per default Ã¨ b450c'''
		self.co=co
		if di:
			self.di=di
		elif As:
			self.As=As
		if mat:
			self.mat=mat
		self.n=n

	@property
	def As(self):
		return self._As*self.n
	@As.setter
	def As(self,As):
		self._As=As
		self._di=sqrt(As*4/pi)


	@property
	def di(self):
		return self._di
	@di.setter
	def di(self,di):
		self._di=di
		self._As=self.di**2*pi/4

	def plot(self,*args,**kargs):
		r=sqrt(self.As/pi)
		cr=Circle(self.co,r)
		gca().add_patch(cr)
#		plot(self.co[0],self.co[1],'o')
		draw()
		axis('equal')

	def __str__(self):
		return 'x={:6.2f}\ty={:6.2f}|\tdiametro {di:.2f}|\tnumero= {n:.2f}|\tAs={As:.2f}'.format(self.co[0],self.co[1],di=self.di,As=self.As,n=self.n)
		
def ancoraggio(cls,barra,fyd=391.):
	if hasattr(barra,'di'):
		phi=barra.phi
		fyd=barra.mat.fyd
	else:
		phi=barra
	eta={True:1,False:(132-phi)/100}[phi<=32]
	fbk=2.25*eta*cls.fctk
	fbd=fbk/1.5
	return 	phi**2*pi/4*fyd / (fbd*phi*pi)

if __name__=='__main__':
	#sez=sezione(r_[[[0.,0],[-500,0],[-500,1000],[0,1000]]])
	close('all')
	sez=Sezione('rec',1000.,500.)
	cls=Calcestruzzo(30.)
	sez.mat=cls
	barre=[Barra(r_[0,-40.],12,0), Barra(r_[0,-460.],14,5)]
	arm=Armatura(barre)
	sezca=SezioneCA()
	sezca+sez
	sezca+arm
	print(sezca.objects)

	#print sez.fessura(50000,10000000)
	#print sez.cerca_slu(0)
