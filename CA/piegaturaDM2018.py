from numpy import pi
class Mandrino:
	def __init__(self,d):
		self.dbar=d
		self.d=Mandrino.diametro(d)
		self.dm=Mandrino.diametroMedio(d)
		self.r=Mandrino.raggio(d)
		self.rm=Mandrino.raggioMedio(d)
		self.de=Mandrino.diametroEsterno(d)
		self.re=Mandrino.raggioEsterno(d)
		self.lmin=Mandrino.uncinominimo(d)
		self.lext=Mandrino.uncinominimoesterno(d)
		self.lsv=Mandrino.uncinosviluppo(d)

	@staticmethod
	def diametro(d):
		if d<12: return d*4
		elif d<=16:return d*5
		elif d<=25:return d*8
		elif d<=40:return d*10
		else: raise ValueError(f'diametro {d} non piegabile')

	@staticmethod
	def diametroMedio(d):
		return Mandrino.diametro(d)+d

	@staticmethod
	def diametroEsterno(d):
		return Mandrino.diametro(d)+2*d

	@staticmethod
	def raggio(d):
		return Mandrino.diametro(d)/2

	@staticmethod
	def raggioMedio(d):
		return Mandrino.diametroMedio(d)/2

	@staticmethod
	def raggioEsterno(d):
		return Mandrino.diametroEsterno(d)/2
	@staticmethod
	def uncinominimo(d):
		#lunghezza minima tratto rettilineo uncino (fuori piega)
		return 5*d
	@staticmethod
	def uncinominimoesterno(d):
		#lunghezza minima uncino compresa la piega
		return Mandrino.uncinominimo(d) + Mandrino.raggioEsterno(d)
	@staticmethod
	def uncinosviluppo(d):
		#lunghezza uncino + sviluppo piega
		return Mandrino.uncinominimo(d) + Mandrino.raggioEsterno(d)*pi/2

	def __str__(self):
		return f'''Parametri piegatura barra d {self.dbar}
diametro interno={self.d:.1f}, medio{self.dm:.1f} esterno={self.de:.1f}
raggio   interno={self.r:.1f}, medio{self.rm:.1f} esterno={self.re:.1f}
lunghezza uncino {self.lmin:.1f} , compresa piega {self.lext:.1f} sviluppo={self.lsv:.1f}'''


		
