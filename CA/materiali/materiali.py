#-*-coding:utf-8-*-

from pylab import *
from scipy import *
from scipy.optimize import root,fminbound,leastsq
from scipy.interpolate import interp1d
from scipy.linalg import solve,norm
from scipy.spatial import Delaunay
import gueng.geometrie
from copy import deepcopy
import pandas as pa
import os


class ParabolaRettangolo(object):
	def __init__(self,Rck=None,fck=None,FC=1.,Ecm=None,gamma_c=1.5,ec=.002,ecu=.0035):
		self.__FC=FC
		self.__Ecm=Ecm
		self.gamma_c=gamma_c
		self.ec=ec
		self.ecu=ecu
		if fck is None:
			fck=.83*Rck
		elif Rck is None:
			Rck=fck/.83
		self.__Rck=Rck
		self.__fck=fck
		self.calcola()
	
	def calcola(self):
		if self.__fck is None:
			self.__fck=.83*self.__Rck
		elif self.__Rck is None:
			self.__Rck=self.__fck/.83
		self.fck=self.__fck/self.FC
		self.Rck=self.__Rck/self.FC
		self.fcm=self.__fck+8 
		if self.Rck<=60:
			self.fctm=0.3*self.fck**(2./3)
		else:
			self.fctm=2.12*log(1+self.fcm/10)
		self.fctk=.7*self.fctm
		self.fctk_=1.3*self.fctm
		if self.__Ecm is None:
			self.Ecm=22000*(self.fcm/10)**.3
		else: #correggoi il valore di ec in modo da soddisfare Ecm imposto
			self.Ecm=self.__Ecm
			self.taraEpsilonC(self.__Ecm)
				
#			es=(-2+sqrt(2**2-4*-1*-.4))/(2*-1)
#			ec=0.4/es/self.Ecm*self.fcm
#			if ec<=self.ecu: self.ec=ec
#			else:self.ec=self.ecu
		self.G=self.Ecm/(2*(.2+1))	
		self.calcolaFcd()

	def modificaParametri(self,ec=None,ecu=None,fck=None,Rck=None):
		'''funzione per modificare il cls con parametri derivanti da calcolo confinamento
		ec=epsilon fine parabola in condizioni confinate
		ecu= epsilon rottura  in condizioni confinate
		fck rck valore di resistenza caratteristica confinata'''
		if ec is not None:self.ec=ec
		if ecu is not None:self.ecu=ecu
		if fck is not None or Rck is not None:
			self.__fck=fck
			self.__Rck=Rck
		self.__Ecm=None
		self.calcola()
		self.calcolaFcd()

	def taraEpsilonC(self,Ecm):
		def E(ec):
			#trovo parametri della parabola
			M=r_[ec**2,ec,2*ec,1].reshape(2,2)
			b=r_[self.fcm,0]
			a=solve(M,b)
			f=.4*self.fcm
			x=root(lambda x:polyval(r_[a,0],x)-f,0.)
			assert x.success
			E=f/x.x
			return E
		ec=root(lambda x:E(x)-Ecm,.001)
		assert ec.success
		if self.ec>self.ecu:
			raise ValueError('trovato ec>ecu')
		self.ec=ec.x
		self.Ecm=Ecm
		

	def calcolaFcd(self,mode='SLU',mult=1):
		'''mode= slu|sle|k|m'''
		if type(mode)==str:
			mode=mode.upper()
			self.mode=mode

		if mode=='SLU' or mode=='D':
			self.fcd=self.fck/self.gamma_c*.85*mult
			self.fctd=0
		elif mode=='SLE':
			self.fcd=self.fcm*mult
			self.fctd=self.fctm*mult
		elif mode=='K':
			self.fcd=self.fck*mult
			self.fctd=self.fctk*mult
		elif mode=='M':
			self.fcd=self.fcm*mult
			self.fctd=self.fctm*mult

	def _Ecm(self):
		def zerome(x):
			return self.sigma(x)-.4*self.fcd
		x=root(zerome,self.ec/2)
		if x.success:
			return .4*self.fcd/x.x[0] , x
		else:
			return x

	
	@property
	def FC(self):
		return self.__FC
	
	@FC.setter
	def FC(self,val):
		self.__FC=val
		self.calcola()

#	def __sigma(self,ec):
#		'''calcola la tensione nel calestruzzo in un punto'''
#		if not 0< ec <=self.ecu:
#			return 0.
#		if 0<ec< self.ec:
#			return (-1/self.ec**2*ec**2 + 2/self.ec*ec)*self.fcd
#		elif self.ec<=ec<=self.ecu:
#			return self.fcd
#	__sigma_=vectorize(__sigma)	#versione vettorizzata della __sigma, in questo modo richiede come input self ed ec obbligatori	
#	def sigma(self,ec): #versione vettorizzata di sigma che si comporta correttamente come __sigma in fase di chiamata
#		'''calcola la tensione del calcestruzzo nota la deformazione'''
#		return self.__sigma_(self,ec)

	def sigma(self,ec):
		return ((0<=ec) & (ec<self.ec)) * (-1/self.ec**2*ec**2 + 2/self.ec*ec)*self.fcd + ((ec>=self.ec) & (ec<=self.ecu)) *ones_like(ec)*self.fcd

		

	def __str__(self):
		if self.__FC!=1:
			st='FC = %.4g\n  '%self.__FC
		else:st=''
		st+= '''Rck = %(Rck).4g MPa  
f~ck~ = %(fck).4g MPa  
f~cm~ = %(fcm).4g MPa  
f~ctm~ = %(fctm).4g MPa  
f~ctk~ = %(fctk).4g MPa  
E~cm~ = %(Ecm).4g MPa  
G = %(G).4g MPa  '''%vars(self)
		return st

	def plot_diagramma(self,mode='k',mult=1,estart=0,**kargs):
		''' mode=slu|sle|k|m
			estart= epsilon di partenza
		'''
		opt={'plot_E':bool(self.__Ecm), #plotta il modulo elastico
			 'plot_ep':True,
			 'plot_info':True}
		opt.update(kargs)
		if not opt['plot_info']:
			for i in list(opt.keys()):
				opt[i]=False
		old_fcd=self.fcd
		self.calcolaFcd(mode,mult)
		ep=linspace(estart,self.ecu,100)
		s=self.sigma(ep)
		plot(ep,s)
		if mode=='d':
			text(self.ec,self.fcd,r'$\alpha f_{{c{}}}$={:.4g} MPa'.format(mode,self.fcd),horizontalalignment='right')
		else:
			text(self.ec,self.fcd,r'$f_{{c{}}}$={:.4g} MPa'.format(mode,self.fcd),horizontalalignment='right')
		#modulo elastico
		if opt['plot_E']:
			E,x=self._Ecm()
			s=self.sigma(x.x)
			plot([0,x.x,x.x],[s,s,0],'--',color='grey')
			plot(x.x,s,'xr')
			plot([0,x.x],[0,s],'r')
			text(.5*x.x,.5*s,r'$E_{{c{mode}}}$={E:.4g} MPa'.format(**locals()))
			text(x.x,s,r'$0.4 f_{{c{}}}$={:.4g} MPa ; $\epsilon$={:.4e}'.format(mode,s[0],x.x[0]))
		#epsilon
		if opt['plot_ep']:
			h=r_[ylim()].ptp()
			plot([self.ec,self.ec],[0,self.fcd],'--',color='grey')
			text(self.ec,.01*h,r'$\epsilon_c$=%.4e'%self.ec,horizontalalignment='right')
			text(self.ecu,.05*h,r'$\epsilon_{cu}$=%.4e'%self.ecu,horizontalalignment='right')
		xlabel(r'$\epsilon$')
		ylabel(r'$\sigma_c [MPa]$')
		grid(1)
		self.fcd=old_fcd

class Calcestruzzo(ParabolaRettangolo):
	classi=pa.read_csv(os.path.join(os.path.dirname(__file__),'classi_cls.csv'),header=0)
	def __init__(self,Rck=None,fck=None,FC=1.,Ecm=None,gamma_c=1.5,ec=.002,ecu=.0035):
		if Rck:
			if Rck in self.classi.Rck.values:
				fck=self.classi[self.classi.Rck==Rck].iloc[0].fck
		elif fck:
			if fck in self.classi.fck.values:
				Rck=self.classi[self.classi.fck==fck].iloc[0].Rck
		super().__init__(Rck=Rck,fck=fck,FC=FC,Ecm=Ecm,gamma_c=gamma_c,ec=ec,ecu=ecu)


class CalcestruzzoTrazione(Calcestruzzo):
	def __sigma(self,ec):
		'''calcola la tensione nel calestruzzo in un punto'''
		if ec>=self.ecu:
			return 0.
		if 0<ec< self.ec:
			return (-1/self.ec**2*ec**2 + 2/self.ec*ec)*self.fcd
		elif self.ec<=ec<=self.ecu:
			return self.fcd
		elif ec<0:
			v=self.Ecm/2.5*ec
			if v<-self.fctd: v=0.
			return v
	__sigma_=vectorize(__sigma,otypes=[float])	#versione vettorizzata della __sigma, in questo modo richiede come input self ed ec obbligatori	
	def sigma(self,ec): #versione vettorizzata di sigma che si comporta correttamente come __sigma in fase di chiamata
		'''calcola la tensione del calcestruzzo nota la deformazione'''
		return self.__sigma_(self,ec)

	def plot_diagramma(self,mode='k',mult=1,estart=None,**kargs):
		if estart is None:
			f={'slu':0,
				'sle':self.fctk,
				'k':self.fctk,
				'm':self.fctd}[mode.lower()]
			estart=-f/self.Ecm*2
		Calcestruzzo.plot_diagramma(self,mode,mult,estart,**kargs)

class ElastoFragile:
	def __init__(self,fyk=450.,E=210e3,FC=1.,gamma=1.15): #per default b450C
		vars(self).update(locals())
		self.calcola()

	def calcola(self):
		self.fyk/=self.FC
		self.ftk=self.fyk
		self.ftk/=self.FC
		k=self.ftk/self.fyk
		self.euk=self.fyk/self.E
		self.eud=self.euk/self.gamma
		self.eud_=self.eud
		self.eyk=self.fyk/self.E
		self.fyd=self.fyk/self.gamma
		self.ftd=self.fyk*k
		self.eyd=self.fyd/self.E

	def sigma(self,ep):
		ep=atleast_1d(ep)
		sig=self.E*ep
		sig=where(abs(sig)>self.fyd,0,sig)
		return sig


class ElastoPlastico:
	def __init__(self,fyk=450.,ftk=540.,euk=.07,Es=210e3,FC=1.,eud_=.01,gamma=1.15): #per default b450C
		'''fyk= tensione caratteristica snervamento
		ftk tensione caratteristica rottura
		euk= deformazione massima a rottura di calcolo
		Es= modulo elastico
		Fc= fattore di confidenza
		eud_= valore limite di calcolo per slu se None considera euk'''
		if eud_ is None: eud_=euk
		vars(self).update(locals())
		self.calcola()

	def calcola(self):
		self.fyk/=self.FC
		self.ftk/=self.FC
		k=self.ftk/self.fyk
		self.eud=self.euk
		self.eyk=self.fyk/self.Es
		self.fyd=self.fyk/self.gamma
		self.ftd=self.fyk*k
		self.eyd=self.fyd/self.Es

	def sigma(self,es):
		return (abs(es)<=self.eud) * ((abs(es)<self.eyd) * self.Es*es + (abs(es)>=self.eyd)* sign(es)*self.fyd)

	def __str__(self):
		if self.FC==1:
			st=''
		else:
			st='FC= %(FC).4g  \n'%vars(self)
		st+='''f~tk~ = %(ftk).4g MPa  
f~yk~ = %(fyk).4g MPa  
f~td~ = %(ftd).4g MPa  
f~yd~ = %(fyd).4g MPa  
e~uk~ = %(euk).4g  
e~ud~ = %(eud).4g  
e~yk~ = %(eyk).4g  
e~yd~ = %(eyd).4g  
E~s~ = %(Es).4g MPa  '''%vars(self)
		return st

class ElasticoIncrudente(ElastoPlastico): #acciaio con legame incrudente
	def sigma(self,es):
		return (abs(es)<=self.eud).astype(float) * ((abs(es)<self.eyd).astype(float) * self.Es*es + (abs(es)>=self.eyd).astype(float)* sign(es)*(self.fyd + ((self.ftd-self.fyd)/(self.eud -self.eyd)*(abs(es)-self.eyd) )))

class Acciaio(ElastoPlastico):
	pass

class AcciaioIncrudente(ElasticoIncrudente):
	pass

class AcciaioTrazione(Acciaio): 
	def sigma(self,es):
		return (abs(es)<=self.eud) * ((abs(es)<self.eyd) * self.Es*es + (abs(es)>=self.eyd)* sign(es)*self.fyd) *(es<0)



