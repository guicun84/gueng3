#-*-coding:utf-8-*-

from pylab import *
from scipy import *
from scipy.optimize import root,fminbound,leastsq
from scipy.interpolate import interp1d
from scipy.linalg import solve,norm
from scipy.spatial import Delaunay
import gueng.geometrie
from pdb import set_trace
from copy import deepcopy
ion()
import inspect
from .materiali import *
import shapely.geometry as shgm
'''
DA FARE
Per lo stato limite ultimo

Per la sezione in generale
1) ridefinizione iniziale della sezione [tipi standard rettngolare,T ecc..]
2) definizione della rotazione della sezione (sulla sezione gia ereditato da trasportare sulle armature)
3) valutazione domini di rottura e resistenze ultime in asse Y (poca roba avendo definito 2)

FATTO:
1) le deformazioni inferiori sono riferite alla barra più bassa e non al bordo della sezione
2) implementati i vari campi della sezione (100x piu veloce :-P )

'''

class SezionePiana:
	def __init__(self,obj=[]):
		'Classe per il calcolo del momento flettente delle sezioni '
		self.pSup=None #punto di controllo superiore
		self.pInf=None	#punto di controllo inferiore
		self.eSup_=None	#deformata punto inferiore
		self.eInf_=None	#deformata punto superiore
		self.eSupPrec=0
		self.eInfPrec=0
		self._aggiornata=False
		self.objects=list(obj)
		self.piano=r_[0,0] #piano delle deformazioni
		for i in obj: i.parent=self
#		for i in obj:self+i

	@property
	def eSup(self):
		return self.eSup_+self.eSupPrec
	@property
	def eInf(self):
		return self.eInf_+self.eInfPrec

	def __add__(self,val):
		self.objects.append(val)
		if hasattr(val,'parent'):
			val.parent=self
		self._aggiornata=False
			
	def __sub__(self,val):
		for n,i in enumerate(self.objects):
			if i is val:
				del self.objects[n]
		self._aggiornata=False
	
	def aggiorna(self,force=True):
		if not self._aggiornata or force:
			for i in self.objects: i.parent=self
			self.calcola_G()
			if force:
				self.pSup=None
				self.pInf=None
			for i in self.objects:
				if hasattr(i,'aggiorna'):
					i.aggiorna(force)
			self.trova_limiti(force)
		self._aggiornata=True
	
	def deforma(self,eSup,eInf):
		'''imposta le deformazioni della sezione nei punti di controllo self.pSup,self.pInf'''
		self.eSup_=atleast_1d(eSup)[0]
		self.eInf_=atleast_1d(eInf)[0]
		self.piano=polyfit((self.pInf,self.pSup),(self.eInf_,self.eSup_),1)
	
	def e(self,y): 
		'''valuta la deformazione in qualsiasi punto della sezine
		la sezione è deformata in modo piano in base alle deformazioni imposte con self.deforma'''
#		return self.eSup_+(self.eInf_-self.eSup_)/(self.pInf-self.pSup)*(y-self.pSup)
		return polyval(self.piano,y)

	def N(self,eSup=None,eInf=None):
		if eSup is not None:
			self.deforma(eSup,eInf)
		N=sum([i.N() for i in self.objects])
		return N

	def M(self,eSup=None,eInf=None):
		if eSup is not None:
			self.deforma(eSup,eInf)
		M=[i.M() for i in self.objects]
		return sum(M)

	def My(self,eSup=None,eInf=None):
		if eSup is not None:
			self.deforma(eSup,eInf)
		M=[i.My() for i in self.objects]
		return sum(M)

	def Mvec(self,eSup=None,eInf=None):
		return r_[self.M(eSup,eInf),self.My(eSup,eInf)]


	def calcola_G(self):
		sezioni=[i for i in self.objects if isinstance(i,Sezione)]
		G=r_[[i.G for i in sezioni]]
		A=r_[[i.A*i.moltiplicatore for i in sezioni]]
		self.G=r_[[i*j for i,j in zip(G,A)]].sum(0)/sum(A)

	def trova_limiti(self,force=False):
		if not ( self.pSup is None or self.pInf is None or force ):return
		sezioni=[i for i in self.objects if isinstance(i,Sezione)]
		limiti=r_[[i.limiti for i in sezioni]]
		self.limiti=r_[min(limiti[:,0]),max(limiti[:,1]),min(limiti[:,2]),max(limiti[:,3])]
		self.ptp=r_[self.limiti[1]-self.limiti[0],self.limiti[3]-self.limiti[2]]
		self.pSup=self.limiti[3]
		self.pInf=self.limiti[2]

	@property
	def yn(self): #DA RICONTROLLARE
		return self.pSup-(self.pInf-self.pSup)/(self.eInf-self.eSup)*self.eSup

	@property
	def chi(self):
		if self.pSup is not None:
			G=self.G[1]
			return (self.eSup_-self.eInf_)/((self.pSup-self.pInf) + (self.pSup-G)*self.eInf_ + (G-self.pInf)*self.eSup_)
			
		else:
			return 0

	def ruota(self,ang,copy=True):
		if copy:
			ns=deepcopy(self)
		else:
			ns=self
		for n,i in enumerate(ns.objects):
			ns.objects[n]=i.ruota(ang,copy)
			if copy:
				ns.objects[n].parent=ns
		ns.aggiorna(force=True)
		return ns


	def plot(self,*args):
		for i in self.objects:
			if hasattr(i,'moltiplicatore'):
				if i.moltiplicatore!=0:
					i.plot(*args)
			else:
				i.plot(*args)
		grid(1)
		try:
			ax=gca()
			ax.plot(self.G[0],self.G[1],'xr')
		except:
			pass

	def stato(self,fullout=False):
		'''ritorna stringa preformattata dello stato della sezione
		fullout: ritorna locals()'''
#		s=r_[[]]
#		h=r_[[]]
#		for i in self.objects:
#			if hasattr(i,'sezioni'):
#				h=r_[h,i.sezioni[0,:]]
#				s=r_[s,i.mat.sigma(self.e(i.sezioni[0,:]))]
#		ind=argsort(h)
#		h=h[ind]
#		s=s[ind]
#		ssup=s[-1]
#		sinf=s[0]
##		self.yn_()
		yn_=self.pSup-self.yn
		M=self.M()/1e6
		My=self.My()/1e6
		N=self.N()/1e3
		st='''Stato Sezione:  
altezza sezione: {self.ptp[1]:.4g} mm , baricentro={self.G[0]:.4g},{self.G[1]}   
stato deformativo:  
&epsilon;~sup~={self.eSup:.4g} , &epsilon;~inf~={self.eInf:.4g} , &chi;={self.chi:.4g}  
y~n~= {yn_:.4g} mm asse neutro  
azioni interne:  
M={M:.4g} kNm , N={N:.4g} kN , My={My:.4g} kNm'''.format(**locals())  
		if fullout: return locals()
		else:return st

	def plot_tensioni(self,contourf=False,*args,**kargs):
		'''contour: se True controlla se è disponibile il plot con mappa di colori
		multsig opzionale per scalare sigma'''
		sigs=empty(0,float)
		for i in self.objects:
			sigs=append(sigs,i.plot_tensioni())
		if 'multsig' in kargs.keys():
			sigs*=kargs['multsig']
		
		if contourf:
			sigs=sigs.astype(float)
			sigs=sigs[~isnan(sigs)]
#			sigs=sigs[sigs>0]
			vals=linspace(sigs.min(),sigs.max(),255)
			for i in self.objects:
				i.plot_tensioni()
				if hasattr(i,'plot_tensioni_contourf'):
					i.plot_tensioni_contourf(vals=vals,*args,**kargs)
			colorbar(ticks=linspace(vals.min(),vals.max(),10))
		grid(1)
		axvline(0,color='k')
		xlabel('Tensione [MPa]')
		ylabel('Quota [mm]')

	def saveEpsilon(self):
		'''salva la deformazione totale attuale della sezione'''
		self.eSupPrec+=self.eSup_
		self.eInfPrec+=self.eInf_
		for i in self.objects:
			i.saveEpsilon()

class SezioneCA(SezionePiana,object):
	classCA= [Calcestruzzo,CalcestruzzoTrazione]
	classAC=[Acciaio,ElastoFragile,ElastoPlastico,ElasticoIncrudente]
	def __init__(self,obj=[]):
		SezionePiana.__init__(self,obj)
		self._aggiornata=False
		self.campi_xtol=3e-7 #tolleranza per il calcolo dei campi di deformazione
	def trova_limiti(self,force=False):
		if not (self.pSup is None or self.pInf is None or force): return 
		if self.objects:
			sezioni=self.ca#[i for i in self.objects if isinstance(i,Sezione)]
			armature=self.ac#[i for i in self.objects if isinstance(i,Armatura)]
			try: #ricerco limiti sul calcestruzzo
				sup_sez=max([i.limiti[3] for i in sezioni])
				inf_sez=min([i.limiti[2] for i in sezioni])
			except:	
				sup_sez=inf_sez=None
			sup_arm=None
			inf_arm=None
			try:
				for arm in armature:
					if isinstance(arm,Armatura):
						sup=max([i.co[1] for i in arm.barre])
						infe=min([i.co[1] for i in arm.barre])
					else:
						sup=arm.co[0]
						infe=arm.co[1]
					if sup_arm is not None:
						sup_arm=max([sup_arm,sup])
						inf_arm=min([inf_arm,infe])
					else:
						sup_arm=sup
						inf_arm=infe
			except:pass
			#a questo punto ho trovato tutti i limiti estremi degli oggetti.
			#visto che armature e barre sono parenti stretti i loro limiti li metto direttamente a confronto per avere il limite massimo
			#finito con le armature e le barre considero i limiti della sezione se c'è
			if sup_sez is not None:
				self.pSup=sup_sez
				self.pInf=inf_sez
			if self.pSup is None and sup_arm is not None:
				self.pSup=sup_arm
			if inf_arm is not None:
				self.pInf=inf_arm
			aree=[]
			aree.extend(self.ca)
			aree.extend(self.ac)
			if self.ca:
				self.ptp=r_[r_[[i.sezioni[:,1] for i in self.ca]].flatten().max() , self.pSup-self.pInf]
			else:
				self.ptp=self.pSup-self.pInf


	@property
	def ca(self):
		ca=[i for i in self.objects if hasattr(i,'mat')]
		return [i for i in ca if isinstance(i.mat,tuple(SezioneCA.classCA))]
	
	@property
	def ac(self):
		ac=[i for i in self.objects if hasattr(i,'mat')]
		ac= [i for i in ac if isinstance(i.mat,tuple(SezioneCA.classAC))]
		ac.extend(self.barre)
		return ac

	@property
	def Ac(self):
		return sum([i.A * i.moltiplicatore for i in self.ca])

	@property
	def As(self):
		return sum([i.As for i in self.ac])
	
	@property
	def sezioni(self):
		sez=[]
		sez.extend(self.ca)
		sez.extend(self.ac)
		return sez

	@property
	def barre(self):
		armature=[i for i in self.objects if isinstance(i,Armatura)]
		barre=[]
		for arm in armature:
			barre.extend(arm.barre)
		return barre

	def eudInf(self):
		eud=[[i.co[1],i.mat.eud_] for i in self.ac]
		eud.sort()
		eud=eud[0][1]
		return eud
	
	def campo_1(self,N=None):
		'tutta trazione'
		eud=r_[[i.mat.eud_ for i in self.ac]].min()
		if N==None:	#restituisco i limiti di N che caratterizzano il campo 1
			ecu=ecd=-eud
			N=[self.N(ecu,ecd),0]
			ecu=0
			N[1]=self.N(ecu,ecd)
			return N
		else:#cerco le deformazioni per cui eguaglio il momento
			ecd=-eud
			def zerome(ecu,N):
				return (self.N(ecu,ecd)-N)**2
			ecu=fminbound(zerome,-eud/2,0,args=(N,),xtol=self.campi_xtol)
#			ecu=root(zerome,0,args=(N)).x
			return ecu,ecd

	def campo_2(self,N=None):
		eud=self.eudInf()
		ecu=[i.mat.ecu for i in self.ca]
		ecu=min(ecu)
		if N is None:
			N=[self.N(0,-eud),0]
			N[1]=self.N(ecu,-eud)
			return N
		else:
			def zerome(ecu,N):
				return (self.N(ecu,-eud)-N)**2
			ecu=fminbound(zerome,0,ecu,args=(N,),xtol=self.campi_xtol)
			return ecu,-eud

	def campo_3_4(self,N=None):
		eud=self.eudInf()
		eyd=[[i.co[1],i.mat.eyd] for i in self.ac]
		eyd.sort()
		eyd=eyd[0][1]
		ecu=min([i.mat.ecu for i in self.ca])
		if N is None:
			N=[self.N(ecu,-eud) , self.N(ecu,0)]
			return N
		else:
			def zerome(ecd,N):
				return (self.N(ecu,ecd)-N)**2
			ecd=fminbound(zerome,-eud,0,args=(N,),xtol=self.campi_xtol)
			return ecu,ecd

	def campo_5_6(self,N=None):
		ecu=min([i.mat.ecu for i in self.ca])
		ec=min([i.mat.ec for i in self.ca])
		if N is None:
			N=[self.N(ecu,0),self.N(ec,ec)]
			return N
		else:
			def zerome(p,N):
				esup=ecu-p*(ecu-ec)
				einf=ec*p
				return (self.N(esup,einf)-N)**2
#			p=root(zerome,.1,args=(N,),xtol=self.campi_xtol).x
			p=fminbound(zerome,0.,1.,args=(N,),xtol=self.campi_xtol)
			esup=ecu-p*(ecu-ec)
			einf=ec*p
			return esup,einf

	@property
	def campi(self):
		return dict(list(zip(
				'campo 1,campo 2,campo 3 4,campo 5 6'.split(','), 
				[self.campo_1,self.campo_2,self.campo_3_4,self.campo_5_6]
				)))
			
	def cercaCampoRottura(self,N):
		for n,i in list(self.campi.items()):
			val=i()
			if val[0]==val[1]:continue
			if val[0]<=N<=val[1]:
				self.campoRottura=n
				return i

	def Mxrd(self,N=0,verbose=True):
		if not self._aggiornata:
			self.aggiorna()
		n=self.cercaCampoRottura(N)
		if n is None:
			if verbose: print('sezione rotta')
			return 0
		e=n(N)
		M=self.M(*e)
		return M

	def dominio_MN(self,n=20,doppio=True,punti=None):
		'''n numero di punti in cui valutare il dominio 
		doppio: True per eseguire il calcolo anche del momento negativo.
		ritorna N e M'''
		if not self._aggiornata:
			self.aggiorna()
		#definisco i valori di deformazione lungo i campi di rottura per valutare il dominio
		if punti is None:

			eud=r_[[i.mat.eud_ for i in self.ac]].min()
			eyd=[[i.co[1],i.mat.eyd] for i in self.ac]
			eyd.sort()
			eyd=eyd[0][1]
			ecu=min([i.mat.ecu for i in self.ca])
			ec=min([i.mat.ec for i in self.ca])

			punti=r_[[
				[-eud,-eud],
			]]

			nn=int(n/3)+1
			temp=linspace(0,ecu,nn)
			temp=vstack((temp,ones(nn)*-1*eud)).T
			punti=vstack((punti,temp.copy()))

			temp=linspace(-eud,0,nn)[1:]
			temp=vstack((ones(nn-1)*ecu,temp)).T
			punti=vstack((punti,temp.copy()))

			temp=vstack((linspace(ecu,ec,nn)[1:],linspace(0,ec,nn)[1:])).T
			punti=vstack((punti,temp))
			print(punti)

		mn=zeros(punti.shape)
		for n,i in enumerate(punti):
			self.deforma(i[1],i[0])
			mn[n,0]=self.M()
			mn[n,1]=self.N()

		M=mn[:,0]
		N=mn[:,1]

		if doppio:
			s2=self.ruota(pi)
			s2.aggiorna(True)
			m2,n2=s2.dominio_MN(n,doppio=False,punti=punti)
			N=r_[N,n2[-1::-1]]
			M=r_[M,-1*m2[-1::-1]]
		return M,N

	def dominio_MM(self,N=0,n=20):
		'''calcolo il dominio Mx-My per dato N
		N=carico assiale
		n=numero di direzioni di ricerca'''
		a=linspace(0,2*pi,n+1)[:-1]
		da=a[1]-a[0]
		M=zeros((n,2)).astype(float)
		s=deepcopy(self)
		for n,i in enumerate(a):
			s.Mxrd(N)
			M[n]=dot(r_[[[cos(i),sin(i)],[-sin(i),cos(i)]]].T,s.Mvec())
			s=self.ruota(da,copy=False)
			s.aggiorna(True)
		return vstack([M,M[0]])


	def M1fes(self,N=0.,start=1e-4,fullout=False):
		'''calcolo momento di prima fessurazione
		N=sforzo normale
		start=esup iniziale di ricerca'''
		pinf_back=self.pInf
		sezioni=self.ca
		ed=min([x.mat.fctd/x.mat.Ecm for x in sezioni])*(1-1e-4)
		pinf_temp=r_[[i.limiti[-2] for i in sezioni]].min()
		self.pInf=pinf_temp
		x=root(lambda x: self.N(x,-ed)-N,start)
		if not x.success:
			print(x)
		N0,chi0=self.N().copy(),self.chi.copy()
		d=x.x + (-ed-x.x)/(self.pSup-self.pInf)*(self.pSup-pinf_back)
		self.pInf=pinf_back
		self.deforma(x.x,d)
		N1,chi1=self.N().copy(),self.chi.copy()
		if fullout:return self.M(),locals()
		return self.M()

	def fessura(self,M,N=0,c=30,Es=210000,kt=.4,k2=.5):
		'''c=copriferro
		ritorna :
			w fessure lontano barre
			w2 fessure vicino da barre
			d distanza per cui si ha w2
			locals()'''
		if not self._aggiornata:
			self.aggiorna()
		b=self.ptp[0]
		h=self.ptp[1]
#		d=abs(self.armatura.b.min()+self.G[1])
		if c is not None:
			d=h
			h+=c
		else:
			d=h-min([i.co[1] for i in self.barre])
			c=h-d
		fck=min([i.mat.fck for i in self.ca])
		fcm=min([i.mat.fcm for i in self.ca])
		fctm=min([i.mat.fctm for i in self.ca])
		Ecm=min([i.mat.Ecm for i in self.ca])
#		ec=self.SLE(M,N)[0]
		self.SLE(M,N)
		ec=[self.eSup,self.eInf]
		ss=r_[[i.mat.sigma(self.e(i.co[1])) for i in self.barre]]
		ss=(-1*ss[ss<0]).max()
#		ss=abs(self.ss(*ec)).max()
		y0=interp(0,[min(ec),max(ec)],[h,0])
		#diametro equivalente
		#considero tutte le barre tese con b<y0-G
		As=sum([i.As for i  in self.barre if i.co[1]<=(self.pSup-y0)])
		phi=As*4/pi/sum([i.di*i.n for i  in self.barre if i.co[1]<=(self.pSup-y0)])

		Aec=min([ 2.5*(h-d) , (h-y0)/3 , h/2])*b #area efficace di calcestruzzo per sezione rettangolare... andrebbe ridefinita... per le sezioni generiche
		reff=As/Aec
		ale=Es/Ecm
		esm=max([(ss-kt*fctm/reff*(1-ale*reff))/(Es),
			0.6*ss/Es]) #deformazione unitaria delle barre
		dsmax=1.3*(h-y0)# distanza tra fessure
		wd=esm*dsmax #ampiezza lontano dalle barre
		#apertura fessura in prossimità della barra.
		k1=.8
		k3=3.4
		k4=.425
		dsmax2=k3*c+k1*k2*k4*phi/reff #vicino alle barre
		wd2=esm*dsmax2 #vicino alle barre
#		print locals()
		st_out='''
base={b:.4g} mm
altezza={h:.4g} mm
copriferro={c:.4g} mm
f~ck~={fck:.4g} MPa
f~cm~={fcm:.4g} MPa
f~ctm~={fctm:.4g} MPa
E~cm~={Ecm:.4g} MPa
{stato}
y~0~={y0:.4g}mm
As={As:.4g} mm^2^
&phi;={phi:.4g}
A~ec~={Aec:.4g} mm^2^ area efficace di calcestruzzo 
r~eff~={reff:.4g} 
&alpha;~le~={ale:.4g}
&epsilon;~sm~={esm:.4g} deformazione unitaria delle barre
d~s,max~={dsmax:.4g} mm distanza tra le fessure
w~d~={wd:.4g} ampiezza fessure lontano dalle barre
k~1~={k1:.4g} ; k2={k2:.4g} ; k4={k4:.4g}
d~s,max 2~={dsmax2:.4g} mm dimensione trasversale per cui si ha fessura vicino alle barre
w~d2~={wd2:.4g} mm apertura fessure vicino alle barre'''.format(stato=self.stato(),**locals())
		return [wd,wd2,5*(c+phi/2),locals()]
	
	def SLE(self,M=0,N=0,start=5e-4,verbose=False): #da affinare...
		if not hasattr(start,'__len__'):
			dh=self.pSup-self.pInf
			start_=start+1e-3/(750-160)*(dh-160)
			start=max((start,min((start_,1e-3))))
			del start_,dh
			start=r_[start,-start]

		if not self._aggiornata:
			self.aggiorna()

		try:
			if N==0:N=1e-9*self.campo_5_6()[1]
		except:
			N=1.
		try:
			if M==0:M=1e-9*self.Mxrd()
		except:
			M=1.
#		def minimizzami(e,M,N):
#			self.deforma(*e)
#			out= ((r_[self.M(),self.N()]/r_[M,N])-r_[1.,1.])**2
##			out= (r_[self.M(),self.N()]-r_[M,N])
#			return out
#		e=leastsq(minimizzami,r_[.0001,-.0001].astype(float),args=(M,N),xtol=1e-12,ftol=1e-8,maxfev=int(1e6),full_output=True)
		def zerome(e):
			self.deforma(*e)
			return (r_[self.M(),self.N()]/r_[M,N]-1.)**2

		e=root(zerome,start)
		if verbose: print(e)
#		e=root(minimizzami,r_[.001,-.001].astype(float),args=(M,N))
		self.deforma(*e.x)
#		self.yn_()
		
		return

	def stato(self, fullout=False):
		'''ritorna una stringa preformattata con le caratteristiche dello stato della sezione
		fullout: se True ritorna locals()'''
		scmax=self.scmax
		ssmax=self.ssmax
		try:
			campo=self.campoRottura
	#		print 'campo',campo
			if campo=='campo 3 4':
				bar=[i for i in self.barre if i.co[1]==self.pInf]
				if bar:
					bar=bar[0]
				else:
					bar=[ i for i in self.ac if isinstance(i,Sezione)]
					bar=[[i.limiti[2],i] for i in bar]
					bar.sort()
					bar=bar[0][1]
	#			print self.eInf,bar.mat.eyd
				if self.eInf <= -bar.mat.eyd:
					campo='campo 3'
				else:
					campo='campo 4'
			if campo=='campo 5 6':
				e=empty(0,dtype=float)
				for i in self.ca:
					e=append(e,self.e(i.sezioni[0]))
				if all(e>0):
					campo='campo 6'
				else:
					campo='campo 5'
	#		print 'campo',campo
		except: campo='?'
		dat=SezionePiana.stato(self,True)
		st=dat['st']
		st+='''  
&sigma;~Cmax~={scmax:.2f} MPa , &sigma;~Smax~= {ssmax:.2f} MPa  
campo di rottura: {campo}'''.format(**locals())
		if fullout: return locals()
		else: return st
	
	@property
	def regione(self):
		'''ritorna una regione geometrica per calcoare J,W,A....della sola parte in cls'''
		return gueng.geometrie.regione(self.ca)

	def NChi(self,esup,einf):
		self.deforma(esup,einf)
		return r_[self.N(),self.chi]

	def Mchi(self,N,num=20,end=1.1,chi=None):
		'''N=sforzo normale
		num=numero punti
		end= % della curvatura massima imponibile
		chi=r_[] chi da verificare
		**return**
		chi,M,xx 
		chi=curvature indagate
		M=momento corrispondente a chi
		xx=risultato ottimizzazione xx[n].fun=errore su N e chi'''

#		chiu=(.0035+.01)/(self.pSup-self.pInf)*end
		if chi is None:
			self.Mxrd(N)
			chi=linspace(0,self.chi*end,num)
		M=zeros_like(chi)
		xx=[]
		A=sum((i.A for i in self.ca))
		Ec=self.ca[0].mat.Ecm
		eN=N/A/Ec
		for n,c in enumerate(chi):
			e0=r_[1,-1]*c*(self.pSup-self.pInf)/3 + eN*.8
			x=root(lambda x:self.NChi(*x)-r_[N,c],e0)
			xx.append(x)
			M[n]=self.M()
		return chi,M,xx

	@property
	def ssmax(self):
		return -r_[[i.sigma().min() for i in self.objects if isinstance(i,Armatura)],[i.sc().min() for i in self.ac if isinstance(i,Sezione)]].min()

	@property
	def scmax(self):
		return r_[[i.sc().max() for i in self.ca]].max()
			
 
class Sezione (gueng.geometrie.area):
	__version__='1.1'
	def __init__(self,tipo,*args,**kargs):
		'''kargs opzionali:
		num_part=200 numero di striscie in cui dividere la sezione
		moltiplicatore=1: peso per il calcolo del baricentro, il solo segno è considerato per le tensioni
		'''
		self.mat=None
		if 'mat' in list(kargs.keys()):
			self.mat=kargs['mat']
		choose={'rec':self.__init_rect , 
			    'T':self.__init_T}
		if tipo in list(choose.keys()):
			choose[tipo](*args,**kargs)
		else:
			gueng.geometrie.area.__init__(self,args[0])
			if self.A<0:gueng.geometrie.area.__init__(self,args[0][-1::-1])
		if  'numpart' in kargs.keys():
			self.num_part=kargs['num_part']
		else:
			self.num_part=100
		self.parent=None
		vars(self).update(kargs)
#		self.seziona()
		self.moltiplicatore=1 #nel calcolo del baricentro viene preso in considerazione questo valore moltiplicato per l'area, per le resistenze solo il segno

	def __init_rect(self,*args,**kargs):
		b,h=args
		punti=r_[[[-b/2,0.],[-b/2,-h],[b/2,-h],[b/2,0]]]
		gueng.geometrie.area.__init__(self,punti)
		

	def __init_T(self,*args,**kargs):
		h1,h2,b1,b2=args
		punti=r_[	-b2/2,0,
					-b2/2,-h2,
					-(b1)/2,-h2,
					-(b1)/2,-h1,
					(b1)/2,-h1,
					(b1)/2,-h2,
					b2/2,-h2,
					b2/2,0]
		punti=punti.reshape(8,2)
		gueng.geometrie.area.__init__(self,punti)

	def b(self,y):
		'calcola largheza sezione a una data quota'
		if not hasattr(self,'linearRing'):
			pts=[i.i for i in self.lati]
			self.linearRing=shgm.LinearRing(pts)
		ll=shgm.LineString([[self.limiti[0]-1,y],[self.limiti[1]+1,y]])
		p=self.linearRing.intersection(ll)
		if not isinstance(p,shgm.MultiPoint):
			return r_[0,0.]
		p=[i.x for i in p.geoms]
		p.sort()
		l=0
		s=0
		for n in range(0,len(p),2):
			dl=p[n+1]-p[n]
			s+=dl*(p[n+1]+p[n])/2
			l+=dl
		return r_[l,s/l]


	
	def seziona(self,n=None):
		'''divide la sezione in n striscie  parallele asse x e
		calcola la larghezza di ogni striscia'''
		if n == None:
			n=self.num_part
		else:
			self.num_part=n
		h=linspace(self.limiti[2],self.limiti[3],n+1)
		b=c_[[self.b(i) for i in h]]
		self.sezioni=hstack((c_[h],b)).T
		self.epsilon_prec=zeros(n+1)

	def sc(self,ec=None,eprec=None):
		'''calcolo delle sigma per varie epsilon
		ec=epsilon su cui calcolare sigma
		eprec= epsilon da sommare, se entrambe None usa le sezioni itnerne'''
		if ec is None:
			ec=self.parent.e(self.sezioni[0,:])
		if eprec is None:
			eprec=self.epsilon_prec
		return self.mat.sigma(ec+eprec)
	
	##potrebbe non servire più
	def saveEpsilon(self,ec=None):
		'''salva la deformazione totale attuale della sezione
		ec=deformazione totale da salvare [opzionale]'''
		if ec:
			assert len(ec)==self.epsilon_prec.size
			self.epsilon_prec=ec
		else:
			self.epsilon_prec+=self.parent.e(self.sezioni[0,:])
	
	def aggiorna(self,force=False):
		gueng.geometrie.area.aggiorna(self)
#		essendo il metodo in comune con geometria.area se non aggiungo questa
#		clausola viene eseguito seziona anche quando vengono calcolati i
#		parametri della sezine
		if force:
			try: del self.linearRing
			except:pass
			self.seziona()
		
	def N(self,ec=None):
		'''valuta la componente normale dovuta al calcestruzzo'''
		sc=self.sc(ec)
		return trapz(sc*self.sezioni[1,:],self.sezioni[0,:])*sign(self.moltiplicatore)

	def M(self,ec=None):
		'''valuta il momento dovuto al calcestruzzo'''
		sc=self.sc(ec)
		if self.parent is not None:
			YG=self.parent.G[1]
		else:
			YG=self.G[1]
		return trapz(sc*self.sezioni[1,:]*(self.sezioni[0,:]-YG),self.sezioni[0,:])*sign(self.moltiplicatore)

	def My(self,ec=None):
		'''calcola la componente My del momento per sezioni assimmetriche)'''
		sc=self.sc(ec)
		if self.parent is not None:
			XG=self.parent.G[0]
		else:
			XG=self.G[[0]]
		return trapz(sc*self.sezioni[1,:]*(self.sezioni[2,:]-XG),self.sezioni[0,:])*sign(self.moltiplicatore)

	@property
	def co(self): #espediente per ordinarli dal basso verso l'alto
		return r_[self.limiti[3],self.limiti[2]]
	

	def ruota(self,ang,copy=True):
		#eseguo la rotazione ereditata:
		ns=gueng.geometrie.area.ruota(self,ang,copy)
#		ns.seziona()
		if hasattr(ns,'armatura') and copy:
			ns.armatura.parent=ns
			ns.armatura.ruota(ang)
		return ns	

	def plot(self,*args):
		gueng.geometrie.area.plot(self,*args)
		if hasattr(self,'armatura'):
			self.armatura.plot()
	
	def plot_tensioni(self,*args,**kargs):
		'''multsig opzionale per sclare sigma'''
		y=self.sezioni[0,:]
		x=self.sc()*sign(self.moltiplicatore)
		if 'multsig' in kargs.keys():
			x*=kargs['multsig']
		ax=gca()
		ax.plot(x,y,*args)
		return x

	def plot_tensioni_contourf(self,dx=20,dy=5,vals=None,tripl=False,*args,**kargs):
		'dx,dy=spaziatura tra i punti'
		#ricerco punti interni per il calcolo
#		if self.moltiplicatore<0:return #è un buco non lo disegno
#		x=linspace(*r_[self.limiti[:2],self.ptp[0]/dx])[1:-1]
#		y=linspace(*r_[self.limiti[2:],self.ptp[1]/dy])[1:-1]
		x=arange(self.limiti[0],self.limiti[1]+dx,dx)
		y=arange(self.limiti[2],self.limiti[3]+dy,dy)
		X,Y=meshgrid(x,y)
		pt=r_[[[i,j] for i,j in zip(X.flat,Y.flat) if self.contiene(r_[i,j])]]
		if pt.size==0:pt=empty((0,2),float)
		del x,y,X,Y
#		pt=empty((0,2),float)
		#ricerco punti sul perimetro per il calcolo
		def pt2b(p1,p2,p3):
			pm1=(p2+p3)/2
			pm2=(p1+p3)/2
			if p1[0]==pm1[0]: pm1[0]+=1e-6
			if p2[0]==pm2[0]: pm2[0]+=1e-6
			l1=polyfit((p1[0],pm1[0]),(p1[1],pm1[1]),1)
			l2=polyfit((p2[0],pm2[0]),(p2[1],pm2[1]),1)
			return solve(r_[[[l1[0],-1],[l2[0],-1]]],r_[-l1[1],-l2[1]])
		inte=interp1d((1,0),(dx,dy))
		for l in self.lati:
			v=l.j-l.i
			ll=norm(v)
			v/=ll
			k=abs(dot((1,0),v))
			d=inte(k)
			n=ll/d
			x=linspace(l.i[0],l.j[0],n)
			y=linspace(l.i[1],l.j[1],n)
			pt=append(pt,c_[x[1:],y[1:]],0)
		#triangolo tutti i punti e valuto tensioni e mascherature
		d=Delaunay(pt)
		baricentri=array([pt2b(*d.points[i]) for i in d.simplices],float)
		mask=array([self.contiene(i) for i in baricentri],bool)
		elin=self.parent.e(pt[:,1]) #epsilon 'lineari'
		eprec=interp1d(self.sezioni[0],self.epsilon_prec)(pt[:,1])
		sig=self.sc(elin,eprec)*sign(self.moltiplicatore)
		####righe di test####
#		print 'test su elin' ,any(isnan(elin))
#		for i in sig: print i
#		print 'test su eprec' , any(isnan(eprec))
#		print 'test su sigs' , any(isnan(sig))
		###fine rige test###
#		mask2=array([any(sig[i]>0) for i in d.simplices],bool)
		tricontour(pt[:,0],pt[:,1],sig,[0,],triangles=d.simplices,mask=~(mask))
		tricontourf(pt[:,0],pt[:,1],sig,vals,triangles=d.simplices,mask=~(mask))
		if tripl:
			triplot(pt[:,0],pt[:,1],d.simplices,alpha=.2,linewidth=1,marker='.')
#		colorbar(ticks=linspace(sig.max(),sig.min(),10))
#		return locals()

class SezioneCirc(gueng.geometrie.Cerchio,Sezione):
	def __init__(self,de,di=0,G=r_[0,0.],**kargs):
		vars(self).update(locals())
		gueng.geometrie.Cerchio.__init__(self,de,di,G)
		self.num_part=201
		self.parent=None
		self.moltiplicatore=1
		vars(self).update(kargs)
#		self.seziona()
	
	def seziona(self,n=None):
		if n is None:
			n=self.num_part
		l=self.limiti
		y=linspace(l[2],l[3],n)
		b=r_[[self.b(i) for i in y]]
		self.sezioni=r_[[y,b]]
		self.epsilon_prec=zeros(n)

	def G(self):
		return gueng.geometrie.Cerchio.G(self)

	def plot(self):
		gueng.geometrie.Cerchio.plot(self)


#se avessi barre con legami differenti in questo momento non posso trattarle...
#la possibilità è pressochè nulla
class Armatura:
	def __init__(self,barre=[],parent=None):
		self.barre=barre
		self.parent=parent
		self.epsilon_prec=zeros(len(self.barre))
#		self.aggiorna()
		
	def aggiorna(self,force=True):
		if self.parent:
			self.G=self.parent.G
		else:
			self.G=r_[0,0.]
			self.limiti=r_[0,0.]
		self.aree=r_[[i.As for i in self.barre]]
		self.eu=r_[[i.mat.eud for i in self.barre]]
		self.ey=r_[[i.mat.eyd for i in self.barre]]
		self.fyd=r_[[i.mat.fyd for i in self.barre]]
		self.p=r_[[i.co[1] for i in self.barre]]## profondit delle barre di armatura
		self.b=r_[[i.co[1] for i in self.barre]]-self.G[1]## bracci delle barre di armatura]
		self.bx=r_[[i.co[0] for i in self.barre]]-self.G[0]## bracci delle barre di armatura]
		self.E=r_[[i.mat.Es for i in self.barre]]

	def sigma(self,es=None):
		'''calcola la tensione nelle barre'''
		if es is None:
			es=self.parent.e(self.p)+self.epsilon_prec
#		return (abs(es)<=self.eu) * ((abs(es)< self.ey) * self.E*es + (abs(es)>=self.ey) * sign(es)*self.fyd)
		return r_[[i.mat.sigma(j) for i,j in zip(self.barre,es)]]

	def saveEpsilon(self,ec=None):
		'''salva la deformazione totale attuale della sezione
		ec=deformazione totale da salvare [opzionale]'''
		if ec:
			assert len(ec)==self.epsilon_prec.size
			self.epsilon_prec+=ec
		else:
			self.epsilon_prec+=self.parent.e(self.p)

	def N_(self,es=None):
		'''calcola lo sforzo normale nelle barre'''
		return self.aree* self.sigma(es)

	def N(self,es=None):
		return sum(self.N_(es))
	
	def M_(self,es=None):
		'''calcola il momento flettente dovuto alle barre'''
		return self.aree * self.sigma(es)* self.b
	def M(self,es=None):
		return sum(self.M_(es))
	def My(self,es=None):
		'''calcola il momento flettente dovuto alle barre'''
		return sum(self.aree * self.sigma(es)* self.bx)


	def plot(self,*args,**kargs):
		for i in self.barre:
			i.plot(*args,**kargs)
	
	def plot_tensioni(self,*args,**kargs):
		'multsig=opzionale moltiplicatore per sigma'
		y=self.p
		x=self.sigma()
		if 'multsig' in kargs.keys():
			x*=kargs['multsig']
		ax=gca()
		ax.plot(x,y,'x')

	def ruota(self,rot,copy):
		rot=r_[[[cos(rot),sin(rot)],[-sin(rot),cos(rot)]]]
		if copy:
			na=deepcopy(self)
		else:
			na=self
		for b in na.barre:
			b.co=dot(b.co,rot)
#		na.aggiorna()	
		return na

	def __str__(self):
		return '\n'.join(['{}'.format(i) for i in self.barre])

	def sposta_punti(self,dx):
		na=deepcopy(self)
		for b in na.barre:
			b.co-=dx
		return na
		
class Barra(object):
	def __init__(self,co,di=None,n=1,As=None,mat=Acciaio()):
		'''co= coordinate barra
		di=diametro
		As=area [opzionale]
		mat=materiale

		As viene calcolata automaticamente in base al diametro
		se di=0 e As!=0 viene calcolato un diametro equivalente
		mat per default è b450c'''
		self.co=co.astype(float)
		if di:
			self.di=di
		elif As:
			self.As=As
		if mat:
			self.mat=mat
		self.n=n

	@property
	def As(self):
		return self._As*self.n
	@As.setter
	def As(self,As):
		self._As=As
		self._di=(As*4/pi)**.5


	@property
	def di(self):
		return self._di
	@di.setter
	def di(self,di):
		self._di=di
		self._As=self.di**2*pi/4

	def plot(self,*args,**kargs):
		r=(self.As/pi)**.5
		cr=Circle(self.co,r)
		gca().add_patch(cr)
#		plot(self.co[0],self.co[1],'o')
		draw()
		axis('equal')

	def __str__(self):
		return 'x={:6.2f}\ty={:6.2f}|\tdiametro {di:.2f}|\tnumero= {n:.2f}|\tAs={As:.2f}'.format(self.co[0],self.co[1],di=self.di,As=self.As,n=self.n)
		

if __name__=='__main__':
	#sez=sezione(r_[[[0.,0],[-500,0],[-500,1000],[0,1000]]])
	close('all')
	sez=Sezione('rec',1000.,500.)
	cls=Calcestruzzo(30.)
	sez.mat=cls
	barre=[Barra(r_[0,-40.],12,0), Barra(r_[0,-460.],14,5)]
	arm=Armatura(barre)
	sezca=SezioneCA()
	sezca+sez
	sezca+arm
	print(sezca.objects)

	#print sez.fessura(50000,10000000)
	#print sez.cerca_slu(0)
