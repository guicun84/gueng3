#-*-coding=latin-*-

from scipy import *
from gueng.CA import Acciaio,Calcestruzzo
import sys

class PiastraCA:
	def __init__(self,sp,sol,cl,cop=30,ac=None,Relem=None,Rarm=None,RElemGL=True):
		'''
		sp=spessore
		sol=nx ny nxy mx my mxy momenti rispetto assi elemento
		cl=calcestruzzo (resistenza cubica)
		cop=copriferro
		ac=acciaio (B450c di default)
		Relem=matrice rotazione elemeto 
		Rarm matrice rotazione armatura glob-loc (versori in riga)
		RelemGL: True se matrice rotazione elemento con versori in riga, False se in colonna'''
		if ac is None:ac=Acciaio()
		if not isinstance(cl,Calcestruzzo):
			cl=Calcestruzzo(cl)
		if Relem is None: Relem=eye(3)
		if Rarm is None: Rarm=eye(3)
		vars(self).update(locals())

	@property
	def solrot(self):
		'''ruota le sollecitazioni dal sistema elemento a quello armatura'''
		#ruoto il tensore delle sollecitazioni 
		if not hasattr(self,'_PiastraCA__solrot'):
			self.__solrot=zeros_like(self.sol)
			M=zeros((3,3),dtype=float)
			N=zeros((3,3),dtype=float)
			Relem=zeros((3,3),dtype=float)
			for n,i in enumerate(self.sol):
				N[:,:]=[[i[0],i[2],0],[i[2],i[1],0],[0,0,0]]
				M[:,:]=[[i[3],i[5],0],[i[5],i[4],0],[0,0,0]]
				#la matrice di rotazine dell'elemento mi serve da globale a locale
				if self.Relem.size==9:
					Relem=self.Relem
				else:
					Relem[:,:]=[[self.Relem[n,0],self.Relem[n,1],self.Relem[n,2]],
								[self.Relem[n,3],self.Relem[n,4],self.Relem[n,5]],
								[self.Relem[n,6],self.Relem[n,7],self.Relem[n,8]]]
				if not(self.RElemGL): Relem=Relem.T

				N[:,:]=dot(Relem.T,dot(N,Relem))
				N[:,:]=dot(self.Rarm,dot(N,self.Rarm.T))
				M[:,:]=dot(Relem.T,dot(M,Relem))
				M[:,:]=dot(self.Rarm,dot(M,self.Rarm.T))
				self.__solrot[n,:3]=[N[0,0],N[1,1],N[0,1]]
				self.__solrot[n,3:6]=[M[0,0],M[1,1],M[0,1]]
			del M,N
		return self.__solrot

	def reset(self):
		if hasattr(self,'__solrot'):del self.__solrot

	def mud(self):
		'''eurocodice uni env 1992-1-1 §A.2.8
		ritorna [mudx mudxp mudy mudyp]
		mudx momento x con trazione inferiore
		mudxp momento x con trazione sup
		mudy,mudyp in direzione y'''
		#valuto in quale direzione ho momento maggiore
		self.mx=self.solrot[:,3]
		self.my=self.solrot[:,4]
		self.mxy=self.solrot[:,5]
		txy=self.my>=self.mx

		#giro le sollecitazioni per avere my>mx sempre
		my=where(txy,self.my,self.mx)
		mx=where(txy,self.mx,self.my)
		amxy=abs(self.mxy)

		#applico metodo Eurocodice
		t1=mx+amxy>=0
		t2=my-amxy<=0
		mudx=(t1*(mx+amxy)) + (~t1* 0)
		mudy=(t1*(my+amxy)) + (~t1 * (my+self.mxy**2/abs(mx)))
		mudxp=(t2*(-mx+amxy)) + (~t2 *(-mx+self.mxy**2/abs(my)))
		mudyp=(t2*(-my+amxy) + (~t2*0))
#		for i,j in locals().items():
#			print i,j

		#rigiro le sollecitazioni per averle concordi con gli assi iniziali
		mudy=where(txy,mudy,mudx)
		mudyp=where(txy,mudyp,mudxp)
		mudx=where(txy,mudx,mudy)
		mudyx=where(txy,mudxp,mudyp)
		return c_[mudx,mudxp,mudy,mudyp]

	def nud(self):
		nx=self.solrot[:,0] + sign(self.solrot[:,0])*abs(self.solrot[:,2])
		nxp=self.solrot[:,0] - sign(self.solrot[:,0])*abs(self.solrot[:,2])
		ny=self.solrot[:,1] + sign(self.solrot[:,1])*abs(self.solrot[:,2])
		nyp=self.solrot[:,1] - sign(self.solrot[:,1])*abs(self.solrot[:,2])
		return c_[nx,nxp,ny,nyp]
	
	def Asmin(self,sis=True):
		'dm 2018 §4.1.6.1.1'
		if sis: asis=self.Asmin_sis()
		else: asis=0.
		return max((0.26*self.cl.fctm/self.ac.fyk*1e3*(self.sp-30),0.0013*1e3*(self.sp-30),asis))
	
	def Asmin_sis(self):
		'dm 2018 §7.4.6.2.1'
		return 1e3*self.sp*1.4/self.ac.fyk
		
	def predim(self,d0=14,k=.85,minimi=2):
		'''d0=aumento copriferro in direzione y
		k=percentuale tra altezza utile e braccio
		minimi: minimi di armatura:0 non calcolati, 1 statici,2 sismici,
		ritorna 
		ritorna [Asx Asxp Asy Asyp]
		Asx Area x con trazione inferiore
		Asxp Area x con trazione sup
		Asy,Asyp in direzione y
		'''
		m=self.mud()
		n=self.nud()
		Asm=m/(self.sp-ones(4)*self.cop-r_[0,0,d0,d0])*k/self.ac.fyd
		Asn=(n>0) * (n/2/self.ac.fyd)
		Asn[:,0]=where(Asn[:,0]>Asn[:,1],Asn[:,0],Asn[:,1])
		Asn[:,1]=where(Asn[:,2]>Asn[:,3],Asn[:,2],Asn[:,3])
		As=abs(Asm) + c_[Asn[:,0],Asn[:,0],Asm[:,1],Asm[:,1]]
		#controllo minimo normativo
		if minimi in (1,2):
			if minimi==1:Amin=self.Asmin(False)
			else:Amin=self.Asmin(True)
			As=where(As>Amin,As,Amin)
		return As


