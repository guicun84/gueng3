#-*-coding:latin-*-
#connettore
from scipy import pi,r_,floor
class Connettore:
	def __init__(self,A,J,l,E=210e3,ni=.25):
		'''Connettore a taglio:
		A=area connettore
		J=modulo inerzia connettore
		l=lunghezza connettore
		E=modulo elastico
		ni=coeff.poissont'''
		vars(self).update(locals())
		G=210e3/(2*(1+.25))
		kt=A*G/l
		kf=210e3*J/l**3
		self.k=1/(1/kt + 1/kf)

class ConnettoreCilindrico(Connettore):		
		def __init__(self,d,l,E=210e3,ni=.25):
			'''Connettore a taglio cilindrico:
			d=diametro
			l=lunghezza connettore
			E=modulo elastico
			ni=coeff.poissont'''
			vars(self).update(locals())
			A=pi*d**2/4
			J=pi*d**4/64
			Connettore.__init__(self,A,J,l,E,ni)
		
class Chiodatura:
	def __init__(self,l,p,chiodo):
		'''chiodatura a taglio
		l=lunghezza elemento chiodato
		p=passo chiodatura
		chiodo=classe Connettore'''
		vars(self).update(locals())
		self.n=floor(l/p)
		self.k=self.n*chiodo.k/2 #considero met� xche lavorano met� per parte

	def deltax(self,N):
		'''calcolo accorciamento dovuto a deformazione connettori
		N=sforzo normale'''
		return N/self.k
		
	def eps(self,N):
		'''calcolo epsilon dovuto a forzo normale
		N=sforzo Normale'''
		return self.deltax(N)/self.p/self.n*2

from gueng.CA import SezioneCA

class Solaio(SezioneCA):
	def __init__(self):
		SezioneCA.__init__(self)
		self.caldana=None
		self.chiodatura=None

	@property
	def chi(self):
		if self.caldana is None:
			self.caldana=[self.objects[0]]
		N=sum([i.N() for i in self.caldana])
		if self.chiodatura is not None:
			eps=self.chiodatura.eps(N)
		else:
			eps=0
		return (self.e_sup + eps -self.e_inf)/(self.p_sup-self.p_inf)



	
