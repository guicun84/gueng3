#-*-coding:latin-*-

class Neve:
	norma='dm 2018'
	__CE=dict(esposta=0.9,normale=1.0,riparata=1.1)
	provincie_=dict(IA=[ 'Aosta', 'Belluno', 'Bergamo', 'Biella', 'Bolzano', 'Brescia', 'Como', 'Cuneo', 'Lecco', 'Pordenone', 'Sondrio', 'Torino', 'Trento', 'Udine', 'Verbano-Cusio-Ossola', 'Vercelli', 'Vicenza'],
	IM=[
	'Alessandria', 'Ancona', 'Asti', 'Bologna', 'Cremona', 'Forlì-Cesena', 'Lodi', 'Milano', 'Modena', 'Monza Brianza', 'Novara', 'Parma', 'Pavia', 'Pesaro e Urbino', 'Piacenza', 'Ravenna', 'Reggio Emilia', 'Rimini', 'Treviso', 'Varese'
	],
	II=[
	'Arezzo', 'Ascoli Piceno', 'Avellino', 'Bari', 'Barletta-Andria-Trani', 'Benevento', 'Campobasso', 'Chieti', 'Fermo', 'Ferrara', 'Firenze', 'Foggia', 'Frosinone', 'Genova', 'Gorizia', 'Imperia', 'Isernia', 'L’Aquila', 'La Spezia', 'Lucca', 'Macerata', 'Mantova', 'Massa Carrara', 'Padova', 'Perugia', 'Pescara', 'Pistoia', 'Prato', 'Rieti', 'Rovigo', 'Savona', 'Teramo', 'Trieste', 'Venezia', 'Verona'
	],
	III=[
	'Agrigento', 'Brindisi', 'Cagliari', 'Caltanissetta', 'Carbonia-Iglesias', 'Caserta', 'Catania', 'Catanzaro', 'Cosenza', 'Crotone', 'Enna', 'Grosseto', 'Latina', 'Lecce', 'Livorno', 'Matera', 'Medio Campidano', 'Messina', 'Napoli', 'Nuoro', 'Ogliastra', 'Olbia-Tempio', 'Oristano', 'Palermo', 'Pisa', 'Potenza', 'Ragusa', 'Reggio Calabria', 'Roma', 'Salerno', 'Sassari', 'Siena', 'Siracusa', 'Taranto', 'Terni', 'Trapani', 'Vibo Valentia', 'Viterbo'
	])
	__provincie={}
	for zona,pr in list(provincie_.items()):
		for p in pr:
			__provincie[p]=zona
	del provincie_,p,pr,zona
	
	def __init__(self,zona,a_s,alpha,topografia='normale'):
		'''
		zona=IA IM II III zona di riferimento oppure provincia
		a_s=quota del sito
		alpha=pendenza della falda
		topografia='esposta normale riparata'
		'''
		vars(self).update(locals())
		if zona not in ('IA','IM','II','III'):
			self.provincia=zona.capitalize()
			zona=Neve.__provincie[zona.capitalize()]
		self.zona=zona.upper()
	
	@property
	def qs(self):
		'carico sulla struttura'
		return self.qsk*self.CE*self.CT*self.mui

	@property
	def qsk(self):
		'carico di riferimento a terra '
		if self.zona=='IA':
			if self.a_s<=200: return 1.5
			else: return 1.39+(1+(self.a_s/728)**2)
		elif self.zona=='IM':
			if self.a_s<=200: return 1.5
			else: return 1.35*(1+(self.a_s/602)**2)
		elif self.zona=='II':
			if self.a_s<=200: return 1.0
			else: return 0.85*(1+(self.a_s/481)**2)
		elif self.zona=='III':
			if self.a_s<=200: return 0.6
			else: return 0.51*(1+(self.a_s/481)**2)

	@staticmethod
	def mu1(alpha):
		'coefficiente di forma'
		if alpha<30: return 0.8
		elif alpha<60: return 0.8*(60-alpha)/30
		else: return 0

	@property	
	def mui(self):
		'coefficiente di forma'
		return self.mu1(self.alpha)

	@property
	def CE(self):
		'coefficiente di esposizione'
		return Neve.__CE[self.topografia]	

	@property
	def CT(self):
		'coefficiente di temperatura'
		return 1

	def __str__(self):
		if hasattr(self,'provincia'):
			out='provincia: {}  \n'.format(self.provincia)
		else:out=''
		return out+'''Zona={zona}  
a~s~={a_s} mslm 
q~sk~={qsk} kN/m^2^  
&alpha;={alpha} ° inclinazione della falda principale
&mu;~i~={mui}  coefficiente di forma
CE={CE} coefficiente di esposizione
CT={CT} coefficiente di Temperatura

q~s~={qs} KN/m^2^'''.format(**self.dict)

	@property
	def dict(self):
		return dict(zona=self.zona,a_s=self.a_s,qsk=self.qsk,alpha=self.alpha,mui=self.mui,CE=self.CE,CT=self.CT,qs=self.qs)



