#-*-coding:latin-*-

from numpy import r_,sqrt,array,deg2rad,cos,sin,mean,atleast_1d,where
import pandas as pa
import os
from numpy import r_,prod,sqrt
from scipy.interpolate import interp1d

dire=os.path.dirname(__file__)
class HiltyHitHy200R:
	dat_56=pa.read_csv(os.path.join(dire,'dati_filettato_5.6.csv'),sep='\s*,\s*',index_col=0,engine='python').T
	dat_56.index=array(dat_56.index,dtype=int)
	dat_88=pa.read_csv(os.path.join(dire,'dati_filettato_8.8.csv'),sep='\s*,\s*',index_col=0,engine='python').T
	dat_88.index=array(dat_88.index,dtype=int)
	for i in 'NRd VRd NRdc VRdc N0Rdp1 N0Rdp2 N0Rdp3 N0Rdpc1 N0Rdpc2 N0Rdpc3 N0Rdc N0Rdcc V0Rdc V0Rdcc'.split():
		dat_56[i]=dat_56[i]*1e3
		dat_88[i]=dat_88[i]*1e3

	def __init__(self,d,hef=None,h=None,beta=0,fes=True,rck=25,c=None,s=None,tr=1,barra='8.8',testmin=True,f4prod=True):
		'''d=diametro barra
		hef=profonditÓ infissione
		h=spessore elemento cls
		beta=angolo tra direzione forza e perpendicolare al bordo libero [gradi]
		fes=True se calcestruzzo fessurato
		rck=rck calcestruzzo di base
		c=distanza dal bordo libero
		s=distanze da connettori limitrofi
		tr=range di temperature defalutl 3 ([-40,40],[-40,80],[-40,120])
		barra= tpo di acciaio della barra: ['8.8'|'5.6'|'B450c']
		testmin[True false] per eseguire o meno i test sui bordi
		f4prod=True moltiplica i valori di f4 per ogni s passato, se false li pesa sul minimo '''
		vars(self).update(locals())	
		#carico i dati relativi al tipo di barra utilizzata
		self.dat={'5.6':HiltyHitHy200R.dat_56,
			'8.8':HiltyHitHy200R.dat_88}[barra]
		#controllo alcuni dati di input
		if not hasattr(s,'__len__') and s is not None:self.s=[s]
		self.betar=deg2rad(beta)
		if hef is None: self.hef=self.query('hef')
		if h is None: self.h=self.query('h')
		if testmin:self.test_min()

	def test_min(self):
		'''esegue il test dei valori minimi'''
		if self.c is not None:
			if self.c<self.cmin :raise ValueError('distanza dal bordo ={} < cmin={}'.format(self.c,self.cmin))
		if self.s is not None:
			if all([i<self.smin for i in self.s]): raise ValueError('interasse tra connettori ={} < smin={}'.format(self.s,self.smin))
		if  self.h<self.hmin: raise ValueError('altezza supporto ={}<{}=hmin'.format(self.h,self.hmin))
		if self.hef<self.hefmin: raise ValueError('profonditÓ foratura={}<{}=hef_min'.format(self.hef,self.hefmin))


	def query(self,args):
		'''ricerca l'argomento passato in dat eseguento eventuale interpolazione cubica'''
		if self.d in self.dat.index:return self.dat[args][self.d]
		else:
			i=interp1d(self.dat.index.values,self.dat[args],kind='cubic')
			return i(self.d)
	@property
	def d0(self): return self.query('d0')
	@property
	def hefmin(self): return self.query('hefmin')
	@property
	def smin(self): return self.query('smin')
	@property
	def cmin(self): return self.query('cmin')
	@property
	def hmin(self):
		if self.d<=12: return max((100,self.hef+30))
		else:return self.hef+2*self.d0


	@property
	def ccrsp(self):
		'''distanza critica dal bordo per splitting
		h=spessore materiale di base
		hef=profonditÓ ancoraggio'''
		r=self.h/self.hef
		if r>=2: return self.hef
		elif r>1.3: return 4.6*self.hef-1.8*self.h
		else:return 2.26*self.hef

	@property
	def scrsp(self):
		'''distanza critica tra connettori per splitting
		h=spessore materiale di base
		heff=profonditÓ ancoraggio'''
		return 2*self.ccrsp

	@property
	def ccrN(self):
		return 1.5*self.hef

	@property
	def scrN(self):
		return 2*self.ccrN
	
	@property
	def fBp(self):
		'''influenza classe calcesturzzo su pullout'''
		return (self.rck/25)**.11

	@property
	def fBc(self):
		'''influenza da resistenza cls su estrazione cono
		rck=resistenza caratteristica cubica MPa'''
		assert 25<=self.rck<=60, 'rck fuori dal range di validitÓ [25,60]MPa'
		return sqrt(self.rck/25)	

	@property
	def f1N(self):
		'''influenza della distanza dai bordi su estrazione'''
		if self.c is None:return 1
		return min((0.7+(0.3*self.c/self.ccrN),1))

	@property
	def f2N(self):
		'''influenza della distanza dai bordi su estrazione'''
		if self.c is None:return 1
		return min((1,0.5*(1+self.c/self.ccrN)))
	
	@property
	def f3N(self):
		'''influenza della distanza tra connettori su estrazione'''
		if self.s is None:return [1]
		return  [min((1,0.5*(1+i/self.scrN))) for i in self.s]

	@property
	def f1sp(self):
		'''influenza della distanza dai bordi su estrazione'''
		if self.c is None:return 1
		return min((0.7+(0.3*self.c/self.ccrsp),1))

	@property
	def f2sp(self):
		'''influenza della distanza dai bordi su estrazione'''
		if self.c is None:return 1
		return min((1,0.5*(1+self.c/self.ccrsp)))
	
	@property
	def f3sp(self):
		'''influenza della distanza tra connettori su estrazione'''
		if self.s is None:return [1]
		return  [min((1,0.5*(1+i/self.scrsp))) for i in self.s]

	@property
	def fhp(self):
		'influenza profonditÓ su pullout'
		return self.hef/self.query('hef')
	
	@property
	def fhN(self):
		'influenza profonditÓ su estrazione'
		return (self.hef/self.query('hef'))**1.5

	@property
	def Nrdp(self):
		'''resistenza a pullout e cono calcestruzzo'''
		if self.fes:
			N0rdp=self.query({1:'N0Rdpc1',2:'N0Rdpc2',3:'N0Rdpc3'}[self.tr])
		else:
			N0rdp=self.query({1:'N0Rdp1',2:'N0Rdp2',3:'N0Rdp3'}[self.tr])
		return N0rdp*self.fBp*self.f1N*self.f2N*prod(self.f3N)*self.fhp

	@property
	def Nrdc(self):
		'''resistenza del cono di calcestruzzo'''
		if self.fes:
			N0rdc=self.query('N0Rdcc')
		else:
			N0rdc=self.query('N0Rdc')
		return N0rdc*self.fBc*self.f1N*self.f2N*prod(self.f3N)*self.fhN

	@property
	def Nrdsp(self):
		'''resistenza del calcestruzzo a splitting solo per non fessurato'''
		if not self.fes:
			return self.query('N0Rdc')*self.fBc*self.f1sp*self.f2sp*prod(self.f3sp)*self.fhN


	@property
	def fbeta(self):
		'influenza inclinazione carico rispetto a bordo'
		if self.beta>=90:return 2.5
		return min((sqrt(1/ (cos(self.betar)**2 + (sin(self.betar)/2.5)**2)),2.5))

	@property
	def fhV(self):
		'influenza profonditÓ su taglio'
		if self.c is None: return 1
		return min(((self.h/(1.5*self.c))**.5,1))
#	#vechia stesura
#	@property
#	def f4(self):
#		'influenza distanza tra connettori su taglio'
#		if self.c is None and self.s is None: return 1
#		if self.s is None: return (self.c/self.hef)**1.5
#		if self.c is None:
#			c=self.hef*5.5
#		else:
#			c=self.c
#		vmax=(c/self.hef)**1.5
#		return [min((vmax, vmax * (1+i/(3*c))*.5)) for i in self.s]
	
	#rivisitazione 2020-05-28
	@property
	def f4(self):
		'influenza distanza tra connettori su taglio'
		if self.c is None:
			c=self.hef*5.5
		else: c=self.c
		if self.s is None: 
			return (c/self.hef)**1.5
		else:
			s=atleast_1d(self.s)
			val=((c/self.hef)**1.5)*r_[[min((1,(1+x/(3*c))*.5)) for x in s]]
		return val

	@property
	def fhef(self):
		'influenza profonditÓ infissione su taglio '
		return 0.05*(self.hef/self.query('d0'))**1.68

	@property
	def fc(self):
		'influneza distanza bordo su taglio '
		######
		#questo coefficiente non viene usato sul manuale hilty:
		#all'aumentare della distanza dal bordo diminiusce il suo valore (strano)
		#i valori tabellati nei casi di studio usano fc=1
		return 1

		if self.c is None: return 1
		d0=self.query('d0')
		if self.c/d0<4: return .77
		return (d0/self.c)**.19

	@property
	def Vrdc(self):
		'resistenza a taglio per cedimento bordo'
		if self.fes:Vrdc0=self.query('V0Rdcc')
		else:Vrdc0=self.query('V0Rdc')
		f4=atleast_1d(self.f4)
		if self.f4prod:
			f4=prod(f4)
		else:
			f4=prod(f4/f4.max())*f4.max()
		return Vrdc0*self.fBc*self.fbeta*self.fhV*f4*self.fhef*self.fc

	@property
	def Vrdcp(self):
		'resistenza a taglio per pryout'
		return min((self.Nrdp,self.Nrdc))*2

	@property
	def Nrd(self):
		dats=[]
		if self.fes: 
			dats.append(self.query('NRdc'))
		else:
			dats.append( self.query('NRd'))
		dats.append(self.Nrdp)
		dats.append(self.Nrdc)
		if not self.fes: dats.append(self.Nrdsp)
		return min(dats)

	@property
	def Vrd(self):
		dats=[]
		dats.append(self.query('VRd')) #resistenza connettore metallico
		dats.append(self.Vrdcp) #resistenza per pryout
		dats.append(self.Vrdc) #resistenza bordo cls
		return min(dats)

			


	@property
	def dict(self):
		vrs2=dir(self)
		vrs2=[i for i in vrs2 if i not in '__doc__ __init__ __module__ dat self dict'.split()]
		out= dict([(i,getattr(self,i)) for i in vrs2])
		return out


