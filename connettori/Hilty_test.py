#-*-coding:latin-*-

from numpy import *
import Hilty
from Hilty import HiltyHitHy200R as hi
import unittest
'''visto che i valori di test per i vari coefficienti a volte non rispettano i lmiti di distanze disabilito il test nella classe'''
hi.test_min=lambda x:None 


class MyTest(unittest.TestCase):
	__name__='MyTest'
	def test_fBp(self):
		#controllo di Fbp
		cls=r_[25,30,37,45,50,55,60]
		fBpatt=r_[1,1.02,1.04,1.06,1.07,1.08,1.1]
		fBpcalc=empty(0,float)
		for c in cls:
			b=hi(12,rck=c)
			fBpcalc=append(fBpcalc,b.fBp)
		fBperr= (abs(fBpcalc-fBpatt)/fBpatt)
		self.assertTrue(max(fBperr)<1e-2)

	def test_fBc(self):
		#controllo di fBc
		cls=r_[25,30,37,45,50,55,60]
		fBcatt=r_[1,1.1,1.22,1.34,1.41,1.48,1.55]
		fBccalc=empty(0,float)
		for c in cls:
			b=hi(12,rck=c)
			fBccalc=append(fBccalc,b.fBc)
		fBcerr= (abs(fBccalc-fBcatt)/fBcatt)
		self.assertTrue( max(fBcerr)<1e-2)

	def test_f1N(self):
		r=r_[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
		b=hi(12)
		c=b.ccrN*r
		att=r_[.73,.76,.79,.82,.85,.88,.91,.94,.97,1]
		calc=empty(0,float)
		for i in c:
			b=hi(12,c=i)
			calc=append(calc,b.f1N)
		err=abs(calc-att)/att
		self.assertTrue(err.max()<=1e-3)
		
	def test_f1sp(self):
		r=r_[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
		b=hi(12)
		c=b.ccrsp*r
		att=r_[.73,.76,.79,.82,.85,.88,.91,.94,.97,1]
		calc=empty(0,float)
		for i in c:
			b=hi(12,c=i)
			calc=append(calc,b.f1sp)
		err=abs(calc-att)/att
		self.assertTrue(err.max()<=1e-3)

	def test_f2N(self):
		r=r_[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
		b=hi(12)
		c=b.ccrN*r
		att=r_[.55,.60,.65,.7,.75,.8,.85,.9,.95,1]
		calc=empty(0,float)
		for i in c:
			b=hi(12,c=i)
			calc=append(calc,b.f2N)
		err=abs(calc-att)/att
		self.assertTrue(err.max()<=1e-3)

	def test_f2sp(self):
		r=r_[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
		b=hi(12)
		c=b.ccrsp*r
		att=r_[.55,.60,.65,.7,.75,.8,.85,.9,.95,1]
		calc=empty(0,float)
		for i in c:
			b=hi(12,c=i)
			calc=append(calc,b.f2sp)
		err=abs(calc-att)/att
		self.assertTrue(err.max()<=1e-3)

	def test_f3sp(self):
		r=r_[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
		b=hi(12)
		s=b.scrsp*r
		att=r_[.55,.60,.65,.7,.75,.8,.85,.9,.95,1]
		calc=empty(0,float)
		for i in s:
			b=hi(12,s=i)
			calc=append(calc,b.f3sp)
		err=abs(calc-att)/att
		self.assertTrue(err.max()<=1e-3)

	def test_f3N(self):
		r=r_[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]
		b=hi(12)
		s=b.scrN*r
		att=r_[.55,.60,.65,.7,.75,.8,.85,.9,.95,1]
		calc=empty(0,float)
		for i in s:
			b=hi(12,s=i)
			calc=append(calc,b.f3N)
		err=abs(calc-att)/att
		self.assertTrue(err.max()<=1e-3)

	def test_fbeta(self):
		beta=r_[0,10,20,30,40,50,60,70,80,90,100,110]
		att=r_[1,1.01,1.05,1.13,1.24,1.40,1.64,1.97,2.32,2.5,2.5,2.5]
		calc=empty(0,float)
		for b in beta:
			b=hi(12,beta=b)
			calc=append(calc,b.fbeta)
		err=abs(calc-att)/att
		self.assertTrue(err.max()<=1e-2)

	def test_fhV(self):
		hc=r_[.15,.3,.45,.6,.75,.9,1.05,1.2,1.35,1.5,2]
		att=r_[.32,.45,.55,.64,.71,.77,.84,.89,.95,1,1]
		cmin=hi(12).dat.cmin[12]
		h=hc*cmin
		calc=empty(0,float)
		for i in h:
			b=hi(12,h=i,c=cmin)
			calc=append(calc,round(b.fhV,2))
		err=abs(calc-att)
		self.assertTrue(err.max()<=.01+1e-4)

	def test_fhef(self):
		hefd=r_[r_[4,4.5],r_[5:21]]
		att=r_[.51,.63,.75,1.01,1.31,1.64,2,2.39,2.81,3.25,3.72,4.21,4.73,5.27,5.84,6.42,7.04,7.67]
		d=hi(12).d0
		hef=d*hefd
		calc=empty(0,float)
		for h in hef:
			b=hi(12,hef=h)
			calc=append(calc,round(b.fhef,2))
		er=abs(calc-att)
		self.assertTrue(er.max()<.01+1e-4)

	@unittest.skip
	def test_fc(self):
		cd=r_[4,6,8,10,15,20,30,40]
		att=r_[.77,.71,.67,.65,.6,.57,.52,.50]
		d=hi(12).d0
		cmin=hi(12).dat.cmin[12]
		cs=d*cd
		calc=empty(0,float)
		att2=empty(0,float)
		for c in cs:
			if c<cmin:continue
			att2=append(att2,c)
			b=hi(12,c=c)
			calc=append(calc,round(b.fc,2))
		er=abs(calc-att)
		self.assertTrue(er.max()<.01+1e-4)

	def test_f4_singolo(self):
		chef=r_[.5,.75,1,1.25,1.5,1.75]
		att=r_[.35,.65,1,1.4,1.84,2.32]
		h=hi(12).hef
		cs=chef*h
		calc=empty(0,float)
		for c in cs:
			b=hi(12,c=c)
			calc=append(calc,round(b.f4,2))
		err=abs(calc-att)
		self.assertTrue(max(err)<=.01+1e-4)

	def test_f4_doppio(self):
		chef=r_[.5,.75,1,1.25]
		shef=r_[.75,1.5,2.25,3]
		att=r_[	.27,.35,.35,.35,
				.43,.54,.65,.65,
				.63,.75,.88,1,
				.84,.98,1.12,1.26]
		h=hi(12).hef
		cs=chef*h
		ss=shef*h
		calc=empty(0,float)
		serie=[]
		for c in cs:
			for s in ss:
				serie.append({'c':c/h,'s':s/h})
				b=hi(12,c=c,s=s)
				calc=append(calc,round(b.f4[0],2))
		print(calc)
		err=abs(calc-att)
		self.assertTrue(max(err)<=.01+1e-4)


if __name__=='__main__':
	unittest.main(verbosity=2)
