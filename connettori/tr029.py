#-*-coding:latin-*-

from scipy import sin,pi,atleast_1d,sqrt

class WIP(Exception):
	pass

class Connettore:
	def __init__(self,fuk,d,hef,tarurkucrc2025,taurkcr,taurkucr,Rck,fck,c=None,s=None,cracked=True,n=1):
		'''parametri:
		fuk= resistenza ultima dell materiale 
		d=diametro del connettore
		hef= profondità infissione
		taurkucrc2025=taurkucrc per calcestruzzo C20/25
		taurkcr,taurkucr= taurk in funzione del calcestruzzo e dello stato fessurato e non 
		Rck,fck,resistenza del calcestruzzo di base
		c,s distanze dal bordo e tra bulloni
		cracked= bool true se fessurato
		n= numero di connettori
		
		proprietà:
		NRks resistenza caratteristica a trazione del connettore metallico
		NRkp resistenza caratteristica pullout+espulsione del cono
		'''
		vars(self).update(locals())
		self.taurk={True:taurkcr,False:taurkucr}[cracked]

	@property
	def NRks(self):
		'resistenza a trazione connettore metallico'
		return self.d**2*.25*pi*self.fuk

	def _Nrkp(self,fullout=True):
		'''resistenza per pullout'''
		N0rkp=pi*self.d*self.hef*self.taurk
		#calcolo di scrnp
		scrnp=min((20*self.d*(self.taurkucrc2025/7.5)**.5,3*self.hef))
		ccrnp=scrnp/2
		A0pn=scrnp**2
		c=self.c
		s=self.s
		if c is None and s is None:
			Apn=scrnp**2
		elif s is None:
			c=atleast_1d(c)
			if c.size==1: c=r_[c,0]
			for n,i in enumerate(c):
				c[n]=min((i,ccrnp))
			l=scrnp+c
			Apn=c[0]*c[1]
		elif c is None:
			raise WIP('caso con solo s diverso da 0 non implementato')
		else:
			raise WIP('caso con solo s e c diversi da 0 non implementato')
		#distanza dal bordo	
		psisnp=min((1,0.7+0.3*min(c)/ccrnp))
		#effetto gruppo 
		k={True:2.3,False:3.2}[self.cracked]
		psi0gnp=max((1 , sqrt(n)-(sqrt(n)-1)*(self.d*self.tarurk/(k*sqrt(self.heff*self.Rck)))**1.5))
		psignp=max((psi0gn-(mean(r_[s]/scrnp)**2*(psi0gnp-1),1)))
		#eccentricità su gruppo
		psiecnp=min((1/(1+2*en/scrnp),1))
		#effetto dell'armatura
		psirenp=min((0.5*self.hef/200,1))
		self.__Nrkp=N0rkp*(Apn/A0pn)*psisnp*psignp*psiecnp*psirenp
		if fullout:
			Nrkp=self.__Nrkp
			return locals()
		return self.__Nrkp

	@property
	def Nrkp(self):
		if not hasattr(self,'__Nrkp'):
			self._Nrkp(False)
		return self.__Nrkp

	def _Nrkc(self,fullout=True):
		'resistenza per rottura del cono'
		k1={True:7.2,False:10.1}[self.cracked]
		N0rkc=k1*sqrt(self.Rck)*self.hef**1.5
		scrn=3*self.hef
		ccrn=scrn/2
		A0cn=scrn**2
		print('da FARE calcolo di Acn')
		Acn=A0cn
		psisn=min((0.7+0.3*c/crn,1))
		#effetto dell'armatura
		psiren=min((0.5*self.hef/200,1))
		#eccentricità su gruppo
		psiecn=min((1/(1+2*en/scrn),1))
		self.__Nrkc=N0rkc*Acn/A0cn*psisn*psiren*psiecn
		if fullout:
			Nrkc=self.__Nrkc
			return locals()
		return self.__Nrkc

	@property
	def Nrkc(self):
		if not hasattr(self,'__Nrkc'):
			self._Nrkc(False)
		return self.__Nrkc
			





c=Connettore(800,12,150,.2,.4,c=None,s=None)
c.Nrkp(.1)

