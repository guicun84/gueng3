#-*-coding:latin-*-

from scipy import *
from copy import copy
class Vento:
	'''vb =velocitÓ di riferimento
	   qb= pressione cinetica di riferimento
	   ce(z)= coefficiente di esposizione
	   p(z)= pressione da moltiplicare per coefficiente aerodinamico'''
	#valori per le zone di riferimento
	vb0=[25,25,27,28,28,28,28,30,31]
	a0=[1000,750,500,500,750,500,1000,1500,500]
	ka=[.01,.015,.02,.02,.015,.02,.015,.01,.02]
	#parametri per coefficiente di esposizione
	kr=[.17,.19,.2,.22,.23]
	z0=[.01,.05,.1,.3,.7]
	zmin=[2,4,5,8,12]
	rho=1.25
	def __init__(self,zona=7,cat=1,as_=0,h=0):
		'''zona= zona normativa di riferimento
		cat=categoria di esposizione
		as_=altezza sul livello del mare
		h=altezza edificio'''
		vars(self).update(locals())
		self.__cat=cat

		self.vb0,self.a0,self.ka=Vento.vb0[zona-1],Vento.a0[zona-1],Vento.ka[zona-1]
		self.kr,self.z0,self.zmin=Vento.kr[self.__cat-1],Vento.z0[self.__cat-1],Vento.zmin[self.__cat-1]
	@property
	def vb(self):
		'''velocitÓ di riferimento'''
		if self.as_<=self.a0:
			return self.vb0
		else:
			return self.vb0 + self.ka*(self.as_-self.a0)

	@property
	def qb(self): 
		'''pressione di riferimento'''
		return .5*self.vb**2*self.rho

	def __ce(self,z=None):
		'''coefficiente di esposizione'''
		if z==None:
			z=self.h
		if z<self.zmin:
			z=self.zmin
		return self.kr**2*log(z/self.z0)*(7+log(z/self.z0))
	__ce_=vectorize(__ce)
	def ce(self,z=None):
		'''coefficiente di esposizione'''
		return self.__ce_(self,z)

	def __p(self,z=None):
		'''pressione cinetica di picco'''
		if z==None:
			z=self.h
		ce=self.__ce(z)
		return self.qb*ce
	__p_=vectorize(__p)
	def p(self,z=None):
		'''pressione cinetica di picco'''
		out=self.__p_(self,z)
		if out.ndim==0:
			out=out.item()
		return out

	def Iv(self,z=None):
		'''intensitÓ turbolenza vx.std()/vx.mean()'''
		if z is None: z=self.h
		if z<self.z0:
			z=self.z0
		return 1/log(z/self.z0)

	def Lv(self,z=None):
		'''scala integrale turbolenza= dim media vortici '''
		if z is None: z=self.h
		L=300 #da cnr
		z_=200 #da cnr
		if z<self.z0: z=self.z0
		lam=[.44,.52,.55,.61,.65][self.cat-1]
		return L*(z/z_)**lam

	def vr(self,z=None):
		'velocitÓ raffica in funzione della pressione'
		if z is None: z=self.h
		return sqrt(2*self.p(z)/self.rho)

	def __str__(self):
		st='''Dati iniziali
zona ={zona} zona di riferimento
cat= {cat} categoria di esposizione
as= {as_} m altezza sul livello del mare'''
		if self.h: st+='\nh= {h} altezza edificio'
		st+='''\n
Parametri per zona {zona}
v~b0~= {vb0} m/s
a~0~= {a0} m
k~a~= {ka} 

Parametri per categoria di esposizione {cat}
k~r~= {kr}
z~0~= {z0} m 
z~min~={zmin} m'''
		if self.h:
			ce=atleast_1d(self.ce())[0]
			pref=self.p()
			st+='''\nc~e~= {ce:.4g}
p~ref~= {pref:.4g} Pa'''
		dati=copy(vars(self))
		dati.update(locals())
		return st.format(**dati)


