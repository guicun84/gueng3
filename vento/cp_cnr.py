#-*- coding:latin-*-

from scipy import *
#appendice G (pag 89(97)) CNR-DT-207-2008

class Faccia:
	def __init__(self,h,d,codice=''):
		'''h=altezza
		d=dimensione // vento dell'edificio
		codice=[u,l,d] per indicare se sopra vento(u) sotto vento(d) laterale(d)'''
		vars(self).update(locals())
		self.r=h/d
	@property
	def cpe(self):
		pass

	def __str__(self):
		return '''d={:.2f} , h={:.2f} , h/d={:.2f} Cpe{}={:.2f}'''.format(self.d,self.h,self.r,self.codice,self.cpe)

class FacciaSopraVento(Faccia):
	def __init__(self,h,d):
		Faccia.__init__(self,h,d,'u')
	@property
	def cpe(self):
		if self.r<=1: return 0.7+0.1*self.r
		else:return .8
	
class FacciaSottoVento(Faccia):
	def __init__(self,h,d):
		Faccia.__init__(self,h,d,'d')
	@property
	def cpe(self):
		if self.r<=1: return -.3-.2*self.r
		else:return -.5-.05*(self.r-1)
		
class FacciaLaterale(Faccia):
	def __init__(self,h,d):
		Faccia.__init__(self,h,d,'s')
	@property
	def cpe(self):
		if self.r<=.5: return -.5-.8*self.r
		else:return -.9

class EdificioRettangolare:
	def __init__(self,a,b,h,ventoPerpendicolareA=True):
		'''a=dimensione in pianta
		b=dimensione dimensione in pianta
		h=altezza edificio
		trasversale=[True,False] se true vento perpendicolare ad a altrimenti a b'''
		vars(self).update(locals())
	@property
	def d(self):
		return {True:self.b,False:self.a}[self.ventoPerpendicolareA]
	@property
	def fu(self):
		d=self.d
		return FacciaSopraVento(self.h,d)
	@property
	def fd(self):
		d=self.d
		return FacciaSottoVento(self.h,d)
	@property
	def fs(self):
		d=self.d
		return FacciaLaterale(self.h,d)

	@property
	def cpeu(self):
		return self.fu.cpe
	@property
	def cpes(self):
		return self.fs.cpe
	@property
	def cped(self):
		return self.fd.cpe

	def __str__(self):

		return '''dimensioni {:.2f} x {:.2f} x {:.2f}
vento perpendicolare al lato {}
Faccia sopra vento {}
Faccia laterale    {}
Faccia sotto vento {}'''.format(self.a,self.b,self.h,{True:'a',False:'b'}[self.ventoPerpendicolareA],self.fu,self.fs,self.fd)


class FaldaPiana:
	def __init__(self,b,h):
		'''b=dimensione perpendicolare al vento
		h=altezza su piano campagna

		sono distinte zona a e b con definizione di 
		cpeu,lu= cpe,lunghezza zona a iniziale
		cped= cpe zona di riattacco'''
		vars(self).update(locals())
		self.lu=min((b/2,h))
		self.cpeu=-.8
		self.cped=r_[-.2,.2]

	def __str__(self):
		return '''dimensione trasversale={b:.2f} altezza={h:.2f}
estenzione sopra vento= {lu:.2f} 
Cpeu={cpeu:.2f} , Cped={cped}'''.format(**vars(self))
	
		


class FaldaSingola(object):
	def __init__(self,alpha,b=None,h=None):
		'''alpha inclinazione gradi
		b=lunghezza gronda
		h=altezza tetto
		b e h opzionali, necessari solo per definire automaticamente FaldaPiana'''
		vars(self).update(locals())
	def __new__(cls,alpha,b=None,h=None):
		if abs(alpha)<5:
			if b is not None:
				return FaldaPiana(b,h)
			else: raise ValueError('abs(alpha)<5 --> specificare b e h per usare FaldaPiana')
		else:
			return super(FaldaSingola,cls).__new__(cls,alpha,b,h)

	@staticmethod
	def __cpen(a):
		'calcolo cp negativo'
		if a<=-60: return -.5
		elif a<=-15:return -.5-(a+60)/90
		elif a<=30:return -1+(a+15)/75
		elif a<=45:return -.4+(a-30)/37.5
	@staticmethod
	def __cpep(a):
		'calcolo cp poitivo'
		if a<=0: return None
		elif a<=45: return a/75
		elif a<=75: return .6+(a-45)/150

	@staticmethod
	def __cpesu(a):
		'calcolo cpe per vento //colmo parte sopra vento'
		a=abs(a)
		if a<=15: return -.6-a/50
		else: return -1
	
	@staticmethod
	def __lsu(b,h):
		'calcolo estensione zona sopra vento per vento // colmo'
		return min((b/2,h))

	@staticmethod
	def __cpesd(a):
		'calcolo cpe per vento //colmo parte sotto vento'
		a=abs(a)
		if a<=15: return -.6-a/30
		elif a<45: return -.7-(a-15)/150
		else:return -.9+(a-45)/75

	@property
	def cpeu(self):
		'cp con falda sopra vento (vento sale sulla falda)'
		cp=r_[self.__cpen(self.alpha),self.__cpep(self.alpha)]
		return cp[-isnan(cp)]
	@property
	def cped(self):
		'cp con falda sotto vento (vento scende su falda)'
		cp=array([self.__cpen(-self.alpha),self.__cpep(-self.alpha)],float64)
		return cp[-isnan(cp)]
	
	@property
	def cpesu(self):
		'cp laterale sopra vento'
		if self.b is None: return None
		else: return self.__cpesu(self.alpha)
	@property
	def cpesd(self):
		'cp laterale sotto vento'
		if self.b is None: return None
		else: return self.__cpesd(self.alpha)
	@property
	def lsu(self):
		'lunghezza zona a vento laterale sopra vento'
		if self.b is None: return None
		else: return self.__lsu(self.b,self.h)


	def __str__(self):
		st= '''inclinazione {}�
condizione di sopra vento Cpeu={}
condizione di sotto vento Cped={}'''.format(self.alpha,self.cpu,self.cpd)
		if self.b is None: return st
		st+='''\nCondizione vento parallelo colmo:
Zona sopra vento: l={} Cpeu={}
zona sotto vento: Cped={}'''.format(self.lsu,self.cpesu,self.cpesd)
		return st

class FaldaSottovento:
	def __init__(self,alpha):
		self.alpha=alpha
		if abs(alpha)<5:
			self=None
	@property
	def cpe(self):
		a=self.alpha
		if a<-75:return None
		elif a<=-15:return -.85+(a+60)/180
		elif a<15:return -.6
		elif a<45: return -.6+(a-15)/100
		else: return -.3

class FaldaDoppia(object):
	def __init__(self,alphau,alphad=None,b=None,h=None):
		'''alphau[d]= inclinazioni falde, se alphad==N:alphad=alphau
		b,h :lunghezza gronda e altezza per definire eventuale falda piana'''
		if alphad is None: alphad=alphau
		vars(self).update(locals())
		self.__fu=FaldaSingola(alphau,b,h)
		if isinstance(self.__fu,FaldaSingola):
			self.__fd=FaldaSottovento(alphad)
	@property
	def cpeu(self):
		return self.__fu.cpeu
	
	@property
	def cped(self):
		if hasattr(self,'_FaldaDoppia__fd'):
			return self.__fd.cpe
		else:
			return self.__fu.cped
	@property
	def cpesu(self):
		if self.b is None :return None
		a=mean((self.alphau,self.alphad))
		if a<=30: return -1
		elif a <=0: return -.8+a/150
		elif a<30:reurn -.8-a/150
		else: return -1

	@property
	def lsu(self):
		if self.b is None:return None
		return min((self.b/2,self.h))
	
	@property
	def cpesd(self):
		if self.b is None:return None
		a=mean((self.alphau,self.alphad))
		if a<-45: return None
		elif a<=-30: return -.9
		elif a<=10: return -.9+(a+30)/100
		else: return -.5

	def __str__(self):
		if hasattr(self,'_FaldaDoppia__fd'):
			st='''Falda sopra vento:
inclinazione {}�
Cpeu ={}
Falda di sottovento
inclinazione={}�
Cped ={}'''.format(self.alphau,self.cpeu,self.alphad,self.cped)
			if self.b is not None:
				st+='''\nCondizione con vento parallelo colmo:
zona sopra vento : lunghezza {}, Cpesu={}
zona sotto vento : Cpesd={}'''.format(self.lsu,self.cpesu,self.cpesd)
			return st
		else: return self.__fu.__str__()


class PressioniInterneUniformi:
	def __init__(self,vals):
		'''calcolo pressioni interne per strutture con aperture uniformi
		vals=lista con [[base,altezza,cp],...]'''
		if type(vals) in (list,tuple):
			vals=array(vals)
		vars(self).update(locals())
	@property
	def cpi(self):
		A=r_[[i[0]*i[1] for i in self.vals]]
		cpi=sum(A**2*self.vals[:,-1])/sum(A**2)
		return cpi
	
	def __str__(self):
		facce ='\n'.join([f'{i[0]:.3f} x {i[1]:.3f} , cpe={i[2]:.3f}' for i in self.vals])
		return f'facce:\n{facce}\ncpi={self.cpi:.3f}'

